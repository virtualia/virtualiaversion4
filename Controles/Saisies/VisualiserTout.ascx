﻿<%@ Control Language="VB" AutoEventWireup="false" Inherits="Virtualia.Net.Controles_VisualiserTout" Codebehind="VisualiserTout.ascx.vb" %>

<asp:Table ID="Titre" runat="server" CellPadding="1" CellSpacing="0" Width="650px">
    <asp:TableRow>
        <asp:TableCell HorizontalAlign="Center">
            <asp:Label ID="Intitule" runat="server" Height="20px" Width="400px" Text="Nom et prénom"
                    BackColor="#E2FDF7" BorderColor="#B0E0D7"  BorderStyle="Outset"
                    BorderWidth="2px" ForeColor="#142425" Font-Italic="true"
                    Font-Bold="true" Font-Names="Trebuchet MS" Font-Size="Small"
                    style="margin-top: 5px; margin-left: 4px; text-indent: 5px; text-align: center" >
            </asp:Label>
        </asp:TableCell>
    </asp:TableRow>
</asp:Table>
<asp:ListView ID="VisuTout" runat="server">
    <LayoutTemplate>
        <table id="Cadre" runat="server" 
                style="width:600px; background-color:#E9FDF9">
            <tr runat="server" id="itemPlaceholder">
            </tr>
        </table>
        <asp:DataPager runat="server" ID="DataPager" PageSize="10" >
            <Fields>
                <asp:NumericPagerField ButtonCount="5" PreviousPageText="<--" NextPageText="-->" />
            </Fields>
        </asp:DataPager>
    </LayoutTemplate>
    <ItemTemplate>
        <tr id="HlinkObjet" runat="server" style="margin-top:5px; height:20px; padding:0px; line-height:20px">
            <td valign="top" colspan="4" align="left" style="margin-left:10px">
                <asp:Label ID="Objet" runat="server" Height="20px" Width="400px" Text='<%#Eval("NomObjet") %>'
                    BorderStyle="None" ForeColor="#0E5F5C"  BackColor="Transparent"
                    Font-Italic="true" Font-Bold="true" Font-Names="Trebuchet MS" Font-Size="Small"
                    style="margin-top: 0px; margin-left: 5px; text-indent: 5px; text-align: left" >
                </asp:Label>
            </td>
        </tr>
        <tr id="Ligne00" runat="server" style="margin-top:1px; height:10px">
            <td valign="top" align="left" >
                <asp:Label ID="EtiDebut" runat="server" Height="10px" Width="140px" Text='<%#Eval("Etiquette_Debut") %>'
                    BorderStyle="None" ForeColor="#46A390"  BackColor="Transparent"
                    Font-Italic="true" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Smaller"
                    style="margin-top: 0px; margin-left: 5px; text-indent: 5px; text-align: left" >
                </asp:Label>
            </td>
            <td valign="top" align="left">
                <asp:Label ID="DonDebut" runat="server" Height="10px" Width="80px" Text='<%#Eval("Colonne_Debut") %>'
                    BorderStyle="None" ForeColor="#142425"  BackColor="Transparent"
                    Font-Italic="false" Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Smaller"
                    style="margin-top: 0px; margin-left: 2px; text-indent: 5px; text-align: left" >
                </asp:Label>
            </td>
            <td valign="top" align="left" >
                <asp:Label ID="EtiFin" runat="server" Height="10px" Width="140px" Text='<%#Eval("Etiquette_Fin") %>'
                    BorderStyle="None" ForeColor="#46A390"  BackColor="Transparent"
                    Font-Italic="true" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Smaller"
                    style="margin-top: 0px; margin-left: 2px; text-indent: 5px; text-align: left" >
                </asp:Label>
            </td>
            <td valign="top" align="left">
                <asp:Label ID="DonFin" runat="server" Height="10px" Width="80px" Text='<%#Eval("Colonne_Fin") %>'
                    BorderStyle="None" ForeColor="#142425"  BackColor="Transparent"
                    Font-Italic="false" Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Smaller"
                    style="margin-top: 0px; margin-left: 2px; text-indent: 5px; text-align: left" >
                </asp:Label>
            </td>
        </tr>
        <tr id="Ligne01" runat="server" style="margin-top:1px; height:10px; padding:0px; line-height:10px; background-color:#E2FDF7">
            <td valign="top" align="left" colspan="2">
                <asp:Label ID="Eti01" runat="server" Height="10px" Width="170px" Text='<%#Eval("Etiquette_1") %>'
                    BorderStyle="None" ForeColor="GrayText"  BackColor="Transparent"
                    Font-Italic="true" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Smaller"
                    style="margin-top: 0px; margin-left: 5px; text-indent: 5px; text-align: left" >
                </asp:Label>
            </td>
            <td valign="top" align="left" colspan="2">
                <asp:Label ID="Don01" runat="server" Height="10px" Width="320px" Text='<%#Eval("Colonne_1") %>'
                    BorderStyle="None" ForeColor="#142425"  BackColor="Transparent"
                    Font-Italic="false" Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Smaller"
                    style="margin-top: 0px; margin-left: 2px; text-indent: 5px; text-align: left" >
                </asp:Label>
            </td>
        </tr>
        <tr id="Ligne02" runat="server" style="margin-top:1px; height:10px; padding:0px; line-height:10px">
            <td valign="top" align="left" colspan="2">
                <asp:Label ID="Eti02" runat="server" Height="10px" Width="170px" Text='<%#Eval("Etiquette_2") %>'
                    BorderStyle="None" ForeColor="GrayText"  BackColor="Transparent"
                    Font-Italic="true" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Smaller"
                    style="margin-top: 0px; margin-left: 5px; text-indent: 5px; text-align: left" >
                </asp:Label>
            </td>
            <td valign="top" align="left" colspan="2">
                <asp:Label ID="Don02" runat="server" Height="10px" Width="320px" Text='<%#Eval("Colonne_2") %>'
                    BorderStyle="None" ForeColor="#142425"  BackColor="Transparent"
                    Font-Italic="false" Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Smaller"
                    style="margin-top: 0px; margin-left: 2px; text-indent: 5px; text-align: left" >
                </asp:Label>
            </td>
        </tr>
        <tr id="Ligne03" runat="server" style="margin-top:1px; height:10px; padding:0px; line-height:10px; background-color:#E2FDF7">
            <td valign="top" align="left" colspan="2">
                <asp:Label ID="Eti03" runat="server" Height="10px" Width="170px" Text='<%#Eval("Etiquette_3") %>'
                    BorderStyle="None" ForeColor="GrayText"  BackColor="Transparent"
                    Font-Italic="true" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Smaller"
                    style="margin-top: 0px; margin-left: 5px; text-indent: 5px; text-align: left" >
                </asp:Label>
            </td>
            <td valign="top" align="left" colspan="2">
                <asp:Label ID="Don03" runat="server" Height="10px" Width="320px" Text='<%#Eval("Colonne_3") %>'
                    BorderStyle="None" ForeColor="#142425"  BackColor="Transparent"
                    Font-Italic="false" Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Smaller"
                    style="margin-top: 0px; margin-left: 2px; text-indent: 5px; text-align: left" >
                </asp:Label>
            </td>
        </tr>
         <tr id="Ligne04" runat="server" style="margin-top:1px; height:10px; padding:0px; line-height:10px">
            <td valign="top" align="left" colspan="2">
                <asp:Label ID="Eti04" runat="server" Height="10px" Width="170px" Text='<%#Eval("Etiquette_4") %>'
                    BorderStyle="None" ForeColor="GrayText"  BackColor="Transparent"
                    Font-Italic="true" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Smaller"
                    style="margin-top: 0px; margin-left: 5px; text-indent: 5px; text-align: left" >
                </asp:Label>
            </td>
            <td valign="top" align="left" colspan="2">
                <asp:Label ID="Don04" runat="server" Height="10px" Width="320px" Text='<%#Eval("Colonne_4") %>'
                    BorderStyle="None" ForeColor="#142425"  BackColor="Transparent"
                    Font-Italic="false" Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Smaller"
                    style="margin-top: 0px; margin-left: 2px; text-indent: 5px; text-align: left" >
                </asp:Label>
            </td>
        </tr>
         <tr id="Ligne05" runat="server" style="margin-top:1px; height:10px; padding:0px; line-height:10px; background-color:#E2FDF7">
            <td valign="top" align="left" colspan="2">
                <asp:Label ID="Eti05" runat="server" Height="10px" Width="170px" Text='<%#Eval("Etiquette_5") %>'
                    BorderStyle="None" ForeColor="GrayText"  BackColor="Transparent"
                    Font-Italic="true" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Smaller"
                    style="margin-top: 0px; margin-left: 5px; text-indent: 5px; text-align: left" >
                </asp:Label>
            </td>
            <td valign="top" align="left" colspan="2">
                <asp:Label ID="Don05" runat="server" Height="10px" Width="320px" Text='<%#Eval("Colonne_5") %>'
                    BorderStyle="None" ForeColor="#142425"  BackColor="Transparent"
                    Font-Italic="false" Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Smaller"
                    style="margin-top: 0px; margin-left: 2px; text-indent: 5px; text-align: left" >
                </asp:Label>
            </td>
        </tr>
         <tr id="Ligne06" runat="server" style="margin-top:1px; height:10px; padding:0px; line-height:10px">
            <td valign="top" align="left" colspan="2">
                <asp:Label ID="Eti06" runat="server" Height="10px" Width="170px" Text='<%#Eval("Etiquette_6") %>'
                    BorderStyle="None" ForeColor="GrayText"  BackColor="Transparent"
                    Font-Italic="true" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Smaller"
                    style="margin-top: 0px; margin-left: 5px; text-indent: 5px; text-align: left" >
                </asp:Label>
            </td>
            <td valign="top" align="left" colspan="2">
                <asp:Label ID="Don06" runat="server" Height="10px" Width="320px" Text='<%#Eval("Colonne_6") %>'
                    BorderStyle="None" ForeColor="#142425"  BackColor="Transparent"
                    Font-Italic="false" Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Smaller"
                    style="margin-top: 0px; margin-left: 2px; text-indent: 5px; text-align: left" >
                </asp:Label>
            </td>
        </tr>
         <tr id="Ligne07" runat="server" style="margin-top:1px; height:10px; padding:0px; line-height:10px; background-color:#E2FDF7">
            <td valign="top" align="left" colspan="2">
                <asp:Label ID="Eti07" runat="server" Height="10px" Width="170px" Text='<%#Eval("Etiquette_7") %>'
                    BorderStyle="None" ForeColor="GrayText"  BackColor="Transparent"
                    Font-Italic="true" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Smaller"
                    style="margin-top: 0px; margin-left: 5px; text-indent: 5px; text-align: left" >
                </asp:Label>
            </td>
            <td valign="top" align="left" colspan="2">
                <asp:Label ID="Don07" runat="server" Height="10px" Width="320px" Text='<%#Eval("Colonne_7") %>'
                    BorderStyle="None" ForeColor="#142425"  BackColor="Transparent"
                    Font-Italic="false" Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Smaller"
                    style="margin-top: 0px; margin-left: 2px; text-indent: 5px; text-align: left" >
                </asp:Label>
            </td>
        </tr>
         <tr id="Ligne08" runat="server" style="margin-top:1px; height:10px; padding:0px; line-height:10px">
            <td valign="top" align="left" colspan="2">
                <asp:Label ID="Eti08" runat="server" Height="10px" Width="170px" Text='<%#Eval("Etiquette_8") %>'
                    BorderStyle="None" ForeColor="GrayText"  BackColor="Transparent"
                    Font-Italic="true" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Smaller"
                    style="margin-top: 0px; margin-left: 5px; text-indent: 5px; text-align: left" >
                </asp:Label>
            </td>
            <td valign="top" align="left" colspan="2">
                <asp:Label ID="Don08" runat="server" Height="10px" Width="320px" Text='<%#Eval("Colonne_8") %>'
                    BorderStyle="None" ForeColor="#142425"  BackColor="Transparent"
                    Font-Italic="false" Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Smaller"
                    style="margin-top: 0px; margin-left: 2px; text-indent: 5px; text-align: left" >
                </asp:Label>
            </td>
        </tr>
         <tr id="Ligne09" runat="server" style="margin-top:1px; height:10px; padding:0px; line-height:10px; background-color:#E2FDF7">
            <td valign="top" align="left" colspan="2">
                <asp:Label ID="Eti09" runat="server" Height="10px" Width="170px" Text='<%#Eval("Etiquette_9") %>'
                    BorderStyle="None" ForeColor="GrayText"  BackColor="Transparent"
                    Font-Italic="true" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Smaller"
                    style="margin-top: 0px; margin-left: 5px; text-indent: 5px; text-align: left" >
                </asp:Label>
            </td>
            <td valign="top" align="left" colspan="2">
                <asp:Label ID="Don09" runat="server" Height="10px" Width="320px" Text='<%#Eval("Colonne_9") %>'
                    BorderStyle="None" ForeColor="#142425"  BackColor="Transparent"
                    Font-Italic="false" Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Smaller"
                    style="margin-top: 0px; margin-left: 2px; text-indent: 5px; text-align: left" >
                </asp:Label>
            </td>
        </tr>
        <tr id="Ligne10" runat="server" style="margin-top:1px; height:10px; padding:0px; line-height:10px">
            <td valign="top" align="left" colspan="2">
                <asp:Label ID="Eti10" runat="server" Height="10px" Width="170px" Text='<%#Eval("Etiquette_10") %>'
                    BorderStyle="None" ForeColor="GrayText"  BackColor="Transparent"
                    Font-Italic="true" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Smaller"
                    style="margin-top: 0px; margin-left: 5px; text-indent: 5px; text-align: left" >
                </asp:Label>
            </td>
            <td valign="top" align="left" colspan="2">
                <asp:Label ID="Don10" runat="server" Height="10px" Width="320px" Text='<%#Eval("Colonne_10") %>'
                    BorderStyle="None" ForeColor="#142425"  BackColor="Transparent"
                    Font-Italic="false" Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Smaller"
                    style="margin-top: 0px; margin-left: 2px; text-indent: 5px; text-align: left" >
                </asp:Label>
            </td>
        </tr>
        <tr id="Ligne11" runat="server" style="margin-top:1px; height:10px; padding:0px; line-height:10px; background-color:#E2FDF7">
            <td valign="top" align="left" colspan="2">
                <asp:Label ID="Eti11" runat="server" Height="10px" Width="170px" Text='<%#Eval("Etiquette_11") %>'
                    BorderStyle="None" ForeColor="GrayText"  BackColor="Transparent"
                    Font-Italic="true" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Smaller"
                    style="margin-top: 0px; margin-left: 5px; text-indent: 5px; text-align: left" >
                </asp:Label>
            </td>
            <td valign="top" align="left" colspan="2">
                <asp:Label ID="Don11" runat="server" Height="10px" Width="320px" Text='<%#Eval("Colonne_11") %>'
                    BorderStyle="None" ForeColor="#142425"  BackColor="Transparent"
                    Font-Italic="false" Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Smaller"
                    style="margin-top: 0px; margin-left: 2px; text-indent: 5px; text-align: left" >
                </asp:Label>
            </td>
        </tr>
        <tr id="Ligne12" runat="server" style="margin-top:1px; height:10px; padding:0px; line-height:10px">
            <td valign="top" align="left" colspan="2">
                <asp:Label ID="Eti12" runat="server" Height="10px" Width="170px" Text='<%#Eval("Etiquette_12") %>'
                    BorderStyle="None" ForeColor="GrayText"  BackColor="Transparent"
                    Font-Italic="true" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Smaller"
                    style="margin-top: 0px; margin-left: 5px; text-indent: 5px; text-align: left" >
                </asp:Label>
            </td>
            <td valign="top" align="left" colspan="2">
                <asp:Label ID="Don12" runat="server" Height="10px" Width="320px" Text='<%#Eval("Colonne_12") %>'
                    BorderStyle="None" ForeColor="#142425"  BackColor="Transparent"
                    Font-Italic="false" Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Smaller"
                    style="margin-top: 0px; margin-left: 2px; text-indent: 5px; text-align: left" >
                </asp:Label>
            </td>
        </tr>
        <tr id="Ligne13" runat="server" style="margin-top:1px; height:10px; padding:0px; line-height:10px; background-color:#E2FDF7">
            <td valign="top" align="left" colspan="2">
                <asp:Label ID="Eti13" runat="server" Height="10px" Width="170px" Text='<%#Eval("Etiquette_13") %>'
                    BorderStyle="None" ForeColor="GrayText"  BackColor="Transparent"
                    Font-Italic="true" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Smaller"
                    style="margin-top: 0px; margin-left: 5px; text-indent: 5px; text-align: left" >
                </asp:Label>
            </td>
            <td valign="top" align="left" colspan="2">
                <asp:Label ID="Don13" runat="server" Height="10px" Width="320px" Text='<%#Eval("Colonne_13") %>'
                    BorderStyle="None" ForeColor="#142425"  BackColor="Transparent"
                    Font-Italic="false" Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Smaller"
                    style="margin-top: 0px; margin-left: 2px; text-indent: 5px; text-align: left" >
                </asp:Label>
            </td>
        </tr>
         <tr id="Ligne14" runat="server" style="margin-top:1px; height:10px; padding:0px; line-height:10px">
            <td valign="top" align="left" colspan="2">
                <asp:Label ID="Eti14" runat="server" Height="10px" Width="170px" Text='<%#Eval("Etiquette_14") %>'
                    BorderStyle="None" ForeColor="GrayText"  BackColor="Transparent"
                    Font-Italic="true" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Smaller"
                    style="margin-top: 0px; margin-left: 5px; text-indent: 5px; text-align: left" >
                </asp:Label>
            </td>
            <td valign="top" align="left" colspan="2">
                <asp:Label ID="Don14" runat="server" Height="10px" Width="320px" Text='<%#Eval("Colonne_14") %>'
                    BorderStyle="None" ForeColor="#142425"  BackColor="Transparent"
                    Font-Italic="false" Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Smaller"
                    style="margin-top: 0px; margin-left: 2px; text-indent: 5px; text-align: left" >
                </asp:Label>
            </td>
        </tr>
         <tr id="Ligne15" runat="server" style="margin-top:1px; height:10px; padding:0px; line-height:10px; background-color:#E2FDF7">
            <td valign="top" align="left" colspan="2">
                <asp:Label ID="Eti15" runat="server" Height="10px" Width="170px" Text='<%#Eval("Etiquette_15") %>'
                    BorderStyle="None" ForeColor="GrayText"  BackColor="Transparent"
                    Font-Italic="true" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Smaller"
                    style="margin-top: 0px; margin-left: 5px; text-indent: 5px; text-align: left" >
                </asp:Label>
            </td>
            <td valign="top" align="left" colspan="2">
                <asp:Label ID="Don15" runat="server" Height="10px" Width="320px" Text='<%#Eval("Colonne_15") %>'
                    BorderStyle="None" ForeColor="#142425"  BackColor="Transparent"
                    Font-Italic="false" Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Smaller"
                    style="margin-top: 0px; margin-left: 2px; text-indent: 5px; text-align: left" >
                </asp:Label>
            </td>
        </tr>
         <tr id="Ligne16" runat="server" style="margin-top:1px; height:10px; padding:0px; line-height:10px">
            <td valign="top" align="left" colspan="2">
                <asp:Label ID="Eti16" runat="server" Height="10px" Width="170px" Text='<%#Eval("Etiquette_16") %>'
                    BorderStyle="None" ForeColor="GrayText"  BackColor="Transparent"
                    Font-Italic="true" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Smaller"
                    style="margin-top: 0px; margin-left: 5px; text-indent: 5px; text-align: left" >
                </asp:Label>
            </td>
            <td valign="top" align="left" colspan="2">
                <asp:Label ID="Don16" runat="server" Height="10px" Width="320px" Text='<%#Eval("Colonne_16") %>'
                    BorderStyle="None" ForeColor="#142425"  BackColor="Transparent"
                    Font-Italic="false" Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Smaller"
                    style="margin-top: 0px; margin-left: 2px; text-indent: 5px; text-align: left" >
                </asp:Label>
            </td>
        </tr>
         <tr id="Ligne17" runat="server" style="margin-top:1px; height:10px; padding:0px; line-height:10px; background-color:#E2FDF7">
            <td valign="top" align="left" colspan="2">
                <asp:Label ID="Eti17" runat="server" Height="10px" Width="170px" Text='<%#Eval("Etiquette_17") %>'
                    BorderStyle="None" ForeColor="GrayText"  BackColor="Transparent"
                    Font-Italic="true" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Smaller"
                    style="margin-top: 0px; margin-left: 5px; text-indent: 5px; text-align: left" >
                </asp:Label>
            </td>
            <td valign="top" align="left" colspan="2">
                <asp:Label ID="Don17" runat="server" Height="10px" Width="320px" Text='<%#Eval("Colonne_17") %>'
                    BorderStyle="None" ForeColor="#142425"  BackColor="Transparent"
                    Font-Italic="false" Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Smaller"
                    style="margin-top: 0px; margin-left: 2px; text-indent: 5px; text-align: left" >
                </asp:Label>
            </td>
        </tr>
         <tr id="Ligne18" runat="server" style="margin-top:1px; height:10px; padding:0px; line-height:10px">
            <td valign="top" align="left" colspan="2">
                <asp:Label ID="Eti18" runat="server" Height="10px" Width="170px" Text='<%#Eval("Etiquette_18") %>'
                    BorderStyle="None" ForeColor="GrayText"  BackColor="Transparent"
                    Font-Italic="true" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Smaller"
                    style="margin-top: 0px; margin-left: 5px; text-indent: 5px; text-align: left" >
                </asp:Label>
            </td>
            <td valign="top" align="left" colspan="2">
                <asp:Label ID="Don18" runat="server" Height="10px" Width="320px" Text='<%#Eval("Colonne_18") %>'
                    BorderStyle="None" ForeColor="#142425"  BackColor="Transparent"
                    Font-Italic="false" Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Smaller"
                    style="margin-top: 0px; margin-left: 2px; text-indent: 5px; text-align: left" >
                </asp:Label>
            </td>
        </tr>
         <tr id="Ligne19" runat="server" style="margin-top:1px; height:10px; padding:0px; line-height:10px; background-color:#E2FDF7">
            <td valign="top" align="left" colspan="2">
                <asp:Label ID="Eti19" runat="server" Height="10px" Width="170px" Text='<%#Eval("Etiquette_19") %>'
                    BorderStyle="None" ForeColor="GrayText"  BackColor="Transparent"
                    Font-Italic="true" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Smaller"
                    style="margin-top: 0px; margin-left: 5px; text-indent: 5px; text-align: left" >
                </asp:Label>
            </td>
            <td valign="top" align="left" colspan="2">
                <asp:Label ID="Don19" runat="server" Height="10px" Width="320px" Text='<%#Eval("Colonne_19") %>'
                    BorderStyle="None" ForeColor="#142425"  BackColor="Transparent"
                    Font-Italic="false" Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Smaller"
                    style="margin-top: 0px; margin-left: 2px; text-indent: 5px; text-align: left" >
                </asp:Label>
            </td>
        </tr>
         <tr id="Ligne20" runat="server" style="margin-top:1px; height:10px; padding:0px; line-height:10px">
            <td valign="top" align="left" colspan="2">
                <asp:Label ID="Eti20" runat="server" Height="10px" Width="170px" Text='<%#Eval("Etiquette_20") %>'
                    BorderStyle="None" ForeColor="GrayText"  BackColor="Transparent"
                    Font-Italic="true" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Smaller"
                    style="margin-top: 0px; margin-left: 5px; text-indent: 5px; text-align: left" >
                </asp:Label>
            </td>
            <td valign="top" align="left" colspan="2">
                <asp:Label ID="Don20" runat="server" Height="10px" Width="320px" Text='<%#Eval("Colonne_20") %>'
                    BorderStyle="None" ForeColor="#142425"  BackColor="Transparent"
                    Font-Italic="false" Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Smaller"
                    style="margin-top: 0px; margin-left: 2px; text-indent: 5px; text-align: left" >
                </asp:Label>
            </td>
        </tr>
        <tr id="Ligne21" runat="server" style="margin-top:1px; height:10px; padding:0px; line-height:10px">
            <td valign="top" align="left" colspan="2">
                <asp:Label ID="Eti21" runat="server" Height="10px" Width="170px" Text='<%#Eval("Etiquette_21") %>'
                    BorderStyle="None" ForeColor="GrayText"  BackColor="Transparent"
                    Font-Italic="true" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Smaller"
                    style="margin-top: 0px; margin-left: 5px; text-indent: 5px; text-align: left" >
                </asp:Label>
            </td>
            <td valign="top" align="left" colspan="2">
                <asp:Label ID="Don21" runat="server" Height="10px" Width="320px" Text='<%#Eval("Colonne_21") %>'
                    BorderStyle="None" ForeColor="#142425"  BackColor="Transparent"
                    Font-Italic="false" Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Smaller"
                    style="margin-top: 0px; margin-left: 2px; text-indent: 5px; text-align: left" >
                </asp:Label>
            </td>
        </tr>
        <tr id="Ligne22" runat="server" style="margin-top:1px; height:10px; padding:0px; line-height:10px">
            <td valign="top" align="left" colspan="2">
                <asp:Label ID="Eti22" runat="server" Height="10px" Width="170px" Text='<%#Eval("Etiquette_22") %>'
                    BorderStyle="None" ForeColor="GrayText"  BackColor="Transparent"
                    Font-Italic="true" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Smaller"
                    style="margin-top: 0px; margin-left: 5px; text-indent: 5px; text-align: left" >
                </asp:Label>
            </td>
            <td valign="top" align="left" colspan="2">
                <asp:Label ID="Don22" runat="server" Height="10px" Width="320px" Text='<%#Eval("Colonne_22") %>'
                    BorderStyle="None" ForeColor="#142425"  BackColor="Transparent"
                    Font-Italic="false" Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Smaller"
                    style="margin-top: 0px; margin-left: 2px; text-indent: 5px; text-align: left" >
                </asp:Label>
            </td>
        </tr>
        <tr id="Ligne23" runat="server" style="margin-top:1px; height:10px; padding:0px; line-height:10px">
            <td valign="top" align="left" colspan="2">
                <asp:Label ID="Don23" runat="server" Height="10px" Width="170px" Text='<%#Eval("Etiquette_23") %>'
                    BorderStyle="None" ForeColor="GrayText"  BackColor="Transparent"
                    Font-Italic="true" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Smaller"
                    style="margin-top: 0px; margin-left: 5px; text-indent: 5px; text-align: left" >
                </asp:Label>
            </td>
            <td valign="top" align="left" colspan="2">
                <asp:Label ID="Eti23" runat="server" Height="10px" Width="320px" Text='<%#Eval("Colonne_23") %>'
                    BorderStyle="None" ForeColor="#142425"  BackColor="Transparent"
                    Font-Italic="false" Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Smaller"
                    style="margin-top: 0px; margin-left: 2px; text-indent: 5px; text-align: left" >
                </asp:Label>
            </td>
        </tr>
        <tr id="Ligne24" runat="server" style="margin-top:1px; height:10px; padding:0px; line-height:10px">
            <td valign="top" align="left" colspan="2">
                <asp:Label ID="Eti24" runat="server" Height="10px" Width="170px" Text='<%#Eval("Etiquette_24") %>'
                    BorderStyle="None" ForeColor="GrayText"  BackColor="Transparent"
                    Font-Italic="true" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Smaller"
                    style="margin-top: 0px; margin-left: 5px; text-indent: 5px; text-align: left" >
                </asp:Label>
            </td>
            <td valign="top" align="left" colspan="2">
                <asp:Label ID="Don24" runat="server" Height="10px" Width="320px" Text='<%#Eval("Colonne_24") %>'
                    BorderStyle="None" ForeColor="#142425"  BackColor="Transparent"
                    Font-Italic="false" Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Smaller"
                    style="margin-top: 0px; margin-left: 2px; text-indent: 5px; text-align: left" >
                </asp:Label>
            </td>
        </tr>
         <tr id="Ligne25" runat="server" style="margin-top:1px; height:10px; padding:0px; line-height:10px">
            <td valign="top" align="left" colspan="2">
                <asp:Label ID="Eti25" runat="server" Height="10px" Width="170px" Text='<%#Eval("Etiquette_25") %>'
                    BorderStyle="None" ForeColor="GrayText"  BackColor="Transparent"
                    Font-Italic="true" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Smaller"
                    style="margin-top: 0px; margin-left: 5px; text-indent: 5px; text-align: left" >
                </asp:Label>
            </td>
            <td valign="top" align="left" colspan="2">
                <asp:Label ID="Don25" runat="server" Height="10px" Width="320px" Text='<%#Eval("Colonne_25") %>'
                    BorderStyle="None" ForeColor="#142425"  BackColor="Transparent"
                    Font-Italic="false" Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Smaller"
                    style="margin-top: 0px; margin-left: 2px; text-indent: 5px; text-align: left" >
                </asp:Label>
            </td>
        </tr>
         <tr id="Ligne26" runat="server" style="margin-top:1px; height:10px; padding:0px; line-height:10px">
            <td valign="top" align="left" colspan="2">
                <asp:Label ID="Eti26" runat="server" Height="10px" Width="170px" Text='<%#Eval("Etiquette_26") %>'
                    BorderStyle="None" ForeColor="GrayText"  BackColor="Transparent"
                    Font-Italic="true" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Smaller"
                    style="margin-top: 0px; margin-left: 5px; text-indent: 5px; text-align: left" >
                </asp:Label>
            </td>
            <td valign="top" align="left" colspan="2">
                <asp:Label ID="Don26" runat="server" Height="10px" Width="320px" Text='<%#Eval("Colonne_26") %>'
                    BorderStyle="None" ForeColor="#142425"  BackColor="Transparent"
                    Font-Italic="false" Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Smaller"
                    style="margin-top: 0px; margin-left: 2px; text-indent: 5px; text-align: left" >
                </asp:Label>
            </td>
        </tr>
         <tr id="Ligne27" runat="server" style="margin-top:1px; height:10px; padding:0px; line-height:10px">
            <td valign="top" align="left" colspan="2">
                <asp:Label ID="Eti27" runat="server" Height="10px" Width="170px" Text='<%#Eval("Etiquette_27") %>'
                    BorderStyle="None" ForeColor="GrayText"  BackColor="Transparent"
                    Font-Italic="true" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Smaller"
                    style="margin-top: 0px; margin-left: 5px; text-indent: 5px; text-align: left" >
                </asp:Label>
            </td>
            <td valign="top" align="left" colspan="2">
                <asp:Label ID="Don27" runat="server" Height="10px" Width="320px" Text='<%#Eval("Colonne_27") %>'
                    BorderStyle="None" ForeColor="#142425"  BackColor="Transparent"
                    Font-Italic="false" Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Smaller"
                    style="margin-top: 0px; margin-left: 2px; text-indent: 5px; text-align: left" >
                </asp:Label>
            </td>
        </tr>
         <tr id="Ligne28" runat="server" style="margin-top:1px; height:10px; padding:0px; line-height:10px">
            <td valign="top" align="left" colspan="2">
                <asp:Label ID="Eti28" runat="server" Height="10px" Width="170px" Text='<%#Eval("Etiquette_28") %>'
                    BorderStyle="None" ForeColor="GrayText"  BackColor="Transparent"
                    Font-Italic="true" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Smaller"
                    style="margin-top: 0px; margin-left: 5px; text-indent: 5px; text-align: left" >
                </asp:Label>
            </td>
            <td valign="top" align="left" colspan="2">
                <asp:Label ID="Don28" runat="server" Height="10px" Width="320px" Text='<%#Eval("Colonne_28") %>'
                    BorderStyle="None" ForeColor="#142425"  BackColor="Transparent"
                    Font-Italic="false" Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Smaller"
                    style="margin-top: 0px; margin-left: 2px; text-indent: 5px; text-align: left" >
                </asp:Label>
            </td>
        </tr>
         <tr id="Ligne29" runat="server" style="margin-top:1px; height:10px; padding:0px; line-height:10px">
            <td valign="top" align="left" colspan="2">
                <asp:Label ID="Eti29" runat="server" Height="10px" Width="170px" Text='<%#Eval("Etiquette_29") %>'
                    BorderStyle="None" ForeColor="GrayText"  BackColor="Transparent"
                    Font-Italic="true" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Smaller"
                    style="margin-top: 0px; margin-left: 5px; text-indent: 5px; text-align: left" >
                </asp:Label>
            </td>
            <td valign="top" align="left" colspan="2">
                <asp:Label ID="Don29" runat="server" Height="10px" Width="320px" Text='<%#Eval("Colonne_29") %>'
                    BorderStyle="None" ForeColor="#142425"  BackColor="Transparent"
                    Font-Italic="false" Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Smaller"
                    style="margin-top: 0px; margin-left: 2px; text-indent: 5px; text-align: left" >
                </asp:Label>
            </td>
        </tr>
         <tr id="Ligne30" runat="server" style="margin-top:1px; height:10px; padding:0px; line-height:10px">
            <td valign="top" align="left" colspan="2">
                <asp:Label ID="Eti30" runat="server" Height="10px" Width="170px" Text='<%#Eval("Etiquette_30") %>'
                    BorderStyle="None" ForeColor="GrayText"  BackColor="Transparent"
                    Font-Italic="true" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Smaller"
                    style="margin-top: 0px; margin-left: 5px; text-indent: 5px; text-align: left" >
                </asp:Label>
            </td>
            <td valign="top" align="left" colspan="2">
                <asp:Label ID="Don30" runat="server" Height="10px" Width="320px" Text='<%#Eval("Colonne_30") %>'
                    BorderStyle="None" ForeColor="#142425"  BackColor="Transparent"
                    Font-Italic="false" Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Smaller"
                    style="margin-top: 0px; margin-left: 2px; text-indent: 5px; text-align: left" >
                </asp:Label>
            </td>
        </tr>
         <tr id="Ligne31" runat="server" style="margin-top:1px; height:10px; padding:0px; line-height:10px">
            <td valign="top" align="left" colspan="2">
                <asp:Label ID="Eti31" runat="server" Height="10px" Width="170px" Text='<%#Eval("Etiquette_31") %>'
                    BorderStyle="None" ForeColor="GrayText"  BackColor="Transparent"
                    Font-Italic="true" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Smaller"
                    style="margin-top: 0px; margin-left: 5px; text-indent: 5px; text-align: left" >
                </asp:Label>
            </td>
            <td valign="top" align="left" colspan="2">
                <asp:Label ID="Don31" runat="server" Height="10px" Width="320px" Text='<%#Eval("Colonne_31") %>'
                    BorderStyle="None" ForeColor="#142425"  BackColor="Transparent"
                    Font-Italic="false" Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Smaller"
                    style="margin-top: 0px; margin-left: 2px; text-indent: 5px; text-align: left" >
                </asp:Label>
            </td>
        </tr>
        <tr id="Ligne32" runat="server" style="margin-top:1px; height:10px; padding:0px; line-height:10px">
            <td valign="top" align="left" colspan="2">
                <asp:Label ID="Eti32" runat="server" Height="10px" Width="170px" Text='<%#Eval("Etiquette_32") %>'
                    BorderStyle="None" ForeColor="GrayText"  BackColor="Transparent"
                    Font-Italic="true" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Smaller"
                    style="margin-top: 0px; margin-left: 5px; text-indent: 5px; text-align: left" >
                </asp:Label>
            </td>
            <td valign="top" align="left" colspan="2">
                <asp:Label ID="Don32" runat="server" Height="10px" Width="320px" Text='<%#Eval("Colonne_32") %>'
                    BorderStyle="None" ForeColor="#142425"  BackColor="Transparent"
                    Font-Italic="false" Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Smaller"
                    style="margin-top: 0px; margin-left: 2px; text-indent: 5px; text-align: left" >
                </asp:Label>
            </td>
        </tr>
        <tr id="Ligne33" runat="server" style="margin-top:1px; height:10px; padding:0px; line-height:10px">
            <td valign="top" align="left" colspan="2">
                <asp:Label ID="Eti33" runat="server" Height="10px" Width="170px" Text='<%#Eval("Etiquette_33") %>'
                    BorderStyle="None" ForeColor="GrayText"  BackColor="Transparent"
                    Font-Italic="true" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Smaller"
                    style="margin-top: 0px; margin-left: 5px; text-indent: 5px; text-align: left" >
                </asp:Label>
            </td>
            <td valign="top" align="left" colspan="2">
                <asp:Label ID="Don33" runat="server" Height="10px" Width="320px" Text='<%#Eval("Colonne_33") %>'
                    BorderStyle="None" ForeColor="#142425"  BackColor="Transparent"
                    Font-Italic="false" Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Smaller"
                    style="margin-top: 0px; margin-left: 2px; text-indent: 5px; text-align: left" >
                </asp:Label>
            </td>
        </tr>
        <tr id="Ligne34" runat="server" style="margin-top:1px; height:10px; padding:0px; line-height:10px">
            <td valign="top" align="left" colspan="2">
                <asp:Label ID="Eti34" runat="server" Height="10px" Width="170px" Text='<%#Eval("Etiquette_34") %>'
                    BorderStyle="None" ForeColor="GrayText"  BackColor="Transparent"
                    Font-Italic="true" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Smaller"
                    style="margin-top: 0px; margin-left: 5px; text-indent: 5px; text-align: left" >
                </asp:Label>
            </td>
            <td valign="top" align="left" colspan="2">
                <asp:Label ID="Don34" runat="server" Height="10px" Width="320px" Text='<%#Eval("Colonne_34") %>'
                    BorderStyle="None" ForeColor="#142425"  BackColor="Transparent"
                    Font-Italic="false" Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Smaller"
                    style="margin-top: 0px; margin-left: 2px; text-indent: 5px; text-align: left" >
                </asp:Label>
            </td>
        </tr>
         <tr id="Ligne35" runat="server" style="margin-top:1px; height:10px; padding:0px; line-height:10px">
            <td valign="top" align="left" colspan="2">
                <asp:Label ID="Eti35" runat="server" Height="10px" Width="170px" Text='<%#Eval("Etiquette_35") %>'
                    BorderStyle="None" ForeColor="GrayText"  BackColor="Transparent"
                    Font-Italic="true" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Smaller"
                    style="margin-top: 0px; margin-left: 5px; text-indent: 5px; text-align: left" >
                </asp:Label>
            </td>
            <td valign="top" align="left" colspan="2">
                <asp:Label ID="Don35" runat="server" Height="10px" Width="320px" Text='<%#Eval("Colonne_35") %>'
                    BorderStyle="None" ForeColor="#142425"  BackColor="Transparent"
                    Font-Italic="false" Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Smaller"
                    style="margin-top: 0px; margin-left: 2px; text-indent: 5px; text-align: left" >
                </asp:Label>
            </td>
        </tr>
         <tr id="Ligne36" runat="server" style="margin-top:1px; height:10px; padding:0px; line-height:10px">
            <td valign="top" align="left" colspan="2">
                <asp:Label ID="Eti36" runat="server" Height="10px" Width="170px" Text='<%#Eval("Etiquette_36") %>'
                    BorderStyle="None" ForeColor="GrayText"  BackColor="Transparent"
                    Font-Italic="true" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Smaller"
                    style="margin-top: 0px; margin-left: 5px; text-indent: 5px; text-align: left" >
                </asp:Label>
            </td>
            <td valign="top" align="left" colspan="2">
                <asp:Label ID="Don36" runat="server" Height="10px" Width="320px" Text='<%#Eval("Colonne_36") %>'
                    BorderStyle="None" ForeColor="#142425"  BackColor="Transparent"
                    Font-Italic="false" Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Smaller"
                    style="margin-top: 0px; margin-left: 2px; text-indent: 5px; text-align: left" >
                </asp:Label>
            </td>
        </tr>
         <tr id="Ligne37" runat="server" style="margin-top:1px; height:10px; padding:0px; line-height:10px">
            <td valign="top" align="left" colspan="2">
                <asp:Label ID="Eti37" runat="server" Height="10px" Width="170px" Text='<%#Eval("Etiquette_37") %>'
                    BorderStyle="None" ForeColor="GrayText"  BackColor="Transparent"
                    Font-Italic="true" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Smaller"
                    style="margin-top: 0px; margin-left: 5px; text-indent: 5px; text-align: left" >
                </asp:Label>
            </td>
            <td valign="top" align="left" colspan="2">
                <asp:Label ID="Don37" runat="server" Height="10px" Width="320px" Text='<%#Eval("Colonne_37") %>'
                    BorderStyle="None" ForeColor="#142425"  BackColor="Transparent"
                    Font-Italic="false" Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Smaller"
                    style="margin-top: 0px; margin-left: 2px; text-indent: 5px; text-align: left" >
                </asp:Label>
            </td>
        </tr>
         <tr id="Ligne38" runat="server" style="margin-top:1px; height:10px; padding:0px; line-height:10px">
            <td valign="top" align="left" colspan="2">
                <asp:Label ID="Eti38" runat="server" Height="10px" Width="170px" Text='<%#Eval("Etiquette_38") %>'
                    BorderStyle="None" ForeColor="GrayText"  BackColor="Transparent"
                    Font-Italic="true" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Smaller"
                    style="margin-top: 0px; margin-left: 5px; text-indent: 5px; text-align: left" >
                </asp:Label>
            </td>
            <td valign="top" align="left" colspan="2">
                <asp:Label ID="Don38" runat="server" Height="10px" Width="320px" Text='<%#Eval("Colonne_38") %>'
                    BorderStyle="None" ForeColor="#142425"  BackColor="Transparent"
                    Font-Italic="false" Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Smaller"
                    style="margin-top: 0px; margin-left: 2px; text-indent: 5px; text-align: left" >
                </asp:Label>
            </td>
        </tr>
         <tr id="Ligne39" runat="server" style="margin-top:1px; height:10px; padding:0px; line-height:10px">
            <td valign="top" align="left" colspan="2">
                <asp:Label ID="Eti39" runat="server" Height="10px" Width="170px" Text='<%#Eval("Etiquette_39") %>'
                    BorderStyle="None" ForeColor="GrayText"  BackColor="Transparent"
                    Font-Italic="true" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Smaller"
                    style="margin-top: 0px; margin-left: 5px; text-indent: 5px; text-align: left" >
                </asp:Label>
            </td>
            <td valign="top" align="left" colspan="2">
                <asp:Label ID="Don39" runat="server" Height="10px" Width="320px" Text='<%#Eval("Colonne_39") %>'
                    BorderStyle="None" ForeColor="#142425"  BackColor="Transparent"
                    Font-Italic="false" Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Smaller"
                    style="margin-top: 0px; margin-left: 2px; text-indent: 5px; text-align: left" >
                </asp:Label>
            </td>
        </tr>
         <tr id="Ligne40" runat="server" style="margin-top:1px; height:10px; padding:0px; line-height:10px">
            <td valign="top" align="left" colspan="2">
                <asp:Label ID="Eti40" runat="server" Height="10px" Width="170px" Text='<%#Eval("Etiquette_40") %>'
                    BorderStyle="None" ForeColor="GrayText"  BackColor="Transparent"
                    Font-Italic="true" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Smaller"
                    style="margin-top: 0px; margin-left: 5px; text-indent: 5px; text-align: left" >
                </asp:Label>
            </td>
            <td valign="top" align="left" colspan="2">
                <asp:Label ID="Don40" runat="server" Height="10px" Width="320px" Text='<%#Eval("Colonne_40") %>'
                    BorderStyle="None" ForeColor="#142425"  BackColor="Transparent"
                    Font-Italic="false" Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Smaller"
                    style="margin-top: 0px; margin-left: 2px; text-indent: 5px; text-align: left" >
                </asp:Label>
            </td>
        </tr>
         <tr id="Ligne41" runat="server" style="margin-top:1px; height:10px; padding:0px; line-height:10px">
            <td valign="top" align="left" colspan="2">
                <asp:Label ID="Eti41" runat="server" Height="10px" Width="170px" Text='<%#Eval("Etiquette_41") %>'
                    BorderStyle="None" ForeColor="GrayText"  BackColor="Transparent"
                    Font-Italic="true" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Smaller"
                    style="margin-top: 0px; margin-left: 5px; text-indent: 5px; text-align: left" >
                </asp:Label>
            </td>
            <td valign="top" align="left" colspan="2">
                <asp:Label ID="Don41" runat="server" Height="10px" Width="320px" Text='<%#Eval("Colonne_41") %>'
                    BorderStyle="None" ForeColor="#142425"  BackColor="Transparent"
                    Font-Italic="false" Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Smaller"
                    style="margin-top: 0px; margin-left: 2px; text-indent: 5px; text-align: left" >
                </asp:Label>
            </td>
        </tr>
        <tr id="Ligne42" runat="server" style="margin-top:1px; height:10px; padding:0px; line-height:10px">
            <td valign="top" align="left" colspan="2">
                <asp:Label ID="Eti42" runat="server" Height="10px" Width="170px" Text='<%#Eval("Etiquette_42") %>'
                    BorderStyle="None" ForeColor="GrayText"  BackColor="Transparent"
                    Font-Italic="true" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Smaller"
                    style="margin-top: 0px; margin-left: 5px; text-indent: 5px; text-align: left" >
                </asp:Label>
            </td>
            <td valign="top" align="left" colspan="2">
                <asp:Label ID="Don42" runat="server" Height="10px" Width="320px" Text='<%#Eval("Colonne_42") %>'
                    BorderStyle="None" ForeColor="#142425"  BackColor="Transparent"
                    Font-Italic="false" Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Smaller"
                    style="margin-top: 0px; margin-left: 2px; text-indent: 5px; text-align: left" >
                </asp:Label>
            </td>
        </tr>
        <tr id="Ligne43" runat="server" style="margin-top:1px; height:10px; padding:0px; line-height:10px">
            <td valign="top" align="left" colspan="2">
                <asp:Label ID="Eti43" runat="server" Height="10px" Width="170px" Text='<%#Eval("Etiquette_43") %>'
                    BorderStyle="None" ForeColor="GrayText"  BackColor="Transparent"
                    Font-Italic="true" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Smaller"
                    style="margin-top: 0px; margin-left: 5px; text-indent: 5px; text-align: left" >
                </asp:Label>
            </td>
            <td valign="top" align="left" colspan="2">
                <asp:Label ID="Don43" runat="server" Height="10px" Width="320px" Text='<%#Eval("Colonne_43") %>'
                    BorderStyle="None" ForeColor="#142425"  BackColor="Transparent"
                    Font-Italic="false" Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Smaller"
                    style="margin-top: 0px; margin-left: 2px; text-indent: 5px; text-align: left" >
                </asp:Label>
            </td>
        </tr>
        <tr id="Ligne44" runat="server" style="margin-top:1px; height:10px; padding:0px; line-height:10px">
            <td valign="top" align="left" colspan="2">
                <asp:Label ID="Eti44" runat="server" Height="10px" Width="170px" Text='<%#Eval("Etiquette_44") %>'
                    BorderStyle="None" ForeColor="GrayText"  BackColor="Transparent"
                    Font-Italic="true" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Smaller"
                    style="margin-top: 0px; margin-left: 5px; text-indent: 5px; text-align: left" >
                </asp:Label>
            </td>
            <td valign="top" align="left" colspan="2">
                <asp:Label ID="Don44" runat="server" Height="10px" Width="320px" Text='<%#Eval("Colonne_44") %>'
                    BorderStyle="None" ForeColor="#142425"  BackColor="Transparent"
                    Font-Italic="false" Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Smaller"
                    style="margin-top: 0px; margin-left: 2px; text-indent: 5px; text-align: left" >
                </asp:Label>
            </td>
        </tr>
         <tr id="Ligne45" runat="server" style="margin-top:1px; height:10px; padding:0px; line-height:10px">
            <td valign="top" align="left" colspan="2">
                <asp:Label ID="Eti45" runat="server" Height="10px" Width="170px" Text='<%#Eval("Etiquette_45") %>'
                    BorderStyle="None" ForeColor="GrayText"  BackColor="Transparent"
                    Font-Italic="true" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Smaller"
                    style="margin-top: 0px; margin-left: 5px; text-indent: 5px; text-align: left" >
                </asp:Label>
            </td>
            <td valign="top" align="left" colspan="2">
                <asp:Label ID="Don45" runat="server" Height="10px" Width="320px" Text='<%#Eval("Colonne_45") %>'
                    BorderStyle="None" ForeColor="#142425"  BackColor="Transparent"
                    Font-Italic="false" Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Smaller"
                    style="margin-top: 0px; margin-left: 2px; text-indent: 5px; text-align: left" >
                </asp:Label>
            </td>
        </tr>
         <tr id="Ligne46" runat="server" style="margin-top:1px; height:10px; padding:0px; line-height:10px">
            <td valign="top" align="left" colspan="2">
                <asp:Label ID="Eti46" runat="server" Height="10px" Width="170px" Text='<%#Eval("Etiquette_46") %>'
                    BorderStyle="None" ForeColor="GrayText"  BackColor="Transparent"
                    Font-Italic="true" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Smaller"
                    style="margin-top: 0px; margin-left: 5px; text-indent: 5px; text-align: left" >
                </asp:Label>
            </td>
            <td valign="top" align="left" colspan="2">
                <asp:Label ID="Don46" runat="server" Height="10px" Width="320px" Text='<%#Eval("Colonne_46") %>'
                    BorderStyle="None" ForeColor="#142425"  BackColor="Transparent"
                    Font-Italic="false" Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Smaller"
                    style="margin-top: 0px; margin-left: 2px; text-indent: 5px; text-align: left" >
                </asp:Label>
            </td>
        </tr>
         <tr id="Ligne47" runat="server" style="margin-top:1px; height:10px; padding:0px; line-height:10px">
            <td valign="top" align="left" colspan="2">
                <asp:Label ID="Eti47" runat="server" Height="10px" Width="170px" Text='<%#Eval("Etiquette_47") %>'
                    BorderStyle="None" ForeColor="GrayText"  BackColor="Transparent"
                    Font-Italic="true" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Smaller"
                    style="margin-top: 0px; margin-left: 5px; text-indent: 5px; text-align: left" >
                </asp:Label>
            </td>
            <td valign="top" align="left" colspan="2">
                <asp:Label ID="Don47" runat="server" Height="10px" Width="320px" Text='<%#Eval("Colonne_47") %>'
                    BorderStyle="None" ForeColor="#142425"  BackColor="Transparent"
                    Font-Italic="false" Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Smaller"
                    style="margin-top: 0px; margin-left: 2px; text-indent: 5px; text-align: left" >
                </asp:Label>
            </td>
        </tr>
         <tr id="Ligne48" runat="server" style="margin-top:1px; height:10px; padding:0px; line-height:10px">
            <td valign="top" align="left" colspan="2">
                <asp:Label ID="Eti48" runat="server" Height="10px" Width="170px" Text='<%#Eval("Etiquette_48") %>'
                    BorderStyle="None" ForeColor="GrayText"  BackColor="Transparent"
                    Font-Italic="true" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Smaller"
                    style="margin-top: 0px; margin-left: 5px; text-indent: 5px; text-align: left" >
                </asp:Label>
            </td>
            <td valign="top" align="left" colspan="2">
                <asp:Label ID="Don48" runat="server" Height="10px" Width="320px" Text='<%#Eval("Colonne_48") %>'
                    BorderStyle="None" ForeColor="#142425"  BackColor="Transparent"
                    Font-Italic="false" Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Smaller"
                    style="margin-top: 0px; margin-left: 2px; text-indent: 5px; text-align: left" >
                </asp:Label>
            </td>
        </tr>
         <tr id="Ligne49" runat="server" style="margin-top:1px; height:10px; padding:0px; line-height:10px">
            <td valign="top" align="left" colspan="2">
                <asp:Label ID="Eti49" runat="server" Height="10px" Width="170px" Text='<%#Eval("Etiquette_49") %>'
                    BorderStyle="None" ForeColor="GrayText"  BackColor="Transparent"
                    Font-Italic="true" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Smaller"
                    style="margin-top: 0px; margin-left: 5px; text-indent: 5px; text-align: left" >
                </asp:Label>
            </td>
            <td valign="top" align="left" colspan="2">
                <asp:Label ID="Don49" runat="server" Height="10px" Width="320px" Text='<%#Eval("Colonne_49") %>'
                    BorderStyle="None" ForeColor="#142425"  BackColor="Transparent"
                    Font-Italic="false" Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Smaller"
                    style="margin-top: 0px; margin-left: 2px; text-indent: 5px; text-align: left" >
                </asp:Label>
            </td>
        </tr>
         <tr id="Ligne50" runat="server" style="margin-top:1px; height:10px; padding:0px; line-height:10px">
            <td valign="top" align="left" colspan="2">
                <asp:Label ID="Eti50" runat="server" Height="10px" Width="170px" Text='<%#Eval("Etiquette_50") %>'
                    BorderStyle="None" ForeColor="GrayText"  BackColor="Transparent"
                    Font-Italic="true" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Smaller"
                    style="margin-top: 0px; margin-left: 5px; text-indent: 5px; text-align: left" >
                </asp:Label>
            </td>
            <td valign="top" align="left" colspan="2">
                <asp:Label ID="Don50" runat="server" Height="10px" Width="320px" Text='<%#Eval("Colonne_50") %>'
                    BorderStyle="None" ForeColor="#142425"  BackColor="Transparent"
                    Font-Italic="false" Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Smaller"
                    style="margin-top: 0px; margin-left: 2px; text-indent: 5px; text-align: left" >
                </asp:Label>
            </td>
        </tr>
    </ItemTemplate>
</asp:ListView>



