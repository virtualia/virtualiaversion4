﻿<%@ Control Language="VB" AutoEventWireup="false" Inherits="Virtualia.Net.Controles_VCoupleVerticalEtiquetteExperte" CodeBehind="VCoupleVerticalEtiquetteExperte.ascx.vb" ClassName="Controles_VCoupleVerticalEtiquetteExperteBase" %>

<asp:Table ID="CadreExperte" runat="server" CellPadding="1" CellSpacing="0">
    <asp:TableRow>
        <asp:TableCell>
            <asp:Label ID="Etiquette" runat="server" Height="20px" Width="170px" BackColor="#8DA8A3" BorderColor="#C0BAE5" BorderStyle="Outset" BorderWidth="2px" ForeColor="#E9FDF9" Font-Italic="true" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Style="margin-top: 0px; margin-left: 4px; text-indent: 5px; text-align: left" />
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <asp:Label ID="EtiDonnee" runat="server" Height="170px" Width="170px" BackColor="#EEECFD" BorderColor="#C0BAE5" BorderStyle="Inset" BorderWidth="2px" ForeColor="Black" Visible="true" Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Small" Font-Italic="false" Style="margin-top: 0px; margin-left: 4px; text-indent: 5px; text-align: left" />
        </asp:TableCell>
    </asp:TableRow>
</asp:Table>
