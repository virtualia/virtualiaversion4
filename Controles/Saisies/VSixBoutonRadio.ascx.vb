﻿Option Strict On
Option Explicit On
Option Compare Text

Imports Virtualia.ObjetBase
Imports Virtualia.OutilsVisu

Partial Class Controles_VSixBoutonRadio
    Inherits Controles_VSixBoutonRadioBase

    Protected Overrides ReadOnly Property V_VRadioN1 As RadioButton
        Get
            Return Me.VRadioN1
        End Get
    End Property
    Protected Overrides ReadOnly Property V_VRadioN2 As RadioButton
        Get
            Return Me.VRadioN2
        End Get
    End Property
    Protected Overrides ReadOnly Property V_VRadioN3 As RadioButton
        Get
            Return Me.VRadioN3
        End Get
    End Property
    Protected Overrides ReadOnly Property V_VRadioN4 As RadioButton
        Get
            Return Me.VRadioN4
        End Get
    End Property
    Protected Overrides ReadOnly Property V_VRadioN5 As RadioButton
        Get
            Return Me.VRadioN5
        End Get
    End Property
    Protected Overrides ReadOnly Property V_VRadioN6 As RadioButton
        Get
            Return Me.VRadioN6
        End Get
    End Property

    Private _webfonction As WebFonctions
    Protected Overrides ReadOnly Property V_WebFonction As IWebFonctions
        Get
            If (_webfonction Is Nothing) Then
                _webfonction = New WebFonctions(Me)
            End If
            Return _webfonction
        End Get
    End Property

    Protected Overrides Sub OnInit(e As System.EventArgs)
        MyBase.OnInit(e)

        AddHandler VRadioN1.CheckedChanged, AddressOf Me.RadioVirtualia_CheckedChanged
        AddHandler VRadioN2.CheckedChanged, AddressOf Me.RadioVirtualia_CheckedChanged
        AddHandler VRadioN3.CheckedChanged, AddressOf Me.RadioVirtualia_CheckedChanged
        AddHandler VRadioN4.CheckedChanged, AddressOf Me.RadioVirtualia_CheckedChanged
        AddHandler VRadioN5.CheckedChanged, AddressOf Me.RadioVirtualia_CheckedChanged
        AddHandler VRadioN6.CheckedChanged, AddressOf Me.RadioVirtualia_CheckedChanged
    End Sub


    '#Region "Delegate"
    '    Public Delegate Sub Valeur_ChangeEventHandler(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.DonneeChangeEventArgs)
    '    Public Event ValeurChange As Valeur_ChangeEventHandler
    '#End Region

    '#Region "Variables"
    '    Private WsSiEnLectureSeule As Boolean = False
    '#End Region


    '#Region "Proprietes"
    '    Public Property V_SiAutoPostBack() As Boolean
    '        Get
    '            Return VRadioN1.AutoPostBack
    '        End Get
    '        Set(ByVal value As Boolean)
    '            If V_SiEnLectureSeule = True Then
    '                value = False
    '            End If
    '            VRadioN1.AutoPostBack = value
    '            VRadioN2.AutoPostBack = value
    '            VRadioN3.AutoPostBack = value
    '            VRadioN4.AutoPostBack = value
    '            VRadioN5.AutoPostBack = value
    '            VRadioN6.AutoPostBack = value
    '        End Set
    '    End Property

    '    Public Property V_SiEnLectureSeule() As Boolean
    '        Get
    '            Return WsSiEnLectureSeule
    '        End Get
    '        Set(ByVal value As Boolean)
    '            WsSiEnLectureSeule = value
    '            If value = True Then
    '                V_SiAutoPostBack = False
    '            End If
    '        End Set
    '    End Property

    '    Public Property V_Groupe() As String
    '        Get
    '            Return VRadioN1.GroupName
    '        End Get
    '        Set(ByVal value As String)
    '            VRadioN1.GroupName = value
    '            VRadioN2.GroupName = value
    '            VRadioN3.GroupName = value
    '            VRadioN4.GroupName = value
    '            VRadioN5.GroupName = value
    '            VRadioN6.GroupName = value
    '        End Set
    '    End Property

    '    Public Property VRadioN1Text() As String
    '        Get
    '            Return VRadioN1.Text
    '        End Get
    '        Set(ByVal value As String)
    '            VRadioN1.Text = value
    '        End Set
    '    End Property

    '    Public Property VRadioN1Check() As Boolean
    '        Get
    '            Return VRadioN1.Checked
    '        End Get
    '        Set(ByVal value As Boolean)
    '            If value = True Then
    '                VRadioN1.Checked = True
    '                VRadioN2.Checked = False
    '                VRadioN3.Checked = False
    '                VRadioN4.Font.Bold = False
    '                VRadioN5.Font.Bold = False
    '                VRadioN6.Font.Bold = False
    '            End If
    '        End Set
    '    End Property

    '    Public Property VRadioN1Height() As System.Web.UI.WebControls.Unit
    '        Get
    '            Return VRadioN1.Height
    '        End Get
    '        Set(ByVal value As System.Web.UI.WebControls.Unit)
    '            VRadioN1.Height = value
    '        End Set
    '    End Property

    '    Public Property VRadioN1Width() As System.Web.UI.WebControls.Unit
    '        Get
    '            Return VRadioN1.Width
    '        End Get
    '        Set(ByVal value As System.Web.UI.WebControls.Unit)
    '            VRadioN1.Width = value
    '        End Set
    '    End Property

    '    Public Property VRadioN1BackColor() As System.Drawing.Color
    '        Get
    '            Return VRadioN1.BackColor
    '        End Get
    '        Set(ByVal value As System.Drawing.Color)
    '            VRadioN1.BackColor = value
    '        End Set
    '    End Property

    '    Public Property VRadioN1ForeColor() As System.Drawing.Color
    '        Get
    '            Return VRadioN1.ForeColor
    '        End Get
    '        Set(ByVal value As System.Drawing.Color)
    '            VRadioN1.ForeColor = value
    '        End Set
    '    End Property

    '    Public Property VRadioN1BorderColor() As System.Drawing.Color
    '        Get
    '            Return VRadioN1.BorderColor
    '        End Get
    '        Set(ByVal value As System.Drawing.Color)
    '            VRadioN1.BorderColor = value
    '        End Set
    '    End Property

    '    Public Property VRadioN1BorderWidth() As System.Web.UI.WebControls.Unit
    '        Get
    '            Return VRadioN1.BorderWidth
    '        End Get
    '        Set(ByVal value As System.Web.UI.WebControls.Unit)
    '            VRadioN1.BorderWidth = value
    '        End Set
    '    End Property

    '    Public Property VRadioN1BorderStyle() As System.Web.UI.WebControls.BorderStyle
    '        Get
    '            Return VRadioN1.BorderStyle
    '        End Get
    '        Set(ByVal value As System.Web.UI.WebControls.BorderStyle)
    '            VRadioN1.BorderStyle = value
    '        End Set
    '    End Property

    '    Public Property VRadioN1Visible() As Boolean
    '        Get
    '            Return VRadioN1.Visible
    '        End Get
    '        Set(ByVal value As Boolean)
    '            VRadioN1.Visible = value
    '        End Set
    '    End Property

    '    Public WriteOnly Property VRadioN1Style() As String
    '        Set(ByVal value As String)
    '            Dim TableauData(0) As String
    '            Dim TableauW(0) As String
    '            Dim IndiceI As Integer
    '            TableauData = Strings.Split(value, ";")
    '            For IndiceI = 0 To TableauData.Count - 1
    '                If TableauData(IndiceI) = "" Then
    '                    Exit For
    '                End If
    '                TableauW = Strings.Split(TableauData(IndiceI), ":")
    '                VRadioN1.Style.Remove(Strings.Trim(TableauW(0)))
    '                VRadioN1.Style.Add(Strings.Trim(TableauW(0)), Strings.Trim(TableauW(1)))
    '            Next IndiceI
    '        End Set
    '    End Property

    '    Public Property VRadioN2Text() As String
    '        Get
    '            Return VRadioN2.Text
    '        End Get
    '        Set(ByVal value As String)
    '            VRadioN2.Text = value
    '        End Set
    '    End Property

    '    Public Property VRadioN2Check() As Boolean
    '        Get
    '            Return VRadioN2.Checked
    '        End Get
    '        Set(ByVal value As Boolean)
    '            If value = True Then
    '                VRadioN2.Checked = True
    '                VRadioN1.Checked = False
    '                VRadioN3.Checked = False
    '                VRadioN4.Font.Bold = False
    '                VRadioN5.Font.Bold = False
    '                VRadioN6.Font.Bold = False
    '            End If
    '        End Set
    '    End Property

    '    Public Property VRadioN2Height() As System.Web.UI.WebControls.Unit
    '        Get
    '            Return VRadioN2.Height
    '        End Get
    '        Set(ByVal value As System.Web.UI.WebControls.Unit)
    '            VRadioN2.Height = value
    '        End Set
    '    End Property

    '    Public Property VRadioN2Width() As System.Web.UI.WebControls.Unit
    '        Get
    '            Return VRadioN2.Width
    '        End Get
    '        Set(ByVal value As System.Web.UI.WebControls.Unit)
    '            VRadioN2.Width = value
    '        End Set
    '    End Property

    '    Public Property VRadioN2BackColor() As System.Drawing.Color
    '        Get
    '            Return VRadioN2.BackColor
    '        End Get
    '        Set(ByVal value As System.Drawing.Color)
    '            VRadioN2.BackColor = value
    '        End Set
    '    End Property

    '    Public Property VRadioN2ForeColor() As System.Drawing.Color
    '        Get
    '            Return VRadioN2.ForeColor
    '        End Get
    '        Set(ByVal value As System.Drawing.Color)
    '            VRadioN2.ForeColor = value
    '        End Set
    '    End Property

    '    Public Property VRadioN2BorderColor() As System.Drawing.Color
    '        Get
    '            Return VRadioN2.BorderColor
    '        End Get
    '        Set(ByVal value As System.Drawing.Color)
    '            VRadioN2.BorderColor = value
    '        End Set
    '    End Property

    '    Public Property VRadioN2BorderWidth() As System.Web.UI.WebControls.Unit
    '        Get
    '            Return VRadioN2.BorderWidth
    '        End Get
    '        Set(ByVal value As System.Web.UI.WebControls.Unit)
    '            VRadioN2.BorderWidth = value
    '        End Set
    '    End Property

    '    Public Property VRadioN2BorderStyle() As System.Web.UI.WebControls.BorderStyle
    '        Get
    '            Return VRadioN2.BorderStyle
    '        End Get
    '        Set(ByVal value As System.Web.UI.WebControls.BorderStyle)
    '            VRadioN2.BorderStyle = value
    '        End Set
    '    End Property

    '    Public Property VRadioN2Visible() As Boolean
    '        Get
    '            Return VRadioN2.Visible
    '        End Get
    '        Set(ByVal value As Boolean)
    '            VRadioN2.Visible = value
    '        End Set
    '    End Property

    '    Public WriteOnly Property VRadioN2Style() As String
    '        Set(ByVal value As String)
    '            Dim TableauData(0) As String
    '            Dim TableauW(0) As String
    '            Dim IndiceI As Integer
    '            TableauData = Strings.Split(value, ";")
    '            For IndiceI = 0 To TableauData.Count - 1
    '                If TableauData(IndiceI) = "" Then
    '                    Exit For
    '                End If
    '                TableauW = Strings.Split(TableauData(IndiceI), ":")
    '                VRadioN2.Style.Remove(Strings.Trim(TableauW(0)))
    '                VRadioN2.Style.Add(Strings.Trim(TableauW(0)), Strings.Trim(TableauW(1)))
    '            Next IndiceI
    '        End Set
    '    End Property

    '    Public Property VRadioN3Text() As String
    '        Get
    '            Return VRadioN3.Text
    '        End Get
    '        Set(ByVal value As String)
    '            VRadioN3.Text = value
    '        End Set
    '    End Property

    '    Public Property VRadioN3Check() As Boolean
    '        Get
    '            Return VRadioN3.Checked
    '        End Get
    '        Set(ByVal value As Boolean)
    '            If value = True Then
    '                VRadioN3.Checked = True
    '                VRadioN1.Checked = False
    '                VRadioN2.Checked = False
    '                VRadioN4.Font.Bold = False
    '                VRadioN5.Font.Bold = False
    '                VRadioN6.Font.Bold = False
    '            End If
    '        End Set
    '    End Property

    '    Public Property VRadioN3Height() As System.Web.UI.WebControls.Unit
    '        Get
    '            Return VRadioN3.Height
    '        End Get
    '        Set(ByVal value As System.Web.UI.WebControls.Unit)
    '            VRadioN3.Height = value
    '        End Set
    '    End Property

    '    Public Property VRadioN3Width() As System.Web.UI.WebControls.Unit
    '        Get
    '            Return VRadioN3.Width
    '        End Get
    '        Set(ByVal value As System.Web.UI.WebControls.Unit)
    '            VRadioN3.Width = value
    '        End Set
    '    End Property

    '    Public Property VRadioN3BackColor() As System.Drawing.Color
    '        Get
    '            Return VRadioN3.BackColor
    '        End Get
    '        Set(ByVal value As System.Drawing.Color)
    '            VRadioN3.BackColor = value
    '        End Set
    '    End Property

    '    Public Property VRadioN3ForeColor() As System.Drawing.Color
    '        Get
    '            Return VRadioN3.ForeColor
    '        End Get
    '        Set(ByVal value As System.Drawing.Color)
    '            VRadioN3.ForeColor = value
    '        End Set
    '    End Property

    '    Public Property VRadioN3BorderColor() As System.Drawing.Color
    '        Get
    '            Return VRadioN3.BorderColor
    '        End Get
    '        Set(ByVal value As System.Drawing.Color)
    '            VRadioN3.BorderColor = value
    '        End Set
    '    End Property

    '    Public Property VRadioN3BorderWidth() As System.Web.UI.WebControls.Unit
    '        Get
    '            Return VRadioN3.BorderWidth
    '        End Get
    '        Set(ByVal value As System.Web.UI.WebControls.Unit)
    '            VRadioN3.BorderWidth = value
    '        End Set
    '    End Property

    '    Public Property VRadioN3BorderStyle() As System.Web.UI.WebControls.BorderStyle
    '        Get
    '            Return VRadioN3.BorderStyle
    '        End Get
    '        Set(ByVal value As System.Web.UI.WebControls.BorderStyle)
    '            VRadioN3.BorderStyle = value
    '        End Set
    '    End Property

    '    Public Property VRadioN3Visible() As Boolean
    '        Get
    '            Return VRadioN3.Visible
    '        End Get
    '        Set(ByVal value As Boolean)
    '            VRadioN3.Visible = value
    '        End Set
    '    End Property

    '    Public WriteOnly Property VRadioN3Style() As String
    '        Set(ByVal value As String)
    '            Dim TableauData(0) As String
    '            Dim TableauW(0) As String
    '            Dim IndiceI As Integer
    '            TableauData = Strings.Split(value, ";")
    '            For IndiceI = 0 To TableauData.Count - 1
    '                If TableauData(IndiceI) = "" Then
    '                    Exit For
    '                End If
    '                TableauW = Strings.Split(TableauData(IndiceI), ":")
    '                VRadioN3.Style.Remove(Strings.Trim(TableauW(0)))
    '                VRadioN3.Style.Add(Strings.Trim(TableauW(0)), Strings.Trim(TableauW(1)))
    '            Next IndiceI
    '        End Set
    '    End Property

    '    Public Property VRadioN4Text() As String
    '        Get
    '            Return VRadioN4.Text
    '        End Get
    '        Set(ByVal value As String)
    '            VRadioN4.Text = value
    '        End Set
    '    End Property

    '    Public Property VRadioN4Check() As Boolean
    '        Get
    '            Return VRadioN4.Checked
    '        End Get
    '        Set(ByVal value As Boolean)
    '            If value = True Then
    '                VRadioN4.Checked = True
    '                VRadioN1.Checked = False
    '                VRadioN2.Checked = False
    '                VRadioN3.Font.Bold = False
    '                VRadioN5.Font.Bold = False
    '                VRadioN6.Font.Bold = False
    '            End If
    '        End Set
    '    End Property

    '    Public Property VRadioN4Height() As System.Web.UI.WebControls.Unit
    '        Get
    '            Return VRadioN4.Height
    '        End Get
    '        Set(ByVal value As System.Web.UI.WebControls.Unit)
    '            VRadioN4.Height = value
    '        End Set
    '    End Property

    '    Public Property VRadioN4Width() As System.Web.UI.WebControls.Unit
    '        Get
    '            Return VRadioN4.Width
    '        End Get
    '        Set(ByVal value As System.Web.UI.WebControls.Unit)
    '            VRadioN4.Width = value
    '        End Set
    '    End Property

    '    Public Property VRadioN4BackColor() As System.Drawing.Color
    '        Get
    '            Return VRadioN4.BackColor
    '        End Get
    '        Set(ByVal value As System.Drawing.Color)
    '            VRadioN4.BackColor = value
    '        End Set
    '    End Property

    '    Public Property VRadioN4ForeColor() As System.Drawing.Color
    '        Get
    '            Return VRadioN4.ForeColor
    '        End Get
    '        Set(ByVal value As System.Drawing.Color)
    '            VRadioN4.ForeColor = value
    '        End Set
    '    End Property

    '    Public Property VRadioN4BorderColor() As System.Drawing.Color
    '        Get
    '            Return VRadioN4.BorderColor
    '        End Get
    '        Set(ByVal value As System.Drawing.Color)
    '            VRadioN4.BorderColor = value
    '        End Set
    '    End Property

    '    Public Property VRadioN4BorderWidth() As System.Web.UI.WebControls.Unit
    '        Get
    '            Return VRadioN4.BorderWidth
    '        End Get
    '        Set(ByVal value As System.Web.UI.WebControls.Unit)
    '            VRadioN4.BorderWidth = value
    '        End Set
    '    End Property

    '    Public Property VRadioN4BorderStyle() As System.Web.UI.WebControls.BorderStyle
    '        Get
    '            Return VRadioN4.BorderStyle
    '        End Get
    '        Set(ByVal value As System.Web.UI.WebControls.BorderStyle)
    '            VRadioN4.BorderStyle = value
    '        End Set
    '    End Property

    '    Public Property VRadioN4Visible() As Boolean
    '        Get
    '            Return VRadioN4.Visible
    '        End Get
    '        Set(ByVal value As Boolean)
    '            VRadioN4.Visible = value
    '        End Set
    '    End Property

    '    Public WriteOnly Property VRadioN4Style() As String
    '        Set(ByVal value As String)
    '            Dim TableauData(0) As String
    '            Dim TableauW(0) As String
    '            Dim IndiceI As Integer
    '            TableauData = Strings.Split(value, ";")
    '            For IndiceI = 0 To TableauData.Count - 1
    '                If TableauData(IndiceI) = "" Then
    '                    Exit For
    '                End If
    '                TableauW = Strings.Split(TableauData(IndiceI), ":")
    '                VRadioN4.Style.Remove(Strings.Trim(TableauW(0)))
    '                VRadioN4.Style.Add(Strings.Trim(TableauW(0)), Strings.Trim(TableauW(1)))
    '            Next IndiceI
    '        End Set
    '    End Property

    '    Public Property VRadioN5Text() As String
    '        Get
    '            Return VRadioN5.Text
    '        End Get
    '        Set(ByVal value As String)
    '            VRadioN5.Text = value
    '        End Set
    '    End Property

    '    Public Property VRadioN5Check() As Boolean
    '        Get
    '            Return VRadioN5.Checked
    '        End Get
    '        Set(ByVal value As Boolean)
    '            If value = True Then
    '                VRadioN5.Checked = True
    '                VRadioN1.Checked = False
    '                VRadioN2.Checked = False
    '                VRadioN3.Font.Bold = False
    '                VRadioN4.Font.Bold = False
    '                VRadioN6.Font.Bold = False
    '            End If
    '        End Set
    '    End Property

    '    Public Property VRadioN5Height() As System.Web.UI.WebControls.Unit
    '        Get
    '            Return VRadioN5.Height
    '        End Get
    '        Set(ByVal value As System.Web.UI.WebControls.Unit)
    '            VRadioN5.Height = value
    '        End Set
    '    End Property

    '    Public Property VRadioN5Width() As System.Web.UI.WebControls.Unit
    '        Get
    '            Return VRadioN5.Width
    '        End Get
    '        Set(ByVal value As System.Web.UI.WebControls.Unit)
    '            VRadioN5.Width = value
    '        End Set
    '    End Property

    '    Public Property VRadioN5BackColor() As System.Drawing.Color
    '        Get
    '            Return VRadioN5.BackColor
    '        End Get
    '        Set(ByVal value As System.Drawing.Color)
    '            VRadioN5.BackColor = value
    '        End Set
    '    End Property

    '    Public Property VRadioN5ForeColor() As System.Drawing.Color
    '        Get
    '            Return VRadioN5.ForeColor
    '        End Get
    '        Set(ByVal value As System.Drawing.Color)
    '            VRadioN5.ForeColor = value
    '        End Set
    '    End Property

    '    Public Property VRadioN5BorderColor() As System.Drawing.Color
    '        Get
    '            Return VRadioN5.BorderColor
    '        End Get
    '        Set(ByVal value As System.Drawing.Color)
    '            VRadioN5.BorderColor = value
    '        End Set
    '    End Property

    '    Public Property VRadioN5BorderWidth() As System.Web.UI.WebControls.Unit
    '        Get
    '            Return VRadioN5.BorderWidth
    '        End Get
    '        Set(ByVal value As System.Web.UI.WebControls.Unit)
    '            VRadioN5.BorderWidth = value
    '        End Set
    '    End Property

    '    Public Property VRadioN5BorderStyle() As System.Web.UI.WebControls.BorderStyle
    '        Get
    '            Return VRadioN5.BorderStyle
    '        End Get
    '        Set(ByVal value As System.Web.UI.WebControls.BorderStyle)
    '            VRadioN5.BorderStyle = value
    '        End Set
    '    End Property

    '    Public Property VRadioN5Visible() As Boolean
    '        Get
    '            Return VRadioN5.Visible
    '        End Get
    '        Set(ByVal value As Boolean)
    '            VRadioN5.Visible = value
    '        End Set
    '    End Property

    '    Public WriteOnly Property VRadioN5Style() As String
    '        Set(ByVal value As String)
    '            Dim TableauData(0) As String
    '            Dim TableauW(0) As String
    '            Dim IndiceI As Integer
    '            TableauData = Strings.Split(value, ";")
    '            For IndiceI = 0 To TableauData.Count - 1
    '                If TableauData(IndiceI) = "" Then
    '                    Exit For
    '                End If
    '                TableauW = Strings.Split(TableauData(IndiceI), ":")
    '                VRadioN5.Style.Remove(Strings.Trim(TableauW(0)))
    '                VRadioN5.Style.Add(Strings.Trim(TableauW(0)), Strings.Trim(TableauW(1)))
    '            Next IndiceI
    '        End Set
    '    End Property

    '    Public Property VRadioN6Text() As String
    '        Get
    '            Return VRadioN6.Text
    '        End Get
    '        Set(ByVal value As String)
    '            VRadioN6.Text = value
    '        End Set
    '    End Property

    '    Public Property VRadioN6Check() As Boolean
    '        Get
    '            Return VRadioN6.Checked
    '        End Get
    '        Set(ByVal value As Boolean)
    '            If value = True Then
    '                VRadioN6.Checked = True
    '                VRadioN1.Checked = False
    '                VRadioN2.Checked = False
    '                VRadioN3.Font.Bold = False
    '                VRadioN4.Font.Bold = False
    '                VRadioN5.Font.Bold = False
    '            End If
    '        End Set
    '    End Property

    '    Public Property VRadioN6Height() As System.Web.UI.WebControls.Unit
    '        Get
    '            Return VRadioN6.Height
    '        End Get
    '        Set(ByVal value As System.Web.UI.WebControls.Unit)
    '            VRadioN6.Height = value
    '        End Set
    '    End Property

    '    Public Property VRadioN6Width() As System.Web.UI.WebControls.Unit
    '        Get
    '            Return VRadioN6.Width
    '        End Get
    '        Set(ByVal value As System.Web.UI.WebControls.Unit)
    '            VRadioN6.Width = value
    '        End Set
    '    End Property

    '    Public Property VRadioN6BackColor() As System.Drawing.Color
    '        Get
    '            Return VRadioN6.BackColor
    '        End Get
    '        Set(ByVal value As System.Drawing.Color)
    '            VRadioN6.BackColor = value
    '        End Set
    '    End Property

    '    Public Property VRadioN6ForeColor() As System.Drawing.Color
    '        Get
    '            Return VRadioN6.ForeColor
    '        End Get
    '        Set(ByVal value As System.Drawing.Color)
    '            VRadioN6.ForeColor = value
    '        End Set
    '    End Property

    '    Public Property VRadioN6BorderColor() As System.Drawing.Color
    '        Get
    '            Return VRadioN6.BorderColor
    '        End Get
    '        Set(ByVal value As System.Drawing.Color)
    '            VRadioN6.BorderColor = value
    '        End Set
    '    End Property

    '    Public Property VRadioN6BorderWidth() As System.Web.UI.WebControls.Unit
    '        Get
    '            Return VRadioN6.BorderWidth
    '        End Get
    '        Set(ByVal value As System.Web.UI.WebControls.Unit)
    '            VRadioN6.BorderWidth = value
    '        End Set
    '    End Property

    '    Public Property VRadioN6BorderStyle() As System.Web.UI.WebControls.BorderStyle
    '        Get
    '            Return VRadioN6.BorderStyle
    '        End Get
    '        Set(ByVal value As System.Web.UI.WebControls.BorderStyle)
    '            VRadioN6.BorderStyle = value
    '        End Set
    '    End Property

    '    Public Property VRadioN6Visible() As Boolean
    '        Get
    '            Return VRadioN6.Visible
    '        End Get
    '        Set(ByVal value As Boolean)
    '            VRadioN6.Visible = value
    '        End Set
    '    End Property

    '    Public WriteOnly Property VRadioN6Style() As String
    '        Set(ByVal value As String)
    '            Dim TableauData(0) As String
    '            Dim TableauW(0) As String
    '            Dim IndiceI As Integer
    '            TableauData = Strings.Split(value, ";")
    '            For IndiceI = 0 To TableauData.Count - 1
    '                If TableauData(IndiceI) = "" Then
    '                    Exit For
    '                End If
    '                TableauW = Strings.Split(TableauData(IndiceI), ":")
    '                VRadioN6.Style.Remove(Strings.Trim(TableauW(0)))
    '                VRadioN6.Style.Add(Strings.Trim(TableauW(0)), Strings.Trim(TableauW(1)))
    '            Next IndiceI
    '        End Set
    '    End Property
    '#End Region

    '#Region "Evenements"
    '    Protected Overridable Sub Saisie_Change(ByVal e As Virtualia.Systeme.Evenements.DonneeChangeEventArgs)
    '        RaiseEvent ValeurChange(Me, e)
    '    End Sub

    '    Protected Sub RadioVirtualia_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles VRadioN1.CheckedChanged, _
    '    VRadioN2.CheckedChanged, VRadioN3.CheckedChanged, VRadioN4.CheckedChanged, VRadioN5.CheckedChanged, VRadioN6.CheckedChanged
    '        If V_SiEnLectureSeule = True Then
    '            Exit Sub
    '        End If
    '        Dim Valeur As String
    '        Dim ValIndex As Integer
    '        Dim Evenement As Virtualia.Systeme.Evenements.DonneeChangeEventArgs

    '        VRadioN1.Font.Bold = False
    '        VRadioN2.Font.Bold = False
    '        VRadioN3.Font.Bold = False
    '        VRadioN4.Font.Bold = False
    '        VRadioN5.Font.Bold = False
    '        VRadioN6.Font.Bold = False
    '        CType(sender, System.Web.UI.WebControls.RadioButton).Font.Bold = True

    '        Valeur = CType(sender, System.Web.UI.WebControls.RadioButton).Text
    '        If Valeur = "" Then
    '            ValIndex = CInt(Strings.Right(CType(sender, System.Web.UI.WebControls.RadioButton).ID, 1))
    '            If ValIndex > 5 Then
    '                ValIndex = 0
    '            End If
    '            Evenement = New Virtualia.Systeme.Evenements.DonneeChangeEventArgs(ValIndex.ToString)
    '        Else
    '            Evenement = New Virtualia.Systeme.Evenements.DonneeChangeEventArgs(Valeur)
    '        End If
    '        Saisie_Change(Evenement)
    '    End Sub

    '    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
    '        If V_SiDonneeDico = True Then
    '            Select Case VRadioN1.Checked
    '                Case True
    '                    VRadioN1.BackColor = V_InverseBackColor
    '                Case False
    '                    VRadioN1.BackColor = V_EtiBackcolor
    '            End Select
    '            VRadioN1.ForeColor = V_EtiForecolor
    '            VRadioN1.BorderColor = V_EtiBordercolor
    '            Select Case VRadioN2.Checked
    '                Case True
    '                    VRadioN2.BackColor = V_InverseBackColor
    '                Case False
    '                    VRadioN2.BackColor = V_EtiBackcolor
    '            End Select
    '            VRadioN2.ForeColor = V_EtiForecolor
    '            VRadioN2.BorderColor = V_EtiBordercolor
    '            Select Case VRadioN3.Checked
    '                Case True
    '                    VRadioN3.BackColor = V_InverseBackColor
    '                Case False
    '                    VRadioN3.BackColor = V_EtiBackcolor
    '            End Select
    '            VRadioN3.ForeColor = V_EtiForecolor
    '            VRadioN3.BorderColor = V_EtiBordercolor
    '            Select Case VRadioN4.Checked
    '                Case True
    '                    VRadioN4.BackColor = V_InverseBackColor
    '                Case False
    '                    VRadioN4.BackColor = V_EtiBackcolor
    '            End Select
    '            VRadioN4.ForeColor = V_EtiForecolor
    '            VRadioN4.BorderColor = V_EtiBordercolor
    '            Select Case VRadioN5.Checked
    '                Case True
    '                    VRadioN5.BackColor = V_InverseBackColor
    '                Case False
    '                    VRadioN5.BackColor = V_EtiBackcolor
    '            End Select
    '            VRadioN5.ForeColor = V_EtiForecolor
    '            VRadioN5.BorderColor = V_EtiBordercolor
    '            Select Case VRadioN6.Checked
    '                Case True
    '                    VRadioN6.BackColor = V_InverseBackColor
    '                Case False
    '                    VRadioN6.BackColor = V_EtiBackcolor
    '            End Select
    '            VRadioN6.ForeColor = V_EtiForecolor
    '            VRadioN6.BorderColor = V_EtiBordercolor

    '            VRadioN1.Font.Name = V_FontName
    '            VRadioN1.Font.Size = V_FontTaille
    '            VRadioN1.Font.Italic = V_FontItalic
    '            VRadioN2.Font.Name = V_FontName
    '            VRadioN2.Font.Size = V_FontTaille
    '            VRadioN2.Font.Italic = V_FontItalic
    '            VRadioN3.Font.Name = V_FontName
    '            VRadioN3.Font.Size = V_FontTaille
    '            VRadioN3.Font.Italic = V_FontItalic
    '            VRadioN4.Font.Name = V_FontName
    '            VRadioN4.Font.Size = V_FontTaille
    '            VRadioN4.Font.Italic = V_FontItalic
    '            VRadioN5.Font.Name = V_FontName
    '            VRadioN5.Font.Size = V_FontTaille
    '            VRadioN5.Font.Italic = V_FontItalic
    '            VRadioN6.Font.Name = V_FontName
    '            VRadioN6.Font.Size = V_FontTaille
    '            VRadioN6.Font.Italic = V_FontItalic
    '        End If
    '    End Sub
    '#End Region

End Class
