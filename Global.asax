﻿<%@ Application Language="VB" %>
<script runat="server">
    Private VirObjetGlobal As Virtualia.Net.Session.ObjetGlobal
    
    Sub Application_Start(ByVal sender As Object, ByVal e As EventArgs)
        ' Code qui s’exécute au démarrage de l’application
        Dim RhOrdi As Virtualia.Systeme.Outils.Ordinateur
        Dim Proxy As Virtualia.Ressources.WebService.Serveur
        Dim NomMachine As String
        Dim AdresseIP As String
        Dim Pw As String
        Dim NoBd As Short
        Dim TableauData(0) As String
        
        RhOrdi = New Virtualia.Systeme.Outils.Ordinateur
        NomMachine = RhOrdi.NomdelOrdinateur
        AdresseIP = RhOrdi.AdresseIPOrdinateur
        Pw = System.Configuration.ConfigurationManager.AppSettings("Password")
        NoBd = CShort(System.Configuration.ConfigurationManager.AppSettings("NumeroDatabase"))

        Proxy = New Virtualia.Ressources.WebService.Serveur(System.Configuration.ConfigurationManager.AppSettings("WebService.VirtualiaServeur"))
        Try
            Select Case Proxy.OuvertureSession("Virtualia", Pw, NomMachine, AdresseIP, NoBd)
                Case Virtualia.Systeme.Constantes.CnxUtilisateur.CnxOK, Virtualia.Systeme.Constantes.CnxUtilisateur.CnxDejaConnecte, Virtualia.Systeme.Constantes.CnxUtilisateur.CnxReprise, Virtualia.Systeme.Constantes.CnxUtilisateur.CnxVirtualia
                    Application.Add("Database", NoBd)
                Case Else
                    Call EcrireLog("---- Erreur à la connexion au ServiceServeur ---" & System.Configuration.ConfigurationManager.AppSettings("WebService.VirtualiaServeur"))
                    Me.Dispose()
                    Exit Sub
            End Select
        Catch ex As Exception
            Call EcrireLog("---- Erreur ServiceServeur ne répond pas ---" & System.Configuration.ConfigurationManager.AppSettings("WebService.VirtualiaServeur") & Strings.Space(1) & ex.Message)
            Me.Dispose()
            Exit Sub
        End Try
        
        'Try
        VirObjetGlobal = New Virtualia.Net.Session.ObjetGlobal("Virtualia")
        Application.Add("VirGlobales", VirObjetGlobal)
        'Catch ex As Exception
        'Call EcrireLog("---- Erreur au Chargement de l'application ---" & Strings.Space(1) & ex.Message)
        'Me.Dispose()
        'Exit Sub
        'End Try
        
    End Sub
    
    Sub Application_End(ByVal sender As Object, ByVal e As EventArgs)
        ' Code qui s’exécute à l’arrêt de l’application
        Dim IndiceM As Integer
        Dim Init As Integer = 0
        For IndiceM = 0 To Application.Count - 1
            Try
                Select Case Application.Keys.Item(IndiceM)
                    Case Is = "VirGlobales"
                        CType(Application.Item(IndiceM), Virtualia.Net.Session.ObjetGlobal).Dispose()
                End Select
            Catch Ex As Exception
                Exit Sub
            End Try
        Next IndiceM
    End Sub
        
    Sub Application_Error(ByVal sender As Object, ByVal e As EventArgs)
        ' Code qui s’exécute lorsqu’une erreur non gérée se produit
    End Sub

    Sub Session_Start(ByVal sender As Object, ByVal e As EventArgs)
        ' Code qui s’exécute lorsqu’une nouvelle session démarre
        'Session.SessionID
    End Sub

    Sub Session_End(ByVal sender As Object, ByVal e As EventArgs)
        ' Code qui s’exécute lorsqu’une session se termine. 
        ' Remarque : l’événement Session_End est déclenché uniquement lorsque le mode sessionstate
        ' a la valeur InProc dans le fichier Web.config. Si le mode de session a la valeur StateServer 
        ' ou SQLServer, l’événement n’est pas déclenché.
    End Sub
      
    Private Sub EcrireLog(ByVal Msg As String)
        Dim FicStream As System.IO.FileStream
        Dim FicWriter As System.IO.StreamWriter
        Dim NomLog As String
        Dim NomRep As String
        Dim SysFicLog As String = "WebVersion4.log"
        Dim SysCodeIso As System.Text.Encoding = System.Text.Encoding.GetEncoding(1252)
       
        NomRep = System.Configuration.ConfigurationManager.AppSettings("RepertoireVirtualia")
        NomLog = Virtualia.Systeme.Constantes.DossierVirtualiaService(NomRep, "Logs") & SysFicLog
        FicStream = New System.IO.FileStream(NomLog, IO.FileMode.Append, IO.FileAccess.Write)
        FicWriter = New System.IO.StreamWriter(FicStream, SysCodeIso)
        FicWriter.WriteLine(Format(System.DateTime.Now, "g") & Space(1) & Msg)
        FicWriter.Flush()
        FicWriter.Close()
        FicStream.Close()
    End Sub
    
</script>