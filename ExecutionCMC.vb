﻿Option Strict Off
Option Explicit On
Option Compare Text
Imports Microsoft.VisualBasic
Imports VI = Virtualia.Systeme.Constantes
Namespace Script
    Public Class ExecutionCMC
        Implements IDisposable
        Private disposedValue As Boolean = False        ' Pour détecter les appels redondants
        Private WsPointdeVue As Short
        Private AppObjetUser As Virtualia.Net.Session.ObjetSession
        '
        Private WsSiArmoire As Boolean
        Private WsLstDossier As List(Of Virtualia.Ressources.Datas.ObjetDossierPER)
        Private WsTabResultat As ArrayList
        '
        Private WsTabIde() As Integer 'Préselection d'identifiants

        Public ReadOnly Property Resultat(ByVal Index As Integer) As String
            Get
                If Index > WsTabResultat.Count - 1 Then
                    Return ""
                End If
                Return WsTabResultat(Index).ToString
            End Get
        End Property

        Public ReadOnly Property ListeArmoire() As List(Of Virtualia.Ressources.Datas.ObjetDossierPER)
            Get
                Return WsLstDossier
            End Get
        End Property

        Public WriteOnly Property TableIdentifiants() As Integer()
            Set(ByVal value As Integer())
                WsTabIde = value
            End Set
        End Property

        Public Function Executer(ByVal VConditions As ArrayList, ByVal VScript As ArrayList) As Integer
            If VConditions Is Nothing Then
                Return 0
            End If
            If VScript Is Nothing Then
                Return 0
            End If
            Dim I As Integer = 0
            Dim K As Integer = 0
            Dim LigneScript As ArrayList
            Dim ConditionScript As ArrayList
            Dim Pvue As Short
            Dim NumObjet As Short
            Dim NumInfo As Short
            Dim DateDebut As String
            Dim DateFin As String
            Dim OpeComparaison As Short
            Dim OpeInclu As Short
            Dim OpeETOU As Short
            Dim ChaineValeur As System.Text.StringBuilder
            Dim Requetes As ArrayList
            Dim RequeteDyna As String
            Dim Requeteur As Virtualia.Net.Datas.ObjetSelection
            Dim SiPresenceExperte As Boolean = False

            Requeteur = AppObjetUser.PointeurResultatCMC(0)
            Requeteur = AppObjetUser.PointeurResultatCMC(WsPointdeVue)

            ConditionScript = CType(VConditions, ArrayList)
            OpeETOU = CShort(ConditionScript(0))
            DateDebut = ConditionScript(1).ToString
            DateFin = ConditionScript(2).ToString

            '** Traitement des requêtes non expertes ***************************************
            Requetes = New ArrayList
            Do
                LigneScript = CType(VScript(I), ArrayList)
                If LigneScript Is Nothing Then
                    Exit Do
                End If
                Try
                    ChaineValeur = New System.Text.StringBuilder
                    Pvue = CShort(LigneScript(0))
                    NumObjet = CShort(LigneScript(1))
                    NumInfo = CShort(LigneScript(2))
                    If NumInfo < 500 Then
                        OpeComparaison = CShort(LigneScript(3))
                        OpeInclu = CShort(LigneScript(4))
                        If OpeInclu = VI.Operateurs.Inclu Then
                            Select Case OpeComparaison
                                Case VI.Operateurs.Egalite
                                    OpeComparaison = VI.Operateurs.Inclu
                                Case Else
                                    OpeComparaison = VI.Operateurs.Exclu
                            End Select
                        End If
                        For K = 5 To LigneScript.Count - 1
                            ChaineValeur.Append(LigneScript(K).ToString & VI.PointVirgule)
                        Next K
                        Requetes.Add(Requeteur.RequeteDynaSql(NumObjet, NumInfo, _
                                    OpeETOU, OpeComparaison, ChaineValeur.ToString, DateDebut, DateFin, False))
                    Else
                        SiPresenceExperte = True
                    End If
                Catch ex As Exception
                    Exit Try
                End Try
                I += 1
                If I > VScript.Count - 1 Then
                    Exit Do
                End If
            Loop

            RequeteDyna = AppObjetUser.PointeurResultatCMC(Pvue).RequetesFusionnees(Requetes, OpeETOU)
            K = Requeteur.SelectionDynamique(NumObjet, RequeteDyna, WsTabIde)

            '** Expertes *******************************************
            If K > 0 And SiPresenceExperte = True Then
                Dim TabIdeRes() As Integer = AppObjetUser.PointeurResultatCMC(Pvue).ListeIdentifiants
                Dim TypeExperte As Short

                I = 0
                Do
                    LigneScript = CType(VScript(I), ArrayList)
                    If LigneScript Is Nothing Then
                        Exit Do
                    End If
                    Try
                        ChaineValeur = New System.Text.StringBuilder
                        Pvue = CShort(LigneScript(0))
                        NumObjet = CShort(LigneScript(1))
                        NumInfo = CShort(LigneScript(2))
                        If NumInfo > 500 Then
                            TypeExperte = PointeurDicoExperte(Pvue, NumObjet, NumInfo).CategorieDLL
                            OpeComparaison = CShort(LigneScript(3))
                            OpeInclu = CShort(LigneScript(4))
                            If OpeInclu = VI.Operateurs.Inclu Then
                                Select Case OpeComparaison
                                    Case VI.Operateurs.Egalite
                                        OpeComparaison = VI.Operateurs.Inclu
                                    Case Else
                                        OpeComparaison = VI.Operateurs.Exclu
                                End Select
                            End If
                            For K = 5 To LigneScript.Count - 1
                                ChaineValeur.Append(LigneScript(K).ToString & VI.PointVirgule)
                            Next K

                            Select Case TypeExperte
                                Case 0 'Standard Expertes - Dll Expert
                                    Requeteur = AppObjetUser.PointeurNewCMCExperte(PointeurDicoExperte(Pvue, NumObjet, NumInfo), OpeComparaison, ChaineValeur.ToString)
                                    K = Requeteur.RequeteExperte(NumObjet, NumInfo, TabIdeRes, _
                                                     DateDebut, DateFin)
                                    If K = 0 Then
                                        Exit Do
                                    End If
                                    TabIdeRes = AppObjetUser.PointeurResultatCMC(Pvue).ListeIdentifiants
                            End Select

                        End If
                    Catch ex As Exception
                        Exit Try
                    End Try
                    I += 1
                    If I > VScript.Count - 1 Then
                        Exit Do
                    End If
                Loop

            End If
            '*************************************************************************************************
            If K = 0 Then
                Return 0
            End If
            Call TraiterResultat()
            If WsSiArmoire = True Then
                Return WsLstDossier.Count
            Else
                Return WsTabResultat.Count
            End If
        End Function

        Private Sub TraiterResultat()
            Dim Requeteur As Virtualia.Net.Datas.ObjetSelection
            Dim K As Integer
            Dim PDossier As Virtualia.Ressources.Datas.ObjetDossierPER
            Dim Cpt As Integer = 0
            Dim TabIdeAbsent As ArrayList
            Dim ArmoireComplete As Virtualia.Net.Datas.ObjetArmoire
            Dim PointeurClone As Virtualia.Ressources.Datas.ObjetDossierPER
            Dim ListeW As List(Of Virtualia.Ressources.Datas.ObjetDossierPER) = Nothing

            If WsTabResultat IsNot Nothing Then
                WsTabResultat.Clear()
                WsTabResultat = Nothing
            End If
            WsTabResultat = New ArrayList

            ArmoireComplete = AppObjetUser.ItemArmoire(0)
            If WsSiArmoire = True Then
                ListeW = New List(Of Virtualia.Ressources.Datas.ObjetDossierPER)
            End If

            Requeteur = AppObjetUser.PointeurResultatCMC(VI.PointdeVue.PVueApplicatif)

            TabIdeAbsent = New ArrayList
            For K = 0 To Requeteur.ListeResultat.Count - 1
                PDossier = ArmoireComplete.EnsembleDossiers.Find(Function(Recherche) Recherche.V_Identifiant = Requeteur.ListeResultat.Item(K).Identifiant)
                If PDossier IsNot Nothing Then
                    If WsSiArmoire = True Then
                        PointeurClone = New Virtualia.Ressources.Datas.ObjetDossierPER(AppObjetUser.VParent.VirUrlWebServeur, _
                                            AppObjetUser.VParent.VirModele)
                        PointeurClone = PDossier
                        ListeW.Add(PointeurClone)
                    Else
                        Requeteur.ListeResultat.Item(K).CritereTri = PDossier.Nom & Strings.Space(1) & PDossier.Prenom
                    End If
                Else
                    TabIdeAbsent.Add(Requeteur.ListeResultat.Item(K))
                End If
            Next K

            If WsSiArmoire = True Then
                WsLstDossier = New List(Of Virtualia.Ressources.Datas.ObjetDossierPER)
                WsLstDossier = (From instance In ListeW Select instance Where instance.V_Identifiant > 0 Order By instance.Nom Ascending, instance.Prenom Ascending).ToList
                Exit Sub
            End If

            For K = 0 To TabIdeAbsent.Count - 1
                Requeteur.ListeResultat.Remove(CType(TabIdeAbsent(K), Virtualia.Ressources.Datas.ItemSelection))
            Next K

            Dim Restrie As List(Of Virtualia.Ressources.Datas.ItemSelection)
            Dim FicheRes As Virtualia.Ressources.Datas.ItemSelection

            Restrie = (From instance In Requeteur.ListeResultat Select instance Where instance.Identifiant > 0 Order By instance.CritereTri Ascending).ToList
            If Restrie.Count = 0 Then
                Exit Sub
            End If
            Dim IndiceC As IEnumerator = Restrie.GetEnumerator
            While IndiceC.MoveNext
                FicheRes = CType(IndiceC.Current, Virtualia.Ressources.Datas.ItemSelection)
                If FicheRes.CritereTri <> "" Then
                    WsTabResultat.Add(FicheRes.CritereTri)
                End If
                Cpt += 1
            End While
        End Sub

        Private ReadOnly Property PointeurDicoExperte(ByVal PointdeVue As Short, ByVal Numobjet As Short, ByVal NumInfo As Short) As Virtualia.Systeme.MetaModele.Expertes.InformationExperte
            Get
                If PointdeVue = 0 Or Numobjet = 0 Then
                    Return Nothing
                    Exit Property
                End If
                Dim Predicat As New Virtualia.Systeme.MetaModele.Predicats.PredicateDictionnaire(PointdeVue, Numobjet, NumInfo)
                Dim Dico As List(Of Virtualia.Systeme.MetaModele.Outils.FicheInfoExperte)
                Dico = AppObjetUser.VParent.VirListeExpertesDico.FindAll(AddressOf Predicat.InformationParIdeExperte)
                If Dico.Count = 0 Then
                    Return Nothing
                    Exit Property
                End If
                Return Dico.Item(0).PointeurModele
            End Get
        End Property

        Public Sub New(ByVal InstanceUtilisateur As Virtualia.Net.Session.ObjetSession, ByVal PointdeVue As Short, ByVal SiArmoire As Boolean)
            MyBase.New()
            AppObjetUser = InstanceUtilisateur
            WsPointdeVue = PointdeVue
            WsSiArmoire = SiArmoire
        End Sub

        Public Overloads Sub Dispose() Implements IDisposable.Dispose
            Dispose(True)
            GC.SuppressFinalize(Me)
        End Sub

        Protected Overridable Overloads Sub Dispose(ByVal disposing As Boolean)
            If Not Me.disposedValue Then
                If disposing Then
                    If WsTabResultat IsNot Nothing Then
                        WsTabResultat.Clear()
                        WsTabResultat = Nothing
                    End If
                End If
            End If
            Me.disposedValue = True
        End Sub
    End Class
End Namespace
