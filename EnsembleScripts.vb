﻿Option Strict On
Option Explicit On
Option Compare Text
Imports Microsoft.VisualBasic
Imports VI = Virtualia.Systeme.Constantes
Namespace Script
    Public Class EnsembleScripts
        Implements IDisposable
        Private disposedValue As Boolean = False        ' Pour détecter les appels redondants
        Private WsListeScripts As List(Of Virtualia.Net.Script.ObjetScript)
        Private WsPointdeVueAppli As Short
        Private WsPointdeVueOutil As Short
        Private AppObjetUser As Virtualia.Net.Session.ObjetSession

        Public ReadOnly Property ListeDesScripts() As List(Of Virtualia.Net.Script.ObjetScript)
            Get
                Return WsListeScripts
            End Get
        End Property

        Public ReadOnly Property PointdeVueAppli() As Short
            Get
                Return WsPointdeVueAppli
            End Get
        End Property

        Public ReadOnly Property PointdeVueOutil() As Short
            Get
                Return WsPointdeVueOutil
            End Get
        End Property

        Public ReadOnly Property NombredeDossiers() As Integer
            Get
                Return WsListeScripts.Count
            End Get
        End Property

        Public ReadOnly Property PointeurUtilisateur() As Virtualia.Net.Session.ObjetSession
            Get
                Return AppObjetUser
            End Get
        End Property

        Private Sub ConstituerListe(ByVal PtdeVue As Short)
            Dim Proxy As Virtualia.Ressources.WebService.Serveur
            Dim Constructeur As Virtualia.Systeme.Sgbd.Sql.SqlInterne
            Dim ChaineLue As System.Text.StringBuilder
            Dim OrdreSql As String
            Dim ChainePage As String
            Dim NoPage As Integer = 1
            Dim TableauObjet(0) As String
            Dim TableauData(0) As String
            Dim Ide As Integer

            Constructeur = New Virtualia.Systeme.Sgbd.Sql.SqlInterne(AppObjetUser.VParent.VirModele, AppObjetUser.InstanceBd)
            Constructeur.NombredeRequetes(PtdeVue, "", "", VI.Operateurs.ET) = 2
            Constructeur.SiPasdeTriSurIdeDossier = True
            Constructeur.NoInfoSelection(0, 1) = 1
            Constructeur.ValeuraComparer(0, VI.Operateurs.OU, VI.Operateurs.Egalite, False) = WsPointdeVueAppli.ToString
            Constructeur.NoInfoSelection(1, 1) = 3
            Select Case AppObjetUser.V_NomdUtilisateurSgbd
                Case Is = "Virtualia"
                    Constructeur.ValeuraComparer(1, VI.Operateurs.OU, VI.Operateurs.Egalite, False) = "Virtualia" & VI.PointVirgule & "VStandard"
                Case Else
                    Constructeur.ValeuraComparer(1, VI.Operateurs.OU, VI.Operateurs.Egalite, False) = "VStandard" & VI.PointVirgule & AppObjetUser.V_NomdUtilisateurSgbd
            End Select
            Constructeur.InfoExtraite(0, 1, 0) = 1
            Constructeur.InfoExtraite(1, 1, 2) = 2
            Constructeur.InfoExtraite(2, 1, 0) = 3
            Select Case PtdeVue 'Catégorie RH
                Case VI.PointdeVue.PVueScriptCmc
                    Constructeur.InfoExtraite(3, 1, 1) = 8
                Case VI.PointdeVue.PVueScriptExport
                    Constructeur.InfoExtraite(3, 1, 1) = 14
                Case VI.PointdeVue.PVueScriptEdition
                    Constructeur.InfoExtraite(3, 1, 1) = 24
            End Select

            OrdreSql = Constructeur.OrdreSqlDynamique

            Proxy = New Virtualia.Ressources.WebService.Serveur(AppObjetUser.VParent.VirUrlWebServeur)

            ChaineLue = New System.Text.StringBuilder
            Do
                ChainePage = Proxy.SelectionSql(AppObjetUser.V_NomdUtilisateurSgbd, PtdeVue, 1, OrdreSql, NoPage)
                If ChainePage = "" Then
                    Exit Do
                End If
                ChaineLue.Append(ChainePage)
                NoPage += 1
            Loop
            Proxy.Dispose()

            TableauObjet = Strings.Split(ChaineLue.ToString, VI.SigneBarre, -1)

            Dim FicheRef As Virtualia.Net.Script.ObjetScript
            For NoPage = 0 To TableauObjet.Count - 1
                If TableauObjet(NoPage) = "" Then
                    Exit For
                End If
                TableauData = Strings.Split(TableauObjet(NoPage), VI.Tild, -1)
                If IsNumeric(TableauData(0)) Then
                    Ide = CInt(TableauData(0))
                    Select Case PtdeVue 'Fusion Edition-Export
                        Case VI.PointdeVue.PVueScriptEdition
                            Ide += 100000
                    End Select
                    FicheRef = New Virtualia.Net.Script.ObjetScript(Me, Ide)
                    WsListeScripts.Add(FicheRef)
                End If
            Next NoPage
        End Sub

        Public ReadOnly Property PointeurDicoObjet(ByVal PointdeVue As Short, ByVal Numobjet As Short) As Virtualia.Systeme.MetaModele.Donnees.Objet
            Get
                If PointdeVue = 0 Or Numobjet = 0 Then
                    Return Nothing
                    Exit Property
                End If
                Dim Predicat As New Virtualia.Systeme.MetaModele.Predicats.PredicateDictionnaire(PointdeVue, Numobjet)
                Dim Dico As List(Of Virtualia.Systeme.MetaModele.Outils.FicheObjetDictionnaire)
                Dico = AppObjetUser.VParent.VirListeObjetsDico.FindAll(AddressOf Predicat.InformationParIdeObjet)
                If Dico.Count = 0 Then
                    Return Nothing
                    Exit Property
                End If
                Return Dico.Item(0).PointeurModele
            End Get
        End Property

        Public ReadOnly Property PointeurDicoInfo(ByVal PointdeVue As Short, ByVal Numobjet As Short, ByVal NumInfo As Short) As Virtualia.Systeme.MetaModele.Donnees.Information
            Get
                If PointdeVue = 0 Or Numobjet = 0 Then
                    Return Nothing
                    Exit Property
                End If
                Dim Predicat As New Virtualia.Systeme.MetaModele.Predicats.PredicateDictionnaire(PointdeVue, Numobjet, NumInfo)
                Dim Dico As List(Of Virtualia.Systeme.MetaModele.Outils.FicheInfoDictionnaire)
                Dico = AppObjetUser.VParent.VirListeInfosDico.FindAll(AddressOf Predicat.InformationParIdeInfo)
                If Dico.Count = 0 Then
                    Return Nothing
                    Exit Property
                End If
                Return Dico.Item(0).PointeurModele
            End Get
        End Property

        Public ReadOnly Property PointeurDicoExperte(ByVal PointdeVue As Short, ByVal Numobjet As Short, ByVal NumInfo As Short) As Virtualia.Systeme.MetaModele.Expertes.InformationExperte
            Get
                If PointdeVue = 0 Or Numobjet = 0 Then
                    Return Nothing
                    Exit Property
                End If
                Dim Predicat As New Virtualia.Systeme.MetaModele.Predicats.PredicateDictionnaire(PointdeVue, Numobjet, NumInfo)
                Dim Dico As List(Of Virtualia.Systeme.MetaModele.Outils.FicheInfoExperte)
                Dico = AppObjetUser.VParent.VirListeExpertesDico.FindAll(AddressOf Predicat.InformationParIdeExperte)
                If Dico.Count = 0 Then
                    Return Nothing
                    Exit Property
                End If
                Return Dico.Item(0).PointeurModele
            End Get
        End Property

        Public Sub New(ByVal InstanceUtilisateur As Virtualia.Net.Session.ObjetSession, ByVal PtdeVueOutil As Short, ByVal PtdeVueAppli As Short)
            MyBase.New()
            AppObjetUser = InstanceUtilisateur
            WsPointdeVueOutil = PtdeVueOutil
            WsPointdeVueAppli = PtdeVueAppli
            WsListeScripts = New List(Of Virtualia.Net.Script.ObjetScript)
            Call ConstituerListe(WsPointdeVueOutil)
            If WsPointdeVueOutil = VI.PointdeVue.PVueScriptExport Then
                Call ConstituerListe(VI.PointdeVue.PVueScriptEdition)
            End If
        End Sub

        Public Overloads Sub Dispose() Implements IDisposable.Dispose
            Dispose(True)
            GC.SuppressFinalize(Me)
        End Sub

        Protected Overridable Overloads Sub Dispose(ByVal disposing As Boolean)
            If Not Me.disposedValue Then
                If disposing Then
                    WsListeScripts.Clear()
                End If
            End If
            Me.disposedValue = True
        End Sub
    End Class
End Namespace



