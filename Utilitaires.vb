﻿Option Strict On
Option Explicit On
Option Compare Text
Imports Microsoft.VisualBasic
Imports System.Net.Mail
Imports VI = Virtualia.Systeme.Constantes
Namespace Outils
    Public Class Utilitaires
        Private Const quote As String = """"

        Public Function SendMail(ByVal Emetteur As String, ByVal Destinataire As String, ByVal Sujet As String, ByVal Objet As String) As Boolean
            If VI.ParametreApplication("SmtpServeur") = "" Then
                Return False
            End If

            Dim Email As New MailMessage()
            Dim SmtpMail As SmtpClient
            SmtpMail = New SmtpClient(System.Configuration.ConfigurationManager.AppSettings("SmtpServeur"), _
                                      CInt(System.Configuration.ConfigurationManager.AppSettings("SmtpPort")))
            Dim BodyMail As String = ""
            Dim Erreur As String = ""

            Email.To.Add(New MailAddress(Destinataire))
            Email.IsBodyHtml = True

            BodyMail = "<HTML><BODY style=" & quote & "font-size:12pt;font-family:'Trebuchet MS';" & quote
            BodyMail &= ">" & Objet
            BodyMail &= "</BODY></HTML>"

            Email.Body = BodyMail
            Email.Subject = Sujet
            Email.From = New MailAddress(Emetteur)

            Try
                SmtpMail.Send(Email)
            Catch ex As Exception
                Erreur = ex.Message
                Email.To.Clear()
            End Try

            Return True

        End Function

    End Class
End Namespace