﻿Imports Virtualia.Ressources.Registre

Namespace Session
    Partial Public Class ObjetGlobal

        Dim WsParametres As Virtualia.Net.Datas.ParametresGlobaux
        Public ReadOnly Property VParametres As Virtualia.Net.Datas.ParametresGlobaux
            Get
                If WsParametres Is Nothing Then
                    'WsParametres = New Virtualia.Net.Datas.ParametresGlobaux(Me)
                    'La classe parametre globaux ne prend pas le meme constructeur
                End If
                Return WsParametres
            End Get

        End Property

        Protected WsRegistreVirtualia As ObjetConfiguration
        Public ReadOnly Property PointeurRegistre As ObjetConfiguration
            Get
                Return WsRegistreVirtualia
            End Get
        End Property

    End Class
End Namespace
