﻿Partial Public Class WebFonctions
    Public ReadOnly Property PointeurParametres As Virtualia.Net.Datas.ParametresGlobaux
        Get
            Return WsAppObjetGlobal.VParametres
        End Get
    End Property
    Public ReadOnly Property PointeurRegistre As Virtualia.Ressources.Registre.ObjetConfiguration
        Get
            Return WsAppObjetGlobal.PointeurRegistre
        End Get
    End Property
End Class
