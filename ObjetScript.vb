﻿Option Strict Off
Option Explicit On
Option Compare Text
Imports Microsoft.VisualBasic
Imports VI = Virtualia.Systeme.Constantes
    Namespace Script
        Public Class ObjetScript
            Inherits System.Collections.CollectionBase
            Implements IDisposable
            Private disposedValue As Boolean = False        ' Pour détecter les appels redondants
        Private WsParent As Virtualia.Net.Script.EnsembleScripts
        Private WsPointdeVueOutil As Short
        Private WsIdentifiant As Integer
        Private WsIntitule As String
        Private WsCategorie As Short
        '
        Private WsIdentifiantReel As Integer 'Pour gérer la fusion Edtion-Export
        Private WsPointdeVueReel As Integer
        Private WsCacheConditions As ArrayList
        Private WsCacheScript As ArrayList

        Public ReadOnly Property Identifiant() As Integer
            Get
                Return WsIdentifiant
            End Get
        End Property

        Public ReadOnly Property ScriptLu() As ArrayList
            Get
                Return WsCacheScript
            End Get
        End Property

        Public ReadOnly Property ConditionsLues() As ArrayList
            Get
                Return WsCacheConditions
            End Get
        End Property

        Public ReadOnly Property PointdeVue() As Short
            Get
                Return WsParent.PointdeVueAppli
            End Get
        End Property

        Public Property Intitule() As String
            Get
                Return WsIntitule
            End Get
            Set(ByVal value As String)
                WsIntitule = value
            End Set
        End Property

        Public Property Categorie() As Short
            Get
                Return WsCategorie
            End Get
            Set(ByVal value As Short)
                WsCategorie = value
            End Set
        End Property

        Public ReadOnly Property ToolTip() As String
            Get
                Dim Chaine As String = ""
                Dim Ligne As ArrayList
                Dim IndiceI As Integer
                Dim IndiceL As Integer
                Dim LibelOperateur As String = ""
                'Cache Script 
                Dim Pvue As Short     'Position 0
                Dim NumObjet As Short 'Position 1
                Dim NumInfo As Short 'Position 2
                Dim OpeComparaison As Short 'Position 3
                Dim OpeInclu As Short 'Position 4
                Dim Intitule As String 'Position 5
                'Position 6 = Lg, Position 7 = Répétition, Position 8 = Format, Position 9 = Libel Format

                For IndiceL = 0 To WsCacheScript.Count - 1
                    Ligne = CType(WsCacheScript(IndiceL), ArrayList)
                    If Ligne Is Nothing Then
                        Return Chaine
                        Exit Property
                    End If
                    If Ligne.Count < 4 Then
                        Return Chaine
                        Exit Property
                    End If
                    Select Case WsPointdeVueReel
                        Case VI.PointdeVue.PVueScriptCmc
                            OpeComparaison = CShort(Ligne(3))
                            Select Case OpeComparaison
                                Case VI.Operateurs.Egalite
                                    LibelOperateur = "Egalité avec"
                                Case Else
                                    LibelOperateur = "Différence avec"
                            End Select
                            OpeInclu = CShort(Ligne(4))
                            Select Case OpeInclu
                                Case VI.Operateurs.Inclu
                                    LibelOperateur = "Compris entre"
                            End Select
                    End Select
                    Pvue = CShort(Ligne(0))
                    NumObjet = CShort(Ligne(1))
                    NumInfo = CShort(Ligne(2))
                    Try
                        Select Case NumInfo
                            Case Is < 500
                                Intitule = WsParent.PointeurDicoInfo(Pvue, NumObjet, NumInfo).Intitule
                            Case Is > 9000
                                Intitule = Ligne(5).ToString
                            Case Else
                                Intitule = WsParent.PointeurDicoExperte(Pvue, NumObjet, NumInfo).Intitule
                        End Select
                    Catch ex As Exception
                        Return Chaine
                        Exit Property
                    End Try

                    Select Case WsPointdeVueReel
                        Case VI.PointdeVue.PVueScriptCmc
                            If IndiceL > 0 Then
                                Chaine &= Strings.Chr(13) & Strings.Chr(10)
                            End If
                            Chaine &= Intitule & Strings.Space(2)
                            Chaine &= LibelOperateur & Strings.Chr(13) & Strings.Chr(10)
                            For IndiceI = 5 To Ligne.Count - 1
                                If Ligne(IndiceI).ToString = "" Then
                                    Exit For
                                End If
                                If IndiceI < Ligne.Count - 1 Then
                                    Chaine &= Ligne(IndiceI).ToString & VI.Virgule & Strings.Space(2)
                                Else
                                    Chaine &= Ligne(IndiceI).ToString
                                End If
                            Next IndiceI
                        Case Else
                            If IndiceL < WsCacheScript.Count - 1 Then
                                Chaine &= Intitule & VI.Virgule & Strings.Space(2)
                            Else
                                Chaine &= Intitule
                            End If
                    End Select

                Next IndiceL

                Return Chaine
            End Get
        End Property

        Private Sub FormaterLeScript()
            Dim Pvue As Short = 0
            Dim IndiceK As Integer
            Dim IndiceR As Integer
            Dim N1 As Integer
            Dim N2 As Integer
            Dim N3 As Integer
            Dim NumLigne As Short
            Dim NumLigneIndice As String
            Dim OptionEnrichir As Short
            Dim ValeurEnrichir As String
            Dim LigneScript As ArrayList
            Dim CategorieDefault As Short = 0

            WsCacheConditions = New ArrayList
            WsCacheScript = New ArrayList

            Select Case WsPointdeVueReel
                Case VI.PointdeVue.PVueScriptCmc
                    Dim FicheCmc_1 As TablesObjet.ShemaREF.CMC_DESCRIPTIF

                    N1 = IndexTableVirtualia("CMC_DESCRIPTIF")
                    If N1 = -1 Then
                        Exit Sub
                    End If

                    FicheCmc_1 = CType(Item(N1), TablesObjet.ShemaREF.CMC_DESCRIPTIF)
                    Pvue = FicheCmc_1.PointdeVueCMC
                    WsCategorie = FicheCmc_1.CategorieRH
                    WsIntitule = FicheCmc_1.Intitule

                    WsCacheConditions.Add(FicheCmc_1.OperateurLogique)
                    WsCacheConditions.Add("")
                    WsCacheConditions.Add(WsParent.PointeurUtilisateur.V_RhDates.DateduJour(False))
                    WsCacheConditions.Add(0)

                    Dim FicheCmc_2 As TablesObjet.ShemaREF.CMC_SELECTION
                    Dim FicheCmc_3 As TablesObjet.ShemaREF.CMC_VALEURS
                    'Dim FicheCmc_4 As TablesObjet.ShemaREF.CMC_SQL

                    IndiceK = 0
                    Do
                        N2 = IndexFiche(IndiceK, "CMC_SELECTION")
                        If N2 = -1 Then
                            Exit Do
                        End If
                        FicheCmc_2 = CType(Item(N2), TablesObjet.ShemaREF.CMC_SELECTION)
                        NumLigne = FicheCmc_2.Numero_Ligne
                        OptionEnrichir = FicheCmc_2.OptionExperte
                        ValeurEnrichir = FicheCmc_2.ValeurExperte

                        LigneScript = Nothing
                        LigneScript = New ArrayList
                        LigneScript.Add(Pvue)
                        LigneScript.Add(FicheCmc_2.Numero_ObjetCMC)
                        LigneScript.Add(FicheCmc_2.Numero_InfoCMC)
                        Select Case FicheCmc_2.OperateurComparaison
                            Case VI.Operateurs.Egalite
                                LigneScript.Add(VI.Operateurs.Egalite)
                                LigneScript.Add(0)
                            Case VI.Operateurs.Difference
                                LigneScript.Add(VI.Operateurs.Difference)
                                LigneScript.Add(0)
                            Case VI.Operateurs.Inclu
                                LigneScript.Add(VI.Operateurs.Egalite)
                                LigneScript.Add(VI.Operateurs.Inclu)
                        End Select
                        If WsCategorie = 0 Then
                            CategorieDefault = WsParent.PointeurDicoObjet(Pvue, FicheCmc_2.Numero_ObjetCMC).CategorieRH
                        End If

                        IndiceR = 0
                        Do
                            N3 = IndexFiche(IndiceR, "CMC_VALEURS")
                            If N3 = -1 Then
                                Exit Do
                            End If
                            FicheCmc_3 = CType(Item(N3), TablesObjet.ShemaREF.CMC_VALEURS)
                            NumLigneIndice = Strings.Format(FicheCmc_3.Numero_LigneIndice, "0000")
                            If Val(Strings.Left(NumLigneIndice, 2)) = NumLigne Then
                                LigneScript.Add(FicheCmc_3.ValeurComparaison)
                            End If
                            IndiceR += 1
                        Loop

                        WsCacheScript.Add(LigneScript)

                        IndiceK += 1
                    Loop

                    If WsCategorie = 0 Then
                        WsCategorie = CategorieDefault
                    End If

                Case VI.PointdeVue.PVueScriptExport
                    Dim FicheExport_1 As TablesObjet.ShemaREF.EXPORT_DESCRIPTIF

                    N1 = IndexTableVirtualia("EXPORT_DESCRIPTIF")
                    If N1 = -1 Then
                        Exit Sub
                    End If

                    FicheExport_1 = CType(Item(N1), TablesObjet.ShemaREF.EXPORT_DESCRIPTIF)
                    Pvue = FicheExport_1.PointdeVueExport
                    WsCategorie = FicheExport_1.CategorieRH
                    WsIntitule = FicheExport_1.Intitule

                    WsCacheConditions.Add(0)
                    WsCacheConditions.Add("")
                    WsCacheConditions.Add(WsParent.PointeurUtilisateur.V_RhDates.DateduJour(False))
                    If FicheExport_1.SiHistoriqueCoche = True Then
                        WsCacheConditions.Add(1)
                    Else
                        WsCacheConditions.Add(0)
                    End If

                    Dim FicheExport_2 As TablesObjet.ShemaREF.EXPORT_SELECTION
                    Dim FicheExport_3 As TablesObjet.ShemaREF.EXPORT_FILTRE

                    IndiceK = 0
                    Do
                        N2 = IndexFiche(IndiceK, "EXPORT_SELECTION")
                        If N2 = -1 Then
                            Exit Do
                        End If
                        FicheExport_2 = CType(Item(N2), TablesObjet.ShemaREF.EXPORT_SELECTION)
                        NumLigne = FicheExport_2.Numero_Ligne

                        LigneScript = Nothing
                        LigneScript = New ArrayList
                        LigneScript.Add(Pvue)
                        LigneScript.Add(FicheExport_2.Numero_ObjetExtrait)
                        LigneScript.Add(FicheExport_2.Numero_InfoExtraite)
                        LigneScript.Add(0)
                        LigneScript.Add(0)
                        If FicheExport_2.AliasColonne <> "" Then
                            LigneScript.Add(FicheExport_2.AliasColonne)
                        Else
                            Select Case FicheExport_2.Numero_InfoExtraite
                                Case Is < 500
                                    LigneScript.Add(WsParent.PointeurDicoInfo(Pvue, FicheExport_2.Numero_ObjetExtrait, FicheExport_2.Numero_InfoExtraite).Intitule)
                                Case Is > 9000
                                    LigneScript.Add("Z")
                                Case Else
                                    LigneScript.Add(WsParent.PointeurDicoExperte(Pvue, FicheExport_2.Numero_ObjetExtrait, FicheExport_2.Numero_InfoExtraite).Intitule)
                            End Select
                        End If
                        LigneScript.Add(FicheExport_2.LongueurDonnee)
                        LigneScript.Add(FicheExport_2.NombredeColonnes)
                        LigneScript.Add(FicheExport_2.FormatDonnee)

                        IndiceR = 0
                        Do
                            N3 = IndexFiche(IndiceR, "EXPORT_FILTRE")
                            If N3 = -1 Then
                                Exit Do
                            End If
                            FicheExport_3 = CType(Item(N3), TablesObjet.ShemaREF.EXPORT_FILTRE)
                            NumLigneIndice = Strings.Format(FicheExport_3.Numero_LigneIndice, "0000")
                            If Val(Strings.Left(NumLigneIndice, 2)) = NumLigne Then
                                LigneScript.Add(FicheExport_3.ValeurFiltre)
                            End If
                            IndiceR += 1
                        Loop

                        WsCacheScript.Add(LigneScript)

                        Try
                            If WsCategorie = 0 And FicheExport_1.SiHistoriqueCoche = True Then
                                CategorieDefault = WsParent.PointeurDicoObjet(Pvue, FicheExport_2.Numero_ObjetExtrait).CategorieRH
                            Else
                                If CategorieDefault = 0 Then
                                    CategorieDefault = WsParent.PointeurDicoObjet(Pvue, FicheExport_2.Numero_ObjetExtrait).CategorieRH
                                End If
                            End If
                        Catch ex As Exception
                            CategorieDefault = 0
                        End Try

                        IndiceK += 1
                    Loop

                    If WsCategorie = 0 Then
                        WsCategorie = CategorieDefault
                    End If

                Case VI.PointdeVue.PVueScriptEdition
                    Dim FicheEdition_1 As TablesObjet.ShemaREF.EDITION_DESCRIPTIF

                    N1 = IndexTableVirtualia("EDITION_DESCRIPTIF")
                    If N1 = -1 Then
                        Exit Sub
                    End If

                    FicheEdition_1 = CType(Item(N1), TablesObjet.ShemaREF.EDITION_DESCRIPTIF)
                    Pvue = FicheEdition_1.PointdeVueEdition
                    WsCategorie = FicheEdition_1.CategorieRH
                    WsIntitule = FicheEdition_1.Intitule

                    WsCacheConditions.Add(0)
                    WsCacheConditions.Add("")
                    WsCacheConditions.Add(WsParent.PointeurUtilisateur.V_RhDates.DateduJour(False))
                    If FicheEdition_1.SiHistoriqueCoche = True Then
                        WsCacheConditions.Add(1)
                    Else
                        WsCacheConditions.Add(0)
                    End If

                    Dim FicheEdition_2 As TablesObjet.ShemaREF.EDITION_SELECTION
                    Dim FicheEdition_3 As TablesObjet.ShemaREF.EDITION_FILTRE

                    IndiceK = 0
                    Do
                        N2 = IndexFiche(IndiceK, "EDITION_SELECTION")
                        If N2 = -1 Then
                            Exit Do
                        End If
                        FicheEdition_2 = CType(Item(N2), TablesObjet.ShemaREF.EDITION_SELECTION)
                        NumLigne = FicheEdition_2.Numero_Ligne

                        LigneScript = Nothing
                        LigneScript = New ArrayList
                        LigneScript.Add(Pvue)
                        LigneScript.Add(FicheEdition_2.Numero_ObjetEdition)
                        LigneScript.Add(FicheEdition_2.Numero_InfoEdition)
                        LigneScript.Add(0)
                        LigneScript.Add(0)
                        If FicheEdition_2.AliasColonne <> "" Then
                            LigneScript.Add(FicheEdition_2.AliasColonne)
                        Else
                            Try
                                Select Case FicheEdition_2.Numero_InfoEdition
                                    Case Is < 500
                                        LigneScript.Add(WsParent.PointeurDicoInfo(Pvue, FicheEdition_2.Numero_ObjetEdition, FicheEdition_2.Numero_InfoEdition).Intitule)
                                    Case Is > 9000
                                        LigneScript.Add("Z")
                                    Case Else
                                        LigneScript.Add(WsParent.PointeurDicoExperte(Pvue, FicheEdition_2.Numero_ObjetEdition, FicheEdition_2.Numero_InfoEdition).Intitule)
                                End Select
                            Catch ex As Exception
                                LigneScript.Add("")
                            End Try
                        End If
                        LigneScript.Add(0)
                        LigneScript.Add(0)
                        LigneScript.Add(0)

                        IndiceR = 0
                        Do
                            N3 = IndexFiche(IndiceR, "EDITION_FILTRE")
                            If N3 = -1 Then
                                Exit Do
                            End If
                            FicheEdition_3 = CType(Item(N3), TablesObjet.ShemaREF.EDITION_FILTRE)
                            NumLigneIndice = Strings.Format(FicheEdition_3.Numero_LigneIndice, "0000")
                            If Val(Strings.Left(NumLigneIndice, 2)) = NumLigne Then
                                LigneScript.Add(FicheEdition_3.ValeurFiltre)
                            End If
                            IndiceR += 1
                        Loop

                        WsCacheScript.Add(LigneScript)

                        If WsCategorie = 0 And FicheEdition_1.SiHistoriqueCoche = True Then
                            CategorieDefault = WsParent.PointeurDicoObjet(Pvue, FicheEdition_2.Numero_ObjetEdition).CategorieRH
                        Else
                            If CategorieDefault = 0 Then
                                Try
                                    CategorieDefault = WsParent.PointeurDicoObjet(Pvue, FicheEdition_2.Numero_ObjetEdition).CategorieRH
                                Catch ex As Exception
                                    Exit Try
                                End Try
                            End If
                        End If

                        IndiceK += 1
                    Loop

                    If WsCategorie = 0 Then
                        WsCategorie = CategorieDefault
                    End If
            End Select

        End Sub

        Private Sub LectureObjet(ByVal NumObjet As Short)
            Dim TableauObjet(0) As String
            Dim TableauData(0) As String
            Dim IndiceO As Integer
            Dim FicheREF As Object = Nothing
            Dim Proxy As New Virtualia.Ressources.WebService.Serveur(WsParent.PointeurUtilisateur.VParent.VirUrlWebServeur)

            TableauObjet = Strings.Split(Proxy.LireUnObjet(WsParent.PointeurUtilisateur.V_NomdUtilisateurSgbd, WsPointdeVueReel, NumObjet, WsIdentifiantReel, True, 1), VI.SigneBarre, -1)
            Proxy.Dispose()

            For IndiceO = 0 To TableauObjet.Count - 1
                If TableauObjet(IndiceO) = "" Then
                    Exit For
                End If
                TableauData = Strings.Split(TableauObjet(IndiceO), VI.Tild, -1)
                Try
                    FicheREF = NouvelleFiche(WsPointdeVueReel, NumObjet)
                    FicheREF.Contenutable = TableauObjet(IndiceO)
                Catch Ex As Exception
                    Exit Try
                End Try
            Next IndiceO
            Proxy.Dispose()

        End Sub

        Public Function MettreAJourFiche(ByVal NumObjet As Short, ByVal Ancienne As String, ByVal Nouvelle As String) As Boolean
            Dim Uti As Virtualia.Net.Session.ObjetSession = WsParent.PointeurUtilisateur
            If Uti Is Nothing Then
                Return False
                Exit Function
            End If
            If Nouvelle = "" Or (Ancienne = Nouvelle) Then
                Return False
                Exit Function
            End If
            Dim Cretour As Boolean
            Dim Proxy As New Virtualia.Ressources.WebService.Serveur(WsParent.PointeurUtilisateur.VParent.VirUrlWebServeur)
            If Ancienne = "" And Nouvelle <> "" Then
                Cretour = Proxy.InsererUneFiche(Uti.V_NomdUtilisateurSgbd, WsPointdeVueReel, NumObjet, WsIdentifiantReel, Nouvelle)
                Proxy.Dispose()
                Return Cretour
                Exit Function
            End If
            Cretour = Proxy.MettreAJourUneFiche(Uti.V_NomdUtilisateurSgbd, WsPointdeVueReel, NumObjet, WsIdentifiantReel, Ancienne, Nouvelle)
            Proxy.Dispose()
            Return Cretour
        End Function

        Public Function MajCMC(ByVal NewConditions As ArrayList, ByVal NewScripts As ArrayList) As Boolean
            Dim Cretour As Boolean
            Dim IndiceK As Integer
            Dim IndiceR As Integer
            Dim N1 As Integer
            Dim N2 As Integer
            Dim N3 As Integer
            Dim LigneScript As ArrayList
            Dim NumLigneIndice As String
            Dim IdeDossier As Integer = WsIdentifiantReel

            If IdeDossier = 0 Then
                Dim Proxy As New Virtualia.Ressources.WebService.Serveur(WsParent.PointeurUtilisateur.VParent.VirUrlWebServeur)
                WsIdentifiantReel = Proxy.ObtenirUnCompteur(WsParent.PointeurUtilisateur.V_NomdUtilisateurSgbd, "CMC_DESCRIPTIF", "Max") + 1
                Proxy.Dispose()
            End If

            Dim FicheCmc_1 As TablesObjet.ShemaREF.CMC_DESCRIPTIF
            If IdeDossier = 0 Then
                FicheCmc_1 = New TablesObjet.ShemaREF.CMC_DESCRIPTIF
                FicheCmc_1.Ide_Dossier = WsIdentifiantReel
                Me.List.Add(FicheCmc_1)
            Else
                N1 = IndexTableVirtualia("CMC_DESCRIPTIF")
                If N1 = -1 Then
                    Return False
                End If
                FicheCmc_1 = CType(Item(N1), TablesObjet.ShemaREF.CMC_DESCRIPTIF)
            End If
            FicheCmc_1.PointdeVueCMC = WsParent.PointdeVueAppli
            FicheCmc_1.CategorieRH = Categorie
            FicheCmc_1.Intitule = Intitule
            FicheCmc_1.OperateurLogique = NewConditions(0)
            FicheCmc_1.Utilisateur = WsParent.PointeurUtilisateur.V_NomdUtilisateurSgbd
            FicheCmc_1.DatedeDebut = ""
            FicheCmc_1.Date_de_Fin = ""
            FicheCmc_1.SiScriptSql = 0

            Cretour = MettreAJourFiche(1, FicheCmc_1.FicheLue, FicheCmc_1.ContenuTable)
            If Cretour = False Then
                Return False
            End If

            Dim FicheCmc_2 As TablesObjet.ShemaREF.CMC_SELECTION = Nothing
            Dim FicheCmc_3 As TablesObjet.ShemaREF.CMC_VALEURS
            'Dim FicheCmc_4 As TablesObjet.ShemaREF.CMC_SQL

            IndiceK = 0
            Do
                N2 = IndexFiche(IndiceK, "CMC_SELECTION")
                If N2 = -1 Then
                    Exit Do
                Else
                    FicheCmc_2 = CType(Item(N2), TablesObjet.ShemaREF.CMC_SELECTION)
                    Cretour = SupprimerFiche(2, FicheCmc_2.FicheLue)
                End If
                IndiceK += 1
            Loop
            IndiceR = 0
            Do
                N3 = IndexFiche(IndiceR, "CMC_VALEURS")
                If N3 = -1 Then
                    Exit Do
                Else
                    FicheCmc_3 = CType(Item(N3), TablesObjet.ShemaREF.CMC_VALEURS)
                    Cretour = SupprimerFiche(3, FicheCmc_3.FicheLue)
                End If
                IndiceR += 1
            Loop

            For IndiceK = 0 To NewScripts.Count - 1
                FicheCmc_2 = New TablesObjet.ShemaREF.CMC_SELECTION
                FicheCmc_2.Ide_Dossier = WsIdentifiantReel

                LigneScript = CType(NewScripts(IndiceK), ArrayList)

                FicheCmc_2.Numero_Ligne = IndiceK + 1
                FicheCmc_2.Numero_ObjetCMC = CShort(LigneScript(1))
                FicheCmc_2.Numero_InfoCMC = CShort(LigneScript(2))
                FicheCmc_2.OperateurComparaison = CShort(LigneScript(3))
                Select Case CShort(LigneScript(4))
                    Case VI.Operateurs.Inclu
                        FicheCmc_2.OperateurComparaison = VI.Operateurs.Inclu
                End Select
                FicheCmc_2.OptionExperte = 0 'Option Enrichir
                FicheCmc_2.ValeurExperte = "" 'Valeur Enrichir

                Cretour = MettreAJourFiche(2, "", FicheCmc_2.ContenuTable)

                For IndiceR = 5 To LigneScript.Count - 1
                    FicheCmc_3 = New TablesObjet.ShemaREF.CMC_VALEURS
                    FicheCmc_3.Ide_Dossier = WsIdentifiantReel

                    NumLigneIndice = Strings.Format(IndiceK + 1, "00") & Strings.Format(IndiceR - 4, "00")
                    FicheCmc_3.Numero_LigneIndice = CInt(NumLigneIndice)
                    FicheCmc_3.ValeurComparaison = LigneScript(IndiceR).ToString

                    Cretour = MettreAJourFiche(3, "", FicheCmc_3.ContenuTable)
                Next IndiceR

            Next IndiceK

            Return Cretour
        End Function

        Public Function MajExtraction(ByVal NewConditions As ArrayList, ByVal NewScripts As ArrayList) As Boolean
            Dim Cretour As Boolean
            Dim IndiceK As Integer
            Dim IndiceR As Integer
            Dim N1 As Integer
            Dim N2 As Integer
            Dim N3 As Integer
            Dim NumLigneIndice As String
            Dim LigneScript As ArrayList
            Dim IdeDossier As Integer = WsIdentifiantReel

            If WsPointdeVueReel = VI.PointdeVue.PVueScriptEdition Then
                Cretour = SupprimerDossier()
                IdeDossier = 0
                WsPointdeVueReel = VI.PointdeVue.PVueScriptExport
            End If

            If IdeDossier = 0 Then
                Dim Proxy As New Virtualia.Ressources.WebService.Serveur(WsParent.PointeurUtilisateur.VParent.VirUrlWebServeur)
                WsIdentifiantReel = Proxy.ObtenirUnCompteur(WsParent.PointeurUtilisateur.V_NomdUtilisateurSgbd, "EXPORT_DESCRIPTIF", "Max") + 1
                Proxy.Dispose()
            End If

            Dim FicheExport_1 As TablesObjet.ShemaREF.EXPORT_DESCRIPTIF
            If IdeDossier = 0 Then
                FicheExport_1 = New TablesObjet.ShemaREF.EXPORT_DESCRIPTIF
                FicheExport_1.Ide_Dossier = WsIdentifiantReel
                Me.List.Add(FicheExport_1)
            Else
                N1 = IndexTableVirtualia("EXPORT_DESCRIPTIF")
                If N1 = -1 Then
                    Return False
                End If
                FicheExport_1 = CType(Item(N1), TablesObjet.ShemaREF.EXPORT_DESCRIPTIF)
            End If
            FicheExport_1.PointdeVueExport = WsParent.PointdeVueAppli
            FicheExport_1.CategorieRH = Categorie
            FicheExport_1.Intitule = Intitule
            FicheExport_1.Utilisateur = WsParent.PointeurUtilisateur.V_NomdUtilisateurSgbd
            FicheExport_1.DatedeDebut = ""
            FicheExport_1.Date_de_Fin = ""
            FicheExport_1.SiHistoriqueCoche = NewConditions(3)

            Cretour = MettreAJourFiche(1, FicheExport_1.FicheLue, FicheExport_1.ContenuTable)
            If Cretour = False Then
                Return False
            End If

            Dim FicheExport_2 As TablesObjet.ShemaREF.EXPORT_SELECTION = Nothing
            Dim FicheExport_3 As TablesObjet.ShemaREF.EXPORT_FILTRE

            IndiceK = 0
            Do
                N2 = IndexFiche(IndiceK, "EXPORT_SELECTION")
                If N2 = -1 Then
                    Exit Do
                Else
                    FicheExport_2 = CType(Item(N2), TablesObjet.ShemaREF.EXPORT_SELECTION)
                    Cretour = SupprimerFiche(2, FicheExport_2.FicheLue)
                End If
                IndiceK += 1
            Loop
            IndiceR = 0
            Do
                N3 = IndexFiche(IndiceR, "EXPORT_FILTRE")
                If N3 = -1 Then
                    Exit Do
                Else
                    FicheExport_3 = CType(Item(N3), TablesObjet.ShemaREF.EXPORT_FILTRE)
                    Cretour = SupprimerFiche(3, FicheExport_3.FicheLue)
                End If
                IndiceR += 1
            Loop

            For IndiceK = 0 To NewScripts.Count - 1
                FicheExport_2 = New TablesObjet.ShemaREF.EXPORT_SELECTION
                FicheExport_2.Ide_Dossier = WsIdentifiantReel

                LigneScript = CType(NewScripts(IndiceK), ArrayList)

                FicheExport_2.Numero_Ligne = IndiceK + 1
                FicheExport_2.Numero_ObjetExtrait = CShort(LigneScript(1))
                FicheExport_2.Numero_InfoExtraite = CShort(LigneScript(2))
                FicheExport_2.AliasColonne = LigneScript(5).ToString
                FicheExport_2.LongueurDonnee = CShort(LigneScript(6))
                FicheExport_2.NombredeColonnes = CShort(LigneScript(7))
                FicheExport_2.FormatDonnee = CShort(LigneScript(8))

                Cretour = MettreAJourFiche(2, "", FicheExport_2.ContenuTable)

                If LigneScript.Count > 9 Then
                    For IndiceR = 9 To LigneScript.Count - 1
                        FicheExport_3 = New TablesObjet.ShemaREF.EXPORT_FILTRE
                        FicheExport_3.Ide_Dossier = WsIdentifiantReel

                        NumLigneIndice = Strings.Format(IndiceK + 1, "00") & Strings.Format(IndiceR - 4, "00")
                        FicheExport_3.Numero_LigneIndice = CInt(NumLigneIndice)
                        FicheExport_3.ValeurFiltre = LigneScript(IndiceR).ToString
                        FicheExport_3.Numero_Information = CShort(LigneScript(2))

                        Cretour = MettreAJourFiche(3, "", FicheExport_3.ContenuTable)

                    Next IndiceR
                End If

            Next IndiceK

            Return Cretour
        End Function

        Public Function SupprimerDossier() As Boolean
            Dim Uti As Virtualia.Net.Session.ObjetSession = WsParent.PointeurUtilisateur
            If Uti Is Nothing Then
                Return False
                Exit Function
            End If
            Dim Cretour As Boolean
            Dim I As Integer
            Dim FicheREF As Object = Nothing
            Dim NumObjet As Short
            Dim Contenu As String
            Dim Proxy As New Virtualia.Ressources.WebService.Serveur(WsParent.PointeurUtilisateur.VParent.VirUrlWebServeur)
            For I = 0 To Me.List.Count - 1
                FicheREF = Me.List.Item(I)
                NumObjet = FicheREF.NumeroObjet
                Contenu = FicheREF.ContenuTable
                Cretour = Proxy.SupprimerUneFiche(Uti.V_NomdUtilisateurSgbd, WsPointdeVueReel, NumObjet, WsIdentifiantReel, Contenu)
            Next I
            Proxy.Dispose()
            Return Cretour
        End Function

        Public Function SupprimerFiche(ByVal NumObjet As Short, ByVal Contenu As String) As Boolean
            Dim Uti As Virtualia.Net.Session.ObjetSession = WsParent.PointeurUtilisateur
            If Uti Is Nothing Then
                Return False
                Exit Function
            End If
            If Contenu = "" Then
                Return False
                Exit Function
            End If
            Dim Cretour As Boolean
            Dim Proxy As New Virtualia.Ressources.WebService.Serveur(WsParent.PointeurUtilisateur.VParent.VirUrlWebServeur)
            Cretour = Proxy.SupprimerUneFiche(Uti.V_NomdUtilisateurSgbd, WsPointdeVueReel, NumObjet, WsIdentifiantReel, Contenu)
            Proxy.Dispose()
            Return Cretour
        End Function

        Public ReadOnly Property NombredeFiches() As Integer
            Get
                Return Me.List.Count
            End Get
        End Property

        Public ReadOnly Property IndexFiche(ByVal Index As Integer, ByVal NomTable As String) As Integer
            Get
                Dim IndiceV As Integer
                Dim IndexTable As Integer = 0
                For IndiceV = 0 To Me.List.Count - 1
                    Select Case Me.List.Item(IndiceV).GetType.Name
                        Case Is = NomTable
                            If IndexTable = Index Then
                                Return IndiceV
                                Exit Property
                            End If
                            IndexTable += 1
                    End Select
                Next IndiceV
                Return -1
            End Get
        End Property

        Default Public ReadOnly Property Item(ByVal Index As Integer) As Object
            Get
                If Index = -1 Then
                    Return Nothing
                End If
                Select Case Index
                    Case Is > Me.List.Count - 1
                        Return Nothing
                        Exit Property
                End Select
                Select Case Me.List.Item(Index).GetType.Name
                    Case Is = "CMC_DESCRIPTIF"
                        Return CType(Me.List.Item(Index), TablesObjet.ShemaREF.CMC_DESCRIPTIF)
                    Case Is = "CMC_SELECTION"
                        Return CType(Me.List.Item(Index), TablesObjet.ShemaREF.CMC_SELECTION)
                    Case Is = "CMC_SQL"
                        Return CType(Me.List.Item(Index), TablesObjet.ShemaREF.CMC_SQL)
                    Case Is = "CMC_VALEURS"
                        Return CType(Me.List.Item(Index), TablesObjet.ShemaREF.CMC_VALEURS)
                    Case Is = "EDITION_DESCRIPTIF"
                        Return CType(Me.List.Item(Index), TablesObjet.ShemaREF.EDITION_DESCRIPTIF)
                    Case Is = "EDITION_SELECTION"
                        Return CType(Me.List.Item(Index), TablesObjet.ShemaREF.EDITION_SELECTION)
                    Case Is = "EDITION_FILTRE"
                        Return CType(Me.List.Item(Index), TablesObjet.ShemaREF.EDITION_FILTRE)
                    Case Is = "EXPORT_DESCRIPTIF"
                        Return CType(Me.List.Item(Index), TablesObjet.ShemaREF.EXPORT_DESCRIPTIF)
                    Case Is = "EXPORT_SELECTION"
                        Return CType(Me.List.Item(Index), TablesObjet.ShemaREF.EXPORT_SELECTION)
                    Case Is = "EXPORT_FILTRE"
                        Return CType(Me.List.Item(Index), TablesObjet.ShemaREF.EXPORT_FILTRE)
                    Case Else
                        Return Nothing
                End Select
            End Get
        End Property

        Public ReadOnly Property IndexTableVirtualia(ByVal NomTable As String) As Integer
            Get
                Dim IndiceV As Integer
                Dim FicheVirt As Object
                For IndiceV = 0 To Me.List.Count - 1
                    FicheVirt = Me.List.Item(IndiceV)
                    Select Case FicheVirt.GetType.Name
                        Case Is = NomTable
                            Return IndiceV
                            Exit Property
                    End Select
                Next IndiceV
                Return -1
            End Get
        End Property

        Public ReadOnly Property IndexTableInversee(ByVal NomTable As String) As Integer
            Get
                Dim IndiceV As Integer
                Dim FicheVirt As Object
                For IndiceV = Me.List.Count - 1 To 0 Step -1
                    FicheVirt = Me.List.Item(IndiceV)
                    Select Case FicheVirt.GetType.Name
                        Case Is = NomTable
                            Return IndiceV
                            Exit Property
                    End Select
                Next IndiceV
                Return -1
            End Get
        End Property

        Public ReadOnly Property NouvelleFiche(ByVal PtdeVue As Short, ByVal NumObjet As Short) As Object
            Get
                Dim FicheREF As Object = Nothing
                Select Case PtdeVue
                    Case VI.PointdeVue.PVueScriptCmc
                        Select Case NumObjet
                            Case 1
                                FicheREF = New TablesObjet.ShemaREF.CMC_DESCRIPTIF
                            Case 2
                                FicheREF = New TablesObjet.ShemaREF.CMC_SELECTION
                            Case 3
                                FicheREF = New TablesObjet.ShemaREF.CMC_VALEURS
                            Case 4
                                FicheREF = New TablesObjet.ShemaREF.CMC_SQL
                        End Select
                    Case VI.PointdeVue.PVueScriptEdition
                        Select Case NumObjet
                            Case 1
                                FicheREF = New TablesObjet.ShemaREF.EDITION_DESCRIPTIF
                            Case 2
                                FicheREF = New TablesObjet.ShemaREF.EDITION_SELECTION
                            Case 3
                                FicheREF = New TablesObjet.ShemaREF.EDITION_FILTRE
                        End Select
                    Case VI.PointdeVue.PVueScriptExport
                        Select Case NumObjet
                            Case 1
                                FicheREF = New TablesObjet.ShemaREF.EXPORT_DESCRIPTIF
                            Case 2
                                FicheREF = New TablesObjet.ShemaREF.EXPORT_SELECTION
                            Case 3
                                FicheREF = New TablesObjet.ShemaREF.EXPORT_FILTRE
                        End Select
                End Select
                Me.List.Add(FicheREF)
                Return FicheREF
            End Get
        End Property

        Public Sub New(ByVal Host As Virtualia.Net.Script.EnsembleScripts, ByVal Ide As Integer)
            WsParent = Host
            WsIdentifiant = Ide
            WsPointdeVueOutil = WsParent.PointdeVueOutil
            If Ide > 100000 Then
                WsIdentifiantReel = Ide - 100000
                WsPointdeVueReel = VI.PointdeVue.PVueScriptEdition
            Else
                WsIdentifiantReel = Ide
                WsPointdeVueReel = WsPointdeVueOutil
            End If
            If Ide = 0 Then 'New
                Exit Sub
            End If
            Call LectureObjet(1)
            Call LectureObjet(2)
            Call LectureObjet(3)
            Select Case WsPointdeVueReel
                Case VI.PointdeVue.PVueScriptCmc
                    Call LectureObjet(4)
            End Select
            Call FormaterLeScript()
        End Sub

            Public Overloads Sub Dispose() Implements IDisposable.Dispose
                Dispose(True)
                GC.SuppressFinalize(Me)
            End Sub

            Protected Overridable Overloads Sub Dispose(ByVal disposing As Boolean)
                If Not Me.disposedValue Then
                    If disposing Then
                        WsIdentifiantReel = 0
                        Me.List.Clear()
                    End If
                End If
                Me.disposedValue = True
            End Sub

        End Class
    End Namespace
