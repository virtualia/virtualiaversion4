﻿Imports Virtualia.OutilsVisu
Imports System.Drawing
Imports System.Web.UI.WebControls

Public Class V_LabelCombo
    Inherits System.Web.UI.UserControl

#Region "Proprietes de design"
    Public WriteOnly Property EtiText As String
        Set(ByVal value As String)
            Etiquette.Text = value
        End Set
    End Property

    Public WriteOnly Property EtiHeight As Unit
        Set(ByVal value As Unit)
            Etiquette.Height = value
        End Set
    End Property

    Public WriteOnly Property EtiWidth As Unit
        Set(ByVal value As Unit)
            Etiquette.Width = value
        End Set
    End Property

    Public WriteOnly Property EtiBackColor As String
        Set(ByVal value As String)
            Etiquette.BackColor = VisuHelper.ConvertiCouleur(value)
        End Set
    End Property

    Public WriteOnly Property EtiForeColor As String
        Set(ByVal value As String)
            Etiquette.ForeColor = VisuHelper.ConvertiCouleur(value)
        End Set
    End Property

    Public WriteOnly Property EtiBorderColor As String
        Set(ByVal value As String)
            Etiquette.BorderColor = VisuHelper.ConvertiCouleur(value)
        End Set
    End Property

    Public WriteOnly Property EtiBorderWidth As String
        Set(ByVal value As String)
            Etiquette.BorderWidth = New Unit(value)
        End Set
    End Property

    Public WriteOnly Property EtiBorderStyle As BorderStyle
        Set(ByVal value As BorderStyle)
            Etiquette.BorderStyle = value
        End Set
    End Property

    Public WriteOnly Property EtiTooltip As String
        Set(ByVal value As String)
            Etiquette.ToolTip = value
        End Set
    End Property

    Public WriteOnly Property EtiVisible As Boolean
        Set(ByVal value As Boolean)
            Etiquette.Visible = value
        End Set
    End Property

    Public WriteOnly Property EtiStyle As String
        Set(ByVal value As String)
            Etiquette.Style.InitStyle(value)
        End Set
    End Property

    Public WriteOnly Property LstStyle As String
        Set(ByVal value As String)
            VComboBox.Style.InitStyle(value)
        End Set
    End Property

    Public WriteOnly Property LstHeight As String
        Set(ByVal value As String)
            VComboBox.Height = New Unit(value)
        End Set
    End Property

    Public WriteOnly Property LstWidth As String
        Set(ByVal value As String)
            VComboBox.Width = New Unit(value)
        End Set
    End Property

    Public WriteOnly Property LstBackColor As String
        Set(ByVal value As String)
            VComboBox.BackColor = VisuHelper.ConvertiCouleur(value)
        End Set
    End Property

    Public WriteOnly Property LstForeColor As String
        Set(ByVal value As String)
            VComboBox.ForeColor = VisuHelper.ConvertiCouleur(value)
        End Set
    End Property

    Public WriteOnly Property LstBorderColor As String
        Set(ByVal value As String)
            VComboBox.BorderColor = VisuHelper.ConvertiCouleur(value)
        End Set
    End Property

    Public WriteOnly Property LstBorderWidth As String
        Set(ByVal value As String)
            VComboBox.BorderWidth = New Unit(value)
        End Set
    End Property

    Public WriteOnly Property LstBorderStyle As BorderStyle
        Set(ByVal value As BorderStyle)
            VComboBox.BorderStyle = value
        End Set
    End Property

    Public WriteOnly Property LstVisible As Boolean
        Set(ByVal value As Boolean)
            VComboBox.Visible = value
        End Set
    End Property
#End Region

#Region "Proprietes fonctionnelle"
    Private _ItemsFiltre As List(Of String) = New List(Of String)()
    Public ReadOnly Property ItemsFiltre As List(Of String)
        Get
            Return _ItemsFiltre
        End Get
    End Property

    Private _controllerselection As IControllerSelection
    Public WriteOnly Property ControllerSelection As IControllerSelection
        Set(ByVal value As IControllerSelection)
            _controllerselection = value
        End Set
    End Property
#End Region

#Region "Methodes public"
    Public Sub InitFiltres(valeurs As String)

        If (valeurs Is Nothing OrElse valeurs.Trim() = "") Then
            InitFiltres(New List(Of String)())
            Return
        End If

        Dim tmp As String = valeurs


        If (tmp.EndsWith("~")) Then
            tmp = tmp.Substring(0, tmp.Length - 1)
        End If

        Dim lst As List(Of String) = New List(Of String)(tmp.Split("~"c))

        InitFiltres(lst)
    End Sub

    Public Sub InitFiltres(valeurs As IEnumerable(Of String))

        _ItemsFiltre.Add("Toutes les valeurs")

        If (valeurs Is Nothing OrElse valeurs.Count <= 0) Then
            Return
        End If

        _ItemsFiltre.AddRange(valeurs)

        VComboBox.Items.Clear()

        _ItemsFiltre.ForEach(Sub(s)
                                 VComboBox.Items.Add(s)
                             End Sub)
    End Sub
#End Region

#Region "OnInit / OnPrerender"
    Protected Overrides Sub OnInit(e As System.EventArgs)
        MyBase.OnInit(e)

        AddHandler VComboBox.SelectedIndexChanged, AddressOf Me.ComboBox_SelectedIndexChanged
    End Sub
#End Region
    
#Region "Methodes Privees"
    Private Sub ComboBox_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        If (_controllerselection Is Nothing OrElse _controllerselection.Act_SelectDataDansDataGrid Is Nothing) Then
            Return
        End If

        If (DirectCast(sender, DropDownList).SelectedItem Is Nothing) Then
            Return
        End If

        Dim act As Action(Of String) = _controllerselection.Act_SelectDataDansDataGrid
        act(DirectCast(sender, DropDownList).SelectedItem.Text)

    End Sub
#End Region

End Class