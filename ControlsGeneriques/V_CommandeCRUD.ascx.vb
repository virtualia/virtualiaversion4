﻿Imports Virtualia.ObjetBase

Public Class V_CommandeCRUD
    Inherits ObjetCommandeCRUDBase

    'Protected Overrides ReadOnly Property V_CadreCommandeCRUD As Table
    '    Get
    '        Return Me.CadreCmdCRUD
    '    End Get
    'End Property
    Protected Overrides ReadOnly Property V_CadreCommandes As Table
        Get
            Return Me.CadreCommandes
        End Get
    End Property
   
    Protected Overrides ReadOnly Property V_CommandeNew As Button
        Get
            Return Me.CommandeNew
        End Get
    End Property
    Protected Overrides ReadOnly Property V_CadreCommandeNew As Table
        Get
            Return Me.CadreNew
        End Get
    End Property

    Protected Overrides ReadOnly Property V_CommandeOK As Button
        Get
            Return Me.CommandeOK
        End Get
    End Property
    Protected Overrides ReadOnly Property V_CadreCommandeOK As Table
        Get
            Return Me.CadreCmdOK
        End Get
    End Property

    Protected Overrides ReadOnly Property V_CommandeSupp As Button
        Get
            Return Me.CommandeSupp
        End Get
    End Property
    Protected Overrides ReadOnly Property V_CadreCommandeSupp As Table
        Get
            Return Me.CadreSupp
        End Get
    End Property
End Class