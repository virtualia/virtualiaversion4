﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="V_DataGridListeFiltre.ascx.vb" Inherits="Virtualia.Net.V_DataGridListeFiltre" %>

<%@ Register Src="~/Controles/Standards/VListeCombo.ascx" TagName="VListeCombo" TagPrefix="Virtualia" %>
<%@ Register Src="~/Controles/Saisies/VCoupleEtiDonnee.ascx" TagName="VCoupleEtiDonnee" TagPrefix="Virtualia" %>

<%@ Register Src="~/ControlsGeneriques/V_LabelCombo.ascx" TagName="LabelCombo" TagPrefix="Generic" %>

<asp:Table ID="TableFiltre" runat="server" Height="22px" Width="600px" CellPadding="0" CellSpacing="0" BorderStyle="None" BorderColor="#216B68" BorderWidth="1px" HorizontalAlign="Center">
    <asp:TableRow>
        <asp:TableCell HorizontalAlign="Left">
            <%--<Virtualia:VListeCombo ID="LstFiltres" runat="server" EtiVisible="true" EtiText=" nature " EtiWidth="60px" V_NomTable="Mois" LstWidth="250px" LstBorderColor="#B0E0D7" LstBackColor="#EEECFD" EtiStyle="text-align:center" EtiBackColor="#8DA8A3" EtiForeColor="#E9FDF9" SiLigneBlanche="true" />--%>
            <Generic:LabelCombo ID="CboFiltres" runat="server" EtiVisible="true" EtiText=" nature " EtiWidth="60px" LstWidth="250px" LstBorderColor="#B0E0D7" LstBackColor="#EEECFD" EtiStyle="text-align:center" EtiBackColor="#8DA8A3" EtiForeColor="#E9FDF9" />
        </asp:TableCell>
        <asp:TableCell HorizontalAlign="Left">
            <Virtualia:VCoupleEtiDonnee ID="FDateDebut" runat="server" V_SiAutoPostBack="true" V_SiDonneeDico="false" EtiWidth="40px" DonWidth="80px" V_Nature="1" DonStyle="text-align:center" EtiText=" du " EtiBackColor="#8DA8A3" EtiForeColor="#E9FDF9" />
        </asp:TableCell>
        <asp:TableCell HorizontalAlign="Left">
            <Virtualia:VCoupleEtiDonnee ID="FDateFin" runat="server" V_SiAutoPostBack="true" V_SiDonneeDico="false" EtiWidth="40px" DonWidth="80px" V_Nature="1" DonStyle="text-align:center" EtiText=" au " EtiBackColor="#8DA8A3" EtiForeColor="#E9FDF9" EtiStyle="margin-left: 5px" />
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell ColumnSpan="3">
            <asp:GridView ID="GridDonnee" runat="server" Caption="Virtualia" AllowPaging="false" CaptionAlign="Left" CellPadding="2" BackColor="#B0E0D7" ForeColor="#142425" Height="40px" HorizontalAlign="Left" Width="500px" AlternatingRowStyle-BorderStyle="None" BorderStyle="Inset" Font-Names="Trebuchet MS" Font-Size="Small" Font-Italic="True" AutoGenerateColumns="False" AutoGenerateSelectButton="True">
                <RowStyle BackColor="#DFFAF3" Font-Names="Trebuchet MS" Font-Italic="false" Font-Bold="false" Font-Size="Small" Height="18px" HorizontalAlign="Left" VerticalAlign="Middle" />
                <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Left" />
                <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                <HeaderStyle BackColor="#B0E0D7" ForeColor="#1C5150" Font-Bold="False" Font-Italic="True" Font-Names="Trebuchet MS" Font-Size="Small" HorizontalAlign="Left" BorderStyle="Outset" BorderWidth="1px" Height="20px" VerticalAlign="Middle" />
                <EditRowStyle BackColor="#2461BF" />
                <AlternatingRowStyle BackColor="White" />
            </asp:GridView>
        </asp:TableCell>
    </asp:TableRow>
</asp:Table>

