﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="V_CommandeOK.ascx.vb" Inherits="Virtualia.Net.V_CommandeOK" %>

<asp:Table ID="CadreCmdOK" runat="server" Height="22px" CellPadding="0" CellSpacing="0" BackImageUrl="~/Images/Boutons/OK_Std.bmp" Visible="false" BorderWidth="2px" BorderStyle="Outset" BorderColor="#FFEBC8" ForeColor="#FFF2DB" Width="70px" HorizontalAlign="Right" Style="margin-top: 3px; margin-right: 3px">
    <asp:TableRow>
        <asp:TableCell VerticalAlign="Bottom">
            <asp:Button ID="CommandeOK" runat="server" Text="Valider" Width="65px" Height="20px" BackColor="Transparent" BorderColor="#B0E0D7" ForeColor="#D7FAF3" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Font-Italic="true" BorderStyle="None" Style="margin-left: 6px; text-align: center;" />
        </asp:TableCell>
    </asp:TableRow>
</asp:Table>
