﻿Option Strict Off
Option Explicit On
Option Compare Text

Imports System
Imports VI = Virtualia.Systeme.Constantes
Imports System.Drawing
Imports Virtualia.OutilsVisu
Imports Virtualia.Systeme.Datas
Imports System.Collections.Generic

Public Class V_DataGridListeFiltre
    Inherits UserControl
    Implements IControlDataGridFiltreListe

    Implements IControlModule

#Region "IControlModule"
    Public Function GetApplication() As HttpApplicationState Implements IControlModule.GetApplication
        Return Me.Application
    End Function
    Public Function GetSession() As HttpSessionState Implements IControlModule.GetSession
        Return Me.Session
    End Function
#End Region

#Region "Proprietes de design"
    Public Property CadreWidth() As Unit
        Get
            Return GridDonnee.Width
        End Get
        Set(ByVal value As Unit)
            GridDonnee.Width = value
        End Set
    End Property

    Public Property BackColorCaption() As Color
        Get
            Return GridDonnee.BackColor
        End Get
        Set(ByVal value As Color)
            GridDonnee.BackColor = value
            GridDonnee.HeaderStyle.BackColor = value
        End Set
    End Property

    Public Property ForeColorCaption() As Color
        Get
            Return GridDonnee.ForeColor
        End Get
        Set(ByVal value As Color)
            GridDonnee.ForeColor = value
            GridDonnee.HeaderStyle.ForeColor = value
        End Set
    End Property

    Public Property BackColorRow() As Color
        Get
            Return GridDonnee.RowStyle.BackColor
        End Get
        Set(ByVal value As Color)
            GridDonnee.RowStyle.BackColor = value
        End Set
    End Property

    Public Property ForeColorRow() As Color
        Get
            Return GridDonnee.RowStyle.ForeColor
        End Get
        Set(ByVal value As Color)
            GridDonnee.RowStyle.ForeColor = value
        End Set
    End Property

    Private _niveaucharte As TypeNiveauCharte = TypeNiveauCharte.PER
    Public WriteOnly Property V_NiveauCharte As Short
        Set(ByVal value As Short)

            If (value < 0 And value > 3) Then
                _niveaucharte = TypeNiveauCharte.Aucun
                Return
            End If

            Select Case value
                Case 0
                    _niveaucharte = TypeNiveauCharte.Aucun
                Case 1
                    _niveaucharte = TypeNiveauCharte.PERBIS
                Case 2
                    _niveaucharte = TypeNiveauCharte.PER
                Case 3
                    _niveaucharte = TypeNiveauCharte.REF
            End Select

        End Set
    End Property
#End Region

#Region "Proprietes fonctionnelles"
    Public WriteOnly Property SiListeFiltreVisible() As Boolean
        Set(ByVal value As Boolean)
            CboFiltres.Visible = value
        End Set
    End Property

    Public WriteOnly Property SiCaptionVisible As Boolean
        Set(ByVal value As Boolean)
            CacheCourant.DesignCaption.CaptionVisible = value
            Me.ViewState.AjouteValeur(CacheDataGridListeFiltre.KeyState, CacheCourant)
        End Set
    End Property

    Public WriteOnly Property SiColonneSelect As Boolean
        Set(ByVal value As Boolean)
            GridDonnee.AutoGenerateSelectButton = value
        End Set
    End Property

    Public WriteOnly Property SiPagination As Boolean
        Set(ByVal value As Boolean)
            GridDonnee.AllowPaging = value
            If value Then
                GridDonnee.PagerSettings.Mode = PagerButtons.Numeric
                GridDonnee.PagerSettings.Position = PagerPosition.TopAndBottom

                GridDonnee.PagerStyle.BackColor = VisuHelper.ConvertiCouleur("#OE5F5C")
                GridDonnee.PagerStyle.ForeColor = VisuHelper.ConvertiCouleur("#D7FAF3")
            End If
        End Set
    End Property

    Public WriteOnly Property TaillePage As Integer
        Set(ByVal value As Integer)
            GridDonnee.PageSize = value
        End Set
    End Property

    Public ReadOnly Property TotalLignes As Integer Implements IControlDataGridFiltreListe.TotalLignes
        Get
            Return CacheCourant.Donnees.Count
        End Get
    End Property

    Public WriteOnly Property Donnees As String Implements IControlDataGridFiltreListe.Donnees
        Set(ByVal value As String)
            CacheCourant.Donnees = CacheItemCollection.ToCollection(value, VI.SigneBarre, VI.Tild)
            CacheCourant.DoitRecharger = True
            Me.ViewState.AjouteValeur(CacheDataGridListeFiltre.KeyState, CacheCourant)
        End Set
    End Property

    Public ReadOnly Property Colonnes As ColonneDataGridCollection Implements IControlDataGridFiltreListe.Colonnes
        Get
            Return CacheCourant.Colonnes
        End Get
    End Property

    Private _ControleurDataGrid As IControllerSelection
    Public WriteOnly Property ControleurDataGrid As IControllerSelection Implements IControlDataGridFiltreListe.ControleurDataGrid
        Set(value As IControllerSelection)
            _ControleurDataGrid = value
        End Set
    End Property

    Public WriteOnly Property IndexFicheCourante As Integer Implements IControlDataGridFiltreListe.IndexFicheCourante
        Set(ByVal value As Integer)
            CacheCourant.IndexFicheCourante = value
            Me.ViewState.AjouteValeur(CacheDataGridListeFiltre.KeyState, CacheCourant)
        End Set
    End Property

    Private _Act_InitFiltre As Action(Of Integer)
    Public Property Act_InitFiltre As Action(Of Integer) Implements IControlDataGridFiltreListe.Act_InitFiltre
        Get
            Return _Act_InitFiltre
        End Get
        Set(ByVal value As Action(Of Integer))
            _Act_InitFiltre = value
        End Set
    End Property

    Public ReadOnly Property Act_SelectDataDansDataGrid As Action(Of String) Implements IControllerSelection.Act_SelectDataDansDataGrid
        Get
            Return Sub(s)
                       CacheCourant.ValeurFiltre = s
                       CacheCourant.DoitRecharger = True
                       Me.ViewState.AjouteValeur(CacheDataGridListeFiltre.KeyState, CacheCourant)
                   End Sub
        End Get
    End Property

#End Region

#Region "Proprietes privees"
    Private _webfonctions As WebFonctions
    Private ReadOnly Property CacheCourant As CacheDataGridListeFiltre
        Get
            If Not (Me.ViewState.KeyExiste(CacheDataGridListeFiltre.KeyState)) Then
                Me.ViewState.Add(CacheDataGridListeFiltre.KeyState, New CacheDataGridListeFiltre())
            End If

            Return Me.ViewState.GetValeur(Of CacheDataGridListeFiltre)(CacheDataGridListeFiltre.KeyState)

        End Get
    End Property
#End Region

#Region "Methodes publics"
    Public Sub InitCaptions(lstCaptions As List(Of String))
        If (lstCaptions Is Nothing) Then
            CacheCourant.DesignCaption.CaptionVisible = False
        End If
        If (lstCaptions.Count < 3) Then
            CacheCourant.DesignCaption.CaptionVisible = False
        End If

        CacheCourant.DesignCaption.Libelles.Clear()
        CacheCourant.DesignCaption.Libelles.AddRange(lstCaptions)

        Me.ViewState.AjouteValeur(CacheDataGridListeFiltre.KeyState, CacheCourant)

    End Sub
    Public Sub InitColonnes(colonnes As ColonneDataGridCollection)

        If (Me.IsPostBack) Then
            Return
        End If

        If (colonnes Is Nothing) Then
            Return
        End If

        If (colonnes.Count <= 0) Then
            Return
        End If


        Dim bnd As BoundField
        Dim datafield As String

        For Each cl As ColonneDataGrid In colonnes

            datafield = "Colonne_" & cl.Num
            bnd = New BoundField()

            bnd.DataField = datafield
            bnd.HeaderText = cl.Libelle
            bnd.ItemStyle.HorizontalAlign = cl.Alignement

            GridDonnee.Columns.Add(bnd)

            If (cl.EstClef) Then
                GridDonnee.DataKeyNames = New String() {datafield}
                bnd.Visible = False
            End If
        Next

        CacheCourant.Colonnes = colonnes

        Me.ViewState.AjouteValeur(CacheDataGridListeFiltre.KeyState, CacheCourant)

    End Sub
    Public Sub InitFiltre(valeurs As String)
        CboFiltres.InitFiltres(valeurs)
    End Sub

    Public Function GetTableau(sep As String) As List(Of String)

        Dim ret As List(Of String) = New List(Of String)()

        If (CacheCourant.Donnees Is Nothing) Then
            Return ret
        End If

        If (CacheCourant.Donnees.Count <= 0) Then
            Return ret
        End If

        Dim lgchaine = ""
        Dim separateur = ""
        CacheCourant.Donnees.ForEach(Sub(lg)
                                         lgchaine = ""
                                         lg.ForEach(Sub(ch)
                                                        If (ColonneDataGridCollection.ColonneVisible(CacheCourant.Colonnes, ch.NumDonnee)) Then
                                                            lgchaine &= separateur & ch.valeur
                                                            separateur = sep
                                                        End If
                                                    End Sub)
                                         ret.Add(lgchaine)
                                     End Sub)

        Return ret

    End Function
#End Region

#Region "Evenements"
    'Protected Sub LstFiltres_ValeurChange(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.DonneeChangeEventArgs) Handles CboFiltres.ValeurChange
    '    CacheCourant.ValeurFiltre = e.Valeur
    '    CacheCourant.DoitRecharger = True
    '    Me.ViewState.AjouteValeur(CacheDataGridListeFiltre.KeyState, CacheCourant)
    'End Sub
    Protected Sub FDateDebut_ValeurChange(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.DonneeChangeEventArgs) Handles FDateDebut.ValeurChange
        CacheCourant.ValeurDebut = e.Valeur
        CacheCourant.DoitRecharger = True
        Me.ViewState.AjouteValeur(CacheDataGridListeFiltre.KeyState, CacheCourant)
    End Sub
    Protected Sub FDateFin_ValeurChange(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.DonneeChangeEventArgs) Handles FDateFin.ValeurChange
        CacheCourant.ValeurFin = e.Valeur
        CacheCourant.DoitRecharger = True
        Me.ViewState.AjouteValeur(CacheDataGridListeFiltre.KeyState, CacheCourant)
    End Sub

    Protected Sub GridDonnee_PageIndexChanging(sender As Object, e As GridViewPageEventArgs)
        If e.NewPageIndex >= 0 And e.NewPageIndex < GridDonnee.PageCount Then
            CacheCourant.DoitRecharger = True
            CacheCourant.NumPage = e.NewPageIndex
            Me.ViewState.AjouteValeur(CacheDataGridListeFiltre.KeyState, CacheCourant)
        End If
    End Sub
    Protected Sub GridDonnee_PageIndexChanged(sender As Object, e As EventArgs)
        GridDonnee.PageIndex = CacheCourant.NumPage
        Me.ViewState.AjouteValeur(CacheDataGridListeFiltre.KeyState, CacheCourant)
    End Sub
    Protected Sub GridDonnee_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GridDonnee.SelectedIndexChanged
        If (GridDonnee.SelectedDataKey Is Nothing) Then
            Return
        End If

        If (GridDonnee.SelectedDataKey.Values.Count <= 0) Then
            Return
        End If

        If (_ControleurDataGrid Is Nothing) Then
            Return
        End If

        If (_ControleurDataGrid.Act_SelectDataDansDataGrid Is Nothing) Then
            Return
        End If

        Dim act As Action(Of String) = _ControleurDataGrid.Act_SelectDataDansDataGrid
        act(GridDonnee.SelectedDataKey.Item(0).ToString())
    End Sub
#End Region

#Region "Methodes privees"
    Private Sub FaireListe()

        Dim datas As List(Of ObjetIlistGrid) = New List(Of ObjetIlistGrid)

        If (CacheCourant.Donnees Is Nothing) Then
            datas.Add(New ObjetIlistGrid())
            GridDonnee.DataSource = datas
            GridDonnee.DataBind()
            GridDonnee.Caption = CacheCourant.DesignCaption.GetCaption(0)
            GridDonnee.SelectedIndex = -1
            Return
        End If

        If (CacheCourant.Donnees.Count <= 0) Then
            datas.Add(New ObjetIlistGrid())
            GridDonnee.DataSource = datas
            GridDonnee.DataBind()
            GridDonnee.Caption = CacheCourant.DesignCaption.GetCaption(0)
            GridDonnee.SelectedIndex = -1
            Return
        End If

        If CacheCourant.ValeurFiltre = "" Then
            CacheCourant.ValeurFiltre = "Toutes les valeurs"
            If CacheCourant.ValeurDebut = "" Then
                FDateDebut.DonText = "01/01/" & Strings.Right(_webfonctions.ViRhDates.DateduJour(False), 4)
                CacheCourant.ValeurDebut = "01/01/" & Strings.Right(_webfonctions.ViRhDates.DateduJour(False), 4)
            End If
            If CacheCourant.ValeurFin = "" Then
                FDateFin.DonText = "31/12/" & Strings.Right(_webfonctions.ViRhDates.DateduJour(False), 4)
                CacheCourant.ValeurFin = "31/12/" & Strings.Right(_webfonctions.ViRhDates.DateduJour(False), 4)
            End If
            Me.ViewState.AjouteValeur(CacheDataGridListeFiltre.KeyState, CacheCourant)
        End If
        If CacheCourant.ValeurDebut = "" Then
            CacheCourant.ValeurDebut = "01/01/1980"
        End If
        If CacheCourant.ValeurFin = "" Then
            CacheCourant.ValeurFin = "31/12/2050"
        End If
        Me.ViewState.AjouteValeur(CacheDataGridListeFiltre.KeyState, CacheCourant)

        Dim Func_Predicat As Func(Of CacheItem, Boolean) = Function(cit)
                                                               If (cit.Count <= 0) Then
                                                                   Return False
                                                               End If

                                                               Dim afaire As Boolean = True

                                                               If (CacheCourant.ValeurFiltre <> "Toutes les valeurs") Then
                                                                   afaire = cit(1).valeur = CacheCourant.ValeurFiltre
                                                               End If

                                                               If (Not afaire) Then
                                                                   Return False
                                                               End If

                                                               Dim valdate As String = cit(0).valeur

                                                               If (valdate Is Nothing) Then
                                                                   Return False
                                                               End If

                                                               Dim comparaison As Short = _webfonctions.ViRhDates.ComparerDates(valdate, CacheCourant.ValeurDebut)
                                                               If (comparaison = VI.ComparaisonDates.PlusPetit) Then
                                                                   Return False
                                                               End If

                                                               comparaison = _webfonctions.ViRhDates.ComparerDates(valdate, CacheCourant.ValeurFin)
                                                               If (comparaison = VI.ComparaisonDates.PlusGrand) Then
                                                                   Return False
                                                               End If

                                                               Return True
                                                           End Function

        Dim cacheitemfiltres As CacheItemCollection = New CacheItemCollection()

        For Each cit As CacheItem In CacheCourant.Donnees
            If (Func_Predicat(cit)) Then
                cacheitemfiltres.Add(cit)
            End If
        Next

        If (cacheitemfiltres.Count <= 0) Then
            datas.Add(New ObjetIlistGrid())
            GridDonnee.DataSource = datas
            GridDonnee.DataBind()
            GridDonnee.Caption = CacheCourant.DesignCaption.GetCaption(0)
            GridDonnee.SelectedIndex = -1
            GridDonnee_SelectedIndexChanged(Nothing, Nothing)
            Return
        End If

        Dim objlst As ObjetIlistGrid = Nothing
        cacheitemfiltres.ForEach(Sub(cit)
                                     objlst = New ObjetIlistGrid()

                                     For Each col As ColonneDataGrid In CacheCourant.Colonnes
                                         objlst.GetType().GetProperty("Colonne_" & col.Num).SetValue(objlst, cit(col.Num - 1).valeur, Nothing)
                                     Next

                                     datas.Add(objlst)
                                 End Sub)

        GridDonnee.DataSource = datas
        GridDonnee.DataBind()

        GridDonnee.Caption = CacheCourant.DesignCaption.GetCaption(datas.Count)

        If (CacheCourant.IndexFicheCourante >= 0) Then
            GridDonnee.SelectedIndex = CacheCourant.Donnees.GetIndexRow(CacheCourant.IndexFicheCourante, CacheCourant.Colonnes.GetColumnClef())
        Else
            GridDonnee.SelectedIndex = 0
        End If

        GridDonnee_SelectedIndexChanged(Nothing, Nothing)
    End Sub

    Private Function ChargeDonneeData(dons As CacheItem) As ObjetIlistGrid
        Dim objlst = New ObjetIlistGrid()

        For Each col As ColonneDataGrid In CacheCourant.Colonnes
            objlst.GetType().GetProperty("Colonne_" & col.Num).SetValue(objlst, dons(col.Num - 1).valeur, Nothing)
        Next
        Return objlst
    End Function
#End Region

#Region "OnInit / OnPreRender"
    Protected Overrides Sub OnInit(e As EventArgs)
        MyBase.OnInit(e)

        _webfonctions = New WebFonctions(Me)

        Me.CboFiltres.ControllerSelection = Me
    End Sub
    Protected Overrides Sub OnPreRender(e As EventArgs)
        MyBase.OnPreRender(e)

        If _niveaucharte <> TypeNiveauCharte.Aucun Then
            GridDonnee.BackColor = _webfonctions.CouleurCharte(_niveaucharte, "Bordure")
            GridDonnee.ForeColor = _webfonctions.CouleurCharte(_niveaucharte, "Police_Fonce")

            GridDonnee.RowStyle.BackColor = _webfonctions.CouleurCharte(_niveaucharte, "Police_Claire")
            GridDonnee.RowStyle.ForeColor = _webfonctions.CouleurCharte(_niveaucharte, "Selection")

            GridDonnee.HeaderStyle.BackColor = _webfonctions.CouleurCharte(_niveaucharte, "Bordure")
            GridDonnee.HeaderStyle.ForeColor = _webfonctions.CouleurCharte(_niveaucharte, "Selection")

            GridDonnee.SelectedRowStyle.BackColor = _webfonctions.CouleurCharte(_niveaucharte, "Selection")
            GridDonnee.SelectedRowStyle.ForeColor = _webfonctions.CouleurCharte(_niveaucharte, "Police_Claire")
            GridDonnee.EditRowStyle.BackColor = _webfonctions.CouleurCharte(_niveaucharte, "Selection")
            GridDonnee.EditRowStyle.ForeColor = _webfonctions.CouleurCharte(_niveaucharte, "Police_Claire")
        End If

        If (CacheCourant.DoitRecharger) Then
            CacheCourant.DoitRecharger = False
            Me.ViewState.AjouteValeur(CacheDataGridListeFiltre.KeyState, CacheCourant)
            FaireListe()
        End If
    End Sub
#End Region

End Class