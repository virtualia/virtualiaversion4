﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="V_CommandeCRUD.ascx.vb" Inherits="Virtualia.Net.V_CommandeCRUD" %>

<asp:Table ID="CadreCommandes" runat="server" Height="22px" CellPadding="0" CellSpacing="0" Width="244px" HorizontalAlign="Right" Style="margin-top: 3px; margin-right: 3px">
    <asp:TableRow>
        <asp:TableCell VerticalAlign="Bottom">
            <asp:Table ID="CadreNew" runat="server" BackImageUrl="~/Images/Boutons/New_Std.bmp" Width="70px" BorderWidth="2px" BorderStyle="Outset" BorderColor="#B0E0D7" ForeColor="#D7FAF3" CellPadding="0" CellSpacing="0" Visible="true">
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Button ID="CommandeNew" runat="server" Text="Nouveau" Width="68px" Height="20px" BackColor="Transparent" BorderColor="#B0E0D7" ForeColor="#D7FAF3" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Font-Italic="true" BorderStyle="None" Style="margin-left: 10px; text-align: right;" ToolTip="Nouvelle fiche" />
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
        <asp:TableCell VerticalAlign="Bottom">
            <asp:Table ID="CadreSupp" runat="server" BackImageUrl="~/Images/Boutons/Supp_Std.bmp" Width="70px" BorderWidth="2px" BorderStyle="Outset" BorderColor="#B0E0D7" ForeColor="#D7FAF3" CellPadding="0" CellSpacing="0" Visible="true">
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Button ID="CommandeSupp" runat="server" Text="Supprimer" Width="68px" Height="20px" BackColor="Transparent" BorderColor="#B0E0D7" ForeColor="#D7FAF3" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Font-Italic="true" BorderStyle="None" Style="margin-left: 10px; text-align: right;" ToolTip="Supprimer la fiche" />
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>

        <asp:TableCell VerticalAlign="Bottom">
            <asp:Table ID="CadreCmdOK" runat="server" BackImageUrl="~/Images/Boutons/OK_Std.bmp" Width="70px" BorderWidth="2px" BorderStyle="Outset" BorderColor="#B0E0D7" ForeColor="#D7FAF3" CellPadding="0" CellSpacing="0" Visible="true">
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Button ID="CommandeOK" runat="server" Text="Valider" Width="68px" Height="20px" BackColor="Transparent" BorderColor="#B0E0D7" ForeColor="#D7FAF3" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Font-Italic="true" BorderStyle="None" Style="margin-left: 6px; text-align: right;" />
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>
</asp:Table>
