﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="V_DataGrid.ascx.vb" Inherits="Virtualia.Net.V_DataGrid" ClassName="V_DataGrid" %>


<asp:Table ID="CadreGrid" runat="server" HorizontalAlign="Center">
    <asp:TableRow>
        <asp:TableCell HorizontalAlign="Left">
            <asp:CheckBox ID="CaseACocher" runat="server" Text="Afficher le détail" Visible="true" AutoPostBack="true" BackColor="#B0E0D7" BorderColor="#B0E0D7" BorderStyle="Outset" BorderWidth="2px" ForeColor="#142425" Height="20px" Width="250px" Checked="false" Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Small" style="margin-top: 5px; margin-left: 10px; font-style: oblique; text-indent: 5px; text-align: left" />
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell VerticalAlign="Top">
            <asp:GridView ID="GridDonnee" runat="server" Caption="Virtualia" CaptionAlign="Left" CellPadding="2" BackColor="#B0E0D7"  ForeColor="#142425" Height="40px" HorizontalAlign="Left" Width="1000px" AlternatingRowStyle-BorderStyle="None" BorderStyle="Inset"  Font-Names="Trebuchet MS" Font-Size="Small" Font-Italic="True" AutoGenerateColumns="False" AutoGenerateSelectButton="True" OnPageIndexChanging="GridDonnee_PageIndexChanging" OnPageIndexChanged="GridDonnee_PageIndexChanged" OnSelectedIndexChanged="GridDonnee_SelectedIndexChanged">
                <RowStyle BackColor="#DFFAF3" Font-Names="Trebuchet MS" Font-Italic="false"  Font-Bold="false" Font-Size="Small" Height="18px" HorizontalAlign="Left" VerticalAlign="Middle" />
                <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White"/>
                <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Left" />
                <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                <HeaderStyle BackColor="#225C59" ForeColor="White" Font-Bold="False" Font-Italic="True"  Font-Names="Trebuchet MS" Font-Size="Small" HorizontalAlign="Left" BorderStyle="Outset" BorderWidth="1px" Height="20px" VerticalAlign="Middle" />
                <EditRowStyle BackColor="#2461BF" />
                <AlternatingRowStyle BackColor="White" />
            </asp:GridView>
        </asp:TableCell>
    </asp:TableRow>
</asp:Table>
