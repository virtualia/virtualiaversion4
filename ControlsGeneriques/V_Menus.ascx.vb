﻿Imports Virtualia.OutilsVisu
Imports System.Linq
Imports System.Drawing
Imports Virtualia.ObjetBase

Public Class V_Menus
    Inherits UserControl
    Implements IControlMenus

#Region "Variables"
    Private _ControllerMenu As IControllerMenu
#End Region

#Region "Constructeur"
    Public Sub New()
        MyBase.New()
    End Sub
#End Region

#Region "OnInit / OnPreRender"
    Protected Overrides Sub OnInit(e As EventArgs)
        MyBase.OnInit(e)
    End Sub
#End Region

#Region "Proprietes"
    'Public ReadOnly Property MenuItems As MenuItemCollection Implements IControlMenus.MenuItems
    '    Get
    '        Return MenuChoix.Items
    '    End Get
    'End Property

    Public ReadOnly Property NomControl As String Implements IControlMenus.NomControl
        Get
            Return Me.ID
        End Get
    End Property


    Public WriteOnly Property ControllerMenu As IControllerMenu Implements IControlMenus.ControllerMenu
        Set(value As IControllerMenu)
            _ControllerMenu = value
        End Set
    End Property

    Public WriteOnly Property TypeMenu As TypeNiveauMenu Implements IControlMenus.TypeMenu
        Set(value As TypeNiveauMenu)

            If (value = TypeNiveauMenu.Principal) Then
                Return
            End If

            AffecteCouleurSousMenu()
        End Set
    End Property

    Public Sub Charge(items As Object) Implements IControlBase.Charge
        Dim lst As IList(Of MenuItem) = TryCast(items, IList(Of MenuItem))

        If (lst Is Nothing) Then
            Return
        End If

        For Each mnu As MenuItem In lst
            MenuChoix.Items.Add(mnu)
        Next

    End Sub

    Public Sub ReInit(reinit As Boolean) Implements IControlMenusBase.ReInit

        If Not (reinit) Then
            Return
        End If

        If (MenuChoix.Items.Count <= 0) Then
            Return
        End If

        For Each mnu As MenuItem In MenuChoix.Items
            mnu.Selected = False
        Next

        MenuChoix.Items(0).Selected = True
    End Sub
#End Region

#Region "Methodes public"
    Private Sub AffecteCouleurSousMenu()

        Dim style_select As MenuItemStyle = New MenuItemStyle()
        style_select.CopyFrom(Me.MenuChoix.StaticHoverStyle)

        Dim style_hover As MenuItemStyle = New MenuItemStyle()
        style_hover.CopyFrom(Me.MenuChoix.StaticSelectedStyle)

        Dim backcolor As Color = Me.CadreMenus.BackColor
        Dim backcolorInverse As Color = Me.MenuChoix.StaticHoverStyle.BackColor

        Me.CadreMenus.BackColor = backcolorInverse
        style_hover.BackColor = backcolor

        Me.MenuChoix.StaticSelectedStyle.CopyFrom(style_select)
        Me.MenuChoix.StaticHoverStyle.CopyFrom(style_hover)

    End Sub
    Public Sub EndInit() Implements IControlMenus.EndInit
        If Not (_ControllerMenu Is Nothing) AndAlso Not (_ControllerMenu.Act_ClickMenu Is Nothing) Then
            AddHandler MenuChoix.MenuItemClick, AddressOf Me.ClickMenu
        End If
    End Sub
#End Region

#Region "Methodes Privees"
    Private Sub ClickMenu(sender As Object, ev As MenuEventArgs)

        Dim act As Action(Of CacheClickMenu) = _ControllerMenu.Act_ClickMenu

        Dim cache As CacheClickMenu = New CacheClickMenu()
        cache.TypeSource = TypeSourceMenu.V_Menu

        If (ev.Item.Value = "RET") Then
            cache.TypeClick = TypeClickMenu.RETOUR
            act(cache)
            Return
        End If

        cache.TypeClick = TypeClickMenu.VUE
        cache.Value = ev.Item.Value
        act(cache)
    End Sub
#End Region










    




    
End Class