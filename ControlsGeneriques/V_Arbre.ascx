﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="V_Arbre.ascx.vb" Inherits="Virtualia.Net.V_Arbre" ClassName="V_Arbre"%>

<%--BackColor="#1C5151"--%>
<asp:Panel ID="PanelArmoire" runat="server" HorizontalAlign="Center" Width="720px" Height="940px" Font-Names="Trebuchet MS" Font-Italic="true" >
    <asp:Table ID="CadreArmoire" runat="server" HorizontalAlign="Center">
        <asp:TableRow>
            <asp:TableCell>
                <%-- 
                Row des :
                    1 - Cbo Type d'armoire
                    2 - Cadre filtre Lettre
                    3 - Liste arborescente
                    4 - Statut 
                --%>
                <asp:Table ID="CadreListe" runat="server" >
                    <asp:TableRow >
                        <asp:TableCell Width="640px" HorizontalAlign="Center" VerticalAlign="Top" BackColor="#6D9092">
                            <asp:Table ID="TableTypeArmoire" runat="server" CellPadding="0" CellSpacing="0" HorizontalAlign="Center" Height="40px" >
                                <asp:TableRow>
                                    <asp:TableCell Height="22px" Width="180px" VerticalAlign="Middle" HorizontalAlign="Center">
                                        <asp:Label ID="EtiTypeArmoire" runat="server" Height="22px" Width="180px" BackColor="Transparent" ForeColor="White" BorderStyle="Solid" BorderColor="#5E9598" BorderWidth="1px" Text="Choix du type d'armoire" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Font-Italic="true" Style="margin-top: 1px; text-align: center" />
                                    </asp:TableCell>
                                    <asp:TableCell Height="22px" Width="250px" VerticalAlign="Middle" HorizontalAlign="Center">
                                        <asp:DropDownList ID="CboTypeArmoire" runat="server" AutoPostBack="True" Height="22px" Width="250px" BackColor="#A8BBB8" ForeColor="#124545" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Font-Italic="false" Style="margin-top: 0px; border-spacing: 2px; text-indent: 5px; text-align: left" OnSelectedIndexChanged="CboTypeArmoire_SelectedIndexChanged"/>
                                    </asp:TableCell>
                                </asp:TableRow>
                            </asp:Table>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell ID="CelluleLettre" runat="server" Width="650px" Height="25px" Visible="false">
                            <asp:Table ID="CadreLettre" runat="server" CellPadding="0" CellSpacing="0" HorizontalAlign="Center">
                                <asp:TableRow ID="RowLettres">
                                    <asp:TableCell>
                                        <asp:ImageButton Width="20px" ID="ButtonA" runat="server" BorderStyle="None" ImageAlign="Middle" ImageUrl="~/Images/Lettres/A.bmp" OnClick="ButtonLettre_Click"/>
                                    </asp:TableCell>
                                    <asp:TableCell>
                                        <asp:ImageButton Width="20px" ID="ButtonB" runat="server" BorderStyle="None" ImageAlign="Middle" ImageUrl="~/Images/Lettres/B.bmp" OnClick="ButtonLettre_Click"/>
                                    </asp:TableCell>
                                    <asp:TableCell>
                                        <asp:ImageButton Width="20px" ID="ButtonC" runat="server" BorderStyle="None" ImageAlign="Middle" ImageUrl="~/Images/Lettres/C.bmp" OnClick="ButtonLettre_Click"/>
                                    </asp:TableCell>
                                    <asp:TableCell>
                                        <asp:ImageButton Width="20px" ID="ButtonD" runat="server" BorderStyle="None" ImageAlign="Middle" ImageUrl="~/Images/Lettres/D.bmp" OnClick="ButtonLettre_Click"/>
                                    </asp:TableCell>
                                    <asp:TableCell>
                                        <asp:ImageButton Width="20px" ID="ButtonE" runat="server" BorderStyle="None" ImageAlign="Middle" ImageUrl="~/Images/Lettres/E.bmp" OnClick="ButtonLettre_Click"/>
                                    </asp:TableCell>
                                    <asp:TableCell>
                                        <asp:ImageButton Width="20px" ID="ButtonF" runat="server" BorderStyle="None" ImageAlign="Middle" ImageUrl="~/Images/Lettres/F.bmp" OnClick="ButtonLettre_Click"/>
                                    </asp:TableCell>
                                    <asp:TableCell>
                                        <asp:ImageButton Width="20px" ID="ButtonG" runat="server" BorderStyle="None" ImageAlign="Middle" ImageUrl="~/Images/Lettres/G.bmp" OnClick="ButtonLettre_Click"/>
                                    </asp:TableCell>
                                    <asp:TableCell>
                                        <asp:ImageButton Width="20px" ID="ButtonH" runat="server" BorderStyle="None" ImageAlign="Middle" ImageUrl="~/Images/Lettres/H.bmp" OnClick="ButtonLettre_Click"/>
                                    </asp:TableCell>
                                    <asp:TableCell>
                                        <asp:ImageButton Width="20px" ID="ButtonI" runat="server" BorderStyle="None" ImageAlign="Middle" ImageUrl="~/Images/Lettres/I.bmp" OnClick="ButtonLettre_Click"/>
                                    </asp:TableCell>
                                    <asp:TableCell>
                                        <asp:ImageButton Width="20px" ID="ButtonJ" runat="server" BorderStyle="None" ImageAlign="Middle" ImageUrl="~/Images/Lettres/J.bmp" OnClick="ButtonLettre_Click"/>
                                    </asp:TableCell>
                                    <asp:TableCell>
                                        <asp:ImageButton Width="20px" ID="ButtonK" runat="server" BorderStyle="None" ImageAlign="Middle" ImageUrl="~/Images/Lettres/K.bmp" OnClick="ButtonLettre_Click"/>
                                    </asp:TableCell>
                                    <asp:TableCell>
                                        <asp:ImageButton Width="20px" ID="ButtonL" runat="server" BorderStyle="None" ImageAlign="Middle" ImageUrl="~/Images/Lettres/L.bmp" OnClick="ButtonLettre_Click"/>
                                    </asp:TableCell>
                                    <asp:TableCell>
                                        <asp:ImageButton Width="20px" ID="ButtonM" runat="server" BorderStyle="None" ImageAlign="Middle" ImageUrl="~/Images/Lettres/M.bmp" OnClick="ButtonLettre_Click"/>
                                    </asp:TableCell>
                                    <asp:TableCell>
                                        <asp:ImageButton Width="20px" ID="ButtonN" runat="server" BorderStyle="None" ImageAlign="Middle" ImageUrl="~/Images/Lettres/N.bmp" OnClick="ButtonLettre_Click"/>
                                    </asp:TableCell>
                                    <asp:TableCell>
                                        <asp:ImageButton Width="20px" ID="ButtonO" runat="server" BorderStyle="None" ImageAlign="Middle" ImageUrl="~/Images/Lettres/O.bmp" OnClick="ButtonLettre_Click"/>
                                    </asp:TableCell>
                                    <asp:TableCell>
                                        <asp:ImageButton Width="20px" ID="ButtonP" runat="server" BorderStyle="None" ImageAlign="Middle" ImageUrl="~/Images/Lettres/P.bmp" OnClick="ButtonLettre_Click"/>
                                    </asp:TableCell>
                                    <asp:TableCell>
                                        <asp:ImageButton Width="20px" ID="ButtonQ" runat="server" BorderStyle="None" ImageAlign="Middle" ImageUrl="~/Images/Lettres/Q.bmp" OnClick="ButtonLettre_Click"/>
                                    </asp:TableCell>
                                    <asp:TableCell>
                                        <asp:ImageButton Width="20px" ID="ButtonR" runat="server" BorderStyle="None" ImageAlign="Middle" ImageUrl="~/Images/Lettres/R.bmp" OnClick="ButtonLettre_Click"/>
                                    </asp:TableCell>
                                    <asp:TableCell>
                                        <asp:ImageButton Width="20px" ID="ButtonS" runat="server" BorderStyle="None" ImageAlign="Middle" ImageUrl="~/Images/Lettres/S.bmp" OnClick="ButtonLettre_Click"/>
                                    </asp:TableCell>
                                    <asp:TableCell>
                                        <asp:ImageButton Width="20px" ID="ButtonT" runat="server" BorderStyle="None" ImageAlign="Middle" ImageUrl="~/Images/Lettres/T.bmp" OnClick="ButtonLettre_Click"/>
                                    </asp:TableCell>
                                    <asp:TableCell>
                                        <asp:ImageButton Width="20px" ID="ButtonU" runat="server" BorderStyle="None" ImageAlign="Middle" ImageUrl="~/Images/Lettres/U.bmp" OnClick="ButtonLettre_Click"/>
                                    </asp:TableCell>
                                    <asp:TableCell>
                                        <asp:ImageButton Width="20px" ID="ButtonV" runat="server" BorderStyle="None" ImageAlign="Middle" ImageUrl="~/Images/Lettres/V.bmp" OnClick="ButtonLettre_Click"/>
                                    </asp:TableCell>
                                    <asp:TableCell>
                                        <asp:ImageButton Width="20px" ID="ButtonW" runat="server" BorderStyle="None" ImageAlign="Middle" ImageUrl="~/Images/Lettres/W.bmp" OnClick="ButtonLettre_Click"/>
                                    </asp:TableCell>
                                    <asp:TableCell>
                                        <asp:ImageButton Width="20px" ID="ButtonX" runat="server" BorderStyle="None" ImageAlign="Middle" ImageUrl="~/Images/Lettres/X.bmp" OnClick="ButtonLettre_Click"/>
                                    </asp:TableCell>
                                    <asp:TableCell>
                                        <asp:ImageButton Width="20px" ID="ButtonY" runat="server" BorderStyle="None" ImageAlign="Middle" ImageUrl="~/Images/Lettres/Y.bmp" OnClick="ButtonLettre_Click"/>
                                    </asp:TableCell>
                                    <asp:TableCell>
                                        <asp:ImageButton Width="20px" ID="ButtonZ" runat="server" BorderStyle="None" ImageAlign="Middle" ImageUrl="~/Images/Lettres/Z.bmp" OnClick="ButtonLettre_Click"/>
                                    </asp:TableCell>
                                    <asp:TableCell>
                                        <asp:ImageButton Width="20px" ID="ButtonArobase" runat="server" BorderStyle="None" ImageAlign="Middle" ImageUrl="~/Images/Lettres/arobase.bmp" OnClick="ButtonLettre_Click"/>
                                    </asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow Height="25px" VerticalAlign="Middle">
                                    <asp:TableCell HorizontalAlign="Left" ColumnSpan="19">
                                        <asp:Label ID="lblRecherche" runat="server" Text="Recherche par XXXXXXX (Commençant par)" Height="16px" Width="420px" BackColor="Transparent" ForeColor="#D7FAF3" BorderStyle="None" Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Small" Style="margin-top: 0px; margin-right: 5px; font-style: oblique; text-indent: 5px; text-align: right;" />
                                    </asp:TableCell>
                                    <asp:TableCell HorizontalAlign="Left" ColumnSpan="8">
                                        <asp:TextBox ID="txtRecherche" runat="server" Text="" Visible="true" AutoPostBack="true" BackColor="White" BorderColor="#B0E0D7" BorderStyle="InSet" BorderWidth="2px" ForeColor="#124545" Height="16px" Width="220px" MaxLength="35" Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Small" Style="margin-top: 0px; margin-left: 2px; font-style: normal; text-indent: 1px; text-align: left" OnTextChanged="txtRecherche_TextChanged"/>
                                    </asp:TableCell>
                                </asp:TableRow>
                            </asp:Table>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell Width="650px" Height="790px">
                            <asp:Panel ID="PanelTree" runat="server" ScrollBars="Vertical" BackColor="Snow" Wrap="true" Style="margin-top: 0px; vertical-align: top; overflow: hidden; text-align: left">
                                <asp:TreeView ID="TreeListe" runat="server" MaxDataBindDepth="2" BorderStyle="None" BorderWidth="2px" BorderColor="Snow" ForeColor="#142425" ShowCheckBoxes="All" Width="685px" Height="790px" Font-Bold="True" NodeIndent="10" BackColor="Snow" LeafNodeStyle-HorizontalPadding="8px" RootNodeStyle-Font-Bold="True" RootNodeStyle-ImageUrl="~/Images/Icones/DatabaseBleu.bmp" RootNodeStyle-Font-Italic="False">
                                    <SelectedNodeStyle BackColor="#6C9690" BorderColor="#E3924F" BorderStyle="Solid" BorderWidth="1px" ForeColor="White" />
                                </asp:TreeView>
                            </asp:Panel>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell HorizontalAlign="Center">
                            <asp:Label ID="LblStatutListe" runat="server" Height="15px" Width="150px" BackColor="#6D9092" Visible="true" BorderColor="#CCFFFF" BorderStyle="None" BorderWidth="2px" ForeColor="White" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Style="margin-left: 0px; font-style: oblique; text-align: center" />
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
            </asp:TableCell>
        </asp:TableRow>
    </asp:Table>
    <%--<asp:HiddenField ID="HSelLettre" runat="server" Value="" OnValueChanged="HSelLettre_ValueChanged" />--%>
    <asp:HiddenField ID="HDoitRecharger" runat="server" Value="OUI" />
    <asp:HiddenField ID="HButtonLettre" runat="server" Value="" />
</asp:Panel>
