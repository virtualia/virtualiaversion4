﻿Option Strict Off
Option Explicit On
Option Compare Text

Imports Virtualia.OutilsVisu

Public Class V_Message
    Inherits System.Web.UI.UserControl
    Implements IControlMessage

#Region "IControlBase"
    Public Sub Charge(cache As Object) Implements IControlMessage.Charge

        Dim cachetmp As CacheAppelMessage = DirectCast(cache, CacheAppelMessage)

        If (cachetmp Is Nothing) Then
            Return
        End If

        CmdCancel.Visible = False
        CmdOui.Visible = False
        CmdNon.Visible = False
        EtiMsg.Text = ""
        EtiTitre.Text = ""

        For Each bt As BoutonMessage In cachetmp.Boutons
            Select Case bt.TypeCommande
                Case TypeCommandeMessage.CANCEL
                    CmdCancel.Visible = True
                    CmdCancel.Text = IIf(bt.Libelle.Trim() = "", "Annuler", bt.Libelle.Trim())
                Case TypeCommandeMessage.OUI
                    CmdOui.Visible = True
                    CmdOui.Text = IIf(bt.Libelle.Trim() = "", "Oui", bt.Libelle.Trim())
                Case TypeCommandeMessage.NON
                    CmdNon.Visible = True
                    CmdNon.Text = IIf(bt.Libelle.Trim() = "", "Non", bt.Libelle.Trim())
            End Select
        Next

        EtiTitre.Text = cachetmp.Titre

        Dim sep As String = ""
        cachetmp.Messages.ForEach(Sub(msg)
                                      EtiMsg.Text += sep + msg
                                      sep = vbCrLf
                                  End Sub)

        Me.ViewState.AjouteValeur(CacheAppelMessage.KeyState, cachetmp)
    End Sub

    Public ReadOnly Property NomControl As String Implements IControlMessage.NomControl
        Get
            Return Me.ID
        End Get
    End Property

    Protected _ControleurMessage As IControllerMessage
    Public WriteOnly Property ControleurMessage As IControllerMessage Implements IControlMessage.ControleurMessage
        Set(value As IControllerMessage)
            _ControleurMessage = value
        End Set
    End Property
#End Region

#Region "Evenements"
    Protected Sub Cmd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles CmdOui.Click, CmdNon.Click, CmdCancel.Click
        If (_ControleurMessage Is Nothing) Then
            Return
        End If

        If (_ControleurMessage.Act_RetourMessage Is Nothing) Then
            Return
        End If

        GereRetour(DirectCast(sender, Button).Text)
    End Sub
#End Region

#Region "Methodes Privees"
    Private Sub GereRetour(NomBouton As String)

        Dim act As Action(Of CacheRetourMessage) = _ControleurMessage.Act_RetourMessage
        Dim cacheappel As CacheAppelMessage = Me.ViewState.GetValeur(Of CacheAppelMessage)(CacheAppelMessage.KeyState)

        Dim cacheretour As CacheRetourMessage = New CacheRetourMessage()
        If (cacheappel Is Nothing) Then
            cacheretour.Commande = TypeCommandeMessage._AUCUN
            act(cacheretour)
            Return
        End If

        Dim tpcmd As TypeCommandeMessage = (From bt In cacheappel.Boutons Where bt.Libelle = NomBouton Select bt.TypeCommande).First()

        cacheretour.IDControlSaisie = cacheappel.IDControlSaisie
        cacheretour.NomVueSource = cacheappel.NomVueSource
        cacheretour.Commande = tpcmd
        cacheretour.TypeMessage = cacheappel.TypeMessage
        cacheretour.NumObjet = cacheappel.NumObjet

        Me.ViewState.Remove(CacheAppelMessage.KeyState)

        act(cacheretour)

    End Sub
#End Region

End Class