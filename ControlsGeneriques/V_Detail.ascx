﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="V_Detail.ascx.vb" Inherits="Virtualia.Net.V_Detail" %>

<asp:Panel ID="PanelDetail" runat="server" BackColor="#1C5151" HorizontalAlign="Center" Width="320px" Height="940px" BorderStyle="Solid" BorderWidth="1px" BorderColor="#B0E0D7" Font-Names="Trebuchet MS" Font-Italic="true" Style="margin-top: 5px; margin-bottom: 5px">
    <asp:Table ID="TableDetail" runat="server" HorizontalAlign="Center">
        <asp:TableRow>
            <asp:TableCell>
                <%-- 
                Row des :
                    1 - Label Sélection
                    2 - Liste des detail
                    3 - Statut 
                --%>
                <asp:Table ID="Table1" runat="server" Style="margin-top: 10px; width: 300px; height: 845px; background-attachment: inherit; display: table-cell;">
                    <asp:TableRow>
                        <asp:TableCell Width="298px" HorizontalAlign="Center" Height="42px" BackColor="#6D9092">
                            <asp:Label ID="Label1" runat="server" Height="22px" Width="250px" BackColor="Transparent" Visible="true" BorderColor="#CCFFFF" BorderStyle="None" BorderWidth="2px" ForeColor="White" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Style="margin-left: 0px; font-style: oblique; text-align: center" />
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell>
                            <asp:ListBox ID="LstBoxDetail" runat="server" Height="790px" Width="300px" AutoPostBack="true" BackColor="Snow" ForeColor="#142425" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Style="border-color: #CCFFFF; border-width: 2px; border-style: inset; display: table-cell; font-style: normal;" />
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell Width="300px" HorizontalAlign="Center">
                            <asp:Label ID="LblStatutListe" runat="server" Height="15px" Width="150px" BackColor="#6D9092" Visible="true" BorderColor="#CCFFFF" BorderStyle="None" BorderWidth="2px" ForeColor="White" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Style="margin-left: 0px; font-style: oblique; text-align: center" />
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
            </asp:TableCell>
        </asp:TableRow>
    </asp:Table>
</asp:Panel>
