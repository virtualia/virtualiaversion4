﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="V_Menus.ascx.vb" Inherits="Virtualia.Net.V_Menus" %>

<asp:Panel ID="PanelMenus" runat="server" HorizontalAlign="Center" Width="720px" Height="50px" Font-Names="Trebuchet MS" Font-Italic="true">

    <asp:Table ID="CadreMenus" runat="server" HorizontalAlign="Center" BackColor="#1C5151" Width="720px">
        <%-- Row des Menus --%>
        <asp:TableRow VerticalAlign="Middle">
            <asp:TableCell>
                <%--Width="680px"--%>
                <asp:Table ID="TableOnglets" Height="35px"  runat="server" HorizontalAlign="Center">
                    <asp:TableRow>
                        <asp:TableCell HorizontalAlign="Center">
                            <%-- Width="650px" --%>
                            <asp:Menu ID="MenuChoix" runat="server"  DynamicHorizontalOffset="10" Font-Names="Trebuchet MS" Font-Size="Small" BackColor="Transparent" Font-Italic="false" ForeColor="White" Height="32px" Orientation="Horizontal" StaticSubMenuIndent="20px" Font-Bold="True" BorderWidth="2px" StaticEnableDefaultPopOutImage="False" BorderColor="#B0E0D7" BorderStyle="None" >
                                <StaticSelectedStyle BackColor="#B0E0D7" BorderStyle="Solid" Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Small" ForeColor="#000066" />
                                <StaticMenuItemStyle VerticalPadding="2px" HorizontalPadding="10px" Height="24px" BackColor="#0E5F5C" BorderColor="#B0E0D7" BorderStyle="Solid" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" ForeColor="White"/>
                                <StaticHoverStyle BackColor="#7EC8BE" ForeColor="White" BorderColor="#B0E0D7" BorderStyle="Solid" />
                            </asp:Menu>
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
            </asp:TableCell>
        </asp:TableRow>
    </asp:Table>
</asp:Panel>
