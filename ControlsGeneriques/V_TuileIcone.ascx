﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="V_TuileIcone.ascx.vb" Inherits="Virtualia.Net.V_TuileIcone" ClassName="Controles_VCoupleIconeEtiBase"%>

<asp:Table ID="VIcone" runat="server" CellPadding="1" CellSpacing="0" Width="310px" Height="150px" Style="margin-top: 2px" BackColor="Window">
    <asp:TableRow>
        <asp:TableCell HorizontalAlign="Center" VerticalAlign="Top" Width="310px" BorderStyle="None">
            <asp:ImageButton ID="CmdChoix" runat="server" Width="96px" Height="96px" BackColor="Transparent" BorderStyle="None" Style="margin-top: 10px" />
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell VerticalAlign="Bottom" Width="310px" BorderStyle="None">
            <asp:Label ID="Etiquette" runat="server" Height="28px" Width="300px" BackColor="Transparent" BorderColor="#B0E0D7" BorderStyle="None" ForeColor="White" Font-Italic="True" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Style="margin-left: 10px; margin-top: 10px; text-align: left" />
        </asp:TableCell>
    </asp:TableRow>
</asp:Table>
