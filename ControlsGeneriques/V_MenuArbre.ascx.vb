﻿Imports Virtualia.OutilsVisu
Imports System.Collections.Generic

Public Class V_MenuArbre
    Inherits System.Web.UI.UserControl
    Implements IControlMenusArbre


#Region "IControlMenusArbre"
    Private _CmdRetour As ImageButton
    Public WriteOnly Property CmdRetour As ImageButton Implements IControlMenusArbre.CmdRetour
        Set(value As ImageButton)
            _CmdRetour = value
        End Set
    End Property

    Public Sub Charge(source As Object) Implements IControlMenusArbre.Charge

        If (StyleNoeuds.Count <= 0) Then
            Dim st As StyleTreeNode = New StyleTreeNode()
            st.Expanded = True
            st.PopulateOnDemand = False
            st.SelectAction = TreeNodeSelectAction.SelectExpand
            st.ShowCheckBox = False
            st.UrlImage = "~/Images/Armoire/BleuFermer16.bmp"
            StyleNoeuds.Add(st)


            st = New StyleTreeNode()
            st.PopulateOnDemand = False
            st.SelectAction = TreeNodeSelectAction.SelectExpand
            st.ShowCheckBox = False
            st.UrlImage = "~/Images/Armoire/FicheBleue.bmp"
            StyleNoeuds.Add(st)
        End If

        Dim premier As Boolean = True

        For Each st As StyleTreeNode In StyleNoeuds
            If (premier) Then
                st.Propriete_Liee = "Groupe"
                premier = False
            Else
                st.Propriete_Liee = "LibelleMenu"
            End If
        Next

        Dim items As IEnumerable(Of ItemDicoVueBase) = TryCast(source, IEnumerable(Of ItemDicoVueBase))

        If (items Is Nothing) Then
            Return
        End If

        Dim generateur As GenerationVisuArbre(Of ItemDicoVueBase) = New GenerationVisuArbre(Of ItemDicoVueBase)()

        generateur.StyleNoeuds.AddRange(StyleNoeuds)

        Dim listprops As List(Of String) = New List(Of String)()
        listprops.Add("Groupe")
        listprops.Add("LibelleMenu")

        Dim lst As List(Of ItemDicoVueBase) = New List(Of ItemDicoVueBase)(items)

        TreeListeMenu.Nodes.AddRange(generateur.GetArbre(lst, listprops))
    End Sub

    Public ReadOnly Property NomControl As String Implements IControlMenusArbre.NomControl
        Get
            Return Me.ID
        End Get
    End Property

    Public Sub EndInit() Implements IControlMenusArbre.EndInit

        If (_ControllerMenu Is Nothing) OrElse (_ControllerMenu.Act_ClickMenu Is Nothing) Then
            Return
        End If

        'On affecte l'evenement click
        AddHandler TreeListeMenu.SelectedNodeChanged, AddressOf TreeListe_SelectedNodeChanged

        'On affecte le click du bouton retour
        If Not (_CmdRetour Is Nothing) Then
            AddHandler _CmdRetour.Click, AddressOf Me.ClickRetour
        End If

        ReInit(True)

    End Sub

    Public Sub ReInit(reinit As Boolean) Implements IControlMenusArbre.ReInit

        If Not (reinit) Then
            Return
        End If

        Dim premiermenu As TreeNode = Nothing

        For Each n As TreeNode In TreeListeMenu.Nodes
            If (n.ChildNodes.Count > 0) Then
                premiermenu = n.ChildNodes(0)
                Exit For
            End If
        Next

        If (premiermenu Is Nothing) Then
            Return
        End If

        premiermenu.Select()

        TreeListe_SelectedNodeChanged(TreeListeMenu, Nothing)
    End Sub
#End Region

#Region "Proprietes"
    Private _ControllerMenu As IControllerMenu
    Public WriteOnly Property ControllerMenu As IControllerMenu Implements IControlMenusArbre.ControllerMenu
        Set(value As IControllerMenu)
            _ControllerMenu = value
        End Set
    End Property

    Public ReadOnly Property StyleNoeuds As ListStyleTreeNodeCollection
        Get
            If Not (Me.ViewState.KeyExiste("STYLENOEUD")) Then
                Me.ViewState.AjouteValeur("STYLENOEUD", New ListStyleTreeNodeCollection())
            End If

            Return Me.ViewState.GetValeur(Of ListStyleTreeNodeCollection)("STYLENOEUD")
        End Get
    End Property
#End Region

#Region "Methodes privees"
    Private Function EstOk() As Boolean
        Return (TreeListeMenu.Nodes.Count > 0 And (Not (_ControllerMenu Is Nothing) AndAlso Not (_ControllerMenu.Act_ClickMenu Is Nothing)))
    End Function

    Private Sub TreeListe_SelectedNodeChanged(sender As Object, e As EventArgs)

        If (_ControllerMenu Is Nothing) OrElse (_ControllerMenu.Act_ClickMenu Is Nothing) Then
            Return
        End If

        Dim treemenu As TreeView = DirectCast(sender, TreeView)

        If (treemenu.SelectedNode Is Nothing) Then
            Return
        End If

        If (treemenu.SelectedNode.Parent Is Nothing) Then
            Return
        End If

        Dim cacheclick As CacheClickMenu = New CacheClickMenu()
        cacheclick.TypeClick = TypeClickMenu.VUE
        cacheclick.TypeSource = TypeSourceMenu.V_MenuArbre
        cacheclick.Value = treemenu.SelectedNode.Parent.Text & "#" & treemenu.SelectedNode.Text

        Dim act As Action(Of CacheClickMenu) = _ControllerMenu.Act_ClickMenu
        act(cacheclick)

    End Sub

    Private Sub ClickRetour(sender As Object, e As EventArgs)
        Dim cacheclick As CacheClickMenu = New CacheClickMenu()
        cacheclick.TypeClick = TypeClickMenu.RETOUR

        Dim act As Action(Of CacheClickMenu) = _ControllerMenu.Act_ClickMenu
        act(cacheclick)
    End Sub
#End Region

End Class