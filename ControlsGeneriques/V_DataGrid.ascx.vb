﻿Option Strict Off
Option Explicit On
Option Compare Text

Imports System.Drawing
Imports Virtualia.OutilsVisu
Imports Virtualia.Systeme.Datas
Imports VI = Virtualia.Systeme.Constantes
Imports System.Collections.Generic
Imports System.Web.UI

Public Class V_DataGrid
    Inherits UserControl
    Implements IControlDataGrid

#Region "Proprietes de design"
    Public WriteOnly Property CadreWidth As Unit
        Set(ByVal value As Unit)
            GridDonnee.Width = value
        End Set
    End Property
    Public WriteOnly Property BackColorCaption As Color
        Set(ByVal value As Color)
            GridDonnee.BackColor = value
            GridDonnee.HeaderStyle.BackColor = value
        End Set
    End Property
    Public WriteOnly Property ForeColorCaption As Color
        Set(ByVal value As Color)
            GridDonnee.ForeColor = value
            GridDonnee.HeaderStyle.ForeColor = value
        End Set
    End Property
    Public WriteOnly Property BackColorRow As Color
        Set(ByVal value As Color)
            GridDonnee.RowStyle.BackColor = value
        End Set
    End Property
    Public WriteOnly Property ForeColorRow As Color
        Set(ByVal value As Color)
            GridDonnee.RowStyle.ForeColor = value
        End Set
    End Property
#End Region

#Region "Proprietes Fonctionnelles"
    Public WriteOnly Property SiCaseAcocher As Boolean
        Set(ByVal value As Boolean)
            CaseACocher.Visible = value
            If Not (value) Then
                CacheCourant.ValeurCoche = True
            End If
        End Set
    End Property
    Public WriteOnly Property TexteCaseACocher As String
        Set(ByVal value As String)
            CaseACocher.Text = value
        End Set
    End Property

    Public WriteOnly Property CaseDetailCochee As Boolean
        Set(ByVal value As Boolean)
            CaseACocher.Checked = value
            CacheCourant.ValeurCoche = True
            CacheCourant.DoitRecharger = True
            Me.ViewState.AjouteValeur(CacheDataGridCoche.KeyState, CacheCourant)
        End Set
    End Property
    Public WriteOnly Property ValeurCoche As Boolean
        Set(value As Boolean)
            CacheCourant.ValeurCoche = value
            CacheCourant.DoitRecharger = True
            Me.ViewState.AjouteValeur(CacheDataGridCoche.KeyState, CacheCourant)
        End Set
    End Property

    'Ca ne doit servire a rien
    Public WriteOnly Property SiCaptionVisible As Boolean
        Set(ByVal value As Boolean)
            CacheCourant.DesignCaption.CaptionVisible = value
            Me.ViewState.AjouteValeur(CacheDataGridCoche.KeyState, CacheCourant)
        End Set
    End Property
    Public WriteOnly Property NatureCaseACocher As Integer
        Set(ByVal value As Integer)
            CacheCourant.DoitRecharger = True

            If (value = 0) Then
                CacheCourant.NatureCaseACocher = TypeDonneeDataGrid.Data
                Me.ViewState.AjouteValeur(CacheDataGridCoche.KeyState, CacheCourant)
                Return
            End If

            CacheCourant.NatureCaseACocher = TypeDonneeDataGrid.Erreur
            Me.ViewState.AjouteValeur(CacheDataGridCoche.KeyState, CacheCourant)

        End Set
    End Property
    Public WriteOnly Property SiColonneSelect As Boolean
        Set(ByVal value As Boolean)
            GridDonnee.AutoGenerateSelectButton = value
        End Set
    End Property

    Public WriteOnly Property SiPagination As Boolean
        Set(ByVal value As Boolean)
            GridDonnee.AllowPaging = value
            If value Then
                GridDonnee.PagerSettings.Mode = PagerButtons.Numeric
                GridDonnee.PagerSettings.Position = PagerPosition.TopAndBottom

                GridDonnee.PagerStyle.BackColor = VisuHelper.ConvertiCouleur("#OE5F5C")
                GridDonnee.PagerStyle.ForeColor = VisuHelper.ConvertiCouleur("#D7FAF3")
            End If
        End Set
    End Property
    Public WriteOnly Property TaillePage As Integer
        Set(ByVal value As Integer)
            GridDonnee.PageSize = value
        End Set
    End Property
    Public ReadOnly Property TotalLignes As Integer Implements IControlDataGrid.TotalLignes
        Get
            Return CacheCourant.Donnees.Count
        End Get
    End Property

    Public WriteOnly Property Donnees As String Implements IControlDataGrid.Donnees
        Set(ByVal value As String)
            CacheCourant.Donnees = CacheItemCollection.ToCollection(value, VI.SigneBarre, VI.Tild)
            CacheCourant.DoitRecharger = True
            Me.ViewState.AjouteValeur(CacheDataGridCoche.KeyState, CacheCourant)
        End Set
    End Property

    Public ReadOnly Property Colonnes As ColonneDataGridCollection Implements IControlDataGrid.Colonnes
        Get
            Return CacheCourant.Colonnes
        End Get
    End Property

    Private _ControleurDataGrid As IControllerSelection
    Public WriteOnly Property ControleurDataGrid As IControllerSelection Implements IControlDataGrid.ControleurDataGrid
        Set(value As IControllerSelection)
            _ControleurDataGrid = value
        End Set
    End Property

    Public WriteOnly Property IndexFicheCourante As Integer Implements IControlDataGrid.IndexFicheCourante
        Set(ByVal value As Integer)
            CacheCourant.IndexFicheCourante = value
            Me.ViewState.AjouteValeur(CacheDataGridCoche.KeyState, CacheCourant)
        End Set
    End Property
#End Region

#Region "Proprietes privees"
    Private ReadOnly Property CacheCourant As CacheDataGridCoche
        Get
            If Not (Me.ViewState.KeyExiste(CacheDataGridCoche.KeyState)) Then
                Me.ViewState.Add(CacheDataGridCoche.KeyState, New CacheDataGridCoche())
            End If

            Return Me.ViewState.GetValeur(Of CacheDataGridCoche)(CacheDataGridCoche.KeyState)

        End Get
    End Property
#End Region

#Region "OnInit / OnPreRender"
    Protected Overrides Sub OnInit(e As EventArgs)
        MyBase.OnInit(e)
    End Sub
    Protected Overrides Sub OnPreRender(e As EventArgs)
        MyBase.OnPreRender(e)

        If (CacheCourant.DoitRecharger) Then
            CacheCourant.DoitRecharger = False
            Me.ViewState.AjouteValeur(CacheDataGridCoche.KeyState, CacheCourant)
            FaireListe()
        End If
    End Sub
#End Region

#Region "Methodes publics"
    Public Sub InitCaptions(lstCaptions As List(Of String))
        If (lstCaptions Is Nothing) Then
            CacheCourant.DesignCaption.CaptionVisible = False
        End If
        If (lstCaptions.Count < 3) Then
            CacheCourant.DesignCaption.CaptionVisible = False
        End If

        CacheCourant.DesignCaption.Libelles.Clear()
        CacheCourant.DesignCaption.Libelles.AddRange(lstCaptions)

        Me.ViewState.AjouteValeur(CacheDataGridCoche.KeyState, CacheCourant)

    End Sub
    Public Sub InitColonnes(colonnes As ColonneDataGridCollection)

        If (Me.IsPostBack) Then
            Return
        End If

        If (colonnes Is Nothing) Then
            Return
        End If

        If (colonnes.Count <= 0) Then
            Return
        End If

        Dim bnd As BoundField
        Dim datafield As String

        For Each cl As ColonneDataGrid In colonnes

            datafield = "Colonne_" & cl.Num

            bnd = New BoundField()

            bnd.DataField = datafield
            bnd.HeaderText = cl.Libelle
            bnd.ItemStyle.HorizontalAlign = cl.Alignement

            GridDonnee.Columns.Add(bnd)

            If (cl.EstClef) Then
                GridDonnee.DataKeyNames = New String() {datafield}
                bnd.Visible = False
            End If
        Next

        CacheCourant.Colonnes = colonnes

        Me.ViewState.AjouteValeur(CacheDataGridCoche.KeyState, CacheCourant)

    End Sub
    Public Function GetTableau(sep As String) As List(Of String)

        Dim ret As List(Of String) = New List(Of String)()

        If (CacheCourant.Donnees Is Nothing) Then
            Return ret
        End If

        If (CacheCourant.Donnees.Count <= 0) Then
            Return ret
        End If

        Dim lgchaine = ""
        Dim separateur = ""
        CacheCourant.Donnees.ForEach(Sub(lg)
                                         lgchaine = ""
                                         lg.ForEach(Sub(ch)
                                                        If (ColonneDataGridCollection.ColonneVisible(CacheCourant.Colonnes, ch.NumDonnee)) Then
                                                            lgchaine &= separateur & ch.valeur
                                                            separateur = sep
                                                        End If
                                                    End Sub)
                                         ret.Add(lgchaine)
                                     End Sub)

        Return ret

    End Function
#End Region

#Region "Evenements"
    Protected Sub CaseACocher_CheckedChanged(sender As Object, e As EventArgs)
        CacheCourant.ValeurCoche = (DirectCast(sender, CheckBox).Checked)
        CacheCourant.DoitRecharger = True
        Me.ViewState.AjouteValeur(CacheDataGridCoche.KeyState, CacheCourant)
    End Sub
    Protected Sub GridDonnee_PageIndexChanging(sender As Object, e As GridViewPageEventArgs)
        If e.NewPageIndex >= 0 And e.NewPageIndex < GridDonnee.PageCount Then
            CacheCourant.DoitRecharger = True
            CacheCourant.NumPage = e.NewPageIndex
            Me.ViewState.AjouteValeur(CacheDataGridCoche.KeyState, CacheCourant)
        End If
    End Sub
    Protected Sub GridDonnee_PageIndexChanged(sender As Object, e As EventArgs)
        GridDonnee.PageIndex = CacheCourant.NumPage
        Me.ViewState.AjouteValeur(CacheDataGridCoche.KeyState, CacheCourant)
    End Sub
    Protected Sub GridDonnee_SelectedIndexChanged(sender As Object, e As EventArgs)

        If (GridDonnee.SelectedDataKey Is Nothing) Then
            Return
        End If

        If (GridDonnee.SelectedDataKey.Values.Count <= 0) Then
            Return
        End If

        If (_ControleurDataGrid Is Nothing) Then
            Return
        End If

        If (_ControleurDataGrid.Act_SelectDataDansDataGrid Is Nothing) Then
            Return
        End If

        Dim act As Action(Of String) = _ControleurDataGrid.Act_SelectDataDansDataGrid
        act(GridDonnee.SelectedDataKey.Item(0).ToString())
    End Sub
#End Region

#Region "Methodes privees"
    Private Sub FaireListe()

        Dim datas As List(Of ObjetIlistGrid) = New List(Of ObjetIlistGrid)

        If (CacheCourant.Donnees Is Nothing) Then
            datas.Add(New ObjetIlistGrid())
            GridDonnee.DataSource = datas
            GridDonnee.DataBind()
            GridDonnee.Caption = CacheCourant.DesignCaption.GetCaption(0)
            GridDonnee.SelectedIndex = -1
            Return
        End If

        If (CacheCourant.Donnees.Count <= 0) Then
            datas.Add(New ObjetIlistGrid())
            GridDonnee.DataSource = datas
            GridDonnee.DataBind()
            GridDonnee.Caption = CacheCourant.DesignCaption.GetCaption(0)
            GridDonnee.SelectedIndex = -1
            Return
        End If

        Dim objlst As ObjetIlistGrid = Nothing

        For Each cit As CacheItem In CacheCourant.Donnees

            If (CacheCourant.ValeurCoche) Then

                Select Case CacheCourant.NatureCaseACocher
                    Case TypeDonneeDataGrid.Data
                        objlst = ChargeDonneeData(cit)
                    Case TypeDonneeDataGrid.Erreur
                        objlst = ChargeDonneeErreur_900000(cit)
                        If (objlst Is Nothing) Then
                            Continue For
                        End If
                End Select

            Else
                Select Case CacheCourant.NatureCaseACocher
                    Case TypeDonneeDataGrid.Data
                        objlst = ChargeDonneeErreur_100000(cit)
                        If (objlst Is Nothing) Then
                            Continue For
                        End If
                    Case Else
                        objlst = ChargeDonneeData(cit)
                End Select
            End If

            datas.Add(objlst)
        Next

        GridDonnee.DataSource = datas
        GridDonnee.DataBind()

        GridDonnee.Caption = CacheCourant.DesignCaption.GetCaption(datas.Count)

        If (CacheCourant.IndexFicheCourante >= 0) Then
            GridDonnee.SelectedIndex = CacheCourant.Donnees.GetIndexRow(CacheCourant.IndexFicheCourante, CacheCourant.Colonnes.GetColumnClef())
        Else
            GridDonnee.SelectedIndex = 0
        End If

        GridDonnee_SelectedIndexChanged(Nothing, Nothing)
    End Sub

    Private Function ChargeDonneeData(dons As CacheItem) As ObjetIlistGrid
        Dim objlst = New ObjetIlistGrid()

        For Each col As ColonneDataGrid In CacheCourant.Colonnes
            objlst.GetType().GetProperty("Colonne_" & col.Num).SetValue(objlst, dons(col.Num - 1).valeur, Nothing)
        Next
        Return objlst
    End Function
    Private Function ChargeDonneeErreur_900000(donnees As CacheItem) As ObjetIlistGrid

        Dim derniere_donnee As CacheDonnee = donnees.Last()

        If Not (derniere_donnee.valeur.Contains("_")) Then
            Return Nothing
        End If

        Dim lst As List(Of String) = Strings.Split(derniere_donnee.valeur, "_", -1).ToList()

        If Not (IsNumeric(lst(0))) Then
            Return Nothing
        End If

        If (Integer.Parse(lst(0)) < 900000) Then
            Return Nothing
        End If

        Dim objlst = New ObjetIlistGrid()

        donnees.ForEach(Sub(don)
                            objlst.Valeur(don.NumDonnee) = don.valeur
                        End Sub)

        Return objlst

    End Function
    Private Function ChargeDonneeErreur_100000(donnees As CacheItem) As ObjetIlistGrid

        Dim derniere_donnee As CacheDonnee = donnees.Last()

        If Not (IsNumeric(derniere_donnee.valeur)) Then
            Return Nothing
        End If

        If (Integer.Parse(derniere_donnee.valeur) < 100000) Then
            Return Nothing
        End If

        Dim objlst As ObjetIlistGrid = New ObjetIlistGrid()

        donnees.ForEach(Sub(don)
                            objlst.Valeur(don.NumDonnee) = don.valeur
                        End Sub)

        Return objlst

    End Function
#End Region

End Class