﻿Imports Virtualia.ObjetBase

Public Class V_CommandeOK
    Inherits ObjetCommandeOKBase

    Protected Overrides ReadOnly Property V_CadreCommandeOK As Table
        Get
            Return Me.CadreCmdOK
        End Get
    End Property
    Protected Overrides ReadOnly Property V_CommandeOK As Button
        Get
            Return Me.CommandeOK
        End Get
    End Property
End Class