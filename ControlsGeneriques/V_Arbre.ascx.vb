﻿Option Strict On
Option Explicit On
Option Compare Text

Imports Virtualia.OutilsVisu
Imports System.ComponentModel
Imports System.Collections.ObjectModel
Imports Virtualia.TablesObjet.ShemaREF
Imports System.Reflection

Public Class V_Arbre
    Inherits System.Web.UI.UserControl
    Implements IControlArbre

#Region "Variables"
    Private _ControllerArbre As IControllerArbre

    Private _lstStyleNoeuds As ListStyleTreeNodeCollection
    Private _filtreLibelle As ItemFiltreChaine
    Private _lstItemsArmoires As ListItemArmoireCollection

    Private _proprietederecherche As String
#End Region

#Region "Constructeur"
    Public Sub New()
        MyBase.New()
    End Sub
#End Region

#Region "OnInit / OnPreRender"
    Protected Overrides Sub OnInit(e As EventArgs)
        MyBase.OnInit(e)
    End Sub

    Protected Overrides Sub OnPreRender(e As EventArgs)
        MyBase.OnPreRender(e)

        InitVariable()
    End Sub
#End Region

#Region "Proprietes"
    Public WriteOnly Property ControllerArbre As IControllerArbre Implements IControlArbre.ControllerArbre
        Set(value As IControllerArbre)
            _ControllerArbre = value
        End Set
    End Property

    Public ReadOnly Property StyleNoeuds As ListStyleTreeNodeCollection Implements IControlArbre.StyleNoeuds
        Get
            If (_lstStyleNoeuds Is Nothing) Then
                _lstStyleNoeuds = New ListStyleTreeNodeCollection()
            End If

            Return _lstStyleNoeuds
        End Get
    End Property

    Public ReadOnly Property ArmoireItems As ListItemArmoireCollection Implements IControlArbre.ArmoireItems
        Get
            If (_lstItemsArmoires Is Nothing) Then
                _lstItemsArmoires = New ListItemArmoireCollection()
                '_lstItemsArmoires.OnAdd = AddressOf Me.OnAddItemArmoire
            End If

            Return _lstItemsArmoires
        End Get
    End Property

    Public WriteOnly Property LibelleRecherche As String Implements IControlArbre.LibelleRecherche
        Set(value As String)
            lblRecherche.Text = value
        End Set
    End Property

    Public WriteOnly Property ProprieteDeRecherche As String Implements IControlArbre.ProprieteDeRecherche
        Set(value As String)
            _proprietederecherche = value
        End Set
    End Property
#End Region

#Region "Methodes publiques"
    Public Sub InitVariable() Implements IControlArbre.InitVariable

        If (Me.Page.IsPostBack) Then

            '_filtreLibelle As ItemFiltreChaine
            _filtreLibelle = DirectCast(GenereItemFiltre.Genere(Me.ViewState.GetValeur(Of String)("FILTRE")), ItemFiltreChaine)

            '_lstStyleNoeuds As ListStyleTreeNodeCollection
            _lstStyleNoeuds = Me.ViewState.GetValeur(Of ListStyleTreeNodeCollection)("STYLENOEUD")

            '_ItemsArmoires As ListItemArmoireCollection
            _lstItemsArmoires = Me.ViewState.GetValeur(Of ListItemArmoireCollection)("ITEMSARMOIRES")

            _proprietederecherche = Me.ViewState.GetValeur(Of String)("PROPRIETERECHERCHE")
        End If
    End Sub

    Public Sub Charge(Of T As Class)() Implements IControlArbre.Charge

        If (Me.HDoitRecharger.Value <> "OUI") Then Return

        Me.HDoitRecharger.Value = "NON"

        TreeListe.Nodes.Clear()

        Dim val As String = ItemArmoire.GetValue(CboTypeArmoire.SelectedItem)

        If (String.IsNullOrWhiteSpace(val) Or String.IsNullOrEmpty(val)) Then
            Return
        End If

        Dim donnees As List(Of T) = TryCast(_ControllerArbre.Donnees, List(Of T))

        If (donnees Is Nothing) Then
            Return
        End If

        Dim props As List(Of String) = _lstItemsArmoires(val).Proprietes

        Dim generateur As GenerationVisuArbre(Of T) = New GenerationVisuArbre(Of T)()
        generateur.StyleNoeuds.AddRange(_lstStyleNoeuds)

        Dim predicat As Func(Of T, Boolean) = Nothing
        If Not (String.IsNullOrEmpty(_proprietederecherche)) And _
           Not (String.IsNullOrWhiteSpace(_proprietederecherche)) And _
           Not (String.IsNullOrEmpty(_filtreLibelle.BorneDebut)) Then

            Dim p As PropertyInfo = VisuHelper.GetPropertyInfo(Of T)(_proprietederecherche)

            If Not (p Is Nothing) Then
                predicat = Function(it)
                               Dim valeur As String = p.GetValue(it, Nothing).ToString()

                               Dim ret As Boolean = _filtreLibelle.Evalue(valeur)

                               Return ret
                           End Function
            End If
        End If

        Try
            Dim tests As List(Of TreeNode) = generateur.GetArbre(donnees, props, predicat)
            TreeListe.Nodes.AddRange(tests)
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Public Sub EndInitVariables() Implements IControlArbre.EndInitVariables
        Me.ViewState.AjouteValeur("ITEMSARMOIRES", _lstItemsArmoires)
        Me.ViewState.AjouteValeur("STYLENOEUD", _lstStyleNoeuds)
        Me.ViewState.AjouteValeur("PROPRIETERECHERCHE", _proprietederecherche)

        If (_lstItemsArmoires.Count > 0) Then
            _lstItemsArmoires.ForEach(Sub(it)
                                          CboTypeArmoire.Items.Add(ItemArmoire.GetListItem(it))
                                      End Sub)
        End If

        InitValeurParDefaut()
    End Sub

    Public Sub ForcerLeChargement() Implements IControlArbre.ForcerLeChargement
        Me.HDoitRecharger.Value = "OUI"
    End Sub

    Public Sub EndInit() Implements IControlArbre.EndInit

        If Not (_ControllerArbre Is Nothing) AndAlso Not (_ControllerArbre.Act_NoeudChange Is Nothing) Then
            AddHandler TreeListe.SelectedNodeChanged, AddressOf Me.TreeListe_SelectedNodeChanged
        End If
    End Sub
#End Region

#Region "Evenements"
    Private Sub TreeListe_SelectedNodeChanged(sender As Object, e As EventArgs)

        If (_ControllerArbre Is Nothing) Then
            Return
        End If

        Dim arbre As TreeView = DirectCast(sender, TreeView)

        Dim n_selectionne As TreeNode = arbre.SelectedNode

        Dim AutreNoeud_Coches As List(Of TreeNode) = (From n In arbre.CheckedNodes Where Not (n.Equals(n_selectionne)) Select DirectCast(n, TreeNode)).ToList()
        AutreNoeud_Coches.ForEach(Sub(it)
                                      it.Checked = False
                                  End Sub)

        If Not (n_selectionne.Checked) Then
            n_selectionne.Checked = True
        End If

        If Not (_ControllerArbre.Act_NoeudChange Is Nothing) Then
            Dim act As Action(Of TreeNode, String) = _ControllerArbre.Act_NoeudChange
            act(n_selectionne, ItemArmoire.GetValue(CboTypeArmoire.SelectedItem))
        End If
    End Sub

    Protected Sub TreeListe_TreeNodeCheckChanged(sender As Object, e As TreeNodeEventArgs)

    End Sub

    Protected Sub CboTypeArmoire_SelectedIndexChanged(sender As Object, e As EventArgs)

        Dim EstFiltreVisible As Boolean = ItemArmoire.GetMontreFiltre(DirectCast(sender, DropDownList).SelectedItem)

        If EstFiltreVisible Then
            If (HButtonLettre.Value = "") Then
                ButtonLettre_Click(Me.ButtonA, Nothing)
            Else
                Dim idbutton As String = HButtonLettre.Value
                HButtonLettre.Value = ""
                ButtonLettre_Click(VisuHelper.GetImageBoutonLettre(RowLettres, idbutton), Nothing)
            End If
        Else
            InitFiltre("")
        End If

        Me.CelluleLettre.Visible = EstFiltreVisible
    End Sub

    Protected Sub ButtonLettre_Click(sender As Object, e As ImageClickEventArgs)

        txtRecherche.Text = ""

        Dim Button As ImageButton = DirectCast(sender, ImageButton)
        Dim id As String = Button.ID

        If (HButtonLettre.Value = id) Then
            Return
        End If

        Dim Lettre As String = (id.ToLower().Split(New String() {"button"}, StringSplitOptions.RemoveEmptyEntries))(0).ToUpper()

        If (Lettre = "AROBASE") Then
            InitFiltre("")
        Else
            InitFiltre(Lettre)
        End If

        SelOuNon_BoutonLettre(id, Lettre + "_Sel")

        If (HButtonLettre.Value.Trim() <> "") Then
            Dim Lettre_Anc = (HButtonLettre.Value.ToLower().Split(New String() {"button"}, StringSplitOptions.RemoveEmptyEntries))(0).ToUpper()
            SelOuNon_BoutonLettre(HButtonLettre.Value, Lettre_Anc)
        End If

        HButtonLettre.Value = id
    End Sub

    Protected Sub txtRecherche_TextChanged(sender As Object, e As EventArgs)
        InitFiltre(DirectCast(sender, TextBox).Text, TypeOperateurChaine.CONTIENT)
    End Sub
#End Region

#Region "Methodes privees"
    Private Sub InitFiltre(valeur As String)
        InitFiltre(valeur, TypeOperateurChaine.COMMENCEPAR)
    End Sub
    Private Sub InitFiltre(valeur As String, op As TypeOperateurChaine)
        If (_filtreLibelle Is Nothing) Then
            _filtreLibelle = New ItemFiltreChaine()
            _filtreLibelle.Comparaison = op
        End If

        _filtreLibelle.BorneDebut = valeur

        Me.ViewState.AjouteValeur("FILTRE", _filtreLibelle.GetChaineSauvegarde())

        Me.HDoitRecharger.Value = "OUI"

    End Sub
    Private Sub SelOuNon_BoutonLettre(id As String, TypeImage As String)

        Dim bouton As ImageButton = VisuHelper.GetImageBoutonLettre(RowLettres, id)

        If (bouton Is Nothing) Then
            Return
        End If

        bouton.ImageUrl = "~/Images/Lettres/" & TypeImage & ".bmp"

    End Sub
    Private Sub InitValeurParDefaut()
        If (_lstItemsArmoires.Count <= 0) Then
            Me.CelluleLettre.Visible = False
            InitFiltre("")
            Return
        End If

        Dim it As ItemArmoire = _lstItemsArmoires.First()
        Dim lettre As String
        If (it.AfficheFitre) Then
            Me.CelluleLettre.Visible = True

            If (it.FiltreDefaut = ValeurDefaut._TOUS) Then
                lettre = "Arobase"
                InitFiltre("")
            Else
                lettre = [Enum].GetName(GetType(ValeurDefaut), it.FiltreDefaut)
                InitFiltre(lettre)
            End If

            'SelOuNon_BoutonLettre("button" & lettre, lettre & "_sel")
            'HButtonLettre.Value = ""
        Else
            InitFiltre("")
        End If
    End Sub
#End Region

    
End Class