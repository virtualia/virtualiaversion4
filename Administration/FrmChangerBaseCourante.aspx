﻿<%@ Page Language="VB" MasterPageFile="~/PagesMaitre/VirtualiaMain.master" AutoEventWireup="false" 
    Inherits="Virtualia.Net.Administration_FrmChangerBaseCourante" Codebehind="FrmChangerBaseCourante.aspx.vb"
    MaintainScrollPositionOnPostback="True" %>
<%@ MasterType VirtualPath="~/PagesMaitre/VirtualiaMain.master"  %>

<%@ Register src="../ControlesWebUser/Administration/ChangementBase.ascx" tagname="ChangementBase" tagprefix="Virtualia" %>

<asp:Content ID="CadreGeneral" runat="server" ContentPlaceHolderID="CorpsMaster">      
   <asp:Table ID="CadreChangerBD" runat="server" Height="300px" Width="1150px" HorizontalAlign="Center"
             BackColor="#A8BBB8" CellSpacing="2"
             style="margin-top: 0px " >
       <asp:TableRow>
           <asp:TableCell ID="ChangerBD" Height="300px" Width="1000px" VerticalAlign="Top" HorizontalAlign="Center">
               <Virtualia:ChangementBase ID="BaseCourante" runat="server" />  
          </asp:TableCell>
      </asp:TableRow>
   </asp:Table>
</asp:Content>