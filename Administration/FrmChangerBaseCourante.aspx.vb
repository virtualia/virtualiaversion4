﻿
Partial Class Administration_FrmChangerBaseCourante
    Inherits System.Web.UI.Page

    Protected Sub BaseCourante_MessageSaisie(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.MessageSaisieEventArgs) Handles BaseCourante.MessageSaisie
        Dim Eti As System.Web.UI.WebControls.Label
        Dim Lnk As System.Web.UI.WebControls.HyperLink
        Dim Webfct As New Virtualia.Net.WebFonctions(Me, 1)
        Eti = CType(Webfct.VirWebControle(Me.Master, "MsgSaisie", 0), System.Web.UI.WebControls.Label)
        Lnk = CType(Webfct.VirWebControle(Me.Master, "LienDatabase", 0), System.Web.UI.WebControls.HyperLink)

        Select Case e.NatureMessage
            Case Is = "OK"
                If Eti IsNot Nothing Then
                    Eti.ForeColor = Drawing.Color.Green
                    Eti.Text = e.ContenuMessage
                End If
                If Lnk IsNot Nothing Then
                    Lnk.Text = Webfct.PointeurUtilisateur.InstanceBd.Intitule
                End If
            Case Is = "KO"
                If Eti IsNot Nothing Then
                    Eti.ForeColor = Drawing.Color.Red
                    Eti.Text = e.ContenuMessage
                End If
        End Select
        Response.Redirect("~/Fenetres/FrmInfosPersonnelles.aspx?IDVirtualia=" & Me.Master.V_NoSession)

    End Sub

    Private Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        Dim NumSession As String = Me.Master.V_NoSession
        If NumSession = "" Then
            NumSession = Server.HtmlDecode(Request.QueryString("IDVirtualia"))
            Me.Master.V_NoSession = NumSession
        End If
        If NumSession = "" Then
            Dim Msg As String = "Erreur lors de l'identification."
            Response.Redirect("~/Fenetres/Connexion/ErreurApplication.aspx?Msg=" & Msg)
        End If
    End Sub
End Class
