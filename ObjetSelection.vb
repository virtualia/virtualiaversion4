﻿Option Strict On
Option Explicit On
Option Compare Text
Imports Microsoft.VisualBasic
Imports VI = Virtualia.Systeme.Constantes
Namespace Datas
    Public Class ObjetSelection
        Implements IDisposable
        Private disposedValue As Boolean = False        ' Pour détecter les appels redondants
        Private WsPointdeVue As Short
        Private AppObjetUser As Virtualia.Net.Session.ObjetSession
        Private WsListeResultat As List(Of Virtualia.Ressources.Datas.ItemSelection)
        '
        Private WsNumObjet As Short
        Private WsListeRequetes As ArrayList
        '** Analyse Experte **
        Private WsSiExperteCMC As Boolean
        Private WsInfoExperte As Virtualia.Systeme.MetaModele.Expertes.InformationExperte
        Private WsOperateurComparaison As Short
        Private WsValeursComparaison As String
        '
        Private WsTabIde() As Integer 'Préselection d'identifiants

        Public ReadOnly Property ListeResultat() As List(Of Virtualia.Ressources.Datas.ItemSelection)
            Get
                Return WsListeResultat
            End Get
        End Property

        Public ReadOnly Property NombredeDossiers() As Integer
            Get
                Return WsListeResultat.Count
            End Get
        End Property

        Public ReadOnly Property ListeIdentifiants() As Integer()
            Get
                If WsListeResultat Is Nothing Then
                    Return Nothing
                End If
                If WsListeResultat.Count = 0 Then
                    Return Nothing
                End If
                Dim TabIde As Integer()
                Dim ArrayIde As New ArrayList
                Dim I As Integer
                Dim IdeCourant As Integer = 0
                For I = 0 To WsListeResultat.Count - 1
                    If WsListeResultat.Item(I).Identifiant <> IdeCourant Then
                        ArrayIde.Add(WsListeResultat.Item(I).Identifiant)
                        IdeCourant = WsListeResultat.Item(I).Identifiant
                    End If
                Next I
                ReDim TabIde(ArrayIde.Count - 1)
                For I = 0 To ArrayIde.Count - 1
                    TabIde(I) = CInt(ArrayIde(I))
                Next I
                Return TabIde
            End Get
        End Property

        Public ReadOnly Property RequeteExperte(ByVal NoObjet As Short, ByVal IdeExperte As Short, ByVal TabIde() As Integer, ByVal DateDebut As String, ByVal DateFin As String) As Integer
            Get
                If TabIde Is Nothing Then
                    Return 0
                End If
                Dim I As Integer
                Dim Result As String

                WsNumObjet = NoObjet
                If TabIde.Count < 500 Then
                    Dim ChaineIde As New System.Text.StringBuilder
                    For I = 0 To TabIde.Count - 1
                        ChaineIde.Append(TabIde(I).ToString & VI.PointVirgule)
                    Next I
                    AppObjetUser.PointeurDllExpert.PreselectiondeDossiers = ChaineIde.ToString
                Else
                    AppObjetUser.PointeurDllExpert.PreselectiondeDossiers = ""
                End If
                AppObjetUser.PointeurDllExpert.ClasseExperte(WsPointdeVue, NoObjet) = VI.OptionInfo.DicoExport
                Dim Fiche As Virtualia.Ressources.Datas.ItemSelection
                WsListeResultat = New List(Of Virtualia.Ressources.Datas.ItemSelection)
                For I = 0 To TabIde.Count - 1
                    Select Case IdeExperte
                        Case Is = 700 'Identifiant interne
                            Result = TabIde(I).ToString
                        Case Is > 9000 'Constante
                            Result = DateDebut
                        Case Else
                            Try
                                Result = AppObjetUser.PointeurDllExpert.Donnee(WsPointdeVue, NoObjet, IdeExperte, TabIde(I), DateDebut, DateFin)
                            Catch Ex As Exception
                                Result = ""
                            End Try
                    End Select
                    If WsSiExperteCMC = True Then
                        Select Case SiResultatExpertOK(NoObjet, IdeExperte, Result)
                            Case True
                                Fiche = New Virtualia.Ressources.Datas.ItemSelection(TabIde(I))
                                Fiche.ChampExtrait(0) = Result
                                WsListeResultat.Add(Fiche)
                        End Select
                    Else
                        Fiche = New Virtualia.Ressources.Datas.ItemSelection(TabIde(I))
                        Fiche.ChampExtrait(0) = Result
                        WsListeResultat.Add(Fiche)
                    End If
                Next I
                Return WsListeResultat.Count
            End Get
        End Property

        Public ReadOnly Property RequetesFusionnees(ByVal Reqs As ArrayList, ByVal OpeLiaison As Short) As String
            Get
                Dim I As Integer
                Dim ClauseTri As String = " ORDER BY a.Ide_dossier ASC"
                Dim ClauseIn As String = "a.Ide_Dossier IN ("
                Dim ClauseSql As String
                Dim Chaine As New System.Text.StringBuilder

                If Reqs.Count = 0 Then
                    Return ""
                End If
                ClauseSql = Replace(Reqs(I).ToString, ClauseTri, "")
                Chaine.Append(ClauseSql)
                For I = 1 To Reqs.Count - 1
                    Select Case Reqs(I).ToString
                        Case Is = ""
                            Exit For
                    End Select
                    ClauseSql = Replace(Reqs(I).ToString, ClauseTri, "")
                    Select Case OpeLiaison
                        Case VI.Operateurs.ET
                            Chaine.Append(" AND (")
                        Case VI.Operateurs.OU
                            Chaine.Append(" OR (")
                    End Select
                    Chaine.Append(ClauseIn)
                    ClauseSql = Replace(ClauseSql, " a ", " z" & I.ToString & Strings.Space(1))
                    Chaine.Append(Replace(ClauseSql, "a.", "z" & I.ToString & "."))
                    Chaine.Append("))")
                Next I
                Chaine.Append(ClauseTri)
                Return Chaine.ToString
            End Get
        End Property

        Public ReadOnly Property RequeteDynaSql(ByVal NoObjet As Short, ByVal NoInfo As Short, ByVal OpeLiaison As Short, ByVal OpeComparaison As Short, ByVal ConditionValeurs As String, ByVal DateDebut As String, ByVal DateFin As String, ByVal SiInfo As Boolean) As String
            Get
                Dim Constructeur As Virtualia.Systeme.Sgbd.Sql.SqlInterne
                Dim OrdreSql As String
                Dim OpeLien As Short = VI.Operateurs.OU

                Select Case OpeComparaison
                    Case Is = VI.Operateurs.Difference
                        OpeLien = VI.Operateurs.ET
                End Select
                Constructeur = New Virtualia.Systeme.Sgbd.Sql.SqlInterne(AppObjetUser.VParent.VirModele, AppObjetUser.InstanceBd)
                Constructeur.NombredeRequetes(WsPointdeVue, DateDebut, DateFin, OpeLiaison) = 1
                Constructeur.SiPasdeTriSurIdeDossier = False
                Constructeur.PreselectiondIdentifiants = ""
                Constructeur.NoInfoSelection(0, NoObjet) = NoInfo
                Constructeur.LettreAlias = "a"
                Select Case ConditionValeurs
                    Case Is <> ""
                        If ConditionValeurs = "Aucun;" Then
                            ConditionValeurs = "NULLE"
                        ElseIf ConditionValeurs = "Tous;" Then
                            ConditionValeurs = "TOUS"
                        End If
                        Constructeur.ValeuraComparer(0, OpeLien, OpeComparaison, False) = ConditionValeurs
                End Select
                Select Case SiInfo
                    Case True
                        Constructeur.InfoExtraite(0, NoObjet, 0) = NoInfo
                End Select
                OrdreSql = Constructeur.OrdreSqlDynamique

                WsNumObjet = NoObjet
                If WsListeRequetes Is Nothing Then
                    WsListeRequetes = New ArrayList
                End If
                WsListeRequetes.Add(OrdreSql)

                Return OrdreSql
            End Get
        End Property

        Public ReadOnly Property SelectionDynamique(ByVal NoObjet As Short, ByVal OrdreSql As String, ByVal TabIde() As Integer) As Integer
            Get
                Dim Proxy As Virtualia.Ressources.WebService.Serveur
                Dim ChaineLue As System.Text.StringBuilder
                Dim ChainePage As String
                Dim NoPage As Integer = 1
                Dim TableauObjet(0) As String
                Dim TableauData(0) As String
                Dim K As Integer

                WsTabIde = TabIde

                Proxy = New Virtualia.Ressources.WebService.Serveur(AppObjetUser.VParent.VirUrlWebServeur)

                ChaineLue = New System.Text.StringBuilder
                Do
                    ChainePage = Proxy.SelectionSql(AppObjetUser.V_NomdUtilisateurSgbd, WsPointdeVue, NoObjet, OrdreSql, NoPage)
                    If ChainePage = "" Then
                        Exit Do
                    End If
                    ChaineLue.Append(ChainePage)
                    NoPage += 1
                Loop
                Proxy.Dispose()

                TableauObjet = Strings.Split(ChaineLue.ToString, VI.SigneBarre, -1)

                Dim Fiche As Virtualia.Ressources.Datas.ItemSelection
                WsListeResultat = New List(Of Virtualia.Ressources.Datas.ItemSelection)
                For NoPage = 0 To TableauObjet.Count - 1
                    If TableauObjet(NoPage) = "" Then
                        Exit For
                    End If
                    TableauData = Strings.Split(TableauObjet(NoPage), VI.Tild, -1)
                    If SiPreselectionOK(CInt(TableauData(0))) = True Then
                        Fiche = New Virtualia.Ressources.Datas.ItemSelection(CInt(TableauData(0)))
                        For K = 1 To TableauData.Count - 1
                            Fiche.ChampExtrait(0) = TableauData(K)
                        Next K
                        WsListeResultat.Add(Fiche)
                    End If
                Next NoPage
                Return WsListeResultat.Count
            End Get
        End Property

        Private ReadOnly Property SiPreselectionOK(ByVal Ide As Integer) As Boolean
            Get
                If WsTabIde Is Nothing Then
                    Return True
                End If
                Dim I As Integer
                For I = 0 To WsTabIde.Count - 1
                    If WsTabIde(I) = Ide Then
                        Return True
                        Exit Property
                    End If
                Next I
                Return False
            End Get
        End Property

        Public ReadOnly Property SelectionDynamique(ByVal NoObjet As Short, ByVal OrdreSql As String) As Integer
            Get
                Dim Proxy As Virtualia.Ressources.WebService.Serveur
                Dim ChaineLue As System.Text.StringBuilder
                Dim ChainePage As String
                Dim NoPage As Integer = 1
                Dim TableauObjet(0) As String
                Dim TableauData(0) As String
                Dim K As Integer

                Proxy = New Virtualia.Ressources.WebService.Serveur(AppObjetUser.VParent.VirUrlWebServeur)

                ChaineLue = New System.Text.StringBuilder
                Do
                    ChainePage = Proxy.SelectionSql(AppObjetUser.V_NomdUtilisateurSgbd, WsPointdeVue, NoObjet, OrdreSql, NoPage)
                    If ChainePage = "" Then
                        Exit Do
                    End If
                    ChaineLue.Append(ChainePage)
                    NoPage += 1
                Loop
                Proxy.Dispose()

                TableauObjet = Strings.Split(ChaineLue.ToString, VI.SigneBarre, -1)

                Dim Fiche As Virtualia.Ressources.Datas.ItemSelection
                WsListeResultat = New List(Of Virtualia.Ressources.Datas.ItemSelection)
                For NoPage = 0 To TableauObjet.Count - 1
                    If TableauObjet(NoPage) = "" Then
                        Exit For
                    End If
                    TableauData = Strings.Split(TableauObjet(NoPage), VI.Tild, -1)
                    If CInt(TableauData(0)) > 0 Then
                        Fiche = New Virtualia.Ressources.Datas.ItemSelection(CInt(TableauData(0)))
                        For K = 1 To TableauData.Count - 1
                            Fiche.ChampExtrait(0) = TableauData(K)
                        Next K
                        WsListeResultat.Add(Fiche)
                    End If
                Next NoPage
                Return WsListeResultat.Count
            End Get
        End Property

        Public ReadOnly Property RequeteObjetSql(ByVal NoObjet As Short, ByVal ListeInfos As String, ByVal OpeComparaison As Short, ByVal ConditionValeurs As String, ByVal DateDebut As String, ByVal DateFin As String, ByVal TabIde() As Integer) As String
            Get
                Dim Constructeur As Virtualia.Systeme.Sgbd.Sql.SqlInterne
                Dim OrdreSql As String
                Dim OpeLien As Short = VI.Operateurs.OU
                Dim TableauInfos(0) As String
                Dim TableauFiltre(0) As String
                Dim K As Integer
                Dim I As Integer
                Dim ChaineIde As New System.Text.StringBuilder
                Dim SiFiltre As Boolean = False

                If TabIde IsNot Nothing Then
                    If TabIde.Count < 500 Then
                        For I = 0 To TabIde.Count - 1
                            ChaineIde.Append(TabIde(I).ToString & VI.PointVirgule)
                        Next I
                    End If
                End If

                TableauInfos = Strings.Split(ListeInfos, VI.PointVirgule, -1)
                TableauFiltre = Strings.Split(ConditionValeurs, VI.Tild, -1)
                For K = 0 To TableauFiltre.Count - 1
                    If TableauFiltre(K) <> "" Then
                        SiFiltre = True
                        Exit For
                    End If
                Next K

                Select Case OpeComparaison
                    Case Is = VI.Operateurs.Difference
                        OpeLien = VI.Operateurs.ET
                End Select
                Constructeur = New Virtualia.Systeme.Sgbd.Sql.SqlInterne(AppObjetUser.VParent.VirModele, AppObjetUser.InstanceBd)
                Constructeur.NombredeRequetes(WsPointdeVue, DateDebut, DateFin, VI.Operateurs.ET) = 1
                Constructeur.SiPasdeTriSurIdeDossier = False
                Constructeur.PreselectiondIdentifiants = ChaineIde.ToString

                I = 0
                Select Case SiFiltre
                    Case True
                        For K = 0 To TableauInfos.Count - 1
                            If TableauInfos(K) = "" Then
                                Exit For
                            End If
                            If TableauFiltre(K) <> "" Then
                                Constructeur.NoInfoSelection(I, NoObjet) = CShort(TableauInfos(K))
                                Constructeur.ValeuraComparer(I, OpeLien, OpeComparaison, False) = TableauFiltre(K)
                                I += 1
                            End If
                        Next K
                    Case False
                        Constructeur.NoInfoSelection(0, NoObjet) = CShort(TableauInfos(0))
                        If CShort(TableauInfos(0)) > 0 Then
                            I += 1
                        End If
                End Select

                Select Case AppObjetUser.VParent.VirObjet(WsPointdeVue, NoObjet).VNature
                    Case VI.TypeObjet.ObjetDate, VI.TypeObjet.ObjetDateRang
                        If DateDebut <> "" Then
                            Constructeur.NoInfoSelection(I, NoObjet) = 0
                            Constructeur.ValeuraComparer(I, OpeLien, VI.Operateurs.Inclu, False) = DateDebut & VI.PointVirgule & DateFin
                            Constructeur.SiHistoriquedeSituation = True
                        End If
                End Select

                For K = 0 To TableauInfos.Count - 1
                    If TableauInfos(K) = "" Then
                        Exit For
                    End If
                    Constructeur.InfoExtraite(K, NoObjet, 0) = CShort(TableauInfos(K))
                Next K
                OrdreSql = Constructeur.OrdreSqlDynamique

                WsNumObjet = NoObjet
                Return OrdreSql
            End Get
        End Property

        Public ReadOnly Property PointdeVue() As Short
            Get
                Return WsPointdeVue
            End Get
        End Property

        Public ReadOnly Property NumeroObjet() As Short
            Get
                Return WsNumObjet
            End Get
        End Property

        Private ReadOnly Property SiResultatExpertOK(ByVal NoObjet As Short, ByVal NoInfo As Short, ByVal Valeur As String) As Boolean
            Get
                Dim OpeLiaison As Short
                Dim NatureDonnee = AppObjetUser.VParent.VirExpertes
                Dim TableauComparaison(0) As String
                Dim I As Integer

                Select Case WsValeursComparaison
                    Case Is = "NULLE"
                        If Valeur = "" Then
                            Return True
                        End If
                    Case Is = "TOUS"
                        If Valeur <> "" Then
                            Return True
                        End If
                    Case Else
                        If Valeur = "" Then
                            Return False
                        End If
                End Select

                TableauComparaison = Strings.Split(WsValeursComparaison, VI.PointVirgule, -1)

                Select Case WsOperateurComparaison
                    Case VI.Operateurs.Egalite
                        OpeLiaison = VI.Operateurs.OU
                    Case VI.Operateurs.Difference
                        OpeLiaison = VI.Operateurs.ET
                    Case VI.Operateurs.Inclu, VI.Operateurs.Exclu
                        Select Case WsInfoExperte.VNature
                            Case VI.NatureDonnee.DonneeNumerique
                                If Val(AppObjetUser.V_RhFonction.VirgulePoint(Valeur)) _
                                    >= Val(AppObjetUser.V_RhFonction.VirgulePoint(TableauComparaison(0))) Then
                                    If Val(AppObjetUser.V_RhFonction.VirgulePoint(Valeur)) _
                                        <= Val(AppObjetUser.V_RhFonction.VirgulePoint(TableauComparaison(1))) Then
                                        If WsOperateurComparaison = VI.Operateurs.Inclu Then
                                            Return True
                                        Else
                                            Return False
                                        End If
                                    End If
                                End If
                            Case VI.NatureDonnee.DonneeDate
                                Select Case AppObjetUser.V_RhDates.ComparerDates(Valeur, TableauComparaison(0))
                                    Case VI.ComparaisonDates.Egalite, VI.ComparaisonDates.PlusGrand
                                        Select Case AppObjetUser.V_RhDates.ComparerDates(Valeur, TableauComparaison(1))
                                            Case VI.ComparaisonDates.Egalite, VI.ComparaisonDates.PlusPetit
                                                If WsOperateurComparaison = VI.Operateurs.Inclu Then
                                                    Return True
                                                Else
                                                    Return False
                                                End If
                                        End Select
                                End Select
                            Case Else
                                If Valeur >= TableauComparaison(0) Then
                                    If Valeur <= TableauComparaison(1) Then
                                        If WsOperateurComparaison = VI.Operateurs.Inclu Then
                                            Return True
                                        Else
                                            Return False
                                        End If
                                    End If
                                End If
                        End Select
                End Select
                For I = 0 To TableauComparaison.Count - 1
                    Select Case WsInfoExperte.VNature
                        Case VI.NatureDonnee.DonneeNumerique
                            If Val(AppObjetUser.V_RhFonction.VirgulePoint(TableauComparaison(I))) _
                               = Val(AppObjetUser.V_RhFonction.VirgulePoint(Valeur)) Then
                                Select Case WsOperateurComparaison
                                    Case VI.Operateurs.Egalite
                                        If OpeLiaison = VI.Operateurs.OU Then
                                            Return True
                                        Else
                                            Return False
                                        End If
                                    Case VI.Operateurs.Difference
                                        If OpeLiaison = VI.Operateurs.OU Then
                                            Return False
                                        Else
                                            Return True
                                        End If
                                End Select
                            Else
                                If TableauComparaison(I) = Valeur Then
                                    Select Case WsOperateurComparaison
                                        Case VI.Operateurs.Egalite
                                            If OpeLiaison = VI.Operateurs.OU Then
                                                Return True
                                            Else
                                                Return False
                                            End If
                                        Case VI.Operateurs.Difference
                                            If OpeLiaison = VI.Operateurs.OU Then
                                                Return False
                                            Else
                                                Return True
                                            End If
                                    End Select
                                End If
                            End If
                    End Select
                Next I
                Return False
            End Get
        End Property

        Public Sub New(ByVal InstanceUtilisateur As Virtualia.Net.Session.ObjetSession, ByVal InfoExperte As Virtualia.Systeme.MetaModele.Expertes.InformationExperte, ByVal OpeComparaison As Short, ByVal ChaineComparaison As String)
            'CMC Expertes
            MyBase.New()
            AppObjetUser = InstanceUtilisateur
            WsInfoExperte = InfoExperte
            WsOperateurComparaison = OpeComparaison
            WsValeursComparaison = ChaineComparaison
            WsPointdeVue = InfoExperte.PointdeVue
            WsSiExperteCMC = True
        End Sub

        Public Sub New(ByVal InstanceUtilisateur As Virtualia.Net.Session.ObjetSession, ByVal PointdeVue As Short)
            MyBase.New()
            AppObjetUser = InstanceUtilisateur
            WsPointdeVue = PointdeVue
            WsSiExperteCMC = False
        End Sub

        Public Overloads Sub Dispose() Implements IDisposable.Dispose
            Dispose(True)
            GC.SuppressFinalize(Me)
        End Sub

        Protected Overridable Overloads Sub Dispose(ByVal disposing As Boolean)
            If Not Me.disposedValue Then
                If disposing Then
                    If WsListeResultat IsNot Nothing Then
                        WsListeResultat.Clear()
                    End If
                End If
            End If
            Me.disposedValue = True
        End Sub
    End Class
End Namespace
