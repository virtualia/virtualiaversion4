﻿<%@ Page Language="VB" MasterPageFile="~/PagesMaitre/VirtualiaMain.master" AutoEventWireup="false" 
    Inherits="Virtualia.Net.Fenetres_FrmRemuneration" Codebehind="FrmRemuneration.aspx.vb"
    MaintainScrollPositionOnPostback="True" %>
<%@ MasterType VirtualPath="~/PagesMaitre/VirtualiaMain.master"  %>

<%@ Register src="../ControlesWebUser/ObjetsArmoire/ArmoirePersonnes.ascx" tagname="ArmoirePersonnes" tagprefix="VirtuArm" %>
<%@ Register src="../ControlesWebUser/ObjetsArmoire/ListeReferences.ascx" tagname="ListeReferences" tagprefix="VirtuRef" %>
<%@ Register src="../ControlesWebUser/ObjetsArmoire/AlbumdesScripts.ascx" tagname="AlbumdesScripts" tagprefix="VirtuAlbum" %>
<%@ Register src="../ControlesWebUser/Outils/EditionExtraction.ascx" tagname="EditionExtraction" tagprefix="Virtualia" %>
<%@ Register src="../ControlesWebUser/Saisies/VMessage.ascx" tagname="VMessage" tagprefix="Virtualia" %>

<%@ Register src="../ControlesWebUser/ObjetsFenetre/Personnel/PER_AVANTAGES.ascx" tagname="PER_AVANTAGES" tagprefix="Virtualia" %>
<%@ Register src="../ControlesWebUser/ObjetsFenetre/Personnel/PER_HEURESUP.ascx" tagname="PER_HEURESUP" tagprefix="Virtualia" %>
<%@ Register src="../ControlesWebUser/ObjetsFenetre/Personnel/PER_INDEMNITE.ascx" tagname="PER_INDEMNITE" tagprefix="Virtualia" %>
<%@ Register src="../ControlesWebUser/ObjetsFenetre/Personnel/PER_VARIABLE_PAIE.ascx" tagname="PER_VARIABLE_PAIE" tagprefix="Virtualia" %>

<asp:Content ID="CadreGeneral" runat="server" ContentPlaceHolderID="CorpsMaster">
    <asp:UpdatePanel ID="UpdatePanelSaisie" runat="server">
        <ContentTemplate>
           <asp:Table ID="TableOnglets" runat="server" Height="35px" Width="1150px" HorizontalAlign="Center"
                     style="margin-top: 0px" >
             <asp:TableRow>
                <asp:TableCell HorizontalAlign="Left">
                    <asp:Table ID="CadreOnglets" runat="server" Height="35px" Width="1116px" HorizontalAlign="Left"
                       CellPadding="0" CellSpacing="0" BackImageUrl="~/Images/Onglets/Onglet_2bords_187.bmp"
                       style="margin-top: 10px; border-top-color: #124545;
                        border-bottom-color: #B0E0D7; border-bottom-style: groove" >
                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign ="Center" VerticalAlign="Bottom" Width="185px">
                                <asp:Button ID="BoutonN1" runat="server" Height="24px" Width="170px" 
                                    ForeColor="White" Text="Synthèse" Font-Bold="true" Font-Italic="true"
                                    Font-Names="Trebuchet MS" BackColor="#6C9690" BorderStyle="None"
                                    style="padding-left: 1px; margin-left: 1px; margin-bottom: 2px; text-align: center" />     
                            </asp:TableCell>
                           <asp:TableCell HorizontalAlign ="Center" VerticalAlign="Bottom" Width="185px">
                                <asp:Button ID="BoutonN2" runat="server" Height="33px" Width="185px" 
                                    ForeColor="#142425" Text="Bulletins de paie" Font-Bold="false" Font-Italic="true"
                                    Font-Names="Trebuchet MS" BackColor="Transparent" BorderStyle="None"
                                    style="padding-left: 1px; margin-left: 1px; margin-bottom: 2px; text-align: center" />  
                            </asp:TableCell>
                            <asp:TableCell HorizontalAlign ="Center" VerticalAlign="Bottom" Width="185px">
                                <asp:Button ID="BoutonN3" runat="server" Height="33px" Width="185px" 
                                    ForeColor="#142425" Text="Primes" Font-Bold="false" Font-Italic="true"
                                    Font-Names="Trebuchet MS" BackColor="Transparent" BorderStyle="None" 
                                    style="padding-left: 1px; margin-left: 1px; margin-bottom: 2px; text-align: center" />  
                            </asp:TableCell>
                            <asp:TableCell HorizontalAlign ="Center" VerticalAlign="Bottom" Width="185px">
                                <asp:Button ID="BoutonN4" runat="server" Height="33px" Width="185px" 
                                    ForeColor="#142425" Text="Heures Supplémentaires" Font-Bold="false" Font-Italic="true"
                                    Font-Names="Trebuchet MS" BackColor="Transparent" BorderStyle="None" 
                                    style="padding-left: 1px; margin-left: 1px; margin-bottom: 2px; text-align: center" />   
                            </asp:TableCell>
                            <asp:TableCell HorizontalAlign ="Center" VerticalAlign="Bottom" Width="185px">
                                <asp:Button ID="BoutonN5" runat="server" Height="33px" Width="185px" 
                                    ForeColor="#142425" Text="Avantages en nature" Font-Bold="false" Font-Italic="true"
                                    Font-Names="Trebuchet MS" BackColor="Transparent" BorderStyle="None" 
                                    style="padding-left: 1px; margin-left: 1px; margin-bottom: 2px; text-align: center" />  
                            </asp:TableCell>
                            <asp:TableCell HorizontalAlign ="Center" VerticalAlign="Bottom" Width="185px">
                                <asp:Button ID="BoutonN6" runat="server" Height="33px" Width="185px" 
                                    ForeColor="#142425" Text="Variables de paie" Font-Bold="false" Font-Italic="true"
                                    Font-Names="Trebuchet MS" BackColor="Transparent" BorderStyle="None" 
                                    style="padding-left: 1px; margin-left: 1px; margin-bottom: 2px; text-align: center" />  
                            </asp:TableCell>
                        </asp:TableRow>
                    </asp:Table>
                </asp:TableCell>
             </asp:TableRow>
           </asp:Table>
           <asp:Table ID="CadreSaisie" runat="server" Height="1220px" Width="1150px" HorizontalAlign="Center"
                     BackImageUrl="~/Images/Fonds/Fond_Saisie.jpg" CellSpacing="2" style="margin-top: 0px" >
               <asp:TableRow>
                   <asp:TableCell ID="ConteneurVues" Height="1220px" Width="820px" VerticalAlign="Top" HorizontalAlign="Center">
                       <asp:MultiView ID="MultiOnglets" runat="server" ActiveViewIndex="0">
                          <asp:View ID="VueSynthese" runat="server"> 
                              
                          </asp:View>  
                          <asp:View ID="VueBulletin" runat="server">  
                             
                           </asp:View>  
                          <asp:View ID="VuePrimes" runat="server">
                             <Virtualia:PER_INDEMNITE ID="PER_INDEMNITE_21" runat="server"
                               CadreStyle="margin-top: 10px" />  
                          </asp:View>
                          <asp:View ID="VueHeuresSup" runat="server">  
                              <Virtualia:PER_HEURESUP ID="PER_HEURESUP_61" runat="server"
                               CadreStyle="margin-top: 10px" /> 
                          </asp:View> 
                          <asp:View ID="VueAvantage" runat="server">  
                            <Virtualia:PER_AVANTAGES ID="PER_AVANTAGES_49" runat="server"
                               CadreStyle="margin-top: 10px" />  
                          </asp:View>   
                          <asp:View ID="VueVariable" runat="server">  
                             <Virtualia:PER_VARIABLE_PAIE ID="PER_VARIABLE_PAIE_67" runat="server"
                               CadreStyle="margin-top: 10px" />  
                          </asp:View>  
                          <asp:View ID="VueSysRef" runat="server">
                             <VirtuRef:ListeReferences ID="Referentiel" runat="server"
                                SiListeMenuVisible="false" />
                          </asp:View>
                          <asp:View ID="VueAlbum" runat="server">
                             <VirtuAlbum:AlbumdesScripts ID="AlbumRequetes" runat="server"
                                V_PointdeVue="1" />
                          </asp:View>
                          <asp:View ID="VueEdition" runat="server">
                             <Virtualia:EditionExtraction ID="EditionArmoire" runat="server" />
                          </asp:View>
                          <asp:View ID="VueMessage" runat="server">
                             <Virtualia:VMessage ID="MsgVirtualia" runat="server" />
                          </asp:View>
                      </asp:MultiView>
                  </asp:TableCell>
                  <asp:TableCell Height="1200px" Width="330px" VerticalAlign="Top" >
                     <VirtuArm:ArmoirePersonnes ID="ArmoireCourante" runat="server" /> 
                  </asp:TableCell>
              </asp:TableRow>
              <asp:TableRow>
                <asp:TableCell>
                    <asp:HiddenField ID="HSelIde" runat="server" Value="0" />
                </asp:TableCell>
              </asp:TableRow>
            </asp:Table>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>