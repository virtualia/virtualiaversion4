﻿Option Strict On
Option Explicit On
Option Compare Text
Imports VI = Virtualia.Systeme.Constantes
Partial Class Fenetres_FrmNouveauDossierPER
    Inherits System.Web.UI.Page
    Private WebFct As Virtualia.Net.WebFonctions
    Private WsIdeDossier As Integer
    Private WsNomState As String = "VNewDossier"
    Private Const ICivil As Short = 0
    Private Const IRefExterne As Short = 1
    Private Const IAffectation As Short = 2
    Private Const ICollectivite As Short = 3
    Private Const IEtablissement As Short = 4
    Private Const ISysRef As Short = 5
    Private Const IMessage As Short = 6

    Public Property Identifiant() As Integer
        Get
            Return WsIdeDossier
        End Get
        Set(ByVal value As Integer)
            If value = WsIdeDossier Then
                Exit Property
            End If
            WsIdeDossier = value
            PER_ETATCIVIL_1.Identifiant = value
            PER_REFEXTERNE_23.Identifiant = value
            PER_AFFECTATION_17.Identifiant = value
            Select Case WebFct.PointeurGlobal.VirModele.InstanceProduit.ClefModele
                Case VI.ModeleRh.FPCollectivite, VI.ModeleRh.FPPompier, VI.ModeleRh.FPEtat, VI.ModeleRh.AfriquePublic
                    PER_COLLECTIVITE_26.Identifiant = value
                Case Else
                    PER_SOCIETE_26.Identifiant = value
            End Select
        End Set
    End Property

    Protected Sub MsgVirtualia_ValeurRetour(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.MessageRetourEventArgs) Handles MsgVirtualia.ValeurRetour
        Select Case e.Emetteur
            Case Is = "MajDossier"
                WebFct.PointeurContexte.Fenetre_VueActive(VI.CategorieRH.InfosPersonnelles) = ICivil
                Select Case e.NumeroObjet
                    Case VI.ObjetPer.ObaCivil
                        PER_ETATCIVIL_1.V_RetourDialogueMaj(e.ReponseMsg) = e.ChaineDatas
                        Response.Redirect("~/Fenetres/FrmInfosPersonnelles.aspx")
                        Exit Sub
                    Case VI.ObjetPer.ObaExterne
                        PER_REFEXTERNE_23.V_RetourDialogueMaj(e.ReponseMsg) = e.ChaineDatas
                    Case VI.ObjetPer.ObaOrganigramme
                        PER_AFFECTATION_17.V_RetourDialogueMaj(e.ReponseMsg) = e.ChaineDatas
                    Case VI.ObjetPer.ObaSociete
                        Select Case WebFct.PointeurGlobal.VirModele.InstanceProduit.ClefModele
                            Case VI.ModeleRh.FPCollectivite, VI.ModeleRh.FPPompier, VI.ModeleRh.FPEtat, VI.ModeleRh.AfriquePublic
                                PER_COLLECTIVITE_26.V_RetourDialogueMaj(e.ReponseMsg) = e.ChaineDatas
                            Case Else
                                PER_SOCIETE_26.V_RetourDialogueMaj(e.ReponseMsg) = e.ChaineDatas
                        End Select
                End Select
        End Select
        CadreSaisie.BackImageUrl = "~/Images/Fonds/Fond_Saisie.jpg"
        ConteneurVues.Width = New Unit(820)
        MultiOnglets.ActiveViewIndex = WebFct.PointeurContexte.Fenetre_VueActive(VI.CategorieRH.InfosPersonnelles)
    End Sub

    Protected Sub Referentiel_RetourEventHandler(ByVal sender As Object, ByVal e As System.EventArgs) Handles Referentiel.RetourEventHandler
        CadreSaisie.BackImageUrl = "~/Images/Fonds/Fond_Saisie.jpg"
        ConteneurVues.Width = New Unit(820)
        Select Case WebFct.PointeurContexte.SysRef_IDVueRetour
            Case "VueEtatcivil"
                MultiOnglets.SetActiveView(VueEtatCivil)
            Case "VueRefExterne"
                MultiOnglets.SetActiveView(VueRefExterne)
            Case "VueAffectation"
                MultiOnglets.SetActiveView(VueAffectation)
            Case "VueCollectivite"
                MultiOnglets.SetActiveView(VueCollectivite)
            Case "VueEtablissement"
                MultiOnglets.SetActiveView(VueEtablissement)
        End Select
        WebFct.PointeurContexte.Fenetre_VueActive(VI.CategorieRH.InfosPersonnelles) = MultiOnglets.ActiveViewIndex
    End Sub

    Protected Sub Referentiel_ValeurSelectionnee(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.ValeurSelectionneeEventArgs) Handles Referentiel.ValeurSelectionnee
        CadreSaisie.BackImageUrl = "~/Images/Fonds/Fond_Saisie.jpg"
        ConteneurVues.Width = New Unit(820)
        Select Case WebFct.PointeurContexte.SysRef_IDVueRetour
            Case "VueEtatcivil"
                Select Case e.ObjetAppelant
                    Case VI.ObjetPer.ObaCivil
                        PER_ETATCIVIL_1.Dontab_RetourAppelTable(e.ControleAppelant) = e.Valeur
                End Select
                MultiOnglets.SetActiveView(VueEtatCivil)
            Case "VueRefExterne"
                Select Case e.ObjetAppelant
                    Case VI.ObjetPer.ObaExterne
                        PER_REFEXTERNE_23.Dontab_RetourAppelTable(e.ControleAppelant) = e.Valeur
                End Select
                MultiOnglets.SetActiveView(VueRefExterne)
            Case "VueAffectation"
                Select Case e.ObjetAppelant
                    Case VI.ObjetPer.ObaPrevenir
                        PER_AFFECTATION_17.Dontab_RetourAppelTable(e.ControleAppelant) = e.Valeur
                End Select
                MultiOnglets.SetActiveView(VueAffectation)
            Case "VueCollectivite"
                Select Case e.ObjetAppelant
                    Case VI.ObjetPer.ObaSociete
                        PER_COLLECTIVITE_26.Dontab_RetourAppelTable(e.ControleAppelant) = e.Valeur
                End Select
                MultiOnglets.SetActiveView(VueCollectivite)
            Case "VueEtablissement"
                Select Case e.ObjetAppelant
                    Case VI.ObjetPer.ObaSociete
                        PER_SOCIETE_26.Dontab_RetourAppelTable(e.ControleAppelant) = e.Valeur
                End Select
                MultiOnglets.SetActiveView(VueEtablissement)
        End Select
        WebFct.PointeurContexte.Fenetre_VueActive(VI.CategorieRH.InfosPersonnelles) = MultiOnglets.ActiveViewIndex
    End Sub

    Protected Sub PER_AppelTable(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.AppelTableEventArgs) _
    Handles PER_ETATCIVIL_1.AppelTable, PER_AFFECTATION_17.AppelTable, PER_REFEXTERNE_23.AppelTable, _
    PER_COLLECTIVITE_26.AppelTable, PER_SOCIETE_26.AppelTable

        Select Case e.ObjetAppelant
            Case VI.ObjetPer.ObaCivil
                WebFct.PointeurContexte.SysRef_IDVueRetour = VueEtatCivil.ID
            Case VI.ObjetPer.ObaExterne
                WebFct.PointeurContexte.SysRef_IDVueRetour = VueRefExterne.ID
            Case VI.ObjetPer.ObaOrganigramme
                WebFct.PointeurContexte.SysRef_IDVueRetour = VueAffectation.ID
            Case VI.ObjetPer.ObaSociete
                Select Case WebFct.PointeurGlobal.VirModele.InstanceProduit.ClefModele
                    Case VI.ModeleRh.FPCollectivite, VI.ModeleRh.FPPompier, VI.ModeleRh.FPEtat, VI.ModeleRh.AfriquePublic
                        WebFct.PointeurContexte.SysRef_IDVueRetour = VueCollectivite.ID
                    Case Else
                        WebFct.PointeurContexte.SysRef_IDVueRetour = VueEtablissement.ID
                End Select
            Case VI.ObjetPer.ObaEtranger
                WebFct.PointeurContexte.SysRef_IDVueRetour = VueEtatCivil.ID
            Case Else
                Exit Sub
        End Select

        If e.ObjetAppelant = VI.ObjetPer.ObaCivil And e.ControleAppelant = "Dontab06" Then
            Referentiel.V_DuoTable(e.PointdeVueInverse, e.NomdelaTable, e.PointdeVueInverse) = "Pays"
        Else
            Referentiel.V_PointdeVue = e.PointdeVueInverse
            Referentiel.V_NomTable = e.NomdelaTable
        End If
        Referentiel.V_Appelant(e.ObjetAppelant) = e.ControleAppelant
        CadreSaisie.BackImageUrl = ""
        CadreSaisie.BackColor = WebFct.ConvertCouleur("#216B68")
        MultiOnglets.SetActiveView(VueSysRef)
        WebFct.PointeurContexte.Fenetre_VueActive(VI.CategorieRH.InfosPersonnelles) = MultiOnglets.ActiveViewIndex
        ConteneurVues.Width = New Unit(1150)

    End Sub

    Protected Sub MessageDialogue(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.MessageSaisieEventArgs) _
    Handles PER_ETATCIVIL_1.MessageDialogue, PER_AFFECTATION_17.MessageDialogue, PER_COLLECTIVITE_26.MessageDialogue, _
    PER_SOCIETE_26.MessageDialogue, PER_REFEXTERNE_23.MessageDialogue

        MsgVirtualia.AfficherMessage = e
        CadreSaisie.BackImageUrl = ""
        CadreSaisie.BackColor = WebFct.ConvertCouleur("#216B68")
        MultiOnglets.SetActiveView(VueMessage)
        WebFct.PointeurContexte.Fenetre_VueActive(VI.CategorieRH.InfosPersonnelles) = MultiOnglets.ActiveViewIndex
        ConteneurVues.Width = New Unit(1150)
    End Sub

    Protected Sub MessageSaisie(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.MessageSaisieEventArgs) _
    Handles PER_ETATCIVIL_1.MessageSaisie, PER_AFFECTATION_17.MessageSaisie, PER_COLLECTIVITE_26.MessageSaisie, _
    PER_SOCIETE_26.MessageSaisie, PER_REFEXTERNE_23.MessageSaisie

        Dim Eti As System.Web.UI.WebControls.Label
        Eti = CType(WebFct.VirWebControle(Me.Master, "MsgSaisie", 0), System.Web.UI.WebControls.Label)
        Select Case e.NatureMessage
            Case Is = "OK"
                Eti.ForeColor = Drawing.Color.Green
        End Select
        Eti.Text = e.ContenuMessage

        Eti = CType(WebFct.VirWebControle(Me.Master, "MsgSaisiePied", 0), System.Web.UI.WebControls.Label)
        Select Case e.NatureMessage
            Case Is = "OK"
                Eti.ForeColor = Drawing.Color.Green
        End Select
        Eti.Text = e.ContenuMessage

        If e.Emetteur = "PER_ETATCIVIL_1" Then
            If e.NatureMessage = "OK" Then
                If HNewIde.Value = "0" Then
                    HNewIde.Value = e.ChaineDatas
                End If
            End If
        End If
    End Sub

    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        Dim NumSession As String = Me.Master.V_NoSession
        If NumSession = "" Then
            NumSession = Server.HtmlDecode(Request.QueryString("IDVirtualia"))
            Me.Master.V_NoSession = NumSession
        End If
        If NumSession = "" Then
            Dim Msg As String = "Erreur lors de l'identification."
            Response.Redirect("~/Fenetres/Connexion/ErreurApplication.aspx?Msg=" & Msg)
        End If
        WebFct = New Virtualia.Net.WebFonctions(Me, 1)
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        If IsNumeric(HNewIde.Value) Then
            Me.Master.IdentifiantCourant = CInt(HNewIde.Value)
            WebFct.PointeurContexte.Armoire_Identifiant = CInt(HNewIde.Value)
            WebFct.PointeurContexte.Fenetre_VueActive(VI.CategorieRH.InfosPersonnelles) = 0
        End If

        EtiTitre.BackColor = WebFct.CouleurCharte(1, "Titre")
        EtiTitre.ForeColor = WebFct.CouleurCharte(1, "Police_Claire")
        EtiTitre.BorderColor = WebFct.CouleurCharte(1, "Bordure")
    End Sub

    Protected Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
        WebFct.Dispose()
    End Sub

End Class
