﻿<%@ Page Language="VB" MasterPageFile="~/PagesMaitre/VirtualiaMain.master" AutoEventWireup="false" 
    Inherits="Virtualia.Net.Fenetres_FrmPublicFonctionnelles" Codebehind="FrmPublicFonctionnelles.aspx.vb"
    MaintainScrollPositionOnPostback="True" %>
<%@ MasterType VirtualPath="~/PagesMaitre/VirtualiaMain.master"  %>

<%@ Register src="../ControlesWebUser/ObjetsArmoire/ArmoirePersonnes.ascx" tagname="ArmoirePersonnes" tagprefix="VirtuArm" %>
<%@ Register src="../ControlesWebUser/ObjetsArmoire/ListeReferences.ascx" tagname="ListeReferences" tagprefix="VirtuRef" %>
<%@ Register src="../ControlesWebUser/ObjetsArmoire/AlbumdesScripts.ascx" tagname="AlbumdesScripts" tagprefix="VirtuAlbum" %>
<%@ Register src="../ControlesWebUser/Outils/EditionExtraction.ascx" tagname="EditionExtraction" tagprefix="Virtualia" %>
<%@ Register src="../ControlesWebUser/Syntheses/FicheAffectation.ascx" tagname="FicheAffectation" tagprefix="Virtualia" %>
<%@ Register src="../ControlesWebUser/Saisies/VMessage.ascx" tagname="VMessage" tagprefix="Virtualia" %>

<%@ Register src="../ControlesWebUser/ObjetsFenetre/Personnel/PER_COLLECTIVITE.ascx" tagname="PER_COLLECTIVITE" tagprefix="Virtualia" %>
<%@ Register src="../ControlesWebUser/ObjetsFenetre/Personnel/PER_ADRESSEPRO.ascx" tagname="PER_ADRESSEPRO" tagprefix="Virtualia" %>
<%@ Register src="../ControlesWebUser/ObjetsFenetre/Personnel/PER_AFFECTATION.ascx" tagname="PER_AFFECTATION" tagprefix="Virtualia" %>
<%@ Register src="../ControlesWebUser/ObjetsFenetre/Personnel/PER_ACTI_ANNEXES.ascx" tagname="PER_ACTI_ANNEXES" tagprefix="Virtualia" %>
<%@ Register src="../ControlesWebUser/ObjetsFenetre/Personnel/PER_ACTI_INTERNES.ascx" tagname="PER_ACTI_INTERNES" tagprefix="Virtualia" %>
<%@ Register src="../ControlesWebUser/ObjetsFenetre/Personnel/PER_AFFECTATION_SECOND.ascx" tagname="PER_AFFECTATION_SECOND" tagprefix="Virtualia" %>
<%@ Register src="../ControlesWebUser/ObjetsFenetre/Personnel/PER_MANDAT.ascx" tagname="PER_MANDAT" tagprefix="Virtualia" %>

<asp:Content ID="CadreGeneral" runat="server" ContentPlaceHolderID="CorpsMaster">
    <asp:UpdatePanel ID="UpdatePanelSaisie" runat="server">
        <ContentTemplate>
           <asp:Table ID="TableOnglets" runat="server" Height="35px" Width="1150px" HorizontalAlign="Center"
                     style="margin-top: 0px" >
             <asp:TableRow>
                <asp:TableCell HorizontalAlign="Left">
                    <asp:Table ID="CadreOnglets" runat="server" Height="35px" Width="1116px" HorizontalAlign="Left"
                       CellPadding="0" CellSpacing="0" BackImageUrl="~/Images/Onglets/Onglet_2bords_187.bmp"
                       style="margin-top: 10px; border-top-color: #124545;
                        border-bottom-color: #B0E0D7; border-bottom-style: groove" >
                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign ="Center" VerticalAlign="Bottom" Width="185px">
                                <asp:Button ID="BoutonN1" runat="server" Height="24px" Width="170px" 
                                    ForeColor="White" Text="Annuaire" Font-Bold="true" Font-Italic="true"
                                    Font-Names="Trebuchet MS" BackColor="#6C9690" BorderStyle="None"
                                    style="padding-left: 1px; margin-left: 1px; margin-bottom: 2px; text-align: center" />     
                            </asp:TableCell>
                           <asp:TableCell HorizontalAlign ="Center" VerticalAlign="Bottom" Width="185px">
                                <asp:Button ID="BoutonN2" runat="server" Height="33px" Width="185px" 
                                    ForeColor="#142425" Text="Etablissement" Font-Bold="false" Font-Italic="true"
                                    Font-Names="Trebuchet MS" BackColor="Transparent" BorderStyle="None"
                                    style="padding-left: 1px; margin-left: 1px; margin-bottom: 2px; text-align: center" />  
                            </asp:TableCell>
                            <asp:TableCell HorizontalAlign ="Center" VerticalAlign="Bottom" Width="185px">
                                <asp:Button ID="BoutonN3" runat="server" Height="33px" Width="185px" 
                                    ForeColor="#142425" Text="Affectation fonctionnelle" Font-Bold="false" Font-Italic="true"
                                    Font-Names="Trebuchet MS" BackColor="Transparent" BorderStyle="None" 
                                    style="padding-left: 1px; margin-left: 1px; margin-bottom: 2px; text-align: center" />  
                            </asp:TableCell>
                            <asp:TableCell HorizontalAlign ="Center" VerticalAlign="Bottom" Width="185px">
                                <asp:Button ID="BoutonN4" runat="server" Height="33px" Width="185px" 
                                    ForeColor="#142425" Text="Multi affectations" Font-Bold="false" Font-Italic="true"
                                    Font-Names="Trebuchet MS" BackColor="Transparent" BorderStyle="None" 
                                    style="padding-left: 1px; margin-left: 1px; margin-bottom: 2px; text-align: center" />   
                            </asp:TableCell>
                            <asp:TableCell HorizontalAlign ="Center" VerticalAlign="Bottom" Width="185px">
                                <asp:Button ID="BoutonN5" runat="server" Height="33px" Width="185px" 
                                    ForeColor="#142425" Text="Commissions" Font-Bold="false" Font-Italic="true"
                                    Font-Names="Trebuchet MS" BackColor="Transparent" BorderStyle="None" 
                                    style="padding-left: 1px; margin-left: 1px; margin-bottom: 2px; text-align: center" />  
                            </asp:TableCell>
                            <asp:TableCell HorizontalAlign ="Center" VerticalAlign="Bottom" Width="185px">
                                <asp:Button ID="BoutonN6" runat="server" Height="33px" Width="185px" 
                                    ForeColor="#142425" Text="Mandats" Font-Bold="false" Font-Italic="true"
                                    Font-Names="Trebuchet MS" BackColor="Transparent" BorderStyle="None" 
                                    style="padding-left: 1px; margin-left: 1px; margin-bottom: 2px; text-align: center" />  
                            </asp:TableCell>
                        </asp:TableRow>
                    </asp:Table>
                </asp:TableCell>
             </asp:TableRow>
           </asp:Table>
           <asp:Table ID="CadreSaisie" runat="server" Height="1220px" Width="1150px" HorizontalAlign="Center"
                     BackImageUrl="~/Images/Fonds/Fond_Saisie.jpg" CellSpacing="2" style="margin-top: 0px" >
               <asp:TableRow>
                   <asp:TableCell ID="ConteneurVues" Height="1220px" Width="820px" VerticalAlign="Top" HorizontalAlign="Center">
                       <asp:MultiView ID="MultiOnglets" runat="server" ActiveViewIndex="0">
                          <asp:View ID="VueSynthese" runat="server"> 
                              <Virtualia:FicheAffectation ID="Synthese" runat="server" />
                          </asp:View>  
                          <asp:View ID="VueEtablissement" runat="server">  
                             <Virtualia:PER_COLLECTIVITE ID="PER_COLLECTIVITE_26" runat="server"
                               CadreStyle="margin-top: 10px" CadreExpertStyle="margin-top: 10px"  />  
                             <Virtualia:PER_ADRESSEPRO ID="PER_ADRESSEPRO_24" runat="server"
                               CadreStyle="margin-top: 20px"  />  
                           </asp:View>  
                          <asp:View ID="VueAffectation" runat="server">
                             <Virtualia:PER_AFFECTATION ID="PER_AFFECTATION_17" runat="server"
                               CadreStyle="margin-top: 10px" CadreExpertStyle="margin-top: 10px"  />  
                          </asp:View>
                          <asp:View ID="VueMultiAff" runat="server">  
                              <Virtualia:PER_AFFECTATION_SECOND ID="PER_AFFECTATION_78" runat="server"
                               CadreStyle="margin-top: 10px" /> 
                              <Virtualia:PER_AFFECTATION_SECOND ID="PER_AFFECTATION_141" runat="server"
                                   CadreStyle="margin-top: 10px" V_NumObjet="141" 
                                   V_EtiTitreText="Affectation fonctionnelle tertiaire" /> 
                               <Virtualia:PER_AFFECTATION_SECOND ID="PER_AFFECTATION_142" runat="server"
                                   CadreStyle="margin-top: 10px" V_NumObjet="142" 
                                   V_EtiTitreText="Affectation fonctionnelle quaternaire" />
                          </asp:View> 
                          <asp:View ID="VueCommissions" runat="server">  
                            <Virtualia:PER_ACTI_INTERNES ID="PER_ACTI_INTERNES_75" runat="server"
                               CadreStyle="margin-top: 10px" CadreExpertStyle="margin-top: 10px"  />  
                             <Virtualia:PER_ACTI_ANNEXES ID="PER_ACTI_ANNEXES_48" runat="server"
                               CadreStyle="margin-top: 20px"  />  
                          </asp:View>   
                          <asp:View ID="VueMandat" runat="server">  
                             <Virtualia:PER_MANDAT ID="PER_MANDAT_37" runat="server"
                               CadreStyle="margin-top: 20px"  />  
                          </asp:View>  
                          <asp:View ID="VueSysRef" runat="server">
                             <VirtuRef:ListeReferences ID="Referentiel" runat="server"
                                SiListeMenuVisible="false" />
                          </asp:View>
                          <asp:View ID="VueAlbum" runat="server">
                             <VirtuAlbum:AlbumdesScripts ID="AlbumRequetes" runat="server"
                                V_PointdeVue="1" />
                          </asp:View>
                          <asp:View ID="VueEdition" runat="server">
                             <Virtualia:EditionExtraction ID="EditionArmoire" runat="server" />
                          </asp:View>
                          <asp:View ID="VueMessage" runat="server">
                             <Virtualia:VMessage ID="MsgVirtualia" runat="server" />
                          </asp:View>
                      </asp:MultiView>
                  </asp:TableCell>
                  <asp:TableCell Height="1200px" Width="330px" VerticalAlign="Top" >
                     <VirtuArm:ArmoirePersonnes ID="ArmoireCourante" runat="server" /> 
                  </asp:TableCell>
              </asp:TableRow>
              <asp:TableRow>
                <asp:TableCell>
                    <asp:HiddenField ID="HSelIde" runat="server" Value="0" />
                </asp:TableCell>
              </asp:TableRow>
            </asp:Table>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>