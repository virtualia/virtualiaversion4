﻿Option Strict On
Option Explicit On
Option Compare Text
Imports VI = Virtualia.Systeme.Constantes
Partial Class Fenetres_FrmInfosPersonnelles
    Inherits System.Web.UI.Page
    Private WebFct As Virtualia.Net.WebFonctions
    Private Const ISynthese As Short = 0
    Private Const ICivil As Short = 1
    Private Const IAdresse As Short = 2
    Private Const ISitFam As Short = 3
    Private Const IRib As Short = 4
    Private Const ILiens As Short = 5
    Private Const IMemento As Short = 6
    Private Const IDeco As Short = 7
    Private Const IVisuTout As Short = 8
    Private Const ISysref As Short = 9
    Private Const IAlbum As Short = 10
    Private Const IEdition As Short = 11
    Private Const IMessage As Short = 12

    Public Property Identifiant() As Integer
        Get
            Return CInt(HSelIde.Value)
        End Get
        Set(ByVal value As Integer)
            If value = CInt(HSelIde.Value) Then
                Exit Property
            End If
            HSelIde.Value = value.ToString
            If value > 0 Then
                Call FaireVisualiserTout(value)
                Visutout.Visible = True
                Call FaireSynthese(value)
                Synthese.Visible = True
            Else
                Visutout.Visible = False
                Synthese.Visible = False
            End If
            PER_ETATCIVIL_1.Identifiant = value
            PER_DOMICILE_4.Identifiant = value
            PER_DOMICILE_SECOND_72.Identifiant = value
            PER_DOMICILE_CONGE_73.Identifiant = value
            PER_HORSFRANCE_44.Identifiant = value
            PER_ENFANT_3.Identifiant = value
            PER_CONJOINT_2.Identifiant = value
            PER_APREVENIR_30.Identifiant = value
            PER_BANQUE_5.Identifiant = value
            PER_ANNOTATION_20.Identifiant = value
            PER_NOTEAGENDA_28.Identifiant = value
            PER_REFEXTERNE_23.Identifiant = value
            PER_DECORATION_36.Identifiant = value
            PER_PIECE_IDENTITE_31.Identifiant = value
            PER_DOCUMENTS_51.Identifiant = value
        End Set
    End Property

    Protected Sub ArmoireCourante_CmdAlbum_Click(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.AppelAlbumClickEventArgs) Handles ArmoireCourante.CmdAlbum_Click
        MultiOnglets.SetActiveView(VueAlbum)
        Call ChargerAlbum(e.PointdeVue, e.Outil)
    End Sub

    Protected Sub ArmoireCourante_Dossier_Click(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.DossierClickEventArgs) Handles ArmoireCourante.Dossier_Click
        Identifiant = e.Identifiant
        Me.Master.IdentifiantCourant = e.Identifiant
    End Sub

    Protected Sub MsgVirtualia_ValeurRetour(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.MessageRetourEventArgs) Handles MsgVirtualia.ValeurRetour
        Select Case e.Emetteur
            Case Is = "SuppFiche"
                Select Case e.NumeroObjet
                    Case VI.ObjetPer.ObaEnfant
                        PER_ENFANT_3.RetourDialogueSupp(e.ReponseMsg) = e.ChaineDatas
                        WebFct.PointeurContexte.Fenetre_VueActive(VI.CategorieRH.InfosPersonnelles) = ISitFam
                    Case VI.ObjetPer.ObaDocuments
                        PER_DOCUMENTS_51.RetourDialogueSupp(e.ReponseMsg) = e.ChaineDatas
                        WebFct.PointeurContexte.Fenetre_VueActive(VI.CategorieRH.InfosPersonnelles) = ILiens
                    Case VI.ObjetPer.ObaDecoration
                        PER_DECORATION_36.RetourDialogueSupp(e.ReponseMsg) = e.ChaineDatas
                        WebFct.PointeurContexte.Fenetre_VueActive(VI.CategorieRH.InfosPersonnelles) = IDeco
                End Select
            Case Is = "SuppDossier"
                Call SupprimerDossier(e.ReponseMsg)
                Exit Sub
            Case Is = "MajDossier"
                Select Case e.NumeroObjet
                    Case VI.ObjetPer.ObaCivil
                        PER_ETATCIVIL_1.V_RetourDialogueMaj(e.ReponseMsg) = e.ChaineDatas
                        WebFct.PointeurContexte.Fenetre_VueActive(VI.CategorieRH.InfosPersonnelles) = ICivil
                    Case VI.ObjetPer.ObaEtranger
                        PER_PIECE_IDENTITE_31.V_RetourDialogueMaj(e.ReponseMsg) = e.ChaineDatas
                        WebFct.PointeurContexte.Fenetre_VueActive(VI.CategorieRH.InfosPersonnelles) = ICivil
                    Case VI.ObjetPer.ObaAdresse
                        PER_DOMICILE_4.V_RetourDialogueMaj(e.ReponseMsg) = e.ChaineDatas
                        WebFct.PointeurContexte.Fenetre_VueActive(VI.CategorieRH.InfosPersonnelles) = IAdresse
                    Case VI.ObjetPer.ObaAdresse2
                        PER_DOMICILE_SECOND_72.V_RetourDialogueMaj(e.ReponseMsg) = e.ChaineDatas
                        WebFct.PointeurContexte.Fenetre_VueActive(VI.CategorieRH.InfosPersonnelles) = IAdresse
                    Case VI.ObjetPer.ObaAdresseConge
                        PER_DOMICILE_CONGE_73.V_RetourDialogueMaj(e.ReponseMsg) = e.ChaineDatas
                        WebFct.PointeurContexte.Fenetre_VueActive(VI.CategorieRH.InfosPersonnelles) = IAdresse
                    Case VI.ObjetPer.ObaAdresseHorsFr
                        PER_HORSFRANCE_44.V_RetourDialogueMaj(e.ReponseMsg) = e.ChaineDatas
                        WebFct.PointeurContexte.Fenetre_VueActive(VI.CategorieRH.InfosPersonnelles) = IAdresse
                    Case VI.ObjetPer.ObaEnfant
                        PER_ENFANT_3.V_RetourDialogueMaj(e.ReponseMsg) = e.ChaineDatas
                        WebFct.PointeurContexte.Fenetre_VueActive(VI.CategorieRH.InfosPersonnelles) = ISitFam
                    Case VI.ObjetPer.ObaConjoint
                        PER_CONJOINT_2.V_RetourDialogueMaj(e.ReponseMsg) = e.ChaineDatas
                        WebFct.PointeurContexte.Fenetre_VueActive(VI.CategorieRH.InfosPersonnelles) = ISitFam
                    Case VI.ObjetPer.ObaPrevenir
                        PER_APREVENIR_30.V_RetourDialogueMaj(e.ReponseMsg) = e.ChaineDatas
                        WebFct.PointeurContexte.Fenetre_VueActive(VI.CategorieRH.InfosPersonnelles) = ISitFam
                    Case VI.ObjetPer.ObaBanque
                        PER_BANQUE_5.V_RetourDialogueMaj(e.ReponseMsg) = e.ChaineDatas
                        WebFct.PointeurContexte.Fenetre_VueActive(VI.CategorieRH.InfosPersonnelles) = IRib
                    Case VI.ObjetPer.ObaDocuments
                        PER_DOCUMENTS_51.V_RetourDialogueMaj(e.ReponseMsg) = e.ChaineDatas
                        WebFct.PointeurContexte.Fenetre_VueActive(VI.CategorieRH.InfosPersonnelles) = ILiens
                    Case VI.ObjetPer.ObaDecoration
                        PER_DECORATION_36.V_RetourDialogueMaj(e.ReponseMsg) = e.ChaineDatas
                        WebFct.PointeurContexte.Fenetre_VueActive(VI.CategorieRH.InfosPersonnelles) = IDeco
                End Select
        End Select
        CadreSaisie.BackImageUrl = "~/Images/Fonds/Fond_Saisie.jpg"
        ConteneurVues.Width = New Unit(820)
        ArmoireCourante.Visible = True
        MultiOnglets.ActiveViewIndex = WebFct.PointeurContexte.Fenetre_VueActive(VI.CategorieRH.InfosPersonnelles)
    End Sub

    Protected Sub ArmoireCourante_ValeurChange(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.DonneeChangeEventArgs) Handles ArmoireCourante.ValeurChange
        Dim Eti As System.Web.UI.WebControls.Label
        Eti = CType(WebFct.VirWebControle(Me.Master, "MsgArmoire", 0), System.Web.UI.WebControls.Label)
        Eti.Text = e.Valeur
        Eti = CType(WebFct.VirWebControle(Me.Master, "MsgArmoirePied", 0), System.Web.UI.WebControls.Label)
        Eti.Text = e.Valeur
    End Sub

    Protected Sub Onglet_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BoutonN1.Click, BoutonN2.Click, _
    BoutonN3.Click, BoutonN4.Click, BoutonN5.Click, BoutonN6.Click, BoutonN7.Click, BoutonN8.Click, BoutonN9.Click

        CadreSaisie.BackImageUrl = "~/Images/Fonds/Fond_Saisie.jpg"
        ConteneurVues.Width = New Unit(820)
        ArmoireCourante.Visible = True
        Select Case CType(sender, Button).ID
            Case Is = "BoutonN1"
                MultiOnglets.SetActiveView(VueSynthese)
            Case Is = "BoutonN2"
                MultiOnglets.SetActiveView(VueEtatCivil)
            Case Is = "BoutonN3"
                MultiOnglets.SetActiveView(VueAdresse)
            Case Is = "BoutonN4"
                MultiOnglets.SetActiveView(VueSitFam)
            Case Is = "BoutonN5"
                MultiOnglets.SetActiveView(VueRIB)
            Case Is = "BoutonN6"
                MultiOnglets.SetActiveView(VueLiens)
            Case Is = "BoutonN7"
                MultiOnglets.SetActiveView(VueAnnotation)
            Case Is = "BoutonN8"
                MultiOnglets.SetActiveView(VueDecoration)
            Case Is = "BoutonN9"
                MultiOnglets.SetActiveView(VueTout)
        End Select
        Dim Eti As System.Web.UI.WebControls.Label
        Eti = CType(WebFct.VirWebControle(Me.Master, "MsgSaisie", 0), System.Web.UI.WebControls.Label)
        Eti.Text = ""

        Eti = CType(WebFct.VirWebControle(Me.Master, "MsgSaisiePied", 0), System.Web.UI.WebControls.Label)
        Eti.Text = ""
        Call Initialiser()
        WebFct.PointeurContexte.Fenetre_VueActive(VI.CategorieRH.InfosPersonnelles) = MultiOnglets.ActiveViewIndex
    End Sub

    Protected Sub Referentiel_RetourEventHandler(ByVal sender As Object, ByVal e As System.EventArgs) Handles Referentiel.RetourEventHandler
        CadreSaisie.BackImageUrl = "~/Images/Fonds/Fond_Saisie.jpg"
        ConteneurVues.Width = New Unit(820)
        ArmoireCourante.Visible = True
        Select Case WebFct.PointeurContexte.SysRef_IDVueRetour
            Case "VueEtatcivil"
                MultiOnglets.SetActiveView(VueEtatCivil)
            Case "VueAdresse"
                MultiOnglets.SetActiveView(VueAdresse)
            Case "VueSitFam"
                MultiOnglets.SetActiveView(VueSitFam)
            Case "VueRIB"
                MultiOnglets.SetActiveView(VueRIB)
            Case "VueAnnotation"
                MultiOnglets.SetActiveView(VueAnnotation)
            Case "VueLiens"
                MultiOnglets.SetActiveView(VueLiens)
            Case "VueDecoration"
                MultiOnglets.SetActiveView(VueDecoration)
        End Select
        WebFct.PointeurContexte.Fenetre_VueActive(VI.CategorieRH.InfosPersonnelles) = MultiOnglets.ActiveViewIndex
    End Sub

    Protected Sub Referentiel_ValeurSelectionnee(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.ValeurSelectionneeEventArgs) Handles Referentiel.ValeurSelectionnee
        CadreSaisie.BackImageUrl = "~/Images/Fonds/Fond_Saisie.jpg"
        ConteneurVues.Width = New Unit(820)
        ArmoireCourante.Visible = True
        Select Case WebFct.PointeurContexte.SysRef_IDVueRetour
            Case "VueEtatcivil"
                Select Case e.ObjetAppelant
                    Case VI.ObjetPer.ObaCivil
                        PER_ETATCIVIL_1.Dontab_RetourAppelTable(e.ControleAppelant) = e.Valeur
                    Case VI.ObjetPer.ObaEtranger
                        PER_PIECE_IDENTITE_31.Dontab_RetourAppelTable(e.ControleAppelant) = e.Valeur
                End Select
                MultiOnglets.SetActiveView(VueEtatCivil)
            Case "VueAdresse"
                Select Case e.ObjetAppelant
                    Case VI.ObjetPer.ObaAdresse
                        PER_DOMICILE_4.Dontab_RetourAppelTable(e.ControleAppelant) = e.Valeur
                    Case VI.ObjetPer.ObaAdresseHorsFr
                        PER_HORSFRANCE_44.Dontab_RetourAppelTable(e.ControleAppelant) = e.Valeur
                    Case VI.ObjetPer.ObaAdresse2
                        PER_DOMICILE_SECOND_72.Dontab_RetourAppelTable(e.ControleAppelant) = e.Valeur
                    Case VI.ObjetPer.ObaAdresseConge
                        PER_DOMICILE_CONGE_73.Dontab_RetourAppelTable(e.ControleAppelant) = e.Valeur
                End Select
                MultiOnglets.SetActiveView(VueAdresse)
            Case "VueSitFam"
                Select Case e.ObjetAppelant
                    Case VI.ObjetPer.ObaPrevenir
                        PER_APREVENIR_30.Dontab_RetourAppelTable(e.ControleAppelant) = e.Valeur
                End Select
                MultiOnglets.SetActiveView(VueSitFam)
            Case "VueRIB"
                Select Case e.ObjetAppelant
                    Case VI.ObjetPer.ObaBanque
                        PER_BANQUE_5.Dontab_RetourAppelTable(e.ControleAppelant) = e.Valeur
                End Select
                MultiOnglets.SetActiveView(VueRIB)
            Case "VueAnnotation"
                Select Case e.ObjetAppelant
                    Case VI.ObjetPer.ObaAgenda
                        PER_NOTEAGENDA_28.Dontab_RetourAppelTable(e.ControleAppelant) = e.Valeur
                End Select
                MultiOnglets.SetActiveView(VueAnnotation)
            Case "VueLiens"
                Select Case e.ObjetAppelant
                    Case VI.ObjetPer.ObaExterne
                        PER_REFEXTERNE_23.Dontab_RetourAppelTable(e.ControleAppelant) = e.Valeur
                    Case VI.ObjetPer.ObaDocuments
                        PER_DOCUMENTS_51.Dontab_RetourAppelTable(e.ControleAppelant) = e.Valeur
                End Select
                MultiOnglets.SetActiveView(VueLiens)
            Case "VueDecoration"
                Select Case e.ObjetAppelant
                    Case VI.ObjetPer.ObaDecoration
                        PER_DECORATION_36.Dontab_RetourAppelTable(e.ControleAppelant) = e.Valeur
                End Select
                MultiOnglets.SetActiveView(VueDecoration)
        End Select
        WebFct.PointeurContexte.Fenetre_VueActive(VI.CategorieRH.InfosPersonnelles) = MultiOnglets.ActiveViewIndex
    End Sub

    Protected Sub PER_AppelTable(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.AppelTableEventArgs) _
    Handles PER_ETATCIVIL_1.AppelTable, PER_DOMICILE_4.AppelTable, PER_HORSFRANCE_44.AppelTable, _
    PER_DOMICILE_SECOND_72.AppelTable, PER_DOMICILE_CONGE_73.AppelTable, PER_REFEXTERNE_23.AppelTable, _
    PER_APREVENIR_30.AppelTable, PER_BANQUE_5.AppelTable, PER_NOTEAGENDA_28.AppelTable, _
    PER_DECORATION_36.AppelTable, PER_PIECE_IDENTITE_31.AppelTable, PER_DOCUMENTS_51.AppelTable

        Select Case e.ObjetAppelant
            Case VI.ObjetPer.ObaCivil
                WebFct.PointeurContexte.SysRef_IDVueRetour = VueEtatCivil.ID
            Case VI.ObjetPer.ObaAdresse, VI.ObjetPer.ObaAdresseHorsFr, VI.ObjetPer.ObaAdresse2, VI.ObjetPer.ObaAdresseConge
                WebFct.PointeurContexte.SysRef_IDVueRetour = VueAdresse.ID
            Case VI.ObjetPer.ObaPrevenir
                WebFct.PointeurContexte.SysRef_IDVueRetour = VueSitFam.ID
            Case VI.ObjetPer.ObaBanque
                WebFct.PointeurContexte.SysRef_IDVueRetour = VueRIB.ID
            Case VI.ObjetPer.ObaAgenda
                WebFct.PointeurContexte.SysRef_IDVueRetour = VueAnnotation.ID
            Case VI.ObjetPer.ObaExterne
                WebFct.PointeurContexte.SysRef_IDVueRetour = VueLiens.ID
            Case VI.ObjetPer.ObaDecoration
                WebFct.PointeurContexte.SysRef_IDVueRetour = VueDecoration.ID
            Case VI.ObjetPer.ObaEtranger
                WebFct.PointeurContexte.SysRef_IDVueRetour = VueEtatCivil.ID
            Case VI.ObjetPer.ObaDocuments
                WebFct.PointeurContexte.SysRef_IDVueRetour = VueLiens.ID
            Case Else
                Exit Sub
        End Select
        ArmoireCourante.Visible = False

        If e.ObjetAppelant = VI.ObjetPer.ObaCivil And e.ControleAppelant = "Dontab06" Then
            Referentiel.V_DuoTable(e.PointdeVueInverse, e.NomdelaTable, e.PointdeVueInverse) = "Pays"
        Else
            Referentiel.V_PointdeVue = e.PointdeVueInverse
            Referentiel.V_NomTable = e.NomdelaTable
        End If
        Referentiel.V_Appelant(e.ObjetAppelant) = e.ControleAppelant
        CadreSaisie.BackImageUrl = ""
        CadreSaisie.BackColor = WebFct.ConvertCouleur("#216B68")
        MultiOnglets.SetActiveView(VueSysRef)
        WebFct.PointeurContexte.Fenetre_VueActive(VI.CategorieRH.InfosPersonnelles) = MultiOnglets.ActiveViewIndex
        ConteneurVues.Width = New Unit(1150)

    End Sub

    Protected Sub MessageDialogue(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.MessageSaisieEventArgs) _
    Handles ArmoireCourante.MessageDialogue, PER_ETATCIVIL_1.MessageDialogue, PER_DOMICILE_4.MessageDialogue, PER_DOMICILE_SECOND_72.MessageDialogue, _
    PER_DOMICILE_CONGE_73.MessageDialogue, PER_HORSFRANCE_44.MessageDialogue, PER_ENFANT_3.MessageDialogue, _
    PER_CONJOINT_2.MessageDialogue, PER_APREVENIR_30.MessageDialogue, PER_BANQUE_5.MessageDialogue, _
    PER_NOTEAGENDA_28.MessageDialogue, PER_REFEXTERNE_23.MessageDialogue, PER_DECORATION_36.MessageDialogue, _
    PER_PIECE_IDENTITE_31.MessageDialogue, PER_DOCUMENTS_51.MessageDialogue

        MsgVirtualia.AfficherMessage = e
        CadreSaisie.BackImageUrl = ""
        CadreSaisie.BackColor = WebFct.ConvertCouleur("#216B68")
        ArmoireCourante.Visible = False
        MultiOnglets.SetActiveView(VueMessage)
        WebFct.PointeurContexte.Fenetre_VueActive(VI.CategorieRH.InfosPersonnelles) = MultiOnglets.ActiveViewIndex
        ConteneurVues.Width = New Unit(1150)
    End Sub

    Protected Sub MessageSaisie(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.MessageSaisieEventArgs) _
    Handles PER_ETATCIVIL_1.MessageSaisie, PER_DOMICILE_4.MessageSaisie, PER_DOMICILE_SECOND_72.MessageSaisie, _
    PER_DOMICILE_CONGE_73.MessageSaisie, PER_HORSFRANCE_44.MessageSaisie, PER_ENFANT_3.MessageSaisie, _
    PER_CONJOINT_2.MessageSaisie, PER_APREVENIR_30.MessageSaisie, PER_BANQUE_5.MessageSaisie, PER_ANNOTATION_20.MessageSaisie, _
    PER_NOTEAGENDA_28.MessageSaisie, PER_REFEXTERNE_23.MessageSaisie, PER_DECORATION_36.MessageSaisie, _
    PER_PIECE_IDENTITE_31.MessageSaisie, PER_DOCUMENTS_51.MessageSaisie

        Dim Eti As System.Web.UI.WebControls.Label
        Eti = CType(WebFct.VirWebControle(Me.Master, "MsgSaisie", 0), System.Web.UI.WebControls.Label)
        Select Case e.NatureMessage
            Case Is = "OK"
                Eti.ForeColor = Drawing.Color.Green
        End Select
        Eti.Text = e.ContenuMessage

        Eti = CType(WebFct.VirWebControle(Me.Master, "MsgSaisiePied", 0), System.Web.UI.WebControls.Label)
        Select Case e.NatureMessage
            Case Is = "OK"
                Eti.ForeColor = Drawing.Color.Green
        End Select
        Eti.Text = e.ContenuMessage

    End Sub

    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        Dim NumSession As String = Me.Master.V_NoSession
        If NumSession = "" Then
            NumSession = Server.HtmlDecode(Request.QueryString("IDVirtualia"))
            Me.Master.V_NoSession = NumSession
        End If
        If NumSession = "" Then
            Dim Msg As String = "Erreur lors de l'identification."
            Response.Redirect("~/Fenetres/Connexion/ErreurApplication.aspx?Msg=" & Msg)
        End If
        WebFct = New Virtualia.Net.WebFonctions(Me, 1)
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        If WebFct.PointeurContexte.Armoire_Identifiant <> 0 Then
            Identifiant = WebFct.PointeurContexte.Armoire_Identifiant
            MultiOnglets.ActiveViewIndex = WebFct.PointeurContexte.Fenetre_VueActive(VI.CategorieRH.InfosPersonnelles)
            Select Case MultiOnglets.ActiveViewIndex
                Case Is = IAlbum
                    If WebFct.PointeurContexte.Fenetre_VuePrecedente(VI.CategorieRH.InfosPersonnelles) <> IAlbum Then
                        Call ChargerAlbum(0, 0)
                    End If
                Case Else
                    Call Initialiser()
            End Select
        End If
    End Sub

    Protected Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
        WebFct.Dispose()
    End Sub

    Private Sub Initialiser()
        Dim Ctl As Control
        Dim BtnControle As System.Web.UI.WebControls.Button
        Dim IndiceI As Integer = 0
        Dim K As Integer = 0
        Do
            Ctl = WebFct.VirWebControle(Me.CadreOnglets, "Bouton", IndiceI)
            If Ctl Is Nothing Then
                Exit Do
            End If
            BtnControle = CType(Ctl, System.Web.UI.WebControls.Button)
            K = CInt(Strings.Right(BtnControle.ID, 1)) - 1
            If K = MultiOnglets.ActiveViewIndex Then
                BtnControle.Font.Bold = True
                BtnControle.Height = New Unit(24)
                BtnControle.Width = New Unit(105)
                BtnControle.BackColor = WebFct.ConvertCouleur("#7D9F99")
                BtnControle.ForeColor = System.Drawing.Color.White
            Else
                BtnControle.Font.Bold = False
                BtnControle.Height = New Unit(33)
                BtnControle.Width = New Unit(118)
                BtnControle.BackColor = System.Drawing.Color.Transparent
                BtnControle.ForeColor = WebFct.ConvertCouleur("#142425")
            End If
            IndiceI += 1
        Loop
        Select Case MultiOnglets.ActiveViewIndex
            Case ISysref, IEdition, IMessage
                ConteneurVues.Width = New Unit(1150)
                ArmoireCourante.Visible = False
            Case Else
                ConteneurVues.Width = New Unit(820)
                ArmoireCourante.Visible = True
        End Select

    End Sub

    Private Sub ChargerAlbum(ByVal PtdeVue As Short, ByVal Outil As Short)
        If PtdeVue = 0 Then
            PtdeVue = WebFct.PointeurContexte.Fenetre_PointdeVue
            Outil = WebFct.PointeurContexte.Fenetre_Outil
        End If
        AlbumRequetes.V_PointdeVue = PtdeVue
        AlbumRequetes.V_Outil = Outil
        Call Initialiser()
        CadreSaisie.BackImageUrl = ""
        CadreSaisie.BackColor = WebFct.ConvertCouleur("#216B68")
        WebFct.PointeurContexte.Fenetre_PointdeVue = PtdeVue
        WebFct.PointeurContexte.Fenetre_Outil = Outil
        WebFct.PointeurContexte.Fenetre_VueActive(VI.CategorieRH.InfosPersonnelles) = MultiOnglets.ActiveViewIndex
    End Sub

    Private Sub FaireSynthese(ByVal Ide_Dossier As Integer)
        Dim Dossier As Virtualia.Ressources.Datas.ObjetDossierPER = WebFct.PointeurDossier(Ide_Dossier)
        Dim VEnr As ArrayList
        Dim RepRelatif As String

        If Dossier Is Nothing Then
            Exit Sub
        End If

        CadreSaisie.BackImageUrl = "~/Images/Fonds/Fond_Saisie.jpg"
        ConteneurVues.Width = New Unit(820)
        If Application.Item("CheminRelatif") Is Nothing Then
            If Request.ApplicationPath <> "/" Then
                Application.Add("CheminRelatif", Request.ApplicationPath & "/")
            Else
                Application.Add("CheminRelatif", "/")
            End If
        End If
        RepRelatif = Application.Item("CheminRelatif").ToString

        VEnr = New ArrayList
        VEnr.Add(Dossier.SyntheseInfosPersonnelles(WebFct.PointeurUtilisateur.V_NomdUtilisateurSgbd, RepRelatif))
        Synthese.V_Liste = VEnr
    End Sub

    Private Sub FaireVisualiserTout(ByVal Ide_Dossier As Integer)
        Dim ListeObjet = New System.Text.StringBuilder
        Dim Cretour As Boolean
        Dim IndiceI As Integer
        Dim IndiceK As Integer
        Dim Dossier As Virtualia.Ressources.Datas.ObjetDossierPER = WebFct.PointeurDossier(Ide_Dossier)
        Dim Vlisteall As ArrayList
        Dim RepRelatif As String

        If Dossier Is Nothing Then
            Exit Sub
        End If
        CadreSaisie.BackImageUrl = "~/Images/Fonds/Fond_Saisie.jpg"
        ConteneurVues.Width = New Unit(820)

        If Application.Item("CheminRelatif") Is Nothing Then
            If Request.ApplicationPath <> "/" Then
                Application.Add("CheminRelatif", Request.ApplicationPath & "/")
            Else
                Application.Add("CheminRelatif", "/")
            End If
        End If
        RepRelatif = Application.Item("CheminRelatif").ToString

        ListeObjet.Append(VI.ObjetPer.ObaCivil & VI.PointVirgule)
        ListeObjet.Append(VI.ObjetPer.ObaAdresse & VI.PointVirgule)
        ListeObjet.Append(VI.ObjetPer.ObaAdresse2 & VI.PointVirgule)
        ListeObjet.Append(VI.ObjetPer.ObaAdresseConge & VI.PointVirgule)
        ListeObjet.Append(VI.ObjetPer.ObaAdresseHorsFr & VI.PointVirgule)
        ListeObjet.Append(VI.ObjetPer.ObaConjoint & VI.PointVirgule)
        ListeObjet.Append(VI.ObjetPer.ObaEnfant & VI.PointVirgule)
        ListeObjet.Append(VI.ObjetPer.ObaBanque & VI.PointVirgule)
        ListeObjet.Append(VI.ObjetPer.ObaPrevenir & VI.PointVirgule)
        ListeObjet.Append(VI.ObjetPer.ObaMemento & VI.PointVirgule)
        ListeObjet.Append(VI.ObjetPer.ObaAgenda & VI.PointVirgule)
        ListeObjet.Append(VI.ObjetPer.ObaExterne & VI.PointVirgule)
        ListeObjet.Append(VI.ObjetPer.ObaEtranger & VI.PointVirgule)
        ListeObjet.Append(VI.ObjetPer.ObaDecoration & VI.PointVirgule)
        ListeObjet.Append(VI.ObjetPer.ObaDocuments & VI.PointVirgule)
        ListeObjet.Append(VI.ObjetPer.ObaVirtualia & VI.PointVirgule)

        Cretour = Dossier.LireDossier(WebFct.PointeurUtilisateur.V_NomdUtilisateurSgbd, ListeObjet.ToString, 1)

        Vlisteall = New ArrayList
        Vlisteall.Add(VI.PointdeVue.PVueApplicatif)

        IndiceI = Dossier.V_IndexTableDescendant("PER_ETATCIVIL")
        Vlisteall.Add(VI.ObjetPer.ObaCivil)
        Vlisteall.Add(CType(Dossier.Item(IndiceI), Virtualia.TablesObjet.ShemaPER.PER_ETATCIVIL).ContenuTable & VI.SigneBarre)

        IndiceI = Dossier.V_IndexTableDescendant("PER_PIECE_IDENTITE")
        If IndiceI <> -1 Then
            Vlisteall.Add(VI.ObjetPer.ObaEtranger)
            Vlisteall.Add(CType(Dossier.Item(IndiceI), Virtualia.TablesObjet.ShemaPER.PER_PIECE_IDENTITE).ContenuTable & VI.SigneBarre)
        End If

        IndiceK = 0
        Do
            IndiceI = Dossier.V_IndexFiche(IndiceK, "PER_ENFANT")
            If IndiceI = -1 Then
                Exit Do
            End If
            If CType(Dossier.Item(IndiceI), Virtualia.TablesObjet.ShemaPER.PER_ENFANT).Date_de_naissance <> "" Then
                Vlisteall.Add(VI.ObjetPer.ObaEnfant)
                Vlisteall.Add(CType(Dossier.Item(IndiceI), Virtualia.TablesObjet.ShemaPER.PER_ENFANT).ContenuTable & VI.SigneBarre)
            End If
            IndiceK += 1
        Loop
        IndiceI = Dossier.V_IndexTableDescendant("PER_CONJOINT")
        If IndiceI <> -1 Then
            Vlisteall.Add(VI.ObjetPer.ObaConjoint)
            Vlisteall.Add(CType(Dossier.Item(IndiceI), Virtualia.TablesObjet.ShemaPER.PER_CONJOINT).ContenuTable & VI.SigneBarre)
        End If
        IndiceI = Dossier.V_IndexTableDescendant("PER_APREVENIR")
        If IndiceI <> -1 Then
            Vlisteall.Add(VI.ObjetPer.ObaPrevenir)
            Vlisteall.Add(CType(Dossier.Item(IndiceI), Virtualia.TablesObjet.ShemaPER.PER_APREVENIR).ContenuTable & VI.SigneBarre)
        End If

        IndiceI = Dossier.V_IndexTableDescendant("PER_BANQUE")
        If IndiceI <> -1 Then
            Vlisteall.Add(VI.ObjetPer.ObaBanque)
            Vlisteall.Add(CType(Dossier.Item(IndiceI), Virtualia.TablesObjet.ShemaPER.PER_BANQUE).ContenuTable & VI.SigneBarre)
        End If

        IndiceI = Dossier.V_IndexTableDescendant("PER_DOMICILE")
        If IndiceI <> -1 Then
            Vlisteall.Add(VI.ObjetPer.ObaAdresse)
            Vlisteall.Add(CType(Dossier.Item(IndiceI), Virtualia.TablesObjet.ShemaPER.PER_DOMICILE).ContenuTable & VI.SigneBarre)
        End If
        IndiceI = Dossier.V_IndexTableDescendant("PER_DOMICILE_SECOND")
        If IndiceI <> -1 Then
            Vlisteall.Add(VI.ObjetPer.ObaAdresse2)
            Vlisteall.Add(CType(Dossier.Item(IndiceI), Virtualia.TablesObjet.ShemaPER.PER_DOMICILE_SECOND).ContenuTable & VI.SigneBarre)
        End If
        IndiceI = Dossier.V_IndexTableDescendant("PER_DOMICILE_CONGE")
        If IndiceI <> -1 Then
            Vlisteall.Add(VI.ObjetPer.ObaAdresseConge)
            Vlisteall.Add(CType(Dossier.Item(IndiceI), Virtualia.TablesObjet.ShemaPER.PER_DOMICILE_CONGE).ContenuTable & VI.SigneBarre)
        End If
        IndiceI = Dossier.V_IndexTableDescendant("PER_HORSFRANCE")
        If IndiceI <> -1 Then
            Vlisteall.Add(VI.ObjetPer.ObaAdresseHorsFr)
            Vlisteall.Add(CType(Dossier.Item(IndiceI), Virtualia.TablesObjet.ShemaPER.PER_HORSFRANCE).ContenuTable & VI.SigneBarre)
        End If

        IndiceI = Dossier.V_IndexTableDescendant("PER_REFEXTERNE")
        If IndiceI <> -1 Then
            Vlisteall.Add(VI.ObjetPer.ObaExterne)
            Vlisteall.Add(CType(Dossier.Item(IndiceI), Virtualia.TablesObjet.ShemaPER.PER_REFEXTERNE).ContenuTable & VI.SigneBarre)
        End If
        IndiceK = 0
        Do
            IndiceI = Dossier.V_IndexFiche(IndiceK, "PER_DOCUMENTS")
            If IndiceI = -1 Then
                Exit Do
            End If
            If CType(Dossier.Item(IndiceI), Virtualia.TablesObjet.ShemaPER.PER_DOCUMENTS).Chrono <> "" Then
                Vlisteall.Add(VI.ObjetPer.ObaDocuments)
                Vlisteall.Add(CType(Dossier.Item(IndiceI), Virtualia.TablesObjet.ShemaPER.PER_DOCUMENTS).ContenuTable & VI.SigneBarre)
            End If
            IndiceK += 1
        Loop

        IndiceK = 0
        Do
            IndiceI = Dossier.V_IndexFiche(IndiceK, "PER_NOTEAGENDA")
            If IndiceI = -1 Then
                Exit Do
            End If
            If CType(Dossier.Item(IndiceI), Virtualia.TablesObjet.ShemaPER.PER_NOTEAGENDA).Date_de_Valeur <> "" Then
                Vlisteall.Add(VI.ObjetPer.ObaAgenda)
                Vlisteall.Add(CType(Dossier.Item(IndiceI), Virtualia.TablesObjet.ShemaPER.PER_NOTEAGENDA).ContenuTable & VI.SigneBarre)
            End If
            IndiceK += 1
        Loop
        IndiceI = Dossier.V_IndexTableDescendant("PER_ANNOTATION")
        If IndiceI <> -1 Then
            Vlisteall.Add(VI.ObjetPer.ObaMemento)
            Vlisteall.Add(CType(Dossier.Item(IndiceI), Virtualia.TablesObjet.ShemaPER.PER_ANNOTATION).ContenuTable & VI.SigneBarre)
        End If

        IndiceK = 0
        Do
            IndiceI = Dossier.V_IndexFiche(IndiceK, "PER_DECORATION")
            If IndiceI = -1 Then
                Exit Do
            End If
            If CType(Dossier.Item(IndiceI), Virtualia.TablesObjet.ShemaPER.PER_DECORATION).Date_d_effet <> "" Then
                Vlisteall.Add(VI.ObjetPer.ObaDecoration)
                Vlisteall.Add(CType(Dossier.Item(IndiceI), Virtualia.TablesObjet.ShemaPER.PER_DECORATION).ContenuTable & VI.SigneBarre)
            End If
            IndiceK += 1
        Loop

        IndiceI = Dossier.V_IndexTableDescendant("PER_COMPTE_RENDU")
        If IndiceI <> -1 Then
            Vlisteall.Add(VI.ObjetPer.ObaVirtualia)
            Vlisteall.Add(CType(Dossier.Item(IndiceI), Virtualia.TablesObjet.ShemaPER.PER_COMPTE_RENDU).ContenuTable & VI.SigneBarre)
        End If

        Visutout.V_Liste = Vlisteall
    End Sub

    Protected Sub AlbumRequetes_ScriptResultat(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.ScriptResultatEventArgs) Handles AlbumRequetes.ScriptResultat
        Dim I As Integer
        For I = 0 To e.Centrage_Colonne.Count - 1
            EditionArmoire.Centrage_Colonne(I) = e.Centrage_Colonne(I)
        Next I
        ArmoireCourante.Visible = False
        EditionArmoire.VDataGrid(e.IntitulesColonnes, e.Libelles) = e.Valeurs
        EditionArmoire.VIntitule = AlbumRequetes.VTitre
        MultiOnglets.SetActiveView(VueEdition)
        WebFct.PointeurContexte.Fenetre_VueActive(VI.CategorieRH.InfosPersonnelles) = MultiOnglets.ActiveViewIndex
    End Sub

    Protected Sub EditionArmoire_RetourEventHandler(ByVal sender As Object, ByVal e As System.EventArgs) Handles EditionArmoire.RetourEventHandler
        MultiOnglets.ActiveViewIndex = IAlbum
        WebFct.PointeurContexte.Fenetre_VueActive(VI.CategorieRH.InfosPersonnelles) = MultiOnglets.ActiveViewIndex
        Call Initialiser()
    End Sub

    Protected Sub AlbumRequetes_ValeurChange(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.DonneeChangeEventArgs) Handles AlbumRequetes.ValeurChange
        WebFct.PointeurContexte.Armoire_Identifiant = 0
        ArmoireCourante.ArmoirePersonnalisee = e.Valeur

        Dim Eti As System.Web.UI.WebControls.Label
        Eti = CType(WebFct.VirWebControle(Me.Master, "MsgCourant", 0), System.Web.UI.WebControls.Label)
        Eti.Text = ""
        Eti = CType(WebFct.VirWebControle(Me.Master, "MsgCourantPied", 0), System.Web.UI.WebControls.Label)
        Eti.Text = ""

        Eti = CType(WebFct.VirWebControle(Me.Master, "MsgArmoire", 0), System.Web.UI.WebControls.Label)
        Eti.Text = e.Valeur & Strings.Space(1) & ArmoireCourante.CompteDossiers
        Eti = CType(WebFct.VirWebControle(Me.Master, "MsgArmoirePied", 0), System.Web.UI.WebControls.Label)
        Eti.Text = e.Valeur & Strings.Space(1) & ArmoireCourante.CompteDossiers
    End Sub

    Private Sub SupprimerDossier(ByVal MsgReponse As String)
        If MsgReponse = "Non" Then
            CadreSaisie.BackImageUrl = "~/Images/Fonds/Fond_Saisie.jpg"
            ConteneurVues.Width = New Unit(820)
            ArmoireCourante.Visible = True
            MultiOnglets.ActiveViewIndex = 0
            Exit Sub
        End If
        Select Case MsgReponse
            Case "OK", "KO"
                If MsgReponse = "OK" Then
                    Me.Master.IdentifiantCourant = 0
                    Identifiant = 0
                    WebFct.PointeurContexte.Armoire_Identifiant = 0
                    ArmoireCourante.Actualiser()
                End If
                CadreSaisie.BackImageUrl = "~/Images/Fonds/Fond_Saisie.jpg"
                ConteneurVues.Width = New Unit(820)
                WebFct.PointeurContexte.Fenetre_VueActive(VI.CategorieRH.InfosPersonnelles) = 0
                MultiOnglets.SetActiveView(VueSynthese)
                ArmoireCourante.Visible = True
                Exit Sub
        End Select
        '** Traitement du Oui à la suppression
        Dim Cretour As Boolean
        Dim Ide As Integer = WebFct.PointeurContexte.Armoire_Identifiant
        Dim Dossier As Virtualia.Ressources.Datas.ObjetDossierPER
        Dossier = WebFct.PointeurDossier(Ide)
        If Dossier Is Nothing Then
            Exit Sub
        End If
        Dim NomPrenom As String = Dossier.Nom & Strings.Space(1) & Dossier.Prenom

        Cretour = Dossier.V_SupprimerDossier(WebFct.PointeurUtilisateur.V_NomdUtilisateurSgbd, WebFct.PointeurUtilisateur.V_NomdeConnexion, _
                                                    WebFct.PointeurUtilisateur.InstanceBd)

        Dim TitreMsg As String = "Supprimer un dossier"
        Dim ContenuMsg As String = "le dossier N° " & Ide.ToString & " - " & NomPrenom
        Dim Evenement As Virtualia.Systeme.Evenements.MessageSaisieEventArgs
        If Cretour = True Then
            ContenuMsg &= Strings.Chr(13) & Strings.Chr(10) & "a bien été supprimé."
            WebFct.PointeurArmoire.RetirerDossier(Dossier)
            Evenement = New Virtualia.Systeme.Evenements.MessageSaisieEventArgs("SuppDossier", 1, "", "OK", TitreMsg, ContenuMsg)
        Else
            ContenuMsg &= Strings.Chr(13) & Strings.Chr(10) & "n'a pas pu être supprimé"
            Evenement = New Virtualia.Systeme.Evenements.MessageSaisieEventArgs("SuppDossier", 1, "", "KO", TitreMsg, ContenuMsg)
        End If

        MsgVirtualia.AfficherMessage = Evenement
        CadreSaisie.BackImageUrl = ""
        CadreSaisie.BackColor = WebFct.ConvertCouleur("#216B68")
        ArmoireCourante.Visible = False
        MultiOnglets.SetActiveView(VueMessage)
        ConteneurVues.Width = New Unit(1150)
    End Sub
End Class
