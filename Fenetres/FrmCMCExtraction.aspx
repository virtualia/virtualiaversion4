﻿<%@ Page Language="VB" MasterPageFile="~/PagesMaitre/VirtualiaMain.master" AutoEventWireup="false" 
    Inherits="Virtualia.Net.Fenetres_FrmCMCExtraction" Codebehind="FrmCMCExtraction.aspx.vb"
    MaintainScrollPositionOnPostback="True" %>
<%@ MasterType VirtualPath="~/PagesMaitre/VirtualiaMain.master"  %>

<%@ Register src="../ControlesWebUser/Outils/RechercheCMC.ascx" tagname="RechercheCMC" tagprefix="Virtualia" %>
<%@ Register src="../ControlesWebUser/Outils/ChoixValeurs.ascx" tagname="ChoixValeurs" tagprefix="Virtualia" %>
<%@ Register src="../ControlesWebUser/Outils/ExtractionDatas.ascx" tagname="ExtractionDatas" tagprefix="Virtualia" %>
<%@ Register src="../ControlesWebUser/Outils/OptionInformation.ascx" tagname="OptionInformation" tagprefix="Virtualia" %>
<%@ Register src="../ControlesWebUser/Outils/EditionExtraction.ascx" tagname="EditionExtraction" tagprefix="Virtualia" %>
<%@ Register src="../ControlesWebUser/ObjetsArmoire/ListeReferences.ascx" tagname="ListeReferences" tagprefix="VirtuRef" %>

<asp:Content ID="CadreGeneral" runat="server" ContentPlaceHolderID="CorpsMaster">
    <asp:UpdatePanel ID="UpdatePanelCMC" runat="server">
        <ContentTemplate>
          <asp:Table ID="TableOnglets" runat="server" Height="35px" Width="1150px" HorizontalAlign="Center"
                     style="margin-top: 0px">
             <asp:TableRow>
                <asp:TableCell>
                    <asp:Table ID="CadreOnglets" runat="server" Height="35px" Width="374px" HorizontalAlign="Left"
                           CellPadding="0" CellSpacing="0" BackImageUrl="~/Images/Onglets/Onglet_2bords_187.bmp"
                           style="margin-top: 10px; border-top-color: #124545;
                            border-bottom-color: #B0E0D7; border-bottom-style: groove" >
                    <asp:TableRow>
                        <asp:TableCell HorizontalAlign ="Center" VerticalAlign="Bottom" Width="185px">
                            <asp:Button ID="BoutonN1" runat="server" Height="24px" Width="170px" 
                                ForeColor="White" Text="Constituer des armoires" Font-Bold="true" Font-Italic="true"
                                Font-Names="Trebuchet MS" BackColor="#6C9690" BorderStyle="None" 
                                style="padding-left: 1px; margin-left: 1px; margin-bottom: 2px; text-align: center" />  
                        </asp:TableCell>
                        <asp:TableCell HorizontalAlign ="Center" VerticalAlign="Bottom" Width="185px">
                            <asp:Button ID="BoutonN2" runat="server" Height="33px" Width="185px" 
                                ForeColor="#142425" Text="Elaborer des listes" Font-Bold="false" Font-Italic="true"
                                Font-Names="Trebuchet MS" BackColor="Transparent" BorderStyle="None" 
                                style="padding-left: 1px; margin-left: 1px; margin-bottom: 2px; text-align: center" />  
                        </asp:TableCell>
                    </asp:TableRow>
                  </asp:Table>
                </asp:TableCell>
              </asp:TableRow>
            </asp:Table>
            <asp:Table ID="CadreCMCEdition" runat="server" Height="1220px" Width="1150px" HorizontalAlign="Center"
                     BackColor="#82BEA1" CellSpacing="2" style="margin-top: 0px " >
               <asp:TableRow>
                   <asp:TableCell ID="ConteneurVues" Height="1220px" Width="1150px" VerticalAlign="Top" HorizontalAlign="Center">
                       <asp:MultiView ID="MultiOnglets" runat="server" ActiveViewIndex="0">
                          <asp:View ID="VueCMC" runat="server">  
                             <Virtualia:RechercheCMC ID="CMC" runat="server" />  
                           </asp:View>  
                          <asp:View ID="VueExtraction" runat="server">  
                              <Virtualia:ExtractionDatas ID="Extraction" runat="server" />  
                          </asp:View>  
                          <asp:View ID="VueChoix" runat="server">  
                              <Virtualia:ChoixValeurs ID="Choix" runat="server"
                                SiCadreValeursVisible="false"  />  
                          </asp:View>
                          <asp:View ID="VueOption" runat="server">  
                              <Virtualia:OptionInformation ID="OptionInfo" runat="server" />  
                          </asp:View>  
                          <asp:View ID="VueResultat" runat="server">  
                               <Virtualia:EditionExtraction ID="EditionResultat" runat="server" />  
                          </asp:View>   
                          <asp:View ID="VueSysRef" runat="server">
                             <VirtuRef:ListeReferences ID="Referentiel" runat="server"
                                       SiListeMenuVisible="false" />
                          </asp:View>   
                      </asp:MultiView>
                  </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:HiddenField ID="LigneW" runat="server" Value="0" />
                        <asp:HiddenField ID="RetourW" runat="server" Value="CMC" />
                    </asp:TableCell>
                </asp:TableRow>
             </asp:Table>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
