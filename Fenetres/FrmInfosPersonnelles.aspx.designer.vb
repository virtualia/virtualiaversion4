﻿'------------------------------------------------------------------------------
' <généré automatiquement>
'     Ce code a été généré par un outil.
'
'     Les modifications apportées à ce fichier peuvent provoquer un comportement incorrect et seront perdues si
'     le code est régénéré.
' </généré automatiquement>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class Fenetres_FrmInfosPersonnelles

    '''<summary>
    '''Contrôle UpdatePanelSaisie.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents UpdatePanelSaisie As Global.System.Web.UI.UpdatePanel

    '''<summary>
    '''Contrôle TableOnglets.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents TableOnglets As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle CadreOnglets.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CadreOnglets As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle BoutonN1.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents BoutonN1 As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Contrôle BoutonN2.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents BoutonN2 As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Contrôle BoutonN3.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents BoutonN3 As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Contrôle BoutonN4.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents BoutonN4 As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Contrôle BoutonN5.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents BoutonN5 As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Contrôle BoutonN6.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents BoutonN6 As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Contrôle BoutonN7.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents BoutonN7 As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Contrôle BoutonN8.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents BoutonN8 As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Contrôle BoutonN9.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents BoutonN9 As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Contrôle CadreSaisie.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CadreSaisie As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle ConteneurVues.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents ConteneurVues As Global.System.Web.UI.WebControls.TableCell

    '''<summary>
    '''Contrôle MultiOnglets.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents MultiOnglets As Global.System.Web.UI.WebControls.MultiView

    '''<summary>
    '''Contrôle VueSynthese.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VueSynthese As Global.System.Web.UI.WebControls.View

    '''<summary>
    '''Contrôle Synthese.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Synthese As Global.Virtualia.Net.Controles_InfosPersonnelles

    '''<summary>
    '''Contrôle VueEtatCivil.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VueEtatCivil As Global.System.Web.UI.WebControls.View

    '''<summary>
    '''Contrôle PER_ETATCIVIL_1.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PER_ETATCIVIL_1 As Global.Virtualia.Net.Fenetre_PER_ETATCIVIL

    '''<summary>
    '''Contrôle PER_PIECE_IDENTITE_31.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PER_PIECE_IDENTITE_31 As Global.Virtualia.Net.Fenetre_PER_PIECE_IDENTITE

    '''<summary>
    '''Contrôle VueAdresse.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VueAdresse As Global.System.Web.UI.WebControls.View

    '''<summary>
    '''Contrôle PER_DOMICILE_4.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PER_DOMICILE_4 As Global.Virtualia.Net.Fenetre_PER_DOMICILE

    '''<summary>
    '''Contrôle PER_DOMICILE_SECOND_72.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PER_DOMICILE_SECOND_72 As Global.Virtualia.Net.Fenetre_PER_DOMICILE_SECOND

    '''<summary>
    '''Contrôle PER_DOMICILE_CONGE_73.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PER_DOMICILE_CONGE_73 As Global.Virtualia.Net.Fenetre_PER_DOMICILE_CONGE

    '''<summary>
    '''Contrôle PER_HORSFRANCE_44.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PER_HORSFRANCE_44 As Global.Virtualia.Net.Fenetre_PER_HORSFRANCE

    '''<summary>
    '''Contrôle VueSitFam.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VueSitFam As Global.System.Web.UI.WebControls.View

    '''<summary>
    '''Contrôle PER_ENFANT_3.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PER_ENFANT_3 As Global.Virtualia.Net.Fenetre_PER_ENFANT

    '''<summary>
    '''Contrôle PER_CONJOINT_2.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PER_CONJOINT_2 As Global.Virtualia.Net.Fenetre_PER_CONJOINT

    '''<summary>
    '''Contrôle PER_APREVENIR_30.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PER_APREVENIR_30 As Global.Virtualia.Net.Fenetre_PER_APREVENIR

    '''<summary>
    '''Contrôle VueRIB.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VueRIB As Global.System.Web.UI.WebControls.View

    '''<summary>
    '''Contrôle PER_BANQUE_5.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PER_BANQUE_5 As Global.Virtualia.Net.Fenetre_PER_BANQUE

    '''<summary>
    '''Contrôle VueLiens.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VueLiens As Global.System.Web.UI.WebControls.View

    '''<summary>
    '''Contrôle PER_REFEXTERNE_23.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PER_REFEXTERNE_23 As Global.Virtualia.Net.Fenetre_PER_REFEXTERNE

    '''<summary>
    '''Contrôle PER_DOCUMENTS_51.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PER_DOCUMENTS_51 As Global.Virtualia.Net.Fenetre_PER_DOCUMENTS

    '''<summary>
    '''Contrôle VueAnnotation.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VueAnnotation As Global.System.Web.UI.WebControls.View

    '''<summary>
    '''Contrôle PER_NOTEAGENDA_28.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PER_NOTEAGENDA_28 As Global.Virtualia.Net.Fenetre_PER_NOTEAGENDA

    '''<summary>
    '''Contrôle PER_ANNOTATION_20.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PER_ANNOTATION_20 As Global.Virtualia.Net.Fenetre_PER_ANNOTATION

    '''<summary>
    '''Contrôle VueDecoration.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VueDecoration As Global.System.Web.UI.WebControls.View

    '''<summary>
    '''Contrôle PER_DECORATION_36.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PER_DECORATION_36 As Global.Virtualia.Net.Fenetre_PER_DECORATION

    '''<summary>
    '''Contrôle VueTout.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VueTout As Global.System.Web.UI.WebControls.View

    '''<summary>
    '''Contrôle Visutout.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Visutout As Global.Virtualia.Net.Controles_VisualiserTout

    '''<summary>
    '''Contrôle VueSysRef.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VueSysRef As Global.System.Web.UI.WebControls.View

    '''<summary>
    '''Contrôle Referentiel.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Referentiel As Global.Virtualia.Net.Controles_ListeReferences

    '''<summary>
    '''Contrôle VueAlbum.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VueAlbum As Global.System.Web.UI.WebControls.View

    '''<summary>
    '''Contrôle AlbumRequetes.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents AlbumRequetes As Global.Virtualia.Net.Controles_ObjetsArmoire_AlbumdesScripts

    '''<summary>
    '''Contrôle VueEdition.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VueEdition As Global.System.Web.UI.WebControls.View

    '''<summary>
    '''Contrôle EditionArmoire.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EditionArmoire As Global.Virtualia.Net.VirtualiaOutils_EditionExtraction

    '''<summary>
    '''Contrôle VueMessage.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VueMessage As Global.System.Web.UI.WebControls.View

    '''<summary>
    '''Contrôle MsgVirtualia.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents MsgVirtualia As Global.Virtualia.Net.Controles_VMessage

    '''<summary>
    '''Contrôle ArmoireCourante.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents ArmoireCourante As Global.Virtualia.Net.Controles_ArmoirePersonnes

    '''<summary>
    '''Contrôle HSelIde.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents HSelIde As Global.System.Web.UI.WebControls.HiddenField

    '''<summary>
    '''Propriété Master.
    '''</summary>
    '''<remarks>
    '''Propriété générée automatiquement.
    '''</remarks>
    Public Shadows ReadOnly Property Master() As Virtualia.Net.MasterPage
        Get
            Return CType(MyBase.Master, Virtualia.Net.MasterPage)
        End Get
    End Property
End Class
