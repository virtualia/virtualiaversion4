﻿'------------------------------------------------------------------------------
' <généré automatiquement>
'     Ce code a été généré par un outil.
'
'     Les modifications apportées à ce fichier peuvent provoquer un comportement incorrect et seront perdues si
'     le code est régénéré.
' </généré automatiquement>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class Fenetres_FrmTempsTravail

    '''<summary>
    '''Contrôle UpdatePanelSaisie.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents UpdatePanelSaisie As Global.System.Web.UI.UpdatePanel

    '''<summary>
    '''Contrôle TableOnglets.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents TableOnglets As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle CadreOnglets.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CadreOnglets As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle BoutonN1.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents BoutonN1 As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Contrôle BoutonN2.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents BoutonN2 As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Contrôle BoutonN3.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents BoutonN3 As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Contrôle BoutonN4.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents BoutonN4 As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Contrôle BoutonN5.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents BoutonN5 As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Contrôle BoutonN6.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents BoutonN6 As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Contrôle BoutonN7.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents BoutonN7 As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Contrôle BoutonN8.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents BoutonN8 As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Contrôle CadreSaisie.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CadreSaisie As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle ConteneurVues.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents ConteneurVues As Global.System.Web.UI.WebControls.TableCell

    '''<summary>
    '''Contrôle MultiOnglets.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents MultiOnglets As Global.System.Web.UI.WebControls.MultiView

    '''<summary>
    '''Contrôle VueSituation.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VueSituation As Global.System.Web.UI.WebControls.View

    '''<summary>
    '''Contrôle PER_SITUATION_CONGES_15.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PER_SITUATION_CONGES_15 As Global.Virtualia.Net.Fenetre_PER_SITUATION_CONGES

    '''<summary>
    '''Contrôle PER_VPlanning.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PER_VPlanning As Global.Virtualia.Net.Controles_VPlanning

    '''<summary>
    '''Contrôle VueDroits.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VueDroits As Global.System.Web.UI.WebControls.View

    '''<summary>
    '''Contrôle PER_DROIT_CONGES_16.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PER_DROIT_CONGES_16 As Global.Virtualia.Net.Fenetre_PER_DROIT_CONGES

    '''<summary>
    '''Contrôle PER_ANNUALISATION_42.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PER_ANNUALISATION_42 As Global.Virtualia.Net.Fenetre_PER_ANNUALISATION

    '''<summary>
    '''Contrôle PER_TEMPS_TRAVAIL_41.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PER_TEMPS_TRAVAIL_41 As Global.Virtualia.Net.Fenetre_PER_TEMPS_TRAVAIL

    '''<summary>
    '''Contrôle VuePrevision.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VuePrevision As Global.System.Web.UI.WebControls.View

    '''<summary>
    '''Contrôle PER_TRAVAIL_33.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PER_TRAVAIL_33 As Global.Virtualia.Net.Fenetre_PER_TRAVAIL

    '''<summary>
    '''Contrôle PER_MODIF_PLANNING_91.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PER_MODIF_PLANNING_91 As Global.Virtualia.Net.Fenetre_PER_MODIF_PLANNING

    '''<summary>
    '''Contrôle VueAbsences.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VueAbsences As Global.System.Web.UI.WebControls.View

    '''<summary>
    '''Contrôle PER_ABSENCE_15.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PER_ABSENCE_15 As Global.Virtualia.Net.Fenetre_PER_ABSENCE

    '''<summary>
    '''Contrôle VueConstate.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VueConstate As Global.System.Web.UI.WebControls.View

    '''<summary>
    '''Contrôle PER_CET_HEBDO_43.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PER_CET_HEBDO_43 As Global.Virtualia.Net.Fenetre_PER_CET_HEBDO

    '''<summary>
    '''Contrôle PER_EVENEMENTJOUR_40.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PER_EVENEMENTJOUR_40 As Global.Virtualia.Net.Fenetre_PER_EVENEMENTJOUR

    '''<summary>
    '''Contrôle PER_POINTAGE_79.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PER_POINTAGE_79 As Global.Virtualia.Net.Fenetre_PER_POINTAGE

    '''<summary>
    '''Contrôle VueDebitCredit.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VueDebitCredit As Global.System.Web.UI.WebControls.View

    '''<summary>
    '''Contrôle PER_VDebitCredit.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PER_VDebitCredit As Global.Virtualia.Net.Fenetre_PER_DEBIT_CREDIT

    '''<summary>
    '''Contrôle VueReleves.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VueReleves As Global.System.Web.UI.WebControls.View

    '''<summary>
    '''Contrôle PER_VMensuel.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PER_VMensuel As Global.Virtualia.Net.Controles_VDetailMensuel

    '''<summary>
    '''Contrôle PER_VSemaine.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PER_VSemaine As Global.Virtualia.Net.Fenetre_PER_PLANNING_SEMAINE

    '''<summary>
    '''Contrôle VueCET.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VueCET As Global.System.Web.UI.WebControls.View

    '''<summary>
    '''Contrôle PER_CET_80.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PER_CET_80 As Global.Virtualia.Net.Fenetre_PER_CET

    '''<summary>
    '''Contrôle VueSysRef.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VueSysRef As Global.System.Web.UI.WebControls.View

    '''<summary>
    '''Contrôle Referentiel.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Referentiel As Global.Virtualia.Net.Controles_ListeReferences

    '''<summary>
    '''Contrôle VueAlbum.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VueAlbum As Global.System.Web.UI.WebControls.View

    '''<summary>
    '''Contrôle AlbumRequetes.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents AlbumRequetes As Global.Virtualia.Net.Controles_ObjetsArmoire_AlbumdesScripts

    '''<summary>
    '''Contrôle VueEdition.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VueEdition As Global.System.Web.UI.WebControls.View

    '''<summary>
    '''Contrôle EditionArmoire.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EditionArmoire As Global.Virtualia.Net.VirtualiaOutils_EditionExtraction

    '''<summary>
    '''Contrôle VueMessage.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VueMessage As Global.System.Web.UI.WebControls.View

    '''<summary>
    '''Contrôle MsgVirtualia.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents MsgVirtualia As Global.Virtualia.Net.Controles_VMessage

    '''<summary>
    '''Contrôle VueCalendrier.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VueCalendrier As Global.System.Web.UI.WebControls.View

    '''<summary>
    '''Contrôle Calendrier.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Calendrier As Global.Virtualia.Net.Controles_VCalendrier

    '''<summary>
    '''Contrôle ArmoireCourante.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents ArmoireCourante As Global.Virtualia.Net.Controles_ArmoirePersonnes

    '''<summary>
    '''Contrôle HSelIde.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents HSelIde As Global.System.Web.UI.WebControls.HiddenField

    '''<summary>
    '''Propriété Master.
    '''</summary>
    '''<remarks>
    '''Propriété générée automatiquement.
    '''</remarks>
    Public Shadows ReadOnly Property Master() As Virtualia.Net.MasterPage
        Get
            Return CType(MyBase.Master, Virtualia.Net.MasterPage)
        End Get
    End Property
End Class
