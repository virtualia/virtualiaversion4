﻿Option Strict On
Option Explicit On
Option Compare Text
Imports VI = Virtualia.Systeme.Constantes
Partial Class Fenetres_FrmCMCExtraction
    Inherits System.Web.UI.Page
    Private WebFct As Virtualia.Net.WebFonctions

    Protected Sub Onglet_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BoutonN1.Click, BoutonN2.Click
        Call Initialiser()
        Call InverserOnglet(CType(sender, Button))
        Select Case CType(sender, Button).ID
            Case Is = "BoutonN1"
                CadreCMCEdition.BackColor = WebFct.ConvertCouleur("#82BEA1")
                MultiOnglets.SetActiveView(VueCMC)
            Case Is = "BoutonN2"
                CadreCMCEdition.BackColor = WebFct.ConvertCouleur("#B0E0D7")
                MultiOnglets.SetActiveView(VueExtraction)
        End Select

    End Sub

    Protected Sub CMC_AppelChoix(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.AppelChoixEventArgs) Handles CMC.AppelChoix
        LigneW.Value = e.IndiceLigne.ToString
        RetourW.Value = "CMC"
        Choix.V_Appel(0) = e.Valeurs
        MultiOnglets.SetActiveView(VueChoix)
    End Sub

    Protected Sub Extraction_AppelChoix(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.AppelChoixEventArgs) Handles Extraction.AppelChoix
        LigneW.Value = e.IndiceLigne.ToString
        RetourW.Value = "EXT"
        Select Case e.Appelant
            Case Is = "Filtre"
                Choix.V_Appel(1) = e.Valeurs
                MultiOnglets.SetActiveView(VueChoix)
            Case Else
                OptionInfo.V_Appel = e.Valeurs
                MultiOnglets.SetActiveView(VueOption)
        End Select
    End Sub

    Protected Sub Choix_AppelChoix(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.AppelChoixEventArgs) Handles Choix.AppelChoix
        Select Case RetourW.Value
            Case Is = "EXT"
                Extraction.ListeChoix(CInt(LigneW.Value)) = e.Valeurs
                MultiOnglets.SetActiveView(VueExtraction)
            Case Else
                CMC.ListeChoix(CInt(LigneW.Value)) = e.Valeurs
                MultiOnglets.SetActiveView(VueCMC)
        End Select
    End Sub

    Protected Sub Choix_RetourEventHandler(ByVal sender As Object, ByVal e As System.EventArgs) Handles Choix.RetourEventHandler
        Select Case RetourW.Value
            Case Is = "EXT"
                MultiOnglets.SetActiveView(VueExtraction)
            Case Else
                MultiOnglets.SetActiveView(VueCMC)
        End Select
    End Sub

    Protected Sub OptionInfo_AppelChoix(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.AppelChoixEventArgs) Handles OptionInfo.AppelChoix
        Extraction.ListeChoix(CInt(LigneW.Value)) = e.Valeurs
        MultiOnglets.SetActiveView(VueExtraction)
    End Sub

    Protected Sub OptionInfo_RetourEventHandler(ByVal sender As Object, ByVal e As System.EventArgs) Handles OptionInfo.RetourEventHandler
        MultiOnglets.SetActiveView(VueExtraction)
    End Sub

    Protected Sub CMC_AppelTable(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.AppelTableEventArgs) Handles CMC.AppelTable
        Referentiel.V_PointdeVue = e.PointdeVueInverse
        Referentiel.V_NomTable = e.NomdelaTable
        Referentiel.V_Appelant(e.ObjetAppelant) = MultiOnglets.ActiveViewIndex.ToString
        MultiOnglets.SetActiveView(VueSysRef)
        WebFct.PointeurContexte.SysRef_IDVueRetour = "VueCMC"
    End Sub

    Protected Sub Extraction_AppelTable(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.AppelTableEventArgs) Handles Extraction.AppelTable
        Referentiel.V_PointdeVue = e.PointdeVueInverse
        Referentiel.V_NomTable = e.NomdelaTable
        Referentiel.V_Appelant(e.ObjetAppelant) = MultiOnglets.ActiveViewIndex.ToString
        MultiOnglets.SetActiveView(VueSysRef)
        WebFct.PointeurContexte.SysRef_IDVueRetour = "VueExtraction"
    End Sub

    Protected Sub Referentiel_ValeurSelectionnee(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.ValeurSelectionneeEventArgs) Handles Referentiel.ValeurSelectionnee
        Select Case WebFct.PointeurContexte.SysRef_IDVueRetour
            Case "VueCMC"
                CMC.Dontab_RetourAppelTable(e.ControleAppelant) = e.Valeur
                MultiOnglets.SetActiveView(VueCMC)
            Case "VueExtraction"
                Extraction.Dontab_RetourAppelTable(e.ControleAppelant) = e.Valeur
                MultiOnglets.SetActiveView(VueExtraction)
        End Select
    End Sub

    Protected Sub Extraction_ScriptResultat(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.ScriptResultatEventArgs) Handles Extraction.ScriptResultat
        Dim I As Integer
        For I = 0 To e.Centrage_Colonne.Count - 1
            EditionResultat.Centrage_Colonne(I) = e.Centrage_Colonne(I)
        Next I
        EditionResultat.VDataGrid(e.IntitulesColonnes, e.Libelles) = e.Valeurs
        EditionResultat.VIntitule = Extraction.IntituleScript
        MultiOnglets.SetActiveView(VueResultat)
    End Sub

    Protected Sub EditionResultat_RetourEventHandler(ByVal sender As Object, ByVal e As System.EventArgs) Handles EditionResultat.RetourEventHandler
        MultiOnglets.SetActiveView(VueExtraction)
    End Sub

    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        Dim NumSession As String = Me.Master.V_NoSession
        If NumSession = "" Then
            NumSession = Server.HtmlDecode(Request.QueryString("IDVirtualia"))
            Me.Master.V_NoSession = NumSession
        End If
        If NumSession = "" Then
            Dim Msg As String = "Erreur lors de l'identification."
            Response.Redirect("~/Fenetres/Connexion/ErreurApplication.aspx?Msg=" & Msg)
        End If
        WebFct = New Virtualia.Net.WebFonctions(Me, 1)
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            Dim Eti As System.Web.UI.WebControls.Label
            Eti = CType(WebFct.VirWebControle(Me.Master, "MsgArmoire", 0), System.Web.UI.WebControls.Label)
            Eti.Text = WebFct.PointeurArmoire.NomdelArmoire & Strings.Space(1) & "(" & WebFct.PointeurArmoire.NombredeDossiers & ")"
            Eti = CType(WebFct.VirWebControle(Me.Master, "MsgArmoirePied", 0), System.Web.UI.WebControls.Label)
            Eti.Text = WebFct.PointeurArmoire.NomdelArmoire & Strings.Space(1) & "(" & WebFct.PointeurArmoire.NombredeDossiers & ")"
        Catch ex As Exception
            Exit Sub
        End Try
    End Sub

    Protected Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
        WebFct.Dispose()
    End Sub

    Private Sub InverserOnglet(ByVal BtnControle As System.Web.UI.WebControls.Button)
        BtnControle.Font.Bold = True
        BtnControle.Height = New Unit(24)
        BtnControle.Width = New Unit(170)
        BtnControle.BackColor = WebFct.ConvertCouleur("#7D9F99")
        BtnControle.ForeColor = System.Drawing.Color.White
    End Sub

    Private Sub Initialiser()
        Dim Ctl As Control
        Dim BtnControle As System.Web.UI.WebControls.Button
        Dim IndiceI As Integer = 0
        Do
            Ctl = WebFct.VirWebControle(Me.CadreOnglets, "Bouton", IndiceI)
            If Ctl Is Nothing Then
                Exit Do
            End If
            BtnControle = CType(Ctl, System.Web.UI.WebControls.Button)
            BtnControle.Font.Bold = False
            BtnControle.Height = New Unit(33)
            BtnControle.Width = New Unit(185)
            BtnControle.BackColor = System.Drawing.Color.Transparent
            BtnControle.ForeColor = WebFct.ConvertCouleur("#142425")
            IndiceI += 1
        Loop
    End Sub


End Class
