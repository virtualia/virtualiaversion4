﻿<%@ Page Language="VB" MasterPageFile="~/PagesMaitre/VirtualiaMain.master" AutoEventWireup="false" 
    Inherits="Virtualia.Net.Fenetres_FrmDiplomesCV" Codebehind="FrmDiplomesCV.aspx.vb"
    MaintainScrollPositionOnPostback="True" %>
<%@ MasterType VirtualPath="~/PagesMaitre/VirtualiaMain.master"  %>

<%@ Register src="../ControlesWebUser/ObjetsArmoire/ArmoirePersonnes.ascx" tagname="ArmoirePersonnes" tagprefix="VirtuArm" %>
<%@ Register src="../ControlesWebUser/ObjetsArmoire/ListeReferences.ascx" tagname="ListeReferences" tagprefix="VirtuRef" %>
<%@ Register src="../ControlesWebUser/ObjetsArmoire/AlbumdesScripts.ascx" tagname="AlbumdesScripts" tagprefix="VirtuAlbum" %>
<%@ Register src="../ControlesWebUser/Outils/EditionExtraction.ascx" tagname="EditionExtraction" tagprefix="Virtualia" %>
<%@ Register src="../ControlesWebUser/Saisies/VMessage.ascx" tagname="VMessage" tagprefix="Virtualia" %>

<%@ Register src="../ControlesWebUser/ObjetsFenetre/Personnel/PER_DIPLOME.ascx" tagname="PER_DIPLOME" tagprefix="Virtualia" %>
<%@ Register src="../ControlesWebUser/ObjetsFenetre/Personnel/PER_BREVETPRO.ascx" tagname="PER_BREVETPRO" tagprefix="Virtualia" %>
<%@ Register src="../ControlesWebUser/ObjetsFenetre/Personnel/PER_SPECIALITE.ascx" tagname="PER_SPECIALITE" tagprefix="Virtualia" %>
<%@ Register src="../ControlesWebUser/ObjetsFenetre/Personnel/PER_LANGUE_PRATIQUEE.ascx" tagname="PER_LANGUE_PRATIQUEE" tagprefix="Virtualia" %>
<%@ Register src="../ControlesWebUser/ObjetsFenetre/Personnel/PER_EXPERIENCE_CV.ascx" tagname="PER_EXPERIENCE_CV" tagprefix="Virtualia" %>
<%@ Register src="../ControlesWebUser/ObjetsFenetre/Personnel/PER_ACQUIS_CV.ascx" tagname="PER_ACQUIS_CV" tagprefix="Virtualia" %>
<%@ Register src="../ControlesWebUser/ObjetsFenetre/Personnel/PER_LANGUE_CV.ascx" tagname="PER_LANGUE_CV" tagprefix="Virtualia" %>
<%@ Register src="../ControlesWebUser/ObjetsFenetre/Personnel/PER_STAGE_CV.ascx" tagname="PER_STAGE_CV" tagprefix="Virtualia" %>
<%@ Register src="../ControlesWebUser/ObjetsFenetre/Personnel/PER_LOISIR_CV.ascx" tagname="PER_LOISIR_CV" tagprefix="Virtualia" %>

<asp:Content ID="CadreGeneral" runat="server" ContentPlaceHolderID="CorpsMaster">
    <asp:UpdatePanel ID="UpdatePanelSaisie" runat="server">
        <ContentTemplate>
           <asp:Table ID="TableOnglets" runat="server" Height="35px" Width="1150px" HorizontalAlign="Center"
                      style="margin-top: 0px" >
             <asp:TableRow>
                <asp:TableCell>
                    <asp:Table ID="CadreOnglets" runat="server" Height="35px" Width="925px" HorizontalAlign="Left"
                       CellPadding="0" CellSpacing="0" BackImageUrl="~/Images/Onglets/Onglet_2bords_187.bmp"
                       style="margin-top: 10px; border-top-color: #124545;
                        border-bottom-color: #B0E0D7; border-bottom-style: groove" >
                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign ="Center" VerticalAlign="Bottom" Width="185px">
                                <asp:Button ID="BoutonN1" runat="server" Height="24px" Width="170px" 
                                    ForeColor="White" Text="Diplômes et qualifications" Font-Bold="true" Font-Italic="true"
                                    Font-Names="Trebuchet MS" BackColor="#6C9690" BorderStyle="None" 
                                    style="padding-left: 1px; margin-left: 1px; margin-bottom: 2px; text-align: center" />  
                            </asp:TableCell>
                            <asp:TableCell HorizontalAlign ="Center" VerticalAlign="Bottom" Width="185px">
                                <asp:Button ID="BoutonN2" runat="server" Height="33px" Width="185px" 
                                    ForeColor="#142425" Text="Spécialités" Font-Bold="false" Font-Italic="true"
                                    Font-Names="Trebuchet MS" BackColor="Transparent" BorderStyle="None" 
                                    style="padding-left: 1px; margin-left: 1px; margin-bottom: 2px; text-align: center" />  
                            </asp:TableCell>
                            <asp:TableCell HorizontalAlign ="Center" VerticalAlign="Bottom" Width="185px">
                                <asp:Button ID="BoutonN3" runat="server" Height="33px" Width="185px" 
                                    ForeColor="#142425" Text="Expèrience professionnelle" Font-Bold="false" Font-Italic="true"
                                    Font-Names="Trebuchet MS" BackColor="Transparent" BorderStyle="None"
                                    style="padding-left: 1px; margin-left: 1px; margin-bottom: 2px; text-align: center" />  
                            </asp:TableCell>
                            <asp:TableCell HorizontalAlign ="Center" VerticalAlign="Bottom" Width="185px">
                                <asp:Button ID="BoutonN4" runat="server" Height="33px" Width="185px" 
                                    ForeColor="#142425" Text="Acquis techniques" Font-Bold="false" Font-Italic="true"
                                    Font-Names="Trebuchet MS" BackColor="Transparent" BorderStyle="None" 
                                    style="padding-left: 1px; margin-left: 1px; margin-bottom: 2px; text-align: center" />  
                            </asp:TableCell>
                            <asp:TableCell HorizontalAlign ="Center" VerticalAlign="Bottom" Width="185px">
                                <asp:Button ID="BoutonN5" runat="server" Height="33px" Width="185px" 
                                    ForeColor="#142425" Text="Loisirs" Font-Bold="false" Font-Italic="true"
                                    Font-Names="Trebuchet MS" BackColor="Transparent" BorderStyle="None" 
                                    style="padding-left: 1px; margin-left: 1px; margin-bottom: 2px; text-align: center" />  
                            </asp:TableCell>
                        </asp:TableRow>
                    </asp:Table>
                </asp:TableCell>
             </asp:TableRow>
           </asp:Table>
           <asp:Table ID="CadreSaisie" runat="server" Height="1220px" Width="1150px" HorizontalAlign="Center"
                     BackImageUrl="~/Images/Fonds/Fond_Saisie.jpg" CellSpacing="2" style="margin-top: 0px" >
               <asp:TableRow>
                   <asp:TableCell ID="ConteneurVues" Height="1220px" Width="820px" VerticalAlign="Top" HorizontalAlign="Center">
                       <asp:MultiView ID="MultiOnglets" runat="server" ActiveViewIndex="0">
                          <asp:View ID="VueDiplome" runat="server">  
                             <Virtualia:PER_DIPLOME ID="PER_DIPLOME_6" runat="server"
                               CadreStyle="margin-top: 10px" />  
                             <Virtualia:PER_BREVETPRO ID="PER_BREVETPRO_34" runat="server"
                               CadreStyle="margin-top: 20px" />  
                           </asp:View>  
                          <asp:View ID="VueSpecialite" runat="server">
                             <Virtualia:PER_SPECIALITE ID="PER_SPECIALITE_53" runat="server" 
                              CadreStyle="margin-top: 10px" /> 
                             <Virtualia:PER_LANGUE_PRATIQUEE ID="PER_LANGUE_PRATIQUEE_120" runat="server" 
                              CadreStyle="margin-top: 10px" />                
                          </asp:View>
                          <asp:View ID="VueExperience" runat="server">  
                              <Virtualia:PER_EXPERIENCE_CV ID="PER_EXPERIENCE_CV_7" runat="server" 
                              CadreStyle="margin-top: 10px" /> 
                          </asp:View> 
                          <asp:View ID="VueAcquis" runat="server">  
                              <Virtualia:PER_LANGUE_CV ID="PER_LANGUE_CV_9" runat="server" 
                              CadreStyle="margin-top: 10px" /> 
                              <Virtualia:PER_ACQUIS_CV ID="PER_ACQUIS_CV_8" runat="server" 
                              CadreStyle="margin-top: 10px" /> 
                              <Virtualia:PER_STAGE_CV ID="PER_STAGE_CV_11" runat="server" 
                              CadreStyle="margin-top: 10px" /> 
                          </asp:View>
                          <asp:View ID="VueLoisirs" runat="server">  
                              <Virtualia:PER_LOISIR_CV ID="PER_LOISIR_CV_10" runat="server" 
                              CadreStyle="margin-top: 10px" />
                          </asp:View>      
                          <asp:View ID="VueSysRef" runat="server">
                             <VirtuRef:ListeReferences ID="Referentiel" runat="server"
                                       SiListeMenuVisible="false" />
                          </asp:View>
                          <asp:View ID="VueAlbum" runat="server">
                             <VirtuAlbum:AlbumdesScripts ID="AlbumRequetes" runat="server"
                                V_PointdeVue="1" />
                          </asp:View>
                          <asp:View ID="VueEdition" runat="server">
                             <Virtualia:EditionExtraction ID="EditionArmoire" runat="server" />
                          </asp:View>
                          <asp:View ID="VueMessage" runat="server">
                             <Virtualia:VMessage ID="MsgVirtualia" runat="server" />
                          </asp:View>
                      </asp:MultiView>
                  </asp:TableCell>
                  <asp:TableCell Height="1200px" Width="330px" VerticalAlign="Top" >
                     <VirtuArm:ArmoirePersonnes ID="ArmoireCourante" runat="server" />
                  </asp:TableCell>
              </asp:TableRow>
              <asp:TableRow>
                <asp:TableCell>
                    <asp:HiddenField ID="HSelIde" runat="server" Value="0" />
                </asp:TableCell>
              </asp:TableRow>
            </asp:Table>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
