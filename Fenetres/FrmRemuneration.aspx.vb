﻿Option Strict On
Option Explicit On
Option Compare Text
Imports VI = Virtualia.Systeme.Constantes
Partial Class Fenetres_FrmRemuneration
    Inherits System.Web.UI.Page
    Private WebFct As Virtualia.Net.WebFonctions
    Private WsNomState As String = "Vremu"
    Private Const ISynthese As Short = 0
    Private Const IBulletin As Short = 1
    Private Const IPrimes As Short = 2
    Private Const IHeuresSupp As Short = 3
    Private Const IAvantage As Short = 4
    Private Const IVariable As Short = 5
    Private Const ISysref As Short = 6
    Private Const IAlbum As Short = 7
    Private Const IEdition As Short = 8
    Private Const IMessage As Short = 9

    Public Property Identifiant() As Integer
        Get
            Return CInt(HSelIde.Value)
        End Get
        Set(ByVal value As Integer)
            If value = CInt(HSelIde.Value) Then
                Exit Property
            End If
            HSelIde.Value = value.ToString
            'Call FaireSynthese()
            PER_INDEMNITE_21.Identifiant = value
            PER_AVANTAGES_49.Identifiant = value
            PER_HEURESUP_61.Identifiant = value
            PER_VARIABLE_PAIE_67.Identifiant = value
        End Set
    End Property

    Protected Sub ArmoireCourante_CmdAlbum_Click(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.AppelAlbumClickEventArgs) Handles ArmoireCourante.CmdAlbum_Click
        MultiOnglets.SetActiveView(VueAlbum)
        Call ChargerAlbum(e.PointdeVue, e.Outil)
    End Sub

    Protected Sub ArmoireCourante_Dossier_Click(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.DossierClickEventArgs) Handles ArmoireCourante.Dossier_Click
        Identifiant = e.Identifiant
        Me.Master.IdentifiantCourant = e.Identifiant
    End Sub

    Protected Sub MsgVirtualia_ValeurRetour(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.MessageRetourEventArgs) Handles MsgVirtualia.ValeurRetour
        Select Case e.Emetteur
            Case Is = "SuppFiche"
                Select Case e.NumeroObjet
                    Case VI.ObjetPer.ObaIndemnite
                        PER_INDEMNITE_21.RetourDialogueSupp(e.ReponseMsg) = e.ChaineDatas
                        WebFct.PointeurContexte.Fenetre_VueActive(VI.CategorieRH.Remuneration) = IPrimes
                    Case VI.ObjetPer.ObaHeuresSup
                        PER_HEURESUP_61.RetourDialogueSupp(e.ReponseMsg) = e.ChaineDatas
                        WebFct.PointeurContexte.Fenetre_VueActive(VI.CategorieRH.Remuneration) = IHeuresSupp
                    Case VI.ObjetPer.ObaAvantage
                        PER_AVANTAGES_49.RetourDialogueSupp(e.ReponseMsg) = e.ChaineDatas
                        WebFct.PointeurContexte.Fenetre_VueActive(VI.CategorieRH.Remuneration) = IAvantage
                    Case VI.ObjetPer.ObaVariablePaie
                        PER_VARIABLE_PAIE_67.RetourDialogueSupp(e.ReponseMsg) = e.ChaineDatas
                        WebFct.PointeurContexte.Fenetre_VueActive(VI.CategorieRH.Remuneration) = IVariable
                End Select
            Case Is = "SuppDossier"
                Call SupprimerDossier(e.ReponseMsg)
                Exit Sub
            Case Is = "MajDossier"
                Select Case e.NumeroObjet
                    Case VI.ObjetPer.ObaIndemnite
                        PER_INDEMNITE_21.V_RetourDialogueMaj(e.ReponseMsg) = e.ChaineDatas
                        WebFct.PointeurContexte.Fenetre_VueActive(VI.CategorieRH.Remuneration) = IPrimes
                    Case VI.ObjetPer.ObaHeuresSup
                        PER_HEURESUP_61.V_RetourDialogueMaj(e.ReponseMsg) = e.ChaineDatas
                        WebFct.PointeurContexte.Fenetre_VueActive(VI.CategorieRH.Remuneration) = IHeuresSupp
                    Case VI.ObjetPer.ObaAvantage
                        PER_AVANTAGES_49.V_RetourDialogueMaj(e.ReponseMsg) = e.ChaineDatas
                        WebFct.PointeurContexte.Fenetre_VueActive(VI.CategorieRH.Remuneration) = IAvantage
                    Case VI.ObjetPer.ObaVariablePaie
                        PER_VARIABLE_PAIE_67.V_RetourDialogueMaj(e.ReponseMsg) = e.ChaineDatas
                        WebFct.PointeurContexte.Fenetre_VueActive(VI.CategorieRH.Remuneration) = IVariable
                End Select
        End Select
        CadreSaisie.BackImageUrl = "~/Images/Fonds/Fond_Saisie.jpg"
        ConteneurVues.Width = New Unit(820)
        ArmoireCourante.Visible = True
        MultiOnglets.ActiveViewIndex = WebFct.PointeurContexte.Fenetre_VueActive(VI.CategorieRH.Remuneration)
    End Sub

    Protected Sub ArmoireCourante_ValeurChange(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.DonneeChangeEventArgs) Handles ArmoireCourante.ValeurChange
        Dim Eti As System.Web.UI.WebControls.Label
        Eti = CType(WebFct.VirWebControle(Me.Master, "MsgArmoire", 0), System.Web.UI.WebControls.Label)
        Eti.Text = e.Valeur
        Eti = CType(WebFct.VirWebControle(Me.Master, "MsgArmoirePied", 0), System.Web.UI.WebControls.Label)
        Eti.Text = e.Valeur
    End Sub

    Protected Sub Onglet_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BoutonN1.Click, BoutonN2.Click, BoutonN3.Click, BoutonN4.Click, BoutonN5.Click, BoutonN6.Click
        CadreSaisie.BackImageUrl = "~/Images/Fonds/Fond_Saisie.jpg"
        Select Case CType(sender, Button).ID
            Case Is = "BoutonN1"
                MultiOnglets.SetActiveView(VueSynthese)
            Case Is = "BoutonN2"
                MultiOnglets.SetActiveView(VueBulletin)
            Case Is = "BoutonN3"
                MultiOnglets.SetActiveView(VuePrimes)
            Case Is = "BoutonN4"
                MultiOnglets.SetActiveView(VueHeuresSup)
            Case Is = "BoutonN5"
                MultiOnglets.SetActiveView(VueAvantage)
            Case Is = "BoutonN6"
                MultiOnglets.SetActiveView(VueVariable)
        End Select
        Dim Eti As System.Web.UI.WebControls.Label
        Eti = CType(WebFct.VirWebControle(Me.Master, "MsgSaisie", 0), System.Web.UI.WebControls.Label)
        Eti.Text = ""

        Eti = CType(WebFct.VirWebControle(Me.Master, "MsgSaisiePied", 0), System.Web.UI.WebControls.Label)
        Eti.Text = ""
        Call Initialiser()
        WebFct.PointeurContexte.Fenetre_VueActive(VI.CategorieRH.Remuneration) = MultiOnglets.ActiveViewIndex
    End Sub

    Protected Sub Referentiel_RetourEventHandler(ByVal sender As Object, ByVal e As System.EventArgs) Handles Referentiel.RetourEventHandler
        CadreSaisie.BackImageUrl = "~/Images/Fonds/Fond_Saisie.jpg"
        ConteneurVues.Width = New Unit(820)
        ArmoireCourante.Visible = True
        Select Case WebFct.PointeurContexte.SysRef_IDVueRetour
            Case Is = "VuePrimes"
                MultiOnglets.SetActiveView(VuePrimes)
            Case Is = "VueHeuresSup"
                MultiOnglets.SetActiveView(VueHeuresSup)
            Case Is = "VueAvantage"
                MultiOnglets.SetActiveView(VueAvantage)
            Case Is = "VueVariable"
                MultiOnglets.SetActiveView(VueVariable)
        End Select
        WebFct.PointeurContexte.Fenetre_VueActive(VI.CategorieRH.Remuneration) = MultiOnglets.ActiveViewIndex
    End Sub

    Protected Sub Referentiel_ValeurSelectionnee(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.ValeurSelectionneeEventArgs) Handles Referentiel.ValeurSelectionnee
        CadreSaisie.BackImageUrl = "~/Images/Fonds/Fond_Saisie.jpg"
        ConteneurVues.Width = New Unit(820)
        ArmoireCourante.Visible = True
        Select Case WebFct.PointeurContexte.SysRef_IDVueRetour
            Case Is = "VuePrimes"
                Select Case e.ObjetAppelant
                    Case VI.ObjetPer.ObaIndemnite
                        PER_INDEMNITE_21.Dontab_RetourAppelTable(e.ControleAppelant) = e.Valeur
                End Select
                MultiOnglets.SetActiveView(VuePrimes)
            Case Is = "VueCommissions"
                Select Case e.ObjetAppelant
                    Case VI.ObjetPer.ObaAvantage
                        PER_AVANTAGES_49.Dontab_RetourAppelTable(e.ControleAppelant) = e.Valeur
                End Select
                MultiOnglets.SetActiveView(VueAvantage)
            Case Is = "VueVariable"
                Select Case e.ObjetAppelant
                    Case VI.ObjetPer.ObaVariablePaie
                        PER_VARIABLE_PAIE_67.Dontab_RetourAppelTable(e.ControleAppelant) = e.Valeur
                End Select
                MultiOnglets.SetActiveView(VueVariable)
        End Select
        WebFct.PointeurContexte.Fenetre_VueActive(VI.CategorieRH.Remuneration) = MultiOnglets.ActiveViewIndex
    End Sub

    Protected Sub PER_AppelTable(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.AppelTableEventArgs) Handles PER_INDEMNITE_21.AppelTable, _
                                PER_AVANTAGES_49.AppelTable, PER_VARIABLE_PAIE_67.AppelTable
        Select Case e.ObjetAppelant
            Case VI.ObjetPer.ObaIndemnite
                WebFct.PointeurContexte.SysRef_IDVueRetour = VuePrimes.ID
            Case VI.ObjetPer.ObaHeuresSup
                WebFct.PointeurContexte.SysRef_IDVueRetour = VueHeuresSup.ID
            Case VI.ObjetPer.ObaAvantage
                WebFct.PointeurContexte.SysRef_IDVueRetour = VueAvantage.ID
            Case Else
                Exit Sub
        End Select
        ArmoireCourante.Visible = False

        Referentiel.V_PointdeVue = e.PointdeVueInverse
        Referentiel.V_NomTable = e.NomdelaTable
        Referentiel.V_Appelant(e.ObjetAppelant) = e.ControleAppelant
        CadreSaisie.BackImageUrl = ""
        CadreSaisie.BackColor = WebFct.ConvertCouleur("#216B68")
        MultiOnglets.SetActiveView(VueSysRef)
        WebFct.PointeurContexte.Fenetre_VueActive(VI.CategorieRH.Remuneration) = MultiOnglets.ActiveViewIndex
        ConteneurVues.Width = New Unit(1150)

    End Sub

    Protected Sub MessageDialogue(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.MessageSaisieEventArgs) _
    Handles ArmoireCourante.MessageDialogue, PER_INDEMNITE_21.MessageDialogue, PER_AVANTAGES_49.MessageDialogue, PER_HEURESUP_61.MessageDialogue, _
            PER_VARIABLE_PAIE_67.MessageDialogue

        MsgVirtualia.AfficherMessage = e
        CadreSaisie.BackImageUrl = ""
        CadreSaisie.BackColor = WebFct.ConvertCouleur("#216B68")
        ArmoireCourante.Visible = False
        MultiOnglets.SetActiveView(VueMessage)
        WebFct.PointeurContexte.Fenetre_VueActive(VI.CategorieRH.Remuneration) = MultiOnglets.ActiveViewIndex
        ConteneurVues.Width = New Unit(1150)
    End Sub

    Protected Sub MessageSaisie(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.MessageSaisieEventArgs) _
    Handles PER_INDEMNITE_21.MessageSaisie, PER_AVANTAGES_49.MessageSaisie, PER_HEURESUP_61.MessageSaisie, _
            PER_VARIABLE_PAIE_67.MessageSaisie

        Dim Eti As System.Web.UI.WebControls.Label
        Eti = CType(WebFct.VirWebControle(Me.Master, "MsgSaisie", 0), System.Web.UI.WebControls.Label)
        Select Case e.NatureMessage
            Case Is = "OK"
                Eti.ForeColor = Drawing.Color.Green
            Case Else
                Eti.ForeColor = Drawing.Color.Red
        End Select
        Eti.Text = e.ContenuMessage

        Eti = CType(WebFct.VirWebControle(Me.Master, "MsgSaisiePied", 0), System.Web.UI.WebControls.Label)
        Select Case e.NatureMessage
            Case Is = "OK"
                Eti.ForeColor = Drawing.Color.Green
            Case Else
                Eti.ForeColor = Drawing.Color.Red
        End Select
        Eti.Text = e.ContenuMessage

    End Sub

    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        Dim NumSession As String = Me.Master.V_NoSession
        If NumSession = "" Then
            NumSession = Server.HtmlDecode(Request.QueryString("IDVirtualia"))
            Me.Master.V_NoSession = NumSession
        End If
        If NumSession = "" Then
            Dim Msg As String = "Erreur lors de l'identification."
            Response.Redirect("~/Fenetres/Connexion/ErreurApplication.aspx?Msg=" & Msg)
        End If
        WebFct = New Virtualia.Net.WebFonctions(Me, 1)
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        If WebFct.PointeurContexte.Armoire_Identifiant <> 0 Then
            Identifiant = WebFct.PointeurContexte.Armoire_Identifiant
            MultiOnglets.ActiveViewIndex = WebFct.PointeurContexte.Fenetre_VueActive(VI.CategorieRH.Remuneration)
            Select Case MultiOnglets.ActiveViewIndex
                Case Is = IAlbum
                    If WebFct.PointeurContexte.Fenetre_VuePrecedente(VI.CategorieRH.Remuneration) <> 7 Then
                        Call ChargerAlbum(0, 0)
                    End If
                Case Else
                    Call Initialiser()
            End Select
        End If
    End Sub

    Protected Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
        WebFct.Dispose()
    End Sub

    Private Sub ChargerAlbum(ByVal PtdeVue As Short, ByVal Outil As Short)
        If PtdeVue = 0 Then
            PtdeVue = WebFct.PointeurContexte.Fenetre_PointdeVue
            Outil = WebFct.PointeurContexte.Fenetre_Outil
        End If
        AlbumRequetes.V_PointdeVue = PtdeVue
        AlbumRequetes.V_Outil = Outil
        Call Initialiser()
        CadreSaisie.BackImageUrl = ""
        CadreSaisie.BackColor = WebFct.ConvertCouleur("#216B68")
        WebFct.PointeurContexte.Fenetre_PointdeVue = PtdeVue
        WebFct.PointeurContexte.Fenetre_Outil = Outil
        WebFct.PointeurContexte.Fenetre_VueActive(VI.CategorieRH.Remuneration) = MultiOnglets.ActiveViewIndex
    End Sub

    Private Sub Initialiser()
        Dim Ctl As Control
        Dim BtnControle As System.Web.UI.WebControls.Button
        Dim IndiceI As Integer = 0
        Dim K As Integer = 0
        Do
            Ctl = WebFct.VirWebControle(Me.CadreOnglets, "Bouton", IndiceI)
            If Ctl Is Nothing Then
                Exit Do
            End If
            BtnControle = CType(Ctl, System.Web.UI.WebControls.Button)
            K = CInt(Strings.Right(BtnControle.ID, 1)) - 1
            If K = MultiOnglets.ActiveViewIndex Then
                BtnControle.Font.Bold = True
                BtnControle.Height = New Unit(24)
                BtnControle.Width = New Unit(170)
                BtnControle.BackColor = WebFct.ConvertCouleur("#7D9F99")
                BtnControle.ForeColor = System.Drawing.Color.White
            Else
                BtnControle.Font.Bold = False
                BtnControle.Height = New Unit(33)
                BtnControle.Width = New Unit(185)
                BtnControle.BackColor = System.Drawing.Color.Transparent
                BtnControle.ForeColor = WebFct.ConvertCouleur("#142425")
            End If
            IndiceI += 1
        Loop

        Select Case MultiOnglets.ActiveViewIndex
            Case ISysref, IEdition, IMessage
                ConteneurVues.Width = New Unit(1150)
                ArmoireCourante.Visible = False
            Case Else
                ConteneurVues.Width = New Unit(820)
                ArmoireCourante.Visible = True
        End Select
    End Sub

    Protected Sub AlbumRequetes_ScriptResultat(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.ScriptResultatEventArgs) Handles AlbumRequetes.ScriptResultat
        Dim I As Integer
        For I = 0 To e.Centrage_Colonne.Count - 1
            EditionArmoire.Centrage_Colonne(I) = e.Centrage_Colonne(I)
        Next I
        ArmoireCourante.Visible = False
        EditionArmoire.VDataGrid(e.IntitulesColonnes, e.Libelles) = e.Valeurs
        EditionArmoire.VIntitule = AlbumRequetes.VTitre
        MultiOnglets.SetActiveView(VueEdition)
        WebFct.PointeurContexte.Fenetre_VueActive(VI.CategorieRH.Remuneration) = MultiOnglets.ActiveViewIndex
    End Sub

    Protected Sub EditionArmoire_RetourEventHandler(ByVal sender As Object, ByVal e As System.EventArgs) Handles EditionArmoire.RetourEventHandler
        MultiOnglets.ActiveViewIndex = IAlbum
        WebFct.PointeurContexte.Fenetre_VueActive(VI.CategorieRH.Remuneration) = MultiOnglets.ActiveViewIndex
        Call Initialiser()
    End Sub

    Protected Sub AlbumRequetes_ValeurChange(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.DonneeChangeEventArgs) Handles AlbumRequetes.ValeurChange
        WebFct.PointeurContexte.Armoire_Identifiant = 0
        ArmoireCourante.ArmoirePersonnalisee = e.Valeur

        Dim Eti As System.Web.UI.WebControls.Label
        Eti = CType(WebFct.VirWebControle(Me.Master, "MsgCourant", 0), System.Web.UI.WebControls.Label)
        Eti.Text = ""
        Eti = CType(WebFct.VirWebControle(Me.Master, "MsgCourantPied", 0), System.Web.UI.WebControls.Label)
        Eti.Text = ""

        Eti = CType(WebFct.VirWebControle(Me.Master, "MsgArmoire", 0), System.Web.UI.WebControls.Label)
        Eti.Text = e.Valeur & Strings.Space(1) & ArmoireCourante.CompteDossiers
        Eti = CType(WebFct.VirWebControle(Me.Master, "MsgArmoirePied", 0), System.Web.UI.WebControls.Label)
        Eti.Text = e.Valeur & Strings.Space(1) & ArmoireCourante.CompteDossiers
    End Sub

    Private Sub FaireSynthese(ByVal Ide_Dossier As Integer)
        Dim Dossier As Virtualia.Ressources.Datas.ObjetDossierPER = WebFct.PointeurDossier(Ide_Dossier)
        Dim VEnr As ArrayList
        Dim RepRelatif As String

        If Dossier Is Nothing Then
            Exit Sub
        End If

        CadreSaisie.BackImageUrl = "~/Images/Fonds/Fond_Saisie.jpg"
        ConteneurVues.Width = New Unit(820)

        If Application.Item("CheminRelatif") Is Nothing Then
            If Request.ApplicationPath <> "/" Then
                Application.Add("CheminRelatif", Request.ApplicationPath & "/")
            Else
                Application.Add("CheminRelatif", "/")
            End If
        End If
        RepRelatif = Application.Item("CheminRelatif").ToString

        VEnr = New ArrayList
        VEnr.Add(Dossier.SyntheseAffectation(WebFct.PointeurUtilisateur.V_NomdUtilisateurSgbd, RepRelatif))
        'Synthese.V_Liste = VEnr
    End Sub

    Private Sub SupprimerDossier(ByVal MsgReponse As String)
        If MsgReponse = "Non" Then
            CadreSaisie.BackImageUrl = "~/Images/Fonds/Fond_Saisie.jpg"
            ConteneurVues.Width = New Unit(820)
            ArmoireCourante.Visible = True
            MultiOnglets.ActiveViewIndex = 0
            Exit Sub
        End If
        Select Case MsgReponse
            Case "OK", "KO"
                If MsgReponse = "OK" Then
                    Me.Master.IdentifiantCourant = 0
                    Identifiant = 0
                    WebFct.PointeurContexte.Armoire_Identifiant = 0
                    ArmoireCourante.Actualiser()
                End If
                CadreSaisie.BackImageUrl = "~/Images/Fonds/Fond_Saisie.jpg"
                ConteneurVues.Width = New Unit(820)
                WebFct.PointeurContexte.Fenetre_VueActive(VI.CategorieRH.InfosPersonnelles) = 0
                MultiOnglets.SetActiveView(VueSynthese)
                ArmoireCourante.Visible = True
                Exit Sub
        End Select
        '** Traitement du Oui à la suppression
        Dim Cretour As Boolean
        Dim Ide As Integer = WebFct.PointeurContexte.Armoire_Identifiant
        Dim Dossier As Virtualia.Ressources.Datas.ObjetDossierPER
        Dossier = WebFct.PointeurDossier(Ide)
        If Dossier Is Nothing Then
            Exit Sub
        End If
        Dim NomPrenom As String = Dossier.Nom & Strings.Space(1) & Dossier.Prenom

        Cretour = Dossier.V_SupprimerDossier(WebFct.PointeurUtilisateur.V_NomdUtilisateurSgbd, WebFct.PointeurUtilisateur.V_NomdeConnexion, _
                                                    WebFct.PointeurUtilisateur.InstanceBd)

        Dim TitreMsg As String = "Supprimer un dossier"
        Dim ContenuMsg As String = "le dossier N° " & Ide.ToString & " - " & NomPrenom
        Dim Evenement As Virtualia.Systeme.Evenements.MessageSaisieEventArgs
        If Cretour = True Then
            ContenuMsg &= Strings.Chr(13) & Strings.Chr(10) & "a bien été supprimé."
            WebFct.PointeurArmoire.RetirerDossier(Dossier)
            Evenement = New Virtualia.Systeme.Evenements.MessageSaisieEventArgs("SuppDossier", 1, "", "OK", TitreMsg, ContenuMsg)
        Else
            ContenuMsg &= Strings.Chr(13) & Strings.Chr(10) & "n'a pas pu être supprimé"
            Evenement = New Virtualia.Systeme.Evenements.MessageSaisieEventArgs("SuppDossier", 1, "", "KO", TitreMsg, ContenuMsg)
        End If

        MsgVirtualia.AfficherMessage = Evenement
        CadreSaisie.BackImageUrl = ""
        CadreSaisie.BackColor = WebFct.ConvertCouleur("#216B68")
        ArmoireCourante.Visible = False
        MultiOnglets.SetActiveView(VueMessage)
        ConteneurVues.Width = New Unit(1150)
    End Sub

End Class
