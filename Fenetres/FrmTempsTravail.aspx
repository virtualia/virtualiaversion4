﻿<%@ Page Language="VB" MasterPageFile="~/PagesMaitre/VirtualiaMain.master" AutoEventWireup="false" 
    Inherits="Virtualia.Net.Fenetres_FrmTempsTravail" Codebehind="FrmTempsTravail.aspx.vb"
    MaintainScrollPositionOnPostback="True" %>
<%@ MasterType VirtualPath="~/PagesMaitre/VirtualiaMain.master"  %>

<%@ Register src="../ControlesWebUser/ObjetsArmoire/ArmoirePersonnes.ascx" tagname="ArmoirePersonnes" tagprefix="VirtuArm" %>
<%@ Register src="../ControlesWebUser/ObjetsArmoire/ListeReferences.ascx" tagname="ListeReferences" tagprefix="VirtuRef" %>
<%@ Register src="../ControlesWebUser/ObjetsArmoire/AlbumdesScripts.ascx" tagname="AlbumdesScripts" tagprefix="VirtuAlbum" %>
<%@ Register src="../ControlesWebUser/Outils/EditionExtraction.ascx" tagname="EditionExtraction" tagprefix="Virtualia" %>
<%@ Register src="../ControlesWebUser/Saisies/VMessage.ascx" tagname="VMessage" tagprefix="Virtualia" %>
<%@ Register src="../ControlesWebUser/TempsTravail/VCalendrier.ascx" tagname="VCalendrier" tagprefix="Virtualia" %>
<%@ Register src="../ControlesWebUser/TempsTravail/VDetailMensuel.ascx" tagname="VDetailMensuel" tagprefix="Virtualia" %>

<%@ Register src="../ControlesWebUser/ObjetsFenetre/Personnel/PER_ABSENCE.ascx" tagname="PER_ABSENCE" tagprefix="Virtualia" %>
<%@ Register src="../ControlesWebUser/ObjetsFenetre/Personnel/PER_DROIT_CONGES.ascx" tagname="PER_DROIT_CONGES" tagprefix="Virtualia" %>
<%@ Register src="../ControlesWebUser/ObjetsFenetre/Personnel/PER_EVENEMENTJOUR.ascx" tagname="PER_EVENEMENTJOUR" tagprefix="Virtualia" %>
<%@ Register src="../ControlesWebUser/ObjetsFenetre/Personnel/PER_POINTAGE.ascx" tagname="PER_POINTAGE" tagprefix="Virtualia" %>
<%@ Register src="../ControlesWebUser/ObjetsFenetre/Personnel/PER_TEMPS_TRAVAIL.ascx" tagname="PER_TEMPS_TRAVAIL" tagprefix="Virtualia" %>
<%@ Register src="../ControlesWebUser/ObjetsFenetre/Personnel/PER_ANNUALISATION.ascx" tagname="PER_ANNUALISATION" tagprefix="Virtualia" %>
<%@ Register src="../ControlesWebUser/ObjetsFenetre/Personnel/PER_CET.ascx" tagname="PER_CET" tagprefix="Virtualia" %>
<%@ Register src="../ControlesWebUser/ObjetsFenetre/Personnel/PER_MODIF_PLANNING.ascx" tagname="PER_MODIF_PLANNING" tagprefix="Virtualia" %>
<%@ Register src="../ControlesWebUser/ObjetsFenetre/Personnel/PER_SITUATION_CONGES.ascx" tagname="PER_SITUATION_CONGES" tagprefix="Virtualia" %>
<%@ Register src="../ControlesWebUser/ObjetsFenetre/Personnel/PER_TRAVAIL.ascx" tagname="PER_TRAVAIL" tagprefix="Virtualia" %>
<%@ Register src="../ControlesWebUser/ObjetsFenetre/Personnel/PER_CET_HEBDO.ascx" tagname="PER_CET_HEBDO" tagprefix="Virtualia" %>

<%@ Register src="../ControlesWebUser/ObjetsFenetre/Personnel/PER_DEBIT_CREDIT.ascx" tagname="PER_DEBIT_CREDIT" tagprefix="Virtualia" %>
<%@ Register src="../ControlesWebUser/ObjetsFenetre/Personnel/PER_PLANNING_SEMAINE.ascx" tagname="PER_PLANNING_SEMAINE" tagprefix="Virtualia" %>

<%@ Register src="../ControlesWebUser/TempsTravail/VPlanning.ascx" tagname="VPlanning" tagprefix="Virtualia" %>

<asp:Content ID="CadreGeneral" runat="server" ContentPlaceHolderID="CorpsMaster">
   <asp:UpdatePanel ID="UpdatePanelSaisie" runat="server">
        <ContentTemplate>
            <asp:Table ID="TableOnglets" runat="server" Height="35px" Width="1150px" HorizontalAlign="Center"
                     style="margin-top: 0px" >
              <asp:TableRow>
                <asp:TableCell>
                    <asp:Table ID="CadreOnglets" runat="server" Height="35px" Width="960px" HorizontalAlign="Left"
                           CellPadding="0" CellSpacing="0" BackImageUrl="~/Images/Onglets/Onglet_2bords_120.bmp"
                           style="margin-top: 10px; border-top-color: #124545;
                            border-bottom-color: #B0E0D7; border-bottom-style: groove" >
                    <asp:TableRow>
                        <asp:TableCell HorizontalAlign ="Center" VerticalAlign="Bottom" Width="118px">
                            <asp:Button ID="BoutonN1" runat="server" Height="24px" Width="105px" 
                                ForeColor="White" Text="Situation" Font-Bold="true" Font-Italic="true"
                                Font-Names="Trebuchet MS" BackColor="#7D9F99" BorderStyle="None" 
                                style="padding-left: 1px; margin-left: 1px; margin-bottom: 2px; text-align: center" />  
                        </asp:TableCell>
                        <asp:TableCell HorizontalAlign ="Center" VerticalAlign="Bottom" Width="118px">
                            <asp:Button ID="BoutonN2" runat="server" Height="33px" Width="118px" 
                                ForeColor="#142425" Text="Droits" Font-Bold="false" Font-Italic="true"
                                Font-Names="Trebuchet MS" BackColor="Transparent" BorderStyle="None" 
                                style="padding-left: 1px; margin-left: 1px; margin-bottom: 2px; text-align: center" />  
                        </asp:TableCell>
                        <asp:TableCell HorizontalAlign ="Center" VerticalAlign="Bottom" Width="118px">
                            <asp:Button ID="BoutonN3" runat="server" Height="33px" Width="118px" 
                                ForeColor="#142425" Text="Prévisionnel" Font-Bold="false" Font-Italic="true"
                                Font-Names="Trebuchet MS" BackColor="Transparent" BorderStyle="None" 
                                style="padding-left: 1px; margin-left: 1px; margin-bottom: 2px; text-align: center" />  
                        </asp:TableCell>
                        <asp:TableCell HorizontalAlign ="Center" VerticalAlign="Bottom" Width="118px">
                            <asp:Button ID="BoutonN4" runat="server" Height="33px" Width="118px" 
                                ForeColor="#142425" Text="Congés / absences" Font-Bold="false" Font-Italic="true"
                                Font-Names="Trebuchet MS" BackColor="Transparent" BorderStyle="None"
                                style="padding-left: 1px; margin-left: 1px; margin-bottom: 2px; text-align: center" />  
                        </asp:TableCell>
                         <asp:TableCell HorizontalAlign ="Center" VerticalAlign="Bottom" Width="118px">
                            <asp:Button ID="BoutonN5" runat="server" Height="33px" Width="118px" 
                                ForeColor="#142425" Text="Constaté" Font-Bold="false" Font-Italic="true"
                                Font-Names="Trebuchet MS" BackColor="Transparent" BorderStyle="None" 
                                style="padding-left: 1px; margin-left: 1px; margin-bottom: 2px; text-align: center" />  
                        </asp:TableCell>
                        <asp:TableCell HorizontalAlign ="Center" VerticalAlign="Bottom" Width="118px">
                            <asp:Button ID="BoutonN6" runat="server" Height="33px" Width="118px" 
                                ForeColor="#142425" Text="Débit / Crédit" Font-Bold="false" Font-Italic="true"
                                Font-Names="Trebuchet MS" BackColor="Transparent" BorderStyle="None" 
                                style="padding-left: 1px; margin-left: 1px; margin-bottom: 2px; text-align: center;" />  
                        </asp:TableCell>
                        <asp:TableCell HorizontalAlign ="Center" VerticalAlign="Bottom" Width="118px">
                            <asp:Button ID="BoutonN7" runat="server" Height="33px" Width="118px" 
                                ForeColor="#142425" Text="Relevés horaires" Font-Bold="false" Font-Italic="true"
                                Font-Names="Trebuchet MS" BackColor="Transparent" BorderStyle="None" 
                                style="padding-left: 1px; margin-left: 1px; margin-bottom: 2px; text-align: center;" />  
                        </asp:TableCell>
                        <asp:TableCell HorizontalAlign ="Center" VerticalAlign="Bottom" Width="118px">
                            <asp:Button ID="BoutonN8" runat="server" Height="33px" Width="118px" 
                                ForeColor="#142425" Text="CET" Font-Bold="false" Font-Italic="true"
                                Font-Names="Trebuchet MS" BackColor="Transparent" BorderStyle="None" 
                                style="padding-left: 1px; margin-left: 1px; margin-bottom: 2px; text-align: center" />  
                        </asp:TableCell>
                    </asp:TableRow>
                  </asp:Table>
                </asp:TableCell>
              </asp:TableRow>
            </asp:Table>
            <asp:Table ID="CadreSaisie" runat="server" Width="1150px" HorizontalAlign="Center"
                     BackImageUrl="~/Images/Fonds/Fond_Saisie.jpg" CellPadding="0" CellSpacing="0"
                     style="margin-top: 0px " >
               <asp:TableRow>
                   <asp:TableCell ID="ConteneurVues" Width="820px" VerticalAlign="Top" HorizontalAlign="Center">
                       <asp:MultiView ID="MultiOnglets" runat="server" ActiveViewIndex="0">
                          <asp:View ID="VueSituation" runat="server"> 
                              <Virtualia:PER_SITUATION_CONGES ID="PER_SITUATION_CONGES_15" runat="server"/>  
                              <Virtualia:VPlanning ID="PER_VPlanning" runat="server" />  
                          </asp:View>  
                          <asp:View ID="VueDroits" runat="server">  
                             <Virtualia:PER_DROIT_CONGES ID="PER_DROIT_CONGES_16" runat="server"
                               CadreStyle="margin-top: 10px" />  
                             <Virtualia:PER_ANNUALISATION ID="PER_ANNUALISATION_42" runat="server"
                               CadreStyle="margin-top: 20px" />
                             <Virtualia:PER_TEMPS_TRAVAIL ID="PER_TEMPS_TRAVAIL_41" runat="server" 
                              CadreStyle="margin-top: 20px" />  
                          </asp:View>  
                          <asp:View ID="VuePrevision" runat="server">  
                             <Virtualia:PER_TRAVAIL ID="PER_TRAVAIL_33" runat="server" 
                              CadreStyle="margin-top: 10px" /> 
                             <Virtualia:PER_MODIF_PLANNING ID="PER_MODIF_PLANNING_91" runat="server" 
                              CadreStyle="margin-top: 20px" />  
                          </asp:View> 
                          <asp:View ID="VueAbsences" runat="server">
                              <Virtualia:PER_ABSENCE ID="PER_ABSENCE_15" runat="server"
                               CadreStyle="margin-top: 10px" CadreExpertStyle="margin-top: 10px"  />              
                          </asp:View>
                          <asp:View ID="VueConstate" runat="server"> 
                             <Virtualia:PER_CET_HEBDO ID="PER_CET_HEBDO_43" runat="server" 
                              CadreStyle="margin-top: 10px" /> 
                             <Virtualia:PER_EVENEMENTJOUR ID="PER_EVENEMENTJOUR_40" runat="server" 
                              CadreStyle="margin-top: 20px" />
                             <Virtualia:PER_POINTAGE ID="PER_POINTAGE_79" runat="server" />
                          </asp:View>
                          <asp:View ID="VueDebitCredit" runat="server">
                              <Virtualia:PER_DEBIT_CREDIT ID="PER_VDebitCredit" runat="server" />
                          </asp:View>
                          <asp:View ID="VueReleves" runat="server">
                              <Virtualia:VDetailMensuel ID="PER_VMensuel" runat="server" />
                              <Virtualia:PER_PLANNING_SEMAINE ID="PER_VSemaine" runat="server" />
                          </asp:View>
                          <asp:View ID="VueCET" runat="server">  
                              <Virtualia:PER_CET ID="PER_CET_80" runat="server"
                               CadreStyle="margin-top: 10px" />
                          </asp:View>     
                          <asp:View ID="VueSysRef" runat="server">
                             <VirtuRef:ListeReferences ID="Referentiel" runat="server"
                                SiListeMenuVisible="false" />
                          </asp:View>
                          <asp:View ID="VueAlbum" runat="server">
                             <VirtuAlbum:AlbumdesScripts ID="AlbumRequetes" runat="server"
                                V_PointdeVue="1" />
                          </asp:View>
                          <asp:View ID="VueEdition" runat="server">
                             <Virtualia:EditionExtraction ID="EditionArmoire" runat="server" />
                          </asp:View>
                          <asp:View ID="VueMessage" runat="server">
                             <Virtualia:VMessage ID="MsgVirtualia" runat="server" />
                          </asp:View>
                          <asp:View ID="VueCalendrier" runat="server">  
                              <Virtualia:VCalendrier ID="Calendrier" runat="server" TypeCalendrier="ID" />
                          </asp:View>   
                      </asp:MultiView>
                  </asp:TableCell>
                  <asp:TableCell Height="1200px" Width="330px" VerticalAlign="Top" >
                     <VirtuArm:ArmoirePersonnes ID="ArmoireCourante" runat="server" />
                  </asp:TableCell>
              </asp:TableRow>
              <asp:TableRow>
                <asp:TableCell>
                    <asp:HiddenField ID="HSelIde" runat="server" Value="0" />
                </asp:TableCell>
              </asp:TableRow>
            </asp:Table>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
