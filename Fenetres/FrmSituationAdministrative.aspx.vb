﻿Option Strict On
Option Explicit On
Option Compare Text
Imports VI = Virtualia.Systeme.Constantes
Partial Class Fenetres_FrmSituationAdministrative
    Inherits System.Web.UI.Page
    Private WebFct As Virtualia.Net.WebFonctions
    Private WsNomState As String = "Vsitadm"
    '
    Private Const IHisto As Short = 0
    Private Const IStatut As Short = 1
    Private Const IPosition As Short = 2
    Private Const IGrade As Short = 3
    Private Const IAnciennete As Short = 4
    Private Const ISanction As Short = 5
    Private Const ISysref As Short = 6
    Private Const IAlbum As Short = 7
    Private Const IEdition As Short = 8
    Private Const IMessage As Short = 9

    Public Property Identifiant() As Integer
        Get
            Return CInt(HSelIde.Value)
        End Get
        Set(ByVal value As Integer)
            If value = CInt(HSelIde.Value) Then
                Exit Property
            End If
            HSelIde.Value = value.ToString
            If value > 0 Then
                Carriere.Identifiant = value
                Carriere.Visible = True
            Else
                Carriere.Visible = False
            End If
            PER_STATUT_12.Identifiant = value
            PER_POSITION_13.Identifiant = value
            PER_GRADE_14.Identifiant = value
            PER_INTEGRATION_68.Identifiant = value
            PER_STATUT_DETACHE_54.Identifiant = value
            PER_POSITION_DETACHE_55.Identifiant = value
            PER_GRADE_DETACHE_56.Identifiant = value
            PER_ADMORIGINE_25.Identifiant = value
            PER_SANCTION_35.Identifiant = value
        End Set
    End Property

    Protected Sub ArmoireCourante_CmdAlbum_Click(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.AppelAlbumClickEventArgs) Handles ArmoireCourante.CmdAlbum_Click
        MultiOnglets.SetActiveView(VueAlbum)
        Call ChargerAlbum(e.PointdeVue, e.Outil)
    End Sub

    Protected Sub ArmoireCourante_Dossier_Click(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.DossierClickEventArgs) Handles ArmoireCourante.Dossier_Click
        Identifiant = e.Identifiant
        Me.Master.IdentifiantCourant = e.Identifiant
    End Sub

    Protected Sub MsgVirtualia_ValeurRetour(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.MessageRetourEventArgs) _
    Handles MsgVirtualia.ValeurRetour

        Select Case e.Emetteur
            Case Is = "SuppFiche"
                Select Case e.NumeroObjet
                    Case VI.ObjetPer.ObaStatut
                        PER_STATUT_12.RetourDialogueSupp(e.ReponseMsg) = e.ChaineDatas
                        WebFct.PointeurContexte.Fenetre_VueActive(VI.CategorieRH.SituationAdministrative) = IStatut
                    Case VI.ObjetPer.ObaStatutDetache
                        PER_STATUT_DETACHE_54.RetourDialogueSupp(e.ReponseMsg) = e.ChaineDatas
                        WebFct.PointeurContexte.Fenetre_VueActive(VI.CategorieRH.SituationAdministrative) = IStatut
                    Case VI.ObjetPer.ObaActivite
                        PER_POSITION_13.RetourDialogueSupp(e.ReponseMsg) = e.ChaineDatas
                        WebFct.PointeurContexte.Fenetre_VueActive(VI.CategorieRH.SituationAdministrative) = IPosition
                    Case VI.ObjetPer.ObaPositionDetache
                        PER_POSITION_DETACHE_55.RetourDialogueSupp(e.ReponseMsg) = e.ChaineDatas
                        WebFct.PointeurContexte.Fenetre_VueActive(VI.CategorieRH.SituationAdministrative) = IPosition
                    Case VI.ObjetPer.ObaStatut
                        PER_STATUT_12.RetourDialogueSupp(e.ReponseMsg) = e.ChaineDatas
                        WebFct.PointeurContexte.Fenetre_VueActive(VI.CategorieRH.SituationAdministrative) = IStatut
                    Case VI.ObjetPer.ObaStatutDetache
                        PER_STATUT_DETACHE_54.RetourDialogueSupp(e.ReponseMsg) = e.ChaineDatas
                        WebFct.PointeurContexte.Fenetre_VueActive(VI.CategorieRH.SituationAdministrative) = IStatut
                    Case VI.ObjetPer.ObaGrade
                        PER_GRADE_14.RetourDialogueSupp(e.ReponseMsg) = e.ChaineDatas
                        WebFct.PointeurContexte.Fenetre_VueActive(VI.CategorieRH.SituationAdministrative) = IGrade
                    Case VI.ObjetPer.ObaGradeDetache
                        PER_GRADE_DETACHE_56.RetourDialogueSupp(e.ReponseMsg) = e.ChaineDatas
                        WebFct.PointeurContexte.Fenetre_VueActive(VI.CategorieRH.SituationAdministrative) = IGrade
                    Case VI.ObjetPer.ObaIntegrationCorps
                        PER_INTEGRATION_68.RetourDialogueSupp(e.ReponseMsg) = e.ChaineDatas
                        WebFct.PointeurContexte.Fenetre_VueActive(VI.CategorieRH.SituationAdministrative) = IAnciennete
                    Case VI.ObjetPer.ObaSanction
                        PER_SANCTION_35.RetourDialogueSupp(e.ReponseMsg) = e.ChaineDatas
                        WebFct.PointeurContexte.Fenetre_VueActive(VI.CategorieRH.SituationAdministrative) = ISanction
                End Select
            Case Is = "SuppDossier"
                Call SupprimerDossier(e.ReponseMsg)
                Exit Sub
            Case Is = "MajDossier"
                Select Case e.NumeroObjet
                    Case VI.ObjetPer.ObaStatut
                        PER_STATUT_12.V_RetourDialogueMaj(e.ReponseMsg) = e.ChaineDatas
                        WebFct.PointeurContexte.Fenetre_VueActive(VI.CategorieRH.SituationAdministrative) = IStatut
                    Case VI.ObjetPer.ObaStatutDetache
                        PER_STATUT_DETACHE_54.V_RetourDialogueMaj(e.ReponseMsg) = e.ChaineDatas
                        WebFct.PointeurContexte.Fenetre_VueActive(VI.CategorieRH.SituationAdministrative) = IStatut
                    Case VI.ObjetPer.ObaActivite
                        PER_POSITION_13.V_RetourDialogueMaj(e.ReponseMsg) = e.ChaineDatas
                        WebFct.PointeurContexte.Fenetre_VueActive(VI.CategorieRH.SituationAdministrative) = IPosition
                    Case VI.ObjetPer.ObaPositionDetache
                        PER_POSITION_DETACHE_55.V_RetourDialogueMaj(e.ReponseMsg) = e.ChaineDatas
                        WebFct.PointeurContexte.Fenetre_VueActive(VI.CategorieRH.SituationAdministrative) = IPosition
                    Case VI.ObjetPer.ObaStatut
                        PER_STATUT_12.V_RetourDialogueMaj(e.ReponseMsg) = e.ChaineDatas
                        WebFct.PointeurContexte.Fenetre_VueActive(VI.CategorieRH.SituationAdministrative) = IStatut
                    Case VI.ObjetPer.ObaStatutDetache
                        PER_STATUT_DETACHE_54.V_RetourDialogueMaj(e.ReponseMsg) = e.ChaineDatas
                        WebFct.PointeurContexte.Fenetre_VueActive(VI.CategorieRH.SituationAdministrative) = IStatut
                    Case VI.ObjetPer.ObaGrade
                        PER_GRADE_14.V_RetourDialogueMaj(e.ReponseMsg) = e.ChaineDatas
                        WebFct.PointeurContexte.Fenetre_VueActive(VI.CategorieRH.SituationAdministrative) = IGrade
                    Case VI.ObjetPer.ObaGradeDetache
                        PER_GRADE_DETACHE_56.V_RetourDialogueMaj(e.ReponseMsg) = e.ChaineDatas
                        WebFct.PointeurContexte.Fenetre_VueActive(VI.CategorieRH.SituationAdministrative) = IGrade
                    Case VI.ObjetPer.ObaIntegrationCorps
                        PER_INTEGRATION_68.V_RetourDialogueMaj(e.ReponseMsg) = e.ChaineDatas
                        WebFct.PointeurContexte.Fenetre_VueActive(VI.CategorieRH.SituationAdministrative) = IAnciennete
                    Case VI.ObjetPer.ObaOrigine
                        PER_ADMORIGINE_25.V_RetourDialogueMaj(e.ReponseMsg) = e.ChaineDatas
                        WebFct.PointeurContexte.Fenetre_VueActive(VI.CategorieRH.SituationAdministrative) = IAnciennete
                    Case VI.ObjetPer.ObaSanction
                        PER_SANCTION_35.V_RetourDialogueMaj(e.ReponseMsg) = e.ChaineDatas
                        WebFct.PointeurContexte.Fenetre_VueActive(VI.CategorieRH.SituationAdministrative) = ISanction
                End Select
        End Select
        CadreSaisie.BackImageUrl = "~/Images/Fonds/Fond_Saisie.jpg"
        ConteneurVues.Width = New Unit(820)
        ArmoireCourante.Visible = True
        MultiOnglets.ActiveViewIndex = WebFct.PointeurContexte.Fenetre_VueActive(VI.CategorieRH.SituationAdministrative)
    End Sub

    Protected Sub ArmoireCourante_ValeurChange(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.DonneeChangeEventArgs) Handles ArmoireCourante.ValeurChange
        Dim Eti As System.Web.UI.WebControls.Label
        Eti = CType(WebFct.VirWebControle(Me.Master, "MsgArmoire", 0), System.Web.UI.WebControls.Label)
        Eti.Text = e.Valeur
        Eti = CType(WebFct.VirWebControle(Me.Master, "MsgArmoirePied", 0), System.Web.UI.WebControls.Label)
        Eti.Text = e.Valeur
    End Sub

    Protected Sub Onglet_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BoutonN1.Click, BoutonN2.Click, BoutonN3.Click, BoutonN4.Click, BoutonN5.Click, BoutonN6.Click
        CadreSaisie.BackImageUrl = "~/Images/Fonds/Fond_Saisie.jpg"
        Select Case CType(sender, Button).ID
            Case Is = "BoutonN1"
                MultiOnglets.SetActiveView(VueHisto)
            Case Is = "BoutonN2"
                MultiOnglets.SetActiveView(VueStatut)
            Case Is = "BoutonN3"
                MultiOnglets.SetActiveView(VuePosition)
            Case Is = "BoutonN4"
                MultiOnglets.SetActiveView(VueGrade)
            Case Is = "BoutonN5"
                MultiOnglets.SetActiveView(VueAnciennetes)
            Case Is = "BoutonN6"
                MultiOnglets.SetActiveView(VueSanction)
        End Select
        Dim Eti As System.Web.UI.WebControls.Label
        Eti = CType(WebFct.VirWebControle(Me.Master, "MsgSaisie", 0), System.Web.UI.WebControls.Label)
        Eti.Text = ""

        Eti = CType(WebFct.VirWebControle(Me.Master, "MsgSaisiePied", 0), System.Web.UI.WebControls.Label)
        Eti.Text = ""
        Call Initialiser()
        WebFct.PointeurContexte.Fenetre_VueActive(VI.CategorieRH.SituationAdministrative) = MultiOnglets.ActiveViewIndex
    End Sub

    Protected Sub Referentiel_RetourEventHandler(ByVal sender As Object, ByVal e As System.EventArgs) Handles Referentiel.RetourEventHandler
        CadreSaisie.BackImageUrl = "~/Images/Fonds/Fond_Saisie.jpg"
        ConteneurVues.Width = New Unit(820)
        ArmoireCourante.Visible = True
        Select Case WebFct.PointeurContexte.SysRef_IDVueRetour
            Case "VueStatut"
                MultiOnglets.SetActiveView(VueStatut)
            Case "VuePosition"
                MultiOnglets.SetActiveView(VuePosition)
            Case "VueGrade"
                MultiOnglets.SetActiveView(VueGrade)
            Case "VueAnciennetes"
                MultiOnglets.SetActiveView(VueAnciennetes)
            Case "VueSanction"
                MultiOnglets.SetActiveView(VueSanction)
        End Select
        WebFct.PointeurContexte.Fenetre_VueActive(VI.CategorieRH.SituationAdministrative) = MultiOnglets.ActiveViewIndex
    End Sub

    Protected Sub Referentiel_ValeurSelectionnee(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.ValeurSelectionneeEventArgs) Handles Referentiel.ValeurSelectionnee
        CadreSaisie.BackImageUrl = "~/Images/Fonds/Fond_Saisie.jpg"
        ConteneurVues.Width = New Unit(820)
        ArmoireCourante.Visible = True
        Select Case WebFct.PointeurContexte.SysRef_IDVueRetour
            Case "VueStatut"
                Select Case e.ObjetAppelant
                    Case VI.ObjetPer.ObaStatut
                        PER_STATUT_12.Dontab_RetourAppelTable(e.ControleAppelant) = e.Valeur
                    Case VI.ObjetPer.ObaStatutDetache
                        PER_STATUT_DETACHE_54.Dontab_RetourAppelTable(e.ControleAppelant) = e.Valeur
                End Select
                MultiOnglets.SetActiveView(VueStatut)
            Case "VuePosition"
                Select Case e.ObjetAppelant
                    Case VI.ObjetPer.ObaActivite
                        PER_POSITION_13.Dontab_RetourAppelTable(e.ControleAppelant) = e.Valeur
                    Case VI.ObjetPer.ObaPositionDetache
                        PER_POSITION_DETACHE_55.Dontab_RetourAppelTable(e.ControleAppelant) = e.Valeur
                End Select
                MultiOnglets.SetActiveView(VuePosition)
            Case "VueGrade"
                Select Case e.ObjetAppelant
                    Case VI.ObjetPer.ObaGrade
                        PER_GRADE_14.Dontab_RetourAppelTable(e.ControleAppelant) = e.Valeur
                    Case VI.ObjetPer.ObaGradeDetache
                        PER_GRADE_DETACHE_56.Dontab_RetourAppelTable(e.ControleAppelant) = e.Valeur
                End Select
                MultiOnglets.SetActiveView(VueGrade)
            Case "VueAnciennetes"
                Select Case e.ObjetAppelant
                    Case VI.ObjetPer.ObaIntegrationCorps
                        PER_INTEGRATION_68.Dontab_RetourAppelTable(e.ControleAppelant) = e.Valeur
                    Case VI.ObjetPer.ObaOrigine
                        PER_ADMORIGINE_25.Dontab_RetourAppelTable(e.ControleAppelant) = e.Valeur
                End Select
                MultiOnglets.SetActiveView(VueAnciennetes)
            Case "VueSanction"
                Select Case e.ObjetAppelant
                    Case VI.ObjetPer.ObaSanction
                        PER_SANCTION_35.Dontab_RetourAppelTable(e.ControleAppelant) = e.Valeur
                End Select
                MultiOnglets.SetActiveView(VueSanction)
        End Select
        WebFct.PointeurContexte.Fenetre_VueActive(VI.CategorieRH.SituationAdministrative) = MultiOnglets.ActiveViewIndex
    End Sub

    Protected Sub PER_AppelTable(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.AppelTableEventArgs) Handles PER_STATUT_12.AppelTable, _
            PER_POSITION_13.AppelTable, PER_GRADE_14.AppelTable, PER_INTEGRATION_68.AppelTable, PER_STATUT_DETACHE_54.AppelTable, _
            PER_POSITION_DETACHE_55.AppelTable, PER_GRADE_DETACHE_56.AppelTable, PER_ADMORIGINE_25.AppelTable, PER_SANCTION_35.AppelTable
        Select Case e.ObjetAppelant
            Case VI.ObjetPer.ObaStatut, VI.ObjetPer.ObaStatutDetache
                WebFct.PointeurContexte.SysRef_IDVueRetour = VueStatut.ID
            Case VI.ObjetPer.ObaActivite, VI.ObjetPer.ObaPositionDetache
                WebFct.PointeurContexte.SysRef_IDVueRetour = VuePosition.ID
            Case VI.ObjetPer.ObaGrade, VI.ObjetPer.ObaGradeDetache
                WebFct.PointeurContexte.SysRef_IDVueRetour = VueGrade.ID
            Case VI.ObjetPer.ObaIntegrationCorps, VI.ObjetPer.ObaOrigine
                WebFct.PointeurContexte.SysRef_IDVueRetour = VueAnciennetes.ID
            Case VI.ObjetPer.ObaSanction
                WebFct.PointeurContexte.SysRef_IDVueRetour = VueSanction.ID
            Case Else
                Exit Sub
        End Select
        ArmoireCourante.Visible = False

        Referentiel.V_PointdeVue = e.PointdeVueInverse
        Referentiel.V_NomTable = e.NomdelaTable
        Select Case e.ObjetAppelant
            Case VI.ObjetPer.ObaGrade, VI.ObjetPer.ObaGradeDetache
                WebFct.PointeurContexte.TsTampon = Nothing
                Referentiel.V_Appelant(e.ObjetAppelant, e.DatedeValeur) = e.ControleAppelant
            Case Else
                Referentiel.V_Appelant(e.ObjetAppelant) = e.ControleAppelant
        End Select
        CadreSaisie.BackImageUrl = ""
        CadreSaisie.BackColor = WebFct.ConvertCouleur("#216B68")
        MultiOnglets.SetActiveView(VueSysRef)
        WebFct.PointeurContexte.Fenetre_VueActive(VI.CategorieRH.SituationAdministrative) = MultiOnglets.ActiveViewIndex
        ConteneurVues.Width = New Unit(1150)

    End Sub

    Protected Sub MessageDialogue(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.MessageSaisieEventArgs) _
    Handles ArmoireCourante.MessageDialogue, PER_STATUT_12.MessageDialogue, PER_POSITION_13.MessageDialogue, PER_GRADE_14.MessageDialogue, _
    PER_INTEGRATION_68.MessageDialogue, PER_ADMORIGINE_25.MessageDialogue, PER_SANCTION_35.MessageDialogue, PER_STATUT_DETACHE_54.MessageDialogue, _
    PER_POSITION_DETACHE_55.MessageDialogue, PER_GRADE_DETACHE_56.MessageDialogue

        MsgVirtualia.AfficherMessage = e
        CadreSaisie.BackImageUrl = ""
        CadreSaisie.BackColor = WebFct.ConvertCouleur("#216B68")
        ArmoireCourante.Visible = False
        MultiOnglets.SetActiveView(VueMessage)
        WebFct.PointeurContexte.Fenetre_VueActive(VI.CategorieRH.SituationAdministrative) = MultiOnglets.ActiveViewIndex
        ConteneurVues.Width = New Unit(1150)
    End Sub

    Protected Sub MessageSaisie(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.MessageSaisieEventArgs) _
    Handles PER_STATUT_12.MessageSaisie, PER_POSITION_13.MessageSaisie, PER_GRADE_14.MessageSaisie, _
    PER_INTEGRATION_68.MessageSaisie, PER_ADMORIGINE_25.MessageSaisie, PER_SANCTION_35.MessageSaisie, _
    PER_STATUT_DETACHE_54.MessageSaisie, PER_POSITION_DETACHE_55.MessageSaisie, PER_GRADE_DETACHE_56.MessageSaisie

        Dim Eti As System.Web.UI.WebControls.Label
        Eti = CType(WebFct.VirWebControle(Me.Master, "MsgSaisie", 0), System.Web.UI.WebControls.Label)
        Select Case e.NatureMessage
            Case Is = "OK"
                Eti.ForeColor = Drawing.Color.Green
            Case Else
                Eti.ForeColor = Drawing.Color.Red
        End Select
        Eti.Text = e.ContenuMessage

        Eti = CType(WebFct.VirWebControle(Me.Master, "MsgSaisiePied", 0), System.Web.UI.WebControls.Label)
        Select Case e.NatureMessage
            Case Is = "OK"
                Eti.ForeColor = Drawing.Color.Green
            Case Else
                Eti.ForeColor = Drawing.Color.Red
        End Select
        Eti.Text = e.ContenuMessage

    End Sub

    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        Dim NumSession As String = Me.Master.V_NoSession
        If NumSession = "" Then
            NumSession = Server.HtmlDecode(Request.QueryString("IDVirtualia"))
            Me.Master.V_NoSession = NumSession
        End If
        If NumSession = "" Then
            Dim Msg As String = "Erreur lors de l'identification."
            Response.Redirect("~/Fenetres/Connexion/ErreurApplication.aspx?Msg=" & Msg)
        End If
        WebFct = New Virtualia.Net.WebFonctions(Me, 1)
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        If WebFct.PointeurContexte.Armoire_Identifiant <> 0 Then
            Identifiant = WebFct.PointeurContexte.Armoire_Identifiant
            MultiOnglets.ActiveViewIndex = WebFct.PointeurContexte.Fenetre_VueActive(VI.CategorieRH.SituationAdministrative)
            Select Case MultiOnglets.ActiveViewIndex
                Case Is = IAlbum
                    If WebFct.PointeurContexte.Fenetre_VuePrecedente(VI.CategorieRH.SituationAdministrative) <> 7 Then
                        Call ChargerAlbum(0, 0)
                    End If
                Case Else
                    Call Initialiser()
            End Select
        End If
    End Sub

    Protected Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
        WebFct.Dispose()
    End Sub

    Private Sub ChargerAlbum(ByVal PtdeVue As Short, ByVal Outil As Short)
        If PtdeVue = 0 Then
            PtdeVue = WebFct.PointeurContexte.Fenetre_PointdeVue
            Outil = WebFct.PointeurContexte.Fenetre_Outil
        End If
        AlbumRequetes.V_PointdeVue = PtdeVue
        AlbumRequetes.V_Outil = Outil
        Call Initialiser()
        CadreSaisie.BackImageUrl = ""
        CadreSaisie.BackColor = WebFct.ConvertCouleur("#216B68")
        WebFct.PointeurContexte.Fenetre_PointdeVue = PtdeVue
        WebFct.PointeurContexte.Fenetre_Outil = Outil
        WebFct.PointeurContexte.Fenetre_VueActive(VI.CategorieRH.SituationAdministrative) = MultiOnglets.ActiveViewIndex
    End Sub

    Private Sub Initialiser()
        Dim Ctl As Control
        Dim BtnControle As System.Web.UI.WebControls.Button
        Dim IndiceI As Integer = 0
        Dim K As Integer = 0
        Do
            Ctl = WebFct.VirWebControle(Me.CadreOnglets, "Bouton", IndiceI)
            If Ctl Is Nothing Then
                Exit Do
            End If
            BtnControle = CType(Ctl, System.Web.UI.WebControls.Button)
            K = CInt(Strings.Right(BtnControle.ID, 1)) - 1
            If K = MultiOnglets.ActiveViewIndex Then
                BtnControle.Font.Bold = True
                BtnControle.Height = New Unit(24)
                BtnControle.Width = New Unit(135)
                BtnControle.BackColor = WebFct.ConvertCouleur("#7D9F99")
                BtnControle.ForeColor = System.Drawing.Color.White
            Else
                BtnControle.Font.Bold = False
                BtnControle.Height = New Unit(33)
                BtnControle.Width = New Unit(148)
                BtnControle.BackColor = System.Drawing.Color.Transparent
                BtnControle.ForeColor = WebFct.ConvertCouleur("#142425")
            End If
            IndiceI += 1
        Loop

        Select Case MultiOnglets.ActiveViewIndex
            Case ISysref, IEdition, IMessage
                ConteneurVues.Width = New Unit(1150)
                ArmoireCourante.Visible = False
            Case Else
                ConteneurVues.Width = New Unit(820)
                ArmoireCourante.Visible = True
        End Select
    End Sub

    Protected Sub AlbumRequetes_ScriptResultat(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.ScriptResultatEventArgs) Handles AlbumRequetes.ScriptResultat
        Dim I As Integer
        For I = 0 To e.Centrage_Colonne.Count - 1
            EditionArmoire.Centrage_Colonne(I) = e.Centrage_Colonne(I)
        Next I
        ArmoireCourante.Visible = False
        EditionArmoire.VDataGrid(e.IntitulesColonnes, e.Libelles) = e.Valeurs
        EditionArmoire.VIntitule = AlbumRequetes.VTitre
        MultiOnglets.SetActiveView(VueEdition)
        WebFct.PointeurContexte.Fenetre_VueActive(VI.CategorieRH.SituationAdministrative) = MultiOnglets.ActiveViewIndex
    End Sub

    Protected Sub EditionArmoire_RetourEventHandler(ByVal sender As Object, ByVal e As System.EventArgs) Handles EditionArmoire.RetourEventHandler
        MultiOnglets.ActiveViewIndex = IAlbum
        WebFct.PointeurContexte.Fenetre_VueActive(VI.CategorieRH.SituationAdministrative) = MultiOnglets.ActiveViewIndex
        Call Initialiser()
    End Sub

    Protected Sub AlbumRequetes_ValeurChange(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.DonneeChangeEventArgs) Handles AlbumRequetes.ValeurChange
        WebFct.PointeurContexte.Armoire_Identifiant = 0
        ArmoireCourante.ArmoirePersonnalisee = e.Valeur

        Dim Eti As System.Web.UI.WebControls.Label
        Eti = CType(WebFct.VirWebControle(Me.Master, "MsgCourant", 0), System.Web.UI.WebControls.Label)
        Eti.Text = ""
        Eti = CType(WebFct.VirWebControle(Me.Master, "MsgCourantPied", 0), System.Web.UI.WebControls.Label)
        Eti.Text = ""

        Eti = CType(WebFct.VirWebControle(Me.Master, "MsgArmoire", 0), System.Web.UI.WebControls.Label)
        Eti.Text = e.Valeur & Strings.Space(1) & ArmoireCourante.CompteDossiers
        Eti = CType(WebFct.VirWebControle(Me.Master, "MsgArmoirePied", 0), System.Web.UI.WebControls.Label)
        Eti.Text = e.Valeur & Strings.Space(1) & ArmoireCourante.CompteDossiers
    End Sub

    Private Sub SupprimerDossier(ByVal MsgReponse As String)
        If MsgReponse = "Non" Then
            CadreSaisie.BackImageUrl = "~/Images/Fonds/Fond_Saisie.jpg"
            ConteneurVues.Width = New Unit(820)
            ArmoireCourante.Visible = True
            MultiOnglets.ActiveViewIndex = 0
            Exit Sub
        End If
        Select Case MsgReponse
            Case "OK", "KO"
                If MsgReponse = "OK" Then
                    Me.Master.IdentifiantCourant = 0
                    Identifiant = 0
                    WebFct.PointeurContexte.Armoire_Identifiant = 0
                    ArmoireCourante.Actualiser()
                End If
                CadreSaisie.BackImageUrl = "~/Images/Fonds/Fond_Saisie.jpg"
                ConteneurVues.Width = New Unit(820)
                WebFct.PointeurContexte.Fenetre_VueActive(VI.CategorieRH.InfosPersonnelles) = 0
                MultiOnglets.SetActiveView(VueHisto)
                ArmoireCourante.Visible = True
                Exit Sub
        End Select
        '** Traitement du Oui à la suppression
        Dim Cretour As Boolean
        Dim Ide As Integer = WebFct.PointeurContexte.Armoire_Identifiant
        Dim Dossier As Virtualia.Ressources.Datas.ObjetDossierPER
        Dossier = WebFct.PointeurDossier(Ide)
        If Dossier Is Nothing Then
            Exit Sub
        End If
        Dim NomPrenom As String = Dossier.Nom & Strings.Space(1) & Dossier.Prenom

        Cretour = Dossier.V_SupprimerDossier(WebFct.PointeurUtilisateur.V_NomdUtilisateurSgbd, WebFct.PointeurUtilisateur.V_NomdeConnexion, _
                                                    WebFct.PointeurUtilisateur.InstanceBd)

        Dim TitreMsg As String = "Supprimer un dossier"
        Dim ContenuMsg As String = "le dossier N° " & Ide.ToString & " - " & NomPrenom
        Dim Evenement As Virtualia.Systeme.Evenements.MessageSaisieEventArgs
        If Cretour = True Then
            ContenuMsg &= Strings.Chr(13) & Strings.Chr(10) & "a bien été supprimé."
            WebFct.PointeurArmoire.RetirerDossier(Dossier)
            Evenement = New Virtualia.Systeme.Evenements.MessageSaisieEventArgs("SuppDossier", 1, "", "OK", TitreMsg, ContenuMsg)
        Else
            ContenuMsg &= Strings.Chr(13) & Strings.Chr(10) & "n'a pas pu être supprimé"
            Evenement = New Virtualia.Systeme.Evenements.MessageSaisieEventArgs("SuppDossier", 1, "", "KO", TitreMsg, ContenuMsg)
        End If

        MsgVirtualia.AfficherMessage = Evenement
        CadreSaisie.BackImageUrl = ""
        CadreSaisie.BackColor = WebFct.ConvertCouleur("#216B68")
        ArmoireCourante.Visible = False
        MultiOnglets.SetActiveView(VueMessage)
        ConteneurVues.Width = New Unit(1150)
    End Sub

End Class
