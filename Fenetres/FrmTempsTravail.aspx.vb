﻿Option Strict On
Option Explicit On
Option Compare Text
Imports VI = Virtualia.Systeme.Constantes
Partial Class Fenetres_FrmTempsTravail
    Inherits System.Web.UI.Page
    Private WebFct As Virtualia.Net.WebFonctions
    Private WsNomState As String = "Vtravail"
    Private Const ISituation As Short = 0
    Private Const IDroits As Short = 1
    Private Const IPrevisionnel As Short = 2
    Private Const IAbsences As Short = 3
    Private Const IConstate As Short = 4
    Private Const IDebitCredit As Short = 5
    Private Const IReleves As Short = 6
    Private Const ICET As Short = 7
    Private Const ISysref As Short = 8
    Private Const IAlbum As Short = 9
    Private Const IEdition As Short = 10
    Private Const IMessage As Short = 11
    Private Const ICalendrier As Short = 12

    Public Property Identifiant() As Integer
        Get
            Return CInt(HSelIde.Value)
        End Get
        Set(ByVal value As Integer)
            If value = CInt(HSelIde.Value) Then
                Exit Property
            End If
            HSelIde.Value = value.ToString
            If value > 0 Then
                WebFct.PointeurDossier(value).LireDossierRTT(WebFct.PointeurUtilisateur.V_NomdUtilisateurSgbd)
            End If
            PER_VDebitCredit.Identifiant = value
            PER_SITUATION_CONGES_15.Identifiant = value
            PER_VPlanning.Identifiant = value
            PER_VMensuel.Identifiant = value
            PER_VSemaine.Identifiant = value

            PER_ABSENCE_15.Identifiant = value
            PER_DROIT_CONGES_16.Identifiant = value
            PER_TRAVAIL_33.Identifiant = value
            PER_CET_HEBDO_43.Identifiant = value
            PER_EVENEMENTJOUR_40.Identifiant = value
            PER_TEMPS_TRAVAIL_41.Identifiant = value
            PER_ANNUALISATION_42.Identifiant = value
            PER_POINTAGE_79.Identifiant = value
            PER_CET_80.Identifiant = value

            'Calendrier.Identifiant = value
        End Set
    End Property

    Protected Sub ArmoireCourante_CmdAlbum_Click(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.AppelAlbumClickEventArgs) Handles ArmoireCourante.CmdAlbum_Click
        MultiOnglets.SetActiveView(VueAlbum)
        Call ChargerAlbum(e.PointdeVue, e.Outil)
    End Sub

    Protected Sub ArmoireCourante_Dossier_Click(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.DossierClickEventArgs) Handles ArmoireCourante.Dossier_Click
        Identifiant = e.Identifiant
        Me.Master.IdentifiantCourant = e.Identifiant
    End Sub

    Protected Sub MsgVirtualia_ValeurRetour(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.MessageRetourEventArgs) Handles MsgVirtualia.ValeurRetour
        Select Case e.Emetteur
            Case Is = "SuppFiche"
                Select Case e.NumeroObjet
                    Case VI.ObjetPer.ObaAbsence
                        PER_ABSENCE_15.RetourDialogueSupp(e.ReponseMsg) = e.ChaineDatas
                        WebFct.PointeurContexte.Fenetre_VueActive(VI.CategorieRH.TempsdeTravail) = IAbsences
                    Case VI.ObjetPer.ObaDroits
                        PER_DROIT_CONGES_16.RetourDialogueSupp(e.ReponseMsg) = e.ChaineDatas
                        WebFct.PointeurContexte.Fenetre_VueActive(VI.CategorieRH.TempsdeTravail) = IDroits
                    Case VI.ObjetPer.ObaPresence
                        PER_TRAVAIL_33.RetourDialogueSupp(e.ReponseMsg) = e.ChaineDatas
                        WebFct.PointeurContexte.Fenetre_VueActive(VI.CategorieRH.TempsdeTravail) = IPrevisionnel
                    Case VI.ObjetPer.ObaJournee
                        PER_EVENEMENTJOUR_40.RetourDialogueSupp(e.ReponseMsg) = e.ChaineDatas
                        WebFct.PointeurContexte.Fenetre_VueActive(VI.CategorieRH.TempsdeTravail) = IConstate
                    Case VI.ObjetPer.ObaValtemps
                        PER_TEMPS_TRAVAIL_41.RetourDialogueSupp(e.ReponseMsg) = e.ChaineDatas
                        WebFct.PointeurContexte.Fenetre_VueActive(VI.CategorieRH.TempsdeTravail) = IDroits
                    Case VI.ObjetPer.ObaAnnualisation
                        PER_ANNUALISATION_42.RetourDialogueSupp(e.ReponseMsg) = e.ChaineDatas
                        WebFct.PointeurContexte.Fenetre_VueActive(VI.CategorieRH.TempsdeTravail) = IDroits
                    Case VI.ObjetPer.ObaPointage
                        PER_POINTAGE_79.RetourDialogueSupp(e.ReponseMsg) = e.ChaineDatas
                        WebFct.PointeurContexte.Fenetre_VueActive(VI.CategorieRH.TempsdeTravail) = IConstate
                End Select
            Case Is = "SuppDossier"
                Call SupprimerDossier(e.ReponseMsg)
                Exit Sub
            Case Is = "MajDossier"
                Select Case e.NumeroObjet
                    Case VI.ObjetPer.ObaAbsence
                        PER_ABSENCE_15.V_RetourDialogueMaj(e.ReponseMsg) = e.ChaineDatas
                        WebFct.PointeurContexte.Fenetre_VueActive(VI.CategorieRH.TempsdeTravail) = IAbsences
                    Case VI.ObjetPer.ObaDroits
                        PER_DROIT_CONGES_16.V_RetourDialogueMaj(e.ReponseMsg) = e.ChaineDatas
                        WebFct.PointeurContexte.Fenetre_VueActive(VI.CategorieRH.TempsdeTravail) = IDroits
                    Case VI.ObjetPer.ObaPresence
                        PER_TRAVAIL_33.V_RetourDialogueMaj(e.ReponseMsg) = e.ChaineDatas
                        WebFct.PointeurContexte.Fenetre_VueActive(VI.CategorieRH.TempsdeTravail) = IPrevisionnel
                    Case VI.ObjetPer.ObaJournee
                        PER_EVENEMENTJOUR_40.V_RetourDialogueMaj(e.ReponseMsg) = e.ChaineDatas
                        WebFct.PointeurContexte.Fenetre_VueActive(VI.CategorieRH.TempsdeTravail) = IConstate
                    Case VI.ObjetPer.ObaValtemps
                        PER_TEMPS_TRAVAIL_41.V_RetourDialogueMaj(e.ReponseMsg) = e.ChaineDatas
                        WebFct.PointeurContexte.Fenetre_VueActive(VI.CategorieRH.TempsdeTravail) = IDroits
                    Case VI.ObjetPer.ObaAnnualisation
                        PER_ANNUALISATION_42.V_RetourDialogueMaj(e.ReponseMsg) = e.ChaineDatas
                        WebFct.PointeurContexte.Fenetre_VueActive(VI.CategorieRH.TempsdeTravail) = IDroits
                    Case VI.ObjetPer.ObaPointage
                        PER_POINTAGE_79.V_RetourDialogueMaj(e.ReponseMsg) = e.ChaineDatas
                        WebFct.PointeurContexte.Fenetre_VueActive(VI.CategorieRH.TempsdeTravail) = IConstate
                End Select
        End Select
        CadreSaisie.BackImageUrl = "~/Images/Fonds/Fond_Saisie.jpg"
        ConteneurVues.Width = New Unit(820)
        ArmoireCourante.Visible = True
        MultiOnglets.ActiveViewIndex = WebFct.PointeurContexte.Fenetre_VueActive(VI.CategorieRH.TempsdeTravail)
    End Sub

    Protected Sub ArmoireCourante_ValeurChange(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.DonneeChangeEventArgs) Handles ArmoireCourante.ValeurChange
        Dim Eti As System.Web.UI.WebControls.Label
        Eti = CType(WebFct.VirWebControle(Me.Master, "MsgArmoire", 0), System.Web.UI.WebControls.Label)
        Eti.Text = e.Valeur
        Eti = CType(WebFct.VirWebControle(Me.Master, "MsgArmoirePied", 0), System.Web.UI.WebControls.Label)
        Eti.Text = e.Valeur
    End Sub

    Protected Sub Onglet_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BoutonN1.Click, BoutonN2.Click, _
    BoutonN3.Click, BoutonN4.Click, BoutonN5.Click, BoutonN6.Click, BoutonN7.Click, BoutonN8.Click

        CadreSaisie.BackImageUrl = "~/Images/Fonds/Fond_Saisie.jpg"
        ConteneurVues.Width = New Unit(820)
        ArmoireCourante.Visible = True
        Select Case CType(sender, Button).ID
            Case Is = "BoutonN1"
                MultiOnglets.SetActiveView(VueSituation)
            Case Is = "BoutonN2"
                MultiOnglets.SetActiveView(VueDroits)
            Case Is = "BoutonN3"
                MultiOnglets.SetActiveView(VuePrevision)
            Case Is = "BoutonN4"
                MultiOnglets.SetActiveView(VueAbsences)
            Case Is = "BoutonN5"
                MultiOnglets.SetActiveView(VueConstate)
            Case Is = "BoutonN6"
                MultiOnglets.SetActiveView(VueDebitCredit)
            Case Is = "BoutonN7"
                MultiOnglets.SetActiveView(VueReleves)
            Case Is = "BoutonN8"
                MultiOnglets.SetActiveView(VueCET)
        End Select
        Dim Eti As System.Web.UI.WebControls.Label
        Eti = CType(WebFct.VirWebControle(Me.Master, "MsgSaisie", 0), System.Web.UI.WebControls.Label)
        Eti.Text = ""

        Eti = CType(WebFct.VirWebControle(Me.Master, "MsgSaisiePied", 0), System.Web.UI.WebControls.Label)
        Eti.Text = ""
        Call Initialiser()
        WebFct.PointeurContexte.Fenetre_VueActive(VI.CategorieRH.TempsdeTravail) = MultiOnglets.ActiveViewIndex
    End Sub

    Protected Sub Referentiel_RetourEventHandler(ByVal sender As Object, ByVal e As System.EventArgs) Handles Referentiel.RetourEventHandler
        CadreSaisie.BackImageUrl = "~/Images/Fonds/Fond_Saisie.jpg"
        ConteneurVues.Width = New Unit(820)
        ArmoireCourante.Visible = True
        Select Case WebFct.PointeurContexte.SysRef_IDVueRetour
            Case "VueSituation"
                MultiOnglets.SetActiveView(VueSituation)
            Case "VueDroits"
                MultiOnglets.SetActiveView(VueDroits)
            Case "VuePrevision"
                MultiOnglets.SetActiveView(VuePrevision)
            Case "VueAbsences"
                MultiOnglets.SetActiveView(VueAbsences)
            Case "VueConstate"
                MultiOnglets.SetActiveView(VueConstate)
            Case "VueDebitCredit"
                MultiOnglets.SetActiveView(VueDebitCredit)
            Case "VueReleves"
                MultiOnglets.SetActiveView(VueReleves)
            Case "VueCET"
                MultiOnglets.SetActiveView(VueCET)
        End Select
        WebFct.PointeurContexte.Fenetre_VueActive(VI.CategorieRH.TempsdeTravail) = MultiOnglets.ActiveViewIndex
    End Sub

    Protected Sub Referentiel_ValeurSelectionnee(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.ValeurSelectionneeEventArgs) Handles Referentiel.ValeurSelectionnee
        CadreSaisie.BackImageUrl = "~/Images/Fonds/Fond_Saisie.jpg"
        ConteneurVues.Width = New Unit(820)
        ArmoireCourante.Visible = True
        Select Case WebFct.PointeurContexte.SysRef_IDVueRetour
            Case "VueDroits"
                Select Case e.ObjetAppelant
                    Case VI.ObjetPer.ObaAnnualisation
                        PER_ANNUALISATION_42.Dontab_RetourAppelTable(e.ControleAppelant) = e.Valeur
                    Case VI.ObjetPer.ObaValtemps
                        PER_TEMPS_TRAVAIL_41.Dontab_RetourAppelTable(e.ControleAppelant) = e.Valeur
                End Select
                MultiOnglets.SetActiveView(VueConstate)
            Case "VueAbsences"
                Select Case e.ObjetAppelant
                    Case VI.ObjetPer.ObaAbsence
                        PER_ABSENCE_15.Dontab_RetourAppelTable(e.ControleAppelant) = e.Valeur
                End Select
                MultiOnglets.SetActiveView(VueAbsences)
            Case "VueConstate"
                Select Case e.ObjetAppelant
                    Case VI.ObjetPer.ObaValtemps
                        PER_EVENEMENTJOUR_40.Dontab_RetourAppelTable(e.ControleAppelant) = e.Valeur
                End Select
                MultiOnglets.SetActiveView(VueConstate)
            Case "VuePrevision"
                Select Case e.ObjetAppelant
                    Case VI.ObjetPer.ObaPresence
                        PER_TRAVAIL_33.Dontab_RetourAppelTable(e.ControleAppelant) = e.Valeur
                End Select
                MultiOnglets.SetActiveView(VuePrevision)
        End Select
        WebFct.PointeurContexte.Fenetre_VueActive(VI.CategorieRH.TempsdeTravail) = MultiOnglets.ActiveViewIndex
    End Sub

    Protected Sub PER_AppelTable(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.AppelTableEventArgs) _
    Handles PER_ABSENCE_15.AppelTable, PER_TRAVAIL_33.AppelTable, PER_EVENEMENTJOUR_40.AppelTable, PER_TEMPS_TRAVAIL_41.AppelTable, _
    PER_ANNUALISATION_42.AppelTable

        Select Case e.ObjetAppelant
            Case VI.ObjetPer.ObaAbsence
                WebFct.PointeurContexte.SysRef_IDVueRetour = VueAbsences.ID
            Case VI.ObjetPer.ObaPresence
                WebFct.PointeurContexte.SysRef_IDVueRetour = VuePrevision.ID
            Case VI.ObjetPer.ObaAnnualisation
                WebFct.PointeurContexte.SysRef_IDVueRetour = VueConstate.ID
            Case VI.ObjetPer.ObaDroits
                WebFct.PointeurContexte.SysRef_IDVueRetour = VueDroits.ID
            Case VI.ObjetPer.ObaJournee
                WebFct.PointeurContexte.SysRef_IDVueRetour = VueConstate.ID
            Case VI.ObjetPer.ObaValtemps
                WebFct.PointeurContexte.SysRef_IDVueRetour = VueDroits.ID
            Case Else
                Exit Sub
        End Select
        ArmoireCourante.Visible = False

        Referentiel.V_PointdeVue = e.PointdeVueInverse
        Referentiel.V_NomTable = e.NomdelaTable
        Referentiel.V_Appelant(e.ObjetAppelant) = e.ControleAppelant
        CadreSaisie.BackImageUrl = ""
        CadreSaisie.BackColor = WebFct.ConvertCouleur("#216B68")
        MultiOnglets.SetActiveView(VueSysRef)
        WebFct.PointeurContexte.Fenetre_VueActive(VI.CategorieRH.TempsdeTravail) = MultiOnglets.ActiveViewIndex
        ConteneurVues.Width = New Unit(1200)

    End Sub

    Protected Sub MessageDialogue(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.MessageSaisieEventArgs) _
    Handles ArmoireCourante.MessageDialogue, PER_ABSENCE_15.MessageDialogue, PER_DROIT_CONGES_16.MessageDialogue, _
     PER_TRAVAIL_33.MessageDialogue, PER_EVENEMENTJOUR_40.MessageDialogue, PER_TEMPS_TRAVAIL_41.MessageDialogue, _
     PER_ANNUALISATION_42.MessageDialogue, PER_POINTAGE_79.MessageDialogue, PER_CET_80.MessageDialogue

        MsgVirtualia.AfficherMessage = e
        CadreSaisie.BackImageUrl = ""
        CadreSaisie.BackColor = WebFct.ConvertCouleur("#216B68")
        ArmoireCourante.Visible = False
        MultiOnglets.SetActiveView(VueMessage)
        WebFct.PointeurContexte.Fenetre_VueActive(VI.CategorieRH.TempsdeTravail) = MultiOnglets.ActiveViewIndex
        ConteneurVues.Width = New Unit(1200)
    End Sub

    Protected Sub MessageSaisie(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.MessageSaisieEventArgs) _
    Handles PER_ABSENCE_15.MessageSaisie, PER_DROIT_CONGES_16.MessageSaisie, PER_TRAVAIL_33.MessageSaisie, _
    PER_EVENEMENTJOUR_40.MessageSaisie, PER_TEMPS_TRAVAIL_41.MessageSaisie, PER_ANNUALISATION_42.MessageSaisie, _
    PER_POINTAGE_79.MessageSaisie, PER_CET_80.MessageSaisie

        Dim Eti As System.Web.UI.WebControls.Label
        Eti = CType(WebFct.VirWebControle(Me.Master, "MsgSaisie", 0), System.Web.UI.WebControls.Label)
        Select Case e.NatureMessage
            Case Is = "OK"
                Eti.ForeColor = Drawing.Color.Green
        End Select
        Eti.Text = e.ContenuMessage

        Eti = CType(WebFct.VirWebControle(Me.Master, "MsgSaisiePied", 0), System.Web.UI.WebControls.Label)
        Select Case e.NatureMessage
            Case Is = "OK"
                Eti.ForeColor = Drawing.Color.Green
        End Select
        Eti.Text = e.ContenuMessage

    End Sub

    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        Dim NumSession As String = Me.Master.V_NoSession
        If NumSession = "" Then
            NumSession = Server.HtmlDecode(Request.QueryString("IDVirtualia"))
            Me.Master.V_NoSession = NumSession
        End If
        If NumSession = "" Then
            Dim Msg As String = "Erreur lors de l'identification."
            Response.Redirect("~/Fenetres/Connexion/ErreurApplication.aspx?Msg=" & Msg)
        End If
        WebFct = New Virtualia.Net.WebFonctions(Me, 1)
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        If WebFct.PointeurContexte.Armoire_Identifiant <> 0 Then
            Identifiant = WebFct.PointeurContexte.Armoire_Identifiant
            MultiOnglets.ActiveViewIndex = WebFct.PointeurContexte.Fenetre_VueActive(VI.CategorieRH.TempsdeTravail)
            Select Case MultiOnglets.ActiveViewIndex
                Case Is = IAlbum
                    If WebFct.PointeurContexte.Fenetre_VuePrecedente(VI.CategorieRH.TempsdeTravail) <> IAlbum Then
                        Call ChargerAlbum(0, 0)
                    End If
                Case Else
                    Call Initialiser()
            End Select
        End If
    End Sub

    Protected Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
        WebFct.Dispose()
    End Sub

    Private Sub Initialiser()
        Dim Ctl As Control
        Dim BtnControle As System.Web.UI.WebControls.Button
        Dim IndiceI As Integer = 0
        Dim K As Integer = 0
        Do
            Ctl = WebFct.VirWebControle(Me.CadreOnglets, "Bouton", IndiceI)
            If Ctl Is Nothing Then
                Exit Do
            End If
            BtnControle = CType(Ctl, System.Web.UI.WebControls.Button)
            K = CInt(Strings.Right(BtnControle.ID, 1)) - 1
            If K = MultiOnglets.ActiveViewIndex Then
                BtnControle.Font.Bold = True
                BtnControle.Height = New Unit(24)
                BtnControle.Width = New Unit(105)
                BtnControle.BackColor = WebFct.ConvertCouleur("#7D9F99")
                BtnControle.ForeColor = System.Drawing.Color.White
            Else
                BtnControle.Font.Bold = False
                BtnControle.Height = New Unit(33)
                BtnControle.Width = New Unit(118)
                BtnControle.BackColor = System.Drawing.Color.Transparent
                BtnControle.ForeColor = WebFct.ConvertCouleur("#142425")
            End If
            IndiceI += 1
        Loop
        Select Case MultiOnglets.ActiveViewIndex
            Case ISysref, IEdition, IMessage
                ConteneurVues.Width = New Unit(1200)
                ArmoireCourante.Visible = False
            Case Else
                ConteneurVues.Width = New Unit(820)
                ArmoireCourante.Visible = True
        End Select
    End Sub

    Private Sub ChargerAlbum(ByVal PtdeVue As Short, ByVal Outil As Short)
        If PtdeVue = 0 Then
            PtdeVue = WebFct.PointeurContexte.Fenetre_PointdeVue
            Outil = WebFct.PointeurContexte.Fenetre_Outil
        End If
        AlbumRequetes.V_PointdeVue = PtdeVue
        AlbumRequetes.V_Outil = Outil
        Call Initialiser()
        CadreSaisie.BackImageUrl = ""
        CadreSaisie.BackColor = WebFct.ConvertCouleur("#216B68")
        WebFct.PointeurContexte.Fenetre_PointdeVue = PtdeVue
        WebFct.PointeurContexte.Fenetre_Outil = Outil
        WebFct.PointeurContexte.Fenetre_VueActive(VI.CategorieRH.TempsdeTravail) = MultiOnglets.ActiveViewIndex
    End Sub

    Protected Sub AlbumRequetes_ScriptResultat(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.ScriptResultatEventArgs) Handles AlbumRequetes.ScriptResultat
        Dim I As Integer
        For I = 0 To e.Centrage_Colonne.Count - 1
            EditionArmoire.Centrage_Colonne(I) = e.Centrage_Colonne(I)
        Next I
        ArmoireCourante.Visible = False
        EditionArmoire.VDataGrid(e.IntitulesColonnes, e.Libelles) = e.Valeurs
        EditionArmoire.VIntitule = AlbumRequetes.VTitre
        MultiOnglets.SetActiveView(VueEdition)
        WebFct.PointeurContexte.Fenetre_VueActive(VI.CategorieRH.TempsdeTravail) = MultiOnglets.ActiveViewIndex
    End Sub

    Protected Sub EditionArmoire_RetourEventHandler(ByVal sender As Object, ByVal e As System.EventArgs) Handles EditionArmoire.RetourEventHandler
        MultiOnglets.ActiveViewIndex = IAlbum
        WebFct.PointeurContexte.Fenetre_VueActive(VI.CategorieRH.TempsdeTravail) = MultiOnglets.ActiveViewIndex
        Call Initialiser()
    End Sub

    Protected Sub AlbumRequetes_ValeurChange(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.DonneeChangeEventArgs) Handles AlbumRequetes.ValeurChange
        WebFct.PointeurContexte.Armoire_Identifiant = 0
        ArmoireCourante.ArmoirePersonnalisee = e.Valeur

        Dim Eti As System.Web.UI.WebControls.Label
        Eti = CType(WebFct.VirWebControle(Me.Master, "MsgCourant", 0), System.Web.UI.WebControls.Label)
        Eti.Text = ""
        Eti = CType(WebFct.VirWebControle(Me.Master, "MsgCourantPied", 0), System.Web.UI.WebControls.Label)
        Eti.Text = ""

        Eti = CType(WebFct.VirWebControle(Me.Master, "MsgArmoire", 0), System.Web.UI.WebControls.Label)
        Eti.Text = e.Valeur & Strings.Space(1) & ArmoireCourante.CompteDossiers
        Eti = CType(WebFct.VirWebControle(Me.Master, "MsgArmoirePied", 0), System.Web.UI.WebControls.Label)
        Eti.Text = e.Valeur & Strings.Space(1) & ArmoireCourante.CompteDossiers
    End Sub

    Private Sub SupprimerDossier(ByVal MsgReponse As String)
        If MsgReponse = "Non" Then
            CadreSaisie.BackImageUrl = "~/Images/Fonds/Fond_Saisie.jpg"
            ConteneurVues.Width = New Unit(820)
            ArmoireCourante.Visible = True
            MultiOnglets.ActiveViewIndex = 0
            Exit Sub
        End If
        Select Case MsgReponse
            Case "OK", "KO"
                If MsgReponse = "OK" Then
                    Me.Master.IdentifiantCourant = 0
                    Identifiant = 0
                    WebFct.PointeurContexte.Armoire_Identifiant = 0
                    ArmoireCourante.Actualiser()
                End If
                CadreSaisie.BackImageUrl = "~/Images/Fonds/Fond_Saisie.jpg"
                ConteneurVues.Width = New Unit(820)
                WebFct.PointeurContexte.Fenetre_VueActive(VI.CategorieRH.InfosPersonnelles) = 0
                MultiOnglets.SetActiveView(VueSituation)
                ArmoireCourante.Visible = True
                Exit Sub
        End Select
        '** Traitement du Oui à la suppression
        Dim Cretour As Boolean
        Dim Ide As Integer = WebFct.PointeurContexte.Armoire_Identifiant
        Dim Dossier As Virtualia.Ressources.Datas.ObjetDossierPER
        Dossier = WebFct.PointeurDossier(Ide)
        If Dossier Is Nothing Then
            Exit Sub
        End If
        Dim NomPrenom As String = Dossier.Nom & Strings.Space(1) & Dossier.Prenom

        Cretour = Dossier.V_SupprimerDossier(WebFct.PointeurUtilisateur.V_NomdUtilisateurSgbd, WebFct.PointeurUtilisateur.V_NomdeConnexion, _
                                                    WebFct.PointeurUtilisateur.InstanceBd)

        Dim TitreMsg As String = "Supprimer un dossier"
        Dim ContenuMsg As String = "le dossier N° " & Ide.ToString & " - " & NomPrenom
        Dim Evenement As Virtualia.Systeme.Evenements.MessageSaisieEventArgs
        If Cretour = True Then
            ContenuMsg &= Strings.Chr(13) & Strings.Chr(10) & "a bien été supprimé."
            WebFct.PointeurArmoire.RetirerDossier(Dossier)
            Evenement = New Virtualia.Systeme.Evenements.MessageSaisieEventArgs("SuppDossier", 1, "", "OK", TitreMsg, ContenuMsg)
        Else
            ContenuMsg &= Strings.Chr(13) & Strings.Chr(10) & "n'a pas pu être supprimé"
            Evenement = New Virtualia.Systeme.Evenements.MessageSaisieEventArgs("SuppDossier", 1, "", "KO", TitreMsg, ContenuMsg)
        End If

        MsgVirtualia.AfficherMessage = Evenement
        CadreSaisie.BackImageUrl = ""
        CadreSaisie.BackColor = WebFct.ConvertCouleur("#216B68")
        ArmoireCourante.Visible = False
        MultiOnglets.SetActiveView(VueMessage)
        ConteneurVues.Width = New Unit(1150)
    End Sub

End Class
