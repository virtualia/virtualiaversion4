﻿<%@ Page Language="VB" MasterPageFile="~/PagesMaitre/VirtualiaMain.master" AutoEventWireup="false" 
    Inherits="Virtualia.Net.Fenetres_FrmInfosPersonnelles" Codebehind="FrmInfosPersonnelles.aspx.vb"
    MaintainScrollPositionOnPostback="True" %>
<%@ MasterType VirtualPath="~/PagesMaitre/VirtualiaMain.master"  %>

<%@ Register src="../ControlesWebUser/ObjetsArmoire/ArmoirePersonnes.ascx" tagname="ArmoirePersonnes" tagprefix="VirtuArm" %>
<%@ Register src="../ControlesWebUser/ObjetsArmoire/ListeReferences.ascx" tagname="ListeReferences" tagprefix="VirtuRef" %>
<%@ Register src="../ControlesWebUser/ObjetsArmoire/AlbumdesScripts.ascx" tagname="AlbumdesScripts" tagprefix="VirtuAlbum" %>
<%@ Register src="../ControlesWebUser/Outils/EditionExtraction.ascx" tagname="EditionExtraction" tagprefix="Virtualia" %>
<%@ Register src="../ControlesWebUser/Saisies/VMessage.ascx" tagname="VMessage" tagprefix="Virtualia" %>

<%@ Register src="../ControlesWebUser/Saisies/VisualiserTout.ascx" tagname="VisualiserTout" tagprefix="Virtualia" %>
<%@ Register src="../ControlesWebUser/Syntheses/InfosPersonnelles.ascx" tagname="InfosPersonnelles" tagprefix="Virtualia" %>
<%@ Register src="../ControlesWebUser/ObjetsFenetre/Personnel/PER_ETATCIVIL.ascx" tagname="PER_ETATCIVIL" tagprefix="Virtualia" %>
<%@ Register src="../ControlesWebUser/ObjetsFenetre/Personnel/PER_PIECE_IDENTITE.ascx" tagname="PER_PIECE_IDENTITE" tagprefix="Virtualia" %>
<%@ Register src="../ControlesWebUser/ObjetsFenetre/Personnel/PER_DOMICILE.ascx" tagname="PER_DOMICILE" tagprefix="Virtualia" %>
<%@ Register src="../ControlesWebUser/ObjetsFenetre/Personnel/PER_DOMICILE_SECOND.ascx" tagname="PER_DOMICILE_SECOND" tagprefix="Virtualia" %>
<%@ Register src="../ControlesWebUser/ObjetsFenetre/Personnel/PER_DOMICILE_CONGE.ascx" tagname="PER_DOMICILE_CONGE" tagprefix="Virtualia" %>
<%@ Register src="../ControlesWebUser/ObjetsFenetre/Personnel/PER_HORSFRANCE.ascx" tagname="PER_HORSFRANCE" tagprefix="Virtualia" %>
<%@ Register src="../ControlesWebUser/ObjetsFenetre/Personnel/PER_ENFANT.ascx" tagname="PER_ENFANT" tagprefix="Virtualia" %>
<%@ Register src="../ControlesWebUser/ObjetsFenetre/Personnel/PER_CONJOINT.ascx" tagname="PER_CONJOINT" tagprefix="Virtualia" %>
<%@ Register src="../ControlesWebUser/ObjetsFenetre/Personnel/PER_APREVENIR.ascx" tagname="PER_APREVENIR" tagprefix="Virtualia" %>
<%@ Register src="../ControlesWebUser/ObjetsFenetre/Personnel/PER_BANQUE.ascx" tagname="PER_BANQUE" tagprefix="Virtualia" %>
<%@ Register src="../ControlesWebUser/ObjetsFenetre/Personnel/PER_ANNOTATION.ascx" tagname="PER_ANNOTATION" tagprefix="Virtualia" %>
<%@ Register src="../ControlesWebUser/ObjetsFenetre/Personnel/PER_NOTEAGENDA.ascx" tagname="PER_NOTEAGENDA" tagprefix="Virtualia" %>
<%@ Register src="../ControlesWebUser/ObjetsFenetre/Personnel/PER_REFEXTERNE.ascx" tagname="PER_REFEXTERNE" tagprefix="Virtualia" %>
<%@ Register src="../ControlesWebUser/ObjetsFenetre/Personnel/PER_DECORATION.ascx" tagname="PER_DECORATION" tagprefix="Virtualia" %>
<%@ Register src="../ControlesWebUser/ObjetsFenetre/Personnel/PER_DOCUMENTS.ascx" tagname="PER_DOCUMENTS" tagprefix="Virtualia" %>

<asp:Content ID="CadreGeneral" runat="server" ContentPlaceHolderID="CorpsMaster">
    <asp:UpdatePanel ID="UpdatePanelSaisie" runat="server">
        <ContentTemplate>
          <asp:Table ID="TableOnglets" runat="server" Height="35px" Width="1150px" HorizontalAlign="Center"
                     style="margin-top : 0px" >
             <asp:TableRow>
                <asp:TableCell>
                    <asp:Table ID="CadreOnglets" runat="server" Height="35px" Width="1080px" HorizontalAlign="Left"
                           CellPadding="0" CellSpacing="0" BackImageUrl="~/Images/Onglets/Onglet_2bords_120.bmp"
                           style="margin-top: 10px; border-top-color: #124545;
                            border-bottom-color: #B0E0D7; border-bottom-style: groove" >
                    <asp:TableRow>
                        <asp:TableCell HorizontalAlign ="Center" VerticalAlign="Bottom" Width="118px">
                            <asp:Button ID="BoutonN1" runat="server" Height="24px" Width="105px" 
                                ForeColor="White" Text="Fiche" Font-Bold="true" Font-Italic="true"
                                Font-Names="Trebuchet MS" BackColor="#7D9F99" BorderStyle="None" 
                                style="padding-left: 1px; margin-left: 1px; margin-bottom: 2px; text-align: center" />  
                        </asp:TableCell>
                        <asp:TableCell HorizontalAlign ="Center" VerticalAlign="Bottom" Width="118px">
                            <asp:Button ID="BoutonN2" runat="server" Height="33px" Width="118px" 
                                ForeColor="#142425" Text="Etat civil" Font-Bold="false" Font-Italic="true"
                                Font-Names="Trebuchet MS" BackColor="Transparent" BorderStyle="None" 
                                style="padding-left: 1px; margin-left: 1px; margin-bottom: 2px; text-align: center" />  
                        </asp:TableCell>
                        <asp:TableCell HorizontalAlign ="Center" VerticalAlign="Bottom" Width="118px">
                            <asp:Button ID="BoutonN3" runat="server" Height="33px" Width="118px" 
                                ForeColor="#142425" Text="Adresses" Font-Bold="false" Font-Italic="true"
                                Font-Names="Trebuchet MS" BackColor="Transparent" BorderStyle="None"
                                style="padding-left: 1px; margin-left: 1px; margin-bottom: 2px; text-align: center" />  
                        </asp:TableCell>
                        <asp:TableCell HorizontalAlign ="Center" VerticalAlign="Bottom" Width="118px">
                            <asp:Button ID="BoutonN4" runat="server" Height="33px" Width="118px" 
                                ForeColor="#142425" Text="Situation familale" Font-Bold="false" Font-Italic="true"
                                Font-Names="Trebuchet MS" BackColor="Transparent" BorderStyle="None" 
                                style="padding-left: 1px; margin-left: 1px; margin-bottom: 2px; text-align: center" />  
                        </asp:TableCell>
                        <asp:TableCell HorizontalAlign ="Center" VerticalAlign="Bottom" Width="118px">
                            <asp:Button ID="BoutonN5" runat="server" Height="33px" Width="118px" 
                                ForeColor="#142425" Text="RIB" Font-Bold="false" Font-Italic="true"
                                Font-Names="Trebuchet MS" BackColor="Transparent" BorderStyle="None" 
                                style="padding-left: 1px; margin-left: 1px; margin-bottom: 2px; text-align: center" />  
                        </asp:TableCell>
                        <asp:TableCell HorizontalAlign ="Center" VerticalAlign="Bottom" Width="118px">
                            <asp:Button ID="BoutonN6" runat="server" Height="33px" Width="118px" 
                                ForeColor="#142425" Text="Liens" Font-Bold="false" Font-Italic="true"
                                Font-Names="Trebuchet MS" BackColor="Transparent" BorderStyle="None" 
                                style="padding-left: 1px; margin-left: 1px; margin-bottom: 2px; text-align: center; overflow: auto" />  
                        </asp:TableCell>
                        <asp:TableCell HorizontalAlign ="Center" VerticalAlign="Bottom" Width="118px">
                            <asp:Button ID="BoutonN7" runat="server" Height="33px" Width="118px" 
                                ForeColor="#142425" Text="agenda et post-it" Font-Bold="false" Font-Italic="true"
                                Font-Names="Trebuchet MS" BackColor="Transparent" BorderStyle="None" 
                                style="padding-left: 1px; margin-left: 1px; margin-bottom: 2px; text-align: center;" />  
                        </asp:TableCell>
                        <asp:TableCell HorizontalAlign ="Center" VerticalAlign="Bottom" Width="118px">
                            <asp:Button ID="BoutonN8" runat="server" Height="33px" Width="118px" 
                                ForeColor="#142425" Text="Décorations" Font-Bold="false" Font-Italic="true"
                                Font-Names="Trebuchet MS" BackColor="Transparent" BorderStyle="None" 
                                style="padding-left: 1px; margin-left: 1px; margin-bottom: 2px; text-align: center" />  
                        </asp:TableCell>
                        <asp:TableCell HorizontalAlign ="Center" VerticalAlign="Bottom" Width="118px">
                            <asp:Button ID="BoutonN9" runat="server" Height="33px" Width="118px" 
                                ForeColor="#142425" Text="Tout le dossier" Font-Bold="false" Font-Italic="true"
                                Font-Names="Trebuchet MS" BackColor="Transparent" BorderStyle="None" 
                                style="padding-left: 1px; margin-left: 1px; margin-bottom: 2px; text-align: center" />  
                        </asp:TableCell>
                    </asp:TableRow>
                  </asp:Table>
                </asp:TableCell>
              </asp:TableRow>
            </asp:Table>
            <asp:Table ID="CadreSaisie" runat="server" Height="1220px" Width="1150px" HorizontalAlign="Center"
                     BackImageUrl="~/Images/Fonds/Fond_Saisie.jpg" CellSpacing="2"
                     style="margin-top: 0px " >
               <asp:TableRow>
                   <asp:TableCell ID="ConteneurVues" Height="1220px" Width="820px" VerticalAlign="Top" HorizontalAlign="Center">
                       <asp:MultiView ID="MultiOnglets" runat="server" ActiveViewIndex="0">
                          <asp:View ID="VueSynthese" runat="server"> 
                              <Virtualia:InfosPersonnelles ID="Synthese" runat="server" />
                          </asp:View>  
                          <asp:View ID="VueEtatCivil" runat="server">  
                             <Virtualia:PER_ETATCIVIL ID="PER_ETATCIVIL_1" runat="server"
                               CadreStyle="margin-top: 10px" CadreExpertStyle="margin-top: 10px"  />  
                             <Virtualia:PER_PIECE_IDENTITE ID="PER_PIECE_IDENTITE_31" runat="server"
                               CadreStyle="margin-top: 20px" />  
                           </asp:View>  
                          <asp:View ID="VueAdresse" runat="server">
                             <Virtualia:PER_DOMICILE ID="PER_DOMICILE_4" runat="server" 
                              CadreStyle="margin-top: 10px" />   
                             <Virtualia:PER_DOMICILE_SECOND ID="PER_DOMICILE_SECOND_72" runat="server" 
                              CadreStyle="margin-top: 10px" />
                             <Virtualia:PER_DOMICILE_CONGE ID="PER_DOMICILE_CONGE_73" runat="server" 
                              CadreStyle="margin-top: 10px" /> 
                             <Virtualia:PER_HORSFRANCE ID="PER_HORSFRANCE_44" runat="server" 
                              CadreStyle="margin-top: 10px" />                 
                          </asp:View>
                          <asp:View ID="VueSitFam" runat="server">  
                              <Virtualia:PER_ENFANT ID="PER_ENFANT_3" runat="server" 
                              CadreStyle="margin-top: 10px" /> 
                              <Virtualia:PER_CONJOINT ID="PER_CONJOINT_2" runat="server" 
                              CadreStyle="margin-top: 10px" />
                              <Virtualia:PER_APREVENIR ID="PER_APREVENIR_30" runat="server" 
                              CadreStyle="margin-top: 10px" /> 
                          </asp:View> 
                          <asp:View ID="VueRIB" runat="server">  
                              <Virtualia:PER_BANQUE ID="PER_BANQUE_5" runat="server" 
                              CadreStyle="margin-top: 10px" /> 
                          </asp:View>
                          <asp:View ID="VueLiens" runat="server">  
                              <Virtualia:PER_REFEXTERNE ID="PER_REFEXTERNE_23" runat="server" 
                              CadreStyle="margin-top: 10px" />
                              <Virtualia:PER_DOCUMENTS ID="PER_DOCUMENTS_51" runat="server" 
                              CadreStyle="margin-top: 10px" />
                          </asp:View>      
                          <asp:View ID="VueAnnotation" runat="server">
                              <Virtualia:PER_NOTEAGENDA ID="PER_NOTEAGENDA_28" runat="server" 
                              CadreStyle="margin-top: 10px" />  
                              <Virtualia:PER_ANNOTATION ID="PER_ANNOTATION_20" runat="server" 
                              CadreStyle="margin-top: 10px" />
                          </asp:View>
                          <asp:View ID="VueDecoration" runat="server">  
                              <Virtualia:PER_DECORATION ID="PER_DECORATION_36" runat="server" 
                              CadreStyle="margin-top: 10px" />
                          </asp:View> 
                          <asp:View ID="VueTout" runat="server">  
                              <Virtualia:VisualiserTout ID="Visutout" runat="server" />
                          </asp:View>       
                          <asp:View ID="VueSysRef" runat="server">
                             <VirtuRef:ListeReferences ID="Referentiel" runat="server"
                                SiListeMenuVisible="false" />
                          </asp:View>
                          <asp:View ID="VueAlbum" runat="server">
                             <VirtuAlbum:AlbumdesScripts ID="AlbumRequetes" runat="server"
                                V_PointdeVue="1" />
                          </asp:View>
                          <asp:View ID="VueEdition" runat="server">
                             <Virtualia:EditionExtraction ID="EditionArmoire" runat="server" />
                          </asp:View>
                          <asp:View ID="VueMessage" runat="server">
                             <Virtualia:VMessage ID="MsgVirtualia" runat="server" />
                          </asp:View>
                      </asp:MultiView>
                  </asp:TableCell>
                  <asp:TableCell Height="1200px" Width="330px" VerticalAlign="Top" >
                     <VirtuArm:ArmoirePersonnes ID="ArmoireCourante" runat="server" />
                  </asp:TableCell>
              </asp:TableRow>
              <asp:TableRow>
                <asp:TableCell>
                    <asp:HiddenField ID="HSelIde" runat="server" Value="0" />
                </asp:TableCell>
              </asp:TableRow>
            </asp:Table>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
