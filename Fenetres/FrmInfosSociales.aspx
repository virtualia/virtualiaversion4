﻿<%@ Page Language="VB" MasterPageFile="~/PagesMaitre/VirtualiaMain.master" AutoEventWireup="false" 
    Inherits="Virtualia.Net.Fenetres_FrmInfosSociales" Codebehind="FrmInfosSociales.aspx.vb"
    MaintainScrollPositionOnPostback="True" %>

<%@ MasterType VirtualPath="~/PagesMaitre/VirtualiaMain.master"  %>

<%@ Register src="../ControlesWebUser/ObjetsArmoire/ArmoirePersonnes.ascx" tagname="ArmoirePersonnes" tagprefix="VirtuArm" %>
<%@ Register src="../ControlesWebUser/ObjetsArmoire/ListeReferences.ascx" tagname="ListeReferences" tagprefix="VirtuRef" %>
<%@ Register src="../ControlesWebUser/ObjetsArmoire/AlbumdesScripts.ascx" tagname="AlbumdesScripts" tagprefix="VirtuAlbum" %>
<%@ Register src="../ControlesWebUser/Outils/EditionExtraction.ascx" tagname="EditionExtraction" tagprefix="Virtualia" %>
<%@ Register src="../ControlesWebUser/Saisies/VMessage.ascx" tagname="VMessage" tagprefix="Virtualia" %>

<%@ Register src="../ControlesWebUser/ObjetsFenetre/Personnel/PER_AFFILIATION.ascx" tagname="PER_AFFILIATION" tagprefix="Virtualia" %>
<%@ Register src="../ControlesWebUser/ObjetsFenetre/Personnel/PER_MUTUELLE.ascx" tagname="PER_MUTUELLE" tagprefix="Virtualia" %>
<%@ Register src="../ControlesWebUser/ObjetsFenetre/Personnel/PER_VISITE_MEDICA.ascx" tagname="PER_VISITE_MEDICA" tagprefix="Virtualia" %>
<%@ Register src="../ControlesWebUser/ObjetsFenetre/Personnel/PER_VACCINATION.ascx" tagname="PER_VACCINATION" tagprefix="Virtualia" %>
<%@ Register src="../ControlesWebUser/ObjetsFenetre/Personnel/PER_HANDICAP.ascx" tagname="PER_HANDICAP" tagprefix="Virtualia" %>
<%@ Register src="../ControlesWebUser/ObjetsFenetre/Personnel/PER_PREVENTION_RISQUE.ascx" tagname="PER_PREVENTION_RISQUE" tagprefix="Virtualia" %>
<%@ Register src="../ControlesWebUser/ObjetsFenetre/Personnel/PER_PRET.ascx" tagname="PER_PRET" tagprefix="Virtualia" %>

<asp:Content ID="CadreGeneral" runat="server" ContentPlaceHolderID="CorpsMaster">
    <asp:UpdatePanel ID="UpdatePanelSaisie" runat="server">
        <ContentTemplate>
          <asp:Table ID="TableOnglets" runat="server" Height="35px" Width="1150px" HorizontalAlign="Center"
                     style="margin-top: 0px" >
             <asp:TableRow>
                <asp:TableCell>
                    <asp:Table ID="CadreOnglets" runat="server" Height="35px" Width="360px" HorizontalAlign="Left"
                           CellPadding="0" CellSpacing="0" BackImageUrl="~/Images/Onglets/Onglet_2bords_120.bmp"
                           style="margin-top: 10px; border-top-color: #124545;
                            border-bottom-color: #B0E0D7; border-bottom-style: groove" >
                    <asp:TableRow>
                        <asp:TableCell HorizontalAlign ="Center" VerticalAlign="Bottom" Width="118px">
                            <asp:Button ID="BoutonN1" runat="server" Height="24px" Width="105px" 
                                ForeColor="White" Text="Affiliations" Font-Bold="true" Font-Italic="true"
                                Font-Names="Trebuchet MS" BackColor="#7D9F99" BorderStyle="None" 
                                style="padding-left: 1px; margin-left: 1px; margin-bottom: 2px; text-align: center" />  
                        </asp:TableCell>
                        <asp:TableCell HorizontalAlign ="Center" VerticalAlign="Bottom" Width="118px">
                            <asp:Button ID="BoutonN2" runat="server" Height="33px" Width="118px" 
                                ForeColor="#142425" Text="Médico-social" Font-Bold="false" Font-Italic="true"
                                Font-Names="Trebuchet MS" BackColor="Transparent" BorderStyle="None" 
                                style="padding-left: 1px; margin-left: 1px; margin-bottom: 2px; text-align: center" />  
                        </asp:TableCell>
                        <asp:TableCell HorizontalAlign ="Center" VerticalAlign="Bottom" Width="118px">
                            <asp:Button ID="BoutonN3" runat="server" Height="33px" Width="118px" 
                                ForeColor="#142425" Text="Prêts" Font-Bold="false" Font-Italic="true"
                                Font-Names="Trebuchet MS" BackColor="Transparent" BorderStyle="None" 
                                style="padding-left: 1px; margin-left: 1px; margin-bottom: 2px; text-align: center" />  
                        </asp:TableCell>
                    </asp:TableRow>
                  </asp:Table>
                </asp:TableCell>
              </asp:TableRow>
            </asp:Table>
            <asp:Table ID="CadreSaisie" runat="server" Height="1220px" Width="1150px" HorizontalAlign="Center"
                     BackImageUrl="~/Images/Fonds/Fond_Saisie.jpg" CellSpacing="2"
                     style="margin-top: 0px " >
               <asp:TableRow>
                   <asp:TableCell ID="ConteneurVues" Height="1220px" Width="820px" VerticalAlign="Top" HorizontalAlign="Center">
                       <asp:MultiView ID="MultiOnglets" runat="server" ActiveViewIndex="0">
                          <asp:View ID="VueAffiliation" runat="server">  
                            <Virtualia:PER_AFFILIATION ID="PER_AFFILIATION_27" runat="server" 
                              CadreStyle="margin-top: 10px" /> 
                            <Virtualia:PER_MUTUELLE ID="PER_MUTUELLE_59" runat="server" 
                              CadreStyle="margin-top: 10px" />    
                          </asp:View>  
                          <asp:View ID="VueMedical" runat="server">
                             <Virtualia:PER_VISITE_MEDICA ID="PER_VISITE_MEDICA_32" runat="server" 
                              CadreStyle="margin-top: 10px" />   
                             <Virtualia:PER_VACCINATION ID="PER_VACCINATION_58" runat="server" 
                              CadreStyle="margin-top: 10px" />   
                              <Virtualia:PER_PREVENTION_RISQUE ID="PER_PREVENTION_RISQUE_76" runat="server" 
                              CadreStyle="margin-top: 10px" />          
                          </asp:View>
                          <asp:View ID="VuePret" runat="server">  
                              <Virtualia:PER_PRET ID="PER_PRET_82" runat="server" 
                              CadreStyle="margin-top: 10px" /> 
                          </asp:View>
                          <asp:View ID="VueSysRef" runat="server">
                             <VirtuRef:ListeReferences ID="Referentiel" runat="server"
                                SiListeMenuVisible="false" />
                          </asp:View>
                          <asp:View ID="VueAlbum" runat="server">
                             <VirtuAlbum:AlbumdesScripts ID="AlbumRequetes" runat="server"
                                V_PointdeVue="1" />
                          </asp:View>
                          <asp:View ID="VueEdition" runat="server">
                             <Virtualia:EditionExtraction ID="EditionArmoire" runat="server" />
                          </asp:View>
                          <asp:View ID="VueMessage" runat="server">
                             <Virtualia:VMessage ID="MsgVirtualia" runat="server" />
                          </asp:View>
                      </asp:MultiView>
                  </asp:TableCell>
                  <asp:TableCell Height="1200px" Width="330px" VerticalAlign="Top" >
                     <VirtuArm:ArmoirePersonnes ID="ArmoireCourante" runat="server" />
                  </asp:TableCell>
              </asp:TableRow>
              <asp:TableRow>
                <asp:TableCell>
                    <asp:HiddenField ID="HSelIde" runat="server" Value="0" />
                </asp:TableCell>
              </asp:TableRow>
            </asp:Table>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
