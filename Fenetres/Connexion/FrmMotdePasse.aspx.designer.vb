﻿'------------------------------------------------------------------------------
' <généré automatiquement>
'     Ce code a été généré par un outil.
'
'     Les modifications apportées à ce fichier peuvent provoquer un comportement incorrect et seront perdues si
'     le code est régénéré.
' </généré automatiquement>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class FrmMotdePasse

    '''<summary>
    '''Contrôle UpdatePanelSaisie.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents UpdatePanelSaisie As Global.System.Web.UI.UpdatePanel

    '''<summary>
    '''Contrôle CadreSaisie.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CadreSaisie As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle Titre.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Titre As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle VEMail.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VEMail As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle EtiEMailPro.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiEMailPro As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle EMailPro.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EMailPro As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''Contrôle EtiConfirmeMail.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiConfirmeMail As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle EMailConfirme.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EMailConfirme As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''Contrôle EtiOubli.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiOubli As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle VPassword.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VPassword As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle EtiOldPassword.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiOldPassword As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle OldPassword.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents OldPassword As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''Contrôle EtiNewPassword.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiNewPassword As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle NewPassword.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents NewPassword As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''Contrôle EtiNewConfirmation.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiNewConfirmation As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle NewConfirmation.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents NewConfirmation As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''Contrôle CadreCmdOK.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CadreCmdOK As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle CommandeOK.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CommandeOK As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Contrôle EtiMessage.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiMessage As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle LienRetour.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LienRetour As Global.System.Web.UI.WebControls.HyperLink

    '''<summary>
    '''Propriété Master.
    '''</summary>
    '''<remarks>
    '''Propriété générée automatiquement.
    '''</remarks>
    Public Shadows ReadOnly Property Master() As Virtualia.Net.VirtualiaCnx
        Get
            Return CType(MyBase.Master, Virtualia.Net.VirtualiaCnx)
        End Get
    End Property
End Class
