﻿<%@ Page Language="vb" MasterPageFile="~/PagesMaitre/VirtualiaCnx.master" AutoEventWireup="false" CodeBehind="FrmConnexion.aspx.vb" 
   Inherits="Virtualia.Net.FrmConnexion" UICulture="fr" %>

<%@ MasterType VirtualPath="~/PagesMaitre/VirtualiaCnx.master"  %>

<asp:Content ID="CadreConnexion" runat="server" ContentPlaceHolderID="CorpsMaster">
  <asp:UpdatePanel ID="UpdatePanelSaisie" runat="server">
    <ContentTemplate> 
            <asp:Table ID="CadreSaisie" runat="server" Height="500px" Width="1150px" HorizontalAlign="Center"
                              BackColor="#124545"
                             style="margin-top: 0px; margin-bottom: 0px; border-width: 4px; border-style: ridge;
                             border-color: #B0E0D7" >
                <asp:TableRow Height="50px">
                    <asp:TableCell>
                        <div style="margin-top: 10px; background-color: transparent; text-align: center; font-family:Times New Roman; 
                                font-style: oblique; color: White; width: 1040px; font-size:small; font-weight:normal">
                            <h1>
                                Bienvenue dans Virtualia.Net
                            </h1>
                            <h1>
                                Module ETP et dépenses de personnel
                            </h1>
                        </div>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow Height="50px">
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="Titre" runat="server" Text="SE CONNECTER" Height="25px" Width="700px"
                            BackColor="Transparent" ForeColor="White"
                            Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Large"
                            style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                            font-style: normal; text-indent: 5px; text-align: center;" >
                        </asp:Label>     
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow Height="30px">
                     <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="Videntification" runat="server" CellPadding="1" CellSpacing="0" 
                          HorizontalAlign="Left">
                           <asp:TableRow>
                              <asp:TableCell>
                                <asp:Label ID="EtiNom" runat="server" Height="20px" Width="170px" Text="Nom"
                                        BackColor="#B0E0D7" BorderColor="#B0E0D7"  BorderStyle="Outset"
                                        BorderWidth="2px" ForeColor="#142425" Font-Italic="true"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                        style="margin-top: 0px; margin-left:150px; text-indent: 5px; text-align: left" >
                                </asp:Label>
                            </asp:TableCell>
                            <asp:TableCell>
                                <asp:TextBox ID="Nom" runat="server" Height="16px" Width="300px" MaxLength="35"
                                        BackColor="White" BorderColor="#B0E0D7"  BorderStyle="Inset" TabIndex="1"
                                        BorderWidth="2px" ForeColor="Black" Visible="true" AutoPostBack="false"
                                        Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Small" Font-Italic="false"
                                        style="margin-left: 3px; margin-top: 1px; text-indent: 1px; text-align: left" TextMode="SingleLine"> 
                                </asp:TextBox>
                            </asp:TableCell>
                          </asp:TableRow>
                          <asp:TableRow>
                              <asp:TableCell>
                                <asp:Label ID="EtiPrenom" runat="server" Height="20px" Width="170px" Text="Prénom"
                                        BackColor="#B0E0D7" BorderColor="#B0E0D7"  BorderStyle="Outset"
                                        BorderWidth="2px" ForeColor="#142425" Font-Italic="true" Visible="false"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                        style="margin-top: 0px; margin-left:150px; text-indent: 5px; text-align: left" >
                                </asp:Label>
                            </asp:TableCell>
                            <asp:TableCell>
                                <asp:TextBox ID="Prenom" runat="server" Height="16px" Width="300px" MaxLength="35"
                                        BackColor="White" BorderColor="#B0E0D7"  BorderStyle="Inset" TabIndex="1"
                                        BorderWidth="2px" ForeColor="Black" Visible="false" AutoPostBack="false"
                                        Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Small" Font-Italic="false"
                                        style="margin-left: 3px; margin-top: 1px; text-indent: 1px; text-align: left" TextMode="SingleLine"> 
                                </asp:TextBox>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow Height="40px">
                    <asp:TableCell HorizontalAlign="Center">
                       <asp:Table ID="VPassword" runat="server" CellPadding="1" CellSpacing="0"
                           HorizontalAlign="Left">
                           <asp:TableRow>
                              <asp:TableCell>
                                <asp:Label ID="EtiPassword" runat="server" Height="20px" Width="170px" Text="Mot de passe"
                                        BackColor="#B0E0D7" BorderColor="#B0E0D7"  BorderStyle="Outset"
                                        BorderWidth="2px" ForeColor="#142425" Font-Italic="true"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                        style="margin-top: 0px; margin-left:150px; text-indent: 5px; text-align: left" >
                                </asp:Label>
                            </asp:TableCell>
                            <asp:TableCell>
                                <asp:TextBox ID="Password" runat="server" Height="16px" Width="150px" MaxLength="16"
                                        BackColor="White" BorderColor="#B0E0D7"  BorderStyle="Inset" TabIndex="2"
                                        BorderWidth="2px" ForeColor="Black" Visible="true" AutoPostBack="false"
                                        Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Small" Font-Italic="false"
                                        style="margin-left: 3px; margin-top: 1px; text-indent: 1px; text-align: left" TextMode="Password"> 
                                </asp:TextBox>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow Height="50px">
                    <asp:TableCell HorizontalAlign="Center" VerticalAlign="Middle">
                        <asp:Table ID="CadreCmdOK" runat="server" BackImageUrl="~/Images/Boutons/OK_Std.bmp" Width="70px"
                                    BorderWidth="2px" BorderStyle="Outset" BorderColor="#B0E0D7" ForeColor="#D7FAF3"
                                    CellPadding="0" CellSpacing="0" Visible="true" >
                            <asp:TableRow>
                                <asp:TableCell>
                                    <asp:Button ID="CommandeOK" runat="server" Text="Valider" Width="65px" Height="20px"
                                            BackColor="Transparent" BorderColor="#B0E0D7" ForeColor="#D7FAF3" TabIndex="3" 
                                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Font-Italic="true"
                                            BorderStyle="None" style=" margin-left: 6px; text-align: right;">
                                        </asp:Button>
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>    
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow Height="30px">
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="EtiMessage" runat="server" Text="" Height="25px" Width="700px"
                            BackColor="Transparent" ForeColor="#D8484F"
                            Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Large"
                            style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                            font-style: normal; text-indent: 5px; text-align: center;" >
                        </asp:Label>     
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:CheckBox ID="CocheCookieIde" runat="server" Text="Se souvenir de moi" Visible="true" AutoPostBack="true"
                          BackColor="Transparent" BorderColor="#B0E0D7" BorderStyle="None" BorderWidth="2px"
                          ForeColor="#B0E0D7" Height="20px" Width="500px" Checked="false" 
                          ToolTip="Virtualia retient votre nom et votre prénom afin d'éviter une ressaisie"
                          Font-Bold="true" Font-Names="Trebuchet MS" Font-Size="Small"
                          style="margin-top: 0px; margin-left: 10px; font-style: oblique;
                          text-indent: 5px; text-align: left" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:CheckBox ID="CocheCookiePw" runat="server" Text="Se souvenir également de mon mot de passe" 
                          Visible="true" AutoPostBack="true"
                          BackColor="Transparent" BorderColor="#B0E0D7" BorderStyle="None" BorderWidth="2px"
                          ForeColor="#B0E0D7" Height="20px" Width="500px" Checked="false"
                          ToolTip="Virtualia retient votre mot de passe afin d'éviter une ressaisie. Ceci est déconseillé si 
                          vous n'êtes pas le seul utiliser cet ordinateur."
                          Font-Bold="true" Font-Names="Trebuchet MS" Font-Size="Small"
                          style="margin-top: 0px; margin-left: 10px; font-style: oblique;
                          text-indent: 5px; text-align: left" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:HyperLink ID="LienModifierPw" runat="server" Text="Changer de mot de passe"
                            BackColor="Transparent" BorderStyle="None" Width="500px"
                            Font-Names="Trebuchet MS" Font-Size="Small" ForeColor="White" Font-Bold="true"
                            Font-Underline="true" Font-Italic="true" Tooltip="" Visible="true"
                            style="margin-left: 10px; margin-top: 10px" /> 
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:HyperLink ID="LienOubliPw" runat="server" Text="Mot de passe oublié"
                            BackColor="Transparent" BorderStyle="None" Width="500px"
                            Font-Names="Trebuchet MS" Font-Size="Small" ForeColor="White" Font-Bold="true"
                            Font-Underline="true" Font-Italic="true" Tooltip="" Visible="true"
                            style="margin-left: 10px; margin-top: 10px" /> 
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow Height="30px">
                    <asp:TableCell>

                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
      </ContentTemplate>
   </asp:UpdatePanel>     
</asp:Content>