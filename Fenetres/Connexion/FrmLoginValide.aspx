﻿<%@ Page Language="vb" MasterPageFile="~/PagesMaitre/VirtualiaCnx.master" AutoEventWireup="false" CodeBehind="FrmLoginValide.aspx.vb" 
     Inherits="Virtualia.Net.FrmLoginValide" UICulture="fr" %>

<%@ MasterType VirtualPath="~/PagesMaitre/VirtualiaCnx.master" %>

<asp:Content ID="CadreValidation" runat="server" ContentPlaceHolderID="CorpsMaster">
    <asp:UpdatePanel ID="UpdatePanelValide" runat="server">
        <ContentTemplate>
            <asp:Timer ID="HorlogeLogin" runat="server" Interval="1000">
            </asp:Timer>
            <asp:Table ID="CadreAttente" runat="server" Height="600px" Width="1150px" HorizontalAlign="Center"
                                BackColor="#124545" style="margin-top: 0px; margin-bottom: 0px; border-width: 4px; border-style: ridge;
                                border-color: #B0E0D7" >
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center" ForeColor="White" Height="60px">
                        <asp:UpdateProgress ID="UpdateAttente" runat="server">
                                <ProgressTemplate>
                                    <img alt="" src="../../Images/General/Loading.gif" 
                                    style="vertical-align:top; text-align:center; font-family: 'Trebuchet MS';" 
                                    height="30px" /> 
                                    Chargement de l'application - Traitement en cours ... 
                                </ProgressTemplate>
                        </asp:UpdateProgress>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="250px" HorizontalAlign="Center" VerticalAlign="Top">
                        <div style="margin-top: 10px; background-color: transparent; text-align: center; 
                                font-style: oblique; color: White; width: 800px;">
                            <h1>
                                Connexion autorisée
                            </h1>
                            <h1>
                                veuillez patienter ...
                            </h1>
                        </div>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:HiddenField ID="HCompteur" runat="server" Value="0" />
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </ContentTemplate>
    </asp:UpdatePanel>     
</asp:Content>  