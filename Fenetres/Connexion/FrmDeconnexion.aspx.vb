﻿Public Class FrmDeconnexion
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim ChaineSession As String = Server.HtmlDecode(Request.QueryString("IDVirtualia"))
        If ChaineSession <> "" Then
            Dim WebFct As New Virtualia.Net.WebFonctions(Me, 1)
            WebFct.PointeurGlobal.Deconnexion = ChaineSession
        End If
    End Sub

End Class