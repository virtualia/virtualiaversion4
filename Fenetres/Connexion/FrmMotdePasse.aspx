﻿<%@ Page Language="vb" MasterPageFile="~/PagesMaitre/VirtualiaCnx.master" AutoEventWireup="false" CodeBehind="FrmMotdePasse.aspx.vb" 
    Inherits="Virtualia.Net.FrmMotdePasse" UICulture="fr" %>

<%@ MasterType VirtualPath="~/PagesMaitre/VirtualiaCnx.master"  %>

<asp:Content ID="CadreConnexion" runat="server" ContentPlaceHolderID="CorpsMaster">
  <asp:UpdatePanel ID="UpdatePanelSaisie" runat="server">
    <ContentTemplate> 
            <asp:Table ID="CadreSaisie" runat="server" Height="500px" Width="850px" HorizontalAlign="Center"
                              BackColor="#124545"
                             style="margin-top: 0px; margin-bottom: 0px; border-width: 4px; border-style: ridge;
                             border-color: #B0E0D7" >
                <asp:TableRow Height="50px">
                    <asp:TableCell>
                        <div style="margin-top: 10px; background-color: transparent; text-align: center; 
                                font-style: oblique; color: White; width: 840px;">
                            <h1>
                                Virtualia.Net
                            </h1>
                            <h1>
                                Gestion des mots de passe
                            </h1>
                        </div>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow Height="50px">
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="Titre" runat="server" Text="" Height="25px" Width="700px"
                            BackColor="Transparent" ForeColor="White"
                            Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Large"
                            style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                            font-style: normal; text-indent: 5px; text-align: center;" >
                        </asp:Label>     
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Table ID="VEMail" runat="server" CellPadding="1" CellSpacing="0" HorizontalAlign="Left">
                            <asp:TableRow>
                                <asp:TableCell>
                                    <asp:Label ID="EtiEMailPro" runat="server" Height="20px" Width="250px"
                                         BackColor="#B0E0D7" BorderColor="#B0E0D7"  BorderStyle="Outset"
                                        BorderWidth="2px" ForeColor="#142425" Font-Italic="true" Text="Adresse email professionnelle"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                        style="margin-top: 0px; margin-left:100px; text-indent: 5px; text-align: left" >
                                    </asp:Label>     
                                </asp:TableCell>
                                <asp:TableCell>
                                    <asp:TextBox ID="EMailPro" runat="server" Height="16px" Width="400px" MaxLength="250"
                                            BackColor="White" BorderColor="#B0E0D7"  BorderStyle="Inset" TabIndex="2"
                                            BorderWidth="2px" ForeColor="Black" Visible="true" AutoPostBack="false"
                                            Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Small" Font-Italic="false"
                                            style="margin-left: 3px; margin-top: 1px; text-indent: 1px; text-align: left" TextMode="SingleLine"> 
                                    </asp:TextBox>
                                </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                                <asp:TableCell>
                                    <asp:Label ID="EtiConfirmeMail" runat="server" Height="20px" Width="250px"
                                         BackColor="#B0E0D7" BorderColor="#B0E0D7"  BorderStyle="Outset"
                                        BorderWidth="2px" ForeColor="#142425" Font-Italic="true" Text="Confirmez l'adresse email"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                        style="margin-top: 0px; margin-left:100px; text-indent: 5px; text-align: left" >
                                    </asp:Label>     
                                </asp:TableCell>
                                <asp:TableCell>
                                    <asp:TextBox ID="EMailConfirme" runat="server" Height="16px" Width="400px" MaxLength="250"
                                            BackColor="White" BorderColor="#B0E0D7"  BorderStyle="Inset" TabIndex="2"
                                            BorderWidth="2px" ForeColor="Black" Visible="true" AutoPostBack="false"
                                            Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Small" Font-Italic="false"
                                            style="margin-left: 3px; margin-top: 1px; text-indent: 1px; text-align: left" TextMode="SingleLine"> 
                                    </asp:TextBox>
                                </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                                <asp:TableCell ColumnSpan="2">
                                    <asp:Label ID="EtiOubli" runat="server" Height="40px" Width="650px"
                                         BackColor="Transparent" BorderColor="#B0E0D7"  BorderStyle="None"
                                        BorderWidth="2px" ForeColor="White" Font-Italic="true" Text="En confirmant l'opération, 
                                        vous recevrez dans votre messagerie un nouveau mot de passe permettant d'accéder à Virtualia.net"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                        style="margin-top: 0px; margin-left:100px; text-indent: 5px; text-align: left" >
                                    </asp:Label>
                                </asp:TableCell>
                           </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                       <asp:Table ID="VPassword" runat="server" CellPadding="1" CellSpacing="0" HorizontalAlign="Left">
                           <asp:TableRow>
                              <asp:TableCell>
                                <asp:Label ID="EtiOldPassword" runat="server" Height="20px" Width="250px" Text="Ancien mot de passe"
                                        BackColor="#B0E0D7" BorderColor="#B0E0D7"  BorderStyle="Outset"
                                        BorderWidth="2px" ForeColor="#142425" Font-Italic="true"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                        style="margin-top: 0px; margin-left:100px; text-indent: 5px; text-align: left" >
                                </asp:Label>
                            </asp:TableCell>
                            <asp:TableCell HorizontalAlign="Left">
                                <asp:TextBox ID="OldPassword" runat="server" Height="16px" Width="150px" MaxLength="16"
                                        BackColor="White" BorderColor="#B0E0D7"  BorderStyle="Inset" TabIndex="2"
                                        BorderWidth="2px" ForeColor="Black" Visible="true" AutoPostBack="false"
                                        Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Small" Font-Italic="false"
                                        style="margin-left: 3px; margin-top: 1px; text-indent: 1px; text-align: left" TextMode="Password"> 
                                </asp:TextBox>
                            </asp:TableCell>
                          </asp:TableRow>
                          <asp:TableRow>
                              <asp:TableCell>
                                <asp:Label ID="EtiNewPassword" runat="server" Height="20px" Width="250px" Text="Nouveau mot de passe"
                                        BackColor="#B0E0D7" BorderColor="#B0E0D7"  BorderStyle="Outset"
                                        BorderWidth="2px" ForeColor="#142425" Font-Italic="true"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                        style="margin-top: 0px; margin-left:100px; text-indent: 5px; text-align: left" >
                                </asp:Label>
                            </asp:TableCell>
                            <asp:TableCell HorizontalAlign="Left">
                                <asp:TextBox ID="NewPassword" runat="server" Height="16px" Width="150px" MaxLength="16"
                                        BackColor="White" BorderColor="#B0E0D7"  BorderStyle="Inset" TabIndex="2"
                                        BorderWidth="2px" ForeColor="Black" Visible="true" AutoPostBack="false"
                                        Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Small" Font-Italic="false"
                                        style="margin-left: 3px; margin-top: 1px; text-indent: 1px; text-align: left" TextMode="Password"> 
                                </asp:TextBox>
                            </asp:TableCell>
                          </asp:TableRow>
                          <asp:TableRow>
                              <asp:TableCell>
                                <asp:Label ID="EtiNewConfirmation" runat="server" Height="20px" Width="250px" Text="Confirmation du nouveau mot de passe"
                                        BackColor="#B0E0D7" BorderColor="#B0E0D7"  BorderStyle="Outset"
                                        BorderWidth="2px" ForeColor="#142425" Font-Italic="true"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                        style="margin-top: 0px; margin-left:100px; text-indent: 5px; text-align: left" >
                                </asp:Label>
                            </asp:TableCell>
                            <asp:TableCell HorizontalAlign="Left">
                                <asp:TextBox ID="NewConfirmation" runat="server" Height="16px" Width="150px" MaxLength="16"
                                        BackColor="White" BorderColor="#B0E0D7"  BorderStyle="Inset" TabIndex="2"
                                        BorderWidth="2px" ForeColor="Black" Visible="true" AutoPostBack="false"
                                        Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Small" Font-Italic="false"
                                        style="margin-left: 3px; margin-top: 1px; text-indent: 1px; text-align: left" TextMode="Password"> 
                                </asp:TextBox>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow Height="50px">
                    <asp:TableCell HorizontalAlign="Center" VerticalAlign="Middle">
                        <asp:Table ID="CadreCmdOK" runat="server" BackImageUrl="~/Images/Boutons/OK_Std.bmp" Width="70px"
                                    BorderWidth="2px" BorderStyle="Outset" BorderColor="#B0E0D7" ForeColor="#D7FAF3"
                                    CellPadding="0" CellSpacing="0" Visible="true" >
                            <asp:TableRow>
                                <asp:TableCell>
                                    <asp:Button ID="CommandeOK" runat="server" Text="Valider" Width="65px" Height="20px"
                                            BackColor="Transparent" BorderColor="#B0E0D7" ForeColor="#D7FAF3" TabIndex="3" 
                                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Font-Italic="true"
                                            BorderStyle="None" style=" margin-left: 6px; text-align: right;">
                                        </asp:Button>
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>    
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow Height="30px">
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="EtiMessage" runat="server" Text="" Height="25px" Width="700px"
                            BackColor="Transparent" ForeColor="#D8484F"
                            Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Large"
                            style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                            font-style: normal; text-indent: 5px; text-align: center;" >
                        </asp:Label>     
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:HyperLink ID="LienRetour" runat="server" Text="Revenir à l'authentification"
                            BackColor="Transparent" BorderStyle="None" Width="500px" NavigateUrl="../Connexion/FrmConnexion.aspx"
                            Font-Names="Trebuchet MS" Font-Size="Small" ForeColor="White" Font-Bold="true"
                            Font-Underline="true" Font-Italic="true" Tooltip="" Visible="true"
                            style="margin-left: 10px; margin-top: 10px" /> 
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow Height="60px">
                    <asp:TableCell>

                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
      </ContentTemplate>
   </asp:UpdatePanel>     
</asp:Content>