﻿Option Strict On
Option Explicit On
Option Compare Text
Imports VI = Virtualia.Systeme.Constantes
Public Class FrmConnexion
    Inherits System.Web.UI.Page
    Private WebFct As Virtualia.Net.WebFonctions
    Private WsRhFonction As Virtualia.Systeme.Fonctions.Generales
    Private WsNomStateUti As String = "Utilisateur"
    Private WsCacheUti As ArrayList

    Private Sub EcrireCookie()
        If Me.ViewState(WsNomStateUti) Is Nothing Then
            Exit Sub
        End If
        If WebFct Is Nothing Then
            WebFct = New Virtualia.Net.WebFonctions(Me, 1)
        End If
        Dim Chaine As New System.Text.StringBuilder
        Dim IndiceI As Integer
        Dim ChaineAccentuee As String

        WsCacheUti = CType(Me.ViewState(WsNomStateUti), ArrayList)
        If WsCacheUti(3).ToString = "1" Then
            For IndiceI = 0 To WsCacheUti.Count - 1
                Select Case IndiceI
                    Case 2
                        If WsCacheUti(4).ToString = "1" Then
                            Chaine.Append(WsCacheUti(IndiceI).ToString & VI.Tild)
                        Else
                            Chaine.Append(VI.Tild)
                        End If
                    Case Else
                        Chaine.Append(WsCacheUti(IndiceI).ToString & VI.Tild)
                End Select
            Next IndiceI
        Else
            Chaine.Append(VI.Tild & VI.Tild & VI.Tild & "0" & VI.Tild & "0" & VI.Tild)
        End If
        ChaineAccentuee = WebFct.EncodageCookie(Chaine.ToString)
        If Request.Cookies("MyVirtualia") Is Nothing Then
            Response.Cookies.Set(New HttpCookie("MyVirtualia", ChaineAccentuee))
        Else
            Response.Cookies("MyVirtualia").Value = ChaineAccentuee
        End If
        Response.Cookies("MyVirtualia").Expires = DateTime.Now.AddYears(30)
    End Sub

    Private Sub LireCookie()
        If Request.Cookies("MyVirtualia") Is Nothing Then
            Exit Sub
        End If
        If WebFct Is Nothing Then
            WebFct = New Virtualia.Net.WebFonctions(Me, 1)
        End If
        Dim Chaine As String = WebFct.DecodageCookie(Request.Cookies("MyVirtualia").Value)
        Dim TableauData(0) As String
        Dim IndiceI As Integer

        TableauData = Strings.Split(Chaine, VI.Tild, -1)
        WsCacheUti = New ArrayList
        For IndiceI = 0 To TableauData.Count - 1
            WsCacheUti.Add(TableauData(IndiceI))
            If WsCacheUti.Count > 4 Then
                Exit For
            End If
        Next IndiceI
        Select Case WebFct.PointeurGlobal.VirModeConnexxion
            Case Is = "V4"
                WsCacheUti(1) = ""
        End Select
        Me.ViewState.Add(WsNomStateUti, WsCacheUti)

    End Sub

    Private Sub FrmConnexion_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        WebFct = New Virtualia.Net.WebFonctions(Me, 1)
        WsRhFonction = New Virtualia.Systeme.Fonctions.Generales
    End Sub

    Private Sub FrmConnexion_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        If Session.Count = 0 Then
            Response.Redirect("~/Default.aspx")
        End If
        Dim SiPossible As Boolean = False

        If Me.ViewState(WsNomStateUti) IsNot Nothing Then
            WsCacheUti = CType(Me.ViewState(WsNomStateUti), ArrayList)
        Else
            Call LireCookie()
        End If
        If WsCacheUti Is Nothing Then
            Exit Sub
        End If
        If WsRhFonction Is Nothing Then
            WsRhFonction = New Virtualia.Systeme.Fonctions.Generales
        End If
        Nom.Text = WsCacheUti(0).ToString
        Select Case WebFct.PointeurGlobal.VirModeConnexxion
            Case Is = "V4"
                Prenom.Text = ""
                Prenom.Visible = False
                EtiPrenom.Visible = False
            Case Is = "VI"
                Prenom.Text = WsCacheUti(1).ToString
                Prenom.Visible = True
                EtiPrenom.Visible = True
        End Select
        If WsCacheUti(2).ToString <> "" Then
            Try
                Password.Text = WsRhFonction.DecryptageVirtualia(WsCacheUti(2).ToString)
            Catch ex As Exception
                Password.Text = ""
            End Try
        End If
        Select Case WsCacheUti(3).ToString
            Case Is = "0"
                CocheCookieIde.Checked = False
            Case Is = "1"
                CocheCookieIde.Checked = True
        End Select
        Select Case WsCacheUti(4).ToString
            Case Is = "0"
                CocheCookiePw.Checked = False
            Case Is = "1"
                CocheCookiePw.Checked = True
        End Select

        Select Case WebFct.PointeurGlobal.VirModeConnexxion
            Case Is = "V4"
                If WsCacheUti(0).ToString <> "" Then
                    SiPossible = True
                End If
            Case Is = "VI"
                If WsCacheUti(0).ToString <> "" And WsCacheUti(1).ToString <> "" Then
                    SiPossible = True
                End If
        End Select
        If SiPossible = True Then
            If WebFct Is Nothing Then
                WebFct = New Virtualia.Net.WebFonctions(Me, 1)
            End If
            Dim CnxTmp As New Virtualia.Net.Session.LoginConnexion(WebFct.PointeurGlobal)
            CnxTmp.IDSessionIIS(WsCacheUti(0).ToString, WsCacheUti(1).ToString) = Session.SessionID
            WebFct.PointeurGlobal.ConnexionTemporaire(Session.SessionID) = CnxTmp
            LienModifierPw.NavigateUrl = "../Connexion/FrmMotdePasse.aspx?IDVirtualia=" & Server.HtmlEncode(Session.SessionID) & "&Mode=1"
            LienOubliPw.NavigateUrl = "../Connexion/FrmMotdePasse.aspx?IDVirtualia=" & Server.HtmlEncode(Session.SessionID) & "&Mode=2"
        End If
    End Sub

    Protected Sub Nom_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Nom.TextChanged
        If WsRhFonction Is Nothing Then
            WsRhFonction = New Virtualia.Systeme.Fonctions.Generales
        End If
        If Me.ViewState(WsNomStateUti) IsNot Nothing Then
            WsCacheUti = CType(Me.ViewState(WsNomStateUti), ArrayList)
            WsCacheUti(0) = WsRhFonction.Lettre1Capi(Nom.Text, 2)
            Me.ViewState.Remove(WsNomStateUti)
        Else
            WsCacheUti = New ArrayList
            WsCacheUti.Add(WsRhFonction.Lettre1Capi(Nom.Text, 2))
            WsCacheUti.Add("")
            WsCacheUti.Add("")
            WsCacheUti.Add("0")
            WsCacheUti.Add("0")
        End If
        Me.ViewState.Add(WsNomStateUti, WsCacheUti)
        EtiMessage.Text = ""
        Prenom.Focus()
    End Sub

    Private Sub Prenom_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Prenom.TextChanged
        If WsRhFonction Is Nothing Then
            WsRhFonction = New Virtualia.Systeme.Fonctions.Generales
        End If
        If Me.ViewState(WsNomStateUti) IsNot Nothing Then
            WsCacheUti = CType(Me.ViewState(WsNomStateUti), ArrayList)
            WsCacheUti(1) = WsRhFonction.Lettre1Capi(Prenom.Text, 1)
            Me.ViewState.Remove(WsNomStateUti)
        Else
            WsCacheUti = New ArrayList
            WsCacheUti.Add("")
            WsCacheUti.Add(WsRhFonction.Lettre1Capi(Prenom.Text, 1))
            WsCacheUti.Add("")
            WsCacheUti.Add("0")
            WsCacheUti.Add("0")
        End If
        Me.ViewState.Add(WsNomStateUti, WsCacheUti)
        EtiMessage.Text = ""
        Password.Focus()
    End Sub

    Protected Sub Password_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Password.TextChanged
        If WsRhFonction Is Nothing Then
            WsRhFonction = New Virtualia.Systeme.Fonctions.Generales
        End If
        If Me.ViewState(WsNomStateUti) IsNot Nothing Then
            WsCacheUti = CType(Me.ViewState(WsNomStateUti), ArrayList)
            WsCacheUti(2) = WsRhFonction.CryptageVirtualia(Password.Text)
            Me.ViewState.Remove(WsNomStateUti)
        Else
            WsCacheUti = New ArrayList
            WsCacheUti.Add("")
            WsCacheUti.Add("")
            WsCacheUti.Add(WsRhFonction.CryptageVirtualia(Password.Text))
            WsCacheUti.Add("0")
            WsCacheUti.Add("0")
        End If
        Me.ViewState.Add(WsNomStateUti, WsCacheUti)
        EtiMessage.Text = ""
        CommandeOK.Focus()
    End Sub

    Protected Sub CommandeOK_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles CommandeOK.Click
        If Me.ViewState(WsNomStateUti) Is Nothing Then
            Exit Sub
        End If
        If WsRhFonction Is Nothing Then
            WsRhFonction = New Virtualia.Systeme.Fonctions.Generales
        End If
        If WebFct Is Nothing Then
            WebFct = New Virtualia.Net.WebFonctions(Me, 1)
        End If
        Dim Msg As String = ""
        WsCacheUti = CType(Me.ViewState(WsNomStateUti), ArrayList)
        Select Case WebFct.PointeurGlobal.VirModeConnexxion
            Case Is = "V4"
                If WsCacheUti(0).ToString = "" Or WsCacheUti(2).ToString = "" Then
                    Msg = "Le nom et le mot de passe sont obligatoires"
                    EtiMessage.Text = Msg
                    Exit Sub
                End If
            Case Is = "VI"
                If WsCacheUti(0).ToString = "" Or WsCacheUti(1).ToString = "" Or WsCacheUti(2).ToString = "" Then
                    Msg = "Le nom, le prénom et le mot de passe sont obligatoires"
                    EtiMessage.Text = Msg
                    Exit Sub
                End If
        End Select
        Dim ObjetCnx As Virtualia.Net.Session.LoginConnexion
        Dim LigneMsg As String = ""
        Dim AppObjetGlobal As Virtualia.Net.Session.ObjetGlobal = WebFct.PointeurGlobal
        Dim SiOK As Boolean = False

        ObjetCnx = New Virtualia.Net.Session.LoginConnexion(AppObjetGlobal)
        Select WebFct.PointeurGlobal.VirModeConnexxion
            Case Is = "V4"
                Dim PW As String = WsRhFonction.HashSHA(WsRhFonction.DecryptageVirtualia(WsCacheUti(2).ToString))
                SiOK = ObjetCnx.Si_V4_ConnexionAutorisee(WsCacheUti(0).ToString, _
                            PW, Session.Item("Machine").ToString, _
                            Session.Item("IP").ToString, 0)
            Case Is = "VI"
                SiOK = ObjetCnx.Si_VI_ConnexionAutorisee(WsCacheUti(0).ToString, WsCacheUti(1).ToString, _
                                                     WsRhFonction.DecryptageVirtualia(WsCacheUti(2).ToString), "Virtualia")
        End Select

        If SiOK = False Then
            Msg = "Connexion refusée - Identification invalide"
            LigneMsg = Msg & "Utilisateur : " & WsCacheUti(0).ToString & " Machine : " & Session.Item("Machine").ToString & " IP : " & Session.Item("IP").ToString
            Call AppObjetGlobal.EcrireLogTraitement("Connexion", True, LigneMsg)
            EtiMessage.Text = Msg
            Exit Sub
        End If
        Call EcrireCookie()
        LigneMsg = "Connexion de : " & ObjetCnx.Identifiant & " Machine : " & Session.Item("Machine").ToString & " IP : " & Session.Item("IP").ToString
        Call AppObjetGlobal.EcrireLogTraitement("Connexion", True, LigneMsg)
        '********************************
        ObjetCnx.IDSessionIIS = Session.SessionID
        AppObjetGlobal.Connexion(Session.SessionID) = ObjetCnx

        Call PurgerTelechargement(ObjetCnx.Identifiant)

        Response.Redirect("~/Fenetres/Connexion/FrmLoginValide.aspx?IDVirtualia=" & Server.HtmlEncode(ObjetCnx.IDSessionIIS))
    End Sub

    Private Sub CocheCookieIde_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles CocheCookieIde.CheckedChanged
        If Me.ViewState(WsNomStateUti) Is Nothing Then
            Exit Sub
        End If
        WsCacheUti = CType(Me.ViewState(WsNomStateUti), ArrayList)
        Me.ViewState.Remove(WsNomStateUti)
        If CType(sender, System.Web.UI.WebControls.CheckBox).Checked = True Then
            WsCacheUti(3) = "1"
        Else
            WsCacheUti(3) = "0"
        End If
        Me.ViewState.Add(WsNomStateUti, WsCacheUti)
        Call EcrireCookie()
    End Sub

    Private Sub CocheCookiePw_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles CocheCookiePw.CheckedChanged
        If Me.ViewState(WsNomStateUti) Is Nothing Then
            Exit Sub
        End If
        WsCacheUti = CType(Me.ViewState(WsNomStateUti), ArrayList)
        Me.ViewState.Remove(WsNomStateUti)
        If CType(sender, System.Web.UI.WebControls.CheckBox).Checked = True Then
            WsCacheUti(4) = "1"
        Else
            WsCacheUti(4) = "0"
        End If
        Me.ViewState.Add(WsNomStateUti, WsCacheUti)
        Call EcrireCookie()
    End Sub

    Private Sub PurgerTelechargement(ByVal Ide As Integer)
        Dim ChemindeBase As String
        Dim NomRepertoire As String

        ChemindeBase = Request.PhysicalApplicationPath
        If Strings.Right(ChemindeBase, 1) <> "\" Then
            ChemindeBase &= "\"
        End If
        NomRepertoire = ChemindeBase & "Telechargement\" & Ide.ToString
        If My.Computer.FileSystem.DirectoryExists(NomRepertoire) = True Then
            Try
                My.Computer.FileSystem.DeleteDirectory(NomRepertoire, FileIO.DeleteDirectoryOption.DeleteAllContents)
            Catch ex As Exception
                Exit Try
            End Try
        End If

    End Sub

End Class
