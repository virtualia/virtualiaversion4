﻿<%@ Page Title="Qui sommes-nous" Language="VB" MasterPageFile="~/PagesMaitre/VirtualiaCnx.Master" AutoEventWireup="false" 
    CodeBehind="FrmAbout.aspx.vb" Inherits="Virtualia.Net.FrmAbout" UICulture="fr" %>

<%@ MasterType VirtualPath="~/PagesMaitre/VirtualiaCnx.master"  %>

<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="EnteteSite">
</asp:Content>
<asp:Content ID="CadreConnexion" runat="server" ContentPlaceHolderID="CorpsMaster">
        <asp:Table ID="CadreIfos" runat="server" Height="500px" Width="1150px" HorizontalAlign="Center"
                        BackColor="#124545" Font-Names="Trebuchet MS" Font-Size="Small"
                        style="margin-top: 0px; margin-bottom: 0px; border-width: 4px; border-style: ridge;
                        border-color: #B0E0D7" >
        <asp:TableRow Height="50px">
            <asp:TableCell>
                <div style="margin-top: 10px; background-color: transparent; text-align: center; 
                                font-style: oblique; color: White; width: 1140px;">
                    <h1>
                        A PROPOS
                    </h1>
                </div>
                <div style="margin-top: 10px; background-color: transparent; text-align: center; 
                        font-style: oblique; color: White; width: 1040px;">
                    <h1 style="font-size: medium">
                        Nom du produit : Virtualia.Net
                    </h1>
                    <h1 style="font-size: medium">
                        Numéro de version : 4.0
                    </h1>
                    <h1 style="font-size: medium">
                        Société : Virtualia Sarl
                    </h1>
                    <h1 style="font-size: medium">
                        Auteurs : Roger Perrodin et Michel Bridier
                    </h1>
                    <h1 style="font-size: medium">
                        Date de création : Juillet 1992
                    </h1>
                    <h1 style="font-size: medium">
                        Date de la version : Mars 2011
                    </h1>
                </div>
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableCell HorizontalAlign="Center">
                <asp:HyperLink ID="LienSiteOfficiel" runat="server" Text="Site officiel"
                    BackColor="Transparent" BorderStyle="None" Width="400px" NavigateUrl="http://www.virtualia.fr"
                    Font-Names="Trebuchet MS" Font-Size="Small" ForeColor="White" Font-Bold="true"
                    Font-Underline="true" Font-Italic="true" Tooltip="" Visible="true"
                    style="margin-left: 10px; margin-top: 10px" Target="_new" /> 
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow Height="200px">
            <asp:TableCell>
            </asp:TableCell>
        </asp:TableRow>
    </asp:Table>
</asp:Content>