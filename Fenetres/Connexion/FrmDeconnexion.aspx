﻿<%@ Page Language="vb" MasterPageFile="~/PagesMaitre/VirtualiaCnx.master" AutoEventWireup="false" 
   CodeBehind="FrmDeconnexion.aspx.vb" Inherits="Virtualia.Net.FrmDeconnexion" UICulture="fr" %>

<%@ MasterType VirtualPath="~/PagesMaitre/VirtualiaCnx.master"  %>

<asp:Content ID="CadreDeconnexion" runat="server" ContentPlaceHolderID="CorpsMaster">
    <asp:Table ID="CadreAttente" runat="server" Height="500px" Width="1150px" HorizontalAlign="Center"
                        BackColor="#124545" style="margin-top: 0px; margin-bottom: 0px; border-width: 4px; border-style: ridge;
                        border-color: #B0E0D7" >
        <asp:TableRow Height="50px">
            <asp:TableCell VerticalAlign="Top">
                <div style="margin-top: 30px; background-color: transparent; text-align: center; font-family:Times New Roman; 
                                font-style: oblique; color: White; width: 1140px; font-size:small; font-weight:normal">
                    <h1>
                        Virtualia.Net
                    </h1>
                    <h1>
                        Module ETP et dépenses de personnel
                    </h1>
                </div>
            </asp:TableCell>
        </asp:TableRow>
    </asp:Table>
</asp:Content>  