﻿Option Strict On
Option Explicit On
Option Compare Text
Imports VI = Virtualia.Systeme.Constantes
Public Class FrmMotdePasse
    Inherits System.Web.UI.Page
    Private WebFct As Virtualia.Net.WebFonctions
    Private WsRhFonction As Virtualia.Systeme.Fonctions.Generales
    Private WsNomStateUti As String = "Utilisateur"
    Private WsCacheUti As ArrayList
    Private WsNomStateValid As String = "Validation"
    Private WsCacheValid As ArrayList

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        WebFct = New Virtualia.Net.WebFonctions(Me, 1)
        If Me.ViewState(WsNomStateUti) Is Nothing Then
            Dim ChaineSession As String = Server.HtmlDecode(Request.QueryString("IDVirtualia"))
            Dim ObjetCnxTmp As Virtualia.Net.Session.LoginConnexion
            ObjetCnxTmp = WebFct.PointeurGlobal.ConnexionTemporaire(ChaineSession)
            If ObjetCnxTmp Is Nothing Then
                Dim Msg As String = "Erreur lors de l'identification du dossier accédé."
                Response.Redirect("~/Fenetres/ErreurApplication.aspx?Msg=" & Msg)
                Exit Sub
            End If
            WsCacheUti = New ArrayList
            WsCacheUti.Add(ObjetCnxTmp.Nom)
            WsCacheUti.Add(ObjetCnxTmp.Prenom)
            WsCacheUti.Add(Request.QueryString("Mode"))
            WebFct.PointeurGlobal.SupprimerCnxTemporaire(ChaineSession)
        Else
            WsCacheUti = CType(Me.ViewState(WsNomStateUti), ArrayList)
        End If
        Me.ViewState.Add(WsNomStateUti, WsCacheUti)
        Titre.Text = WsCacheUti(0).ToString & Strings.Space(1) & WsCacheUti(1).ToString
        If WsCacheUti(2).ToString = "1" Then
            VPassword.Visible = True
            VEMail.Visible = False
        Else
            VPassword.Visible = False
            VEMail.Visible = True
        End If
    End Sub

    Private Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        If Me.ViewState(WsNomStateValid) IsNot Nothing Then
            WsCacheValid = CType(Me.ViewState(WsNomStateValid), ArrayList)
        End If
        If WsCacheValid Is Nothing Then
            Exit Sub
        End If
        If WsCacheUti(2).ToString = "1" Then
            If WsRhFonction Is Nothing Then
                WsRhFonction = New Virtualia.Systeme.Fonctions.Generales
            End If
            If WsCacheValid(0).ToString <> "" Then
                Try
                    OldPassword.Text = WsRhFonction.DecryptageVirtualia(WsCacheValid(0).ToString)
                Catch ex As Exception
                    OldPassword.Text = ""
                End Try
            End If
            If WsCacheValid(1).ToString <> "" Then
                Try
                    NewPassword.Text = WsRhFonction.DecryptageVirtualia(WsCacheValid(1).ToString)
                Catch ex As Exception
                    NewPassword.Text = ""
                End Try
            End If
            If WsCacheValid(2).ToString <> "" Then
                Try
                    NewConfirmation.Text = WsRhFonction.DecryptageVirtualia(WsCacheValid(2).ToString)
                Catch ex As Exception
                    NewConfirmation.Text = ""
                End Try
            End If
        Else
            If WsCacheValid(0).ToString <> "" Then
                Try
                    EMailPro.Text = WsCacheValid(0).ToString
                Catch ex As Exception
                    EMailPro.Text = ""
                End Try
            End If
            If WsCacheValid(1).ToString <> "" Then
                Try
                    EMailConfirme.Text = WsCacheValid(1).ToString
                Catch ex As Exception
                    EMailConfirme.Text = ""
                End Try
            End If
        End If
    End Sub

    Private Sub EMailPro_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles EMailPro.TextChanged
        If Me.ViewState(WsNomStateValid) IsNot Nothing Then
            WsCacheValid = CType(Me.ViewState(WsNomStateValid), ArrayList)
            WsCacheValid(0) = EMailPro.Text
            Me.ViewState.Remove(WsNomStateValid)
        Else
            WsCacheValid = New ArrayList
            WsCacheValid.Add(EMailPro.Text)
            WsCacheValid.Add("")
        End If
        Me.ViewState.Add(WsNomStateValid, WsCacheValid)
        EtiMessage.Text = ""
        EMailConfirme.Focus()
    End Sub

    Private Sub EMailConfirme_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles EMailConfirme.TextChanged
        If Me.ViewState(WsNomStateValid) IsNot Nothing Then
            WsCacheValid = CType(Me.ViewState(WsNomStateValid), ArrayList)
            WsCacheValid(1) = EMailConfirme.Text
            Me.ViewState.Remove(WsNomStateValid)
        Else
            WsCacheValid = New ArrayList
            WsCacheValid.Add("")
            WsCacheValid.Add(EMailConfirme.Text)
        End If
        Me.ViewState.Add(WsNomStateValid, WsCacheValid)
        EtiMessage.Text = ""
        CommandeOK.Focus()
    End Sub

    Private Sub OldPassword_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles OldPassword.TextChanged
        If WsRhFonction Is Nothing Then
            WsRhFonction = New Virtualia.Systeme.Fonctions.Generales
        End If
        If Me.ViewState(WsNomStateValid) IsNot Nothing Then
            WsCacheValid = CType(Me.ViewState(WsNomStateValid), ArrayList)
            WsCacheValid(0) = WsRhFonction.CryptageVirtualia(OldPassword.Text)
            Me.ViewState.Remove(WsNomStateValid)
        Else
            WsCacheValid = New ArrayList
            WsCacheValid.Add(WsRhFonction.CryptageVirtualia(OldPassword.Text))
            WsCacheValid.Add("")
            WsCacheValid.Add("")
        End If
        Me.ViewState.Add(WsNomStateValid, WsCacheValid)
        EtiMessage.Text = ""
        NewPassword.Focus()
    End Sub

    Private Sub NewPassword_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles NewPassword.TextChanged
        If WsRhFonction Is Nothing Then
            WsRhFonction = New Virtualia.Systeme.Fonctions.Generales
        End If
        If Me.ViewState(WsNomStateValid) IsNot Nothing Then
            WsCacheValid = CType(Me.ViewState(WsNomStateValid), ArrayList)
            WsCacheValid(1) = WsRhFonction.CryptageVirtualia(NewPassword.Text)
            Me.ViewState.Remove(WsNomStateValid)
        Else
            WsCacheValid = New ArrayList
            WsCacheValid.Add("")
            WsCacheValid.Add(WsRhFonction.CryptageVirtualia(NewPassword.Text))
            WsCacheValid.Add("")
        End If
        Me.ViewState.Add(WsNomStateValid, WsCacheValid)
        EtiMessage.Text = ""
        NewConfirmation.Focus()
    End Sub

    Private Sub NewConfirmation_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles NewConfirmation.TextChanged
        If WsRhFonction Is Nothing Then
            WsRhFonction = New Virtualia.Systeme.Fonctions.Generales
        End If
        If Me.ViewState(WsNomStateValid) IsNot Nothing Then
            WsCacheValid = CType(Me.ViewState(WsNomStateValid), ArrayList)
            WsCacheValid(2) = WsRhFonction.CryptageVirtualia(NewConfirmation.Text)
            Me.ViewState.Remove(WsNomStateValid)
        Else
            WsCacheValid = New ArrayList
            WsCacheValid.Add("")
            WsCacheValid.Add("")
            WsCacheValid.Add(WsRhFonction.CryptageVirtualia(NewConfirmation.Text))
        End If
        Me.ViewState.Add(WsNomStateValid, WsCacheValid)
        EtiMessage.Text = ""
        CommandeOK.Focus()
    End Sub

    Private Sub CommandeOK_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles CommandeOK.Click
        If Me.ViewState(WsNomStateUti) Is Nothing Then
            Exit Sub
        End If
        If Me.ViewState(WsNomStateValid) Is Nothing Then
            Exit Sub
        End If
        If WsRhFonction Is Nothing Then
            WsRhFonction = New Virtualia.Systeme.Fonctions.Generales
        End If

        Dim Msg As String = ""
        WsCacheUti = CType(Me.ViewState(WsNomStateUti), ArrayList)
        WsCacheValid = CType(Me.ViewState(WsNomStateValid), ArrayList)
        If WsCacheUti(2).ToString = "1" Then
            If WsCacheValid(0).ToString = "" Or WsCacheValid(1).ToString = "" Or WsCacheValid(2).ToString = "" Then
                Msg = "La saisie des trois champs est obligatoire"
                EtiMessage.Text = Msg
                Exit Sub
            End If
            If WsCacheValid(1).ToString <> WsCacheValid(2).ToString Then
                Msg = "La confirmation du nouveau mot de passe n'est pas correcte"
                EtiMessage.Text = Msg
                Exit Sub
            End If
        Else
            If WsCacheValid(0).ToString <> WsCacheValid(1).ToString Then
                Msg = "La confirmation de l'adresse Email n'est pas correcte"
                EtiMessage.Text = Msg
                Exit Sub
            End If
        End If
        If WebFct Is Nothing Then
            WebFct = New Virtualia.Net.WebFonctions(Me, 1)
        End If

        Dim ObjetCnx As Virtualia.Net.Session.LoginConnexion
        Dim AppObjetGlobal As Virtualia.Net.Session.ObjetGlobal = WebFct.PointeurGlobal
        Dim SiOK As Boolean = False

        ObjetCnx = New Virtualia.Net.Session.LoginConnexion(AppObjetGlobal)
        If WsCacheUti(2).ToString = "1" Then
            Select Case WebFct.PointeurGlobal.VirModeConnexxion
                Case Is = "V4"
                    SiOK = ObjetCnx.Si_V4_ConnexionAutorisee(WsCacheUti(0).ToString, _
                                            WsRhFonction.HashSHA(WsRhFonction.DecryptageVirtualia(WsCacheValid(0).ToString)), _
                                            Request.UserHostName, Request.UserHostAddress, 0)
                Case Is = "VI"
                    SiOK = ObjetCnx.Si_VI_ConnexionAutorisee(WsCacheUti(0).ToString, WsCacheUti(1).ToString, _
                                                 WsRhFonction.DecryptageVirtualia(WsCacheValid(0).ToString), "Virtualia")
            End Select

            If SiOK = False Then
                Msg = "Modification refusée - Ancien mot de passe invalide"
                EtiMessage.Text = Msg
                Exit Sub
            End If
            Select WebFct.PointeurGlobal.VirModeConnexxion
                Case Is = "V4"
                    SiOK = ObjetCnx.Si_V4_ModificationPasswordOK(WsCacheUti(0).ToString, _
                                                                WsRhFonction.HashSHA(WsRhFonction.DecryptageVirtualia(WsCacheValid(1).ToString)))
                Case Is = "VI"
                    SiOK = ObjetCnx.Si_VI_ModificationPasswordOK(ObjetCnx.Identifiant, WsRhFonction.DecryptageVirtualia(WsCacheValid(1).ToString), "Virtualia")
            End Select
            If SiOK = True Then
                Msg = "Modification effectuée."
            Else
                Msg = "La modification n'a pas pu être effectuée."
            End If
            EtiMessage.Text = Msg
            Call VerifierCookie()
            Exit Sub
        Else
            Dim NewPasse As String = ""
            Select WebFct.PointeurGlobal.VirModeConnexxion
                Case Is = "V4"
                    NewPasse = ObjetCnx.V4_NouveauPassword(WsCacheUti(0).ToString, WsCacheValid(0).ToString)
                Case Is = "VI"
                    NewPasse = ObjetCnx.VI_NouveauPassword(WsCacheUti(0).ToString, WsCacheUti(1).ToString, WsCacheValid(0).ToString, "Virtualia")
            End Select
            If NewPasse <> "" Then
                Dim ProxyOutil As New Virtualia.Ressources.WebService.Outils(System.Configuration.ConfigurationManager.AppSettings("WebService.VirtualiaUtils"))
                Dim ContenuMsg As String = "Vous avez demandé un nouveau mot de passe pour vous connecter à l'application Virtualia.Net." & vbCrLf
                ContenuMsg &= "Le mot de passe généré automatiquement est le suivant :   " & NewPasse & Strings.Space(3)

                SiOK = ProxyOutil.EnvoyerEmail("", WsCacheValid(0).ToString, ContenuMsg, False)

                If SiOK = True Then
                    Msg = "la demande a été prise en compte. Un email de confirmation vous a été transmis"
                Else
                    Msg = "la demande n'a pas pu aboutir. "
                End If
            Else
                Msg = "La modification n'a pas pu aboutir."
            End If
            EtiMessage.Text = Msg
        End If
    End Sub

    Private Sub VerifierCookie()
        If Request.Cookies("MyVirtualia") Is Nothing Then
            Exit Sub
        End If
        If Me.ViewState(WsNomStateValid) Is Nothing Then
            Exit Sub
        End If
        WsCacheValid = CType(Me.ViewState(WsNomStateValid), ArrayList)
        Dim Valeurs As String = WebFct.DecodageCookie(Request.Cookies("MyVirtualia").Value)
        Dim Chaine As New System.Text.StringBuilder
        Dim TableauData(0) As String
        Dim IndiceI As Integer

        TableauData = Strings.Split(Valeurs, VI.Tild, -1)
        If TableauData(4) = "1" Then
            TableauData(2) = WsCacheValid(1).ToString
        End If
        For IndiceI = 0 To TableauData.Count - 1
            Chaine.Append(TableauData(IndiceI) & VI.Tild)
        Next IndiceI
        Response.Cookies("MyVirtualia").Value = WebFct.EncodageCookie(Chaine.ToString)
        Response.Cookies("MyVirtualia").Expires = DateTime.Now.AddYears(30)
    End Sub

End Class