﻿Option Compare Text
Public Class FrmLoginValide
    Inherits System.Web.UI.Page
    Private WebFct As Virtualia.Net.WebFonctions

    Private Sub FrmLoginValide_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        WebFct = New Virtualia.Net.WebFonctions(Me, 1)
        Dim Cpt As Integer = CInt(HCompteur.Value)

        If Cpt > 0 Then
            '**Création de l'Objet Utilisateur
            Dim AppObjetGlobal As Virtualia.Net.Session.ObjetGlobal = WebFct.PointeurGlobal
            Dim Cnx As Virtualia.Net.Session.LoginConnexion
            Dim SessionVirtualia As Virtualia.Net.Session.ObjetSession
            Dim IdSession As String = Server.HtmlDecode(Request.QueryString("IDVirtualia"))
            Cnx = AppObjetGlobal.Connexion(IdSession)

            If Cnx Is Nothing Then
                Dim Msg As String = "Erreur lors de l'identification du dossier accédé."
                Response.Redirect("~/Fenetres/ErreurApplication.aspx?Msg=" & Msg)
                Exit Sub
            End If

            SessionVirtualia = WebFct.PointeurGlobal.ItemUtilisateur(Cnx.Nom)
            If SessionVirtualia Is Nothing Then
                SessionVirtualia = WebFct.PointeurGlobal.AjouterUneSession(IdSession, Cnx.Nom, Cnx.Identifiant, _
                                                        Cnx.Filtre_Etablissement, Cnx.ParametreNumerique)
            Else
                SessionVirtualia.V_IDSession = IdSession
            End If
            SessionVirtualia.ID_AdresseIP = Session.Item("Machine").ToString
            SessionVirtualia.ID_LogonIdentite = Session.Item("Identite").ToString
            SessionVirtualia.ID_Machine = Session.Item("IP").ToString

            Response.Redirect("~/Fenetres/FrmInfosPersonnelles.aspx?IDVirtualia=" & Server.HtmlEncode(SessionVirtualia.V_IDSession))
        End If
        Cpt += 1
        HCompteur.Value = Cpt.ToString
    End Sub

End Class