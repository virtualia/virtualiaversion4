﻿<%@ Page Language="VB" MasterPageFile="~/PagesMaitre/VirtualiaMain.master" AutoEventWireup="false" 
    Inherits="Virtualia.Net.Fenetres_FrmPublicRetraite" Codebehind="FrmPublicRetraite.aspx.vb"
    MaintainScrollPositionOnPostback="True" %>
<%@ MasterType VirtualPath="~/PagesMaitre/VirtualiaMain.master"  %>

<%@ Register src="../ControlesWebUser/ObjetsArmoire/ArmoirePersonnes.ascx" tagname="ArmoirePersonnes" tagprefix="VirtuArm" %>
<%@ Register src="../ControlesWebUser/ObjetsArmoire/ListeReferences.ascx" tagname="ListeReferences" tagprefix="VirtuRef" %>
<%@ Register src="../ControlesWebUser/ObjetsArmoire/AlbumdesScripts.ascx" tagname="AlbumdesScripts" tagprefix="VirtuAlbum" %>
<%@ Register src="../ControlesWebUser/Outils/EditionExtraction.ascx" tagname="EditionExtraction" tagprefix="Virtualia" %>
<%@ Register src="../ControlesWebUser/Saisies/VMessage.ascx" tagname="VMessage" tagprefix="Virtualia" %>

<%@ Register src="../ControlesWebUser/ObjetsFenetre/Personnel/PER_RETRAITE.ascx" tagname="PER_RETRAITE" tagprefix="Virtualia" %>
<%@ Register src="../ControlesWebUser/ObjetsFenetre/Personnel/PER_RETRAITE_ENFANTS.ascx" tagname="PER_RETRAITE_ENFANTS" tagprefix="Virtualia" %>
<%@ Register src="../ControlesWebUser/ObjetsFenetre/Personnel/PER_RETRAITE_SERVICES.ascx" tagname="PER_RETRAITE_SERVICES" tagprefix="Virtualia" %>
<%@ Register src="../ControlesWebUser/ObjetsFenetre/Personnel/PER_SERVICE_NATION.ascx" tagname="PER_SERVICE_NATION" tagprefix="Virtualia" %>
<%@ Register src="../ControlesWebUser/ObjetsFenetre/Personnel/PER_RETRAITE_CATEGORIE.ascx" tagname="PER_RETRAITE_CATEGORIE" tagprefix="Virtualia" %>
<%@ Register src="../ControlesWebUser/ObjetsFenetre/Personnel/PER_RETRAITE_BONIFICATION.ascx" tagname="PER_RETRAITE_BONIFICATION" tagprefix="Virtualia" %>
<%@ Register src="../ControlesWebUser/ObjetsFenetre/Personnel/PER_RETRAITE_CONGE_HT.ascx" tagname="PER_RETRAITE_CONGE_HT" tagprefix="Virtualia" %>
<%@ Register src="../ControlesWebUser/ObjetsFenetre/Personnel/PER_BASE_COTISATION.ascx" tagname="PER_BASE_COTISATION" tagprefix="Virtualia" %>

<asp:Content ID="CadreGeneral" runat="server" ContentPlaceHolderID="CorpsMaster">
    <asp:UpdatePanel ID="UpdatePanelSaisie" runat="server">
        <ContentTemplate>
          <asp:Table ID="TableOnglets" runat="server" Height="35px" Width="1150px" HorizontalAlign="Center"
                     style="margin-top: 0px" >
             <asp:TableRow>
                <asp:TableCell>
                    <asp:Table ID="CadreOnglets" runat="server" Height="35px" Width="600px" HorizontalAlign="Left"
                           CellPadding="0" CellSpacing="0" BackImageUrl="~/Images/Onglets/Onglet_2bords_120.bmp"
                           style="margin-top: 10px; border-top-color: #124545;
                            border-bottom-color: #B0E0D7; border-bottom-style: groove" >
                    <asp:TableRow>
                        <asp:TableCell HorizontalAlign ="Center" VerticalAlign="Bottom" Width="118px">
                            <asp:Button ID="BoutonN1" runat="server" Height="24px" Width="105px" 
                                ForeColor="White" Text="Retraite" Font-Bold="true" Font-Italic="true"
                                Font-Names="Trebuchet MS" BackColor="#7D9F99" BorderStyle="None" 
                                style="padding-left: 1px; margin-left: 1px; margin-bottom: 2px; text-align: center" />  
                        </asp:TableCell>
                        <asp:TableCell HorizontalAlign ="Center" VerticalAlign="Bottom" Width="118px">
                            <asp:Button ID="BoutonN2" runat="server" Height="33px" Width="118px" 
                                ForeColor="#142425" Text="Enfants" Font-Bold="false" Font-Italic="true"
                                Font-Names="Trebuchet MS" BackColor="Transparent" BorderStyle="None" 
                                style="padding-left: 1px; margin-left: 1px; margin-bottom: 2px; text-align: center" />  
                        </asp:TableCell>
                        <asp:TableCell HorizontalAlign ="Center" VerticalAlign="Bottom" Width="118px">
                            <asp:Button ID="BoutonN3" runat="server" Height="33px" Width="118px" 
                                ForeColor="#142425" Text="Services" Font-Bold="false" Font-Italic="true"
                                Font-Names="Trebuchet MS" BackColor="Transparent" BorderStyle="None"
                                style="padding-left: 1px; margin-left: 1px; margin-bottom: 2px; text-align: center" />  
                        </asp:TableCell>
                        <asp:TableCell HorizontalAlign ="Center" VerticalAlign="Bottom" Width="118px">
                            <asp:Button ID="BoutonN4" runat="server" Height="33px" Width="118px" 
                                ForeColor="#142425" Text="Bonifications" Font-Bold="false" Font-Italic="true"
                                Font-Names="Trebuchet MS" BackColor="Transparent" BorderStyle="None" 
                                style="padding-left: 1px; margin-left: 1px; margin-bottom: 2px; text-align: center" />  
                        </asp:TableCell>
                        <asp:TableCell HorizontalAlign ="Center" VerticalAlign="Bottom" Width="118px">
                            <asp:Button ID="BoutonN5" runat="server" Height="33px" Width="118px" 
                                ForeColor="#142425" Text="Cotisations" Font-Bold="false" Font-Italic="true"
                                Font-Names="Trebuchet MS" BackColor="Transparent" BorderStyle="None" 
                                style="padding-left: 1px; margin-left: 1px; margin-bottom: 2px; text-align: center" />  
                        </asp:TableCell>
                    </asp:TableRow>
                  </asp:Table>
                </asp:TableCell>
              </asp:TableRow>
            </asp:Table>
            <asp:Table ID="CadreSaisie" runat="server" Height="1220px" Width="1150px" HorizontalAlign="Center"
                     BackImageUrl="~/Images/Fonds/Fond_Saisie.jpg" CellSpacing="2"
                     style="margin-top: 0px " >
               <asp:TableRow>
                   <asp:TableCell ID="ConteneurVues" Height="1220px" Width="820px" VerticalAlign="Top" HorizontalAlign="Center">
                       <asp:MultiView ID="MultiOnglets" runat="server" ActiveViewIndex="0">
                          <asp:View ID="VuePension" runat="server">  
                             <Virtualia:PER_RETRAITE ID="PER_RETRAITE_77" runat="server"
                               CadreStyle="margin-top: 10px" />  
                           </asp:View>  
                          <asp:View ID="VueEnfant" runat="server">
                             <Virtualia:PER_RETRAITE_ENFANTS ID="PER_RETRAITE_ENFANTS_66" runat="server" 
                              CadreStyle="margin-top: 10px" />                 
                          </asp:View>
                          <asp:View ID="VueService" runat="server">  
                              <Virtualia:PER_RETRAITE_SERVICES ID="PER_RETRAITE_SERVICES_99" runat="server" 
                              CadreStyle="margin-top: 10px" /> 
                              <Virtualia:PER_RETRAITE_CATEGORIE ID="PER_RETRAITE_CATEGORIE_97" runat="server" 
                              CadreStyle="margin-top: 10px" /> 
                              <Virtualia:PER_SERVICE_NATION ID="PER_SERVICE_NATION_22" runat="server" 
                              CadreStyle="margin-top: 10px" /> 
                          </asp:View> 
                          <asp:View ID="VueBonification" runat="server">  
                              <Virtualia:PER_RETRAITE_BONIFICATION ID="PER_RETRAITE_BONIFICATION_96" runat="server" 
                              CadreStyle="margin-top: 10px" /> 
                              <Virtualia:PER_RETRAITE_CONGE_HT ID="PER_RETRAITE_CONGE_HT_98" runat="server" 
                              CadreStyle="margin-top: 10px" /> 
                          </asp:View>
                          <asp:View ID="VueCotisation" runat="server">
                             <Virtualia:PER_BASE_COTISATION ID="PER_BASE_COTISATION_85" runat="server" 
                              CadreStyle="margin-top: 10px" />                 
                          </asp:View>
                          <asp:View ID="VueSysRef" runat="server">
                             <VirtuRef:ListeReferences ID="Referentiel" runat="server"
                                SiListeMenuVisible="false" />
                          </asp:View>
                          <asp:View ID="VueAlbum" runat="server">
                             <VirtuAlbum:AlbumdesScripts ID="AlbumRequetes" runat="server"
                                V_PointdeVue="1" />
                          </asp:View>
                          <asp:View ID="VueEdition" runat="server">
                             <Virtualia:EditionExtraction ID="EditionArmoire" runat="server" />
                          </asp:View>
                          <asp:View ID="VueMessage" runat="server">
                             <Virtualia:VMessage ID="MsgVirtualia" runat="server" />
                          </asp:View>
                      </asp:MultiView>
                  </asp:TableCell>
                  <asp:TableCell Height="1200px" Width="330px" VerticalAlign="Top" >
                     <VirtuArm:ArmoirePersonnes ID="ArmoireCourante" runat="server" />
                  </asp:TableCell>
              </asp:TableRow>
              <asp:TableRow>
                <asp:TableCell>
                    <asp:HiddenField ID="HSelIde" runat="server" Value="0" />
                </asp:TableCell>
              </asp:TableRow>
            </asp:Table>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
