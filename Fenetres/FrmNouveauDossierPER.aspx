﻿<%@ Page Language="VB" MasterPageFile="~/PagesMaitre/VirtualiaMain.master" AutoEventWireup="false" 
    Inherits="Virtualia.Net.Fenetres_FrmNouveauDossierPER" Codebehind="FrmNouveauDossierPER.aspx.vb"
    MaintainScrollPositionOnPostback="True" %>
<%@ MasterType VirtualPath="~/PagesMaitre/VirtualiaMain.master"  %>

<%@ Register src="../ControlesWebUser/ObjetsArmoire/ListeReferences.ascx" tagname="ListeReferences" tagprefix="VirtuRef" %>
<%@ Register src="../ControlesWebUser/Saisies/VMessage.ascx" tagname="VMessage" tagprefix="Virtualia" %>

<%@ Register src="../ControlesWebUser/ObjetsFenetre/Personnel/PER_ETATCIVIL.ascx" tagname="PER_ETATCIVIL" tagprefix="Virtualia" %>
<%@ Register src="../ControlesWebUser/ObjetsFenetre/Personnel/PER_REFEXTERNE.ascx" tagname="PER_REFEXTERNE" tagprefix="Virtualia" %>
<%@ Register src="../ControlesWebUser/ObjetsFenetre/Personnel/PER_COLLECTIVITE.ascx" tagname="PER_COLLECTIVITE" tagprefix="Virtualia" %>
<%@ Register src="../ControlesWebUser/ObjetsFenetre/Personnel/PER_AFFECTATION.ascx" tagname="PER_AFFECTATION" tagprefix="Virtualia" %>
<%@ Register src="../ControlesWebUser/ObjetsFenetre/Personnel/PER_SOCIETE.ascx" tagname="PER_SOCIETE" tagprefix="Virtualia" %>

<asp:Content ID="CadreGeneral" runat="server" ContentPlaceHolderID="CorpsMaster">
    <asp:UpdatePanel ID="UpdatePanelSaisie" runat="server">
        <ContentTemplate>
           <asp:Table ID="CadreSaisie" runat="server" Height="1220px" Width="1150px" HorizontalAlign="Center"
                     BackImageUrl="~/Images/Fonds/Fond_Saisie.jpg" CellSpacing="2" style="margin-top: 0px" >
               <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="EtiTitre" runat="server" BorderStyle="Ridge" BorderWidth="3px" 
                            Text="CREATION D'UN NOUVEAU DOSSIER" Font-Names="Trebuchet MS"
                            Font-Bold="true" Font-Size="Large" Width="600px" Height="50px"
                            style="text-align:center; vertical-align: middle" />
                    </asp:TableCell>
               </asp:TableRow>
               <asp:TableRow>
                   <asp:TableCell ID="ConteneurVues" Height="1220px" Width="1100px" VerticalAlign="Top" HorizontalAlign="Center">
                       <asp:MultiView ID="MultiOnglets" runat="server" ActiveViewIndex="0">
                          <asp:View ID="VueEtatCivil" runat="server">  
                             <Virtualia:PER_ETATCIVIL ID="PER_ETATCIVIL_1" runat="server"
                               CadreStyle="margin-top: 30px" CadreExpertVisible="false" />  
                           </asp:View>  
                          <asp:View ID="VueRefExterne" runat="server">
                             <Virtualia:PER_REFEXTERNE ID="PER_REFEXTERNE_23" runat="server" 
                              CadreStyle="margin-top: 30px" />
                          </asp:View>
                          <asp:View ID="VueAffectation" runat="server">
                             <Virtualia:PER_AFFECTATION ID="PER_AFFECTATION_17" runat="server"
                               CadreStyle="margin-top: 30px" CadreExpertVisible="false"  />  
                          </asp:View>
                          <asp:View ID="VueCollectivite" runat="server">  
                             <Virtualia:PER_COLLECTIVITE ID="PER_COLLECTIVITE_26" runat="server"
                               CadreStyle="margin-top: 30px" CadreExpertVisible="false"  />   
                           </asp:View>
                           <asp:View ID="VueEtablissement" runat="server">  
                             <Virtualia:PER_SOCIETE ID="PER_SOCIETE_26" runat="server"
                               CadreStyle="margin-top: 30px" CadreExpertVisible="false"  />  
                           </asp:View>    
                          <asp:View ID="VueSysRef" runat="server">
                             <VirtuRef:ListeReferences ID="Referentiel" runat="server"
                                SiListeMenuVisible="false" />
                          </asp:View>
                          <asp:View ID="VueMessage" runat="server">
                             <Virtualia:VMessage ID="MsgVirtualia" runat="server" />
                          </asp:View>
                      </asp:MultiView>
                  </asp:TableCell>
              </asp:TableRow>
              <asp:TableRow>
                <asp:TableCell>
                    <asp:HiddenField ID="HNewIde" runat="server" Value="0" />
                </asp:TableCell>
              </asp:TableRow>
             </asp:Table>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
