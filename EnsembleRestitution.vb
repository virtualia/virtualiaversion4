﻿Option Strict On
Option Explicit On
Option Compare Text
Imports Microsoft.VisualBasic
Imports VI = Virtualia.Systeme.Constantes
Namespace Script
    Public Class EnsembleRestitution
        Implements IDisposable
        Private disposedValue As Boolean = False        ' Pour détecter les appels redondants
        Private WsNomFichier As String
        Private WsNoPage As Integer 'Pagination des vues par bloc de 200 000 lignes max
        Private WsNoOrdre As Integer
        Private WsSiDernierePage As Boolean
        Private WsListeRestit As List(Of TablesObjet.ShemaVUE.VUE_RESTITUTION)

        Public ReadOnly Property ListeDesLignes() As List(Of TablesObjet.ShemaVUE.VUE_RESTITUTION)
            Get
                Return WsListeRestit
            End Get
        End Property

        Public Property Pagination() As Integer
            Get
                Return WsNoPage
            End Get
            Set(ByVal value As Integer)
                WsNoPage = value
                WsListeRestit.Clear()
            End Set
        End Property

        Public ReadOnly Property NumOrdre() As Integer
            Get
                Return WsNoOrdre
            End Get
        End Property

        Public Property SiDernierePage() As Boolean
            Get
                Return WsSiDernierePage
            End Get
            Set(ByVal value As Boolean)
                WsSiDernierePage = value
            End Set
        End Property

        Public Sub AjouterLigne(ByVal NomTable As String, ByVal Valeur As String)
            If Valeur = "" Then
                Exit Sub
            End If
            WsNoOrdre += 1
            Dim FicheRef As New TablesObjet.ShemaVUE.VUE_RESTITUTION
            FicheRef = New TablesObjet.ShemaVUE.VUE_RESTITUTION
            FicheRef.Ide_Dossier = WsNoOrdre
            FicheRef.Nom_Table = NomTable
            FicheRef.ClauseSql = Valeur
            WsListeRestit.Add(FicheRef)
        End Sub

        Public ReadOnly Property NombredeLignes() As Integer
            Get
                Return WsListeRestit.Count
            End Get
        End Property

        Public ReadOnly Property NomFichier() As String
            Get
                Return WsNomFichier
            End Get
        End Property

        Public Sub New(ByVal NomduFichierLu As String)
            MyBase.New()
            WsNomFichier = NomduFichierLu
            WsNoPage = 0
            WsNoOrdre = 0
            WsSiDernierePage = False
            WsListeRestit = New List(Of TablesObjet.ShemaVUE.VUE_RESTITUTION)
        End Sub

        Public Overloads Sub Dispose() Implements IDisposable.Dispose
            Dispose(True)
            GC.SuppressFinalize(Me)
        End Sub

        Protected Overridable Overloads Sub Dispose(ByVal disposing As Boolean)
            If Not Me.disposedValue Then
                If disposing Then
                    WsListeRestit.Clear()
                End If
            End If
            Me.disposedValue = True
        End Sub
    End Class
End Namespace

