﻿<%@ Control Language="VB" AutoEventWireup="false" Inherits="Virtualia.Net.Controles_VMessage" Codebehind="VMessage.ascx.vb" %>

<asp:Table ID="MessageVirtualia" runat="server" CellPadding="0" CellSpacing="4" 
           BackImageUrl="~/Images/Fonds/Presentation.jpg" Width="730px" Height="550px"
            HorizontalAlign="Center"> 
    <asp:TableRow Height="50px">
        <asp:TableCell Width="420px">
            <asp:HiddenField ID="DatasH" runat="server" Value="" />
        </asp:TableCell> 
        <asp:TableCell Width="310px" ColumnSpan="2">
            <asp:HiddenField ID="ObjetH" runat="server" Value="" />
        </asp:TableCell> 
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell HorizontalAlign="Right" VerticalAlign="Middle" ColumnSpan="3" >
            <asp:Label ID="EtiTitre" runat="server" Height="40px" Width="600px"
                    BorderColor="#B0E0D7" BorderStyle="Outset" BorderWidth="1px"
                    BackColor="Transparent" ForeColor="#FF9849" Font-Italic="true"
                    Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Small"
                    style="margin-right: 40px; vertical-align: middle; text-align: center" >
            </asp:Label>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell HorizontalAlign="Right" ColumnSpan="3">
            <asp:TextBox ID="EtiMsg" runat="server" Height="120px" Width="550px"
                    BorderColor="#B0E0D7" BorderStyle="Outset" BorderWidth="1px" TextMode="MultiLine"
                    BackColor="Transparent" ForeColor="#FF9849" Font-Italic="true" ReadOnly="true"
                    Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Small"
                    style="margin-top: 5px; margin-right: 10px; text-indent: 5px; text-align: left" >
            </asp:TextBox>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell HorizontalAlign="Right">
            <asp:Button ID="CmdOui" runat="server" Text="Oui" Height="26px" Width="110px" 
                        ForeColor="#142425" Font-Italic="true" BackColor="#FFB277" 
                        BorderWidth="2px" BorderStyle="Outset" BorderColor="#FFEBC8"
                        Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Small" TabIndex="1" 
                        Tooltip="Je confirme l'opération" />
        </asp:TableCell>
        <asp:TableCell HorizontalAlign="Center">
            <asp:Button ID="CmdNon" runat="server" Text="Non" Height="26px" Width="110px" 
                        ForeColor="#142425" Font-Italic="true" BackColor="#FFB277" 
                        BorderWidth="2px" BorderStyle="Outset" BorderColor="#FFEBC8"
                        Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Small" TabIndex="2" 
                        Tooltip="Je vais corriger ma demande" />
        </asp:TableCell>
        <asp:TableCell HorizontalAlign="Left">
            <asp:Button ID="CmdCancel" runat="server" Text="Annuler" Height="26px" Width="110px" 
                        ForeColor="#142425" Font-Italic="true" BackColor="#FFB277"
                        BorderWidth="2px" BorderStyle="Outset" BorderColor="#FFEBC8"
                        Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Small" TabIndex="2"
                        Tooltip="J'annule l'opération en cours" />
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell Width="700px" Height="160px" ColumnSpan="3">
            <asp:HiddenField ID="EmetteurH" runat="server" Value="" />
        </asp:TableCell>
     </asp:TableRow>
</asp:Table>
