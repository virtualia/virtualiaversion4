﻿Option Strict On
Option Explicit On
Option Compare Text
Imports VI = Virtualia.Systeme.Constantes
Public Class Controles_VListBoxEtCoches
    Inherits System.Web.UI.UserControl
    Public Delegate Sub Valeur_ChangeEventHandler(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.DonneeChangeEventArgs)
    Public Event ValeurChange As Valeur_ChangeEventHandler
    Private WebFct As Virtualia.Net.WebFonctions
    Private WsNomState As String = "EtatCourant"
    Private WsNomStateSel As String = "VSelCoches"

    Protected Overridable Sub Saisie_Change(ByVal e As Virtualia.Systeme.Evenements.DonneeChangeEventArgs)
        RaiseEvent ValeurChange(Me, e)
    End Sub

    Public Property V_PointdeVue As Short
        Get
            Dim VCache As ArrayList
            If Me.ViewState(WsNomState) IsNot Nothing Then
                VCache = CType(Me.ViewState(WsNomState), ArrayList)
                If IsNumeric(VCache(0)) Then
                    Return CShort(VCache(0))
                End If
            End If
            Return 0
        End Get
        Set(ByVal value As Short)
            Dim VCache As ArrayList
            If Me.ViewState(WsNomState) Is Nothing Then
                VCache = New ArrayList
                VCache.Add(value)
                VCache.Add("")
                VCache.Add("")
            Else
                VCache = CType(Me.ViewState(WsNomState), ArrayList)
                If IsNumeric(VCache(0)) Then
                    If CInt(VCache(0)) = value Then
                        Exit Property
                    End If
                End If
                VCache(0) = value
                Me.ViewState.Remove(WsNomState)
            End If
            If IsNumeric(value) Then
                If value > 2 Then
                    VCache(2) = ListeSysReference(value, "")
                    VCheckListBox.Items.Clear()
                End If
            End If
            Me.ViewState.Add(WsNomState, VCache)
            If value = 0 Then
                VCheckListBox.Items.Clear()
            End If
        End Set
    End Property

    Public Property V_NomTable As String
        Get
            Dim VCache As ArrayList
            If Me.ViewState(WsNomState) IsNot Nothing Then
                VCache = CType(Me.ViewState(WsNomState), ArrayList)
                Return VCache(1).ToString
            End If
            Return ""
        End Get
        Set(ByVal value As String)
            If value = "" Then
                Exit Property
            End If
            Dim VCache As ArrayList
            VCache = CType(Me.ViewState(WsNomState), ArrayList)
            If VCache(1) IsNot Nothing Then
                If VCache(1).ToString = value Then
                    Exit Property
                End If
                Me.ViewState.Remove(WsNomState)
            Else
                VCache.Add(2)
                VCache.Add("")
                VCache.Add("")
            End If
            VCache(2) = ListeSysReference(2, value)
            Me.ViewState.Add(WsNomState, VCache)

            VCheckListBox.Items.Clear()
        End Set
    End Property

    Public ReadOnly Property V_Valeurs As String
        Get
            Dim VCache As ArrayList
            If Me.ViewState(WsNomState) IsNot Nothing Then
                VCache = CType(Me.ViewState(WsNomState), ArrayList)
                Return VCache(2).ToString
            End If
            Return ""
        End Get
    End Property

    Public Property V_ListeCochee As ArrayList
        Get
            If Me.ViewState(WsNomStateSel) Is Nothing Then
                Return Nothing
            End If
            Return CType(Me.ViewState(WsNomStateSel), ArrayList)
        End Get
        Set(ByVal value As ArrayList)
            If Me.ViewState(WsNomStateSel) IsNot Nothing Then
                Me.ViewState.Remove(WsNomStateSel)
            End If
            If value IsNot Nothing Then
                Me.ViewState.Add(WsNomStateSel, value)
            End If
            VCheckListBox.Items.Clear()
        End Set
    End Property

    Public Property EtiText As String
        Get
            Return Etiquette.Text
        End Get
        Set(ByVal value As String)
            Etiquette.Text = value
        End Set
    End Property

    Public Property EtiHeight As System.Web.UI.WebControls.Unit
        Get
            Return Etiquette.Height
        End Get
        Set(ByVal value As System.Web.UI.WebControls.Unit)
            Etiquette.Height = value
        End Set
    End Property

    Public Property EtiWidth As System.Web.UI.WebControls.Unit
        Get
            Return Etiquette.Width
        End Get
        Set(ByVal value As System.Web.UI.WebControls.Unit)
            Etiquette.Width = value
        End Set
    End Property

    Public Property EtiBackColor As System.Drawing.Color
        Get
            Return Etiquette.BackColor
        End Get
        Set(ByVal value As System.Drawing.Color)
            Etiquette.BackColor = value
        End Set
    End Property

    Public Property EtiForeColor As System.Drawing.Color
        Get
            Return Etiquette.ForeColor
        End Get
        Set(ByVal value As System.Drawing.Color)
            Etiquette.ForeColor = value
        End Set
    End Property

    Public Property EtiBorderColor As System.Drawing.Color
        Get
            Return Etiquette.BorderColor
        End Get
        Set(ByVal value As System.Drawing.Color)
            Etiquette.BorderColor = value
        End Set
    End Property

    Public Property EtiBorderWidth As System.Web.UI.WebControls.Unit
        Get
            Return Etiquette.BorderWidth
        End Get
        Set(ByVal value As System.Web.UI.WebControls.Unit)
            Etiquette.BorderWidth = value
        End Set
    End Property

    Public Property EtiBorderStyle As System.Web.UI.WebControls.BorderStyle
        Get
            Return Etiquette.BorderStyle
        End Get
        Set(ByVal value As System.Web.UI.WebControls.BorderStyle)
            Etiquette.BorderStyle = value
        End Set
    End Property

    Public Property EtiTooltip As String
        Get
            Return Etiquette.ToolTip
        End Get
        Set(ByVal value As String)
            Etiquette.ToolTip = value
        End Set
    End Property

    Public Property EtiVisible As Boolean
        Get
            Return Etiquette.Visible
        End Get
        Set(ByVal value As Boolean)
            Etiquette.Visible = value
        End Set
    End Property

    Public WriteOnly Property EtiStyle As String
        Set(ByVal value As String)
            Dim TableauData(0) As String
            Dim TableauW(0) As String
            Dim IndiceI As Integer
            TableauData = Strings.Split(value, ";")
            For IndiceI = 0 To TableauData.Count - 1
                If TableauData(IndiceI) = "" Then
                    Exit For
                End If
                TableauW = Strings.Split(TableauData(IndiceI), ":")
                Etiquette.Style.Remove(Strings.Trim(TableauW(0)))
                Etiquette.Style.Add(Strings.Trim(TableauW(0)), Strings.Trim(TableauW(1)))
            Next IndiceI
        End Set
    End Property

    Public WriteOnly Property LstStyle As String
        Set(ByVal value As String)
            Dim TableauData(0) As String
            Dim TableauW(0) As String
            Dim IndiceI As Integer
            TableauData = Strings.Split(value, ";")
            For IndiceI = 0 To TableauData.Count - 1
                If TableauData(IndiceI) = "" Then
                    Exit For
                End If
                TableauW = Strings.Split(TableauData(IndiceI), ":")
                VCheckListBox.Style.Remove(Strings.Trim(TableauW(0)))
                VCheckListBox.Style.Add(Strings.Trim(TableauW(0)), Strings.Trim(TableauW(1)))
            Next IndiceI
        End Set
    End Property

    Public Property LstText As String
        Get
            Return VCheckListBox.Text
        End Get
        Set(ByVal value As String)
            Try
                VCheckListBox.Text = value
            Catch ex As Exception
                Exit Property
            End Try
        End Set
    End Property

    Public Property LstHeight As System.Web.UI.WebControls.Unit
        Get
            Return VCheckListBox.Height
        End Get
        Set(ByVal value As System.Web.UI.WebControls.Unit)
            VCheckListBox.Height = value
        End Set
    End Property

    Public Property LstWidth As System.Web.UI.WebControls.Unit
        Get
            Return VCheckListBox.Width
        End Get
        Set(ByVal value As System.Web.UI.WebControls.Unit)
            VCheckListBox.Width = value
        End Set
    End Property

    Public Property LstBackColor As System.Drawing.Color
        Get
            Return VCheckListBox.BackColor
        End Get
        Set(ByVal value As System.Drawing.Color)
            VCheckListBox.BackColor = value
        End Set
    End Property

    Public Property LstForeColor As System.Drawing.Color
        Get
            Return VCheckListBox.ForeColor
        End Get
        Set(ByVal value As System.Drawing.Color)
            VCheckListBox.ForeColor = value
        End Set
    End Property

    Public Property LstBorderColor As System.Drawing.Color
        Get
            Return VCheckListBox.BorderColor
        End Get
        Set(ByVal value As System.Drawing.Color)
            VCheckListBox.BorderColor = value
        End Set
    End Property

    Public Property LstBorderWidth As System.Web.UI.WebControls.Unit
        Get
            Return VCheckListBox.BorderWidth
        End Get
        Set(ByVal value As System.Web.UI.WebControls.Unit)
            VCheckListBox.BorderWidth = value
        End Set
    End Property

    Public Property LstBorderStyle As System.Web.UI.WebControls.BorderStyle
        Get
            Return VCheckListBox.BorderStyle
        End Get
        Set(ByVal value As System.Web.UI.WebControls.BorderStyle)
            VCheckListBox.BorderStyle = value
        End Set
    End Property

    Public Property LstVisible As Boolean
        Get
            Return VCheckListBox.Visible
        End Get
        Set(ByVal value As Boolean)
            VCheckListBox.Visible = value
        End Set
    End Property

    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        WebFct = New Virtualia.Net.WebFonctions(Me, 0)
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        If VCheckListBox.Items.Count > 0 Then
            Exit Sub
        End If
        Dim IndiceI As Integer
        Dim TableauData(0) As String

        VCheckListBox.Items.Clear()
        VCheckListBox.Font.Name = WebFct.PoliceCharte

        If Me.ViewState(WsNomState) Is Nothing Then
            Exit Sub
        End If
        If V_Valeurs = "" Then
            Exit Sub
        End If
        TableauData = Strings.Split(V_Valeurs, VI.Tild, -1)
        For IndiceI = 0 To TableauData.Count - 1
            If TableauData(IndiceI) <> "" Then
                VCheckListBox.Items.Add(TableauData(IndiceI))
            End If
        Next IndiceI

        If Me.ViewState(WsNomStateSel) Is Nothing Then
            Exit Sub
        End If
        Dim VCache As ArrayList = CType(Me.ViewState(WsNomStateSel), ArrayList)
        If VCache Is Nothing Then
            Exit Sub
        End If
        Dim SelItem As System.Web.UI.WebControls.ListItem
        For IndiceI = 0 To VCache.Count - 1
            SelItem = VCheckListBox.Items.FindByValue(VCache(IndiceI).ToString)
            If SelItem IsNot Nothing Then
                SelItem.Selected = True
            End If
        Next IndiceI
    End Sub

    Private Function ListeSysReference(ByVal PVue As Short, ByVal NomTable As String) As String
        Dim Chaine As New System.Text.StringBuilder
        Dim TableauObjet(0) As String
        Dim TableauData(0) As String
        Dim IndiceK As Integer

        If WebFct Is Nothing Then
            WebFct = New Virtualia.Net.WebFonctions(Me, 0)
        End If
        If WebFct.PointeurUtilisateur Is Nothing Then
            Return ("")
        End If
        Select Case PVue
            Case Is = 2
                If NomTable <> "" Then
                    TableauObjet = Strings.Split(WebFct.PointeurUtilisateur.LireTableGeneraleSimple(NomTable), VI.SigneBarre, -1)
                End If
            Case Is > 2
                TableauObjet = Strings.Split(WebFct.PointeurUtilisateur.LireTableSysReference(PVue), VI.SigneBarre, -1)
            Case Else
                Return ""
        End Select
        For IndiceK = 0 To TableauObjet.Count - 1
            If TableauObjet(IndiceK) = "" Then
                Exit For
            End If
            TableauData = Strings.Split(TableauObjet(IndiceK), VI.Tild, -1)
            Chaine.Append(TableauData(1) & VI.Tild)
        Next IndiceK
        Return Chaine.ToString
    End Function

    Protected Sub VCheckListBox_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles VCheckListBox.SelectedIndexChanged
        Dim Chaine As New System.Text.StringBuilder
        Dim Valeurs As New ArrayList
        Dim IndiceI As Integer
        For IndiceI = 0 To VCheckListBox.Items.Count - 1
            Select Case VCheckListBox.Items.Item(IndiceI).Selected
                Case True
                    Valeurs.Add(VCheckListBox.Items.Item(IndiceI).Text)
            End Select
        Next IndiceI
        V_ListeCochee = Valeurs

        Dim Evenement As Virtualia.Systeme.Evenements.DonneeChangeEventArgs
        Evenement = New Virtualia.Systeme.Evenements.DonneeChangeEventArgs(Valeurs)
        Saisie_Change(Evenement)
    End Sub


End Class