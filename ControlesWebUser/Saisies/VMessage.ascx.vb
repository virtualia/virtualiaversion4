﻿Option Strict On
Option Explicit On
Option Compare Text
Partial Class Controles_VMessage
    Inherits System.Web.UI.UserControl
    Public Delegate Sub Retour_MsgEventHandler(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.MessageRetourEventArgs)
    Public Event ValeurRetour As Retour_MsgEventHandler

    Protected Overridable Sub ReponseRetour(ByVal e As Virtualia.Systeme.Evenements.MessageRetourEventArgs)
        RaiseEvent ValeurRetour(Me, e)
    End Sub

    Public WriteOnly Property AfficherMessage() As Virtualia.Systeme.Evenements.MessageSaisieEventArgs
        Set(ByVal value As Virtualia.Systeme.Evenements.MessageSaisieEventArgs)
            Dim TableauW(0) As String
            EmetteurH.Value = value.Emetteur
            EtiTitre.Text = value.TitreMessage
            EtiMsg.Text = value.ContenuMessage
            DatasH.Value = value.ChaineDatas
            ObjetH.Value = value.NumeroObjet.ToString
            TableauW = Strings.Split(value.NatureMessage, ";", -1)
            CmdOui.Text = TableauW(0)
            If TableauW.Count = 1 Then
                CmdNon.Visible = False
                CmdCancel.Visible = False
                Exit Property
            End If
            CmdNon.Visible = True
            CmdNon.Text = TableauW(1)
            If TableauW.Count = 2 Then
                CmdCancel.Visible = False
                Exit Property
            End If
            CmdCancel.Visible = True
            CmdCancel.Text = TableauW(2)
        End Set
    End Property

    Protected Sub CmdOui_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles CmdOui.Click, CmdNon.Click, CmdCancel.Click
        If EmetteurH.Value = "" Then
            Exit Sub
        End If
        Dim Reponse As String = CType(sender, System.Web.UI.WebControls.Button).Text
        Dim Evenement As New Virtualia.Systeme.Evenements.MessageRetourEventArgs(EmetteurH.Value, CShort(ObjetH.Value), DatasH.Value, Reponse)
        ReponseRetour(Evenement)
    End Sub

End Class
