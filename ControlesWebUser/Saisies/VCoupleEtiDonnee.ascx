﻿<%@ Control Language="VB" AutoEventWireup="false" Inherits="Virtualia.Net.Controles_VCoupleEtiDonnee" EnableViewState="true" CodeBehind="VCoupleEtiDonnee.ascx.vb" ClassName="Controles_VCoupleEtiDonneeBase" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ToolKit" %>

<asp:Table ID="VStandard" runat="server" CellPadding="1" CellSpacing="0">
    <asp:TableRow>
        <asp:TableCell>
            <asp:Label ID="Etiquette" runat="server" Height="20px" Width="170px" BackColor="#B0E0D7" BorderColor="#B0E0D7" BorderStyle="Outset" BorderWidth="2px" ForeColor="#142425" Font-Italic="true" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Style="margin-top: 0px; margin-left: 4px; text-indent: 5px; text-align: left" />
        </asp:TableCell>
        <asp:TableCell>
            <asp:TextBox ID="Donnee" runat="server" Height="16px" Width="300px" MaxLength="0" BackColor="White" BorderColor="#B0E0D7" BorderStyle="Inset" BorderWidth="2px" ForeColor="Black" Visible="true" AutoPostBack="false" Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Small" Font-Italic="false" Style="margin-top: 1px; text-indent: 1px; text-align: left" TextMode="SingleLine" />
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow ID="RowValidator">
        <asp:TableCell ColumnSpan="2">
            <%--OnServerValidate="Validator_ServerValidate"--%>
            <asp:CustomValidator ID="Validator" Enabled="false" Display="None" Font-Bold="true" ForeColor="Red" Font-Name="Trebuchet MS" Font-Size="10pt" runat="server" />
            <ToolKit:ValidatorCalloutExtender runat="Server" ID="VCEAgeMiss" TargetControlID="Validator" Width="250px" />
        </asp:TableCell>
    </asp:TableRow>
</asp:Table>


