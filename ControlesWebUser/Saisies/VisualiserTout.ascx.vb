﻿Option Strict On
Option Explicit On
Option Compare Text
Imports VI = Virtualia.Systeme.Constantes
Partial Class Controles_VisualiserTout
    Inherits System.Web.UI.UserControl
    Private WebFct As Virtualia.Net.WebFonctions

    Public Property V_Liste() As ArrayList
        Get
            If Me.ViewState("EtatCourant") Is Nothing Then
                Return Nothing
                Exit Property
            End If
            Dim VCache As ArrayList
            VCache = CType(Me.ViewState("EtatCourant"), ArrayList)
            Return VCache
        End Get
        Set(ByVal value As ArrayList)
            If value Is Nothing Then
                Exit Property
            End If
            If Me.ViewState("EtatCourant") IsNot Nothing Then
                Me.ViewState.Remove("EtatCourant")
            End If
            Me.ViewState.Add("EtatCourant", value)

            Call FaireListe(0)
        End Set
    End Property

    Private Sub FaireListe(ByVal NoPage As Integer)
        Dim VCache As ArrayList
        Dim Pvue As Short
        Dim IndiceO As Integer
        Dim NoObjet As Short
        Dim RhObjet As Virtualia.Systeme.MetaModele.Donnees.Objet
        Dim TableauObjet(0) As String
        Dim TableauData(0) As String
        Dim IndiceA As Integer
        Dim IndiceC As Integer
        Dim VClasseVisu As Virtualia.Ressources.Datas.ObjetVisualiserTout
        Dim Datas As ArrayList
        Dim Idebut As Integer
        Dim IposFin As Integer

        If Me.ViewState("EtatCourant") Is Nothing Then
            Exit Sub
        End If
        VCache = CType(Me.ViewState("EtatCourant"), ArrayList)
        Pvue = CShort(VCache(0))
        IndiceO = 0
        Datas = New ArrayList
        Do
            IndiceO += 1
            If IndiceO > VCache.Count - 1 Then
                Exit Do
            End If
            NoObjet = CShort(VCache(IndiceO))
            RhObjet = WebFct.PointeurGlobal.VirObjet(Pvue, NoObjet)
            If RhObjet Is Nothing Then
                Exit Sub
            End If
            Select Case RhObjet.VNature
                Case VI.TypeObjet.ObjetDate, VI.TypeObjet.ObjetDateRang
                    Idebut = 0
                    IposFin = RhObjet.PositionDateFin
                Case Else
                    Idebut = 1
                    IposFin = 0
            End Select

            IndiceO += 1
            TableauObjet = Strings.Split(VCache(IndiceO).ToString, VI.SigneBarre, -1)
            For IndiceA = 0 To TableauObjet.Count - 1
                If TableauObjet(IndiceA) = "" Then
                    Exit For
                End If
                TableauData = Strings.Split(TableauObjet(IndiceA), VI.Tild, -1)
                VClasseVisu = New Virtualia.Ressources.Datas.ObjetVisualiserTout(RhObjet.Intitule)
                For IndiceC = Idebut To TableauData.Count - 1
                    If IndiceC > RhObjet.NombredInformations - 1 Then
                        Exit For
                    End If
                    Select Case IndiceC
                        Case Is = 0
                            VClasseVisu.Colonne_Debut = TableauData(IndiceC)
                            VClasseVisu.Etiquette_Debut = RhObjet.Item(0).Intitule
                        Case Else
                            Select Case IndiceC
                                Case Is = IposFin
                                    VClasseVisu.Colonne_Fin = TableauData(IndiceC)
                                    VClasseVisu.Etiquette_Fin = RhObjet.Item(IndiceC).Intitule
                                Case Else
                                    VClasseVisu.Donnee(IndiceC) = TableauData(IndiceC)
                                    VClasseVisu.Etiquette(IndiceC) = RhObjet.Item(IndiceC - Idebut).Intitule
                            End Select
                    End Select
                Next IndiceC
                Datas.Add(VClasseVisu)
                If NoObjet = 1 Then
                    If Pvue = VI.PointdeVue.PVueApplicatif Then
                        Intitule.Text = TableauData(2) & Strings.Space(2) & TableauData(3)
                    End If
                End If
            Next IndiceA
        Loop
        VisuTout.DataSource = Datas
        VisuTout.DataBind()

        Dim CtlTr As System.Web.UI.HtmlControls.HtmlTableRow = Nothing
        Dim CtlTd As System.Web.UI.HtmlControls.HtmlTableCell = Nothing
        Dim CtlInfo As System.Web.UI.WebControls.Label
        Dim CtlLnk As System.Web.UI.WebControls.HyperLink = Nothing
        Dim Ctl As Control
        Dim IndiceK As Integer
        Dim Rupture As String = ""
        For IndiceA = 0 To VisuTout.Items.Count - 1
            For IndiceC = 0 To VisuTout.Items.Item(IndiceA).Controls.Count - 1
                If TypeOf (VisuTout.Items.Item(IndiceA).Controls.Item(IndiceC)) Is System.Web.UI.HtmlControls.HtmlTableRow Then
                    CtlTr = CType(VisuTout.Items.Item(IndiceA).Controls.Item(IndiceC), System.Web.UI.HtmlControls.HtmlTableRow)
                    For IndiceO = 0 To CtlTr.Controls.Count - 1
                        Ctl = CtlTr.Controls.Item(IndiceO)
                        If TypeOf (Ctl) Is System.Web.UI.HtmlControls.HtmlTableCell Then
                            CtlTd = CType(Ctl, System.Web.UI.HtmlControls.HtmlTableCell)
                            For IndiceK = 0 To CtlTd.Controls.Count - 1
                                Ctl = CtlTd.Controls.Item(IndiceK)
                                If TypeOf (Ctl) Is System.Web.UI.WebControls.Label Then
                                    CtlInfo = CType(Ctl, System.Web.UI.WebControls.Label)
                                    If CtlTr.ID = "Ligne00" Then
                                        If CtlInfo.Text = "" Then
                                            CtlInfo.Visible = False
                                        End If
                                    Else
                                        If CtlInfo.Text = "" Then
                                            CtlTr.Visible = False
                                        Else
                                            CtlTr.Visible = True
                                        End If
                                    End If
                                    If CtlInfo.ID = "Objet" Then
                                        If CtlInfo.Text = Rupture Then
                                            CtlInfo.Visible = False
                                        End If
                                        Rupture = CtlInfo.Text
                                    End If
                                End If
                            Next IndiceK
                        End If
                    Next IndiceO
                End If
            Next IndiceC
        Next IndiceA

    End Sub

    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        WebFct = New Virtualia.Net.WebFonctions(Me, 0)
    End Sub

    Protected Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
        WebFct.Dispose()
    End Sub

End Class
