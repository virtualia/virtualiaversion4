﻿Option Strict On
Option Explicit On
Option Compare Text
Imports VI = Virtualia.Systeme.Constantes
Partial Class Fenetre_ChangementBase
    Inherits System.Web.UI.UserControl
    Public Delegate Sub MessageEventHandler(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.MessageSaisieEventArgs)
    Public Event MessageSaisie As MessageEventHandler
    Private WebFct As Virtualia.Net.WebFonctions
    Private WsNomStateBd As String = "ChangerBD"

    Protected Overridable Sub Message_Saisie(ByVal e As Virtualia.Systeme.Evenements.MessageSaisieEventArgs)
        RaiseEvent MessageSaisie(Me, e)
    End Sub

    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        WebFct = New Virtualia.Net.WebFonctions(Me, 0)
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        VDatabase.DonText = WebFct.PointeurUtilisateur.InstanceBd.Intitule
    End Sub

    Protected Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
        WebFct.Dispose()
    End Sub

    Protected Sub CommandeOK_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles CommandeOK.Click
        Dim CacheSgbd As ArrayList
        Dim NumBd As Short

        If Me.ViewState(WsNomStateBd) Is Nothing Then
            Exit Sub
        End If
        CacheSgbd = CType(Me.ViewState(WsNomStateBd), ArrayList)
        NumBd = CShort(CacheSgbd(0))

        Dim Cretour As Boolean
        Dim MajOK As Virtualia.Systeme.Evenements.MessageSaisieEventArgs = Nothing
        Cretour = WebFct.PointeurUtilisateur.ChangerBaseCourante(CShort(NumBd))
        Select Case Cretour
            Case False
                MajOK = New Virtualia.Systeme.Evenements.MessageSaisieEventArgs(Me.ID, "KO", "Impossible de se connecter à la base de données")
            Case True
                MajOK = New Virtualia.Systeme.Evenements.MessageSaisieEventArgs(Me.ID, "OK", "Opération effectuée avec succés")
        End Select
        Message_Saisie(MajOK)
    End Sub

    Protected Sub CtlListeDatabases_ListeSgbd_Click(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.ListeSgbdEventArgs) Handles CtlListeDatabases.ListeSgbd_Click
        VDatabase.DonText = e.IntituleBd
        Dim CacheSgbd As ArrayList

        If Me.ViewState(WsNomStateBd) IsNot Nothing Then
            Me.ViewState.Remove(WsNomStateBd)
        End If
        CacheSgbd = New ArrayList(9)
        CacheSgbd.Add(e.NumeroBd)
        CacheSgbd.Add(e.NomBd)
        CacheSgbd.Add(e.IntituleBd)
        Me.ViewState.Add(WsNomStateBd, CacheSgbd)

    End Sub
End Class
