﻿<%@ Control Language="VB" AutoEventWireup="false" Inherits="Virtualia.Net.Fenetre_ChangementBase" Codebehind="ChangementBase.ascx.vb" %>

<%@ Register src="../Saisies/VCoupleEtiDonnee.ascx" tagname="VCoupleEtiDonnee" tagprefix="Virtualia" %>
<%@ Register src="../ObjetsArmoire/ListeDatabases.ascx" tagname="ListeDatabases" tagprefix="Virtualia" %>
   
<asp:Panel ID="CadreChangeBD" runat="server" BackColor="#7D9F99" HorizontalAlign="Center"
           Style="margin-top: 10px; border-color: #B0E0D7; border-style: ridge; border-width: 4px;
                    height: 590px; width: 600px; text-align: left; ">
   <asp:Table ID="CadreChange" runat="server" Width="600px">
       <asp:TableRow>
        <asp:TableCell Height="10px"></asp:TableCell>
       </asp:TableRow>
       <asp:TableRow>
            <asp:TableCell>
                <asp:Table ID="CadreCmdOK" runat="server" Height="22px" CellPadding="0" 
                     CellSpacing="0" BackImageUrl="~/Images/Boutons/OK_Std.bmp" Visible="true"
                     BorderWidth="2px" BorderStyle="Outset" BorderColor="#B0E0D7" ForeColor="#D7FAF3"
                     Width="70px" HorizontalAlign="Left" style="margin-top: 3px; margin-right:3px" >
                    <asp:TableRow VerticalAlign="Top">
                        <asp:TableCell>
                             <asp:Button ID="CommandeOK" runat="server" Text="Valider" Width="65px" Height="20px"
                                 BackColor="Transparent" BorderColor="#B0E0D7" ForeColor="#D7FAF3"
                                 Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Font-Italic="true"
                                 BorderStyle="None" style=" margin-left: 6px; text-align: center;">
                             </asp:Button>
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
            </asp:TableCell>
       </asp:TableRow>
       <asp:TableRow>
            <asp:TableCell Height="10px"></asp:TableCell>
       </asp:TableRow>
       <asp:TableRow>
            <asp:TableCell Height="30px" HorizontalAlign="Left">
               <Virtualia:VCoupleEtiDonnee ID="VDatabase" runat="server" EtiBackColor="#98D4C4" 
                    EtiWidth="250px" DonWidth="300px" V_SiEnLectureSeule="true"
                    V_SiDonneeDico="false" EtiText="Nom de la base de données courante" />
           </asp:TableCell>
       </asp:TableRow>
       <asp:TableRow>
            <asp:TableCell HorizontalAlign="Left" >
                <Virtualia:ListeDatabases ID="CtlListeDatabases" runat="server" 
                      SiIntituleComplet="true" ListeWidth="500px" /> 
            </asp:TableCell>
       </asp:TableRow>
    </asp:Table>
 </asp:Panel>