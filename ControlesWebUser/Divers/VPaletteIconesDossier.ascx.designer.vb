﻿'------------------------------------------------------------------------------
' <généré automatiquement>
'     Ce code a été généré par un outil.
'
'     Les modifications apportées à ce fichier peuvent provoquer un comportement incorrect et seront perdues si
'     le code est régénéré.
' </généré automatiquement>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class Controles_VPaletteIcones

    '''<summary>
    '''Contrôle VIconesDossier.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VIconesDossier As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle VImage00.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VImage00 As Global.System.Web.UI.WebControls.ImageButton

    '''<summary>
    '''Contrôle VImage01.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VImage01 As Global.System.Web.UI.WebControls.ImageButton

    '''<summary>
    '''Contrôle VImage02.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VImage02 As Global.System.Web.UI.WebControls.ImageButton

    '''<summary>
    '''Contrôle VImage03.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VImage03 As Global.System.Web.UI.WebControls.ImageButton

    '''<summary>
    '''Contrôle VImage04.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VImage04 As Global.System.Web.UI.WebControls.ImageButton

    '''<summary>
    '''Contrôle VImage05.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VImage05 As Global.System.Web.UI.WebControls.ImageButton

    '''<summary>
    '''Contrôle VImage06.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VImage06 As Global.System.Web.UI.WebControls.ImageButton

    '''<summary>
    '''Contrôle VImage07.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VImage07 As Global.System.Web.UI.WebControls.ImageButton

    '''<summary>
    '''Contrôle VImage08.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VImage08 As Global.System.Web.UI.WebControls.ImageButton

    '''<summary>
    '''Contrôle VImage09.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VImage09 As Global.System.Web.UI.WebControls.ImageButton

    '''<summary>
    '''Contrôle VImage10.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VImage10 As Global.System.Web.UI.WebControls.ImageButton

    '''<summary>
    '''Contrôle VImage11.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VImage11 As Global.System.Web.UI.WebControls.ImageButton

    '''<summary>
    '''Contrôle VImage12.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VImage12 As Global.System.Web.UI.WebControls.ImageButton

    '''<summary>
    '''Contrôle VImage13.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VImage13 As Global.System.Web.UI.WebControls.ImageButton

    '''<summary>
    '''Contrôle VImage14.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VImage14 As Global.System.Web.UI.WebControls.ImageButton
End Class
