﻿<%@ Control Language="VB" AutoEventWireup="false" Inherits="Virtualia.Net.Controles_VPaletteIcones" Codebehind="VPaletteIconesDossier.ascx.vb" %>

<asp:Table ID="VIconesDossier" runat="server"  Height="120px" Width="120px" CellPadding="1" CellSpacing="1" 
                        BorderStyle="Ridge" BorderColor="#B0E0D7" BorderWidth="3px" >
    <asp:TableRow>
        <asp:TableCell HorizontalAlign="Center" VerticalAlign="Middle">
            <asp:ImageButton ID="VImage00" runat="server" BorderStyle="None"
                 ImageAlign="Middle" ImageUrl="~/Images/Armoire/JauneFermer16.bmp" />
        </asp:TableCell>
        <asp:TableCell HorizontalAlign="Center" VerticalAlign="Middle">
            <asp:ImageButton ID="VImage01" runat="server" BorderStyle="None" 
                 ImageAlign="Middle" ImageUrl="~/Images/Armoire/OrangeFonceFermer16.bmp" />
        </asp:TableCell>
        <asp:TableCell HorizontalAlign="Center" VerticalAlign="Middle">
            <asp:ImageButton ID="VImage02" runat="server" BorderStyle="None" 
                 ImageAlign="Middle" ImageUrl="~/Images/Armoire/TurquoiseFermer16.bmp" />
        </asp:TableCell>
        <asp:TableCell HorizontalAlign="Center" VerticalAlign="Middle">
            <asp:ImageButton ID="VImage03" runat="server" BorderStyle="None" 
                 ImageAlign="Middle" ImageUrl="~/Images/Armoire/BleuFermer16.bmp" />
        </asp:TableCell>
        <asp:TableCell HorizontalAlign="Center" VerticalAlign="Middle">
            <asp:ImageButton ID="VImage04" runat="server" BorderStyle="None" 
                 ImageAlign="Middle" ImageUrl="~/Images/Armoire/BleuFonceFermer16.bmp" />
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
         <asp:TableCell HorizontalAlign="Center" VerticalAlign="Middle">
            <asp:ImageButton ID="VImage05" runat="server" BorderStyle="None" 
                 ImageAlign="Middle" ImageUrl="~/Images/Armoire/SaumonFermer16.bmp" />
        </asp:TableCell> 
        <asp:TableCell HorizontalAlign="Center" VerticalAlign="Middle">
            <asp:ImageButton ID="VImage06" runat="server" BorderStyle="None" 
                 ImageAlign="Middle" ImageUrl="~/Images/Armoire/RougeCarminFermer16.bmp" />
        </asp:TableCell>
        <asp:TableCell HorizontalAlign="Center" VerticalAlign="Middle">
            <asp:ImageButton ID="VImage07" runat="server" BorderStyle="None" 
                 ImageAlign="Middle" ImageUrl="~/Images/Armoire/MarronFermer16.bmp" />
        </asp:TableCell>
        <asp:TableCell HorizontalAlign="Center" VerticalAlign="Middle">
            <asp:ImageButton ID="VImage08" runat="server" BorderStyle="None" 
                 ImageAlign="Middle" ImageUrl="~/Images/Armoire/GrisClairFermer16.bmp" />
        </asp:TableCell>
        <asp:TableCell HorizontalAlign="Center" VerticalAlign="Middle">
            <asp:ImageButton ID="VImage09" runat="server" BorderStyle="None" 
                 ImageAlign="Middle" ImageUrl="~/Images/Armoire/GrisFonceFermer16.bmp" />
        </asp:TableCell>
    </asp:TableRow>
     <asp:TableRow>
        <asp:TableCell HorizontalAlign="Center" VerticalAlign="Middle">
            <asp:ImageButton ID="VImage10" runat="server" BorderStyle="None" 
                 ImageAlign="Middle" ImageUrl="~/Images/Armoire/NoirFermer16.bmp" />
        </asp:TableCell>
         <asp:TableCell HorizontalAlign="Center" VerticalAlign="Middle">
            <asp:ImageButton ID="VImage11" runat="server" BorderStyle="None" 
                 ImageAlign="Middle" ImageUrl="~/Images/Armoire/VerdatreFermer16.bmp" />
        </asp:TableCell>
        <asp:TableCell HorizontalAlign="Center" VerticalAlign="Middle">
            <asp:ImageButton ID="VImage12" runat="server" BorderStyle="None" 
                 ImageAlign="Middle" ImageUrl="~/Images/Armoire/VertClairFermer16.bmp" />
        </asp:TableCell>
        <asp:TableCell HorizontalAlign="Center" VerticalAlign="Middle">
            <asp:ImageButton ID="VImage13" runat="server" BorderStyle="None" 
                 ImageAlign="Middle" ImageUrl="~/Images/Armoire/VertFermer16.bmp" />
        </asp:TableCell>
        <asp:TableCell HorizontalAlign="Center" VerticalAlign="Middle">
            <asp:ImageButton ID="VImage14" runat="server" BorderStyle="None" 
                 ImageAlign="Middle" ImageUrl="~/Images/Armoire/VertFonceFermer16.bmp" />
        </asp:TableCell>
    </asp:TableRow>
</asp:Table>
    