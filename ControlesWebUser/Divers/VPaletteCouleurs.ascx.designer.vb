﻿'------------------------------------------------------------------------------
' <généré automatiquement>
'     Ce code a été généré par un outil.
'
'     Les modifications apportées à ce fichier peuvent provoquer un comportement incorrect et seront perdues si
'     le code est régénéré.
' </généré automatiquement>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class Controles_VPaletteCouleurs

    '''<summary>
    '''Contrôle VPalette.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VPalette As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle CadreCmdOK.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CadreCmdOK As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle CommandeOK.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CommandeOK As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Contrôle EtiCouleur.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiCouleur As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle DonCouleur.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents DonCouleur As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''Contrôle Marron00.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Marron00 As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Contrôle Marron01.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Marron01 As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Contrôle Marron02.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Marron02 As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Contrôle Marron03.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Marron03 As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Contrôle Marron04.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Marron04 As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Contrôle Marron05.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Marron05 As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Contrôle Marron06.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Marron06 As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Contrôle Rouge00.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Rouge00 As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Contrôle Rouge01.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Rouge01 As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Contrôle Rouge02.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Rouge02 As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Contrôle Rouge03.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Rouge03 As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Contrôle Rouge04.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Rouge04 As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Contrôle Rouge05.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Rouge05 As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Contrôle Rouge06.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Rouge06 As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Contrôle Rose00.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Rose00 As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Contrôle Rose01.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Rose01 As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Contrôle Rose02.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Rose02 As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Contrôle Rose03.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Rose03 As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Contrôle Rose04.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Rose04 As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Contrôle Rose05.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Rose05 As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Contrôle Rose06.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Rose06 As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Contrôle Jaune00.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Jaune00 As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Contrôle Jaune01.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Jaune01 As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Contrôle Jaune02.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Jaune02 As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Contrôle Jaune03.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Jaune03 As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Contrôle Jaune04.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Jaune04 As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Contrôle Jaune05.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Jaune05 As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Contrôle Jaune06.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Jaune06 As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Contrôle Gold00.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Gold00 As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Contrôle Gold01.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Gold01 As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Contrôle Gold02.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Gold02 As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Contrôle Gold03.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Gold03 As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Contrôle Gold04.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Gold04 As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Contrôle Gold05.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Gold05 As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Contrôle Gold06.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Gold06 As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Contrôle Orange00.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Orange00 As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Contrôle Orange01.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Orange01 As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Contrôle Orange02.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Orange02 As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Contrôle Orange03.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Orange03 As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Contrôle Orange04.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Orange04 As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Contrôle Orange05.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Orange05 As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Contrôle Orange06.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Orange06 As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Contrôle Vert00.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Vert00 As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Contrôle Vert01.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Vert01 As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Contrôle Vert02.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Vert02 As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Contrôle Vert03.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Vert03 As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Contrôle Vert04.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Vert04 As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Contrôle Vert05.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Vert05 As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Contrôle Vert06.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Vert06 As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Contrôle Vert07.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Vert07 As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Contrôle Vert08.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Vert08 As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Contrôle Vert09.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Vert09 As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Contrôle Vert10.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Vert10 As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Contrôle Vert11.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Vert11 As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Contrôle Vert12.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Vert12 As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Contrôle Vert13.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Vert13 As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Contrôle Vert14.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Vert14 As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Contrôle Vert15.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Vert15 As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Contrôle Vert16.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Vert16 As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Contrôle Vert17.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Vert17 As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Contrôle Vert18.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Vert18 As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Contrôle Vert19.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Vert19 As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Contrôle Vert20.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Vert20 As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Contrôle Bleu00.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Bleu00 As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Contrôle Bleu01.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Bleu01 As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Contrôle Bleu02.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Bleu02 As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Contrôle Bleu03.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Bleu03 As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Contrôle Bleu04.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Bleu04 As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Contrôle Bleu05.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Bleu05 As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Contrôle Bleu06.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Bleu06 As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Contrôle Bleu07.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Bleu07 As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Contrôle Bleu08.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Bleu08 As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Contrôle Bleu09.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Bleu09 As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Contrôle Bleu10.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Bleu10 As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Contrôle Bleu11.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Bleu11 As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Contrôle Bleu12.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Bleu12 As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Contrôle Bleu13.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Bleu13 As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Contrôle Bleu14.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Bleu14 As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Contrôle Bleu15.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Bleu15 As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Contrôle Bleu16.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Bleu16 As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Contrôle Bleu17.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Bleu17 As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Contrôle Bleu18.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Bleu18 As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Contrôle Bleu19.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Bleu19 As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Contrôle Bleu20.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Bleu20 As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Contrôle Gris00.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Gris00 As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Contrôle Gris01.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Gris01 As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Contrôle Gris02.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Gris02 As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Contrôle Gris03.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Gris03 As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Contrôle Gris04.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Gris04 As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Contrôle Gris05.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Gris05 As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Contrôle Gris06.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Gris06 As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Contrôle Gris07.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Gris07 As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Contrôle Gris08.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Gris08 As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Contrôle Gris09.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Gris09 As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Contrôle Gris10.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Gris10 As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Contrôle Gris11.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Gris11 As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Contrôle Gris12.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Gris12 As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Contrôle Gris13.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Gris13 As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Contrôle Gris14.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Gris14 As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Contrôle Gris15.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Gris15 As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Contrôle Gris16.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Gris16 As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Contrôle Gris17.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Gris17 As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Contrôle Gris18.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Gris18 As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Contrôle Gris19.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Gris19 As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Contrôle Gris20.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Gris20 As Global.System.Web.UI.WebControls.Button
End Class
