﻿Option Strict On
Option Explicit On
Option Compare Text
Partial Class Controles_VPaletteIcones
    Inherits System.Web.UI.UserControl
    Public Delegate Sub Valeur_ChangeEventHandler(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.DonneeChangeEventArgs)
    Public Event ValeurChange As Valeur_ChangeEventHandler
    Private WebFct As Virtualia.Net.WebFonctions

    Protected Overridable Sub Valeur_Change(ByVal e As Virtualia.Systeme.Evenements.DonneeChangeEventArgs)
        RaiseEvent ValeurChange(Me, e)
    End Sub

    Public WriteOnly Property Numero() As Integer
        Set(ByVal value As Integer)
            Dim IndiceI As Integer
            Dim Ctl As Control
            Dim VirIcone As System.Web.UI.WebControls.ImageButton
            IndiceI = 0
            Do
                Ctl = WebFct.VirWebControle(Me.VIconesDossier, "VImage", IndiceI)
                If Ctl Is Nothing Then
                    Exit Do
                End If
                VirIcone = CType(Ctl, System.Web.UI.WebControls.ImageButton)
                If CInt(Strings.Right(VirIcone.ID, 2)) = value Then
                    VirIcone.BorderStyle = BorderStyle.Solid
                    VirIcone.BorderColor = Drawing.Color.DarkGreen
                    VirIcone.BorderWidth = New Unit(2)
                Else
                    VirIcone.BorderStyle = BorderStyle.None
                End If
                IndiceI += 1
            Loop
        End Set
    End Property

    Protected Sub VImage_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles VImage00.Click, _
    VImage01.Click, VImage02.Click, VImage03.Click, VImage04.Click, VImage05.Click, VImage06.Click, VImage07.Click, _
    VImage08.Click, VImage09.Click, VImage10.Click, VImage11.Click, VImage12.Click, VImage13.Click, VImage14.Click
        Dim IndexImage As Integer

        IndexImage = CInt(Strings.Right(CType(sender, System.Web.UI.WebControls.ImageButton).ID, 2))
        Numero = IndexImage

        Dim Evenement As Virtualia.Systeme.Evenements.DonneeChangeEventArgs
        Evenement = New Virtualia.Systeme.Evenements.DonneeChangeEventArgs("Icone", IndexImage.ToString)
        Valeur_Change(Evenement)
    End Sub

    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        WebFct = New Virtualia.Net.WebFonctions(Me, 0)
    End Sub
End Class
