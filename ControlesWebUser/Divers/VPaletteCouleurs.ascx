﻿<%@ Control Language="VB" AutoEventWireup="false" Inherits="Virtualia.Net.Controles_VPaletteCouleurs" Codebehind="VPaletteCouleurs.ascx.vb" %>

<asp:Table ID="VPalette" runat="server"  Height="382px" Width="345px" CellPadding="0" CellSpacing="0" 
                        BorderStyle="Ridge" BorderColor="#B0E0D7" BorderWidth="4px" >
    <asp:TableRow>
        <asp:TableCell ColumnSpan="7">
            <asp:Table ID="CadreCmdOK" runat="server" Height="22px" CellPadding="0" 
                     CellSpacing="0" BackImageUrl="~/Images/Boutons/OK_Std.bmp" Visible="true"
                     BorderWidth="2px" BorderStyle="Outset" BorderColor="#FFEBC8" ForeColor="#FFF2DB"
                     Width="70px" HorizontalAlign="Right" style="margin-top: 5px; margin-right:3px" >
                    <asp:TableRow>
                        <asp:TableCell VerticalAlign="Bottom">
                             <asp:Button ID="CommandeOK" runat="server" Text="Valider" Width="65px" Height="20px"
                                 BackColor="Transparent" BorderColor="#B0E0D7" ForeColor="#D7FAF3"
                                 Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Smaller" Font-Italic="true"
                                 BorderStyle="None" style=" margin-left: 6px; text-align: center;">
                             </asp:Button>
                        </asp:TableCell>
                    </asp:TableRow>
             </asp:Table>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
         <asp:TableCell ColumnSpan="2">
            <asp:Label ID="EtiCouleur" runat="server" Height="20px" Width="90px"
                    BackColor="#FFE5B7" BorderColor="#FFEBC8"  BorderStyle="Outset"
                    BorderWidth="2px" ForeColor="Black" Font-Italic="true" Text="Intitulé"
                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                    style="margin-top: 5px; margin-left: 4px; text-indent: 5px; text-align: center" >
            </asp:Label>
        </asp:TableCell>
        <asp:TableCell ColumnSpan="5">
            <asp:TextBox ID="DonCouleur" runat="server" Height="16px" Width="225px" MaxLength="0"
                    BackColor="White" BorderColor="#FFE5B7"  BorderStyle="Inset"
                    BorderWidth="2px" ForeColor="Black" Visible="true" AutoPostBack="true"
                    Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Small" Font-Italic="false"
                    style="margin-top: 6px; text-indent: 1px; text-align: left" TextMode="SingleLine"> 
            </asp:TextBox>
        </asp:TableCell>
    </asp:TableRow>      
    <asp:TableRow>
          <asp:TableCell>
                <asp:Button ID="Marron00" runat="server" Height="22px" Width="40px" BackColor="Maroon" ForeColor="#064F4D"
                    BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px" ToolTip="Maroon" CommandName="VChoix"
                    Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                    style="margin-left: 4px; margin-top: 10px; text-align: left; vertical-align: top;" />
           </asp:TableCell>
           <asp:TableCell>
                <asp:Button ID="Marron01" runat="server" Height="22px" Width="40px" BackColor="Moccasin" ForeColor="#064F4D"
                    BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px" ToolTip="Moccasin"
                    Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                    style="margin-top: 10px; text-align: left; vertical-align: top;" />
           </asp:TableCell>
           <asp:TableCell>
                <asp:Button ID="Marron02" runat="server" Height="22px" Width="40px" BackColor="Brown" ForeColor="#064F4D"
                    BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px" ToolTip="Brown"
                    Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                    style="margin-top: 10px; text-align: left; vertical-align: top;" />
           </asp:TableCell>
           <asp:TableCell>
                <asp:Button ID="Marron03" runat="server" Height="22px" Width="40px" BackColor="CornSilk" ForeColor="#064F4D"
                    BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px" ToolTip="CornSilk"
                    Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                    style="margin-top: 10px; text-align: left; vertical-align: top;" />
           </asp:TableCell>
           <asp:TableCell>
                <asp:Button ID="Marron04" runat="server" Height="22px" Width="40px" BackColor="DarkKhaki" ForeColor="#064F4D"
                    BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px" ToolTip="DarkKhaki"
                    Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                    style="margin-top: 10px; text-align: left; vertical-align: top;" />
           </asp:TableCell>
           <asp:TableCell>
                <asp:Button ID="Marron05" runat="server" Height="22px" Width="40px" BackColor="Peru" ForeColor="#064F4D"
                    BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px" ToolTip="Peru"
                    Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                    style="margin-top: 10px; text-align: left; vertical-align: top;" />
           </asp:TableCell>
           <asp:TableCell>
                <asp:Button ID="Marron06" runat="server" Height="22px" Width="40px" BackColor="Tan" ForeColor="#064F4D"
                    BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px" ToolTip="Tan"
                    Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                    style="margin-top: 10px; text-align: left; vertical-align: top;" />
           </asp:TableCell>         
    </asp:TableRow>
    <asp:TableRow>
          <asp:TableCell>
                <asp:Button ID="Rouge00" runat="server" Height="22px" Width="40px" BackColor="Red" ForeColor="#064F4D"
                    BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px" ToolTip="Red"
                    Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                    style="margin-left: 4px; margin-top: 1px; text-align: left; vertical-align: top;" />
           </asp:TableCell>
           <asp:TableCell>
                <asp:Button ID="Rouge01" runat="server" Height="22px" Width="40px" BackColor="Tomato" ForeColor="#064F4D"
                    BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px" ToolTip="Tomato"
                    Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                    style="margin-top: 1px; text-align: left; vertical-align: top;" />
           </asp:TableCell>
           <asp:TableCell>
                <asp:Button ID="Rouge02" runat="server" Height="22px" Width="40px" BackColor="Firebrick" ForeColor="#064F4D"
                    BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px" ToolTip="Firebrick"
                    Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                    style="margin-top: 1px; text-align: left; vertical-align: top;" />
           </asp:TableCell>
           <asp:TableCell>
                <asp:Button ID="Rouge03" runat="server" Height="22px" Width="40px" BackColor="Darkred" ForeColor="#064F4D"
                    BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px" ToolTip="Darkred"
                    Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                    style="margin-top: 1px; text-align: left; vertical-align: top;" />
           </asp:TableCell>
           <asp:TableCell>
                <asp:Button ID="Rouge04" runat="server" Height="22px" Width="40px" BackColor="Crimson" ForeColor="#064F4D"
                    BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px" ToolTip="Crimson"
                    Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                    style="margin-top: 1px; text-align: left; vertical-align: top;" />
           </asp:TableCell>
           <asp:TableCell>
                <asp:Button ID="Rouge05" runat="server" Height="22px" Width="40px" BackColor="Coral" ForeColor="#064F4D"
                    BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px" ToolTip="Coral"
                    Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                    style="margin-top: 1px; text-align: left; vertical-align: top;" />
           </asp:TableCell>
           <asp:TableCell>
                <asp:Button ID="Rouge06" runat="server" Height="22px" Width="40px" BackColor="LightCoral" ForeColor="#064F4D"
                    BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px" ToolTip="LightCoral"
                    Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                    style="margin-top: 1px; text-align: left; vertical-align: top;" />
           </asp:TableCell>         
    </asp:TableRow>
     <asp:TableRow>
          <asp:TableCell>
                <asp:Button ID="Rose00" runat="server" Height="22px" Width="40px" BackColor="Pink" ForeColor="#064F4D"
                    BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px" ToolTip="Pink"
                    Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                    style="margin-left: 4px; margin-top: 1px; text-align: left; vertical-align: top;" />
           </asp:TableCell>
           <asp:TableCell>
                <asp:Button ID="Rose01" runat="server" Height="22px" Width="40px" BackColor="Magenta" ForeColor="#064F4D"
                    BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px" ToolTip="Magenta"
                    Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                    style="margin-top: 1px; text-align: left; vertical-align: top;" />
           </asp:TableCell>
           <asp:TableCell>
                <asp:Button ID="Rose02" runat="server" Height="22px" Width="40px" BackColor="MediumVioletRed" ForeColor="#064F4D"
                    BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px" ToolTip="MediumVioletRed"
                    Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                    style="margin-top: 1px; text-align: left; vertical-align: top;" />
           </asp:TableCell>
           <asp:TableCell>
                <asp:Button ID="Rose03" runat="server" Height="22px" Width="40px" BackColor="HotPink" ForeColor="#064F4D"
                    BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px" ToolTip="HotPink"
                    Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                    style="margin-top: 1px; text-align: left; vertical-align: top;" />
           </asp:TableCell>
           <asp:TableCell>
                <asp:Button ID="Rose04" runat="server" Height="22px" Width="40px" BackColor="Fuchsia" ForeColor="#064F4D"
                    BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px" ToolTip="Fuchsia"
                    Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                    style="margin-top: 1px; text-align: left; vertical-align: top;" />
           </asp:TableCell>
           <asp:TableCell>
                <asp:Button ID="Rose05" runat="server" Height="22px" Width="40px" BackColor="DeepPink" ForeColor="#064F4D"
                    BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px" ToolTip="DeepPink"
                    Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                    style="margin-top: 1px; text-align: left; vertical-align: top;" />
           </asp:TableCell>
           <asp:TableCell>
                <asp:Button ID="Rose06" runat="server" Height="22px" Width="40px" BackColor="DarkMagenta" ForeColor="#064F4D"
                    BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px" ToolTip="DarkMagenta"
                    Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                    style="margin-top: 1px; text-align: left; vertical-align: top;" />
           </asp:TableCell>         
    </asp:TableRow>
    <asp:TableRow>
          <asp:TableCell>
                <asp:Button ID="Jaune00" runat="server" Height="22px" Width="40px" BackColor="Yellow" ForeColor="#064F4D"
                    BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px" ToolTip="Yellow"
                    Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                    style="margin-left: 4px; margin-top: 1px; text-align: left; vertical-align: top;" />
           </asp:TableCell>
           <asp:TableCell>
                <asp:Button ID="Jaune01" runat="server" Height="22px" Width="40px" BackColor="LemonChiffon" ForeColor="#064F4D"
                    BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px" ToolTip="LemonChiffon"
                    Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                    style="margin-top: 1px; text-align: left; vertical-align: top;" />
           </asp:TableCell>
           <asp:TableCell>
                <asp:Button ID="Jaune02" runat="server" Height="22px" Width="40px" BackColor="Olive" ForeColor="#064F4D"
                    BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px" ToolTip="Olive"
                    Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                    style="margin-top: 1px; text-align: left; vertical-align: top;" />
           </asp:TableCell>
           <asp:TableCell>
                <asp:Button ID="Jaune03" runat="server" Height="22px" Width="40px" BackColor="Sienna" ForeColor="#064F4D"
                    BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px" ToolTip="Sienna"
                    Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                    style="margin-top: 1px; text-align: left; vertical-align: top;" />
           </asp:TableCell>
           <asp:TableCell>
                <asp:Button ID="Jaune04" runat="server" Height="22px" Width="40px" BackColor="BurlyWood" ForeColor="#064F4D"
                    BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px" ToolTip="BurlyWood"
                    Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                    style="margin-top: 1px; text-align: left; vertical-align: top;" />
           </asp:TableCell>
           <asp:TableCell>
                <asp:Button ID="Jaune05" runat="server" Height="22px" Width="40px" BackColor="Khaki" ForeColor="#064F4D"
                    BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px" ToolTip="Khaki"
                    Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                    style="margin-top: 1px; text-align: left; vertical-align: top;" />
           </asp:TableCell>
           <asp:TableCell>
                <asp:Button ID="Jaune06" runat="server" Height="22px" Width="40px" BackColor="NavajoWhite" ForeColor="#064F4D"
                    BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px" ToolTip="NavajoWhite"
                    Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                    style="margin-top: 1px; text-align: left; vertical-align: top;" />
           </asp:TableCell>         
    </asp:TableRow>
    <asp:TableRow>
          <asp:TableCell>
                <asp:Button ID="Gold00" runat="server" Height="22px" Width="40px" BackColor="Gold" ForeColor="#064F4D"
                    BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px" ToolTip="Gold"
                    Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                    style="margin-left: 4px; margin-top: 1px; text-align: left; vertical-align: top;" />
           </asp:TableCell>
           <asp:TableCell>
                <asp:Button ID="Gold01" runat="server" Height="22px" Width="40px" BackColor="PaleGoldenrod" ForeColor="#064F4D"
                    BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px" ToolTip="PaleGoldenrod"
                    Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                    style="margin-top: 1px; text-align: left; vertical-align: top;" />
           </asp:TableCell>
           <asp:TableCell>
                <asp:Button ID="Gold02" runat="server" Height="22px" Width="40px" BackColor="DarkSalmon" ForeColor="#064F4D"
                    BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px" ToolTip="DarkSalmon"
                    Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                    style="margin-top: 1px; text-align: left; vertical-align: top;" />
           </asp:TableCell>
           <asp:TableCell>
                <asp:Button ID="Gold03" runat="server" Height="22px" Width="40px" BackColor="DarkGoldenrod" ForeColor="#064F4D"
                    BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px" ToolTip="DarkGoldenrod"
                    Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                    style="margin-top: 1px; text-align: left; vertical-align: top;" />
           </asp:TableCell>
           <asp:TableCell>
                <asp:Button ID="Gold04" runat="server" Height="22px" Width="40px" BackColor="Bisque" ForeColor="#064F4D"
                    BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px" ToolTip="Bisque"
                    Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                    style="margin-top: 1px; text-align: left; vertical-align: top;" />
           </asp:TableCell>
           <asp:TableCell>
                <asp:Button ID="Gold05" runat="server" Height="22px" Width="40px" BackColor="SandyBrown" ForeColor="#064F4D"
                    BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px" ToolTip="SandyBrown"
                    Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                    style="margin-top: 1px; text-align: left; vertical-align: top;" />
           </asp:TableCell>
           <asp:TableCell>
                <asp:Button ID="Gold06" runat="server" Height="22px" Width="40px" BackColor="PeachPuff" ForeColor="#064F4D"
                    BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px" ToolTip="PeachPuff"
                    Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                    style="margin-top: 1px; text-align: left; vertical-align: top;" />
           </asp:TableCell>         
    </asp:TableRow>
    <asp:TableRow>
          <asp:TableCell>
                <asp:Button ID="Orange00" runat="server" Height="22px" Width="40px" BackColor="Orange" ForeColor="#064F4D"
                    BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px" ToolTip="Orange"
                    Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                    style="margin-left: 4px; margin-top: 1px; text-align: left; vertical-align: top;" />
           </asp:TableCell>
           <asp:TableCell>
                <asp:Button ID="Orange01" runat="server" Height="22px" Width="40px" BackColor="OrangeRed" ForeColor="#064F4D"
                    BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px" ToolTip="OrangeRed"
                    Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                    style="margin-top: 1px; text-align: left; vertical-align: top;" />
           </asp:TableCell>
           <asp:TableCell>
                <asp:Button ID="Orange02" runat="server" Height="22px" Width="40px" BackColor="Salmon" ForeColor="#064F4D"
                    BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px" ToolTip="Salmon"
                    Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                    style="margin-top: 1px; text-align: left; vertical-align: top;" />
           </asp:TableCell>
           <asp:TableCell>
                <asp:Button ID="Orange03" runat="server" Height="22px" Width="40px" BackColor="IndianRed" ForeColor="#064F4D"
                    BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px" ToolTip="IndianRed"
                    Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                    style="margin-top: 1px; text-align: left; vertical-align: top;" />
           </asp:TableCell>
           <asp:TableCell>
                <asp:Button ID="Orange04" runat="server" Height="22px" Width="40px" BackColor="DarkOrange" ForeColor="#064F4D"
                    BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px" ToolTip="DarkOrange"
                    Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                    style="margin-top: 1px; text-align: left; vertical-align: top;" />
           </asp:TableCell>
           <asp:TableCell>
                <asp:Button ID="Orange05" runat="server" Height="22px" Width="40px" BackColor="Chocolate" ForeColor="#064F4D"
                    BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px" ToolTip="Chocolate"
                    Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                    style="margin-top: 1px; text-align: left; vertical-align: top;" />
           </asp:TableCell>
           <asp:TableCell>
                <asp:Button ID="Orange06" runat="server" Height="22px" Width="40px" BackColor="LightSalmon" ForeColor="#064F4D"
                    BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px" ToolTip="LightSalmon"
                    Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                    style="margin-top: 1px; text-align: left; vertical-align: top;" />
           </asp:TableCell>         
    </asp:TableRow>
    <asp:TableRow>
          <asp:TableCell>
                <asp:Button ID="Vert00" runat="server" Height="22px" Width="40px" BackColor="Green" ForeColor="#064F4D"
                    BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px" ToolTip="Green"
                    Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                    style="margin-left: 4px; margin-top: 1px; text-align: left; vertical-align: top;" />
           </asp:TableCell>
           <asp:TableCell>
                <asp:Button ID="Vert01" runat="server" Height="22px" Width="40px" BackColor="GreenYellow" ForeColor="#064F4D"
                    BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px" ToolTip="GreenYellow"
                    Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                    style="margin-top: 1px; text-align: left; vertical-align: top;" />
           </asp:TableCell>
           <asp:TableCell>
                <asp:Button ID="Vert02" runat="server" Height="22px" Width="40px" BackColor="DarkSeaGreen" ForeColor="#064F4D"
                    BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px" ToolTip="DarkSeaGreen"
                    Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                    style="margin-top: 1px; text-align: left; vertical-align: top;" />
           </asp:TableCell>
           <asp:TableCell>
                <asp:Button ID="Vert03" runat="server" Height="22px" Width="40px" BackColor="Chartreuse" ForeColor="#064F4D"
                    BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px" ToolTip="Chartreuse"
                    Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                    style="margin-top: 1px; text-align: left; vertical-align: top;" />
           </asp:TableCell>
           <asp:TableCell>
                <asp:Button ID="Vert04" runat="server" Height="22px" Width="40px" BackColor="DarkGreen" ForeColor="#064F4D"
                    BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px" ToolTip="DarkGreen"
                    Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                    style="margin-top: 1px; text-align: left; vertical-align: top;" />
           </asp:TableCell>
           <asp:TableCell>
                <asp:Button ID="Vert05" runat="server" Height="22px" Width="40px" BackColor="DarkOliveGreen" ForeColor="#064F4D"
                    BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px" ToolTip="DarkOliveGreen"
                    Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                    style="margin-top: 1px; text-align: left; vertical-align: top;" />
           </asp:TableCell>
           <asp:TableCell>
                <asp:Button ID="Vert06" runat="server" Height="22px" Width="40px" BackColor="LawnGreen" ForeColor="#064F4D"
                    BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px" ToolTip="LawnGreen"
                    Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                    style="margin-top: 1px; text-align: left; vertical-align: top;" />
           </asp:TableCell>         
    </asp:TableRow>
     <asp:TableRow>
          <asp:TableCell>
                <asp:Button ID="Vert07" runat="server" Height="22px" Width="40px" BackColor="YellowGreen" ForeColor="#064F4D"
                    BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px" ToolTip="YellowGreen"
                    Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                    style="margin-left: 4px; margin-top: 1px; text-align: left; vertical-align: top;" />
           </asp:TableCell>
           <asp:TableCell>
                <asp:Button ID="Vert08" runat="server" Height="22px" Width="40px" BackColor="SpringGreen" ForeColor="#064F4D"
                    BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px" ToolTip="SpringGreen"
                    Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                    style="margin-top: 1px; text-align: left; vertical-align: top;" />
           </asp:TableCell>
           <asp:TableCell>
                <asp:Button ID="Vert09" runat="server" Height="22px" Width="40px" BackColor="SeaGreen" ForeColor="#064F4D"
                    BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px" ToolTip="SeaGreen"
                    Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                    style="margin-top: 1px; text-align: left; vertical-align: top;" />
           </asp:TableCell>
           <asp:TableCell>
                <asp:Button ID="Vert10" runat="server" Height="22px" Width="40px" BackColor="PaleGreen" ForeColor="#064F4D"
                    BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px" ToolTip="PaleGreen"
                    Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                    style="margin-top: 1px; text-align: left; vertical-align: top;" />
           </asp:TableCell>
           <asp:TableCell>
                <asp:Button ID="Vert11" runat="server" Height="22px" Width="40px" BackColor="MediumSpringGreen" ForeColor="#064F4D"
                    BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px" ToolTip="MediumSpringGreen"
                    Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                    style="margin-top: 1px; text-align: left; vertical-align: top;" />
           </asp:TableCell>
           <asp:TableCell>
                <asp:Button ID="Vert12" runat="server" Height="22px" Width="40px" BackColor="MediumSeaGreen" ForeColor="#064F4D"
                    BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px" ToolTip="MediumSeaGreen"
                    Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                    style="margin-top: 1px; text-align: left; vertical-align: top;" />
           </asp:TableCell>
           <asp:TableCell>
                <asp:Button ID="Vert13" runat="server" Height="22px" Width="40px" BackColor="LimeGreen" ForeColor="#064F4D"
                    BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px" ToolTip="LimeGreen"
                    Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                    style="margin-top: 1px; text-align: left; vertical-align: top;" />
           </asp:TableCell>         
    </asp:TableRow>
     <asp:TableRow>
          <asp:TableCell>
                <asp:Button ID="Vert14" runat="server" Height="22px" Width="40px" BackColor="Lime" ForeColor="#064F4D"
                    BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px" ToolTip="Lime"
                    Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                    style="margin-left: 4px; margin-top: 1px; text-align: left; vertical-align: top;" />
           </asp:TableCell>
           <asp:TableCell>
                <asp:Button ID="Vert15" runat="server" Height="22px" Width="40px" BackColor="MediumAquamarine" ForeColor="#064F4D"
                    BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px" ToolTip="MediumAquamarine"
                    Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                    style="margin-top: 1px; text-align: left; vertical-align: top;" />
           </asp:TableCell>
           <asp:TableCell>
                <asp:Button ID="Vert16" runat="server" Height="22px" Width="40px" BackColor="LightGreen" ForeColor="#064F4D"
                    BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px" ToolTip="LightGreen"
                    Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                    style="margin-top: 1px; text-align: left; vertical-align: top;" />
           </asp:TableCell>
           <asp:TableCell>
                <asp:Button ID="Vert17" runat="server" Height="22px" Width="40px" BackColor="LightSeaGreen" ForeColor="#064F4D"
                    BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px" ToolTip="LightSeaGreen"
                    Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                    style="margin-top: 1px; text-align: left; vertical-align: top;" />
           </asp:TableCell>
           <asp:TableCell>
                <asp:Button ID="Vert18" runat="server" Height="22px" Width="40px" BackColor="AquaMarine" ForeColor="#064F4D"
                    BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px" ToolTip="AquaMarine"
                    Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                    style="margin-top: 1px; text-align: left; vertical-align: top;" />
           </asp:TableCell>
           <asp:TableCell>
                <asp:Button ID="Vert19" runat="server" Height="22px" Width="40px" BackColor="ForestGreen" ForeColor="#064F4D"
                    BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px" ToolTip="ForestGreen"
                    Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                    style="margin-top: 1px; text-align: left; vertical-align: top;" />
           </asp:TableCell>
           <asp:TableCell>
                <asp:Button ID="Vert20" runat="server" Height="22px" Width="40px" BackColor="OliveDrab" ForeColor="#064F4D"
                    BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px" ToolTip="OliveDrab"
                    Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                    style="margin-top: 1px; text-align: left; vertical-align: top;" />
           </asp:TableCell>         
    </asp:TableRow>
    <asp:TableRow>
          <asp:TableCell>
                <asp:Button ID="Bleu00" runat="server" Height="22px" Width="40px" BackColor="Aqua" ForeColor="#064F4D"
                    BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px" ToolTip="Aqua"
                    Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                    style="margin-left: 4px; margin-top: 1px; text-align: left; vertical-align: top;" />
           </asp:TableCell>
           <asp:TableCell>
                <asp:Button ID="Bleu01" runat="server" Height="22px" Width="40px" BackColor="Blue" ForeColor="#064F4D"
                    BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px" ToolTip="Blue"
                    Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                    style="margin-top: 1px; text-align: left; vertical-align: top;" />
           </asp:TableCell>
           <asp:TableCell>
                <asp:Button ID="Bleu02" runat="server" Height="22px" Width="40px" BackColor="RoyalBlue" ForeColor="#064F4D"
                    BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px" ToolTip="RoyalBlue"
                    Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                    style="margin-top: 1px; text-align: left; vertical-align: top;" />
           </asp:TableCell>
           <asp:TableCell>
                <asp:Button ID="Bleu03" runat="server" Height="22px" Width="40px" BackColor="DeepSkyBlue" ForeColor="#064F4D"
                    BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px" ToolTip="DeepSkyBlue"
                    Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                    style="margin-top: 1px; text-align: left; vertical-align: top;" />
           </asp:TableCell>
           <asp:TableCell>
                <asp:Button ID="Bleu04" runat="server" Height="22px" Width="40px" BackColor="LightBlue" ForeColor="#064F4D"
                    BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px" ToolTip="LightBlue"
                    Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                    style="margin-top: 1px; text-align: left; vertical-align: top;" />
           </asp:TableCell>
           <asp:TableCell>
                <asp:Button ID="Bleu05" runat="server" Height="22px" Width="40px" BackColor="LightSkyBlue" ForeColor="#064F4D"
                    BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px" ToolTip="LightSkyBlue"
                    Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                    style="margin-top: 1px; text-align: left; vertical-align: top;" />
           </asp:TableCell>
           <asp:TableCell>
                <asp:Button ID="Bleu06" runat="server" Height="22px" Width="40px" BackColor="MediumBlue" ForeColor="#064F4D"
                    BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px" ToolTip="MediumBlue"
                    Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                    style="margin-top: 1px; text-align: left; vertical-align: top;" />
           </asp:TableCell>         
    </asp:TableRow>
    <asp:TableRow>
          <asp:TableCell>
                <asp:Button ID="Bleu07" runat="server" Height="22px" Width="40px" BackColor="MediumTurquoise" ForeColor="#064F4D"
                    BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px" ToolTip="MediumTurquoise"
                    Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                    style="margin-left: 4px; margin-top: 1px; text-align: left; vertical-align: top;" />
           </asp:TableCell>
           <asp:TableCell>
                <asp:Button ID="Bleu08" runat="server" Height="22px" Width="40px" BackColor="Navy" ForeColor="#064F4D"
                    BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px" ToolTip="Navy"
                    Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                    style="margin-top: 1px; text-align: left; vertical-align: top;" />
           </asp:TableCell>
           <asp:TableCell>
                <asp:Button ID="Bleu09" runat="server" Height="22px" Width="40px" BackColor="PaleTurquoise" ForeColor="#064F4D"
                    BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px" ToolTip="PaleTurquoise"
                    Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                    style="margin-top: 1px; text-align: left; vertical-align: top;" />
           </asp:TableCell>
           <asp:TableCell>
                <asp:Button ID="Bleu10" runat="server" Height="22px" Width="40px" BackColor="MidnightBlue" ForeColor="#064F4D"
                    BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px" ToolTip="MidnightBlue"
                    Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                    style="margin-top: 1px; text-align: left; vertical-align: top;" />
           </asp:TableCell>
           <asp:TableCell>
                <asp:Button ID="Bleu11" runat="server" Height="22px" Width="40px" BackColor="SteelBlue" ForeColor="#064F4D"
                    BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px" ToolTip="SteelBlue"
                    Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                    style="margin-top: 1px; text-align: left; vertical-align: top;" />
           </asp:TableCell>
           <asp:TableCell>
                <asp:Button ID="Bleu12" runat="server" Height="22px" Width="40px" BackColor="Teal" ForeColor="#064F4D"
                    BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px" ToolTip="Teal"
                    Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                    style="margin-top: 1px; text-align: left; vertical-align: top;" />
           </asp:TableCell>
           <asp:TableCell>
                <asp:Button ID="Bleu13" runat="server" Height="22px" Width="40px" BackColor="Turquoise" ForeColor="#064F4D"
                    BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px" ToolTip="Turquoise"
                    Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                    style="margin-top: 1px; text-align: left; vertical-align: top;" />
           </asp:TableCell>         
    </asp:TableRow>
    <asp:TableRow>
          <asp:TableCell>
                <asp:Button ID="Bleu14" runat="server" Height="22px" Width="40px" BackColor="DodgerBlue" ForeColor="#064F4D"
                    BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px" ToolTip="DodgerBlue"
                    Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                    style="margin-left: 4px; margin-top: 1px; text-align: left; vertical-align: top;" />
           </asp:TableCell>
           <asp:TableCell>
                <asp:Button ID="Bleu15" runat="server" Height="22px" Width="40px" BackColor="PowderBlue" ForeColor="#064F4D"
                    BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px" ToolTip="PowderBlue"
                    Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                    style="margin-top: 1px; text-align: left; vertical-align: top;" />
           </asp:TableCell>
           <asp:TableCell>
                <asp:Button ID="Bleu16" runat="server" Height="22px" Width="40px" BackColor="CadetBlue" ForeColor="#064F4D"
                    BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px" ToolTip="CadetBlue"
                    Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                    style="margin-top: 1px; text-align: left; vertical-align: top;" />
           </asp:TableCell>
           <asp:TableCell>
                <asp:Button ID="Bleu17" runat="server" Height="22px" Width="40px" BackColor="DarkBlue" ForeColor="#064F4D"
                    BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px" ToolTip="DarkBlue"
                    Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                    style="margin-top: 1px; text-align: left; vertical-align: top;" />
           </asp:TableCell>
           <asp:TableCell>
                <asp:Button ID="Bleu18" runat="server" Height="22px" Width="40px" BackColor="CornflowerBlue" ForeColor="#064F4D"
                    BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px" ToolTip="CornflowerBlue"
                    Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                    style="margin-top: 1px; text-align: left; vertical-align: top;" />
           </asp:TableCell>
           <asp:TableCell>
                <asp:Button ID="Bleu19" runat="server" Height="22px" Width="40px" BackColor="SlateBlue" ForeColor="#064F4D"
                    BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px" ToolTip="SlateBlue"
                    Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                    style="margin-top: 1px; text-align: left; vertical-align: top;" />
           </asp:TableCell>
           <asp:TableCell>
                <asp:Button ID="Bleu20" runat="server" Height="22px" Width="40px" BackColor="DarkTurquoise" ForeColor="#064F4D"
                    BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px" ToolTip="DarkTurquoise"
                    Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                    style="margin-top: 1px; text-align: left; vertical-align: top;" />
           </asp:TableCell>         
    </asp:TableRow>
    <asp:TableRow>
          <asp:TableCell>
                <asp:Button ID="Gris00" runat="server" Height="22px" Width="40px" BackColor="Silver" ForeColor="#064F4D"
                    BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px" ToolTip="Silver"
                    Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                    style="margin-left: 4px; margin-top: 1px; text-align: left; vertical-align: top;" />
           </asp:TableCell>
           <asp:TableCell>
                <asp:Button ID="Gris01" runat="server" Height="22px" Width="40px" BackColor="LightGray" ForeColor="#064F4D"
                    BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px" ToolTip="LightGray"
                    Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                    style="margin-top: 1px; text-align: left; vertical-align: top;" />
           </asp:TableCell>
           <asp:TableCell>
                <asp:Button ID="Gris02" runat="server" Height="22px" Width="40px" BackColor="LightSlateGray" ForeColor="#064F4D"
                    BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px" ToolTip="LightSlateGray"
                    Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                    style="margin-top: 1px; text-align: left; vertical-align: top;" />
           </asp:TableCell>
           <asp:TableCell>
                <asp:Button ID="Gris03" runat="server" Height="22px" Width="40px" BackColor="MintCream" ForeColor="#064F4D"
                    BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px" ToolTip="MintCream"
                    Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                    style="margin-top: 1px; text-align: left; vertical-align: top;" />
           </asp:TableCell>
           <asp:TableCell>
                <asp:Button ID="Gris04" runat="server" Height="22px" Width="40px" BackColor="SeaShell" ForeColor="#064F4D"
                    BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px" ToolTip="SeaShell"
                    Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                    style="margin-top: 1px; text-align: left; vertical-align: top;" />
           </asp:TableCell>
           <asp:TableCell>
                <asp:Button ID="Gris05" runat="server" Height="22px" Width="40px" BackColor="SlateGray" ForeColor="#064F4D"
                    BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px" ToolTip="SlateGray"
                    Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                    style="margin-top: 1px; text-align: left; vertical-align: top;" />
           </asp:TableCell>
           <asp:TableCell>
                <asp:Button ID="Gris06" runat="server" Height="22px" Width="40px" BackColor="Snow" ForeColor="#064F4D"
                    BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px" ToolTip="Snow"
                    Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                    style="margin-top: 1px; text-align: left; vertical-align: top;" />
           </asp:TableCell>         
    </asp:TableRow>
    <asp:TableRow>
          <asp:TableCell>
                <asp:Button ID="Gris07" runat="server" Height="22px" Width="40px" BackColor="Wheat" ForeColor="#064F4D"
                    BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px" ToolTip="Wheat"
                    Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                    style="margin-left: 4px; margin-top: 1px; text-align: left; vertical-align: top;" />
           </asp:TableCell>
           <asp:TableCell>
                <asp:Button ID="Gris08" runat="server" Height="22px" Width="40px" BackColor="Gray" ForeColor="#064F4D"
                    BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px" ToolTip="Gray"
                    Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                    style="margin-top: 1px; text-align: left; vertical-align: top;" />
           </asp:TableCell>
           <asp:TableCell>
                <asp:Button ID="Gris09" runat="server" Height="22px" Width="40px" BackColor="DarkSlateGray" ForeColor="#064F4D"
                    BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px" ToolTip="DarkSlateGray"
                    Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                    style="margin-top: 1px; text-align: left; vertical-align: top;" />
           </asp:TableCell>
           <asp:TableCell>
                <asp:Button ID="Gris10" runat="server" Height="22px" Width="40px" BackColor="Thistle" ForeColor="#064F4D"
                    BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px" ToolTip="Thistle"
                    Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                    style="margin-top: 1px; text-align: left; vertical-align: top;" />
           </asp:TableCell>
           <asp:TableCell>
                <asp:Button ID="Gris11" runat="server" Height="22px" Width="40px" BackColor="DarkGray" ForeColor="#064F4D"
                    BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px" ToolTip="DarkGray"
                    Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                    style="margin-top: 1px; text-align: left; vertical-align: top;" />
           </asp:TableCell>
           <asp:TableCell>
                <asp:Button ID="Gris12" runat="server" Height="22px" Width="40px" BackColor="DimGray" ForeColor="#064F4D"
                    BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px" ToolTip="DimGray"
                    Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                    style="margin-top: 1px; text-align: left; vertical-align: top;" />
           </asp:TableCell>
           <asp:TableCell>
                <asp:Button ID="Gris13" runat="server" Height="22px" Width="40px" BackColor="Plum" ForeColor="#064F4D"
                    BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px" ToolTip="Plum"
                    Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                    style="margin-top: 1px; text-align: left; vertical-align: top;" />
           </asp:TableCell>         
    </asp:TableRow>
    <asp:TableRow>
          <asp:TableCell>
                <asp:Button ID="Gris14" runat="server" Height="22px" Width="40px" BackColor="BlueViolet" ForeColor="#064F4D"
                    BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px" ToolTip="BlueViolet"
                    Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                    style="margin-left: 4px; margin-top: 1px; text-align: left; vertical-align: top;" />
           </asp:TableCell>
           <asp:TableCell>
                <asp:Button ID="Gris15" runat="server" Height="22px" Width="40px" BackColor="DarkSlateBlue" ForeColor="#064F4D"
                    BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px" ToolTip="DarkSlateBlue"
                    Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                    style="margin-top: 1px; text-align: left; vertical-align: top;" />
           </asp:TableCell>
           <asp:TableCell>
                <asp:Button ID="Gris16" runat="server" Height="22px" Width="40px" BackColor="Gainsboro" ForeColor="#064F4D"
                    BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px" ToolTip="Gainsboro"
                    Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                    style="margin-top: 1px; text-align: left; vertical-align: top;" />
           </asp:TableCell>
           <asp:TableCell>
                <asp:Button ID="Gris17" runat="server" Height="22px" Width="40px" BackColor="Purple" ForeColor="#064F4D"
                    BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px" ToolTip="Purple"
                    Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                    style="margin-top: 1px; text-align: left; vertical-align: top;" />
           </asp:TableCell>
           <asp:TableCell>
                <asp:Button ID="Gris18" runat="server" Height="22px" Width="40px" BackColor="Lavender" ForeColor="#064F4D"
                    BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px" ToolTip="Lavender"
                    Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                    style="margin-top: 1px; text-align: left; vertical-align: top;" />
           </asp:TableCell>
           <asp:TableCell>
                <asp:Button ID="Gris19" runat="server" Height="22px" Width="40px" BackColor="Indigo" ForeColor="#064F4D"
                    BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px" ToolTip="Indigo"
                    Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                    style="margin-top: 1px; text-align: left; vertical-align: top;" />
           </asp:TableCell>
           <asp:TableCell>
                <asp:Button ID="Gris20" runat="server" Height="22px" Width="40px" BackColor="DarkOrchid" ForeColor="#064F4D"
                    BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px" ToolTip="DarkOrchid"
                    Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                    style="margin-top: 1px; text-align: left; vertical-align: top;" />
           </asp:TableCell>         
    </asp:TableRow>
</asp:Table>
