﻿Option Strict On
Option Explicit On
Option Compare Text
Partial Class Controles_VPaletteCouleurs
    Inherits System.Web.UI.UserControl
    Public Delegate Sub Valeur_ChangeEventHandler(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.DonneeChangeEventArgs)
    Public Event ValeurChange As Valeur_ChangeEventHandler

    Protected Overridable Sub Valeur_Change(ByVal e As Virtualia.Systeme.Evenements.DonneeChangeEventArgs)
        RaiseEvent ValeurChange(Me, e)
    End Sub

    Protected Sub BoutonCouleur_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Marron00.Click, _
    Marron01.Click, Marron02.Click, Marron03.Click, Marron04.Click, Marron05.Click, Marron06.Click, _
    Bleu00.Click, Bleu01.Click, Bleu02.Click, Bleu03.Click, Bleu04.Click, Bleu05.Click, Bleu06.Click, _
    Bleu07.Click, Bleu08.Click, Bleu09.Click, Bleu10.Click, Bleu11.Click, Bleu12.Click, Bleu13.Click, _
    Bleu14.Click, Bleu15.Click, Bleu16.Click, Bleu17.Click, Bleu18.Click, Bleu19.Click, Bleu20.Click, _
    Gold00.Click, Gold01.Click, Gold02.Click, Gold03.Click, Gold04.Click, Gold05.Click, Gold06.Click, _
    Gris00.Click, Gris01.Click, Gris02.Click, Gris03.Click, Gris04.Click, Gris05.Click, Gris06.Click, _
    Gris07.Click, Gris08.Click, Gris09.Click, Gris10.Click, Gris11.Click, Gris12.Click, Gris13.Click, _
    Gris14.Click, Gris15.Click, Gris16.Click, Gris17.Click, Gris18.Click, Gris19.Click, Gris20.Click, _
    Jaune00.Click, Jaune01.Click, Jaune02.Click, Jaune03.Click, Jaune04.Click, Jaune05.Click, Jaune06.Click, _
    Orange00.Click, Orange01.Click, Orange02.Click, Orange03.Click, Orange04.Click, Orange05.Click, Orange06.Click, _
    Rose00.Click, Rose01.Click, Rose02.Click, Rose03.Click, Rose04.Click, Rose05.Click, Rose06.Click, _
    Rouge00.Click, Rouge01.Click, Rouge02.Click, Rouge03.Click, Rouge04.Click, Rouge05.Click, Rouge06.Click, _
    Vert00.Click, Vert01.Click, Vert02.Click, Vert03.Click, Vert04.Click, Vert05.Click, Vert06.Click, _
    Vert07.Click, Vert08.Click, Vert09.Click, Vert10.Click, Vert11.Click, Vert12.Click, Vert13.Click, _
    Vert14.Click, Vert15.Click, Vert16.Click, Vert17.Click, Vert18.Click, Vert19.Click, Vert20.Click

        EtiCouleur.BackColor = CType(sender, System.Web.UI.WebControls.Button).BackColor

        Dim Rouge As String
        Dim Vert As String
        Dim Bleu As String
        Dim Res As String

        Rouge = Hex(CType(sender, System.Web.UI.WebControls.Button).BackColor.R).ToString
        If Rouge = "0" Then Rouge = "00"
        Vert = Hex(CType(sender, System.Web.UI.WebControls.Button).BackColor.G).ToString
        If Vert = "0" Then Vert = "00"
        Bleu = Hex(CType(sender, System.Web.UI.WebControls.Button).BackColor.B).ToString
        If Bleu = "0" Then Bleu = "00"

        Res = "#" & Rouge & Vert & Bleu

        Dim Evenement As Virtualia.Systeme.Evenements.DonneeChangeEventArgs
        Evenement = New Virtualia.Systeme.Evenements.DonneeChangeEventArgs("Couleur", Res)
        Valeur_Change(Evenement)
    End Sub
End Class
