﻿'------------------------------------------------------------------------------
' <généré automatiquement>
'     Ce code a été généré par un outil.
'
'     Les modifications apportées à ce fichier peuvent provoquer un comportement incorrect et seront perdues si
'     le code est régénéré.
' </généré automatiquement>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class VirtualiaOutils_RechercheCMC

    '''<summary>
    '''Contrôle CMC.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CMC As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle VDictionnaire.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VDictionnaire As Global.Virtualia.Net.Controles_Dictionnaire

    '''<summary>
    '''Contrôle CMCScript.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CMCScript As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle VScriptCMC.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VScriptCMC As Global.Virtualia.Net.Controles_ScriptDictionnaire

    '''<summary>
    '''Contrôle CadreResultat.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CadreResultat As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle EtiAlbum.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiAlbum As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle Cmd20.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Cmd20 As Global.System.Web.UI.WebControls.ImageButton

    '''<summary>
    '''Contrôle Cmd21.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Cmd21 As Global.System.Web.UI.WebControls.ImageButton

    '''<summary>
    '''Contrôle Cmd22.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Cmd22 As Global.System.Web.UI.WebControls.ImageButton

    '''<summary>
    '''Contrôle ListeResultat.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents ListeResultat As Global.System.Web.UI.WebControls.ListBox

    '''<summary>
    '''Contrôle EtiNBResultat.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiNBResultat As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle EtiPanel.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiPanel As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle Cmd30.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Cmd30 As Global.System.Web.UI.WebControls.ImageButton

    '''<summary>
    '''Contrôle ListePanel.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents ListePanel As Global.System.Web.UI.WebControls.ListBox

    '''<summary>
    '''Contrôle EtiNBPanel.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiNBPanel As Global.System.Web.UI.WebControls.Label
End Class
