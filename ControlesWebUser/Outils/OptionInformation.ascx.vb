﻿Option Strict On
Option Explicit On
Option Compare Text
Imports VI = Virtualia.Systeme.Constantes
Partial Class VirtualiaOutils_OptionInformation
    Inherits System.Web.UI.UserControl
    Public Delegate Sub EventHandler(ByVal sender As Object, ByVal e As EventArgs)
    Public Event RetourEventHandler As EventHandler
    Public Delegate Sub AppelChoixEventHandler(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.AppelChoixEventArgs)
    Public Event AppelChoix As AppelChoixEventHandler
    Private WebFct As Virtualia.Net.WebFonctions
    Private WsNomState As String = "Voption"

    Protected Overridable Sub Appel_Choix(ByVal e As Virtualia.Systeme.Evenements.AppelChoixEventArgs)
        RaiseEvent AppelChoix(Me, e)
    End Sub

    Public WriteOnly Property V_Appel() As ArrayList
        Set(ByVal value As ArrayList)
            If value Is Nothing Then
                Exit Property
            End If
            Dim Pvue As Short = CShort(value(0))
            Dim NoObjet As Short = CShort(value(1))
            Dim NoInfo As Short = CShort(value(2))
            Dim NatDonneeNum As Short
            Dim NomCol As String = value(5).ToString
            Dim LgCol As Integer = CInt(value(6))
            Dim NbCol As Integer = CInt(value(7))
            Dim FormatCol As Integer = CInt(value(8))

            Select Case NoInfo
                Case Is < 500
                    EtiAppelant.Text = WebFct.PointeurDicoInfo(Pvue, NoObjet, NoInfo).Intitule
                    NatDonneeNum = WebFct.PointeurDicoInfo(Pvue, NoObjet, NoInfo).VNature
                    If NomCol = "" Then
                        Intitule.DonText = EtiAppelant.Text
                        NomCol = EtiAppelant.Text
                    Else
                        Intitule.DonText = NomCol
                    End If
                    If LgCol = 0 Then
                        Longueur.DonText = ""
                    Else
                        Longueur.DonText = LgCol.ToString
                    End If
                Case Is > 9000
                    Intitule.DonText = NomCol
                Case Else
                    EtiAppelant.Text = WebFct.PointeurDicoExperte(Pvue, NoObjet, NoInfo).Intitule
                    NatDonneeNum = WebFct.PointeurDicoExperte(Pvue, NoObjet, NoInfo).VNature
                    If NomCol = "" Then
                        Intitule.DonText = EtiAppelant.Text
                    Else
                        Intitule.DonText = NomCol
                    End If
                    If LgCol > 0 Then
                        Longueur.DonText = LgCol.ToString
                    End If
            End Select
            If NbCol > 0 Then
                Nombre.DonText = NbCol.ToString
            Else
                Nombre.DonText = ""
            End If
            ListeFormats.Items.Clear()
            Dim I As Integer
            Dim LibelNat As String = ""
            Dim Chaine As String
            Select Case NatDonneeNum
                Case VI.NatureDonnee.DonneeDate
                    LibelNat = "Date"
                Case VI.NatureDonnee.DonneeNumerique
                    LibelNat = "Numeric"
                Case Else
                    LibelNat = "Alpha"
            End Select
            I = 0
            Do
                Chaine = WebFct.ViRhFonction.OptionInformation(LibelNat, I)
                If Chaine = "" Then Exit Do
                ListeFormats.Items.Add(Chaine)
                I += 1
            Loop
            Chaine = WebFct.ViRhFonction.OptionInformation(LibelNat, FormatCol)
            If Chaine <> "" Then
                ListeFormats.Text = Chaine
            End If
            If Me.ViewState(WsNomState) IsNot Nothing Then
                Me.ViewState.Remove(WsNomState)
            End If
            value(5) = NomCol
            value(6) = LgCol
            value(7) = NbCol
            value(8) = FormatCol
            Me.ViewState.Add(WsNomState, value)
        End Set
    End Property

    Protected Sub Cmd01_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Cmd01.Click
        Dim VListe As New ArrayList
        Dim Evenement As Virtualia.Systeme.Evenements.AppelChoixEventArgs

        If Me.ViewState(WsNomState) Is Nothing Then
            Exit Sub
        End If
        VListe = CType(Me.ViewState(WsNomState), ArrayList)

        Evenement = New Virtualia.Systeme.Evenements.AppelChoixEventArgs(VListe)
        Appel_Choix(Evenement)
    End Sub

    Protected Sub Intitule_ValeurChange(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.DonneeChangeEventArgs) Handles Intitule.ValeurChange
        If Me.ViewState(WsNomState) Is Nothing Then
            Exit Sub
        End If
        Dim VListe As ArrayList
        VListe = CType(Me.ViewState(WsNomState), ArrayList)
        VListe(5) = e.Valeur
        Me.ViewState.Remove(WsNomState)
        Me.ViewState.Add(WsNomState, VListe)
    End Sub

    Protected Sub Longueur_ValeurChange(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.DonneeChangeEventArgs) Handles Longueur.ValeurChange
        If Me.ViewState(WsNomState) Is Nothing Then
            Exit Sub
        End If
        Dim VListe As ArrayList
        VListe = CType(Me.ViewState(WsNomState), ArrayList)
        VListe(6) = e.Valeur
        Me.ViewState.Remove(WsNomState)
        Me.ViewState.Add(WsNomState, VListe)
    End Sub

    Protected Sub Nombre_ValeurChange(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.DonneeChangeEventArgs) Handles Nombre.ValeurChange
        If Me.ViewState(WsNomState) Is Nothing Then
            Exit Sub
        End If
        Dim VListe As ArrayList
        VListe = CType(Me.ViewState(WsNomState), ArrayList)
        VListe(7) = e.Valeur
        Me.ViewState.Remove(WsNomState)
        Me.ViewState.Add(WsNomState, VListe)
    End Sub

    Protected Sub ListeFormats_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ListeFormats.SelectedIndexChanged
        If Me.ViewState(WsNomState) Is Nothing Then
            Exit Sub
        End If
        Dim VListe As ArrayList
        VListe = CType(Me.ViewState(WsNomState), ArrayList)
        VListe(8) = ListeFormats.SelectedIndex
        Me.ViewState.Remove(WsNomState)
        Me.ViewState.Add(WsNomState, VListe)
    End Sub

    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        WebFct = New Virtualia.Net.WebFonctions(Me, 0)
    End Sub

    Protected Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
        WebFct.Dispose()
    End Sub

End Class
