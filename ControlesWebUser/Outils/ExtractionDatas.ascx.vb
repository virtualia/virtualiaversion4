﻿Option Strict On
Option Explicit On
Option Compare Text
Imports VI = Virtualia.Systeme.Constantes
Partial Class VirtualiaOutils_ExtractionDatas
    Inherits System.Web.UI.UserControl
    Public Delegate Sub AppelChoixEventHandler(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.AppelChoixEventArgs)
    Public Event AppelChoix As AppelChoixEventHandler
    Public Delegate Sub ScriptResultatEventHandler(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.ScriptResultatEventArgs)
    Public Event ScriptResultat As ScriptResultatEventHandler
    Public Delegate Sub AppelTableEventHandler(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.AppelTableEventArgs)
    Public Event AppelTable As AppelTableEventHandler
    Private WebFct As Virtualia.Net.WebFonctions
    Private WsOutil As Short = VI.OptionInfo.DicoExport
    '** Exécution
    Private WsPointdeVue As Short
    Private WsListeRequetes As ArrayList 'Liste des requetes effectues = ObjetSelection
    Private WsTabIde() As Integer 'Préselection d'identifiants
    Private WsListeRes As List(Of Virtualia.Ressources.Datas.ItemSelection)
    Private WsTabObjet As ArrayList
    Private WsTabColonnes As ArrayList
    Private WsObjetHisto As Short

    Protected Overridable Sub Appel_Choix(ByVal e As Virtualia.Systeme.Evenements.AppelChoixEventArgs)
        RaiseEvent AppelChoix(Me, e)
    End Sub

    Protected Overridable Sub Appel_Table(ByVal e As Virtualia.Systeme.Evenements.AppelTableEventArgs)
        RaiseEvent AppelTable(Me, e)
    End Sub

    Public WriteOnly Property Dontab_RetourAppelTable(ByVal IDAppelant As String) As String
        Set(ByVal value As String)
            VScriptExtraction.Dontab_RetourAppelTable(IDAppelant) = value
        End Set
    End Property

    Protected Overridable Sub Resultat_Script(ByVal e As Virtualia.Systeme.Evenements.ScriptResultatEventArgs)
        RaiseEvent ScriptResultat(Me, e)
    End Sub

    Public WriteOnly Property ListeChoix(ByVal Numligne As Integer) As ArrayList
        Set(ByVal value As ArrayList)
            If value IsNot Nothing Then
                VScriptExtraction.AjouterNoeud(Numligne) = value
            End If
        End Set
    End Property

    Public ReadOnly Property IntituleScript() As String
        Get
            Return VScriptExtraction.VTitre
        End Get
    End Property

    Protected Sub VDictionnaire_Dictionnaire_Click(ByVal sender As Object, ByVal e As DictionnaireEventArgs) Handles VDictionnaire.Dictionnaire_Click
        Dim VListe As New ArrayList

        VListe.Add(e.PointdeVue)
        VListe.Add(e.NumeroObjet)
        VListe.Add(e.NumeroInfo)
        VListe.Add(0)
        VListe.Add(0)
        Select Case ModeW.Value
            Case Is = "0"
                Select Case e.NumeroInfo
                    Case Is < 500
                        VListe.Add(WebFct.PointeurDicoInfo(e.PointdeVue, e.NumeroObjet, e.NumeroInfo).Intitule) 'Intitulé colonne
                    Case Is > 9000
                        VListe.Add("Constante")
                    Case Else
                        VListe.Add(WebFct.PointeurDicoExperte(e.PointdeVue, e.NumeroObjet, e.NumeroInfo).Intitule) 'Intitulé colonne
                End Select
                VListe.Add(0)
                VListe.Add(0)
                VListe.Add(0)
                VScriptExtraction.AjouterNoeud(0) = VListe
            Case Else
                VListe.Add("") 'Intitulé colonne
                VListe.Add(0)
                VListe.Add(0)
                VListe.Add(0)
                Dim Evenement As Virtualia.Systeme.Evenements.AppelChoixEventArgs
                Evenement = New Virtualia.Systeme.Evenements.AppelChoixEventArgs(VListe)
                Appel_Choix(Evenement)
        End Select

    End Sub

    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        WebFct = New Virtualia.Net.WebFonctions(Me, 0)
    End Sub

    Protected Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
        WebFct.Dispose()
    End Sub

    Protected Sub VScriptExtraction_AppelChoix(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.AppelChoixEventArgs) Handles VScriptExtraction.AppelChoix
        Dim Evenement As Virtualia.Systeme.Evenements.AppelChoixEventArgs
        Evenement = New Virtualia.Systeme.Evenements.AppelChoixEventArgs(e.IndiceLigne, e.Valeurs, e.Appelant)
        Appel_Choix(Evenement)
    End Sub

    Protected Sub VScriptExtraction_AppelTable(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.AppelTableEventArgs) Handles VScriptExtraction.AppelTable
        Dim Evenement As Virtualia.Systeme.Evenements.AppelTableEventArgs
        Evenement = New Virtualia.Systeme.Evenements.AppelTableEventArgs(e.ControleAppelant, e.ObjetAppelant, e.PointdeVueInverse, e.NomdelaTable)
        Appel_Table(Evenement)
    End Sub

    Protected Sub VScriptExtraction_ExecutionScript(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.ScriptExecutionEventArgs) Handles VScriptExtraction.ExecutionScript
        WsPointdeVue = e.VPointdeVue
        Dim ScriptExe As Virtualia.Net.Script.ExecutionExtraction
        Dim K As Integer = 0

        ScriptExe = New Virtualia.Net.Script.ExecutionExtraction(WebFct.PointeurUtilisateur, e.VPointdeVue)
        K = ScriptExe.Executer(e.VConditions, e.VScript)
        If K > 0 Then
            Dim Evenement As Virtualia.Systeme.Evenements.ScriptResultatEventArgs
            Dim LibCols As String = ScriptExe.LibelleColonnes

            Evenement = New Virtualia.Systeme.Evenements.ScriptResultatEventArgs("", ScriptExe.LibelleColonnes, ScriptExe.Resultat, ScriptExe.CentrageColonne)
            Resultat_Script(Evenement)
        End If

        ScriptExe.Dispose()

    End Sub

    Protected Sub VScriptExtraction_ValeurChange(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.DonneeChangeEventArgs) Handles VScriptExtraction.ValeurChange
        Dim Tableau(0) As String
        Tableau = Strings.Split(e.Valeur, VI.PointVirgule, -1)
        ModeW.Value = Tableau(0)
    End Sub

End Class
