﻿Option Strict On
Option Explicit On
Option Compare Text
Imports VI = Virtualia.Systeme.Constantes
Partial Class VirtualiaOutils_RechercheCMC
    Inherits System.Web.UI.UserControl
    Public Delegate Sub AppelChoixEventHandler(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.AppelChoixEventArgs)
    Public Event AppelChoix As AppelChoixEventHandler
    Public Delegate Sub AppelTableEventHandler(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.AppelTableEventArgs)
    Public Event AppelTable As AppelTableEventHandler
    Private WebFct As Virtualia.Net.WebFonctions
    Private WsOutil As Short = VI.OptionInfo.DicoCmc
   
    Protected Overridable Sub Appel_Choix(ByVal e As Virtualia.Systeme.Evenements.AppelChoixEventArgs)
        RaiseEvent AppelChoix(Me, e)
    End Sub

    Protected Overridable Sub Appel_Table(ByVal e As Virtualia.Systeme.Evenements.AppelTableEventArgs)
        RaiseEvent AppelTable(Me, e)
    End Sub

    Public WriteOnly Property Dontab_RetourAppelTable(ByVal IDAppelant As String) As String
        Set(ByVal value As String)
            VScriptCMC.Dontab_RetourAppelTable(IDAppelant) = value
        End Set
    End Property

    Public WriteOnly Property ListeChoix(ByVal NumLigne As Integer) As ArrayList
        Set(ByVal value As ArrayList)
            If value IsNot Nothing Then
                VScriptCMC.AjouterNoeud(NumLigne) = value
            End If
        End Set
    End Property

    Protected Sub VDictionnaire_Dictionnaire_Click(ByVal sender As Object, ByVal e As DictionnaireEventArgs) Handles VDictionnaire.Dictionnaire_Click
        Dim VListe As New ArrayList
        Dim Evenement As Virtualia.Systeme.Evenements.AppelChoixEventArgs

        VListe.Add(e.PointdeVue)
        VListe.Add(e.NumeroObjet)
        VListe.Add(e.NumeroInfo)
        VListe.Add(VI.Operateurs.Egalite)
        If e.NumeroInfo < 500 Then
            Select Case WebFct.PointeurDicoInfo(e.PointdeVue, e.NumeroObjet, e.NumeroInfo).VNature
                Case VI.NatureDonnee.DonneeDate, VI.NatureDonnee.DonneeNumerique
                    VListe.Add(VI.Operateurs.Inclu)
                Case Else
                    VListe.Add(VI.Operateurs.Egalite)
            End Select
        Else
            VListe.Add(VI.Operateurs.Egalite)
        End If

        Evenement = New Virtualia.Systeme.Evenements.AppelChoixEventArgs(VListe)
        Appel_Choix(Evenement)

    End Sub

    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        WebFct = New Virtualia.Net.WebFonctions(Me, 0)
    End Sub

    Protected Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
        WebFct.Dispose()
    End Sub

    Protected Sub VScriptCMC_AppelChoix(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.AppelChoixEventArgs) Handles VScriptCMC.AppelChoix
        Dim Evenement As Virtualia.Systeme.Evenements.AppelChoixEventArgs
        Evenement = New Virtualia.Systeme.Evenements.AppelChoixEventArgs(e.IndiceLigne, e.Valeurs)
        Appel_Choix(Evenement)
    End Sub

    Protected Sub VScriptCMC_AppelTable(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.AppelTableEventArgs) Handles VScriptCMC.AppelTable
        Dim Evenement As Virtualia.Systeme.Evenements.AppelTableEventArgs
        Evenement = New Virtualia.Systeme.Evenements.AppelTableEventArgs(e.ControleAppelant, e.ObjetAppelant, e.PointdeVueInverse, e.NomdelaTable)
        Appel_Table(Evenement)
    End Sub

    Protected Sub VScriptCMC_ValeurChange(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.DonneeChangeEventArgs) Handles VScriptCMC.ValeurChange
        Select Case e.Valeur
            Case Is = "Nouveau"
                ListeResultat.Items.Clear()
                EtiNBResultat.Text = ""
        End Select
    End Sub

    Protected Sub Cmd20_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Cmd20.Click
        Dim Requeteur As Virtualia.Net.Datas.ObjetSelection
        'Placer le résultat de la recherche dans le panel
        ListePanel.Items.Clear()
        Requeteur = WebFct.PointeurUtilisateur.PointeurResultatCMC(VI.PointdeVue.PVueApplicatif)
        If Requeteur Is Nothing Then
            Exit Sub
        End If
        WebFct.PointeurContexte.InitialiserPanel()

        Dim FicheRes As Virtualia.Ressources.Datas.ItemSelection
        Dim ResTrie = New List(Of Virtualia.Ressources.Datas.ItemSelection)
        Dim Cpt As Integer = 0

        ResTrie = (From instance In Requeteur.ListeResultat Select instance Where instance.Identifiant > 0 Order By instance.CritereTri Ascending).ToList
        If ResTrie.Count = 0 Then
            Exit Sub
        End If
        Dim IndiceC As IEnumerator = ResTrie.GetEnumerator
        Dim CloneFiche As Virtualia.Ressources.Datas.ItemSelection
        Dim I As Integer
        While IndiceC.MoveNext
            FicheRes = CType(IndiceC.Current, Virtualia.Ressources.Datas.ItemSelection)
            If FicheRes.CritereTri <> "" Then
                CloneFiche = New Virtualia.Ressources.Datas.ItemSelection(FicheRes.Identifiant)
                For I = 0 To FicheRes.NombredeChamps - 1
                    CloneFiche.ChampExtrait(0) = FicheRes.ChampExtrait(I)
                Next I
                CloneFiche.CritereTri = FicheRes.CritereTri
                WebFct.PointeurContexte.ListePanel.Add(CloneFiche)
                ListePanel.Items.Add(FicheRes.CritereTri)
                Cpt += 1
            End If
        End While
        EtiNBPanel.Text = Cpt.ToString
    End Sub

    Protected Sub CmdPanel(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Cmd21.Click, Cmd22.Click
        Dim Requeteur As Virtualia.Net.Datas.ObjetSelection
        Dim FicheRes As Virtualia.Ressources.Datas.ItemSelection
        Dim FichePanel As Virtualia.Ressources.Datas.ItemSelection
        Dim SiOK As Boolean
        Dim ResTrie = New List(Of Virtualia.Ressources.Datas.ItemSelection)
        Dim I As Integer

        If WebFct.PointeurContexte.ListePanel Is Nothing Then
            WebFct.PointeurContexte.InitialiserPanel()
        End If
        'Ajouter ou Retrancher au Panel
        Select Case CType(sender, ImageButton).ID
            Case Is = "Cmd21" 'Ajouter
                SiOK = False
            Case Else
                SiOK = True  'Retrancher
        End Select
        Requeteur = WebFct.PointeurUtilisateur.PointeurResultatCMC(VI.PointdeVue.PVueApplicatif)

        ResTrie = (From instance In Requeteur.ListeResultat Select instance Where instance.Identifiant > 0 Order By instance.CritereTri Ascending).ToList
        If ResTrie.Count = 0 Then
            Exit Sub
        End If

        Dim IndiceC As IEnumerator = ResTrie.GetEnumerator
        While IndiceC.MoveNext
            FicheRes = CType(IndiceC.Current, Virtualia.Ressources.Datas.ItemSelection)
            If SiDansLePanel(FicheRes.Identifiant) = SiOK Then
                Select Case SiOK
                    Case False
                        FichePanel = New Virtualia.Ressources.Datas.ItemSelection(FicheRes.Identifiant)
                        For I = 0 To FicheRes.NombredeChamps - 1
                            FichePanel.ChampExtrait(0) = FicheRes.ChampExtrait(I)
                        Next I
                        FichePanel.CritereTri = FicheRes.CritereTri
                        WebFct.PointeurContexte.ListePanel.Add(FichePanel)
                    Case True
                        Try
                            FichePanel = WebFct.PointeurContexte.ListePanel.Find(Function(Recherche) Recherche.Identifiant = FicheRes.Identifiant)
                            If FichePanel IsNot Nothing Then
                                WebFct.PointeurContexte.ListePanel.Remove(FichePanel)
                            End If
                        Catch ex As Exception
                            Exit Try
                        End Try
                End Select
            End If
        End While
        ListePanel.Items.Clear()
        If WebFct.PointeurContexte.ListePanel.Count = 0 Then
            Exit Sub
        End If

        ResTrie = New List(Of Virtualia.Ressources.Datas.ItemSelection)
        ResTrie = (From instance In WebFct.PointeurContexte.ListePanel Select instance Where instance.Identifiant > 0 Order By instance.CritereTri Ascending).ToList
        If ResTrie.Count = 0 Then
            Exit Sub
        End If

        Dim IndiceD As IEnumerator = ResTrie.GetEnumerator
        While IndiceD.MoveNext
            FicheRes = CType(IndiceD.Current, Virtualia.Ressources.Datas.ItemSelection)
            If FicheRes.CritereTri <> "" Then
                ListePanel.Items.Add(FicheRes.CritereTri)
            End If
        End While
        EtiNBPanel.Text = ListePanel.Items.Count.ToString

    End Sub

    Private Function SiDansLePanel(ByVal Ide As Integer) As Boolean
        If WebFct.PointeurContexte.ListePanel Is Nothing Then
            Return False
        End If
        If WebFct.PointeurContexte.ListePanel.Count = 0 Then
            Return False
        End If
        If WebFct.PointeurContexte.ListePanel.Find(Function(Recherche) Recherche.Identifiant = Ide) Is Nothing Then
            Return False
        End If
        Return True
    End Function

    Protected Sub VScriptCMC_ExecutionScript(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.ScriptExecutionEventArgs) Handles VScriptCMC.ExecutionScript
        If WebFct.PointeurArmoire Is Nothing Then
            Exit Sub
        End If
        Dim ScriptExe As Virtualia.Net.Script.ExecutionCMC
        Dim K As Integer = 0

        ListeResultat.Items.Clear()
        EtiNBResultat.Text = ""

        ScriptExe = New Virtualia.Net.Script.ExecutionCMC(WebFct.PointeurUtilisateur, e.VPointdeVue, False)
        ScriptExe.TableIdentifiants = WebFct.PointeurArmoire.TabIdentifiants
        K = ScriptExe.Executer(e.VConditions, e.VScript)

        If K > 0 Then
            Dim I As Integer
            For I = 0 To K - 1
                ListeResultat.Items.Add(ScriptExe.Resultat(I))
            Next I
            EtiNBResultat.Text = K.ToString
        End If
        ScriptExe.Dispose()
    End Sub

    Protected Sub Cmd30_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Cmd30.Click
        'Initialiser le panel
        ListePanel.Items.Clear()
        WebFct.PointeurContexte.InitialiserPanel()
        EtiNBPanel.Text = ""
    End Sub

End Class
