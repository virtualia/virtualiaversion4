﻿<%@ Control Language="VB" AutoEventWireup="false" Inherits="Virtualia.Net.VirtualiaOutils_RechercheCMC" Codebehind="RechercheCMC.ascx.vb" %>

<%@ Register src="../../ControlesWebUser/ObjetsArmoire/Dictionnaire.ascx" tagname="Dictionnaire" tagprefix="Virtualia" %>
<%@ Register src="../../ControlesWebUser/ObjetsArmoire/ScriptDictionnaire.ascx" tagname="ScriptDictionnaire" tagprefix="Virtualia" %>

<asp:Table ID="CMC" runat="server" Height="700px" Width="1150px" CellPadding="0" CellSpacing="0">
    <asp:TableRow VerticalAlign="Top">
        <asp:TableCell Width="10px"></asp:TableCell>
        <asp:TableCell Width="340px">
                <Virtualia:Dictionnaire ID="VDictionnaire" runat="server" V_PointdeVue="1"  V_Outil="3" />
        </asp:TableCell>
        <asp:TableCell Width="10px"></asp:TableCell>
        <asp:TableCell Width="780px">
            <asp:Table ID="CMCScript" runat="server">
                <asp:TableRow VerticalAlign="Top">
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:ScriptDictionnaire ID="VScriptCMC" runat="server" 
                         V_PointdeVue="1" V_Outil="3"/>
                    </asp:TableCell>
                    <asp:TableCell Width="300px">
                        <asp:Table ID="CadreResultat" runat="server" BackColor="#225C59" Width="285px"
                                        Height="25px" HorizontalAlign="Center" style="text-align: center">
                            <asp:TableRow>
                                <asp:TableCell>
                                    <asp:Label ID="EtiAlbum" runat="server" BackColor="Transparent" ForeColor="White"
                                        Text="Résultat" Width="60px" Font-Italic="true"
                                        ToolTip="Résultat de la recherche" >
                                    </asp:Label>    
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Left">
                                    <asp:ImageButton ID="Cmd20" runat="server" BorderStyle="None" 
                                        ImageAlign="Middle" ImageUrl="~/Images/Menus/ArmoireJaune_Outil.bmp" 
                                        ToolTip="Placer le résultat de la recherche dans le panel" /> 
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Left">
                                    <asp:ImageButton ID="Cmd21" runat="server" BorderStyle="None" 
                                        ImageAlign="Middle" ImageUrl="~/Images/Icones/AddTable_Outil.bmp" 
                                        ToolTip="Ajouter le résultat de la recherche au panel" /> 
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Left">
                                    <asp:ImageButton ID="Cmd22" runat="server" BorderStyle="None" 
                                        ImageAlign="Middle" ImageUrl="~/Images/Icones/SoustraireTable_Outil.bmp" 
                                        ToolTip="Soustraire le résultat de la recherche du panel" /> 
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell ColumnSpan="4">
                                    <div style="width:300px; height: 500px; border: outset 1px #CAEBE4; background-color: Transparent;
                                        display: table-cell; overflow: scroll" >
                                        <asp:ListBox ID="ListeResultat" runat="server" Width="300px" Height="500px"
                                             BackColor="Snow" style="margin-top: 0px;text-align: left" ForeColor="#124545" />
                                    </div>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell ColumnSpan="4">
                                    <asp:Label ID="EtiNBResultat" runat="server" BackColor="Transparent" ForeColor="#E9FDF9"
                                        Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Small" Font-Italic="true"
                                        Text="Aucun dossier" Width="300px" Height="18px">
                                    </asp:Label>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell ColumnSpan="4" BackColor="Snow" Height="2px" ></asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell ColumnSpan="2">
                                    <asp:Label ID="EtiPanel" runat="server" BackColor="Transparent" ForeColor="White"
                                        Text="Panel" Width="60px" Font-Italic="true">
                                    </asp:Label> 
                                </asp:TableCell>
                                <asp:TableCell ColumnSpan="2">
                                    <asp:ImageButton ID="Cmd30" runat="server" BorderStyle="None" 
                                        ImageAlign="Middle" ImageUrl="~/Images/Menus/NouveauDocument_Outil.bmp"
                                        ToolTip="Initialiser le panel" /> 
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell ColumnSpan="4">
                                    <div style="width:300px; height:580px; border: outset 1px #CAEBE4; background-color: Transparent;
                                        display: table-cell; overflow: scroll" >
                                        <asp:ListBox ID="ListePanel" runat="server" Width="300px" Height="580px"
                                             BackColor="#1A4746" ForeColor="#E9FDF9" style="margin-top: 0px; text-align: left" />
                                    </div>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell ColumnSpan="4">
                                    <asp:Label ID="EtiNBPanel" runat="server" BackColor="Transparent" ForeColor="#E9FDF9"
                                        Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Small" Font-Italic="true"
                                        Text="Aucun dossier" Width="300px" Height="18px">
                                    </asp:Label>
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
             </asp:Table>
        </asp:TableCell>
     </asp:TableRow>
</asp:Table>