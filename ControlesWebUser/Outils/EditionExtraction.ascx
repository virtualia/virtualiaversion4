﻿<%@ Control Language="VB" AutoEventWireup="false" Inherits="Virtualia.Net.VirtualiaOutils_EditionExtraction" Codebehind="EditionExtraction.ascx.vb" %>

<%@ Register src="../Saisies/VListeGrid.ascx" tagname="VListeGrid" tagprefix="Virtualia" %>

 <asp:Table ID="CadreListe" runat="server" BorderStyle="Inset" BorderWidth="1px" BackColor="#476CA3"
    BorderColor="#B6C7E2" HorizontalAlign="Center" style="margin-top: 3px;" >
    <asp:TableRow>
        <asp:TableCell>
            <asp:Panel ID="PanelCommande" runat="server" style="height:25px;width:1120px" >
              <asp:Table ID="CadreCmdOK" runat="server" Height="22px" CellPadding="0" 
                 CellSpacing="0" BackImageUrl="~/Images/Boutons/Retour_Std.bmp"
                 BorderWidth="2px" BorderStyle="Outset" BorderColor="#B6C7E2" ForeColor="#D7FAF3"
                 Width="80px" HorizontalAlign="Left" style="margin-top: 3px;margin-right: 2px">
                <asp:TableRow VerticalAlign="Top">
                    <asp:TableCell>
                         <asp:Button ID="CommandeRetour" runat="server" Text="Retour" Width="68px" Height="18px"
                             BackColor="Transparent" BorderColor="#B6C7E2" ForeColor="#D7FAF3"
                             Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Font-Italic="true"
                             BorderStyle="None" style=" margin-left: 8px; text-align: center;"
                             Tooltip="Revenir au menu initial" >
                         </asp:Button>
                    </asp:TableCell>
                </asp:TableRow>
              </asp:Table>
            </asp:Panel>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="CadreTitre" runat="server" CellPadding="1" CellSpacing="2" HorizontalAlign="Center">
                    <asp:TableRow>
                        <asp:TableCell HorizontalAlign="Left">
                            <asp:Label ID="EtiIntitule" runat="server" Height="20px" Width="600px" Text="Intitulé"
                                BackColor="#25355A" BorderColor="#B6C7E2"  BorderStyle="Groove"
                                BorderWidth="2px" ForeColor="#D7FAF3"
                                Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Small"
                                style="margin-top: 5px; margin-left: 4px; margin-bottom: 10px;
                                font-style: oblique; text-indent: 5px; text-align: center;" >
                            </asp:Label>          
                        </asp:TableCell>
                        <asp:TableCell HorizontalAlign="Left">
                            <asp:Label ID="EtiNbLignes" runat="server" Height="20px" Width="120px" Text="Nb Lignes"
                                BackColor="#25355A" BorderColor="#B6C7E2"  BorderStyle="Groove"
                                BorderWidth="2px" ForeColor="#D7FAF3"
                                Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Small"
                                style="margin-top: 5px; margin-left: 4px; margin-bottom: 10px;
                                font-style: oblique; text-indent: 5px; text-align: center; position:static" >
                            </asp:Label>          
                        </asp:TableCell>          
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell ColumnSpan="2">
                            <Virtualia:VListeGrid ID="ListeGrille" runat="server" CadreWidth="1120px"
                               BackColorCaption="#B6C7E2" BackColorRow="#DAE7FA" SiPagination="True"
                                 SiAppliquerCharte="false" SiColonneSelect="False" TaillePage="200" />
                        </asp:TableCell>
                    </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>
 </asp:Table>
 
 