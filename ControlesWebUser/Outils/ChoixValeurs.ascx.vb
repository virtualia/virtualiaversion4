﻿Option Strict On
Option Explicit On
Option Compare Text
Imports VI = Virtualia.Systeme.Constantes
Partial Class VirtualiaOutils_ChoixValeurs
    Inherits System.Web.UI.UserControl
    Public Delegate Sub EventHandler(ByVal sender As Object, ByVal e As EventArgs)
    Public Event RetourEventHandler As EventHandler
    Public Delegate Sub AppelChoixEventHandler(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.AppelChoixEventArgs)
    Public Event AppelChoix As AppelChoixEventHandler
    Private WebFct As Virtualia.Net.WebFonctions
    Private WsNomState As String = "Choix"

    Protected Overridable Sub Appel_Choix(ByVal e As Virtualia.Systeme.Evenements.AppelChoixEventArgs)
        RaiseEvent AppelChoix(Me, e)
    End Sub

    Public Property SiCadreValeursVisible() As Boolean
        Get
            Return CadreValeurs.Visible
        End Get
        Set(ByVal value As Boolean)
            OptionType.Visible = value
            CadreValeurs.Visible = value
        End Set
    End Property

    Public Property SiSysRefVisible() As Boolean
        Get
            Return SystemeReference.Visible
        End Get
        Set(ByVal value As Boolean)
            SystemeReference.Visible = value
        End Set
    End Property

    Public WriteOnly Property V_Appel(ByVal Modalite As Short) As ArrayList
        Set(ByVal value As ArrayList)
            If value Is Nothing Then
                Exit Property
            End If
            Dim Pvue As Short = CShort(value(0))
            Dim NoObjet As Short = CShort(value(1))
            Dim NoInfo As Short = CShort(value(2))
            Dim Ope As Short = CShort(value(3))
            Dim OpeInclu As Short = CShort(value(4))
            Dim Nature As Short = 0
            Dim PvueInverse As Short
            Dim Nomtable As String
            Dim IndiceI As Integer

            ListeResultat.Items.Clear()
            Valeur.DonText = ""
            ModeW.Value = Modalite.ToString

            Select Case Ope
                Case VI.Operateurs.Egalite
                    OptionOpe.RadioGaucheCheck = True
                Case Else
                    OptionOpe.RadioCentreCheck = True
            End Select
            Select Case OpeInclu
                Case VI.Operateurs.Inclu
                    OptionType.RadioGaucheCheck = True
                Case Else
                    OptionType.RadioCentreCheck = True
            End Select
            Select Case NoInfo
                Case Is < 500
                    EtiAppelant.Text = WebFct.PointeurDicoInfo(Pvue, NoObjet, NoInfo).Intitule
                    Nature = WebFct.PointeurDicoInfo(Pvue, NoObjet, NoInfo).VNature
                    PvueInverse = WebFct.PointeurDicoInfo(Pvue, NoObjet, NoInfo).PointdeVueInverse
                    Nomtable = WebFct.PointeurDicoInfo(Pvue, NoObjet, NoInfo).TabledeReference
                    If PvueInverse > 0 And Nomtable = "" Then
                        Nomtable = WebFct.PointeurDicoObjet(PvueInverse, 1).Intitule
                    End If
                Case Is > 9000
                    EtiAppelant.Text = "Constante"
                    PvueInverse = 0
                    Nomtable = ""
                Case Else
                    EtiAppelant.Text = WebFct.PointeurDicoExperte(Pvue, NoObjet, NoInfo).Intitule
                    Nature = WebFct.PointeurDicoExperte(Pvue, NoObjet, NoInfo).VNature
                    PvueInverse = 0
                    Nomtable = ""
            End Select
            Select Case Nature
                Case VI.NatureDonnee.DonneeTable, VI.NatureDonnee.DonneeTableMemo, VI.NatureDonnee.DonneeBooleenne
                    SiCadreValeursVisible = False
                    SiSysRefVisible = True
                    value(4) = OpeInclu
                    If PvueInverse > VI.PointdeVue.PVueGeneral Then
                        Nomtable = ""
                    End If
                    If Nature = VI.NatureDonnee.DonneeBooleenne Then
                        PvueInverse = VI.PointdeVue.PVueGeneral
                        Nomtable = "Booléen"
                    End If
                    SystemeReference.V_PointdeVue = PvueInverse
                    SystemeReference.V_NomTable = Nomtable
                    SystemeReference.V_Appelant(1) = Me.ID
                Case Else
                    SiSysRefVisible = False
                    SiCadreValeursVisible = True
            End Select
            Select Case Modalite
                Case Is = 0 'CMC
                    If value.Count > 5 Then
                        For IndiceI = 5 To value.Count - 1
                            ListeResultat.Items.Add(value(IndiceI).ToString)
                        Next IndiceI
                    End If
                Case Is = 1 'Edition - Export Traitement du filtre
                    If value.Count > 9 Then
                        For IndiceI = 9 To value.Count - 1
                            If value(IndiceI).ToString <> "" Then
                                ListeResultat.Items.Add(value(IndiceI).ToString)
                            End If
                        Next IndiceI
                    End If
            End Select
            If Me.ViewState(WsNomState) IsNot Nothing Then
                Me.ViewState.Remove(WsNomState)
            End If
            Me.ViewState.Add(WsNomState, value)
        End Set
    End Property

    Protected Sub SystemeReference_ValeurSelectionnee(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.ValeurSelectionneeEventArgs) Handles SystemeReference.ValeurSelectionnee
        If SiAFaireResultat(e.Valeur) = False Then
            Exit Sub
        End If
        ListeResultat.Items.Add(e.Valeur)
        If Me.ViewState(WsNomState) Is Nothing Then
            Exit Sub
        End If
        Dim VListe As ArrayList
        VListe = CType(Me.ViewState(WsNomState), ArrayList)
        VListe.Add(e.Valeur)
        Me.ViewState.Remove(WsNomState)
        Me.ViewState.Add(WsNomState, VListe)
    End Sub

    Protected Sub OptionOpe_ValeurChange(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.DonneeChangeEventArgs) Handles OptionOpe.ValeurChange
        If Me.ViewState(WsNomState) Is Nothing Then
            Exit Sub
        End If
        Dim VListe As ArrayList
        VListe = CType(Me.ViewState(WsNomState), ArrayList)
        Select Case OptionOpe.RadioGaucheCheck
            Case True
                VListe(3) = VI.Operateurs.Egalite
            Case False
                VListe(3) = VI.Operateurs.Difference
        End Select
        Me.ViewState.Remove(WsNomState)
        Me.ViewState.Add(WsNomState, VListe)
    End Sub

    Protected Sub OptionType_ValeurChange(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.DonneeChangeEventArgs) Handles OptionType.ValeurChange
        If Me.ViewState(WsNomState) Is Nothing Then
            Exit Sub
        End If
        Dim VListe As ArrayList
        VListe = CType(Me.ViewState(WsNomState), ArrayList)
        Select Case OptionType.RadioGaucheCheck
            Case True
                VListe(4) = VI.Operateurs.Inclu
            Case False
                VListe(4) = VI.Operateurs.Egalite
        End Select
        Me.ViewState.Remove(WsNomState)
        Me.ViewState.Add(WsNomState, VListe)
    End Sub

    Protected Sub Cmd01_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Cmd01.Click
        Dim VListe As New ArrayList
        Dim Evenement As Virtualia.Systeme.Evenements.AppelChoixEventArgs

        If Me.ViewState(WsNomState) Is Nothing Then
            Exit Sub
        End If
        VListe = CType(Me.ViewState(WsNomState), ArrayList)

        Evenement = New Virtualia.Systeme.Evenements.AppelChoixEventArgs(VListe)
        Appel_Choix(Evenement)

    End Sub

    Protected Sub Cmd02_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Cmd02.Click, Cmd03.Click
        Dim VListe As New ArrayList
        Dim Evenement As Virtualia.Systeme.Evenements.AppelChoixEventArgs

        If Me.ViewState(WsNomState) Is Nothing Then
            Exit Sub
        End If
        VListe = CType(Me.ViewState(WsNomState), ArrayList)
        ListeResultat.Items.Clear()
        Select Case ModeW.Value
            Case Is = "0" 'CMC
                Try
                    Do While VListe.Count > 4
                        VListe.RemoveAt(5)
                    Loop
                Catch ex As Exception
                    Exit Try
                End Try
            Case Is = "1" 'Extraction
                Try
                    Do While VListe.Count > 8
                        VListe.RemoveAt(9)
                    Loop
                Catch ex As Exception
                    Exit Try
                End Try
        End Select
        If CType(sender, Button).ID = "Cmd02" Then
            VListe.Add("Tous")
        Else
            VListe.Add("Aucun")
        End If
        Evenement = New Virtualia.Systeme.Evenements.AppelChoixEventArgs(VListe)
        Appel_Choix(Evenement)

    End Sub

    Protected Sub Cmd04_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Cmd04.Click
        If Me.ViewState(WsNomState) Is Nothing Then
            Exit Sub
        End If
        Try
            Dim Nb As Integer = 4
            Dim VListe As ArrayList
            Select Case ModeW.Value
                Case Is = "0" 'CMC
                    Nb = 4
                Case Is = "1" 'Edition
                    Nb = 8
            End Select
            VListe = CType(Me.ViewState(WsNomState), ArrayList)
            If VListe.Count > Nb Then
                VListe.Remove(ListeResultat.SelectedItem.Text)
                ListeResultat.Items.Remove(ListeResultat.SelectedItem.Text)
            End If
            Me.ViewState.Remove(WsNomState)
            Me.ViewState.Add(WsNomState, VListe)
        Catch ex As Exception
            Exit Sub
        End Try
    End Sub

    Protected Sub Cmd05_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Cmd05.Click
        If Me.ViewState(WsNomState) Is Nothing Then
            Exit Sub
        End If
        Dim VListe As ArrayList
        VListe = CType(Me.ViewState(WsNomState), ArrayList)
        ListeResultat.Items.Clear()
        Select Case ModeW.Value
            Case Is = "0" 'CMC
                Try
                    Do While VListe.Count > 4
                        VListe.RemoveAt(5)
                    Loop
                Catch ex As Exception
                    Exit Try
                End Try
            Case Is = "1" 'Extraction
                Try
                    Do While VListe.Count > 8
                        VListe.RemoveAt(9)
                    Loop
                Catch ex As Exception
                    Exit Try
                End Try
        End Select

        Me.ViewState.Remove(WsNomState)
        Me.ViewState.Add(WsNomState, VListe)
    End Sub

    Protected Sub CmdAdd_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles CmdAdd.Click
        If SiAFaireResultat(Valeur.DonText) = False Then
            Exit Sub
        End If
        If Me.ViewState(WsNomState) Is Nothing Then
            Exit Sub
        End If
        Dim VListe As ArrayList
        VListe = CType(Me.ViewState(WsNomState), ArrayList)

        Dim Pvue As Short = CShort(VListe(0))
        Dim NoObjet As Short = CShort(VListe(1))
        Dim NoInfo As Short = CShort(VListe(2))
        Dim Vnature As Short
        Dim Vformat As Short
        Dim Chaine As String

        Select Case NoInfo
            Case Is < 500
                Vnature = WebFct.PointeurDicoInfo(Pvue, NoObjet, NoInfo).VNature
                Vformat = WebFct.PointeurDicoInfo(Pvue, NoObjet, NoInfo).Format
            Case Is > 9000
                Vnature = 0
                Vformat = 0
            Case Else
                Vnature = WebFct.PointeurDicoExperte(Pvue, NoObjet, NoInfo).VNature
                Vformat = WebFct.PointeurDicoExperte(Pvue, NoObjet, NoInfo).Format
        End Select
        Chaine = WebFct.Controle_LostFocusStd(Vnature, Vformat, Valeur.DonText)

        ListeResultat.Items.Add(Chaine)
        VListe.Add(Chaine)
        Me.ViewState.Remove(WsNomState)
        Me.ViewState.Add(WsNomState, VListe)

        Valeur.DonText = ""
    End Sub

    Protected Sub Cmd07_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Cmd07.Click
        RaiseEvent RetourEventHandler(Me, e)
    End Sub

    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        WebFct = New Virtualia.Net.WebFonctions(Me, 0)
    End Sub

    Protected Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
        WebFct.Dispose()
    End Sub

    Private Function SiAFaireResultat(ByVal Valeur As String) As Boolean
        Dim IndiceI As Integer
        For IndiceI = 0 To ListeResultat.Items.Count - 1
            If ListeResultat.Items.Item(IndiceI).Text = Valeur Then
                Return False
                Exit Function
            End If
        Next IndiceI
        Return True
    End Function

End Class
