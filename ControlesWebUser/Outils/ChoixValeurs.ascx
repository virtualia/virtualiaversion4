﻿<%@ Control Language="VB" AutoEventWireup="false" Inherits="Virtualia.Net.VirtualiaOutils_ChoixValeurs" Codebehind="ChoixValeurs.ascx.vb" %>

<%@ Register src="../Saisies/VCoupleEtiDonnee.ascx" tagname="VCoupleEtiDonnee" tagprefix="Virtualia" %>
<%@ Register src="../Saisies/VTrioHorizontalRadio.ascx" tagname="VTrioHorizontalRadio" tagprefix="Virtualia" %> 
<%@ Register src="../Saisies/VTrioVerticalRadio.ascx" tagname="VTrioVerticalRadio" tagprefix="Virtualia" %> 
<%@ Register src="../ObjetsArmoire/ListeReferences.ascx" tagname="ListeReferences" tagprefix="Virtualia" %>

<asp:Table ID="ChoixCriteres" runat="server" Width="780px" BackColor="#7CC3BE" CellPadding="0">
    <asp:TableRow>
        <asp:TableCell Width="780px" HorizontalAlign="Center" VerticalAlign="Middle" BackColor="#225C59">
            <asp:Panel ID="PanelCmdChoix" runat="server" Height="22px" Width="560px"
                       BackImageUrl="~/Images/Boutons/MenuChoixValeurs.bmp"
                       style="text-align: left; vertical-align: middle">
                <asp:Button ID="Cmd01" runat="server" Height="20px" Width="76px" ForeColor="#E9FDF9"
                     BackColor="Transparent" Text="Valider" ToolTip="Valider les choix effectués"
                     Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Small" Font-Italic="true"
                     BorderStyle="None" />
                <asp:Button ID="Cmd02" runat="server" Height="20px" Width="76px" ForeColor="#E9FDF9"
                     BackColor="Transparent" Text="Tout" ToolTip="Toutes les valeurs possibles"
                     Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Small" Font-Italic="true" 
                     BorderStyle="None" />    
                <asp:Button ID="Cmd03" runat="server" Height="20px" Width="76px" ForeColor="#E9FDF9"
                     BackColor="Transparent" Text="Aucune" ToolTip="Non renseignée"
                     Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Small" Font-Italic="true"
                     BorderStyle="None" />     
                <asp:Button ID="Cmd04" runat="server" Height="20px" Width="76px" ForeColor="#E9FDF9"
                     BackColor="Transparent" Text="Effacer" ToolTip="Retirer une valeur"
                     Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Small" Font-Italic="true"
                     BorderStyle="None" /> 
                <asp:Button ID="Cmd05" runat="server" Height="20px" Width="76px" ForeColor="#E9FDF9"
                     BackColor="Transparent" Text="Init" ToolTip="Remettre à blanc les choix"
                     Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Small" Font-Italic="true"
                     BorderStyle="None" /> 
                <asp:Button ID="Cmd06" runat="server" Height="20px" Width="76px" ForeColor="#E9FDF9"
                     BackColor="Transparent" Text="Enrichir" ToolTip="Enrichissement expert"
                     Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Small" Font-Italic="true"
                     BorderStyle="None" /> 
                <asp:Button ID="Cmd07" runat="server" Height="20px" Width="76px" ForeColor="#E9FDF9"
                     BackColor="Transparent" Text="Retour" ToolTip="Revenir en arrière"
                     Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Small" Font-Italic="true"
                     BorderStyle="None" />              
            </asp:Panel>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell Height="2px">
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell Width="30px" HorizontalAlign="Center" VerticalAlign="Middle">
            <asp:Label ID="EtiAppelant" runat="server" Height="24px" Width = "400px" 
              BackColor="Transparent" ForeColor="#E9FDF9" Text="" 
              Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Medium" Font-Italic="false">
            </asp:Label>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell Width="780px" HorizontalAlign="Center"
                  BorderWidth="1px" BorderStyle="None" BorderColor="#E9FDF9" >
            <Virtualia:VTrioHorizontalRadio ID="OptionOpe" runat="server" 
               RadioGaucheText="Egalité avec" RadioCentreText="Différence avec" RadioDroiteText="" 
               V_Groupe="GroupeOpe" V_SiDonneeDico="False" RadioDroiteVisible="false"
               RadioGaucheBackColor="#225C59" RadioCentreBackColor="#225C59"
               RadioGaucheForeColor="#E9FDF9" RadioCentreForeColor="#E9FDF9"
               RadioGaucheBorderColor="#B0E0D7" RadioCentreBorderColor="#B0E0D7" 
               RadioGaucheWidth="150px" RadioCentreWidth="150px"/>
        </asp:TableCell>
   </asp:TableRow>
   <asp:TableRow>
        <asp:TableCell Width="780px" HorizontalAlign="Center">
            <asp:Table ID="CadreListe" runat="server" Width="460px">
                <asp:TableRow>
                    <asp:TableCell>
                        <div style="width:440px; border: outset 1px #225C59; background-color: Transparent;
                            display: table-cell; overflow: scroll" >
                            <asp:ListBox ID="ListeResultat" runat="server" Width="430px" Height="100px"
                                 BackColor="Snow" style="margin-top: 0px;text-align: left" />
                        </div>
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell Width="780px" HorizontalAlign="Center"
                BorderWidth="1px" BorderStyle="Solid" BorderColor="#E9FDF9">
           <Virtualia:VTrioVerticalRadio ID="OptionType" runat="server" 
               RadioGaucheText="Comprise entre un minimum et un maximum" RadioCentreText="Choix de valeurs précises" RadioDroiteText="" 
               V_Groupe="GroupeType" V_SiDonneeDico="False" RadioDroiteVisible="false"
               RadioGaucheBackColor="#225C59" RadioCentreBackColor="#225C59"
               RadioGaucheForeColor="#E9FDF9" RadioCentreForeColor="#E9FDF9"
               RadioGaucheBorderColor="#B0E0D7" RadioCentreBorderColor="#B0E0D7"
               RadioGaucheWidth="400px" RadioCentreWidth="400px"
               RadioGaucheStyle="text-align: left" RadioCentreStyle="text-align: left" />
       </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell Width="780px" HorizontalAlign="Center"
                    BorderWidth="1px" BorderStyle="None" BorderColor="#E9FDF9">
            <asp:Table ID="CadreValeurs" runat="server" Width="420px">
                <asp:TableRow>
                    <asp:TableCell Width="300px" HorizontalAlign="Left">
                        <Virtualia:VCoupleEtiDonnee ID="Valeur" runat="server" EtiBackColor="#225C59" EtiForeColor="#E9FDF9"
                            EtiWidth="100px" DonWidth="200px" EtiBorderColor="#B0E0D7" DonBorderColor="#B0E0D7"
                            V_SiDonneeDico="false" EtiText="Valeur" />
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:ImageButton ID="CmdAdd" runat="server" ImageUrl="~/Images/Icones/AddTable_Choix.bmp"
                         ToolTip="Ajouter la valeur à la liste des valeurs sélectionnées" />
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>     
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <asp:TableCell>
                <Virtualia:ListeReferences ID="SystemeReference" runat="server"
                    SiListeMenuVisible="false" SiPanelTitreVisible="false" />
            </asp:TableCell>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <asp:HiddenField ID="ModeW" runat="server" Value="0" />
        </asp:TableCell>
    </asp:TableRow>
 </asp:Table>