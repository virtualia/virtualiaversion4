﻿Option Strict On
Option Explicit On
Option Compare Text
Imports VI = Virtualia.Systeme.Constantes
Partial Class VirtualiaOutils_EditionExtraction
    Inherits System.Web.UI.UserControl
    Public Delegate Sub EventHandler(ByVal sender As Object, ByVal e As EventArgs)
    Public Event RetourEventHandler As EventHandler
    Private WebFct As Virtualia.Net.WebFonctions
    Private WsNomState As String

    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        WebFct = New Virtualia.Net.WebFonctions(Me, 0)
    End Sub

    Protected Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
        WebFct.Dispose()
    End Sub

    Public WriteOnly Property Centrage_Colonne(ByVal Index As Integer) As Short
        Set(ByVal value As Short)
            ListeGrille.Centrage_Colonne(Index) = value
        End Set
    End Property

    Public Property VDataGrid(ByVal LibColonnes As String, ByVal LibRes As String) As String
        Get
            Return ListeGrille.V_Liste("OK", "")
        End Get
        Set(ByVal value As String)
            ListeGrille.V_Liste(LibColonnes, LibRes) = value
        End Set
    End Property

    Public Property VIntitule() As String
        Get
            Return EtiIntitule.Text
        End Get
        Set(ByVal value As String)
            EtiIntitule.Text = value
            EtiNbLignes.Text = ListeGrille.TotalLignes.ToString
        End Set
    End Property

    Protected Sub CommandeRetour_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles CommandeRetour.Click
        RaiseEvent RetourEventHandler(Me, e)
    End Sub
End Class
