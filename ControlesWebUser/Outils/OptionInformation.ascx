﻿<%@ Control Language="VB" AutoEventWireup="false" Inherits="Virtualia.Net.VirtualiaOutils_OptionInformation" Codebehind="OptionInformation.ascx.vb" %>

<%@ Register src="../Saisies/VCoupleEtiDonnee.ascx" tagname="VCoupleEtiDonnee" tagprefix="Virtualia" %>

<asp:Table ID="OptionInformation" runat="server" Width="600px" Height="400px" BackColor="#7CC3BE" CellPadding="0">
    <asp:TableRow>
        <asp:TableCell Width="600px" HorizontalAlign="Left" VerticalAlign="Middle" BackColor="#225C59">
            <asp:Panel ID="PanelCmdChoix" runat="server" Height="22px" Width="80px"
                       BackImageUrl="~/Images/Boutons/MenuChoixValeurs.bmp"
                       style="text-align: left; vertical-align: middle">
                <asp:Button ID="Cmd01" runat="server" Height="20px" Width="76px" ForeColor="#E9FDF9"
                     BackColor="Transparent" Text="Valider" ToolTip="Valider les choix effectués"
                     Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Small" Font-Italic="true"
                     BorderStyle="None" />
            </asp:Panel>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell Height="10px"></asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell Width="30px" HorizontalAlign="Center" VerticalAlign="Middle">
            <asp:Label ID="EtiAppelant" runat="server" Height="24px" Width="400px" 
              BackColor="Transparent" ForeColor="#E9FDF9" Text="Nom de la donnée" 
              Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Medium" Font-Italic="false">
            </asp:Label>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell Width="550px" HorizontalAlign="Left">
            <Virtualia:VCoupleEtiDonnee ID="Intitule" runat="server" EtiBackColor="#225C59" EtiForeColor="#E9FDF9"
                EtiWidth="250px" DonWidth="300px" EtiBorderColor="#B0E0D7" DonBorderColor="#B0E0D7"
                V_SiDonneeDico="false" EtiText="Intitulé de la colonne" />
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell Width="400px" HorizontalAlign="Left">
            <Virtualia:VCoupleEtiDonnee ID="Longueur" runat="server" EtiBackColor="#225C59" EtiForeColor="#E9FDF9"
                EtiWidth="250px" DonWidth="60px" EtiBorderColor="#B0E0D7" DonBorderColor="#B0E0D7"
                V_SiDonneeDico="false" EtiText="Longueur de la colonne" DonStyle="text-align:center" />
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell Width="400px" HorizontalAlign="Left">
            <Virtualia:VCoupleEtiDonnee ID="Nombre" runat="server" EtiBackColor="#225C59" EtiForeColor="#E9FDF9"
                EtiWidth="250px" DonWidth="60px" EtiBorderColor="#B0E0D7" DonBorderColor="#B0E0D7"
                V_SiDonneeDico="false" EtiText="Nombre de colonnes répétées" DonStyle="text-align:center" />
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell HorizontalAlign="Center">
            <asp:Label ID="EtiFormat" runat="server" Height="20px" Width="250px" Text="Format de la donnée"
                    BackColor="#225C59" BorderColor="#B0E0D7"  BorderStyle="Outset"
                    BorderWidth="2px" ForeColor="#E9FDF9" Font-Italic="true"
                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                    style="margin-top: 0px; margin-left: 4px; text-indent: 5px; text-align: center" >
            </asp:Label>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell HorizontalAlign="Center" VerticalAlign="Top">
            <asp:ListBox ID="ListeFormats" runat="server" Width="255px" Height="140px"
                    BackColor="White" BorderColor="#B0E0D7"  BorderStyle="Inset"
                    BorderWidth="2px" ForeColor="#142425" Font-Italic="true"
                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small">
            </asp:ListBox>
        </asp:TableCell>
    </asp:TableRow>
 </asp:Table>