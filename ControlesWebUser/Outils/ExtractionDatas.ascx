﻿<%@ Control Language="VB" AutoEventWireup="false" Inherits="Virtualia.Net.VirtualiaOutils_ExtractionDatas" Codebehind="ExtractionDatas.ascx.vb" %>

<%@ Register src="../../ControlesWebUser/ObjetsArmoire/Dictionnaire.ascx" tagname="Dictionnaire" tagprefix="Virtualia" %>
<%@ Register src="../../ControlesWebUser/ObjetsArmoire/ScriptDictionnaire.ascx" tagname="ScriptDictionnaire" tagprefix="Virtualia" %>

<asp:Table ID="Extraction" runat="server" Height="700px" Width="1000px" CellPadding="0" CellSpacing="0">
    <asp:TableRow VerticalAlign="Top">
        <asp:TableCell Width="10px"></asp:TableCell>
        <asp:TableCell Width="340px">
                <Virtualia:Dictionnaire ID="VDictionnaire" runat="server" V_PointdeVue="1" V_Outil="6" />
        </asp:TableCell>
        <asp:TableCell Width="10px"></asp:TableCell>
        <asp:TableCell Width="600px">
            <asp:Table ID="ExtractionScript" runat="server">
                <asp:TableRow VerticalAlign="Top">
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:ScriptDictionnaire ID="VScriptExtraction" runat="server" 
                         V_PointdeVue="1" V_Outil="6" V_Width="600px"
                         V_PanelWidth="587px" />
                    </asp:TableCell>
                </asp:TableRow>
             </asp:Table>
        </asp:TableCell>
     </asp:TableRow>
     <asp:TableRow>
        <asp:TableCell>
            <asp:HiddenField ID="ModeW" runat="server" Value="0" />
        </asp:TableCell>
     </asp:TableRow>
</asp:Table>