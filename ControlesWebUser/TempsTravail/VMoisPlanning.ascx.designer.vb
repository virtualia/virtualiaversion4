﻿'------------------------------------------------------------------------------
' <généré automatiquement>
'     Ce code a été généré par un outil.
'
'     Les modifications apportées à ce fichier peuvent provoquer un comportement incorrect et seront perdues si
'     le code est régénéré.
' </généré automatiquement>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class Controles_VMoisPlanning

    '''<summary>
    '''Contrôle VPlanningMois.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VPlanningMois As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle VSelection.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VSelection As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle VMois.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VMois As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle EtiMois.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiMois As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle EtiAnnee.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiAnnee As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle VJour00.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VJour00 As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle JAM00.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents JAM00 As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Contrôle JPM00.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents JPM00 As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Contrôle VJour01.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VJour01 As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle JAM01.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents JAM01 As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Contrôle JPM01.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents JPM01 As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Contrôle VJour02.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VJour02 As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle JAM02.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents JAM02 As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Contrôle JPM02.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents JPM02 As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Contrôle VJour03.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VJour03 As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle JAM03.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents JAM03 As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Contrôle JPM03.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents JPM03 As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Contrôle VJour04.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VJour04 As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle JAM04.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents JAM04 As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Contrôle JPM04.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents JPM04 As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Contrôle VJour05.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VJour05 As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle JAM05.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents JAM05 As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Contrôle JPM05.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents JPM05 As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Contrôle VJour06.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VJour06 As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle JAM06.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents JAM06 As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Contrôle JPM06.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents JPM06 As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Contrôle VJour07.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VJour07 As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle JAM07.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents JAM07 As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Contrôle JPM07.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents JPM07 As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Contrôle VJour08.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VJour08 As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle JAM08.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents JAM08 As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Contrôle JPM08.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents JPM08 As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Contrôle VJour09.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VJour09 As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle JAM09.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents JAM09 As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Contrôle JPM09.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents JPM09 As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Contrôle VJour10.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VJour10 As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle JAM10.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents JAM10 As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Contrôle JPM10.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents JPM10 As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Contrôle VJour11.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VJour11 As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle JAM11.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents JAM11 As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Contrôle JPM11.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents JPM11 As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Contrôle VJour12.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VJour12 As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle JAM12.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents JAM12 As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Contrôle JPM12.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents JPM12 As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Contrôle VJour13.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VJour13 As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle JAM13.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents JAM13 As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Contrôle JPM13.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents JPM13 As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Contrôle VJour14.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VJour14 As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle JAM14.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents JAM14 As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Contrôle JPM14.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents JPM14 As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Contrôle VJour15.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VJour15 As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle JAM15.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents JAM15 As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Contrôle JPM15.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents JPM15 As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Contrôle VJour16.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VJour16 As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle JAM16.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents JAM16 As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Contrôle JPM16.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents JPM16 As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Contrôle VJour17.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VJour17 As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle JAM17.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents JAM17 As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Contrôle JPM17.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents JPM17 As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Contrôle VJour18.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VJour18 As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle JAM18.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents JAM18 As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Contrôle JPM18.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents JPM18 As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Contrôle VJour19.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VJour19 As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle JAM19.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents JAM19 As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Contrôle JPM19.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents JPM19 As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Contrôle VJour20.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VJour20 As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle JAM20.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents JAM20 As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Contrôle JPM20.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents JPM20 As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Contrôle VJour21.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VJour21 As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle JAM21.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents JAM21 As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Contrôle JPM21.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents JPM21 As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Contrôle VJour22.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VJour22 As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle JAM22.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents JAM22 As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Contrôle JPM22.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents JPM22 As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Contrôle VJour23.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VJour23 As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle JAM23.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents JAM23 As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Contrôle JPM23.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents JPM23 As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Contrôle VJour24.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VJour24 As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle JAM24.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents JAM24 As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Contrôle JPM24.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents JPM24 As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Contrôle VJour25.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VJour25 As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle JAM25.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents JAM25 As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Contrôle JPM25.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents JPM25 As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Contrôle VJour26.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VJour26 As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle JAM26.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents JAM26 As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Contrôle JPM26.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents JPM26 As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Contrôle VJour27.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VJour27 As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle JAM27.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents JAM27 As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Contrôle JPM27.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents JPM27 As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Contrôle VJour28.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VJour28 As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle JAM28.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents JAM28 As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Contrôle JPM28.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents JPM28 As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Contrôle VJour29.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VJour29 As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle JAM29.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents JAM29 As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Contrôle JPM29.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents JPM29 As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Contrôle VJour30.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VJour30 As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle JAM30.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents JAM30 As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Contrôle JPM30.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents JPM30 As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Contrôle VJour31.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VJour31 As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle JAM31.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents JAM31 As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Contrôle JPM31.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents JPM31 As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Contrôle VJour32.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VJour32 As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle JAM32.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents JAM32 As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Contrôle JPM32.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents JPM32 As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Contrôle VJour33.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VJour33 As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle JAM33.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents JAM33 As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Contrôle JPM33.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents JPM33 As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Contrôle VJour34.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VJour34 As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle JAM34.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents JAM34 As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Contrôle JPM34.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents JPM34 As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Contrôle VJour35.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VJour35 As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle JAM35.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents JAM35 As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Contrôle JPM35.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents JPM35 As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Contrôle VJour36.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VJour36 As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle JAM36.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents JAM36 As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Contrôle JPM36.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents JPM36 As Global.System.Web.UI.WebControls.Button
End Class
