﻿Option Strict On
Option Explicit On
Option Compare Text
Public Class Controles_V2LignesDebitCredit
    Inherits System.Web.UI.UserControl

    Public Sub Initialiser(ByVal Index As Integer)
        Select Case Index
            Case 0
                LabelTitreLigne1.Text = ""
                Ligne1M01.Text = ""
                Ligne1M02.Text = ""
                Ligne1M03.Text = ""
                Ligne1M04.Text = ""
                Ligne1M05.Text = ""
                Ligne1M06.Text = ""
                Ligne1M07.Text = ""
                Ligne1M08.Text = ""
                Ligne1M09.Text = ""
                Ligne1M10.Text = ""
                Ligne1M11.Text = ""
                Ligne1M12.Text = ""
                Ligne1.Visible = True
            Case 1
                LabelTitreLigne2.Text = ""
                Ligne2M01.Text = ""
                Ligne2M02.Text = ""
                Ligne2M03.Text = ""
                Ligne2M04.Text = ""
                Ligne2M05.Text = ""
                Ligne2M06.Text = ""
                Ligne2M07.Text = ""
                Ligne2M08.Text = ""
                Ligne2M09.Text = ""
                Ligne2M10.Text = ""
                Ligne2M11.Text = ""
                Ligne2M12.Text = ""
                Ligne2.Visible = True
        End Select
    End Sub

    Public WriteOnly Property SiLigneVisible(ByVal Index As Integer) As Boolean
        Set(ByVal value As Boolean)
            Select Case Index
                Case 0
                    Ligne1.Visible = value
                Case 1
                    Ligne2.Visible = value
            End Select
        End Set
    End Property

    Public WriteOnly Property VText_Ligne(ByVal Index As Integer) As String
        Set(ByVal value As String)
            Select Case Index
                Case 0
                    LabelTitreLigne1.Text = value
                Case 1
                    LabelTitreLigne2.Text = value
            End Select
        End Set
    End Property

    Public WriteOnly Property VTooltip_Ligne(ByVal Index As Integer) As String
        Set(ByVal value As String)
            Select Case Index
                Case 0
                    LabelTitreLigne1.ToolTip = value
                Case 1
                    LabelTitreLigne2.ToolTip = value
            End Select
        End Set
    End Property

    Public WriteOnly Property VText_Colonne(ByVal Index As Integer, ByVal Mois As Integer) As String
        Set(ByVal value As String)
            Select Case Index
                Case 0
                    Select Case Mois
                        Case 1
                            Ligne1M01.Text = value
                        Case 2
                            Ligne1M02.Text = value
                        Case 3
                            Ligne1M03.Text = value
                        Case 4
                            Ligne1M04.Text = value
                        Case 5
                            Ligne1M05.Text = value
                        Case 6
                            Ligne1M06.Text = value
                        Case 7
                            Ligne1M07.Text = value
                        Case 8
                            Ligne1M08.Text = value
                        Case 9
                            Ligne1M09.Text = value
                        Case 10
                            Ligne1M10.Text = value
                        Case 11
                            Ligne1M11.Text = value
                        Case 12
                            Ligne1M12.Text = value
                    End Select
                Case 1
                    Select Case Mois
                        Case 1
                            Ligne2M01.Text = value
                        Case 2
                            Ligne2M02.Text = value
                        Case 3
                            Ligne2M03.Text = value
                        Case 4
                            Ligne2M04.Text = value
                        Case 5
                            Ligne2M05.Text = value
                        Case 6
                            Ligne2M06.Text = value
                        Case 7
                            Ligne2M07.Text = value
                        Case 8
                            Ligne2M08.Text = value
                        Case 9
                            Ligne2M09.Text = value
                        Case 10
                            Ligne2M10.Text = value
                        Case 11
                            Ligne2M11.Text = value
                        Case 12
                            Ligne2M12.Text = value
                    End Select
            End Select
        End Set
    End Property
End Class