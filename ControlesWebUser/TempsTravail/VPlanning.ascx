﻿<%@ Control Language="VB" AutoEventWireup="false" Inherits="Virtualia.Net.Controles_VPlanning" Codebehind="VPlanning.ascx.vb" %>

<%@ Register src="../TempsTravail/VLibelJourPlanning.ascx" tagname="VLibelJourPlanning" tagprefix="Virtualia" %>
<%@ Register src="../TempsTravail/VMoisPlanning.ascx" tagname="VMoisPlanning" tagprefix="Virtualia" %>
<%@ Register src="../TempsTravail/VLegendeCouleurs.ascx" tagname="VLegendeCouleurs" tagprefix="Virtualia" %>

<asp:UpdatePanel ID="UpdatePanelPlanning" runat="server">
  <ContentTemplate>
     <asp:Table ID="CadrePlanning" runat="server" BorderStyle="Ridge" BorderWidth="2px" Width="825px"
         BackColor="#E9FDF9" BorderColor="#B0E0D7" style="margin-left: 3px; margin-top: 3px;"   
         HorizontalAlign="Center" CellPadding="0" CellSpacing="0">
            <asp:TableRow>
                <asp:TableCell Height="10px"></asp:TableCell>      
            </asp:TableRow>
            <asp:TableRow>
                <asp:TableCell>
                    <Virtualia:VLibelJourPlanning ID="LibelJours" runat="server" />
                </asp:TableCell>      
            </asp:TableRow>
            <asp:TableRow>
                <asp:TableCell Height="10px"></asp:TableCell>      
            </asp:TableRow>
            <asp:TableRow>
                <asp:TableCell>
                    <Virtualia:VMoisPlanning ID="PlanningM00" runat="server" />
                </asp:TableCell>      
            </asp:TableRow>
            <asp:TableRow>
                <asp:TableCell Height="3px"></asp:TableCell>      
            </asp:TableRow>
            <asp:TableRow>
                <asp:TableCell>
                    <Virtualia:VMoisPlanning ID="PlanningM01" runat="server" />
                </asp:TableCell>      
            </asp:TableRow>
             <asp:TableRow>
                <asp:TableCell Height="3px"></asp:TableCell>      
            </asp:TableRow>
            <asp:TableRow>
                <asp:TableCell>
                    <Virtualia:VMoisPlanning ID="PlanningM02" runat="server" />
                </asp:TableCell>      
            </asp:TableRow>
             <asp:TableRow>
                <asp:TableCell Height="3px"></asp:TableCell>      
            </asp:TableRow>
            <asp:TableRow>
                <asp:TableCell>
                    <Virtualia:VMoisPlanning ID="PlanningM03" runat="server" />
                </asp:TableCell>      
            </asp:TableRow>
             <asp:TableRow>
                <asp:TableCell Height="3px"></asp:TableCell>      
            </asp:TableRow>
            <asp:TableRow>
                <asp:TableCell>
                    <Virtualia:VMoisPlanning ID="PlanningM04" runat="server" />
                </asp:TableCell>      
            </asp:TableRow>
            <asp:TableRow>
                <asp:TableCell Height="3px"></asp:TableCell>      
            </asp:TableRow>
            <asp:TableRow>
                <asp:TableCell>
                    <Virtualia:VMoisPlanning ID="PlanningM05" runat="server" />
                </asp:TableCell>      
            </asp:TableRow>
            <asp:TableRow>
                <asp:TableCell Height="3px"></asp:TableCell>      
            </asp:TableRow>
            <asp:TableRow>
                <asp:TableCell>
                    <Virtualia:VMoisPlanning ID="PlanningM06" runat="server" />
                </asp:TableCell>      
            </asp:TableRow>
            <asp:TableRow>
                <asp:TableCell Height="3px"></asp:TableCell>      
            </asp:TableRow>
            <asp:TableRow>
                <asp:TableCell>
                    <Virtualia:VMoisPlanning ID="PlanningM07" runat="server" />
                </asp:TableCell>      
            </asp:TableRow>
            <asp:TableRow>
                <asp:TableCell Height="3px"></asp:TableCell>      
            </asp:TableRow>
            <asp:TableRow>
                <asp:TableCell>
                    <Virtualia:VMoisPlanning ID="PlanningM08" runat="server" />
                </asp:TableCell>      
            </asp:TableRow>
            <asp:TableRow>
                <asp:TableCell Height="3px"></asp:TableCell>      
            </asp:TableRow>
            <asp:TableRow>
                <asp:TableCell>
                    <Virtualia:VMoisPlanning ID="PlanningM09" runat="server" />
                </asp:TableCell>      
            </asp:TableRow>
            <asp:TableRow>
                <asp:TableCell Height="3px"></asp:TableCell>      
            </asp:TableRow>
            <asp:TableRow>
                <asp:TableCell>
                    <Virtualia:VMoisPlanning ID="PlanningM10" runat="server" />
                </asp:TableCell>      
            </asp:TableRow>
             <asp:TableRow>
                <asp:TableCell Height="3px"></asp:TableCell>      
            </asp:TableRow>
            <asp:TableRow>
                <asp:TableCell>
                    <Virtualia:VMoisPlanning ID="PlanningM11" runat="server" />
                </asp:TableCell>      
            </asp:TableRow>
            <asp:TableRow>
                <asp:TableCell Height="3px"></asp:TableCell>      
            </asp:TableRow>
            <asp:TableRow>
                <asp:TableCell>
                    <Virtualia:VMoisPlanning ID="PlanningM12" runat="server" />
                </asp:TableCell>      
            </asp:TableRow>
            <asp:TableRow>
                <asp:TableCell Height="3px"></asp:TableCell>      
            </asp:TableRow>
            <asp:TableRow>
                <asp:TableCell>
                    <Virtualia:VMoisPlanning ID="PlanningM13" runat="server" />
                </asp:TableCell>      
            </asp:TableRow>
            <asp:TableRow>
                <asp:TableCell Height="3px"></asp:TableCell>      
            </asp:TableRow>
            <asp:TableRow>
                <asp:TableCell>
                    <Virtualia:VMoisPlanning ID="PlanningM14" runat="server" />
                </asp:TableCell>      
            </asp:TableRow>
            <asp:TableRow>
                <asp:TableCell Height="3px"></asp:TableCell>      
            </asp:TableRow>
            <asp:TableRow>
                <asp:TableCell>
                    <Virtualia:VMoisPlanning ID="PlanningM15" runat="server" />
                </asp:TableCell>      
            </asp:TableRow>
            <asp:TableRow>
                <asp:TableCell Height="3px"></asp:TableCell>      
            </asp:TableRow>
            <asp:TableRow>
                <asp:TableCell>
                    <Virtualia:VMoisPlanning ID="PlanningM16" runat="server" />
                </asp:TableCell>      
            </asp:TableRow>
            <asp:TableRow>
                <asp:TableCell Height="3px"></asp:TableCell>      
            </asp:TableRow>
            <asp:TableRow>
                <asp:TableCell>
                    <Virtualia:VMoisPlanning ID="PlanningM17" runat="server"/>
                </asp:TableCell>      
            </asp:TableRow>
             <asp:TableRow>
                <asp:TableCell Height="3px"></asp:TableCell>      
            </asp:TableRow>
            <asp:TableRow>
                <asp:TableCell>
                    <Virtualia:VLegendeCouleurs ID="Legende" runat="server" />
                </asp:TableCell>      
            </asp:TableRow>
            <asp:TableRow>
                <asp:TableCell Height="5px"></asp:TableCell>      
            </asp:TableRow>
            <asp:TableRow>
                <asp:TableCell>
                    <asp:HiddenField ID="HSelIde" runat="server" Value="0" />
                </asp:TableCell>
          </asp:TableRow>
    </asp:Table>
  </ContentTemplate>
</asp:UpdatePanel>
       