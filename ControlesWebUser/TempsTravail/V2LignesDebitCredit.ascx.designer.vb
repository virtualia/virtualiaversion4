﻿'------------------------------------------------------------------------------
' <généré automatiquement>
'     Ce code a été généré par un outil.
'
'     Les modifications apportées à ce fichier peuvent provoquer un comportement incorrect et seront perdues si
'     le code est régénéré.
' </généré automatiquement>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class Controles_V2LignesDebitCredit

    '''<summary>
    '''Contrôle LignesDebitCredit.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LignesDebitCredit As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle Ligne1.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Ligne1 As Global.System.Web.UI.WebControls.TableRow

    '''<summary>
    '''Contrôle CellTitreLigne1.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellTitreLigne1 As Global.System.Web.UI.WebControls.TableCell

    '''<summary>
    '''Contrôle LabelTitreLigne1.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelTitreLigne1 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle CellLigne1M01.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellLigne1M01 As Global.System.Web.UI.WebControls.TableCell

    '''<summary>
    '''Contrôle Ligne1M01.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Ligne1M01 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle CellLigne1M02.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellLigne1M02 As Global.System.Web.UI.WebControls.TableCell

    '''<summary>
    '''Contrôle Ligne1M02.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Ligne1M02 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle CellLigne1M03.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellLigne1M03 As Global.System.Web.UI.WebControls.TableCell

    '''<summary>
    '''Contrôle Ligne1M03.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Ligne1M03 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle CellLigne1M04.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellLigne1M04 As Global.System.Web.UI.WebControls.TableCell

    '''<summary>
    '''Contrôle Ligne1M04.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Ligne1M04 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle CellLigne1M05.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellLigne1M05 As Global.System.Web.UI.WebControls.TableCell

    '''<summary>
    '''Contrôle Ligne1M05.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Ligne1M05 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle CellLigne1M06.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellLigne1M06 As Global.System.Web.UI.WebControls.TableCell

    '''<summary>
    '''Contrôle Ligne1M06.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Ligne1M06 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle CellLigne1M07.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellLigne1M07 As Global.System.Web.UI.WebControls.TableCell

    '''<summary>
    '''Contrôle Ligne1M07.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Ligne1M07 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle CellLigne1M08.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellLigne1M08 As Global.System.Web.UI.WebControls.TableCell

    '''<summary>
    '''Contrôle Ligne1M08.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Ligne1M08 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle CellLigne1M09.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellLigne1M09 As Global.System.Web.UI.WebControls.TableCell

    '''<summary>
    '''Contrôle Ligne1M09.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Ligne1M09 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle CellLigne1M10.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellLigne1M10 As Global.System.Web.UI.WebControls.TableCell

    '''<summary>
    '''Contrôle Ligne1M10.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Ligne1M10 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle CellLigne1M11.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellLigne1M11 As Global.System.Web.UI.WebControls.TableCell

    '''<summary>
    '''Contrôle Ligne1M11.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Ligne1M11 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle CellLigne1M12.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellLigne1M12 As Global.System.Web.UI.WebControls.TableCell

    '''<summary>
    '''Contrôle Ligne1M12.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Ligne1M12 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle Ligne2.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Ligne2 As Global.System.Web.UI.WebControls.TableRow

    '''<summary>
    '''Contrôle CellTitreLigne2.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellTitreLigne2 As Global.System.Web.UI.WebControls.TableCell

    '''<summary>
    '''Contrôle LabelTitreLigne2.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelTitreLigne2 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle CellLigne2M01.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellLigne2M01 As Global.System.Web.UI.WebControls.TableCell

    '''<summary>
    '''Contrôle Ligne2M01.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Ligne2M01 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle CellLigne2M02.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellLigne2M02 As Global.System.Web.UI.WebControls.TableCell

    '''<summary>
    '''Contrôle Ligne2M02.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Ligne2M02 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle CellLigne2M03.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellLigne2M03 As Global.System.Web.UI.WebControls.TableCell

    '''<summary>
    '''Contrôle Ligne2M03.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Ligne2M03 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle CellLigne2M04.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellLigne2M04 As Global.System.Web.UI.WebControls.TableCell

    '''<summary>
    '''Contrôle Ligne2M04.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Ligne2M04 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle CellLigne2M05.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellLigne2M05 As Global.System.Web.UI.WebControls.TableCell

    '''<summary>
    '''Contrôle Ligne2M05.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Ligne2M05 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle CellLigne2M06.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellLigne2M06 As Global.System.Web.UI.WebControls.TableCell

    '''<summary>
    '''Contrôle Ligne2M06.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Ligne2M06 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle CellLigne2M07.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellLigne2M07 As Global.System.Web.UI.WebControls.TableCell

    '''<summary>
    '''Contrôle Ligne2M07.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Ligne2M07 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle CellLigne2M08.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellLigne2M08 As Global.System.Web.UI.WebControls.TableCell

    '''<summary>
    '''Contrôle Ligne2M08.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Ligne2M08 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle CellLigne2M09.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellLigne2M09 As Global.System.Web.UI.WebControls.TableCell

    '''<summary>
    '''Contrôle Ligne2M09.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Ligne2M09 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle CellLigne2M10.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellLigne2M10 As Global.System.Web.UI.WebControls.TableCell

    '''<summary>
    '''Contrôle Ligne2M10.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Ligne2M10 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle CellLigne2M11.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellLigne2M11 As Global.System.Web.UI.WebControls.TableCell

    '''<summary>
    '''Contrôle Ligne2M11.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Ligne2M11 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle CellLigne2M12.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellLigne2M12 As Global.System.Web.UI.WebControls.TableCell

    '''<summary>
    '''Contrôle Ligne2M12.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Ligne2M12 As Global.System.Web.UI.WebControls.Label
End Class
