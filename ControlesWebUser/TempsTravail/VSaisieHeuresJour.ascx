﻿<%@ Control Language="VB" AutoEventWireup="false" Inherits="Virtualia.Net.Controles_VSaisieHeuresJour" Codebehind="VSaisieHeuresJour.ascx.vb" %>

<asp:Table ID="SaisieJour" runat="server" Height="50px">
    <asp:TableRow>
        <asp:TableCell BackColor="#CAEBE4" BorderColor="#B0E0D7" BorderStyle="Ridge" BorderWidth="1px">
            <asp:Label ID="EtiLibelJour" runat="server" Height="48px" Width="120px" BackColor="#B0E0D7"
                 ForeColor="#1C5151" BorderStyle="None" Text="Libel du jour" 
                 Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Small" Font-Italic="false"
                 style="margin-top: 1px; margin-left: 0px; text-indent:1px; text-align: center; padding-top: 15px" />
        </asp:TableCell>
        <asp:TableCell  BorderColor="#7EC8BE" BorderStyle="Ridge" BorderWidth="1px">
            <asp:Table ID="HorairesJour" runat="server" Width="155px" BorderStyle="None">
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Table ID="SaisieMatin" runat="server">
                            <asp:TableRow>
                                <asp:TableCell>
                                    <asp:TextBox ID="TxtMatinA" runat="server" AutoPostBack="true" Height="20px" Width="45px"
                                                 style="text-align: center;">
                                    </asp:TextBox>
                                </asp:TableCell>
                                <asp:TableCell>
                                    <asp:Label ID="EtiLabelMatin" runat="server" Height="20px" Width="15px" BackColor="#6C9690"
                                         ForeColor="White" BorderStyle="None" Text="à"
                                         Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Small" Font-Italic="false"
                                         style="margin-top: 1px; margin-left: 0px; text-indent:1px; text-align: center; vertical-align: top" />
                                </asp:TableCell>
                                <asp:TableCell>
                                    <asp:TextBox ID="TxtMatinD" runat="server" AutoPostBack="true" Height="20px" Width="45px"
                                                 style="text-align: center;">
                                    </asp:TextBox>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell>
                                    <asp:TextBox ID="TxtSoirA" runat="server" AutoPostBack="true" Height="20px" Width="45px"
                                                  style="text-align: center;">
                                    </asp:TextBox>
                                </asp:TableCell>
                                <asp:TableCell>
                                    <asp:Label ID="EtiLabelSoir" runat="server" Height="20px" Width="15px" BackColor="#6C9690"
                                         ForeColor="White" BorderStyle="None" Text="à"
                                         Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Small" Font-Italic="false"
                                         style="margin-top: 1px; margin-left: 0px; text-indent:1px; text-align: center; vertical-align: top" />
                                </asp:TableCell>
                                <asp:TableCell>
                                    <asp:TextBox ID="TxtSoirD" runat="server" AutoPostBack="true" Height="20px" Width="45px"
                                                 style="text-align: center;">
                                    </asp:TextBox>
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                    <asp:TableCell VerticalAlign="Middle">
                        <asp:Button ID="CommandeAide" runat="server" Height="20px" Width="15px" 
                            Text="..." style="text-align: center;" />
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
        <asp:TableCell  BorderColor="#7EC8BE" BorderStyle="Ridge" BorderWidth="1px">
             <asp:Table ID="TableTotal" runat="server" Width="155px" HorizontalAlign="Center" BorderStyle="None">
               <asp:TableRow>
                    <asp:TableCell>
                        <asp:Table ID="CadreTotal" runat="server">
                            <asp:TableRow>
                                <asp:TableCell>
                                    <asp:Label ID="TotalMatin" runat="server" Height="25px" Width="65px"
                                               BackColor="#6C9690"  ForeColor="White" BorderStyle="None"
                                               Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Small" Font-Italic="false"
                                               style="margin-top: 0px; margin-left: 0px; text-indent:1px; 
                                               text-align: center; vertical-align: top" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell>
                                    <asp:Label ID="TotalSoir" runat="server" Height="25px" Width="65px"
                                               BackColor="#6C9690"  ForeColor="White" BorderStyle="None"
                                               Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Small" Font-Italic="false"
                                               style="margin-top: 5px; margin-left: 0px; text-indent:1px; 
                                               text-align: center; vertical-align: top" />
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                    <asp:TableCell>
                        <asp:Table ID="TableTotalJour" runat="server">
                            <asp:TableRow>
                                <asp:TableCell>
                                    <asp:Label ID="TotalJour" runat="server" Height="25px" Width="71px"
                                       BackColor="#6C9690"  ForeColor="White" BorderStyle="None"
                                       Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Small" Font-Italic="false"
                                       style="margin-top: 0px; margin-left: 0px; text-indent:1px; 
                                       text-align: center; vertical-align: top" /> 
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>
</asp:Table>