﻿Option Strict On
Option Explicit On
Option Compare Text
Imports VI = Virtualia.Systeme.Constantes
Imports Microsoft.VisualBasic
Partial Class Controles_VSaisieAbsence
    Inherits System.Web.UI.UserControl
    Private WebFct As Virtualia.Net.WebFonctions

    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        WebFct = New Virtualia.Net.WebFonctions(Me, 0)
    End Sub

    Protected Sub CalendDebut_ValeurChange(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.DonneeChangeEventArgs) Handles CalendDebut.ValeurChange
        Select Case e.Parametre
            Case Is = "Jour"
                CalendFin.DateSelectionnee(0) = e.Valeur
                HDateDebut.Value = e.Valeur
            Case Is = "Mois"
                CalendFin.MoisSelectionne = e.Valeur
                CalendDebut.DateSelectionnee(0) = ""
                CalendFin.DateSelectionnee(0) = ""
                HMoisDebut.Value = e.Valeur
                HDateDebut.Value = ""
                HDateFin.Value = ""
        End Select
    End Sub

    Protected Sub CalendFin_ValeurChange(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.DonneeChangeEventArgs) Handles CalendFin.ValeurChange
        Dim TableauW(0) As String
        Dim Mois As Integer
        Dim Annee As Integer
        Select Case e.Parametre
            Case Is = "Jour"
                HDateFin.Value = e.Valeur
            Case Is = "Mois"
                TableauW = Strings.Split(e.Valeur, Strings.Space(1), -1)
                Annee = CInt(TableauW(1))
                Mois = WebFct.ViRhDates.IndexduMois(TableauW(0))
                If HDateDebut.Value <> "" Then
                    If CInt(Strings.Mid(HDateDebut.Value, 4, 2)) = Mois Then
                        CalendFin.DateSelectionnee(0) = HDateDebut.Value
                    Else
                        CalendFin.DateSelectionnee(0) = ""
                    End If
                Else
                    CalendFin.DateSelectionnee(0) = ""
                End If
        End Select
    End Sub
End Class
