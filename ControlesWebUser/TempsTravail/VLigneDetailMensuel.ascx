﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="VLigneDetailMensuel.ascx.vb" 
   Inherits="Virtualia.Net.Controles_VLigneDetailMensuel" %>

<asp:Table ID="CadreLigne" runat="server" CellPadding="0" CellSpacing="0" Width="750px"
            BorderStyle="Notset" BorderWidth="1px" BorderColor="#1C5151" HorizontalAlign="Left" Visible="true">                
    <asp:TableRow Width="746px" Height="20px">
        <asp:TableCell ID="CellTitreJour" BackColor="#f0f0f0" BorderColor="#A8BBB8" BorderStyle="Notset" HorizontalAlign="Center" Height="20px"> 
            <asp:Label ID="LabelDate" runat="server" Height="20px" Width="234px"
                BackColor="#f0f0f0" BorderColor="#A8BBB8" BorderStyle="Solid" Text="Lundi 5 avril 2010"
                BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="13px"
                style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                font-style: normal; text-indent: 15px; text-align:  left; padding-top: 1px;">
            </asp:Label>          
        </asp:TableCell>
        <asp:TableCell ID="CellLigneMatin" BackColor="#f0f0f0" BorderColor="#A8BBB8" BorderStyle="Notset" Visible="true"> 
            <asp:Label ID="LigneMatin" runat="server" Height="20px" Width="71px"
                    BackColor="White" BorderColor="#A8BBB8" BorderStyle="Solid" Text=""
                    BorderWidth="1px" ForeColor="Black"
                    Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Smaller" Font-Italic="false"
                    style="margin-top: 0px; margin-left: 0px; text-indent: 0px; text-align: center; padding-top: 1px;"> 
            </asp:Label>
        </asp:TableCell>
        <asp:TableCell ID="CellLigneMatinHDebut" BackColor="#f0f0f0" BorderColor="#A8BBB8" BorderStyle="Notset" Visible="true"> 
            <asp:Label ID="LigneMatinHDebut" runat="server" Height="20px" Width="55px"
                    BackColor="White" BorderColor="#A8BBB8" BorderStyle="Solid" Text=""
                    BorderWidth="1px" ForeColor="Black"
                    Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Smaller" Font-Italic="false"
                    style="margin-top: 0px; margin-left: 0px; text-indent: 0px; text-align: center; padding-top: 1px;"> 
            </asp:Label> 
        </asp:TableCell>
        <asp:TableCell ID="CellLigneMatinHFin" BackColor="#f0f0f0" BorderColor="#A8BBB8" BorderStyle="Notset" Visible="true"> 
            <asp:Label ID="LigneMatinHFin" runat="server" Height="20px" Width="55px"
                    BackColor="White" BorderColor="#A8BBB8" BorderStyle="Solid" Text=""
                    BorderWidth="1px" ForeColor="Black"
                    Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Smaller" Font-Italic="false"
                    style="margin-top: 0px; margin-left: 0px; text-indent: 0px; text-align: center; padding-top: 1px;"> 
            </asp:Label>         
        </asp:TableCell>
        <asp:TableCell ID="CellLigneApresMidi" BackColor="#f0f0f0" BorderColor="#A8BBB8" BorderStyle="Notset" Visible="true"> 
            <asp:Label ID="LigneApresMidi" runat="server" Height="20px" Width="71px"
                    BackColor="White" BorderColor="#A8BBB8" BorderStyle="Solid" Text=""
                    BorderWidth="1px" ForeColor="Black"
                    Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Smaller" Font-Italic="false"
                    style="margin-top: 0px; margin-left: 0px; text-indent: 0px; text-align: center; padding-top: 1px;"> 
            </asp:Label>           
        </asp:TableCell>
        <asp:TableCell ID="CellLigneApresMidiHDebut" BackColor="#f0f0f0" BorderColor="#A8BBB8" BorderStyle="Notset" Visible="true"> 
            <asp:Label ID="LigneApresMidiHDebut" runat="server" Height="20px" Width="56px"
                    BackColor="White" BorderColor="#A8BBB8" BorderStyle="Solid" Text=""
                    BorderWidth="1px" ForeColor="Black"
                    Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Smaller" Font-Italic="false"
                    style="margin-top: 0px; margin-left: 0px; text-indent: 0px; text-align: center; padding-top: 1px;"> 
            </asp:Label>        
        </asp:TableCell>
        <asp:TableCell ID="CellLigneApresMidiHFin" BackColor="#f0f0f0" BorderColor="#A8BBB8" BorderStyle="Notset" Visible="true"> 
            <asp:Label ID="LigneApresMidiHFin" runat="server" Height="20px" Width="55px"
                    BackColor="White" BorderColor="#A8BBB8" BorderStyle="Solid" Text=""
                    BorderWidth="1px" ForeColor="Black"
                    Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Smaller" Font-Italic="false"
                    style="margin-top: 0px; margin-left: 0px; text-indent: 0px; text-align: center; padding-top: 1px;"> 
            </asp:Label>           
        </asp:TableCell>
        <asp:TableCell ID="CellLigneTotalJour" BackColor="#f0f0f0" BorderColor="#A8BBB8" BorderStyle="Notset" Visible="true"> 
            <asp:Label ID="LigneTotalJour" runat="server" Height="20px" Width="60px"
                    BackColor="White" BorderColor="#A8BBB8" BorderStyle="Solid" Text=""
                    BorderWidth="1px" ForeColor="Black"
                    Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Smaller" Font-Italic="false"
                    style="margin-top: 0px; margin-left: 0px; text-indent: 0px; text-align: center; padding-top: 1px;"> 
            </asp:Label>           
        </asp:TableCell>
        <asp:TableCell ID="CellLigneTotalSem" BackColor="#f0f0f0" BorderColor="#A8BBB8" BorderStyle="Notset" Visible="true"> 
            <asp:Label ID="LigneTotalSem" runat="server" Height="20px" Width="61px"
                    BackColor="White" BorderColor="#A8BBB8" BorderStyle="Solid" Text=""
                    BorderWidth="1px" ForeColor="Black"
                    Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Smaller" Font-Italic="false"
                    style="margin-top: 0px; margin-left: 0px; text-indent: 0px; text-align: center; padding-top: 1px;"> 
            </asp:Label>           
        </asp:TableCell>           
    </asp:TableRow>
 </asp:Table>