﻿Option Strict On
Option Explicit On
Option Compare Text
Imports VI = Virtualia.Systeme.Constantes
Public Class Controles_VLigneDetailMensuel
    Inherits System.Web.UI.UserControl
    Private WsNomState As String = "VJour"
    Private WsCacheMaj As ArrayList

    Public Sub Initialiser()
        LabelDate.Text = ""
        LigneMatin.Text = ""
        LigneMatin.ToolTip = ""
        LigneApresMidi.Text = ""
        LigneApresMidi.ToolTip = ""
        LigneMatinHDebut.Text = ""
        LigneMatinHFin.Text = ""
        LigneApresMidiHDebut.Text = ""
        LigneApresMidiHFin.Text = ""
        LigneTotalJour.Text = ""
        LigneTotalSem.Text = ""
    End Sub

    Public WriteOnly Property VText_Jour As String
        Set(ByVal value As String)
            LabelDate.ForeColor = Drawing.Color.Black
            LabelDate.Text = value
            If value = "" Then
                LabelDate.ForeColor = LabelDate.BackColor
                LabelDate.Text = "..."
            End If
        End Set
    End Property

    Public WriteOnly Property VText_Matin As String
        Set(ByVal value As String)
            LigneMatin.ForeColor = Drawing.Color.Black
            LigneMatin.Text = value
            If value = "" Then
                LigneMatin.ForeColor = LigneMatin.BackColor
                LigneMatin.Text = "..."
            End If
        End Set
    End Property

    Public WriteOnly Property VToolTip_Matin As String
        Set(ByVal value As String)
            LigneMatin.ToolTip = value
        End Set
    End Property

    Public WriteOnly Property VText_Soir As String
        Set(ByVal value As String)
            LigneApresMidi.ForeColor = Drawing.Color.Black
            LigneApresMidi.Text = value
            If value = "" Then
                LigneApresMidi.ForeColor = LigneApresMidi.BackColor
                LigneApresMidi.Text = "..."
            End If
        End Set
    End Property

    Public WriteOnly Property VToolTip_Soir As String
        Set(ByVal value As String)
            LigneApresMidi.ToolTip = value
        End Set
    End Property

    Public WriteOnly Property VBackColor_Matin() As System.Drawing.Color
        Set(ByVal value As System.Drawing.Color)
            LigneMatin.BackColor = value
            LigneMatinHDebut.BackColor = value
            LigneMatinHFin.BackColor = value
        End Set
    End Property

    Public WriteOnly Property VBackColor_Soir() As System.Drawing.Color
        Set(ByVal value As System.Drawing.Color)
            LigneApresMidi.BackColor = value
            LigneApresMidiHDebut.BackColor = value
            LigneApresMidiHFin.BackColor = value
        End Set
    End Property

    Public WriteOnly Property VTotal_Comptabilise As String
        Set(ByVal value As String)
            LigneTotalJour.ForeColor = Drawing.Color.Black
            LigneTotalJour.Text = value
            If value = "" Then
                LigneTotalJour.ForeColor = LigneTotalJour.BackColor
                LigneTotalJour.Text = "..."
                Exit Property
            End If
            If Strings.Left(value, 1) = "-" Then
                LigneTotalJour.ForeColor = Drawing.Color.Red
            End If
        End Set
    End Property

    Public WriteOnly Property VTotal_Cumule As String
        Set(ByVal value As String)
            LigneTotalSem.ForeColor = Drawing.Color.Black
            LigneTotalSem.Text = value
            If value = "" Then
                LigneTotalSem.ForeColor = LigneTotalSem.BackColor
                LigneTotalSem.Text = "..."
                Exit Property
            End If
            If Strings.Left(value, 1) = "-" Then
                LigneTotalSem.ForeColor = Drawing.Color.Red
            End If
        End Set
    End Property

    Public Property VHeures() As ArrayList
        Get
            If Me.ViewState(WsNomState) Is Nothing Then
                Return Nothing
            End If
            WsCacheMaj = CType(Me.ViewState(WsNomState), ArrayList)
            Return WsCacheMaj
        End Get
        Set(ByVal value As ArrayList)
            WsCacheMaj = New ArrayList
            If value.Count >= 2 Then
                LigneMatinHDebut.ForeColor = Drawing.Color.Black
                LigneMatinHFin.ForeColor = Drawing.Color.Black
                LigneMatinHDebut.Text = value(0).ToString
                LigneMatinHFin.Text = value(1).ToString
                WsCacheMaj.Add(value(0))
                WsCacheMaj.Add(value(1))
            Else
                LigneMatinHDebut.ForeColor = LigneMatinHDebut.BackColor
                LigneMatinHFin.ForeColor = LigneMatinHFin.BackColor
                LigneMatinHDebut.Text = "..."
                LigneMatinHFin.Text = "..."
                WsCacheMaj.Add("")
                WsCacheMaj.Add("")
            End If
            If value.Count >= 4 Then
                LigneApresMidiHDebut.ForeColor = Drawing.Color.Black
                LigneApresMidiHFin.ForeColor = Drawing.Color.Black
                LigneApresMidiHDebut.Text = value(2).ToString
                LigneApresMidiHFin.Text = value(3).ToString
                WsCacheMaj.Add(value(2))
                WsCacheMaj.Add(value(3))
            Else
                LigneApresMidiHDebut.ForeColor = LigneApresMidiHDebut.BackColor
                LigneApresMidiHFin.ForeColor = LigneApresMidiHFin.BackColor
                LigneApresMidiHDebut.Text = "..."
                LigneApresMidiHFin.Text = "..."
                WsCacheMaj.Add("")
                WsCacheMaj.Add("")
            End If
            If Me.ViewState(WsNomState) IsNot Nothing Then
                Me.ViewState.Remove(WsNomState)
            End If
            Me.ViewState.Add(WsNomState, WsCacheMaj)
            Dim chaine As String = VTotalJour_EnHeures
        End Set
    End Property

    Public ReadOnly Property VTotalJour_EnHeures() As String
        Get
            Dim TotHeures As Integer
            Dim TotJour As Integer = 0
            Dim Chaine As String

            TotJour = VTotalJour_EnMinutes
            TotHeures = VTotalJour_EnMinutes \ 60
            Chaine = TotHeures.ToString & " h " & Strings.Format(TotJour Mod 60, "00")
            Return Chaine
        End Get
    End Property

    Public ReadOnly Property VTotalJour_EnMinutes() As Integer
        Get
            Dim TableauW(0) As String
            Dim Minutes_A As Integer
            Dim Minutes_D As Integer
            Dim TotMinutes As Integer = 0

            If Me.ViewState(WsNomState) Is Nothing Then
                Return 0
            End If
            WsCacheMaj = CType(Me.ViewState(WsNomState), ArrayList)

            If WsCacheMaj(0).ToString <> "" And WsCacheMaj(1).ToString <> "" Then
                TableauW = Strings.Split(WsCacheMaj(0).ToString, ":")
                Minutes_A = (CInt(TableauW(0)) * 60) + CInt(TableauW(1))
                TableauW = Strings.Split(WsCacheMaj(1).ToString, ":")
                Minutes_D = (CInt(TableauW(0)) * 60) + CInt(TableauW(1))
                TotMinutes = Minutes_D - Minutes_A
            End If
            If WsCacheMaj(2).ToString <> "" And WsCacheMaj(3).ToString <> "" Then
                TableauW = Strings.Split(WsCacheMaj(2).ToString, ":")
                Minutes_A = (CInt(TableauW(0)) * 60) + CInt(TableauW(1))
                TableauW = Strings.Split(WsCacheMaj(3).ToString, ":")
                Minutes_D = (CInt(TableauW(0)) * 60) + CInt(TableauW(1))
                TotMinutes += Minutes_D - Minutes_A
            End If
            Return TotMinutes
        End Get
    End Property
End Class