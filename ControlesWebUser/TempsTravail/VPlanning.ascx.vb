﻿Option Strict On
Option Explicit On
Option Compare Text
Imports VI = Virtualia.Systeme.Constantes
Imports Microsoft.VisualBasic
Partial Class Controles_VPlanning
    Inherits System.Web.UI.UserControl
    Private WebFct As Virtualia.Net.WebFonctions
    Private WsTypePlanning As String = "I" 'I=Individuel, C=Collectif
    Private WsIde_Dossier As Integer = 0
    Private WsLstIde_Dossier As String = ""
    'Execution
    Private WsNomStateIde As String = "VplanningIde"
    Private WsNomStateParam As String = "VplanningParam"
    Private WsCacheIde As ArrayList
    Private WsCacheParam As ArrayList
    Private WsDossierRtt As Virtualia.Metier.TempsTravail.DossierRTT
    Private WsPlanningRtt As Virtualia.Metier.TempsTravail.PlanningIndividuel
    Private WsListeVues As List(Of Virtualia.TablesObjet.ShemaVUE.VUE_JourTravail)

    Public Property Identifiant() As Integer
        Get
            If Me.ViewState(WsNomStateIde) IsNot Nothing Then
                WsCacheIde = CType(Me.ViewState(WsNomStateIde), ArrayList)
                WsIde_Dossier = CInt(WsCacheIde(0))
            End If
            Return WsIde_Dossier
        End Get
        Set(ByVal value As Integer)
            TypeduPlanning = "I"
            If Me.ViewState(WsNomStateIde) IsNot Nothing Then
                WsCacheIde = CType(Me.ViewState(WsNomStateIde), ArrayList)
                WsIde_Dossier = CInt(WsCacheIde(0))
            End If
            If value = WsIde_Dossier Then
                Exit Property
            End If
            WsIde_Dossier = value
            If WsIde_Dossier = 0 Then
                Exit Property
            End If
            If Me.ViewState(WsNomStateIde) IsNot Nothing Then
                Me.ViewState.Remove(WsNomStateIde)
            End If
            '** MettreEnCache le dossier **
            WsCacheIde = New ArrayList
            WsCacheIde.Add(WsIde_Dossier)
            Me.ViewState.Add(WsNomStateIde, WsCacheIde)
            '**
            WsDossierRtt = WebFct.PointeurUtilisateur.PointeurDllRTT.ItemDossier(WsIde_Dossier)
            If WsDossierRtt Is Nothing Then
                WebFct.PointeurUtilisateur.PointeurDllRTT.Identifiant(WsIde_Dossier) = _
                        WebFct.PointeurDossier(WsIde_Dossier).V_ListeDesFiches( _
                            WebFct.PointeurUtilisateur.PointeurDllRTT.ObjetsDllARTT)

                WsDossierRtt = WebFct.PointeurUtilisateur.PointeurDllRTT.ItemDossier(WsIde_Dossier)
            End If
        End Set
    End Property

    Public Property Identifiants() As String
        Get
            Return WsLstIde_Dossier
        End Get
        Set(ByVal value As String)
            TypeduPlanning = "C"
            If value = WsLstIde_Dossier Then
                Exit Property
            End If
            WsLstIde_Dossier = value
            If WsLstIde_Dossier = "" Then
                Exit Property
            End If
            If Me.ViewState(WsNomStateIde) IsNot Nothing Then
                Me.ViewState.Remove(WsNomStateIde)
            End If
            '** MettreEnCache le dossier **
            Dim TableauIde(0) As String
            Dim IndiceI As Integer
            TableauIde = Strings.Split(WsLstIde_Dossier, VI.PointVirgule, -1)
            WsCacheIde = New ArrayList
            For IndiceI = 0 To TableauIde.Count - 1
                WsCacheIde.Add(TableauIde(IndiceI))
            Next IndiceI
            Me.ViewState.Add(WsNomStateIde, WsCacheIde)
            '**
            If Me.ViewState(WsNomStateParam) IsNot Nothing Then
                Me.ViewState.Remove(WsNomStateParam)
            End If
        End Set
    End Property

    Public Property TypeduPlanning() As String
        Get
            Return WsTypePlanning
        End Get
        Set(ByVal value As String)
            WsTypePlanning = value
        End Set
    End Property

    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        WebFct = New Virtualia.Net.WebFonctions(Me, 0)
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Call FairePlanning()
    End Sub

    Protected Sub LibelJours_PaginationChange(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.DonneeChangeEventArgs) Handles LibelJours.PaginationChange
        Dim Mois As Integer
        Dim Annee As Integer
        Dim Nbmois As Integer
        Dim NbIde As Integer
        Dim NoPage As Integer
        Dim NbMaxiPages As Integer

        If Me.ViewState(WsNomStateParam) Is Nothing Then
            Exit Sub
        End If
        WsCacheParam = CType(Me.ViewState(WsNomStateParam), ArrayList)
        Annee = CInt(WsCacheParam(0))
        Mois = CInt(WsCacheParam(1))
        Nbmois = CInt(WsCacheParam(2))
        NbIde = CInt(WsCacheParam(3))
        NoPage = CInt(WsCacheParam(4))

        NbMaxiPages = (NbIde \ 18) + 1
        Select Case e.Parametre
            Case Is = "Precedente"
                NoPage -= 1
            Case Is = "Suivante"
                NoPage += 1
        End Select
        If NoPage > NbMaxiPages Then
            NoPage = NbMaxiPages
        End If
        If NoPage < 1 Then
            NoPage = 1
        End If
        If Me.ViewState(WsNomStateParam) IsNot Nothing Then
            Me.ViewState.Remove(WsNomStateParam)
        End If
        '** MettreEnCache les paramètres **
        WsCacheParam = New ArrayList
        WsCacheParam.Add(Annee)
        WsCacheParam.Add(Mois)
        WsCacheParam.Add(Nbmois)
        WsCacheParam.Add(NbIde)
        WsCacheParam.Add(NoPage)
        Me.ViewState.Add(WsNomStateParam, WsCacheParam)

    End Sub

    Protected Sub LibelJours_ValeurChange(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.DonneeChangeEventArgs) Handles LibelJours.ValeurChange
        Dim Mois As Integer
        Dim Annee As Integer
        Dim Nbmois As Integer
        Dim NbIde As Integer
        Dim NoPage As Integer
        Dim TableauW(0) As String

        If Me.ViewState(WsNomStateParam) Is Nothing Then
            Exit Sub
        End If
        WsCacheParam = CType(Me.ViewState(WsNomStateParam), ArrayList)
        Annee = CInt(WsCacheParam(0))
        Mois = CInt(WsCacheParam(1))
        Nbmois = CInt(WsCacheParam(2))
        NbIde = CInt(WsCacheParam(3))
        NoPage = CInt(WsCacheParam(4))

        TableauW = Strings.Split(e.Valeur, Strings.Space(1), -1)
        Select Case e.Parametre
            Case Is = "Mois"
                Annee = CInt(TableauW(1))
                Mois = WebFct.ViRhDates.IndexduMois(TableauW(0))
            Case Is = "Nombre"
                Nbmois = CInt(TableauW(0))
        End Select
        If Me.ViewState(WsNomStateParam) IsNot Nothing Then
            Me.ViewState.Remove(WsNomStateParam)
        End If
        '** MettreEnCache les paramètres **
        WsCacheParam = New ArrayList
        WsCacheParam.Add(Annee)
        WsCacheParam.Add(Mois)
        WsCacheParam.Add(Nbmois)
        WsCacheParam.Add(NbIde)
        WsCacheParam.Add(NoPage)
        Me.ViewState.Add(WsNomStateParam, WsCacheParam)

    End Sub

    Private Sub FairePlanning()
        Dim Mois As Integer
        Dim Annee As Integer
        Dim NbIde As Integer
        Dim DateW As String
        Dim NoJour As Integer
        Dim NbMois As Integer
        Dim NoPage As Integer
        Dim NbLignes As Integer
        Dim EcartIde As Integer = 0

        If Me.ViewState(WsNomStateIde) IsNot Nothing Then
            WsCacheIde = CType(Me.ViewState(WsNomStateIde), ArrayList)
            NbIde = WsCacheIde.Count
        Else
            WsCacheIde = New ArrayList
            WsCacheIde.Add(0)
            NbIde = 1
        End If

        If Me.ViewState(WsNomStateParam) Is Nothing Then
            Annee = CInt(Strings.Right(WebFct.ViRhDates.DateduJour(False), 4))
            Select Case WsTypePlanning
                Case Is = "I"
                    Mois = 1
                    NbMois = 12
                Case Is = "C"
                    Mois = CInt(Strings.Mid(WebFct.ViRhDates.DateduJour(False), 4, 2))
                    NbMois = 1
            End Select
            NoPage = 1
            WsCacheParam = New ArrayList
            WsCacheParam.Add(Annee)
            WsCacheParam.Add(Mois)
            WsCacheParam.Add(NbMois)
            WsCacheParam.Add(NbIde)
            WsCacheParam.Add(NoPage)
            Me.ViewState.Add(WsNomStateParam, WsCacheParam)
        Else
            WsCacheParam = CType(Me.ViewState(WsNomStateParam), ArrayList)
            Annee = CInt(WsCacheParam(0))
            Mois = CInt(WsCacheParam(1))
            NbMois = CInt(WsCacheParam(2))
            NbIde = CInt(WsCacheParam(3))
            NoPage = CInt(WsCacheParam(4))
        End If

        Select Case WsTypePlanning
            Case Is = "I"
                NbLignes = NbMois - 1
                NoPage = 1
                Call InitialiserListeMois(NbMois)
            Case Is = "C"
                EcartIde = (NoPage - 1) * 18
                NbLignes = (NbIde - ((NoPage - 1) * 18)) - 1
                If NbLignes > 17 Then
                    NbLignes = 17
                End If
                Call InitialiserListeMois(NbLignes + 1)
        End Select

        Select Case WsTypePlanning
            Case Is = "I"
                LibelJours.SiLstNbMoisVisible = True
                LibelJours.SiEtiSemaineVisible = False
                LibelJours.SiPaginationVisible = False

            Case Is = "C"
                LibelJours.SelectionLibelMois = WebFct.ViRhDates.MoisEnClair(CShort(Mois)) & Strings.Space(1) & Annee.ToString
                LibelJours.SiLstNbMoisVisible = False
                LibelJours.SiEtiSemaineVisible = True
                LibelJours.SiPaginationVisible = True
                LibelJours.EtiPagination = NoPage.ToString & " / " & ((NbIde \ 18) + 1).ToString
        End Select

        Dim IndiceM As Integer
        Dim IndiceA As Integer = Annee
        Dim IndiceNb As Integer = 0
        Dim IndiceJ As Integer
        Dim IndiceCtl As Integer
        Dim ISemaine As Integer = 0
        Dim Ctl As Control
        Dim VirControle As Controles_VMoisPlanning
        Dim DateDebut As String
        Dim DateFin As String

        IndiceM = Mois

        DateDebut = "01/" & Strings.Format(IndiceM, "00") & "/" & IndiceA.ToString
        WsPlanningRtt = Nothing
        Select Case WsTypePlanning
            Case Is = "I"
                If Identifiant > 0 Then
                    WsDossierRtt = WebFct.PointeurUtilisateur.PointeurDllRTT.ItemDossier(WsIde_Dossier)
                    If IndiceM + NbMois - 1 > 12 Then
                        DateFin = WebFct.ViRhDates.DateSaisieVerifiee("31/" & Strings.Format(IndiceM + NbMois - 13, "00") & "/" & (IndiceA + 1).ToString)
                    Else
                        DateFin = WebFct.ViRhDates.DateSaisieVerifiee("31/" & Strings.Format(IndiceM + NbMois - 1, "00") & "/" & IndiceA.ToString)
                    End If
                    WsPlanningRtt = WsDossierRtt.PlanningIndividuel(DateDebut, DateFin)
                    WsListeVues = WsPlanningRtt.ListedesVues(DateDebut, DateFin)
                    LibelJours.LibelSelection = WsDossierRtt.Nom & Strings.Space(1) & WsDossierRtt.Prenom
                End If
        End Select

        Do  'Traitement du mois
            DateW = "01/" & Strings.Format(IndiceM, "00") & "/" & IndiceA.ToString
            NoJour = Weekday(DateValue(DateW), FirstDayOfWeek.Monday) - 1

            Ctl = WebFct.VirWebControle(Me.CadrePlanning, "PlanningM", IndiceNb)
            If Ctl IsNot Nothing Then
                VirControle = CType(Ctl, Controles_VMoisPlanning)
                VirControle.LibelleMois = WebFct.ViRhDates.MoisEnClair(CShort(IndiceM))
                VirControle.LibelleAnnee = IndiceA.ToString
                'VirControle.VJoursStyle = "none"
                Select Case WsTypePlanning
                    Case Is = "I"
                        VirControle.SiEtiSelectionVisible = False
                    Case Is = "C"
                        VirControle.SiEtiSelectionVisible = True
                        VirControle.EtiSelectionText = WsCacheIde(IndiceNb + EcartIde).ToString
                        If ISemaine <> 99 Then
                            For ISemaine = 0 To 5
                                LibelJours.VNumSemaine(ISemaine) = 0
                            Next ISemaine
                            ISemaine = 0
                            LibelJours.VNumSemaine(ISemaine) = WebFct.ViRhDates.NumerodelaSemaine(DateW)
                            If NoJour > 0 Then
                                ISemaine = 1
                            End If
                        End If
                End Select
                For IndiceCtl = 0 To NoJour - 1
                    VirControle.SiJourVisible(IndiceCtl, "JAM") = False
                    VirControle.SiJourVisible(IndiceCtl, "JPM") = False
                Next IndiceCtl
            End If
            IndiceJ = NoJour

            Do 'Traitement des jours du mois
                If CInt(Strings.Mid(DateW, 4, 2)) <> IndiceM Then
                    If Ctl IsNot Nothing Then
                        VirControle = CType(Ctl, Controles_VMoisPlanning)
                        For IndiceCtl = IndiceJ To 36
                            VirControle.SiJourVisible(IndiceCtl, "JAM") = False
                            VirControle.SiJourVisible(IndiceCtl, "JPM") = False
                        Next IndiceCtl
                    End If
                    Exit Do
                End If
                Select Case WsTypePlanning
                    Case Is = "C"
                        If NoJour = 0 And ISemaine <> 99 Then
                            LibelJours.VNumSemaine(ISemaine) = WebFct.ViRhDates.NumerodelaSemaine(DateW)
                            ISemaine += 1
                        End If
                End Select
                If Ctl IsNot Nothing Then
                    VirControle = CType(Ctl, Controles_VMoisPlanning)
                    VirControle.SiJourVisible(IndiceJ, "JAM") = True
                    VirControle.SiJourVisible(IndiceJ, "JPM") = True
                    VirControle.VJourStyle(IndiceJ, "JAM") = "none"
                    VirControle.VJourStyle(IndiceJ, "JPM") = "none"
                    VirControle.VBorderColor(IndiceJ, "JAM") = WebFct.ConvertCouleur("#216B68")
                    VirControle.VBorderColor(IndiceJ, "JPM") = WebFct.ConvertCouleur("#216B68")
                    VirControle.VText(IndiceJ, "JAM") = CShort(Strings.Left(DateW, 2)).ToString
                    VirControle.VDemiJourToolTip(IndiceJ, "JAM") = WebFct.ViRhDates.ClairDate(DateW, True)
                    VirControle.VDemiJourToolTip(IndiceJ, "JPM") = ""
                    If WebFct.ViRhDates.SiJourOuvre(DateW, True) = True Then
                        VirControle.VBackColor(IndiceJ, "JAM") = WebFct.PointeurUtilisateur.PointeurParametres.CouleurPlanning(0)
                        VirControle.VBackColor(IndiceJ, "JPM") = WebFct.PointeurUtilisateur.PointeurParametres.CouleurPlanning(0)
                    Else
                        VirControle.VBackColor(IndiceJ, "JAM") = WebFct.PointeurUtilisateur.PointeurParametres.CouleurPlanning(1)
                        VirControle.VBackColor(IndiceJ, "JPM") = WebFct.PointeurUtilisateur.PointeurParametres.CouleurPlanning(1)
                        If WebFct.ViRhDates.SiJourFerie(DateW) = True Then
                            VirControle.VBorderColor(IndiceJ, "JAM") = Drawing.Color.Red
                            VirControle.VBorderColor(IndiceJ, "JPM") = Drawing.Color.Red
                        End If
                    End If
                    Select Case WsTypePlanning
                        Case Is = "I"
                            If WsIde_Dossier > 0 Then
                                Call DessinerJour(DateW, VirControle, IndiceJ)
                            End If
                    End Select
                End If

                DateW = WebFct.ViRhDates.CalcDateMoinsJour(DateW, "1", "0")
                NoJour = Weekday(DateValue(DateW), FirstDayOfWeek.Monday) - 1
                IndiceJ += 1
            Loop

            IndiceNb += 1
            If IndiceNb > NbLignes Or IndiceNb > 17 Then
                Exit Do
            End If

            Select Case WsTypePlanning
                Case Is = "I"
                    IndiceM += 1
                    If IndiceM > 12 Then
                        IndiceM = 1
                        IndiceA += 1
                    End If
                Case Is = "C"
                    ISemaine = 99
            End Select
        Loop

    End Sub

    Private Sub DessinerJour(ByVal DateValeur As String, ByVal CtlMois As Controles_VMoisPlanning, ByVal IndexJour As Integer)
        Dim FicheVue As Virtualia.TablesObjet.ShemaVUE.VUE_JourTravail
        Dim Couleur As Drawing.Color
        Dim ChaineTip As System.Text.StringBuilder
        Dim ChaineConstatee(1) As String
        Dim IndiceP As Integer
        Dim RetourChariot As String = Chr(13) & Chr(10)

        FicheVue = WsListeVues.Find(Function(Recherche) Recherche.Date_de_Valeur = DateValeur)

        If FicheVue Is Nothing Then
            If WsDossierRtt.SiEnActivite(DateValeur) = True Then
                CtlMois.VBackColor(IndexJour, "JAM") = WebFct.PointeurUtilisateur.PointeurParametres.CouleurPlanning(1)
                CtlMois.VBackColor(IndexJour, "JPM") = WebFct.PointeurUtilisateur.PointeurParametres.CouleurPlanning(1)
            Else
                CtlMois.VBackColor(IndexJour, "JAM") = WebFct.ConvertCouleur("#73829A")
                CtlMois.VBackColor(IndexJour, "JPM") = WebFct.ConvertCouleur("#73829A")
                CtlMois.VDemiJourToolTip(IndexJour, "JAM") = "Hors effectif"
                CtlMois.VDemiJourToolTip(IndexJour, "JPM") = "Hors effectif"
            End If
            Exit Sub
        End If
        ChaineTip = New System.Text.StringBuilder
        ChaineTip.Append("Semaine " & FicheVue.NumeroSemaine & RetourChariot)
        If FicheVue.IntituleduJour <> "" Then
            ChaineTip.Append(FicheVue.IntituleduJour & RetourChariot)
        Else
            ChaineTip.Append(WebFct.ViRhDates.ClairDate(DateValeur, True) & RetourChariot)
        End If
        If FicheVue.DureeComptabiliseeEnMinutes > 0 Then
            ChaineTip.Append("Durée comptablisée = " & FicheVue.DureeComptabilisee & RetourChariot)
        End If
        If FicheVue.DureeConstateeEnMinutes > 0 Then
            ChaineTip.Append("Durée constatée = " & FicheVue.DureeConstatee & RetourChariot)
        End If
        If FicheVue.DebitCredit_Minutes <> 0 Then
            ChaineTip.Append("Débit-Crédit = " & FicheVue.DebitCredit & RetourChariot)
        End If

        For IndiceP = FicheVue.NombredeFiches - 1 To 0 Step -1
            Couleur = Nothing
            ChaineConstatee.Initialize()
            If FicheVue.ItemPlage(IndiceP).EvenementPlage <> "" Then
                Select Case FicheVue.ItemPlage(IndiceP).SiPrevisionIntranet
                    Case Is = 1
                        Couleur = WebFct.PointeurUtilisateur.PointeurParametres.CouleurPlanning(4)
                    Case Else
                        Couleur = WebFct.PointeurUtilisateur.PointeurParametres.CouleurPlanning(WebFct.PointeurUtilisateur.PointeurParametres.IndexCouleurPlanning(FicheVue.ItemPlage(IndiceP).EvenementPlage))
                End Select
            End If
            Select Case FicheVue.ItemPlage(IndiceP).NumeroPlage
                Case Is = VI.NumeroPlage.Plage1_Presence
                    If FicheVue.ItemPlage(IndiceP).Horaire_Constate_Minutes > 0 And FicheVue.ItemPlage(IndiceP).HeureDebut_Constate <> "" Then
                        ChaineConstatee(0) = FicheVue.ItemPlage(IndiceP).HeureDebut_Constate & " - " & FicheVue.ItemPlage(IndiceP).HeureFin_Constate
                    End If
                    If FicheVue.ItemPlage(IndiceP).HoraireCycle_Prevu_Minutes = 0 Then
                        CtlMois.VBackColor(IndexJour, "JAM") = WebFct.PointeurUtilisateur.PointeurParametres.CouleurPlanning(1)
                    End If
                Case Is = VI.NumeroPlage.Plage2_Presence
                    If FicheVue.ItemPlage(IndiceP).Horaire_Constate_Minutes > 0 And FicheVue.ItemPlage(IndiceP).HeureDebut_Constate <> "" Then
                        ChaineConstatee(1) = FicheVue.ItemPlage(IndiceP).HeureDebut_Constate & " - " & FicheVue.ItemPlage(IndiceP).HeureFin_Constate
                    End If
                    If FicheVue.ItemPlage(IndiceP).HoraireCycle_Prevu_Minutes = 0 Then
                        CtlMois.VBackColor(IndexJour, "JPM") = WebFct.PointeurUtilisateur.PointeurParametres.CouleurPlanning(1)
                    End If
                Case Is = VI.NumeroPlage.Jour_Absence
                    If FicheVue.SiJourFerie = False Or FicheVue.DureePrevueCycleEnMinutes > 0 Then
                        CtlMois.VBackColor(IndexJour, "JAM") = Couleur
                        CtlMois.VBackColor(IndexJour, "JPM") = Couleur
                    End If
                Case Is = VI.NumeroPlage.Plage1_Absence
                    CtlMois.VBackColor(IndexJour, "JAM") = Couleur
                    CtlMois.VJourStyle(IndexJour, "JAM") = "solid"
                    If FicheVue.ItemPlage(IndiceP).EvenementPlage <> "" Then
                        ChaineTip.Append(FicheVue.ItemPlage(IndiceP).EvenementPlage & RetourChariot)
                    End If
                Case Is = VI.NumeroPlage.Plage2_Absence
                    CtlMois.VBackColor(IndexJour, "JPM") = Couleur
                    CtlMois.VJourStyle(IndexJour, "JPM") = "solid"
                    If FicheVue.ItemPlage(IndiceP).EvenementPlage <> "" Then
                        ChaineTip.Append(FicheVue.ItemPlage(IndiceP).EvenementPlage & RetourChariot)
                    End If
                Case Is = VI.NumeroPlage.Jour_Formation
                    Couleur = WebFct.PointeurUtilisateur.PointeurParametres.CouleurPlanning(WebFct.PointeurUtilisateur.PointeurParametres.IndexCouleurPlanning("Formation"))
                    CtlMois.VBackColor(IndexJour, "JAM") = Couleur
                    CtlMois.VBackColor(IndexJour, "JPM") = Couleur
                    If FicheVue.ItemPlage(IndiceP).EvenementPlage <> "" Then
                        ChaineTip.Append(FicheVue.ItemPlage(IndiceP).EvenementPlage & RetourChariot)
                    End If
                Case Is = VI.NumeroPlage.Plage1_Formation
                    Couleur = WebFct.PointeurUtilisateur.PointeurParametres.CouleurPlanning(WebFct.PointeurUtilisateur.PointeurParametres.IndexCouleurPlanning("Formation"))
                    CtlMois.VBackColor(IndexJour, "JAM") = Couleur
                    CtlMois.VJourStyle(IndexJour, "JAM") = "solid"
                    If FicheVue.ItemPlage(IndiceP).EvenementPlage <> "" Then
                        ChaineTip.Append(FicheVue.ItemPlage(IndiceP).EvenementPlage & RetourChariot)
                    End If
                Case Is = VI.NumeroPlage.Plage2_Formation
                    Couleur = WebFct.PointeurUtilisateur.PointeurParametres.CouleurPlanning(WebFct.PointeurUtilisateur.PointeurParametres.IndexCouleurPlanning("Formation"))
                    CtlMois.VBackColor(IndexJour, "JPM") = Couleur
                    CtlMois.VJourStyle(IndexJour, "JPM") = "solid"
                    If FicheVue.ItemPlage(IndiceP).EvenementPlage <> "" Then
                        ChaineTip.Append(FicheVue.ItemPlage(IndiceP).EvenementPlage & RetourChariot)
                    End If
                Case Is = VI.NumeroPlage.Jour_Mission
                    Couleur = WebFct.PointeurUtilisateur.PointeurParametres.CouleurPlanning(WebFct.PointeurUtilisateur.PointeurParametres.IndexCouleurPlanning("Mission"))
                    CtlMois.VBackColor(IndexJour, "JAM") = Couleur
                    CtlMois.VBackColor(IndexJour, "JPM") = Couleur
                    If FicheVue.ItemPlage(IndiceP).EvenementPlage <> "" Then
                        ChaineTip.Append(FicheVue.ItemPlage(IndiceP).EvenementPlage & RetourChariot)
                    End If
                Case Is = VI.NumeroPlage.Plage1_Mission
                    Couleur = WebFct.PointeurUtilisateur.PointeurParametres.CouleurPlanning(WebFct.PointeurUtilisateur.PointeurParametres.IndexCouleurPlanning("Mission"))
                    CtlMois.VBackColor(IndexJour, "JAM") = Couleur
                    CtlMois.VJourStyle(IndexJour, "JAM") = "solid"
                    If FicheVue.ItemPlage(IndiceP).EvenementPlage <> "" Then
                        ChaineTip.Append(FicheVue.ItemPlage(IndiceP).EvenementPlage & RetourChariot)
                    End If
                Case Is = VI.NumeroPlage.Plage2_Mission
                    Couleur = WebFct.PointeurUtilisateur.PointeurParametres.CouleurPlanning(WebFct.PointeurUtilisateur.PointeurParametres.IndexCouleurPlanning("Mission"))
                    CtlMois.VBackColor(IndexJour, "JPM") = Couleur
                    CtlMois.VJourStyle(IndexJour, "JPM") = "solid"
                    If FicheVue.ItemPlage(IndiceP).EvenementPlage <> "" Then
                        ChaineTip.Append(FicheVue.ItemPlage(IndiceP).EvenementPlage & RetourChariot)
                    End If
            End Select
        Next IndiceP
        For IndiceP = 0 To 1
            If ChaineConstatee(IndiceP) <> "" Then
                ChaineTip.Append(ChaineConstatee(IndiceP) & RetourChariot)
            End If
        Next IndiceP
        CtlMois.VDemiJourToolTip(IndexJour, "JAM") = ChaineTip.ToString
        CtlMois.VDemiJourToolTip(IndexJour, "JPM") = ChaineTip.ToString
    End Sub

    Private Sub InitialiserListeMois(ByVal NbMois As Integer)
        Dim Ctl As Control
        Dim VirControle As Controles_VMoisPlanning
        Dim IndiceI As Integer = 0
        Do
            Ctl = WebFct.VirWebControle(Me.CadrePlanning, "PlanningM", IndiceI)
            If Ctl Is Nothing Then
                Exit Do
            End If
            VirControle = CType(Ctl, Controles_VMoisPlanning)
            If IndiceI < NbMois Then
                VirControle.SiMoisVisible = True
            Else
                VirControle.SiMoisVisible = False
            End If
            IndiceI += 1
        Loop
    End Sub

End Class
