﻿<%@ Control Language="VB" AutoEventWireup="false" Inherits="Virtualia.Net.Controles_VSaisieAbsence" Codebehind="VSaisieAbsence.ascx.vb" %>

<%@ Register src="../TempsTravail/VCalendrier.ascx" tagname="VCalendrier" tagprefix="Virtualia" %>


<asp:Panel ID="PanelSaisie" runat="server" BorderStyle="Solid" BorderWidth="1px" Width="820px" HorizontalAlign="Left" 
             BackColor="#E9FDF9" BorderColor="#B0E0D7" style="margin-top: 3px;"  >
     <asp:Table ID="CadreAbsence" runat="server" Width="700px" HorizontalAlign="Center">
        <asp:TableRow>
            <asp:TableCell>
                <Virtualia:VCalendrier ID="CalendDebut" runat="server" TypeCalendrier="ID" >
                </Virtualia:VCalendrier>
            </asp:TableCell>
            <asp:TableCell>
                <Virtualia:VCalendrier ID="CalendFin" runat="server" TypeCalendrier="IF" >
                </Virtualia:VCalendrier>
            </asp:TableCell>
        </asp:TableRow>
     </asp:Table>
     <asp:HiddenField ID="HMoisDebut" runat="server" Value="" />
     <asp:HiddenField ID="HDateDebut" runat="server" Value="" />
     <asp:HiddenField ID="HCocheDebut" runat="server" Value="" /> 
     <asp:HiddenField ID="HMoisFin" runat="server" Value="" />
     <asp:HiddenField ID="HDateFin" runat="server" Value="" />
     <asp:HiddenField ID="HCocheFin" runat="server" Value="" />       
</asp:Panel>
