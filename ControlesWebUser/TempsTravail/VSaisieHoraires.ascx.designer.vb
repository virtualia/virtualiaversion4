﻿'------------------------------------------------------------------------------
' <généré automatiquement>
'     Ce code a été généré par un outil.
'
'     Les modifications apportées à ce fichier peuvent provoquer un comportement incorrect et seront perdues si
'     le code est régénéré.
' </généré automatiquement>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class Controles_VSaisieHoraires

    '''<summary>
    '''Contrôle SaisieHoraires.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents SaisieHoraires As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle CadreSaisie.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CadreSaisie As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle EtiSemaine.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiSemaine As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle EtiTitreJour.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiTitreJour As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle EtiTitreHoraire.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiTitreHoraire As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle EtiTitreTotal.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiTitreTotal As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle SaisieLundi.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents SaisieLundi As Global.Virtualia.Net.Controles_VSaisieHeuresJour

    '''<summary>
    '''Contrôle SaisieMardi.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents SaisieMardi As Global.Virtualia.Net.Controles_VSaisieHeuresJour

    '''<summary>
    '''Contrôle SaisieMercredi.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents SaisieMercredi As Global.Virtualia.Net.Controles_VSaisieHeuresJour

    '''<summary>
    '''Contrôle SaisieJeudi.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents SaisieJeudi As Global.Virtualia.Net.Controles_VSaisieHeuresJour

    '''<summary>
    '''Contrôle SaisieVendredi.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents SaisieVendredi As Global.Virtualia.Net.Controles_VSaisieHeuresJour

    '''<summary>
    '''Contrôle SaisieSamedi.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents SaisieSamedi As Global.Virtualia.Net.Controles_VSaisieHeuresJour

    '''<summary>
    '''Contrôle SaisieDimanche.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents SaisieDimanche As Global.Virtualia.Net.Controles_VSaisieHeuresJour

    '''<summary>
    '''Contrôle EtiTotalSemaine.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiTotalSemaine As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle TotalSemaine.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents TotalSemaine As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle CadreAide.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CadreAide As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle EtiJour.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiJour As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle EtiMatin.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiMatin As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle EtiSoir.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiSoir As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle EtiMatinA.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiMatinA As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle EtiMatinD.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiMatinD As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle EtiSoirA.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiSoirA As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle EtiSoirD.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiSoirD As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle LstMatinA.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LstMatinA As Global.System.Web.UI.WebControls.ListBox

    '''<summary>
    '''Contrôle LstMatinD.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LstMatinD As Global.System.Web.UI.WebControls.ListBox

    '''<summary>
    '''Contrôle LstSoirA.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LstSoirA As Global.System.Web.UI.WebControls.ListBox

    '''<summary>
    '''Contrôle LstSoirD.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LstSoirD As Global.System.Web.UI.WebControls.ListBox

    '''<summary>
    '''Contrôle SelMatinA.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents SelMatinA As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle SelMatinD.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents SelMatinD As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle SelSoirA.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents SelSoirA As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle SelSoirD.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents SelSoirD As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle SelTotMatin.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents SelTotMatin As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle SelTotSoir.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents SelTotSoir As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle CadreOK.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CadreOK As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle CommandeOK.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CommandeOK As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Contrôle CadreAnnuler.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CadreAnnuler As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle CommandeAnnuler.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CommandeAnnuler As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Contrôle HSelJour.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents HSelJour As Global.System.Web.UI.WebControls.HiddenField

    '''<summary>
    '''Contrôle HSelLst.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents HSelLst As Global.System.Web.UI.WebControls.HiddenField
End Class
