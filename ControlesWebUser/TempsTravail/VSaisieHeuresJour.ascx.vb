﻿Option Strict On
Option Explicit On
Option Compare Text
Imports VI = Virtualia.Systeme.Constantes
Partial Class Controles_VSaisieHeuresJour
    Inherits System.Web.UI.UserControl
    Public Delegate Sub Valeur_ChangeEventHandler(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.DonneeChangeEventArgs)
    Public Event ValeurChange As Valeur_ChangeEventHandler
    Private WebFct As Virtualia.Net.WebFonctions
    Private WsIndexJour As Short
    Private WsNomState As String = "VJour"
    Private WsCacheMaj As ArrayList

    Public Property VIndexJour() As Short
        Get
            Return WsIndexJour
        End Get
        Set(ByVal value As Short)
            WsIndexJour = value
        End Set
    End Property

    Public WriteOnly Property VText As String
        Set(ByVal value As String)
            EtiLibelJour.Text = value
        End Set
    End Property

    Public Property VHeures() As ArrayList
        Get
            If Me.ViewState(WsNomState) Is Nothing Then
                Return Nothing
            End If
            WsCacheMaj = CType(Me.ViewState(WsNomState), ArrayList)
            Return WsCacheMaj
        End Get
        Set(ByVal value As ArrayList)
            WsCacheMaj = New ArrayList
            If value.Count >= 2 Then
                TxtMatinA.Text = value(0).ToString
                TxtMatinD.Text = value(1).ToString
                WsCacheMaj.Add(value(0))
                WsCacheMaj.Add(value(1))
            Else
                WsCacheMaj.Add("")
                WsCacheMaj.Add("")
            End If
            If value.Count >= 4 Then
                TxtSoirA.Text = value(2).ToString
                TxtSoirD.Text = value(3).ToString
                WsCacheMaj.Add(value(2))
                WsCacheMaj.Add(value(3))
            Else
                WsCacheMaj.Add("")
                WsCacheMaj.Add("")
            End If
            If Me.ViewState(WsNomState) IsNot Nothing Then
                Me.ViewState.Remove(WsNomState)
            End If
            Me.ViewState.Add(WsNomState, WsCacheMaj)
            Dim chaine As String = VTotalJour_EnHeures
        End Set
    End Property

    Public ReadOnly Property VTotalJour_EnHeures() As String
        Get
            Dim TableauW(0) As String
            Dim Chaine As String = ""
            Dim Minutes_A As Integer
            Dim Minutes_D As Integer
            Dim TotHeures As Integer
            Dim TotMinutes As Integer = 0
            Dim TotJour As Integer = 0

            If Me.ViewState(WsNomState) Is Nothing Then
                Return "0 h 00"
            End If
            WsCacheMaj = CType(Me.ViewState(WsNomState), ArrayList)

            If WsCacheMaj(0).ToString <> "" And WsCacheMaj(1).ToString <> "" Then
                TableauW = Strings.Split(WsCacheMaj(0).ToString, ":")
                Minutes_A = (CInt(TableauW(0)) * 60) + CInt(TableauW(1))
                TableauW = Strings.Split(WsCacheMaj(1).ToString, ":")
                Minutes_D = (CInt(TableauW(0)) * 60) + CInt(TableauW(1))
                TotMinutes = Minutes_D - Minutes_A
                TotHeures = TotMinutes \ 60
                Chaine = TotHeures.ToString & " h " & Strings.Format(TotMinutes Mod 60, "00")
                TotalMatin.Text = Chaine
            Else
                TotalMatin.Text = ""
            End If
            TotJour += TotMinutes
            TotMinutes = 0
            If WsCacheMaj(2).ToString <> "" And WsCacheMaj(3).ToString <> "" Then
                TableauW = Strings.Split(WsCacheMaj(2).ToString, ":")
                Minutes_A = (CInt(TableauW(0)) * 60) + CInt(TableauW(1))
                TableauW = Strings.Split(WsCacheMaj(3).ToString, ":")
                Minutes_D = (CInt(TableauW(0)) * 60) + CInt(TableauW(1))
                TotMinutes = Minutes_D - Minutes_A
                TotHeures = TotMinutes \ 60
                Chaine = TotHeures.ToString & " h " & Strings.Format(TotMinutes Mod 60, "00")
                TotalSoir.Text = Chaine
            Else
                TotalSoir.Text = ""
            End If
            If TotalMatin.Text = "" And TotalSoir.Text = "" Then
                TotalJour.Text = ""
                Return ""
            End If
            TotJour += TotMinutes
            TotHeures = TotJour \ 60
            Chaine = TotHeures.ToString & " h " & Strings.Format(TotJour Mod 60, "00")
            TotalJour.Text = Chaine
            Return Chaine
        End Get
    End Property

    Public ReadOnly Property VTotalJour_EnMinutes() As Integer
        Get
            Dim TableauW(0) As String
            Dim Minutes_A As Integer
            Dim Minutes_D As Integer
            Dim TotMinutes As Integer = 0

            If Me.ViewState(WsNomState) Is Nothing Then
                Return 0
            End If
            WsCacheMaj = CType(Me.ViewState(WsNomState), ArrayList)

            If WsCacheMaj(0).ToString <> "" And WsCacheMaj(1).ToString <> "" Then
                TableauW = Strings.Split(WsCacheMaj(0).ToString, ":")
                Minutes_A = (CInt(TableauW(0)) * 60) + CInt(TableauW(1))
                TableauW = Strings.Split(WsCacheMaj(1).ToString, ":")
                Minutes_D = (CInt(TableauW(0)) * 60) + CInt(TableauW(1))
                TotMinutes = Minutes_D - Minutes_A
            End If
            If WsCacheMaj(2).ToString <> "" And WsCacheMaj(3).ToString <> "" Then
                TableauW = Strings.Split(WsCacheMaj(2).ToString, ":")
                Minutes_A = (CInt(TableauW(0)) * 60) + CInt(TableauW(1))
                TableauW = Strings.Split(WsCacheMaj(3).ToString, ":")
                Minutes_D = (CInt(TableauW(0)) * 60) + CInt(TableauW(1))
                TotMinutes += Minutes_D - Minutes_A
            End If
            Return TotMinutes
        End Get
    End Property

    Protected Overridable Sub Saisie_Change(ByVal e As Virtualia.Systeme.Evenements.DonneeChangeEventArgs)
        RaiseEvent ValeurChange(Me, e)
    End Sub

    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        WebFct = New Virtualia.Net.WebFonctions(Me, 0)
    End Sub

    Protected Sub TxtMatinA_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles TxtMatinA.TextChanged
        Dim Chaine As String = ""

        If TxtMatinA.Text <> "" Then
            Chaine = WebFct.Controle_LostFocusStd(VI.NatureDonnee.DonneeHeure, VI.FormatDonnee.Heure, TxtMatinA.Text)
        End If
        If Me.ViewState(WsNomState) Is Nothing Then
            WsCacheMaj = New ArrayList
            WsCacheMaj.Add("")
            WsCacheMaj.Add("")
            WsCacheMaj.Add("")
            WsCacheMaj.Add("")
        Else
            WsCacheMaj = CType(Me.ViewState(WsNomState), ArrayList)
        End If
        If WsCacheMaj(1).ToString <> "" Then
            Select Case WebFct.ViRhDates.ComparerHeures(Chaine, WsCacheMaj(1).ToString)
                Case VI.ComparaisonDates.Egalite, VI.ComparaisonDates.PlusGrand
                    Chaine = WsCacheMaj(0).ToString
            End Select
        End If
        If WsCacheMaj(2).ToString <> "" Then
            Select Case WebFct.ViRhDates.ComparerHeures(Chaine, WsCacheMaj(2).ToString)
                Case VI.ComparaisonDates.Egalite, VI.ComparaisonDates.PlusGrand
                    Chaine = WsCacheMaj(0).ToString
            End Select
        End If
        TxtMatinA.Text = Chaine
        WsCacheMaj(0) = Chaine

        Chaine = VTotalJour_EnHeures

        If Me.ViewState(WsNomState) IsNot Nothing Then
            Me.ViewState.Remove(WsNomState)
        End If
        Me.ViewState.Add(WsNomState, WsCacheMaj)

        Dim Evenement As Virtualia.Systeme.Evenements.DonneeChangeEventArgs
        Evenement = New Virtualia.Systeme.Evenements.DonneeChangeEventArgs("Heure", TxtMatinA.Text)
        Saisie_Change(Evenement)
    End Sub

    Protected Sub TxtMatinD_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles TxtMatinD.TextChanged
        Dim Chaine As String = ""

        If TxtMatinD.Text <> "" Then
            Chaine = WebFct.Controle_LostFocusStd(VI.NatureDonnee.DonneeHeure, VI.FormatDonnee.Heure, TxtMatinD.Text)
        End If
        If Me.ViewState(WsNomState) Is Nothing Then
            WsCacheMaj = New ArrayList
            WsCacheMaj.Add("")
            WsCacheMaj.Add("")
            WsCacheMaj.Add("")
            WsCacheMaj.Add("")
        Else
            WsCacheMaj = CType(Me.ViewState(WsNomState), ArrayList)
        End If
        If WsCacheMaj(0).ToString <> "" Then
            Select Case WebFct.ViRhDates.ComparerHeures(Chaine, WsCacheMaj(0).ToString)
                Case VI.ComparaisonDates.Egalite, VI.ComparaisonDates.PlusPetit
                    Chaine = WsCacheMaj(1).ToString
            End Select
        End If
        If WsCacheMaj(2).ToString <> "" Then
            Select Case WebFct.ViRhDates.ComparerHeures(Chaine, WsCacheMaj(2).ToString)
                Case VI.ComparaisonDates.Egalite, VI.ComparaisonDates.PlusGrand
                    Chaine = WsCacheMaj(1).ToString
            End Select
        End If
        TxtMatinD.Text = Chaine
        WsCacheMaj(1) = Chaine

        Chaine = VTotalJour_EnHeures

        If Me.ViewState(WsNomState) IsNot Nothing Then
            Me.ViewState.Remove(WsNomState)
        End If
        Me.ViewState.Add(WsNomState, WsCacheMaj)

        Dim Evenement As Virtualia.Systeme.Evenements.DonneeChangeEventArgs
        Evenement = New Virtualia.Systeme.Evenements.DonneeChangeEventArgs("Heure", TxtMatinD.Text)
        Saisie_Change(Evenement)
    End Sub

    Protected Sub TxtSoirA_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles TxtSoirA.TextChanged
        Dim Chaine As String = ""

        If TxtSoirA.Text <> "" Then
            Chaine = WebFct.Controle_LostFocusStd(VI.NatureDonnee.DonneeHeure, VI.FormatDonnee.Heure, TxtSoirA.Text)
        End If
        If Me.ViewState(WsNomState) Is Nothing Then
            WsCacheMaj = New ArrayList
            WsCacheMaj.Add("")
            WsCacheMaj.Add("")
            WsCacheMaj.Add("")
            WsCacheMaj.Add("")
        Else
            WsCacheMaj = CType(Me.ViewState(WsNomState), ArrayList)
        End If
        If WsCacheMaj(1).ToString <> "" Then
            Select Case WebFct.ViRhDates.ComparerHeures(Chaine, WsCacheMaj(1).ToString)
                Case VI.ComparaisonDates.Egalite, VI.ComparaisonDates.PlusPetit
                    Chaine = WsCacheMaj(2).ToString
            End Select
        End If
        If WsCacheMaj(3).ToString <> "" Then
            Select Case WebFct.ViRhDates.ComparerHeures(Chaine, WsCacheMaj(3).ToString)
                Case VI.ComparaisonDates.Egalite, VI.ComparaisonDates.PlusGrand
                    Chaine = WsCacheMaj(2).ToString
            End Select
        End If
        TxtSoirA.Text = Chaine
        WsCacheMaj(2) = Chaine

        Chaine = VTotalJour_EnHeures

        If Me.ViewState(WsNomState) IsNot Nothing Then
            Me.ViewState.Remove(WsNomState)
        End If
        Me.ViewState.Add(WsNomState, WsCacheMaj)

        Dim Evenement As Virtualia.Systeme.Evenements.DonneeChangeEventArgs
        Evenement = New Virtualia.Systeme.Evenements.DonneeChangeEventArgs("Heure", TxtSoirA.Text)
        Saisie_Change(Evenement)
    End Sub

    Protected Sub TxtSoirD_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles TxtSoirD.TextChanged
        Dim Chaine As String = ""

        If TxtSoirD.Text <> "" Then
            Chaine = WebFct.Controle_LostFocusStd(VI.NatureDonnee.DonneeHeure, VI.FormatDonnee.Heure, TxtSoirD.Text)
        End If
        If Me.ViewState(WsNomState) Is Nothing Then
            WsCacheMaj = New ArrayList
            WsCacheMaj.Add("")
            WsCacheMaj.Add("")
            WsCacheMaj.Add("")
            WsCacheMaj.Add("")
        Else
            WsCacheMaj = CType(Me.ViewState(WsNomState), ArrayList)
        End If
        If WsCacheMaj(1).ToString <> "" Then
            Select Case WebFct.ViRhDates.ComparerHeures(Chaine, WsCacheMaj(1).ToString)
                Case VI.ComparaisonDates.Egalite, VI.ComparaisonDates.PlusPetit
                    Chaine = WsCacheMaj(3).ToString
            End Select
        End If
        If WsCacheMaj(2).ToString <> "" Then
            Select Case WebFct.ViRhDates.ComparerHeures(Chaine, WsCacheMaj(2).ToString)
                Case VI.ComparaisonDates.Egalite, VI.ComparaisonDates.PlusPetit
                    Chaine = WsCacheMaj(3).ToString
            End Select
        End If
        TxtSoirD.Text = Chaine
        WsCacheMaj(3) = Chaine

        Chaine = VTotalJour_EnHeures

        If Me.ViewState(WsNomState) IsNot Nothing Then
            Me.ViewState.Remove(WsNomState)
        End If
        Me.ViewState.Add(WsNomState, WsCacheMaj)

        Dim Evenement As Virtualia.Systeme.Evenements.DonneeChangeEventArgs
        Evenement = New Virtualia.Systeme.Evenements.DonneeChangeEventArgs("Heure", TxtSoirD.Text)
        Saisie_Change(Evenement)
    End Sub

    Protected Sub CommandeAide_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles CommandeAide.Click
        Dim Evenement As Virtualia.Systeme.Evenements.DonneeChangeEventArgs
        Evenement = New Virtualia.Systeme.Evenements.DonneeChangeEventArgs("Jour", WsIndexJour.ToString)
        Saisie_Change(Evenement)
    End Sub
End Class
