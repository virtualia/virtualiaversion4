﻿<%@ Control Language="VB" AutoEventWireup="false" Inherits="Virtualia.Net.Controles_VSaisieHoraires" Codebehind="VSaisieHoraires.ascx.vb" %>

<%@ Register src="../TempsTravail/VSaisieHeuresJour.ascx" tagname="VSaisieHeuresJour" tagprefix="Virtualia" %>

<asp:Table ID="SaisieHoraires" runat="server" BackColor="#F0F0F0">
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="CadreSaisie" runat="server" CellPadding="0" CellSpacing="0" Width="430px"
                        BorderStyle="Ridge" BorderColor="#B0E0D7" BorderWidth="3px" >
                <asp:TableRow>
                    <asp:TableCell BackColor="#D7FAF3" ColumnSpan="3" BorderColor="#D7FAF3" BorderStyle="Ridge" BorderWidth="1px">
                        <asp:Label ID="EtiSemaine" runat="server" Height="25px" Width="430px" BackColor="#D7FAF3"
                             ForeColor="#1C5151" BorderStyle="None" Text="semaine"
                             Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Small" Font-Italic="false"
                             style="margin-top: 1px; margin-left: 0px; text-indent:1px; text-align: center; vertical-align: top" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell BackColor="#A8BBB8" BorderColor="#D7FAF3" BorderStyle="Ridge" BorderWidth="1px">
                        <asp:Label ID="EtiTitreJour" runat="server" Height="25px" Width="120px" BackColor="#A8BBB8"
                             ForeColor="#B7FAF3" BorderStyle="None" Text="Jours"
                             Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Small" Font-Italic="false"
                             style="margin-top: 1px; margin-left: 0px; text-indent:1px; text-align: center; vertical-align: top" />
                    </asp:TableCell>
                    <asp:TableCell BackColor="#A8BBB8" BorderColor="#D7FAF3" BorderStyle="Ridge" BorderWidth="1px">
                        <asp:Label ID="EtiTitreHoraire" runat="server" Height="25px" Width="155px" BackColor="#A8BBB8"
                             ForeColor="#B7FAF3" BorderStyle="None" Text="Horaires"
                             Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Small" Font-Italic="false"
                             style="margin-top: 1px; margin-left: 0px; text-indent:1px; text-align: center; vertical-align: top" />
                    </asp:TableCell>
                    <asp:TableCell BackColor="#A8BBB8" BorderColor="#D7FAF3" BorderStyle="Ridge" BorderWidth="1px">
                        <asp:Label ID="EtiTitreTotal" runat="server" Height="25px" Width="155px" BackColor="#A8BBB8"
                             ForeColor="#B7FAF3" BorderStyle="None" Text="Totaux"
                             Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Small" Font-Italic="false"
                             style="margin-top: 1px; margin-left: 0px; text-indent:1px; text-align: center; vertical-align: top" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>                
                    <asp:TableCell ColumnSpan="3">
                         <Virtualia:VSaisieHeuresJour ID="SaisieLundi" runat="server" VIndexJour="1" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>                
                    <asp:TableCell ColumnSpan="3">
                         <Virtualia:VSaisieHeuresJour ID="SaisieMardi" runat="server" VIndexJour="2" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>                
                    <asp:TableCell ColumnSpan="3">
                         <Virtualia:VSaisieHeuresJour ID="SaisieMercredi" runat="server" VIndexJour="3" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>                
                    <asp:TableCell ColumnSpan="3">
                         <Virtualia:VSaisieHeuresJour ID="SaisieJeudi" runat="server" VIndexJour="4" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>                
                    <asp:TableCell ColumnSpan="3">
                         <Virtualia:VSaisieHeuresJour ID="SaisieVendredi" runat="server" VIndexJour="5" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>                
                    <asp:TableCell ColumnSpan="3">
                         <Virtualia:VSaisieHeuresJour ID="SaisieSamedi" runat="server" VIndexJour="6" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>                
                    <asp:TableCell ColumnSpan="3">
                         <Virtualia:VSaisieHeuresJour ID="SaisieDimanche" runat="server" VIndexJour="7" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell BackColor="#A8BBB8" ColumnSpan="2" BorderColor="#D7FAF3" BorderStyle="Ridge" BorderWidth="1px">
                        <asp:Label ID="EtiTotalSemaine" runat="server" Height="25px" Width="275px" BackColor="#A8BBB8"
                             ForeColor="#B7FAF3" BorderStyle="None" Text="Total de la semaine"
                             Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Small" Font-Italic="false"
                             style="margin-top: 1px; margin-left: 0px; text-indent:1px; text-align: center; vertical-align: top" />
                    </asp:TableCell>
                    <asp:TableCell BackColor="#A8BBB8" BorderColor="#D7FAF3" BorderStyle="Ridge" BorderWidth="1px">
                        <asp:Label ID="TotalSemaine" runat="server" Height="25px" Width="155px" BackColor="#D7FAF3"
                             ForeColor="#1C5151" BorderStyle="None" Text="0 h 00"
                             Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Small" Font-Italic="false"
                             style="margin-top: 1px; margin-left: 0px; text-indent:1px; text-align: center; vertical-align: top" />
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
        
        <asp:TableCell>
            <asp:Table ID="CadreAide" runat="server" CellPadding="0" CellSpacing="0" Width="282px"
                        BorderStyle="Solid" BorderColor="#B0E0D7" BorderWidth="3px" Visible="false">
                <asp:TableRow>
                    <asp:TableCell BackColor="#D7FAF3" ColumnSpan="4" BorderColor="#7EC8BE" BorderStyle="Ridge" BorderWidth="1px">
                        <asp:Label ID="EtiJour" runat="server" Height="20px" Width="286px" BackColor="#D7FAF3"
                             ForeColor="#1C5151" BorderStyle="None" Text=""
                             Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Small" Font-Italic="false"
                             style="margin-top: 0px; margin-left: 0px; text-indent:1px; text-align: center; 
                             vertical-align: top" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left" ColumnSpan="2" BorderColor="#7EC8BE" BorderStyle="Ridge" 
                            BorderWidth="1px">
                        <asp:Label ID="EtiMatin" runat="server" Height="20px" Width="143px" BackColor="#A8BBB8"
                             ForeColor="#B7FAF3" BorderStyle="None" Text="matin"
                             Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Small" Font-Italic="false"
                             style="margin-left: 0px; text-indent:1px; text-align: center; vertical-align: top" />
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left" ColumnSpan="2" BorderColor="#7EC8BE" BorderStyle="Ridge" 
                            BorderWidth="1px">
                        <asp:Label ID="EtiSoir" runat="server" Height="20px" Width="142px" BackColor="#A8BBB8"
                             ForeColor="#B7FAF3" BorderStyle="None" Text="aprés-midi"
                             Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Small" Font-Italic="false"
                             style="margin-left: 0px; text-indent:1px; text-align: center; vertical-align: top" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left" BorderColor="#7EC8BE" BorderStyle="Ridge" BorderWidth="1px">
                        <asp:Label ID="EtiMatinA" runat="server" Height="20px" Width="72px" BackColor="#A8BBB8"
                             ForeColor="#B7FAF3" BorderStyle="None" Text="arrivée"
                             Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Small" Font-Italic="false"
                             style="margin-left: 0px; text-indent:1px; text-align: center; vertical-align: top" />
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left" BorderColor="#7EC8BE" BorderStyle="Ridge" BorderWidth="1px">
                        <asp:Label ID="EtiMatinD" runat="server" Height="20px" Width="70px" BackColor="#A8BBB8"
                             ForeColor="#B7FAF3" BorderStyle="None" Text="départ"
                             Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Small" Font-Italic="false"
                             style="margin-left: 0px; text-indent:1px; text-align: center; vertical-align: top" />
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left" BorderColor="#7EC8BE" BorderStyle="Ridge" BorderWidth="1px">
                        <asp:Label ID="EtiSoirA" runat="server" Height="20px" Width="71px" BackColor="#A8BBB8"
                             ForeColor="#B7FAF3" BorderStyle="None" Text="arrivée"
                             Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Small" Font-Italic="false"
                             style="margin-left: 0px; text-indent:1px; text-align: center; vertical-align: top" />
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left" BorderColor="#7EC8BE" BorderStyle="Ridge" BorderWidth="1px">
                        <asp:Label ID="EtiSoirD" runat="server" Height="20px" Width="70px" BackColor="#A8BBB8"
                             ForeColor="#B7FAF3" BorderStyle="None" Text="départ"
                             Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Small" Font-Italic="false"
                             style="margin-left: 0px; text-indent:1px; text-align: center; vertical-align: top" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left" >
                        <asp:ListBox ID="LstMatinA" runat="server" Height="320px" Width="70px" BackColor="Snow"
                            AutoPostBack="true" ForeColor="#142425" 
                            Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Small" Font-Italic="false"
                            style="margin-left: 0px; border-color: #7EC8BE; border-width: 1px; border-style: solid;
                                 display: table-cell;">
                        </asp:ListBox>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left" >
                            <asp:ListBox ID="LstMatinD" runat="server" Height="320px" Width="70px" BackColor="Snow"
                                AutoPostBack="true" ForeColor="#142425"
                                Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Small" Font-Italic="false"
                                style="margin-left: 0px; border-color: #7EC8BE; border-width: 1px; border-style: solid;
                                     display: table-cell;">
                            </asp:ListBox>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left" >
                        <asp:ListBox ID="LstSoirA" runat="server" Height="320px" Width="70px" BackColor="Snow"
                            AutoPostBack="true" ForeColor="#142425"
                            Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Small" Font-Italic="false"
                            style="margin-left: 0px; border-color: #7EC8BE; border-width: 1px; border-style: solid;
                                 display: table-cell;">
                         </asp:ListBox>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left" >
                            <asp:ListBox ID="LstSoirD" runat="server" Height="320px" Width="70px" BackColor="Snow"
                                AutoPostBack="true" ForeColor="#142425"
                                Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Small" Font-Italic="false"
                                style="margin-left: 0px; border-color: #7EC8BE; border-width: 1px; border-style: solid;
                                     display: table-cell;">
                             </asp:ListBox>
                    </asp:TableCell>
                 </asp:TableRow>
                 <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="SelMatinA" runat="server" Height="20px" Width="40px" BackColor="Snow"
                             ForeColor="#142425" BorderWidth="1px" BorderStyle="Solid" BorderColor="#142425"
                             Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Small" Font-Italic="false"
                             style="margin-left: 5px; margin-top: 10px; text-indent:1px; text-align: center; vertical-align: top" />
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Right">
                        <asp:Label ID="SelMatinD" runat="server" Height="20px" Width="40px" BackColor="Snow"
                             ForeColor="#142425" BorderWidth="1px" BorderStyle="Solid" BorderColor="#142425"
                             Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Small" Font-Italic="false"
                             style="margin-Right: 5px; margin-top: 10px; text-indent:1px; text-align: center; vertical-align: top" />
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="SelSoirA" runat="server" Height="20px" Width="40px" BackColor="Snow"
                             ForeColor="#142425" BorderWidth="1px" BorderStyle="Solid" BorderColor="#142425"
                             Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Small" Font-Italic="false"
                             style="margin-left: 5px; margin-top: 10px; text-indent:1px; text-align: center; vertical-align: top" />
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Right">
                        <asp:Label ID="SelSoirD" runat="server" Height="20px" Width="40px" BackColor="Snow"
                             ForeColor="#142425" BorderWidth="1px" BorderStyle="Solid" BorderColor="#142425"
                             Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Small" Font-Italic="false"
                             style="margin-Right: 5px; margin-top: 10px; text-indent:1px; text-align: center; vertical-align: top" />
                    </asp:TableCell>
                 </asp:TableRow>
                 <asp:TableRow>
                    <asp:TableCell ColumnSpan="2" HorizontalAlign="Center">
                        <asp:Label ID="SelTotMatin" runat="server" Height="20px" Width="48px" BackColor="Snow"
                             ForeColor="#142425" BorderWidth="1px" BorderStyle="Solid" BorderColor="#142425"
                             Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Small" Font-Italic="false"
                             style="margin-top: 0px; text-indent:1px; text-align: center; vertical-align: top" />
                    </asp:TableCell>
                    <asp:TableCell ColumnSpan="2" HorizontalAlign="Center">
                        <asp:Label ID="SelTotSoir" runat="server" Height="20px" Width="48px" BackColor="Snow"
                             ForeColor="#142425" BorderWidth="1px" BorderStyle="Solid" BorderColor="#142425"
                             Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Small" Font-Italic="false"
                             style="margin-top: 0px; text-indent:1px; text-align: center; vertical-align: top" />
                    </asp:TableCell>
                 </asp:TableRow>
                 <asp:TableRow>
                    <asp:TableCell ColumnSpan="2">
                        <asp:Table ID="CadreOK" runat="server" Height="20px" CellPadding="0" 
                             CellSpacing="0" BackImageUrl="~/Images/Boutons/OK_Std.bmp" Visible="true"
                             BorderWidth="2px" BorderStyle="Outset" BorderColor="#B0E0D7" ForeColor="#D7FAF3"
                             Width="70px" HorizontalAlign="Left" style="margin-top: 15px; margin-left: 50px" >
                            <asp:TableRow VerticalAlign="Top">
                                <asp:TableCell>
                                     <asp:Button ID="CommandeOK" runat="server" Text="Valider" Width="65px" Height="20px"
                                         BackColor="Transparent" BorderColor="#B0E0D7" ForeColor="#D7FAF3"
                                         Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Font-Italic="true"
                                         BorderStyle="None" style=" margin-left: 6px; text-align: center;">
                                     </asp:Button>
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                    <asp:TableCell ColumnSpan="2">
                        <asp:Table ID="CadreAnnuler" runat="server" Height="20px" CellPadding="0" 
                             CellSpacing="0" BackImageUrl="~/Images/Boutons/Retour_Std.bmp" Visible="true"
                             BorderWidth="2px" BorderStyle="Outset" BorderColor="#B0E0D7" ForeColor="#D7FAF3"
                             Width="70px" HorizontalAlign="Left" style="margin-top: 15px; margin-left: 30px" >
                            <asp:TableRow VerticalAlign="Top">
                                <asp:TableCell>
                                     <asp:Button ID="CommandeAnnuler" runat="server" Text="Annuler" Width="65px" Height="18px"
                                         BackColor="Transparent" BorderColor="#B0E0D7" ForeColor="#D7FAF3"
                                         Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Font-Italic="true"
                                         BorderStyle="None" style=" margin-left: 6px; text-align: center;">
                                     </asp:Button>
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                 </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>
</asp:Table>
<asp:HiddenField ID="HSelJour" runat="server" Value="" />
<asp:HiddenField ID="HSelLst" runat="server" Value="07:45;11:45;13:30;18:00" />