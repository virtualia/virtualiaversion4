﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="VDetailMensuel.ascx.vb" Inherits="Virtualia.Net.Controles_VDetailMensuel" %>

<%@ Register src="../Saisies/VListeCombo.ascx" tagname="VListeCombo" tagprefix="Virtualia" %>
<%@ Register src="../TempsTravail/VLigneDetailMensuel.ascx" tagname="VLigneDetail" tagprefix="Virtualia" %>

<asp:Table ID="CadreInfo" runat="server" BorderStyle="None" BorderWidth="2px" Visible="true"
    BorderColor="#B0E0D7" Height="250px" Width="750px" HorizontalAlign="Center" style="margin-top: 3px;">
      <asp:TableRow>
        <asp:TableCell>
          <asp:Table ID="CadreTitre" runat="server" Height="30px" CellPadding="0" Width="750px" 
             CellSpacing="0" HorizontalAlign="Center" BorderColor="Black" Visible="true" BorderStyle="NotSet" >
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="Etiquette" runat="server" Text="Planning mensuel détaillé" Height="20px" Width="320px"
                            BackColor="#216B68" BorderColor="#B0E0D7"  BorderStyle="Groove"
                            BorderWidth="2px" ForeColor="#D7FAF3"
                            Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Small"
                            style="margin-top: 5px; margin-left: 4px; margin-bottom: 20px;
                            font-style: oblique; text-indent: 5px; text-align: center;">
                        </asp:Label>          
                    </asp:TableCell>      
                </asp:TableRow>
          </asp:Table>
        </asp:TableCell>
      </asp:TableRow>
      <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="TableauPlanningDetaille" runat="server" CellPadding="0" CellSpacing="0" Width="750px"
            BorderStyle="Notset" BorderWidth="1px" BorderColor="#1C5151" HorizontalAlign="Left" Visible="true">
                <asp:TableRow>
                    <asp:TableCell ColumnSpan="9" Height="40px">
                        <Virtualia:VListeCombo ID="LstLibelMois" runat="server" EtiVisible="true" EtiText="Mois" EtiWidth="60px"
                             V_NomTable="Mois" LstWidth="180px" LstBorderColor="#B0E0D7" LstBackColor="#EEECFD"
                              EtiStyle="text-align:center" EtiBackColor="#8DA8A3" EtiForeColor="#E9FDF9" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow ID="LigneEntete" Width="746px" Height="23px" VerticalAlign="Bottom">
                 <asp:TableCell ID="CellTitreEntete" BackColor="#A8BBB8" BorderColor="LightGray" BorderStyle="None" HorizontalAlign="Center" BorderWidth="1px"> 
                  <asp:Label ID="LabelTitreEntete" runat="server" Height="23px" Width="234px"
                      BackColor="#A8BBB8" BorderColor="LightGray" BorderStyle="Solid" Text="ANDRIEU Béatrice"
                      BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                      Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Smaller"
                      style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                      font-style: normal; text-indent: 5px; text-align:  center; padding-top: 2px;">
                  </asp:Label>          
                 </asp:TableCell>
                 <asp:TableCell ID="CellMatin" BackColor="#A8BBB8" BorderColor="LightGray" BorderStyle="None" BorderWidth="1px"> 
                  <asp:Label ID="LabelMatin" runat="server" Height="23px" Width="71px" 
                      BackColor="#A8BBB8" BorderColor="LightGray" BorderStyle="Solid" Text="Matin"
                      BorderWidth="1px" ForeColor="#D7FAF3" Font-Italic="False"
                      Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Smaller"
                      style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                      font-style: normal; text-indent: 0px;  text-align:  center; padding-top: 2px;">
                  </asp:Label>          
                 </asp:TableCell>
                 <asp:TableCell ID="CellMatinHDebut" BackColor="#A8BBB8" BorderColor="LightGray" BorderStyle="None" BorderWidth="1px"> 
                  <asp:Label ID="LabelMatinHDebut" runat="server" Height="23px" Width="55px"
                      BackColor="#A8BBB8" BorderColor="LightGray" BorderStyle="Solid" Text="H.début"
                      BorderWidth="1px" ForeColor="#D7FAF3" Font-Italic="False"
                      Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Smaller"
                      style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                      font-style: normal; text-indent: 0px; text-align:  center; padding-top: 2px;">
                  </asp:Label>          
                 </asp:TableCell>
                 <asp:TableCell ID="CellMatinHFin" BackColor="#A8BBB8" BorderColor="LightGray" BorderStyle="None" BorderWidth="1px"> 
                  <asp:Label ID="LabelMatinHFin" runat="server" Height="23px" Width="55px"
                      BackColor="#A8BBB8" BorderColor="LightGray" BorderStyle="Solid" Text="H.fin"
                      BorderWidth="1px" ForeColor="#D7FAF3" Font-Italic="False"
                      Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Smaller"
                      style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                      font-style: normal; text-indent: 0px; text-align:  center; padding-top: 2px;">
                  </asp:Label>          
                 </asp:TableCell>
                 <asp:TableCell ID="CellApresmidi" BackColor="#A8BBB8" BorderColor="LightGray" BorderStyle="None" BorderWidth="1px"> 
                  <asp:Label ID="LabelApresmidi" runat="server" Height="23px" Width="71px"
                      BackColor="#A8BBB8" BorderColor="LightGray" BorderStyle="Solid" Text="Après-midi"
                      BorderWidth="1px" ForeColor="#D7FAF3" Font-Italic="False"
                      Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Smaller"
                      style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                      font-style: normal; text-indent: 0px; text-align:  center; padding-top: 2px;">
                  </asp:Label>          
                 </asp:TableCell>
                 <asp:TableCell ID="CellApresmidiHDebut" BackColor="#A8BBB8" BorderColor="LightGray" BorderStyle="None" BorderWidth="1px"> 
                  <asp:Label ID="LabelApresmidiHDebut" runat="server" Height="23px" Width="56px"
                      BackColor="#A8BBB8" BorderColor="LightGray" BorderStyle="Solid" Text="H.début"
                      BorderWidth="1px" ForeColor="#D7FAF3" Font-Italic="False"
                      Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Smaller"
                      style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                      font-style: normal; text-indent: 0px; text-align:  center; padding-top: 2px;">
                  </asp:Label>          
                 </asp:TableCell>
                 <asp:TableCell ID="CellApresmidiHFin" BackColor="#A8BBB8" BorderColor="LightGray" BorderStyle="None" BorderWidth="1px"> 
                  <asp:Label ID="LabelApresmidiHFin" runat="server" Height="23px" Width="55px"
                      BackColor="#A8BBB8" BorderColor="LightGray" BorderStyle="Solid" Text="H.fin"
                      BorderWidth="1px" ForeColor="#D7FAF3" Font-Italic="False"
                      Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Smaller"
                      style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                      font-style: normal; text-indent: 0px; text-align:  center; padding-top: 2px;">
                  </asp:Label>          
                 </asp:TableCell>
                 <asp:TableCell ID="CellTotalJour" BackColor="#A8BBB8" BorderColor="LightGray" BorderStyle="None" BorderWidth="1px"> 
                  <asp:Label ID="LabelTotalJour" runat="server" Height="23px" Width="60px"
                      BackColor="#A8BBB8" BorderColor="LightGray" BorderStyle="Solid" Text="Tot.jour"
                      BorderWidth="1px" ForeColor="#D7FAF3" Font-Italic="False"
                      Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Smaller"
                      style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                      font-style: normal; text-indent: 0px; text-align:  center; padding-top: 2px;">
                  </asp:Label>          
                 </asp:TableCell>
                 <asp:TableCell ID="CellTotSem" BackColor="#A8BBB8" BorderColor="LightGray" BorderStyle="None" BorderWidth="1px" > 
                  <asp:Label ID="LabelTotSem" runat="server" Height="23px" Width="61px"
                      BackColor="#A8BBB8" BorderColor="LightGray" BorderStyle="Solid" Text="Cumul"
                      BorderWidth="1px" ForeColor="#D7FAF3" Font-Italic="False"
                      Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Smaller"
                      style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                      font-style: normal; text-indent: 0px; text-align:  center; padding-top: 2px;">
                  </asp:Label>          
                 </asp:TableCell>      
              </asp:TableRow>
              <asp:TableRow>                
                    <asp:TableCell ColumnSpan="9">
                         <Virtualia:VLigneDetail ID="Jour00" runat="server" />
                    </asp:TableCell>
               </asp:TableRow>
              <asp:TableRow>                
                    <asp:TableCell ColumnSpan="9">
                         <Virtualia:VLigneDetail ID="Jour01" runat="server" />
                    </asp:TableCell>
               </asp:TableRow>
               <asp:TableRow>                
                    <asp:TableCell ColumnSpan="9">
                         <Virtualia:VLigneDetail ID="Jour02" runat="server" />
                    </asp:TableCell>
               </asp:TableRow>
               <asp:TableRow>                
                    <asp:TableCell ColumnSpan="9">
                         <Virtualia:VLigneDetail ID="Jour03" runat="server" />
                    </asp:TableCell>
               </asp:TableRow>
               <asp:TableRow>                
                    <asp:TableCell ColumnSpan="9">
                         <Virtualia:VLigneDetail ID="Jour04" runat="server" />
                    </asp:TableCell>
               </asp:TableRow>
               <asp:TableRow>                
                    <asp:TableCell ColumnSpan="9">
                         <Virtualia:VLigneDetail ID="Jour05" runat="server" />
                    </asp:TableCell>
               </asp:TableRow>
               <asp:TableRow>                
                    <asp:TableCell ColumnSpan="9">
                         <Virtualia:VLigneDetail ID="Jour06" runat="server" />
                    </asp:TableCell>
               </asp:TableRow>
               <asp:TableRow>                
                    <asp:TableCell ColumnSpan="9">
                         <Virtualia:VLigneDetail ID="Jour07" runat="server" />
                    </asp:TableCell>
               </asp:TableRow>
               <asp:TableRow>                
                    <asp:TableCell ColumnSpan="9">
                         <Virtualia:VLigneDetail ID="Jour08" runat="server" />
                    </asp:TableCell>
               </asp:TableRow>
               <asp:TableRow>                
                    <asp:TableCell ColumnSpan="9">
                         <Virtualia:VLigneDetail ID="Jour09" runat="server" />
                    </asp:TableCell>
               </asp:TableRow>
               <asp:TableRow>                
                    <asp:TableCell ColumnSpan="9">
                         <Virtualia:VLigneDetail ID="Jour10" runat="server" />
                    </asp:TableCell>
               </asp:TableRow>
               <asp:TableRow>                
                    <asp:TableCell ColumnSpan="9">
                         <Virtualia:VLigneDetail ID="Jour11" runat="server" />
                    </asp:TableCell>
               </asp:TableRow>
               <asp:TableRow>                
                    <asp:TableCell ColumnSpan="9">
                         <Virtualia:VLigneDetail ID="Jour12" runat="server" />
                    </asp:TableCell>
               </asp:TableRow>
               <asp:TableRow>                
                    <asp:TableCell ColumnSpan="9">
                         <Virtualia:VLigneDetail ID="Jour13" runat="server" />
                    </asp:TableCell>
               </asp:TableRow>
               <asp:TableRow>                
                    <asp:TableCell ColumnSpan="9">
                         <Virtualia:VLigneDetail ID="Jour14" runat="server" />
                    </asp:TableCell>
               </asp:TableRow>
               <asp:TableRow>                
                    <asp:TableCell ColumnSpan="9">
                         <Virtualia:VLigneDetail ID="Jour15" runat="server" />
                    </asp:TableCell>
               </asp:TableRow>
               <asp:TableRow>                
                    <asp:TableCell ColumnSpan="9">
                         <Virtualia:VLigneDetail ID="Jour16" runat="server" />
                    </asp:TableCell>
               </asp:TableRow>
               <asp:TableRow>                
                    <asp:TableCell ColumnSpan="9">
                         <Virtualia:VLigneDetail ID="Jour17" runat="server" />
                    </asp:TableCell>
               </asp:TableRow>
               <asp:TableRow>                
                    <asp:TableCell ColumnSpan="9">
                         <Virtualia:VLigneDetail ID="Jour18" runat="server" />
                    </asp:TableCell>
               </asp:TableRow>
               <asp:TableRow>                
                    <asp:TableCell ColumnSpan="9">
                         <Virtualia:VLigneDetail ID="Jour19" runat="server" />
                    </asp:TableCell>
               </asp:TableRow>
               <asp:TableRow>                
                    <asp:TableCell ColumnSpan="9">
                         <Virtualia:VLigneDetail ID="Jour20" runat="server" />
                    </asp:TableCell>
               </asp:TableRow>
               <asp:TableRow>                
                    <asp:TableCell ColumnSpan="9">
                         <Virtualia:VLigneDetail ID="Jour21" runat="server" />
                    </asp:TableCell>
               </asp:TableRow>
               <asp:TableRow>                
                    <asp:TableCell ColumnSpan="9">
                         <Virtualia:VLigneDetail ID="Jour22" runat="server" />
                    </asp:TableCell>
               </asp:TableRow>
               <asp:TableRow>                
                    <asp:TableCell ColumnSpan="9">
                         <Virtualia:VLigneDetail ID="Jour23" runat="server" />
                    </asp:TableCell>
               </asp:TableRow>
               <asp:TableRow>                
                    <asp:TableCell ColumnSpan="9">
                         <Virtualia:VLigneDetail ID="Jour24" runat="server" />
                    </asp:TableCell>
               </asp:TableRow>
               <asp:TableRow>                
                    <asp:TableCell ColumnSpan="9">
                         <Virtualia:VLigneDetail ID="Jour25" runat="server" />
                    </asp:TableCell>
               </asp:TableRow>
               <asp:TableRow>                
                    <asp:TableCell ColumnSpan="9">
                         <Virtualia:VLigneDetail ID="Jour26" runat="server" />
                    </asp:TableCell>
               </asp:TableRow>
               <asp:TableRow>                
                    <asp:TableCell ColumnSpan="9">
                         <Virtualia:VLigneDetail ID="Jour27" runat="server" />
                    </asp:TableCell>
               </asp:TableRow>
               <asp:TableRow>                
                    <asp:TableCell ColumnSpan="9">
                         <Virtualia:VLigneDetail ID="Jour28" runat="server" />
                    </asp:TableCell>
               </asp:TableRow>
               <asp:TableRow>                
                    <asp:TableCell ColumnSpan="9">
                         <Virtualia:VLigneDetail ID="Jour29" runat="server" />
                    </asp:TableCell>
               </asp:TableRow>
               <asp:TableRow>                
                    <asp:TableCell ColumnSpan="9">
                         <Virtualia:VLigneDetail ID="Jour30" runat="server" />
                    </asp:TableCell>
               </asp:TableRow>
            </asp:Table>
         </asp:TableCell>     
      </asp:TableRow> 
 </asp:Table>