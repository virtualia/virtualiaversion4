﻿Option Strict On
Option Explicit On
Option Compare Text
Imports VI = Virtualia.Systeme.Constantes
Imports Microsoft.VisualBasic
Public Class Controles_VDetailMensuel
    Inherits System.Web.UI.UserControl
    Private WebFct As Virtualia.Net.WebFonctions
    Private WsIde_Dossier As Integer = 0
    'Execution
    Private WsNomStateIde As String = "VDetailMoisIde"
    Private WsNomStateParam As String = "VplanningParam"
    Private WsCacheIde As ArrayList
    Private WsCacheParam As ArrayList
    Private WsDossierRtt As Virtualia.Metier.TempsTravail.DossierRTT
    Private WsPlanningRtt As Virtualia.Metier.TempsTravail.PlanningIndividuel
    Private WsListeVues As List(Of Virtualia.TablesObjet.ShemaVUE.VUE_JourTravail)
    Private WsNbHeuresTotales As Integer

    Public Property Identifiant() As Integer
        Get
            If Me.ViewState(WsNomStateIde) IsNot Nothing Then
                WsCacheIde = CType(Me.ViewState(WsNomStateIde), ArrayList)
                WsIde_Dossier = CInt(WsCacheIde(0))
            End If
            Return WsIde_Dossier
        End Get
        Set(ByVal value As Integer)
            If Me.ViewState(WsNomStateIde) IsNot Nothing Then
                WsCacheIde = CType(Me.ViewState(WsNomStateIde), ArrayList)
                WsIde_Dossier = CInt(WsCacheIde(0))
            End If
            If value = WsIde_Dossier Then
                Exit Property
            End If
            WsIde_Dossier = value
            If WsIde_Dossier = 0 Then
                Exit Property
            End If
            If Me.ViewState(WsNomStateIde) IsNot Nothing Then
                Me.ViewState.Remove(WsNomStateIde)
            End If
            '** MettreEnCache le dossier **
            WsCacheIde = New ArrayList
            WsCacheIde.Add(WsIde_Dossier)
            Me.ViewState.Add(WsNomStateIde, WsCacheIde)
            '**
            Dim PointeurUti As Virtualia.Net.Session.ObjetSession = WebFct.PointeurUtilisateur
            If PointeurUti Is Nothing Then
                Exit Property
            End If
            WsDossierRtt = PointeurUti.PointeurDllRTT.ItemDossier(WsIde_Dossier)
            If WsDossierRtt Is Nothing Then
                PointeurUti.PointeurDllRTT.Identifiant(WsIde_Dossier) = _
                        WebFct.PointeurDossier(WsIde_Dossier).V_ListeDesFiches( _
                            PointeurUti.PointeurDllRTT.ObjetsDllARTT)

                WsDossierRtt = PointeurUti.PointeurDllRTT.ItemDossier(WsIde_Dossier)
            End If
        End Set
    End Property

    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        WebFct = New Virtualia.Net.WebFonctions(Me, 0)
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        If Me.ViewState(WsNomStateParam) Is Nothing Then
            Call FaireListeMois()
        End If
        Call FairePlanningMensuel()
    End Sub

    Protected Sub LstLibelMois_ValeurChange(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.DonneeChangeEventArgs) Handles LstLibelMois.ValeurChange
        Dim Mois As Integer
        Dim Annee As Integer
        Dim TableauW(0) As String

        TableauW = Strings.Split(e.Valeur, Strings.Space(1), -1)
        Annee = CInt(TableauW(1))
        Mois = WebFct.ViRhDates.IndexduMois(TableauW(0))
        If Me.ViewState(WsNomStateParam) IsNot Nothing Then
            Me.ViewState.Remove(WsNomStateParam)
        End If
        '** MettreEnCache les paramètres **
        WsCacheParam = New ArrayList
        WsCacheParam.Add(Annee)
        WsCacheParam.Add(Mois)
        WsCacheParam.Add(WebFct.ViRhDates.DateSaisieVerifiee("31/" & Strings.Format(Mois, "00") & "/" & Strings.Format(Annee, "0000")))
        Me.ViewState.Add(WsNomStateParam, WsCacheParam)
    End Sub

    Private Sub FairePlanningMensuel()
        Dim DateFin As String
        Dim Mois As Integer
        Dim Annee As Integer
        Dim DateW As String
        Dim NbHeures As Integer

        WsNbHeuresTotales = 0

        If Me.ViewState(WsNomStateIde) Is Nothing Then
            Exit Sub
        End If
        WsCacheIde = CType(Me.ViewState(WsNomStateIde), ArrayList)

        If Me.ViewState(WsNomStateParam) Is Nothing Then
            DateFin = WebFct.ViRhDates.DateduJour(False)
            Annee = CInt(Strings.Right(DateFin, 4))
            Mois = CInt(Strings.Mid(DateFin, 4, 2))

            WsCacheParam = New ArrayList
            WsCacheParam.Add(Annee)
            WsCacheParam.Add(Mois)
            WsCacheParam.Add(DateFin)
            Me.ViewState.Add(WsNomStateParam, WsCacheParam)
            LstLibelMois.LstText = WebFct.ViRhDates.MoisEnClair(CShort(Mois)) & Strings.Space(1) & Strings.Format(Annee, "0000")
        Else
            WsCacheParam = CType(Me.ViewState(WsNomStateParam), ArrayList)
            Annee = CInt(WsCacheParam(0))
            Mois = CInt(WsCacheParam(1))
            DateFin = WsCacheParam(2).ToString
        End If

        Dim IndiceJ As Integer
        Dim Ctl As Control
        Dim VirControle As Controles_VLigneDetailMensuel
        Dim DateDebut As String

        DateDebut = "01/" & Strings.Format(Mois, "00") & "/" & Annee.ToString
        WsPlanningRtt = Nothing
        If Identifiant > 0 Then
            WsDossierRtt = WebFct.PointeurUtilisateur.PointeurDllRTT.ItemDossier(WsIde_Dossier)
            WsPlanningRtt = WsDossierRtt.PlanningIndividuel(DateDebut, DateFin)
            WsListeVues = WsPlanningRtt.ListedesVues(DateDebut, DateFin)
            LabelTitreEntete.Text = WsDossierRtt.Nom & Strings.Space(1) & WsDossierRtt.Prenom
        End If

        IndiceJ = 0
        DateW = DateDebut

        Do 'Traitement des jours du mois
            Ctl = WebFct.VirWebControle(Me.TableauPlanningDetaille, "Jour", IndiceJ)
            If Ctl IsNot Nothing Then
                VirControle = CType(Ctl, Controles_VLigneDetailMensuel)
                VirControle.Initialiser()
                VirControle.VText_Jour = WebFct.ViRhDates.ClairDate(DateW, True)
                VirControle.Visible = True
                If WebFct.ViRhDates.SiJourOuvre(DateW, True) = True Then
                    VirControle.VText_Matin = "TR"
                    VirControle.VBackColor_Matin = Drawing.Color.White
                    VirControle.VText_Soir = "TR"
                    VirControle.VBackColor_Soir = Drawing.Color.White
                Else
                    If WebFct.ViRhDates.SiJourFerie(DateW) = True Then
                        VirControle.VText_Matin = "JF"
                        VirControle.VText_Soir = "JF"
                    Else
                        VirControle.VText_Matin = "RH"
                        VirControle.VText_Soir = "RH"
                    End If
                    VirControle.VBackColor_Matin = WebFct.PointeurUtilisateur.PointeurParametres.CouleurPlanning(1)
                    VirControle.VBackColor_Soir = WebFct.PointeurUtilisateur.PointeurParametres.CouleurPlanning(1)
                End If
                If WsIde_Dossier > 0 Then
                    Call FaireJour(DateW, VirControle)
                    If WsNbHeuresTotales >= 0 Then
                        NbHeures = WsNbHeuresTotales \ 60
                        VirControle.VTotal_Cumule = NbHeures.ToString & " h " & Strings.Format(WsNbHeuresTotales Mod 60, "00")
                    Else
                        NbHeures = -WsNbHeuresTotales \ 60
                        VirControle.VTotal_Cumule = "-" & NbHeures.ToString & " h " & Strings.Format(-WsNbHeuresTotales Mod 60, "00")
                    End If
                End If
            End If

            DateW = WebFct.ViRhDates.CalcDateMoinsJour(DateW, "1", "0")
            Select Case WebFct.ViRhDates.ComparerDates(DateW, DateFin)
                Case VI.ComparaisonDates.PlusGrand
                    Exit Do
            End Select
            IndiceJ += 1
            If IndiceJ > 30 Then
                Exit Do
            End If
        Loop
        For Mois = IndiceJ To 30
            Ctl = WebFct.VirWebControle(Me.TableauPlanningDetaille, "Jour", Mois)
            If Ctl IsNot Nothing Then
                VirControle = CType(Ctl, Controles_VLigneDetailMensuel)
                VirControle.Visible = False
            End If
        Next Mois
    End Sub

    Private Sub FaireJour(ByVal DateValeur As String, ByVal CtlJour As Controles_VLigneDetailMensuel)
        Dim FicheVue As Virtualia.TablesObjet.ShemaVUE.VUE_JourTravail
        Dim FicheRglAbs As Virtualia.TablesObjet.ShemaREF.ABS_ABSENCE
        Dim Horaires As New ArrayList
        Dim Couleur As Drawing.Color
        Dim IndiceP As Integer
        Dim IndiceC As Integer

        FicheVue = WsListeVues.Find(Function(Recherche) Recherche.Date_de_Valeur = DateValeur)

        CtlJour.VToolTip_Matin = ""
        CtlJour.VToolTip_Soir = ""

        If FicheVue Is Nothing Then
            If WsDossierRtt.SiEnActivite(DateValeur) = True Then
                If WebFct.ViRhDates.SiJourOuvre(DateValeur, True) = False Then
                    If WebFct.ViRhDates.SiJourFerie(DateValeur) = True Then
                        CtlJour.VText_Matin = "JF"
                        CtlJour.VText_Soir = "JF"
                    Else
                        CtlJour.VText_Matin = "RH"
                        CtlJour.VText_Soir = "RH"
                    End If
                Else
                    CtlJour.VText_Matin = "NT"
                    CtlJour.VText_Soir = "NT"
                End If
                CtlJour.VBackColor_Matin = WebFct.PointeurUtilisateur.PointeurParametres.CouleurPlanning(1)
                CtlJour.VBackColor_Soir = WebFct.PointeurUtilisateur.PointeurParametres.CouleurPlanning(1)
            Else
                CtlJour.VText_Matin = ""
                CtlJour.VBackColor_Matin = WebFct.ConvertCouleur("#73829A")
                CtlJour.VText_Soir = ""
                CtlJour.VBackColor_Soir = WebFct.ConvertCouleur("#73829A")
            End If
            CtlJour.VHeures = Horaires
            Exit Sub
        End If

        CtlJour.VTotal_Comptabilise = FicheVue.DureeComptabilisee
        WsNbHeuresTotales += FicheVue.DureeComptabiliseeEnMinutes

        For IndiceP = 0 To FicheVue.NombredeFiches - 1
            Couleur = Nothing
            If FicheVue.ItemPlage(IndiceP).EvenementPlage <> "" Then
                Select Case FicheVue.ItemPlage(IndiceP).SiPrevisionIntranet
                    Case Is = 1
                        Couleur = WebFct.PointeurUtilisateur.PointeurParametres.CouleurPlanning(4)
                    Case Else
                        IndiceC = WebFct.PointeurUtilisateur.PointeurParametres.IndexCouleurPlanning(FicheVue.ItemPlage(IndiceP).EvenementPlage)
                        If IndiceC = 0 Then
                            Couleur = Drawing.Color.White
                        Else
                            Couleur = WebFct.PointeurUtilisateur.PointeurParametres.CouleurPlanning(IndiceC)
                        End If
                End Select
            End If

            Select Case FicheVue.ItemPlage(IndiceP).NumeroPlage
                Case Is = VI.NumeroPlage.Plage1_Presence
                    CtlJour.VText_Matin = "TR"
                    Horaires.Add(FicheVue.ItemPlage(IndiceP).HeureDebut_Constate)
                    Horaires.Add(FicheVue.ItemPlage(IndiceP).HeureFin_Constate)
                    If FicheVue.ItemPlage(IndiceP).HoraireCycle_Prevu_Minutes = 0 Then
                        CtlJour.VBackColor_Matin = WebFct.PointeurUtilisateur.PointeurParametres.CouleurPlanning(1)
                        CtlJour.VText_Matin = "NT"
                    End If
                Case Is = VI.NumeroPlage.Plage2_Presence
                    CtlJour.VText_Soir = "TR"
                    Horaires.Add(FicheVue.ItemPlage(IndiceP).HeureDebut_Constate)
                    Horaires.Add(FicheVue.ItemPlage(IndiceP).HeureFin_Constate)
                    If FicheVue.ItemPlage(IndiceP).HoraireCycle_Prevu_Minutes = 0 Then
                        CtlJour.VBackColor_Soir = WebFct.PointeurUtilisateur.PointeurParametres.CouleurPlanning(1)
                        CtlJour.VText_Soir = "NT"
                    End If
                Case Is = VI.NumeroPlage.Jour_Absence
                    If FicheVue.SiJourFerie = False Or FicheVue.DureePrevueCycleEnMinutes > 0 Then
                        CtlJour.VBackColor_Matin = Couleur
                        CtlJour.VBackColor_Soir = Couleur
                    End If
                    If FicheVue.ItemPlage(IndiceP).EvenementPlage <> "" Then
                        FicheRglAbs = WebFct.PointeurUtilisateur.PointeurParametres.PointeurAbsences.Fiche_Absence(FicheVue.ItemPlage(IndiceP).EvenementPlage)
                        If FicheRglAbs IsNot Nothing Then
                            CtlJour.VText_Matin = FicheRglAbs.CodeRegroupement
                            CtlJour.VText_Soir = FicheRglAbs.CodeRegroupement
                        Else
                            CtlJour.VText_Matin = FicheVue.ItemPlage(IndiceP).EvenementPlage
                            CtlJour.VText_Soir = FicheVue.ItemPlage(IndiceP).EvenementPlage
                        End If
                        CtlJour.VToolTip_Matin = FicheVue.ItemPlage(IndiceP).EvenementPlage
                        CtlJour.VToolTip_Soir = FicheVue.ItemPlage(IndiceP).EvenementPlage
                    End If
                Case Is = VI.NumeroPlage.Plage1_Absence
                    CtlJour.VBackColor_Matin = Couleur
                    If FicheVue.ItemPlage(IndiceP).EvenementPlage <> "" Then
                        FicheRglAbs = WebFct.PointeurUtilisateur.PointeurParametres.PointeurAbsences.Fiche_Absence(FicheVue.ItemPlage(IndiceP).EvenementPlage)
                        If FicheRglAbs IsNot Nothing Then
                            CtlJour.VText_Matin = FicheRglAbs.CodeRegroupement
                        Else
                            CtlJour.VText_Matin = FicheVue.ItemPlage(IndiceP).EvenementPlage
                        End If
                    End If
                    CtlJour.VToolTip_Matin = FicheVue.ItemPlage(IndiceP).EvenementPlage
                Case Is = VI.NumeroPlage.Plage2_Absence
                    CtlJour.VBackColor_Soir = Couleur
                    If FicheVue.ItemPlage(IndiceP).EvenementPlage <> "" Then
                        FicheRglAbs = WebFct.PointeurUtilisateur.PointeurParametres.PointeurAbsences.Fiche_Absence(FicheVue.ItemPlage(IndiceP).EvenementPlage)
                        If FicheRglAbs IsNot Nothing Then
                            CtlJour.VText_Soir = FicheRglAbs.CodeRegroupement
                        Else
                            CtlJour.VText_Soir = FicheVue.ItemPlage(IndiceP).EvenementPlage
                        End If
                    End If
                    CtlJour.VToolTip_Soir = FicheVue.ItemPlage(IndiceP).EvenementPlage
                Case Is = VI.NumeroPlage.Jour_Formation
                    Couleur = WebFct.PointeurUtilisateur.PointeurParametres.CouleurPlanning(WebFct.PointeurUtilisateur.PointeurParametres.IndexCouleurPlanning("Formation"))
                    CtlJour.VBackColor_Matin = Couleur
                    CtlJour.VBackColor_Soir = Couleur
                    CtlJour.VText_Matin = "FO"
                    CtlJour.VText_Soir = "FO"
                    If FicheVue.IntituleduJour <> "" Then
                        CtlJour.VToolTip_Matin = FicheVue.IntituleduJour
                        CtlJour.VToolTip_Soir = FicheVue.IntituleduJour
                    Else
                        CtlJour.VToolTip_Matin = FicheVue.ItemPlage(IndiceP).EvenementPlage
                        CtlJour.VToolTip_Soir = FicheVue.ItemPlage(IndiceP).EvenementPlage
                    End If
                Case Is = VI.NumeroPlage.Plage1_Formation
                    Couleur = WebFct.PointeurUtilisateur.PointeurParametres.CouleurPlanning(WebFct.PointeurUtilisateur.PointeurParametres.IndexCouleurPlanning("Formation"))
                    CtlJour.VBackColor_Matin = Couleur
                    CtlJour.VText_Matin = "FO"
                    If FicheVue.IntituleduJour <> "" Then
                        CtlJour.VToolTip_Matin = FicheVue.IntituleduJour
                    Else
                        CtlJour.VToolTip_Matin = FicheVue.ItemPlage(IndiceP).EvenementPlage
                    End If
                Case Is = VI.NumeroPlage.Plage2_Formation
                    Couleur = WebFct.PointeurUtilisateur.PointeurParametres.CouleurPlanning(WebFct.PointeurUtilisateur.PointeurParametres.IndexCouleurPlanning("Formation"))
                    CtlJour.VBackColor_Soir = Couleur
                    CtlJour.VText_Soir = "FO"
                    If FicheVue.IntituleduJour <> "" Then
                        CtlJour.VToolTip_Soir = FicheVue.IntituleduJour
                    Else
                        CtlJour.VToolTip_Soir = FicheVue.ItemPlage(IndiceP).EvenementPlage
                    End If
                Case Is = VI.NumeroPlage.Jour_Mission
                    Couleur = WebFct.PointeurUtilisateur.PointeurParametres.CouleurPlanning(WebFct.PointeurUtilisateur.PointeurParametres.IndexCouleurPlanning("Mission"))
                    CtlJour.VBackColor_Matin = Couleur
                    CtlJour.VBackColor_Soir = Couleur
                    CtlJour.VText_Matin = "MI"
                    CtlJour.VText_Soir = "MI"
                    If FicheVue.IntituleduJour <> "" Then
                        CtlJour.VToolTip_Matin = FicheVue.IntituleduJour
                        CtlJour.VToolTip_Soir = FicheVue.IntituleduJour
                    Else
                        CtlJour.VToolTip_Matin = FicheVue.ItemPlage(IndiceP).EvenementPlage
                        CtlJour.VToolTip_Soir = FicheVue.ItemPlage(IndiceP).EvenementPlage
                    End If
                Case Is = VI.NumeroPlage.Plage1_Mission
                    Couleur = WebFct.PointeurUtilisateur.PointeurParametres.CouleurPlanning(WebFct.PointeurUtilisateur.PointeurParametres.IndexCouleurPlanning("Mission"))
                    CtlJour.VBackColor_Matin = Couleur
                    CtlJour.VText_Matin = "MI"
                    If FicheVue.IntituleduJour <> "" Then
                        CtlJour.VToolTip_Matin = FicheVue.IntituleduJour
                    Else
                        CtlJour.VToolTip_Matin = FicheVue.ItemPlage(IndiceP).EvenementPlage
                    End If
                Case Is = VI.NumeroPlage.Plage2_Mission
                    Couleur = WebFct.PointeurUtilisateur.PointeurParametres.CouleurPlanning(WebFct.PointeurUtilisateur.PointeurParametres.IndexCouleurPlanning("Mission"))
                    CtlJour.VBackColor_Soir = Couleur
                    CtlJour.VText_Soir = "MI"
                    If FicheVue.IntituleduJour <> "" Then
                        CtlJour.VToolTip_Soir = FicheVue.IntituleduJour
                    Else
                        CtlJour.VToolTip_Soir = FicheVue.ItemPlage(IndiceP).EvenementPlage
                    End If
            End Select
        Next IndiceP
        CtlJour.VHeures = Horaires
    End Sub

    Private Sub FaireListeMois()
        Dim Chaine As New System.Text.StringBuilder
        Dim Libel As String = "Aucun mois" & VI.Tild & "Un mois" & VI.Tild & "possibilités"
        Dim Annee As Integer
        Dim IndiceI As Integer
        Dim IndiceA As Integer
        Annee = CInt(Strings.Right(WebFct.ViRhDates.DateduJour(False), 4))

        IndiceA = Annee + 1
        Do
            For IndiceI = 12 To 1 Step -1
                Chaine.Append(WebFct.ViRhDates.MoisEnClair(CShort(IndiceI)) & Strings.Space(1) & IndiceA.ToString & VI.Tild)
            Next IndiceI
            IndiceA -= 1
            If IndiceA < Annee - 3 Then
                Exit Do
            End If
        Loop
        LstLibelMois.V_Liste(Libel) = Chaine.ToString
        Chaine.Clear()
        LstLibelMois.LstText = WebFct.ViRhDates.MoisEnClair(1) & Strings.Space(1) & Annee.ToString
    End Sub
End Class