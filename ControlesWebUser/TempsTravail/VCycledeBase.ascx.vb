﻿Option Strict On
Option Explicit On
Option Compare Text
Imports VI = Virtualia.Systeme.Constantes
Partial Class Controles_VCycledeBase
    Inherits System.Web.UI.UserControl
    Private WebFct As Virtualia.Net.WebFonctions
    '
    Public WriteOnly Property SiJourVisible(ByVal NoJour As Integer, ByVal Prefixe As String) As Boolean
        Set(ByVal value As Boolean)
            Dim Ctl As Control
            Dim VirControle As System.Web.UI.WebControls.Label
            Ctl = WebFct.VirWebControle(Me, Prefixe, NoJour)
            If Ctl Is Nothing Then
                Exit Property
            End If
            VirControle = CType(Ctl, System.Web.UI.WebControls.Label)
            VirControle.Visible = value
            If value = False Then
                VirControle.BackColor = Drawing.Color.Transparent
                VirControle.BorderStyle = BorderStyle.None
                VirControle.Text = ""
                VirControle.ToolTip = ""
            Else
                VirControle.BorderStyle = BorderStyle.Solid
            End If
        End Set
    End Property

    Public WriteOnly Property VEtiText() As String
        Set(ByVal value As String)
            VIntitule.Text = value
        End Set
    End Property

    Public WriteOnly Property VEtiToolTip() As String
        Set(ByVal value As String)
            VIntitule.ToolTip = value
        End Set
    End Property

    Public WriteOnly Property VText(ByVal NoJour As Integer, ByVal Prefixe As String) As String
        Set(ByVal value As String)
            Dim Ctl As Control
            Dim VirControle As System.Web.UI.WebControls.Label
            Ctl = WebFct.VirWebControle(Me, Prefixe, NoJour)
            If Ctl Is Nothing Then
                Exit Property
            End If
            VirControle = CType(Ctl, System.Web.UI.WebControls.Label)
            VirControle.Text = value
        End Set
    End Property

    Public WriteOnly Property VDemiJourToolTip(ByVal NoJour As Integer, ByVal Prefixe As String) As String
        Set(ByVal value As String)
            Dim Ctl As Control
            Dim VirControle As System.Web.UI.WebControls.Label
            Ctl = WebFct.VirWebControle(Me, Prefixe, NoJour)
            If Ctl Is Nothing Then
                Exit Property
            End If
            VirControle = CType(Ctl, System.Web.UI.WebControls.Label)
            VirControle.ToolTip = value
        End Set
    End Property

    Public WriteOnly Property VBorduresColor(ByVal Prefixe As String) As System.Drawing.Color
        Set(ByVal value As System.Drawing.Color)
            Dim IndiceI As Integer
            If WebFct Is Nothing Then
                WebFct = New Virtualia.Net.WebFonctions(Me, 0)
            End If
            For IndiceI = 0 To 36
                VBorderColor(IndiceI, Prefixe) = value
            Next IndiceI
        End Set
    End Property

    Public WriteOnly Property VBackColor(ByVal NoJour As Integer, ByVal Prefixe As String) As System.Drawing.Color
        Set(ByVal value As System.Drawing.Color)
            Dim Ctl As Control
            Dim VirControle As System.Web.UI.WebControls.Label
            Ctl = WebFct.VirWebControle(Me, Prefixe, NoJour)
            If Ctl Is Nothing Then
                Exit Property
            End If
            VirControle = CType(Ctl, System.Web.UI.WebControls.Label)
            VirControle.BackColor = value
        End Set
    End Property

    Public WriteOnly Property VBorderColor(ByVal NoJour As Integer, ByVal Prefixe As String) As System.Drawing.Color
        Set(ByVal value As System.Drawing.Color)
            Dim Ctl As Control
            Dim VirControle As System.Web.UI.WebControls.Label
            Ctl = WebFct.VirWebControle(Me, Prefixe, NoJour)
            If Ctl Is Nothing Then
                Exit Property
            End If
            VirControle = CType(Ctl, System.Web.UI.WebControls.Label)
            VirControle.BorderColor = value
        End Set
    End Property

    Public WriteOnly Property VJoursStyle() As String
        Set(ByVal value As String)
            Dim IndiceI As Integer
            If WebFct Is Nothing Then
                WebFct = New Virtualia.Net.WebFonctions(Me, 0)
            End If
            For IndiceI = 0 To 36
                VJourStyle(IndiceI, "JAM") = value
                VJourStyle(IndiceI, "JPM") = value
            Next IndiceI
        End Set
    End Property

    Public WriteOnly Property VJourStyle(ByVal NoJour As Integer, ByVal Prefixe As String) As String
        Set(ByVal value As String)
            Dim Ctl As Control
            Dim VirControle As System.Web.UI.WebControls.Label
            Dim StyleBordure As String = "border-top-style"
            Dim NumBouton As Integer

            If Prefixe = "JAM" Then
                StyleBordure = "border-bottom-style"
            End If

            Ctl = WebFct.VirWebControle(Me, Prefixe, NoJour)
            If Ctl Is Nothing Then
                Exit Property
            End If
            VirControle = CType(Ctl, System.Web.UI.WebControls.Label)
            NumBouton = CInt(Strings.Right(VirControle.ID, 2))
            If NumBouton = NoJour Then
                VirControle.Style.Remove(StyleBordure)
                VirControle.Style.Add(StyleBordure, value)
            End If

        End Set
    End Property

    Public WriteOnly Property VFontSize(ByVal NoJour As Integer, ByVal Prefixe As String) As System.Web.UI.WebControls.FontUnit
        Set(ByVal value As System.Web.UI.WebControls.FontUnit)
            Dim Ctl As Control
            Dim VirControle As System.Web.UI.WebControls.Label
            Ctl = WebFct.VirWebControle(Me, Prefixe, NoJour)
            If Ctl Is Nothing Then
                Exit Property
            End If
            VirControle = CType(Ctl, System.Web.UI.WebControls.Label)
            VirControle.Font.Size = value
        End Set
    End Property

    Public WriteOnly Property VFontBold(ByVal NoJour As Integer, ByVal Prefixe As String) As Boolean
        Set(ByVal value As Boolean)
            Dim Ctl As Control
            Dim VirControle As System.Web.UI.WebControls.Label
            Ctl = WebFct.VirWebControle(Me, Prefixe, NoJour)
            If Ctl Is Nothing Then
                Exit Property
            End If
            VirControle = CType(Ctl, System.Web.UI.WebControls.Label)
            VirControle.Font.Bold = value
        End Set
    End Property

    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        WebFct = New Virtualia.Net.WebFonctions(Me, 0)
    End Sub
End Class

