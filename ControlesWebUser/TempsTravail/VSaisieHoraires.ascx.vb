﻿Option Strict On
Option Explicit On
Option Compare Text
Imports VI = Virtualia.Systeme.Constantes
Imports Microsoft.VisualBasic
Partial Class Controles_VSaisieHoraires
    Inherits System.Web.UI.UserControl
    Private WebFct As Virtualia.Net.WebFonctions
    Public Delegate Sub Valeur_ChangeEventHandler(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.DonneeChangeEventArgs)
    Public Event ValeurChange As Valeur_ChangeEventHandler
    '***********************************************************

    Protected Overridable Sub Saisie_Change(ByVal e As Virtualia.Systeme.Evenements.DonneeChangeEventArgs)
        RaiseEvent ValeurChange(Me, e)
    End Sub

    Public WriteOnly Property V_FicheSemaine As Virtualia.TablesObjet.ShemaPER.PER_CET_HEBDO
        Set(ByVal value As Virtualia.TablesObjet.ShemaPER.PER_CET_HEBDO)
            If value Is Nothing Then
                Exit Property
            End If
            Dim I As Integer
            Dim ResAide As ArrayList
            For I = 0 To 6
                ResAide = New ArrayList
                If value.HeureDeDebutPlage(I, 0) IsNot Nothing Then
                    ResAide.Add(value.HeureDeDebutPlage(I, 0))
                Else
                    ResAide.Add("")
                End If
                If value.HeureDeFinPlage(I, 0) IsNot Nothing Then
                    ResAide.Add(value.HeureDeFinPlage(I, 0))
                Else
                    ResAide.Add("")
                End If
                If value.HeureDeDebutPlage(I, 1) IsNot Nothing Then
                    ResAide.Add(value.HeureDeDebutPlage(I, 1))
                Else
                    ResAide.Add("")
                End If
                If value.HeureDeFinPlage(I, 1) IsNot Nothing Then
                    ResAide.Add(value.HeureDeFinPlage(I, 1))
                Else
                    ResAide.Add("")
                End If
                Select Case I
                    Case Is = 0
                        SaisieLundi.VText = WebFct.ViRhDates.ClairDate(value.DateJournee(I), True)
                        SaisieLundi.VHeures = ResAide
                    Case Is = 1
                        SaisieMardi.VText = WebFct.ViRhDates.ClairDate(value.DateJournee(I), True)
                        SaisieMardi.VHeures = ResAide
                    Case Is = 2
                        SaisieMercredi.VText = WebFct.ViRhDates.ClairDate(value.DateJournee(I), True)
                        SaisieMercredi.VHeures = ResAide
                    Case Is = 3
                        SaisieJeudi.VText = WebFct.ViRhDates.ClairDate(value.DateJournee(I), True)
                        SaisieJeudi.VHeures = ResAide
                    Case Is = 4
                        SaisieVendredi.VText = WebFct.ViRhDates.ClairDate(value.DateJournee(I), True)
                        SaisieVendredi.VHeures = ResAide
                    Case Is = 5
                        SaisieSamedi.VText = WebFct.ViRhDates.ClairDate(value.DateJournee(I), True)
                        SaisieSamedi.VHeures = ResAide
                    Case Is = 6
                        SaisieDimanche.VText = WebFct.ViRhDates.ClairDate(value.DateJournee(I), True)
                        SaisieDimanche.VHeures = ResAide
                End Select
            Next I
            Call FaireTotalSemaine()
        End Set
    End Property

    Public Sub Initialiser()
        TotalSemaine.Text = ""
        CadreAide.Visible = False
        CadreSaisie.Enabled = True
    End Sub

    Private Sub ConsoliderSaisieAssistee()
        Dim TableauW(0) As String
        Dim Chaine As String = ""
        Dim Minutes_A As Integer
        Dim Minutes_D As Integer
        Dim TotHeures As Integer
        Dim TotMinutes As Integer = 0
        Dim TotJour As Integer = 0

        If SelMatinA.Text <> "" And SelMatinD.Text <> "" Then
            TableauW = Strings.Split(SelMatinA.Text, ":")
            Minutes_A = (CInt(TableauW(0)) * 60) + CInt(TableauW(1))
            TableauW = Strings.Split(SelMatinD.Text, ":")
            Minutes_D = (CInt(TableauW(0)) * 60) + CInt(TableauW(1))
            TotMinutes = Minutes_D - Minutes_A
            TotHeures = TotMinutes \ 60
            Chaine = TotHeures.ToString & " h " & Strings.Format(TotMinutes Mod 60, "00")
            SelTotMatin.Text = Chaine
        Else
            SelTotMatin.Text = ""
        End If
        TotJour += TotMinutes
        TotMinutes = 0
        If SelSoirA.Text <> "" And SelSoirD.Text <> "" Then
            TableauW = Strings.Split(SelSoirA.Text, ":")
            Minutes_A = (CInt(TableauW(0)) * 60) + CInt(TableauW(1))
            TableauW = Strings.Split(SelSoirD.Text, ":")
            Minutes_D = (CInt(TableauW(0)) * 60) + CInt(TableauW(1))
            TotMinutes = Minutes_D - Minutes_A
            TotHeures = TotMinutes \ 60
            Chaine = TotHeures.ToString & " h " & Strings.Format(TotMinutes Mod 60, "00")
            SelTotSoir.Text = Chaine
        Else
            SelTotSoir.Text = ""
        End If
        TotJour += TotMinutes
        TotHeures = TotJour \ 60
        Chaine = TotHeures.ToString & " h " & Strings.Format(TotJour Mod 60, "00")
    End Sub

    Private Sub FaireListeHeures()
        Dim TableauW(0) As String
        Dim IndiceHH As Integer
        Dim IndiceMN As Integer
        Dim Heure As String

        LstMatinA.Items.Clear()
        LstMatinD.Items.Clear()
        LstSoirA.Items.Clear()
        LstSoirD.Items.Clear()

        IndiceHH = 0
        IndiceMN = 0
        Do
            Heure = Strings.Format(IndiceHH, "00") & ":" & Strings.Format(IndiceMN, "00")
            LstMatinA.Items.Add(Heure)
            LstMatinD.Items.Add(Heure)
            LstSoirA.Items.Add(Heure)
            LstSoirD.Items.Add(Heure)

            IndiceMN += 5
            If IndiceMN >= 60 Then
                IndiceHH += 1
                If IndiceHH > 23 Then
                    Exit Do
                End If
                IndiceMN = 0
            End If
        Loop
        TableauW = Strings.Split(HSelLst.Value, VI.PointVirgule)
        If TableauW(0) <> "" Then
            Try
                LstMatinA.SelectedValue = TableauW(0)
            Catch ex As Exception
                Exit Try
            End Try
        End If
        If TableauW.Count < 3 Then
            Exit Sub
        End If
        If TableauW(1) <> "" Then
            Try
                LstMatinD.SelectedValue = TableauW(1)
            Catch ex As Exception
                Exit Try
            End Try
        End If
        If TableauW(2) <> "" Then
            Try
                LstSoirA.SelectedValue = TableauW(2)
            Catch ex As Exception
                Exit Try
            End Try
        End If
        If TableauW(3) <> "" Then
            Try
                LstSoirD.SelectedValue = TableauW(3)
            Catch ex As Exception
                Exit Try
            End Try
        End If
    End Sub

    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        WebFct = New Virtualia.Net.WebFonctions(Me, 0)
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Call FaireListeHeures()
    End Sub

    Protected Sub LstMatinA_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles LstMatinA.SelectedIndexChanged
        Dim TableauW(0) As String
        If SelMatinD.Text <> "" Then
            Select Case WebFct.ViRhDates.ComparerHeures(LstMatinA.SelectedValue, SelMatinD.Text)
                Case VI.ComparaisonDates.Egalite, VI.ComparaisonDates.PlusGrand
                    Exit Sub
            End Select
        End If
        If SelSoirA.Text <> "" Then
            Select Case WebFct.ViRhDates.ComparerHeures(LstMatinA.SelectedValue, SelSoirA.Text)
                Case VI.ComparaisonDates.Egalite, VI.ComparaisonDates.PlusGrand
                    Exit Sub
            End Select
        End If
        SelMatinA.Text = LstMatinA.SelectedValue
        TableauW = Strings.Split(HSelLst.Value, VI.PointVirgule)
        TableauW(0) = LstMatinA.SelectedValue
        HSelLst.Value = Strings.Join(TableauW, VI.PointVirgule)
        Call ConsoliderSaisieAssistee()
    End Sub

    Protected Sub LstMatinD_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles LstMatinD.SelectedIndexChanged
        Dim TableauW(0) As String
        If SelMatinA.Text <> "" Then
            Select Case WebFct.ViRhDates.ComparerHeures(LstMatinD.SelectedValue, SelMatinA.Text)
                Case VI.ComparaisonDates.Egalite, VI.ComparaisonDates.PlusPetit
                    Exit Sub
            End Select
        End If
        If SelSoirA.Text <> "" Then
            Select Case WebFct.ViRhDates.ComparerHeures(LstMatinD.SelectedValue, SelSoirA.Text)
                Case VI.ComparaisonDates.Egalite, VI.ComparaisonDates.PlusGrand
                    Exit Sub
            End Select
        End If
        SelMatinD.Text = LstMatinD.SelectedValue
        TableauW = Strings.Split(HSelLst.Value, VI.PointVirgule)
        TableauW(1) = LstMatinD.SelectedValue
        HSelLst.Value = Strings.Join(TableauW, VI.PointVirgule)
        Call ConsoliderSaisieAssistee()
    End Sub

    Protected Sub LstSoirA_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles LstSoirA.SelectedIndexChanged
        Dim TableauW(0) As String
        If SelMatinD.Text <> "" Then
            Select Case WebFct.ViRhDates.ComparerHeures(LstSoirA.SelectedValue, SelMatinD.Text)
                Case VI.ComparaisonDates.Egalite, VI.ComparaisonDates.PlusPetit
                    Exit Sub
            End Select
        End If
        If SelSoirD.Text <> "" Then
            Select Case WebFct.ViRhDates.ComparerHeures(LstSoirA.SelectedValue, SelSoirD.Text)
                Case VI.ComparaisonDates.Egalite, VI.ComparaisonDates.PlusGrand
                    Exit Sub
            End Select
        End If
        SelSoirA.Text = LstSoirA.SelectedValue
        TableauW = Strings.Split(HSelLst.Value, VI.PointVirgule)
        TableauW(2) = LstSoirA.SelectedValue
        HSelLst.Value = Strings.Join(TableauW, VI.PointVirgule)
        Call ConsoliderSaisieAssistee()
    End Sub

    Protected Sub LstSoirD_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles LstSoirD.SelectedIndexChanged
        Dim TableauW(0) As String
        If SelMatinD.Text <> "" Then
            Select Case WebFct.ViRhDates.ComparerHeures(LstSoirD.SelectedValue, SelMatinD.Text)
                Case VI.ComparaisonDates.Egalite, VI.ComparaisonDates.PlusPetit
                    Exit Sub
            End Select
        End If
        If SelSoirA.Text <> "" Then
            Select Case WebFct.ViRhDates.ComparerHeures(LstSoirD.SelectedValue, SelSoirA.Text)
                Case VI.ComparaisonDates.Egalite, VI.ComparaisonDates.PlusPetit
                    Exit Sub
            End Select
        End If
        SelSoirD.Text = LstSoirD.SelectedValue
        TableauW = Strings.Split(HSelLst.Value, VI.PointVirgule)
        TableauW(3) = LstSoirD.SelectedValue
        HSelLst.Value = Strings.Join(TableauW, VI.PointVirgule)
        Call ConsoliderSaisieAssistee()
    End Sub

    Protected Sub CommandeOK_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles CommandeOK.Click
        Dim ResAide As New ArrayList
        ResAide.Add(SelMatinA.Text)
        ResAide.Add(SelMatinD.Text)
        ResAide.Add(SelSoirA.Text)
        ResAide.Add(SelSoirD.Text)
        Select Case HSelJour.Value
            Case Is = "1"
                SaisieLundi.VHeures = ResAide
            Case Is = "2"
                SaisieMardi.VHeures = ResAide
            Case Is = "3"
                SaisieMercredi.VHeures = ResAide
            Case Is = "4"
                SaisieJeudi.VHeures = ResAide
            Case Is = "5"
                SaisieVendredi.VHeures = ResAide
            Case Is = "6"
                SaisieSamedi.VHeures = ResAide
            Case Is = "7"
                SaisieDimanche.VHeures = ResAide
        End Select
        Call FaireTotalSemaine()
        CadreAide.Visible = False
        CadreSaisie.Enabled = True

        Dim Evenement As Virtualia.Systeme.Evenements.DonneeChangeEventArgs
        Evenement = New Virtualia.Systeme.Evenements.DonneeChangeEventArgs(HSelJour.Value)
        Saisie_Change(Evenement)
    End Sub

    Protected Sub CommandeAnnuler_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles CommandeAnnuler.Click
        Call FaireTotalSemaine()
        CadreAide.Visible = False
        CadreSaisie.Enabled = True
    End Sub

    Private Sub FaireTotalSemaine()
        Dim TotMinutes As Integer
        Dim Chaine As String = ""

        TotMinutes = SaisieLundi.VTotalJour_EnMinutes + SaisieMardi.VTotalJour_EnMinutes
        TotMinutes += SaisieMercredi.VTotalJour_EnMinutes + SaisieJeudi.VTotalJour_EnMinutes
        TotMinutes += SaisieVendredi.VTotalJour_EnMinutes + SaisieSamedi.VTotalJour_EnMinutes
        TotMinutes += SaisieDimanche.VTotalJour_EnMinutes
        Chaine = (TotMinutes \ 60).ToString & " h " & Strings.Format(TotMinutes Mod 60, "00")
        TotalSemaine.Text = Chaine
    End Sub

    Protected Sub SaisieJour_ValeurChange(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.DonneeChangeEventArgs) _
    Handles SaisieLundi.ValeurChange, SaisieMardi.ValeurChange, SaisieMercredi.ValeurChange, SaisieJeudi.ValeurChange, _
            SaisieVendredi.ValeurChange, SaisieSamedi.ValeurChange, SaisieDimanche.ValeurChange
        Dim Chaine As String
        Select Case e.Parametre
            Case Is = "Heure"
                Call FaireTotalSemaine()
                Dim Evenement As Virtualia.Systeme.Evenements.DonneeChangeEventArgs
                Evenement = New Virtualia.Systeme.Evenements.DonneeChangeEventArgs(HSelJour.Value)
                Saisie_Change(Evenement)
            Case Is = "Jour"
                Dim TabHor As ArrayList = Nothing
                HSelJour.Value = e.Valeur
                CadreAide.Visible = True
                Select Case e.Valeur
                    Case Is = "1"
                        TabHor = SaisieLundi.VHeures
                    Case Is = "2"
                        TabHor = SaisieMardi.VHeures
                    Case Is = "3"
                        TabHor = SaisieMercredi.VHeures
                    Case Is = "4"
                        TabHor = SaisieJeudi.VHeures
                    Case Is = "5"
                        TabHor = SaisieVendredi.VHeures
                    Case Is = "6"
                        TabHor = SaisieSamedi.VHeures
                    Case Is = "7"
                        TabHor = SaisieDimanche.VHeures
                End Select
                If TabHor IsNot Nothing Then
                    If TabHor(0).ToString <> "" Then
                        Chaine = TabHor(0).ToString & VI.PointVirgule & TabHor(1).ToString & VI.PointVirgule
                        Chaine &= TabHor(2).ToString & VI.PointVirgule & TabHor(3).ToString
                        HSelLst.Value = Chaine
                        SelMatinA.Text = TabHor(0).ToString
                        SelMatinD.Text = TabHor(1).ToString
                        SelSoirA.Text = TabHor(2).ToString
                        SelSoirD.Text = TabHor(3).ToString
                        Try
                            LstMatinA.SelectedValue = TabHor(0).ToString
                        Catch ex As Exception
                            Exit Try
                        End Try
                        Try
                            LstMatinD.SelectedValue = TabHor(1).ToString
                        Catch ex As Exception
                            Exit Try
                        End Try
                        Try
                            LstSoirA.SelectedValue = TabHor(2).ToString
                        Catch ex As Exception
                            Exit Try
                        End Try
                        Try
                            LstSoirD.SelectedValue = TabHor(3).ToString
                        Catch ex As Exception
                            Exit Try
                        End Try
                    End If
                Else
                    SelMatinA.Text = ""
                    SelMatinD.Text = ""
                    SelSoirA.Text = ""
                    SelSoirD.Text = ""
                End If
                Call ConsoliderSaisieAssistee()
                CadreSaisie.Enabled = False
        End Select
    End Sub

End Class
