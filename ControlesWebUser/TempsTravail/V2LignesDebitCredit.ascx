﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="V2LignesDebitCredit.ascx.vb" Inherits="Virtualia.Net.Controles_V2LignesDebitCredit" %>

   <asp:Table ID="LignesDebitCredit" runat="server" CellPadding="0" CellSpacing="0" Width="750px"
        BorderStyle= "Solid" BorderWidth="2px" BorderColor="#B0E0D7" HorizontalAlign="Left" Visible="true">
        <asp:TableRow ID="Ligne1" Width="748px" >
            <asp:TableCell ID="CellTitreLigne1" BackColor="#CAEBE4" BorderColor="#CAEBE4" BorderStyle="Notset"> 
              <asp:Label ID="LabelTitreLigne1" runat="server" Height="16px" Width="204px"
                  BackColor="#CAEBE4" BorderColor="#CAEBE4" BorderStyle="Notset" Text=""
                  BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                  Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Smaller"
                  style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                  font-style: normal; text-indent: 2px; text-align:  left; padding-top: 0px;">
              </asp:Label>          
            </asp:TableCell>
            <asp:TableCell ID="CellLigne1M01" BackColor="#CAEBE4" BorderColor="#CAEBE4" BorderStyle="Notset"> 
              <asp:Label ID="Ligne1M01" runat="server" Height="16px" Width="43px"
                        BackColor="#CAEBE4" BorderColor="#98D4CA" BorderStyle="NotSet" Text=""
                        BorderWidth="1px" ForeColor="Black"
                        Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="11px" Font-Italic="false"
                        style="margin-top: 1px; margin-left: 0px; text-indent: 0px; text-align: center; padding-top: 3px;"> 
              </asp:Label>
            </asp:TableCell>
            <asp:TableCell ID="CellLigne1M02" BackColor="#CAEBE4" BorderColor="#CAEBE4" BorderStyle="Notset"> 
              <asp:Label ID="Ligne1M02" runat="server" Height="16px" Width="43px"
                        BackColor="#CAEBE4" BorderColor="#98D4CA" BorderStyle="Notset" Text=""
                        BorderWidth="1px" ForeColor="Black"
                        Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="11px" Font-Italic="false"
                        style="margin-top: 1px; margin-left: 0px; text-indent: 0px; text-align: center; padding-top: 3px;"> 
              </asp:Label> 
            </asp:TableCell>
            <asp:TableCell ID="CellLigne1M03" BackColor="#CAEBE4" BorderColor="#CAEBE4" BorderStyle="Notset"> 
              <asp:Label ID="Ligne1M03" runat="server" Height="16px" Width="43px"
                        BackColor="#CAEBE4" BorderColor="#98D4CA" BorderStyle="NotSet" Text=""
                        BorderWidth="1px" ForeColor="Black"
                        Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="11px" Font-Italic="false"
                        style="margin-top: 1px; margin-left: 0px; text-indent: 0px; text-align: center; padding-top: 3px;"> 
              </asp:Label>         
            </asp:TableCell>
            <asp:TableCell ID="CellLigne1M04" BackColor="#CAEBE4" BorderColor="#CAEBE4" BorderStyle="Notset"> 
              <asp:Label ID="Ligne1M04" runat="server" Height="16px" Width="43px" 
                        BackColor="#CAEBE4" BorderColor="#98D4CA" BorderStyle="Notset" Text=""
                        BorderWidth="1px" ForeColor="Black" 
                        Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="11px" Font-Italic="false"
                        style="margin-top: 1px; margin-left: 0px; text-indent: 0px; text-align: center; padding-top: 3px;"> 
              </asp:Label>           
            </asp:TableCell>
            <asp:TableCell ID="CellLigne1M05" BackColor="#CAEBE4" BorderColor="#CAEBE4" BorderStyle="Notset"> 
              <asp:Label ID="Ligne1M05" runat="server" Height="16px" Width="43px"
                        BackColor="#CAEBE4" BorderColor="#98D4CA" BorderStyle="Notset" Text=""
                        BorderWidth="1px" ForeColor="Black"
                        Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="11px" Font-Italic="false"
                        style="margin-top: 1px; margin-left: 0px; text-indent: 0px; text-align: center; padding-top: 3px;"> 
              </asp:Label>           
            </asp:TableCell>
            <asp:TableCell ID="CellLigne1M06" BackColor="#CAEBE4" BorderColor="#CAEBE4" BorderStyle="Notset"> 
              <asp:Label ID="Ligne1M06" runat="server" Height="16px" Width="43px"
                        BackColor="#CAEBE4" BorderColor="#98D4CA" BorderStyle="Notset" Text=""
                        BorderWidth="1px" ForeColor="Black"
                        Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="11px" Font-Italic="false"
                        style="margin-top: 1px; margin-left: 0px; text-indent: 0px; text-align: center; padding-top: 3px;"> 
              </asp:Label>        
            </asp:TableCell>
            <asp:TableCell ID="CellLigne1M07" BackColor="#CAEBE4" BorderColor="#CAEBE4" BorderStyle="Notset"> 
              <asp:Label ID="Ligne1M07" runat="server" Height="16px" Width="43px"
                        BackColor="#CAEBE4" BorderColor="#98D4CA" BorderStyle="Notset" Text=""
                        BorderWidth="1px" ForeColor="Black"
                        Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="11px" Font-Italic="false"
                        style="margin-top: 1px; margin-left: 0px; text-indent: 0px; text-align: center; padding-top: 3px;"> 
              </asp:Label>           
            </asp:TableCell>
            <asp:TableCell ID="CellLigne1M08" BackColor="#CAEBE4" BorderColor="#CAEBE4" BorderStyle="Notset"> 
              <asp:Label ID="Ligne1M08" runat="server" Height="16px" Width="43px"
                        BackColor="#CAEBE4" BorderColor="#98D4CA" BorderStyle="Notset" Text=""
                        BorderWidth="1px" ForeColor="Black"
                        Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="11px" Font-Italic="false"
                        style="margin-top: 1px; margin-left: 0px; text-indent: 0px; text-align: center; padding-top: 3px;"> 
              </asp:Label>           
            </asp:TableCell>
            <asp:TableCell ID="CellLigne1M09" BackColor="#CAEBE4" BorderColor="#CAEBE4" BorderStyle="Notset"> 
              <asp:Label ID="Ligne1M09" runat="server" Height="16px" Width="43px"
                        BackColor="#CAEBE4" BorderColor="#98D4CA" BorderStyle="Notset" Text=""
                        BorderWidth="1px" ForeColor="Black"
                        Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="11px" Font-Italic="false"
                        style="margin-top: 1px; margin-left: 0px; text-indent: 0px; text-align: center; padding-top: 3px;"> 
              </asp:Label>           
            </asp:TableCell>
            <asp:TableCell ID="CellLigne1M10" BackColor="#CAEBE4" BorderColor="#CAEBE4" BorderStyle="Notset"> 
              <asp:Label ID="Ligne1M10" runat="server" Height="16px" Width="43px"
                        BackColor="#CAEBE4" BorderColor="#98D4CA" BorderStyle="Notset" Text=""
                        BorderWidth="1px" ForeColor="Black"
                        Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="11px" Font-Italic="false"
                        style="margin-top: 1px; margin-left: 0px; text-indent: 0px; text-align: center; padding-top: 3px;"> 
              </asp:Label>           
            </asp:TableCell>
            <asp:TableCell ID="CellLigne1M11" BackColor="#CAEBE4" BorderColor="#CAEBE4" BorderStyle="Notset"> 
              <asp:Label ID="Ligne1M11" runat="server" Height="16px" Width="43px"
                        BackColor="#CAEBE4" BorderColor="#98D4CA" BorderStyle="Notset" Text=""
                        BorderWidth="1px" ForeColor="Black"
                        Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="11px" Font-Italic="false"
                        style="margin-top: 1px; margin-left: 0px; text-indent: 0px; text-align: center; padding-top: 3px;"> 
              </asp:Label>           
            </asp:TableCell>
            <asp:TableCell ID="CellLigne1M12" BackColor="#CAEBE4" BorderColor="#CAEBE4" BorderStyle="Notset"> 
              <asp:Label ID="Ligne1M12" runat="server" Height="16px" Width="43px"
                        BackColor="#CAEBE4" BorderColor="#98D4CA" BorderStyle="Notset" Text=""
                        BorderWidth="1px" ForeColor="Black"
                        Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="11px" Font-Italic="false"
                        style="margin-top: 1px; margin-left: 0px; text-indent: 0px; text-align: center; padding-top: 3px;"> 
              </asp:Label>           
            </asp:TableCell>     
        </asp:TableRow>
        <asp:TableRow ID="Ligne2" Width="748px" >
            <asp:TableCell ID="CellTitreLigne2" BackColor="#B0E0D7" BorderColor="#B0E0D7" BorderStyle="Notset"> 
              <asp:Label ID="LabelTitreLigne2" runat="server" Height="16px" Width="204px"
                  BackColor="#B0E0D7" BorderColor="#B0E0D7" BorderStyle="Notset" Text="Crédit d'heures récupérées"
                  BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                  Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Smaller"
                  style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                  font-style: normal; text-indent: 2px; text-align:  left; padding-top: 0px;">
              </asp:Label>          
            </asp:TableCell>
            <asp:TableCell ID="CellLigne2M01" BackColor="#B0E0D7" BorderColor="#B0E0D7" BorderStyle="Notset"> 
              <asp:Label ID="Ligne2M01" runat="server" Height="16px" Width="43px"
                        BackColor="#B0E0D7" BorderColor="#CAEBE4" BorderStyle="Notset" Text=""
                        BorderWidth="1px" ForeColor="Black"
                        Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="11px" Font-Italic="false"
                        style="margin-top: 1px; margin-left: 0px; text-indent: 0px; text-align: center; padding-top: 3px;"> 
              </asp:Label>
            </asp:TableCell>
            <asp:TableCell ID="CellLigne2M02" BackColor="#B0E0D7" BorderColor="#B0E0D7" BorderStyle="Notset"> 
              <asp:Label ID="Ligne2M02" runat="server" Height="16px" Width="43px"
                        BackColor="#B0E0D7" BorderColor="#CAEBE4" BorderStyle="Notset" Text=""
                        BorderWidth="1px" ForeColor="Black"
                        Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="11px" Font-Italic="false"
                        style="margin-top: 1px; margin-left: 0px; text-indent: 0px; text-align: center; padding-top: 3px;"> 
              </asp:Label> 
            </asp:TableCell>
            <asp:TableCell ID="CellLigne2M03" BackColor="#B0E0D7" BorderColor="#B0E0D7" BorderStyle="Notset"> 
              <asp:Label ID="Ligne2M03" runat="server" Height="16px" Width="43px"
                        BackColor="#B0E0D7" BorderColor="#CAEBE4" BorderStyle="Notset" Text=""
                        BorderWidth="1px" ForeColor="Black"
                        Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="11px" Font-Italic="false"
                        style="margin-top: 1px; margin-left: 0px; text-indent: 0px; text-align: center; padding-top: 3px;"> 
              </asp:Label>         
            </asp:TableCell>
            <asp:TableCell ID="CellLigne2M04" BackColor="#B0E0D7" BorderColor="#B0E0D7" BorderStyle="Notset"> 
              <asp:Label ID="Ligne2M04" runat="server" Height="16px" Width="43px"
                        BackColor="#B0E0D7" BorderColor="#CAEBE4" BorderStyle="Notset" Text=""
                        BorderWidth="1px" ForeColor="Black"
                        Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="11px" Font-Italic="false"
                        style="margin-top: 1px; margin-left: 0px; text-indent: 0px; text-align: center; padding-top: 3px;"> 
              </asp:Label>           
            </asp:TableCell>
            <asp:TableCell ID="CellLigne2M05" BackColor="#B0E0D7" BorderColor="#B0E0D7" BorderStyle="Notset"> 
              <asp:Label ID="Ligne2M05" runat="server" Height="16px" Width="43px"
                        BackColor="#B0E0D7" BorderColor="#CAEBE4" BorderStyle="Notset" Text=""
                        BorderWidth="1px" ForeColor="Black"
                        Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="11px" Font-Italic="false"
                        style="margin-top: 1px; margin-left: 0px; text-indent: 0px; text-align: center; padding-top: 3px;"> 
              </asp:Label>           
            </asp:TableCell>
            <asp:TableCell ID="CellLigne2M06" BackColor="#B0E0D7" BorderColor="#B0E0D7" BorderStyle="Notset"> 
              <asp:Label ID="Ligne2M06" runat="server" Height="16px" Width="43px"
                        BackColor="#B0E0D7" BorderColor="#CAEBE4" BorderStyle="Notset" Text=""
                        BorderWidth="1px" ForeColor="Black"
                        Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="11px" Font-Italic="false"
                        style="margin-top: 1px; margin-left: 0px; text-indent: 0px; text-align: center; padding-top: 3px;"> 
              </asp:Label>        
            </asp:TableCell>
            <asp:TableCell ID="CellLigne2M07" BackColor="#B0E0D7" BorderColor="#B0E0D7" BorderStyle="Notset"> 
              <asp:Label ID="Ligne2M07" runat="server" Height="16px" Width="43px"
                        BackColor="#B0E0D7" BorderColor="#CAEBE4" BorderStyle="Notset" Text=""
                        BorderWidth="1px" ForeColor="Black"
                        Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="11px" Font-Italic="false"
                        style="margin-top: 1px; margin-left: 0px; text-indent: 0px; text-align: center; padding-top: 3px;"> 
              </asp:Label>           
            </asp:TableCell>
            <asp:TableCell ID="CellLigne2M08" BackColor="#B0E0D7" BorderColor="#B0E0D7" BorderStyle="Notset"> 
              <asp:Label ID="Ligne2M08" runat="server" Height="16px" Width="43px"
                        BackColor="#B0E0D7" BorderColor="#CAEBE4" BorderStyle="Notset" Text=""
                        BorderWidth="1px" ForeColor="Black"
                        Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="11px" Font-Italic="false"
                        style="margin-top: 1px; margin-left: 0px; text-indent: 0px; text-align: center; padding-top: 3px;"> 
              </asp:Label>           
            </asp:TableCell>
            <asp:TableCell ID="CellLigne2M09" BackColor="#B0E0D7" BorderColor="#B0E0D7" BorderStyle="Notset"> 
              <asp:Label ID="Ligne2M09" runat="server" Height="16px" Width="43px"
                        BackColor="#B0E0D7" BorderColor="#CAEBE4" BorderStyle="Notset" Text=""
                        BorderWidth="1px" ForeColor="Black"
                        Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="11px" Font-Italic="false"
                        style="margin-top: 1px; margin-left: 0px; text-indent: 0px; text-align: center; padding-top: 3px;"> 
              </asp:Label>           
            </asp:TableCell>
            <asp:TableCell ID="CellLigne2M10" BackColor="#B0E0D7" BorderColor="#B0E0D7" BorderStyle="Notset"> 
              <asp:Label ID="Ligne2M10" runat="server" Height="16px" Width="43px"
                        BackColor="#B0E0D7" BorderColor="#CAEBE4" BorderStyle="Notset" Text=""
                        BorderWidth="1px" ForeColor="Black"
                        Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="11px" Font-Italic="false"
                        style="margin-top: 1px; margin-left: 0px; text-indent: 0px; text-align: center; padding-top: 3px;"> 
              </asp:Label>           
            </asp:TableCell>
            <asp:TableCell ID="CellLigne2M11" BackColor="#B0E0D7" BorderColor="#B0E0D7" BorderStyle="Notset"> 
              <asp:Label ID="Ligne2M11" runat="server" Height="16px" Width="43px"
                        BackColor="#B0E0D7" BorderColor="#CAEBE4" BorderStyle="Notset" Text=""
                        BorderWidth="1px" ForeColor="Black"
                        Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="11px" Font-Italic="false"
                        style="margin-top: 1px; margin-left: 0px; text-indent: 0px; text-align: center; padding-top: 3px;"> 
              </asp:Label>           
            </asp:TableCell>
            <asp:TableCell ID="CellLigne2M12" BackColor="#B0E0D7" BorderColor="#B0E0D7" BorderStyle="Notset"> 
              <asp:Label ID="Ligne2M12" runat="server" Height="16px" Width="43px"
                        BackColor="#B0E0D7" BorderColor="#CAEBE4" BorderStyle="Notset" Text=""
                        BorderWidth="1px" ForeColor="Black"
                        Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="11px" Font-Italic="false"
                        style="margin-top: 1px; margin-left: 0px; text-indent: 0px; text-align: center; padding-top: 3px;"> 
              </asp:Label>           
            </asp:TableCell>     
       </asp:TableRow>
  </asp:Table>
