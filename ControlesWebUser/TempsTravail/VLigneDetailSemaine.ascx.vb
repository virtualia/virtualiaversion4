﻿Option Strict On
Option Explicit On
Option Compare Text
Imports VI = Virtualia.Systeme.Constantes
Public Class Controles_VLigneDetailSemaine
    Inherits System.Web.UI.UserControl
    Private WsNomState As String = "VJour"
    Private WsCacheMaj As ArrayList

    Public Sub Initialiser()
        LabelJour.Text = ""
        LabelHorairesBadgesP1.Text = ""
        LabelHorairesBadgesP2.Text = ""
        LabelHorairesJourP1.Text = ""
        LabelHorairesJourP2.ToolTip = ""
        LabelDureeJourP1.Text = ""
        LabelDureeJourP2.Text = ""
        LabelDureeTotalJour.Text = ""
        LabelCommentaireJour.Text = ""
    End Sub

    Public WriteOnly Property VLibelleJour As String
        Set(ByVal value As String)
            LabelJour.Text = value
        End Set
    End Property

    Public WriteOnly Property VHorairesBadges(ByVal NoPlage As Integer) As String
        Set(ByVal value As String)
            Select Case NoPlage
                Case VI.NumeroPlage.Plage1_Presence
                    LabelHorairesBadgesP1.ForeColor = Drawing.Color.Black
                    LabelHorairesBadgesP1.Text = value
                    If value = "" Then
                        LabelHorairesBadgesP1.ForeColor = LabelHorairesBadgesP1.BackColor
                        LabelHorairesBadgesP1.Text = "..."
                    End If
                Case VI.NumeroPlage.Plage2_Presence
                    LabelHorairesBadgesP2.ForeColor = Drawing.Color.Black
                    LabelHorairesBadgesP2.Text = value
                    If value = "" Then
                        LabelHorairesBadgesP2.ForeColor = LabelHorairesBadgesP2.BackColor
                        LabelHorairesBadgesP2.Text = "..."
                    End If
            End Select
        End Set
    End Property

    Public WriteOnly Property VHorairesValides(ByVal NoPlage As Integer) As String
        Set(ByVal value As String)
            Select Case NoPlage
                Case VI.NumeroPlage.Plage1_Presence
                    LabelHorairesJourP1.ForeColor = Drawing.Color.Black
                    LabelHorairesJourP1.Text = value
                    If value = "" Then
                        LabelHorairesJourP1.ForeColor = LabelHorairesJourP1.BackColor
                        LabelHorairesJourP1.Text = "..."
                    End If
                Case VI.NumeroPlage.Plage2_Presence
                    LabelHorairesJourP2.ForeColor = Drawing.Color.Black
                    LabelHorairesJourP2.Text = value
                    If value = "" Then
                        LabelHorairesJourP2.ForeColor = LabelHorairesJourP2.BackColor
                        LabelHorairesJourP2.Text = "..."
                    End If
            End Select
        End Set
    End Property

    Public WriteOnly Property VDureesValides(ByVal NoPlage As Integer) As String
        Set(ByVal value As String)
            Select Case NoPlage
                Case VI.NumeroPlage.Plage1_Presence
                    LabelDureeJourP1.ForeColor = Drawing.Color.Black
                    LabelDureeJourP1.Text = value
                    If value = "" Then
                        LabelDureeJourP1.ForeColor = LabelDureeJourP1.BackColor
                        LabelDureeJourP1.Text = "..."
                    End If
                Case VI.NumeroPlage.Plage2_Presence
                    LabelDureeJourP2.ForeColor = Drawing.Color.Black
                    LabelDureeJourP2.Text = value
                    If value = "" Then
                        LabelDureeJourP2.ForeColor = LabelDureeJourP2.BackColor
                        LabelDureeJourP2.Text = "..."
                    End If
            End Select
        End Set
    End Property

    Public WriteOnly Property VTotalValide() As String
        Set(ByVal value As String)
            LabelDureeTotalJour.ForeColor = Drawing.Color.Black
            LabelDureeTotalJour.Text = value
            If value = "" Then
                LabelDureeTotalJour.ForeColor = LabelDureeTotalJour.BackColor
                LabelDureeTotalJour.Text = "..."
            End If
        End Set
    End Property

    Public WriteOnly Property VCommentaire() As String
        Set(ByVal value As String)
            LabelCommentaireJour.ForeColor = Drawing.Color.Black
            LabelCommentaireJour.Text = value
            If value = "" Then
                LabelCommentaireJour.ForeColor = LabelCommentaireJour.BackColor
                LabelCommentaireJour.Text = "..."
            End If
        End Set
    End Property

    Public WriteOnly Property VBackColorPlage(ByVal NoPlage As Integer) As System.Drawing.Color
        Set(ByVal value As System.Drawing.Color)
            Select Case NoPlage
                Case VI.NumeroPlage.Plage1_Presence
                    LabelHorairesBadgesP1.BackColor = value
                    LabelHorairesJourP1.BackColor = value
                    LabelDureeJourP1.BackColor = value
                Case VI.NumeroPlage.Plage2_Presence
                    LabelHorairesBadgesP2.BackColor = value
                    LabelHorairesJourP2.BackColor = value
                    LabelDureeJourP2.BackColor = value
            End Select
        End Set
    End Property
End Class