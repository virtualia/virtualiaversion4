﻿Option Strict On
Option Explicit On
Option Compare Text
Partial Class Controles_VLegendeCouleurs
    Inherits System.Web.UI.UserControl
    Private WebFct As Virtualia.Net.WebFonctions

    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        WebFct = New Virtualia.Net.WebFonctions(Me, 0)
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Call FaireLegendes()
    End Sub

    Private Sub FaireLegendes()
        Dim Ctl As Control
        Dim VirControle As System.Web.UI.WebControls.Label
        Dim IndiceI As Integer = 0
        Do
            Ctl = WebFct.VirWebControle(Me.VLegende, "EtiLegende", IndiceI)
            If Ctl Is Nothing Then
                Exit Do
            End If
            VirControle = CType(Ctl, System.Web.UI.WebControls.Label)
            If WebFct.PointeurUtilisateur.PointeurParametres.LibellePlanning(IndiceI, 0) <> "" Then
                VirControle.Text = WebFct.PointeurUtilisateur.PointeurParametres.LibellePlanning(IndiceI, 0)
                VirControle.Visible = True
            Else
                VirControle.Visible = False
            End If
            IndiceI += 1
        Loop
        IndiceI = 0
        Do
            Ctl = WebFct.VirWebControle(Me.VLegende, "EtiCouleur", IndiceI)
            If Ctl Is Nothing Then
                Exit Do
            End If
            VirControle = CType(Ctl, System.Web.UI.WebControls.Label)
            If WebFct.PointeurUtilisateur.PointeurParametres.LibellePlanning(IndiceI, 0) <> "" Then
                VirControle.BackColor = WebFct.PointeurUtilisateur.PointeurParametres.CouleurPlanning(IndiceI)
                VirControle.Visible = True
            Else
                VirControle.Visible = False
            End If
            IndiceI += 1
        Loop
    End Sub

End Class
