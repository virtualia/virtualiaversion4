﻿Option Strict On
Option Explicit On
Option Compare Text
Imports VI = Virtualia.Systeme.Constantes
Imports Microsoft.VisualBasic
Partial Class Controles_VCalendrier
    Inherits System.Web.UI.UserControl
    Public Delegate Sub Valeur_ChangeEventHandler(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.DonneeChangeEventArgs)
    Public Event ValeurChange As Valeur_ChangeEventHandler
    Private WebFct As Virtualia.Net.WebFonctions
    Private WsTypeCalend As String = "SD" 'SD=Standard Saisie date début, ID=Individuel date début
    'SF=Standard Saisie date fin, IF=Individuel date fin
    Private WsIde_Dossier As Integer = 0
    '
    Private WsNomStateIde As String = "Vcalendrier"
    Private WsCacheIde As ArrayList

    Protected Overridable Sub Valeur_Change(ByVal e As Virtualia.Systeme.Evenements.DonneeChangeEventArgs)
        RaiseEvent ValeurChange(Me, e)
    End Sub

    Public Property Identifiant() As Integer
        Get
            Return WsIde_Dossier
        End Get
        Set(ByVal value As Integer)
            If value = WsIde_Dossier Then
                Exit Property
            End If
            WsIde_Dossier = value
            If WsIde_Dossier = 0 Then
                Exit Property
            End If
            If Me.ViewState(WsNomStateIde) IsNot Nothing Then
                Me.ViewState.Remove(WsNomStateIde)
            End If
            '** MettreEnCache le dossier **
            WsCacheIde = New ArrayList
            WsCacheIde.Add(WsIde_Dossier)
            Me.ViewState.Add(WsNomStateIde, WsCacheIde)
        End Set
    End Property

    Public Property TypeCalendrier() As String
        Get
            Return WsTypeCalend
        End Get
        Set(ByVal value As String)
            WsTypeCalend = value
        End Set
    End Property

    Public Property SiEtiDateSelVisible() As Boolean
        Get
            Return CalDateSel00.Visible
        End Get
        Set(ByVal value As Boolean)
            CalDateSel00.Visible = value
            CalDateSel01.Visible = value
        End Set
    End Property

    Public Property MoisSelectionne() As String
        Get
            Return HSelMois.Value
        End Get
        Set(ByVal value As String)
            HSelMois.Value = value
        End Set
    End Property

    Public Property DateSelectionnee(ByVal Index As Integer) As String
        Get
            Select Case Index
                Case 0
                    Return CalDateSel00.Text
                Case 1
                    Return CalDateSel01.Text
                Case Else
                    Return ""
            End Select
        End Get
        Set(ByVal value As String)
            Select Case Index
                Case 0
                    CalDateSel00.Text = value
                Case 1
                    CalDateSel01.Text = value
            End Select
            HSelDate.Value = value
        End Set
    End Property

    Public WriteOnly Property SiJourVisible(ByVal NoJour As Integer, ByVal Prefixe As String) As Boolean
        Set(ByVal value As Boolean)
            Dim Ctl As Control
            Dim VirControle As System.Web.UI.WebControls.Button
            Ctl = WebFct.VirWebControle(Me, Prefixe, NoJour)
            If Ctl Is Nothing Then
                Exit Property
            End If
            VirControle = CType(Ctl, System.Web.UI.WebControls.Button)
            Select Case value
                Case True
                    VirControle.BackColor = Drawing.Color.LightGray
                    VirControle.BorderStyle = BorderStyle.Solid
                    Select Case WsTypeCalend
                        Case Is = "ID", "IF"
                            VJourStyle(NoJour, "CalPM") = "solid"
                    End Select
                Case False
                    VirControle.BackColor = Drawing.Color.Transparent
                    VirControle.BorderStyle = BorderStyle.None
                    VirControle.Text = ""
                    VJourStyle(NoJour, Prefixe) = "none"
            End Select
        End Set
    End Property

    Public WriteOnly Property SiCocheVisible() As Boolean
        Set(ByVal value As Boolean)
            CalCoche.V_Visible = value
        End Set
    End Property

    Public WriteOnly Property CocheText() As String
        Set(ByVal value As String)
            CalCoche.V_Text = value
        End Set
    End Property

    Public WriteOnly Property VText(ByVal NoJour As Integer, ByVal Prefixe As String) As String
        Set(ByVal value As String)
            Dim Ctl As Control
            Dim VirControle As System.Web.UI.WebControls.Button
            Ctl = WebFct.VirWebControle(Me, Prefixe, NoJour)
            If Ctl Is Nothing Then
                Exit Property
            End If
            VirControle = CType(Ctl, System.Web.UI.WebControls.Button)
            VirControle.Text = value
        End Set
    End Property

    Public WriteOnly Property VDemiJourToolTip(ByVal NoJour As Integer, ByVal Prefixe As String) As String
        Set(ByVal value As String)
            Dim Ctl As Control
            Dim VirControle As System.Web.UI.WebControls.Button
            Ctl = WebFct.VirWebControle(Me, Prefixe, NoJour)
            If Ctl Is Nothing Then
                Exit Property
            End If
            VirControle = CType(Ctl, System.Web.UI.WebControls.Button)
            VirControle.ToolTip = value
        End Set
    End Property

    Public WriteOnly Property VBorduresColor(ByVal Prefixe As String) As System.Drawing.Color
        Set(ByVal value As System.Drawing.Color)
            Dim IndiceI As Integer
            If WebFct Is Nothing Then
                WebFct = New Virtualia.Net.WebFonctions(Me, 0)
            End If
            For IndiceI = 0 To 36
                VBorderColor(IndiceI, Prefixe) = value
            Next IndiceI
        End Set
    End Property

    Public WriteOnly Property VBackColor(ByVal NoJour As Integer, ByVal Prefixe As String) As System.Drawing.Color
        Set(ByVal value As System.Drawing.Color)
            Dim Ctl As Control
            Dim VirControle As System.Web.UI.WebControls.Button
            Ctl = WebFct.VirWebControle(Me, Prefixe, NoJour)
            If Ctl Is Nothing Then
                Exit Property
            End If
            VirControle = CType(Ctl, System.Web.UI.WebControls.Button)
            VirControle.BackColor = value
        End Set
    End Property

    Public WriteOnly Property VBorderColor(ByVal NoJour As Integer, ByVal Prefixe As String) As System.Drawing.Color
        Set(ByVal value As System.Drawing.Color)
            Dim Ctl As Control
            Dim VirControle As System.Web.UI.WebControls.Button
            Ctl = WebFct.VirWebControle(Me, Prefixe, NoJour)
            If Ctl Is Nothing Then
                Exit Property
            End If
            VirControle = CType(Ctl, System.Web.UI.WebControls.Button)
            VirControle.BorderColor = value
        End Set
    End Property

    Public WriteOnly Property VJoursStyle() As String
        Set(ByVal value As String)
            Dim IndiceI As Integer
            If WebFct Is Nothing Then
                WebFct = New Virtualia.Net.WebFonctions(Me, 0)
            End If
            For IndiceI = 0 To 36
                VJourStyle(IndiceI, "CalPM") = value
            Next IndiceI
        End Set
    End Property

    Public WriteOnly Property VJourStyle(ByVal NoJour As Integer, ByVal Prefixe As String) As String
        Set(ByVal value As String)
            Dim Ctl As Control
            Dim VirControle As System.Web.UI.WebControls.Button
            Dim StyleBordure As String = "border-top-style"
            Dim NumBouton As Integer

            If Prefixe = "CalAM" Then
                StyleBordure = "border-bottom-style"
            End If

            Ctl = WebFct.VirWebControle(Me, Prefixe, NoJour)
            If Ctl Is Nothing Then
                Exit Property
            End If
            VirControle = CType(Ctl, System.Web.UI.WebControls.Button)
            NumBouton = CInt(Strings.Right(VirControle.ID, 2))
            If NumBouton = NoJour Then
                VirControle.Style.Remove(StyleBordure)
                VirControle.Style.Add(StyleBordure, value)
            End If
        End Set
    End Property

    Public WriteOnly Property VFontSize(ByVal NoJour As Integer, ByVal Prefixe As String) As System.Web.UI.WebControls.FontUnit
        Set(ByVal value As System.Web.UI.WebControls.FontUnit)
            Dim Ctl As Control
            Dim VirControle As System.Web.UI.WebControls.Button
            Ctl = WebFct.VirWebControle(Me, Prefixe, NoJour)
            If Ctl Is Nothing Then
                Exit Property
            End If
            VirControle = CType(Ctl, System.Web.UI.WebControls.Button)
            VirControle.Font.Size = value
        End Set
    End Property

    Public WriteOnly Property VFontBold(ByVal NoJour As Integer, ByVal Prefixe As String) As Boolean
        Set(ByVal value As Boolean)
            Dim Ctl As Control
            Dim VirControle As System.Web.UI.WebControls.Button
            Ctl = WebFct.VirWebControle(Me, Prefixe, NoJour)
            If Ctl Is Nothing Then
                Exit Property
            End If
            VirControle = CType(Ctl, System.Web.UI.WebControls.Button)
            VirControle.Font.Bold = value
        End Set
    End Property

    Private Sub FaireCalendrier()
        Dim TableauW(0) As String
        Dim Mois As Integer
        Dim Annee As Integer
        Dim DateW As String
        Dim DateFin As String
        Dim NoJour As Integer
        'Dim Predicat As Virtualia.Net.Predicats.PredicateFiche

        If Me.ViewState(WsNomStateIde) IsNot Nothing Then
            WsCacheIde = CType(Me.ViewState(WsNomStateIde), ArrayList)
            WsIde_Dossier = CInt(WsCacheIde(0))
        End If

        If HSelMois.Value = "" Then
            Annee = CInt(Strings.Right(WebFct.ViRhDates.DateduJour(False), 4))
            Mois = CInt(Strings.Mid(WebFct.ViRhDates.DateduJour(False), 4, 2))
        Else
            TableauW = Strings.Split(HSelMois.Value, Strings.Space(1), -1)
            Annee = CInt(TableauW(1))
            Mois = WebFct.ViRhDates.IndexduMois(TableauW(0))
        End If

        Dim IndiceA As Integer = Annee
        Dim IndiceJ As Integer
        Dim IndiceCtl As Integer

        DateW = "01/" & Strings.Format(Mois, "00") & "/" & IndiceA.ToString
        DateFin = WebFct.ViRhDates.DateSaisieVerifiee("31" & "/" & Strings.Format(Mois, "00") & "/" & IndiceA.ToString)

        If WsIde_Dossier > 0 Then
            'Predicat = New Virtualia.Net.Predicats.PredicateFiche(VI.ObjetPer.ObaAbsence, DateW, DateFin)
            'WsListeAbs = New List(Of Virtualia.Systeme.MetaModele.VIR_FICHE)
            'WsListeAbs = WebFct.PointeurDossier(WsIde_Dossier).ListeDesFiches.FindAll(AddressOf Predicat.SiFicheEvtDansPeriode)
        End If

        NoJour = Weekday(DateValue(DateW), FirstDayOfWeek.Monday) - 1
        For IndiceCtl = 0 To NoJour - 1
            SiJourVisible(IndiceCtl, "CalAM") = False
            SiJourVisible(IndiceCtl, "CalPM") = False
        Next IndiceCtl
        IndiceJ = NoJour

        Do 'Traitement des jours du mois
            '** Calendrier Standard
            SiJourVisible(IndiceJ, "CalAM") = True
            SiJourVisible(IndiceJ, "CalPM") = True
            VBorderColor(IndiceJ, "CalAM") = WebFct.ConvertCouleur("#216B68")
            VBorderColor(IndiceJ, "CalPM") = WebFct.ConvertCouleur("#216B68")
            VText(IndiceJ, "CalAM") = CShort(Strings.Left(DateW, 2)).ToString
            VDemiJourToolTip(IndiceJ, "CalAM") = WebFct.ViRhDates.ClairDate(DateW, True)
            VDemiJourToolTip(IndiceJ, "CalPM") = WebFct.ViRhDates.ClairDate(DateW, True)
            If WebFct.ViRhDates.SiJourOuvre(DateW, True) = True Then
                VBackColor(IndiceJ, "CalAM") = WebFct.PointeurUtilisateur.PointeurParametres.CouleurPlanning(0)
                VBackColor(IndiceJ, "CalPM") = WebFct.PointeurUtilisateur.PointeurParametres.CouleurPlanning(0)
            Else
                VBackColor(IndiceJ, "CalAM") = WebFct.PointeurUtilisateur.PointeurParametres.CouleurPlanning(1)
                VBackColor(IndiceJ, "CalPM") = WebFct.PointeurUtilisateur.PointeurParametres.CouleurPlanning(1)
                VJourStyle(IndiceJ, "CalPM") = "none"
                If WebFct.ViRhDates.SiJourFerie(DateW) = True Then
                    VBorderColor(IndiceJ, "CalAM") = Drawing.Color.Red
                    VBorderColor(IndiceJ, "CalPM") = Drawing.Color.Red
                End If
            End If
            If DateW = HSelDate.Value Then
                VBackColor(IndiceJ, "CalAM") = WebFct.PointeurUtilisateur.PointeurParametres.CouleurPlanning(4)
                VBackColor(IndiceJ, "CalPM") = WebFct.PointeurUtilisateur.PointeurParametres.CouleurPlanning(4)
            End If
            '** Inclure les absences
            Select Case TypeCalendrier
                Case Is = "I"
                    Dim FicheAbs As Virtualia.TablesObjet.ShemaPER.PER_ABSENCE = FicheAbsence(DateW)
                    Dim Couleur As Drawing.Color
                    If FicheAbs IsNot Nothing Then
                        Couleur = WebFct.PointeurUtilisateur.PointeurParametres.CouleurPlanning(WebFct.PointeurUtilisateur.PointeurParametres.IndexCouleurPlanning(FicheAbs.Nature))
                        Select Case CInt(FicheAbs.VTypeJour(DateW))
                            Case VI.NumeroPlage.Jour_Absence
                                VJourStyle(IndiceJ, "CalAM") = "none"
                                VBackColor(IndiceJ, "CalAM") = Couleur
                                VDemiJourToolTip(IndiceJ, "CalAM") = FicheAbs.Nature
                                VBackColor(IndiceJ, "CalPM") = Couleur
                                VDemiJourToolTip(IndiceJ, "CalPM") = FicheAbs.Nature
                            Case VI.NumeroPlage.Plage1_Absence
                                VJourStyle(IndiceJ, "CalAM") = "solid"
                                VBackColor(IndiceJ, "CalAM") = Couleur
                                VDemiJourToolTip(IndiceJ, "CalAM") = FicheAbs.Nature
                            Case VI.NumeroPlage.Plage2_Absence
                                VJourStyle(IndiceJ, "CalAM") = "solid"
                                VBackColor(IndiceJ, "CalPM") = Couleur
                                VDemiJourToolTip(IndiceJ, "CalPM") = FicheAbs.Nature
                        End Select
                    End If
            End Select

            DateW = WebFct.ViRhDates.CalcDateMoinsJour(DateW, "1", "0")
            If CInt(Strings.Mid(DateW, 4, 2)) <> Mois Then
                For IndiceCtl = IndiceJ + 1 To 36
                    SiJourVisible(IndiceCtl, "CalAM") = False
                    SiJourVisible(IndiceCtl, "CalPM") = False
                Next IndiceCtl
                Exit Do
            End If
            NoJour = Weekday(DateValue(DateW), FirstDayOfWeek.Monday) - 1
            IndiceJ += 1
        Loop

    End Sub

    Private ReadOnly Property FicheAbsence(ByVal DateValeur As String) As Virtualia.TablesObjet.ShemaPER.PER_ABSENCE
        Get
            'If WsListeAbs Is Nothing Then
            'Return Nothing
            'End If
            'Dim I As Integer
            'For I = 0 To WsListeAbs.Count - 1
            'Select Case WebFct.ViRhDates.ComparerDates(DateValeur, WsListeAbs.Item(I).Date_de_Valeur)
            '    Case VI.ComparaisonDates.Egalite, VI.ComparaisonDates.PlusGrand
            'Select Case WebFct.ViRhDates.ComparerDates(DateValeur, WsListeAbs.Item(I).Date_de_Fin)
            '    Case VI.ComparaisonDates.Egalite, VI.ComparaisonDates.PlusPetit
            'Return CType(WsListeAbs.Item(I), Virtualia.TablesObjet.ShemaPER.PER_ABSENCE)
            'Exit Property
            'End Select
            'End Select
            'Next I
            Return Nothing
        End Get
    End Property

    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        WebFct = New Virtualia.Net.WebFonctions(Me, 0)
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Call FaireListeMois()
        If Me.ViewState(WsNomStateIde) IsNot Nothing Then
            WsCacheIde = CType(Me.ViewState(WsNomStateIde), ArrayList)
            WsIde_Dossier = CInt(WsCacheIde(0))
        End If
        Call FaireCalendrier()
    End Sub

    Private Sub FaireListeMois()
        Dim Chaine As New System.Text.StringBuilder
        Dim Libel As String = "Aucun mois" & VI.Tild & "Un mois" & VI.Tild & "possibilités"
        Dim Annee As Integer
        Dim Mois As Integer
        Dim Limite As Integer
        Dim IndiceI As Integer
        Dim IndiceA As Integer
        Annee = CInt(Strings.Right(WebFct.ViRhDates.DateduJour(False), 4))
        Mois = CInt(Strings.Mid(WebFct.ViRhDates.DateduJour(False), 4, 2))

        Select Case WsTypeCalend
            Case Is = "IF", "SF"
                If HSelDate.Value <> "" Then
                    Annee = CInt(Strings.Right(HSelDate.Value, 4))
                    Mois = CInt(Strings.Mid(HSelDate.Value, 4, 2))
                End If
                IndiceA = Annee
                Limite = 12
                Do
                    For IndiceI = Mois To Limite
                        Chaine.Append(WebFct.ViRhDates.MoisEnClair(CShort(IndiceI)) & Strings.Space(1) & IndiceA.ToString & VI.Tild)
                    Next IndiceI
                    IndiceA += 1
                    If IndiceA - Annee > 1 Then
                        Exit Do
                    End If
                    Limite -= Mois + 1
                    Mois = 1
                Loop
            Case Is = "ID"
                IndiceA = Annee
                Limite = 12
                Do
                    For IndiceI = Mois To Limite
                        Chaine.Append(WebFct.ViRhDates.MoisEnClair(CShort(IndiceI)) & Strings.Space(1) & IndiceA.ToString & VI.Tild)
                    Next IndiceI
                    IndiceA += 1
                    If IndiceA - Annee > 1 Then
                        Exit Do
                    End If
                    Limite -= Mois + 1
                    Mois = 1
                Loop
            Case Is = "SD"
                IndiceA = Annee + 1
                Do
                    For IndiceI = 12 To 1 Step -1
                        Chaine.Append(WebFct.ViRhDates.MoisEnClair(CShort(IndiceI)) & Strings.Space(1) & IndiceA.ToString & VI.Tild)
                    Next IndiceI
                    IndiceA -= 1
                    If IndiceA < Annee - 3 Then
                        Exit Do
                    End If
                Loop
        End Select
        CalLstMois.V_Liste(Libel) = Chaine.ToString
        Chaine.Clear()

        If HSelMois.Value = "" Then
            HSelMois.Value = WebFct.ViRhDates.MoisEnClair(CShort(Strings.Mid(WebFct.ViRhDates.DateduJour(False), 4, 2))) & Strings.Space(1) & Annee.ToString
        End If
        CalLstMois.LstText = HSelMois.Value
    End Sub

    Protected Sub CalLstMois_ValeurChange(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.DonneeChangeEventArgs) Handles CalLstMois.ValeurChange
        HSelMois.Value = e.Valeur
        Dim Evenement As Virtualia.Systeme.Evenements.DonneeChangeEventArgs
        Evenement = New Virtualia.Systeme.Evenements.DonneeChangeEventArgs("Mois", e.Valeur)
        Valeur_Change(Evenement)
    End Sub

    Protected Sub CalAM_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles CalAM00.Click, CalAM01.Click, _
    CalAM02.Click, CalAM03.Click, CalAM04.Click, CalAM05.Click, CalAM06.Click, CalAM07.Click, CalAM08.Click, CalAM09.Click, _
    CalAM10.Click, CalAM11.Click, CalAM12.Click, CalAM13.Click, CalAM14.Click, CalAM15.Click, CalAM08.Click, CalAM16.Click, _
    CalAM17.Click, CalAM18.Click, CalAM19.Click, CalAM20.Click, CalAM21.Click, CalAM22.Click, CalAM23.Click, CalAM24.Click, _
    CalAM25.Click, CalAM26.Click, CalAM27.Click, CalAM28.Click, CalAM29.Click, CalAM30.Click, CalAM31.Click, CalAM32.Click, _
    CalAM33.Click, CalAM34.Click, CalAM35.Click, CalAM36.Click

        If HSelMois.Value = "" Then
            Exit Sub
        End If
        Dim TableauW(0) As String
        Dim Mois As Integer
        Dim Annee As Integer
        Dim Jour As Integer
        Dim DateW As String

        TableauW = Strings.Split(HSelMois.Value, Strings.Space(1), -1)
        Annee = CInt(TableauW(1))
        Mois = WebFct.ViRhDates.IndexduMois(TableauW(0))
        Jour = CInt(CType(sender, System.Web.UI.WebControls.Button).Text)
        DateW = Strings.Format(Jour, "00") & VI.Slash & Strings.Format(Mois, "00") & VI.Slash & Annee.ToString

        Select Case WsTypeCalend
            Case Is = "IF", "SF"
                If HSelDate.Value <> "" Then
                    Select Case WebFct.ViRhDates.ComparerDates(DateW, HSelDate.Value)
                        Case VI.ComparaisonDates.PlusPetit
                            Exit Sub
                    End Select
                End If
        End Select

        HSelDate.Value = DateW
        CalDateSel01.Text = ""
        CalDateSel00.Text = DateW

        Dim Evenement As Virtualia.Systeme.Evenements.DonneeChangeEventArgs
        Evenement = New Virtualia.Systeme.Evenements.DonneeChangeEventArgs("Jour", DateW)
        Valeur_Change(Evenement)
    End Sub

    Protected Sub CalPM_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles CalPM00.Click, CalPM01.Click, _
    CalPM02.Click, CalPM03.Click, CalPM04.Click, CalPM05.Click, CalPM06.Click, CalPM07.Click, CalPM08.Click, CalPM09.Click, _
    CalPM10.Click, CalPM11.Click, CalPM12.Click, CalPM13.Click, CalPM14.Click, CalPM15.Click, CalPM08.Click, CalPM16.Click, _
    CalPM17.Click, CalPM18.Click, CalPM19.Click, CalPM20.Click, CalPM21.Click, CalPM22.Click, CalPM23.Click, CalPM24.Click, _
    CalPM25.Click, CalPM26.Click, CalPM27.Click, CalPM28.Click, CalPM29.Click, CalPM30.Click, CalPM31.Click, CalPM32.Click, _
    CalPM33.Click, CalPM34.Click, CalPM35.Click, CalPM36.Click

        If HSelMois.Value Is Nothing Then
            Exit Sub
        End If
        Dim TableauW(0) As String
        Dim Mois As Integer
        Dim Annee As Integer
        Dim Jour As Integer
        Dim DateW As String
        Dim Ctl As Control
        Dim VirControle As System.Web.UI.WebControls.Button
        Jour = CInt(Strings.Right(CType(sender, System.Web.UI.WebControls.Button).ID, 2))
        Ctl = WebFct.VirWebControle(Me, "CalAM", Jour)
        If Ctl Is Nothing Then
            Exit Sub
        End If
        VirControle = CType(Ctl, System.Web.UI.WebControls.Button)
        Jour = CInt(VirControle.Text)

        TableauW = Strings.Split(HSelMois.Value, Strings.Space(1), -1)
        Annee = CInt(TableauW(1))
        Mois = WebFct.ViRhDates.IndexduMois(TableauW(0))
        DateW = Strings.Format(Jour, "00") & VI.Slash & Strings.Format(Mois, "00") & VI.Slash & Annee.ToString

        Select Case WsTypeCalend
            Case Is = "IF", "SF"
                If HSelDate.Value <> "" Then
                    Select Case WebFct.ViRhDates.ComparerDates(DateW, HSelDate.Value)
                        Case VI.ComparaisonDates.PlusPetit
                            Exit Sub
                    End Select
                End If
        End Select

        HSelDate.Value = DateW
        CalDateSel00.Text = ""
        CalDateSel01.Text = DateW

        Dim Evenement As Virtualia.Systeme.Evenements.DonneeChangeEventArgs
        Evenement = New Virtualia.Systeme.Evenements.DonneeChangeEventArgs("Jour", DateW)
        Valeur_Change(Evenement)
    End Sub

    Protected Sub CalCoche_ValeurChange(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.DonneeChangeEventArgs) Handles CalCoche.ValeurChange
        HSelCoche.Value = e.Valeur
        Dim Evenement As Virtualia.Systeme.Evenements.DonneeChangeEventArgs
        Evenement = New Virtualia.Systeme.Evenements.DonneeChangeEventArgs("Coche", e.Valeur)
        Valeur_Change(Evenement)
    End Sub
End Class
