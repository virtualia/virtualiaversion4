﻿<%@ Control Language="VB" AutoEventWireup="false" Inherits="Virtualia.Net.Controles_VCycledeBase" Codebehind="VCycledeBase.ascx.vb" %>

<asp:Table ID="VPlanningCycle" runat="server" Width="148px" CellPadding="0" CellSpacing="0" HorizontalAlign="Left">
    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="VJour00" runat="server" Height="36px" Width="20px" CellPadding="0" CellSpacing="0" 
                        BorderStyle="None" BorderColor="#216B68" style="z-index:99" >
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Label ID="JAM00" runat="server" Height="18px" Width="18px" BackColor="LightGray"
                                ForeColor="#064F4D" BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px"
                                Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                                style="margin-top: 1px; text-align: center; vertical-align: bottom;
                                border-bottom-style: none" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Label ID="JPM00" runat="server" Height="18px" Width="18px" BackColor="LightGray" 
                                ForeColor="#064F4D" BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px"
                                Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                                style="margin-bottom: 1px; text-align: center; vertical-align: top;
                                border-top-style: none" />
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
        <asp:TableCell>
            <asp:Table ID="VJour01" runat="server" Height="36px" Width="20px" CellPadding="0" CellSpacing="0" 
                        BorderStyle="None" BorderColor="#216B68" style="z-index:99" >
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Label ID="JAM01" runat="server" Height="18px" Width="18px" BackColor="LightGray" 
                                ForeColor="#064F4D" BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px"
                                Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                                style="margin-top: 1px; text-align: center; vertical-align: bottom; 
                                border-bottom-style: none" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Label ID="JPM01" runat="server" Height="18px" Width="18px" BackColor="LightGray" 
                                ForeColor="#064F4D" BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px"
                                Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                                style="margin-bottom: 1px; text-align: center; vertical-align: top; 
                                border-top-style: none" />
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table> 
        </asp:TableCell>
        <asp:TableCell>
            <asp:Table ID="VJour02" runat="server" Height="36px" Width="20px" CellPadding="0" CellSpacing="0"
                        BorderStyle="None" BorderColor="#216B68" style="z-index:99" >
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Label ID="JAM02" runat="server" Height="18px" Width="18px" BackColor="LightGray" 
                                ForeColor="#064F4D" BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px"
                                Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                                style="margin-top: 1px; text-align: center; vertical-align: bottom;
                                border-bottom-style: none" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Label ID="JPM02" runat="server" Height="18px" Width="18px" BackColor="LightGray" 
                                ForeColor="#064F4D" BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px"
                                Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                                style="margin-bottom: 1px; text-align: center; vertical-align: top; 
                                border-top-style: none" />
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
        <asp:TableCell>
            <asp:Table ID="VJour03" runat="server" Height="36px"  Width="20px" CellPadding="0" CellSpacing="0"
                        BorderStyle="None" BorderColor="#216B68" style="z-index:99" >
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Label ID="JAM03" runat="server" Height="18px" Width="18px" BackColor="LightGray" 
                                ForeColor="#064F4D" BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px"
                                Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                                style="margin-top: 1px; text-align: center; vertical-align: bottom;
                                border-bottom-style: none" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Label ID="JPM03" runat="server" Height="18px" Width="18px" BackColor="LightGray" 
                                ForeColor="#064F4D" BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px"
                                Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                                style="margin-bottom: 1px; text-align: center; vertical-align: top; 
                                border-top-style: none" />
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
        <asp:TableCell>
            <asp:Table ID="VJour04" runat="server" Height="36px"  Width="20px" CellPadding="0" CellSpacing="0"
                        BorderStyle="None" BorderColor="#216B68" style="z-index:99" >
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Label ID="JAM04" runat="server" Height="18px" Width="18px" BackColor="LightGray" 
                                ForeColor="#064F4D" BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px"
                                Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                                style="margin-top: 1px; text-align: center; vertical-align: bottom;
                                border-bottom-style: none" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Label ID="JPM04" runat="server" Height="18px" Width="18px" BackColor="LightGray" 
                                ForeColor="#064F4D" BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px"
                                Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                                style="margin-bottom: 1px; text-align: center; vertical-align: top; 
                                border-top-style: none" />
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
        <asp:TableCell>
            <asp:Table ID="VJour05" runat="server" Height="36px"  Width="20px" CellPadding="0" CellSpacing="0"
                        BorderStyle="None" BorderColor="#216B68" style="z-index:99" >
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Label ID="JAM05" runat="server" Height="18px" Width="18px" BackColor="LightGray" 
                                ForeColor="#064F4D" BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px"
                                Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                                style="margin-top: 1px; text-align: center; vertical-align: bottom;
                                border-bottom-style: none" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Label ID="JPM05" runat="server" Height="18px" Width="18px" BackColor="LightGray" 
                                ForeColor="#064F4D" BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px"
                                Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                                style="margin-bottom: 1px; text-align: center; vertical-align: top; 
                                border-top-style: none" />
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
        <asp:TableCell>
            <asp:Table ID="VJour06" runat="server" Height="36px"  Width="20px" CellPadding="0" CellSpacing="0"
                        BorderStyle="None" BorderColor="#216B68" style="z-index:99" >
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Label ID="JAM06" runat="server" Height="18px" Width="18px" BackColor="LightGray" 
                                ForeColor="#064F4D" BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px"
                                Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                                style="margin-top: 1px; text-align: center; vertical-align: bottom; 
                                border-bottom-style: none" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Label ID="JPM06" runat="server" Height="18px" Width="18px" BackColor="LightGray" 
                                ForeColor="#064F4D" BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px"
                                Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                                style="margin-bottom: 1px; text-align: center; vertical-align: top; 
                                border-top-style: none" />
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell ColumnSpan="7" HorizontalAlign="Right">
            <asp:Label ID="VIntitule" runat="server" Height="35px" BackColor="#A8BBB8" ForeColor="#1C5151"
                    BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px" Text="Intitulé du cycle" Visible="true"
                    Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                    Width="148px" style="text-indent: 1px; text-align: center;" />
        </asp:TableCell>
    </asp:TableRow>
</asp:Table>