﻿'------------------------------------------------------------------------------
' <généré automatiquement>
'     Ce code a été généré par un outil.
'
'     Les modifications apportées à ce fichier peuvent provoquer un comportement incorrect et seront perdues si
'     le code est régénéré.
' </généré automatiquement>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class Controles_VDetailMensuel

    '''<summary>
    '''Contrôle CadreInfo.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CadreInfo As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle CadreTitre.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CadreTitre As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle Etiquette.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Etiquette As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle TableauPlanningDetaille.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents TableauPlanningDetaille As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle LstLibelMois.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LstLibelMois As Global.Virtualia.Net.Controles_VListeCombo

    '''<summary>
    '''Contrôle LigneEntete.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LigneEntete As Global.System.Web.UI.WebControls.TableRow

    '''<summary>
    '''Contrôle CellTitreEntete.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellTitreEntete As Global.System.Web.UI.WebControls.TableCell

    '''<summary>
    '''Contrôle LabelTitreEntete.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelTitreEntete As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle CellMatin.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellMatin As Global.System.Web.UI.WebControls.TableCell

    '''<summary>
    '''Contrôle LabelMatin.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelMatin As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle CellMatinHDebut.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellMatinHDebut As Global.System.Web.UI.WebControls.TableCell

    '''<summary>
    '''Contrôle LabelMatinHDebut.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelMatinHDebut As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle CellMatinHFin.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellMatinHFin As Global.System.Web.UI.WebControls.TableCell

    '''<summary>
    '''Contrôle LabelMatinHFin.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelMatinHFin As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle CellApresmidi.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellApresmidi As Global.System.Web.UI.WebControls.TableCell

    '''<summary>
    '''Contrôle LabelApresmidi.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelApresmidi As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle CellApresmidiHDebut.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellApresmidiHDebut As Global.System.Web.UI.WebControls.TableCell

    '''<summary>
    '''Contrôle LabelApresmidiHDebut.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelApresmidiHDebut As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle CellApresmidiHFin.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellApresmidiHFin As Global.System.Web.UI.WebControls.TableCell

    '''<summary>
    '''Contrôle LabelApresmidiHFin.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelApresmidiHFin As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle CellTotalJour.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellTotalJour As Global.System.Web.UI.WebControls.TableCell

    '''<summary>
    '''Contrôle LabelTotalJour.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelTotalJour As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle CellTotSem.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellTotSem As Global.System.Web.UI.WebControls.TableCell

    '''<summary>
    '''Contrôle LabelTotSem.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelTotSem As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle Jour00.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Jour00 As Global.Virtualia.Net.Controles_VLigneDetailMensuel

    '''<summary>
    '''Contrôle Jour01.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Jour01 As Global.Virtualia.Net.Controles_VLigneDetailMensuel

    '''<summary>
    '''Contrôle Jour02.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Jour02 As Global.Virtualia.Net.Controles_VLigneDetailMensuel

    '''<summary>
    '''Contrôle Jour03.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Jour03 As Global.Virtualia.Net.Controles_VLigneDetailMensuel

    '''<summary>
    '''Contrôle Jour04.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Jour04 As Global.Virtualia.Net.Controles_VLigneDetailMensuel

    '''<summary>
    '''Contrôle Jour05.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Jour05 As Global.Virtualia.Net.Controles_VLigneDetailMensuel

    '''<summary>
    '''Contrôle Jour06.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Jour06 As Global.Virtualia.Net.Controles_VLigneDetailMensuel

    '''<summary>
    '''Contrôle Jour07.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Jour07 As Global.Virtualia.Net.Controles_VLigneDetailMensuel

    '''<summary>
    '''Contrôle Jour08.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Jour08 As Global.Virtualia.Net.Controles_VLigneDetailMensuel

    '''<summary>
    '''Contrôle Jour09.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Jour09 As Global.Virtualia.Net.Controles_VLigneDetailMensuel

    '''<summary>
    '''Contrôle Jour10.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Jour10 As Global.Virtualia.Net.Controles_VLigneDetailMensuel

    '''<summary>
    '''Contrôle Jour11.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Jour11 As Global.Virtualia.Net.Controles_VLigneDetailMensuel

    '''<summary>
    '''Contrôle Jour12.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Jour12 As Global.Virtualia.Net.Controles_VLigneDetailMensuel

    '''<summary>
    '''Contrôle Jour13.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Jour13 As Global.Virtualia.Net.Controles_VLigneDetailMensuel

    '''<summary>
    '''Contrôle Jour14.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Jour14 As Global.Virtualia.Net.Controles_VLigneDetailMensuel

    '''<summary>
    '''Contrôle Jour15.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Jour15 As Global.Virtualia.Net.Controles_VLigneDetailMensuel

    '''<summary>
    '''Contrôle Jour16.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Jour16 As Global.Virtualia.Net.Controles_VLigneDetailMensuel

    '''<summary>
    '''Contrôle Jour17.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Jour17 As Global.Virtualia.Net.Controles_VLigneDetailMensuel

    '''<summary>
    '''Contrôle Jour18.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Jour18 As Global.Virtualia.Net.Controles_VLigneDetailMensuel

    '''<summary>
    '''Contrôle Jour19.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Jour19 As Global.Virtualia.Net.Controles_VLigneDetailMensuel

    '''<summary>
    '''Contrôle Jour20.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Jour20 As Global.Virtualia.Net.Controles_VLigneDetailMensuel

    '''<summary>
    '''Contrôle Jour21.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Jour21 As Global.Virtualia.Net.Controles_VLigneDetailMensuel

    '''<summary>
    '''Contrôle Jour22.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Jour22 As Global.Virtualia.Net.Controles_VLigneDetailMensuel

    '''<summary>
    '''Contrôle Jour23.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Jour23 As Global.Virtualia.Net.Controles_VLigneDetailMensuel

    '''<summary>
    '''Contrôle Jour24.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Jour24 As Global.Virtualia.Net.Controles_VLigneDetailMensuel

    '''<summary>
    '''Contrôle Jour25.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Jour25 As Global.Virtualia.Net.Controles_VLigneDetailMensuel

    '''<summary>
    '''Contrôle Jour26.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Jour26 As Global.Virtualia.Net.Controles_VLigneDetailMensuel

    '''<summary>
    '''Contrôle Jour27.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Jour27 As Global.Virtualia.Net.Controles_VLigneDetailMensuel

    '''<summary>
    '''Contrôle Jour28.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Jour28 As Global.Virtualia.Net.Controles_VLigneDetailMensuel

    '''<summary>
    '''Contrôle Jour29.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Jour29 As Global.Virtualia.Net.Controles_VLigneDetailMensuel

    '''<summary>
    '''Contrôle Jour30.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Jour30 As Global.Virtualia.Net.Controles_VLigneDetailMensuel
End Class
