﻿'------------------------------------------------------------------------------
' <généré automatiquement>
'     Ce code a été généré par un outil.
'
'     Les modifications apportées à ce fichier peuvent provoquer un comportement incorrect et seront perdues si
'     le code est régénéré.
' </généré automatiquement>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class Controles_VSaisieHeuresJour

    '''<summary>
    '''Contrôle SaisieJour.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents SaisieJour As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle EtiLibelJour.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiLibelJour As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle HorairesJour.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents HorairesJour As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle SaisieMatin.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents SaisieMatin As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle TxtMatinA.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents TxtMatinA As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''Contrôle EtiLabelMatin.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiLabelMatin As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle TxtMatinD.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents TxtMatinD As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''Contrôle TxtSoirA.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents TxtSoirA As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''Contrôle EtiLabelSoir.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiLabelSoir As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle TxtSoirD.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents TxtSoirD As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''Contrôle CommandeAide.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CommandeAide As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Contrôle TableTotal.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents TableTotal As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle CadreTotal.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CadreTotal As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle TotalMatin.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents TotalMatin As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle TotalSoir.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents TotalSoir As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle TableTotalJour.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents TableTotalJour As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle TotalJour.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents TotalJour As Global.System.Web.UI.WebControls.Label
End Class
