﻿'------------------------------------------------------------------------------
' <généré automatiquement>
'     Ce code a été généré par un outil.
'
'     Les modifications apportées à ce fichier peuvent provoquer un comportement incorrect et seront perdues si
'     le code est régénéré.
' </généré automatiquement>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class Controles_VLigneDetailSemaine

    '''<summary>
    '''Contrôle LigneHorairesSemaine.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LigneHorairesSemaine As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle LigneJour.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LigneJour As Global.System.Web.UI.WebControls.TableRow

    '''<summary>
    '''Contrôle CellTitreJour.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellTitreJour As Global.System.Web.UI.WebControls.TableCell

    '''<summary>
    '''Contrôle TableJour.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents TableJour As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle LabelJour.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelJour As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle CellHorairesBadges.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellHorairesBadges As Global.System.Web.UI.WebControls.TableCell

    '''<summary>
    '''Contrôle TableHorairesBadges.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents TableHorairesBadges As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle LabelHorairesBadgesP1.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelHorairesBadgesP1 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle LabelHorairesBadgesP2.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelHorairesBadgesP2 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle CellHorairesJour.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellHorairesJour As Global.System.Web.UI.WebControls.TableCell

    '''<summary>
    '''Contrôle TableHorairesJour.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents TableHorairesJour As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle LabelHorairesJourP1.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelHorairesJourP1 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle LabelHorairesJourP2.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelHorairesJourP2 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle CellDureeJour.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellDureeJour As Global.System.Web.UI.WebControls.TableCell

    '''<summary>
    '''Contrôle TableDureeJour.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents TableDureeJour As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle LabelDureeJourP1.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelDureeJourP1 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle LabelDureeJourP2.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelDureeJourP2 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle CellDureeTotalJour.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellDureeTotalJour As Global.System.Web.UI.WebControls.TableCell

    '''<summary>
    '''Contrôle TableDureeTotalJour.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents TableDureeTotalJour As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle LabelDureeTotalJour.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelDureeTotalJour As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle CellCommentaireJour.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellCommentaireJour As Global.System.Web.UI.WebControls.TableCell

    '''<summary>
    '''Contrôle TableCommentaireJour.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents TableCommentaireJour As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle LabelCommentaireJour.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelCommentaireJour As Global.System.Web.UI.WebControls.Label
End Class
