﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="VLigneDetailSemaine.ascx.vb" Inherits="Virtualia.Net.Controles_VLigneDetailSemaine" %>

 <asp:Table ID="LigneHorairesSemaine" runat="server" CellPadding="0" CellSpacing="0" Width="600px"
        BorderStyle="Notset" BorderWidth="1px" BorderColor="#1C5151" HorizontalAlign="Center" Visible="true">
          
          <asp:TableRow>
            <asp:TableCell Height="2px" ColumnSpan="6"></asp:TableCell>
          </asp:TableRow> 
                    
          <asp:TableRow ID="LigneJour" Width="600px">
            <asp:TableCell ID="CellTitreJour" BackColor="#f0f0f0" BorderColor="#A8BBB8" 
            BorderStyle="None" HorizontalAlign="Center">
               <asp:Table ID="TableJour" runat="server" CellPadding="0" Width="100px" Visible="true"  
               CellSpacing="0" HorizontalAlign="Center" Height="45px"
               BackColor="#A8BBB8" BorderColor="#A8BBB8" BorderStyle="Solid" BorderWidth="1px">
                 <asp:TableRow> 
                   <asp:TableCell HorizontalAlign="Center" RowSpan="2" ColumnSpan="2">
                     <asp:Label ID="LabelJour" runat="server" Height="20px" Width="99px"
                        BackColor="#A8BBB8" BorderColor="LightGray" BorderStyle="None" Text=""
                        BorderWidth="1px" ForeColor="#D7FAF3" Font-Italic="False"
                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                        style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                        font-style: normal; text-indent: 10px; text-align: left;">
                     </asp:Label>
                   </asp:TableCell>           
                 </asp:TableRow>  
               </asp:Table>                
            </asp:TableCell>
            <asp:TableCell ID="CellHorairesBadges" BackColor="#f0f0f0" BorderColor="#A8BBB8" BorderStyle="None" Visible="true">
               <asp:Table ID="TableHorairesBadges" runat="server" CellPadding="0" Width="100px" Height="45px"
               CellSpacing="0" HorizontalAlign="Center" BorderColor="Black" Visible="true" BorderStyle="None">
                 <asp:TableRow>
                   <asp:TableCell HorizontalAlign="Center">
                     <asp:Label ID="LabelHorairesBadgesP1" runat="server" Height="20px" Width="99px"
                        BackColor="White" BorderColor="#A8BBB8" BorderStyle="Solid" Text=""
                        BorderWidth="1px" ForeColor="Black"
                        Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Small" Font-Italic="false"
                        style="margin-top: 0px; margin-left: 0px; text-indent: 0px; text-align: center;"> 
                     </asp:Label>
                   </asp:TableCell>           
                 </asp:TableRow>
                 <asp:TableRow>
                   <asp:TableCell HorizontalAlign="Center">
                     <asp:Label ID="LabelHorairesBadgesP2" runat="server" Height="20px" Width="99px"
                        BackColor="White" BorderColor="#A8BBB8" BorderStyle="Solid" Text=""
                        BorderWidth="1px" ForeColor="Black"
                        Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Small" Font-Italic="false"
                        style="margin-top: 0px; margin-left: 0px; text-indent: 0px; text-align: center;"> 
                     </asp:Label>
                   </asp:TableCell>           
                 </asp:TableRow>
               </asp:Table>
            </asp:TableCell>
            <asp:TableCell ID="CellHorairesJour" BackColor="#f0f0f0" BorderColor="#A8BBB8" BorderStyle="None" Visible="true">
               <asp:Table ID="TableHorairesJour" runat="server" CellPadding="0" Width="99px" Height="45px"
               CellSpacing="0" HorizontalAlign="Center" BorderColor="Black" Visible="true" BorderStyle="None">
                 <asp:TableRow>
                   <asp:TableCell HorizontalAlign="Center">
                     <asp:Label ID="LabelHorairesJourP1" runat="server" Height="20px" Width="98px"
                        BackColor="White" BorderColor="#A8BBB8" BorderStyle="Solid" Text=""
                        BorderWidth="1px" ForeColor="Black"
                        Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Small" Font-Italic="false"
                        style="margin-top: 0px; margin-left: 0px; text-indent: 0px; text-align: center;"> 
                     </asp:Label>
                   </asp:TableCell>           
                 </asp:TableRow>
                 <asp:TableRow>
                   <asp:TableCell HorizontalAlign="Center">
                     <asp:Label ID="LabelHorairesJourP2" runat="server" Height="20px" Width="98px"
                        BackColor="White" BorderColor="#A8BBB8" BorderStyle="Solid" Text=""
                        BorderWidth="1px" ForeColor="Black"
                        Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Small" Font-Italic="false"
                        style="margin-top: 0px; margin-left: 0px; text-indent: 0px; text-align: center;"> 
                     </asp:Label>
                   </asp:TableCell>           
                 </asp:TableRow>
               </asp:Table>
            </asp:TableCell>
            <asp:TableCell ID="CellDureeJour" BackColor="#f0f0f0" BorderColor="#A8BBB8" BorderStyle="None" Visible="true">
               <asp:Table ID="TableDureeJour" runat="server" CellPadding="0" Width="59px"  Height="45px"
               CellSpacing="0" HorizontalAlign="Center" BorderColor="Black" Visible="true" BorderStyle="None">
                 <asp:TableRow>
                   <asp:TableCell HorizontalAlign="Center">
                     <asp:Label ID="LabelDureeJourP1" runat="server" Height="20px" Width="58px"
                        BackColor="White" BorderColor="#A8BBB8" BorderStyle="Solid" Text=""
                        BorderWidth="1px" ForeColor="Black"
                        Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Small" Font-Italic="false"
                        style="margin-top: 0px; margin-left: 0px; text-indent: 0px; text-align: center;"> 
                     </asp:Label>
                   </asp:TableCell>           
                 </asp:TableRow>
                 <asp:TableRow>
                   <asp:TableCell HorizontalAlign="Center">
                     <asp:Label ID="LabelDureeJourP2" runat="server" Height="20px" Width="58px"
                        BackColor="White" BorderColor="#A8BBB8" BorderStyle="Solid" Text=""
                        BorderWidth="1px" ForeColor="Black"
                        Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Small" Font-Italic="false"
                        style="margin-top: 0px; margin-left: 0px; text-indent: 0px; text-align: center;"> 
                     </asp:Label>
                   </asp:TableCell>           
                 </asp:TableRow>
               </asp:Table>
            </asp:TableCell>
            <asp:TableCell ID="CellDureeTotalJour" BackColor="#f0f0f0" BorderColor="#A8BBB8" 
            BorderStyle="None" HorizontalAlign="Center">
               <asp:Table ID="TableDureeTotalJour" runat="server" CellPadding="0" Width="59px" Visible="true"  
               CellSpacing="0" HorizontalAlign="Center" Height="45px"
               BackColor="#f0f0f0" BorderColor="#A8BBB8" BorderStyle="Solid" BorderWidth="1px">
                 <asp:TableRow> 
                   <asp:TableCell HorizontalAlign="Center" RowSpan="2" ColumnSpan="2">
                     <asp:Label ID="LabelDureeTotalJour" runat="server" Height="20px" Width="58px"
                        BackColor="#f0f0f0" BorderColor="#A8BBB8" BorderStyle="None" Text=""
                        BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                        style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                        font-style: normal; text-indent: 0px; text-align: Center;">
                     </asp:Label>
                   </asp:TableCell>           
                 </asp:TableRow>  
               </asp:Table>                
            </asp:TableCell>
            <asp:TableCell ID="CellCommentaireJour" BackColor="White" BorderColor="#A8BBB8" BorderStyle="None" Visible="true">
               <asp:Table ID="TableCommentaireJour" runat="server" Height="45px" CellPadding="0" Width="167px" 
               CellSpacing="0" HorizontalAlign="Center" Visible="true"
               BackColor="White" BorderColor="#A8BBB8" BorderStyle="Solid" BorderWidth="1px">
                 <asp:TableRow>
                   <asp:TableCell HorizontalAlign="Center" RowSpan="2" ColumnSpan="2">
                     <asp:Label ID="LabelCommentaireJour" runat="server" Height="30px" Width="166px"
                        BackColor="White" BorderColor="#A8BBB8" BorderStyle="None" Text=""
                        BorderWidth="1px" ForeColor="Black"
                        Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Small" Font-Italic="false"
                        style="margin-top: 1px; margin-left: 0px; margin-right: 0px;
                        text-indent: 0px; text-align: center;"> 
                     </asp:Label>  
                   </asp:TableCell>           
                 </asp:TableRow> 
               </asp:Table>
            </asp:TableCell>           
          </asp:TableRow>
          
          <asp:TableRow>
            <asp:TableCell Height="2px" ColumnSpan="6"></asp:TableCell>
          </asp:TableRow> 
 </asp:Table>
