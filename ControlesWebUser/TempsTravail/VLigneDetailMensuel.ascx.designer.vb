﻿'------------------------------------------------------------------------------
' <généré automatiquement>
'     Ce code a été généré par un outil.
'
'     Les modifications apportées à ce fichier peuvent provoquer un comportement incorrect et seront perdues si
'     le code est régénéré.
' </généré automatiquement>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class Controles_VLigneDetailMensuel

    '''<summary>
    '''Contrôle CadreLigne.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CadreLigne As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle CellTitreJour.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellTitreJour As Global.System.Web.UI.WebControls.TableCell

    '''<summary>
    '''Contrôle LabelDate.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelDate As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle CellLigneMatin.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellLigneMatin As Global.System.Web.UI.WebControls.TableCell

    '''<summary>
    '''Contrôle LigneMatin.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LigneMatin As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle CellLigneMatinHDebut.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellLigneMatinHDebut As Global.System.Web.UI.WebControls.TableCell

    '''<summary>
    '''Contrôle LigneMatinHDebut.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LigneMatinHDebut As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle CellLigneMatinHFin.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellLigneMatinHFin As Global.System.Web.UI.WebControls.TableCell

    '''<summary>
    '''Contrôle LigneMatinHFin.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LigneMatinHFin As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle CellLigneApresMidi.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellLigneApresMidi As Global.System.Web.UI.WebControls.TableCell

    '''<summary>
    '''Contrôle LigneApresMidi.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LigneApresMidi As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle CellLigneApresMidiHDebut.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellLigneApresMidiHDebut As Global.System.Web.UI.WebControls.TableCell

    '''<summary>
    '''Contrôle LigneApresMidiHDebut.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LigneApresMidiHDebut As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle CellLigneApresMidiHFin.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellLigneApresMidiHFin As Global.System.Web.UI.WebControls.TableCell

    '''<summary>
    '''Contrôle LigneApresMidiHFin.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LigneApresMidiHFin As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle CellLigneTotalJour.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellLigneTotalJour As Global.System.Web.UI.WebControls.TableCell

    '''<summary>
    '''Contrôle LigneTotalJour.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LigneTotalJour As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle CellLigneTotalSem.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellLigneTotalSem As Global.System.Web.UI.WebControls.TableCell

    '''<summary>
    '''Contrôle LigneTotalSem.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LigneTotalSem As Global.System.Web.UI.WebControls.Label
End Class
