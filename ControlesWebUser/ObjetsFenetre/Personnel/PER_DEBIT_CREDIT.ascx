﻿<%@ Control Language="VB" AutoEventWireup="false" Inherits="Virtualia.Net.Fenetre_PER_DEBIT_CREDIT"  Codebehind="PER_DEBIT_CREDIT.ascx.vb" %>

<%@ Register src="../../Saisies/VListeCombo.ascx" tagname="VListeCombo" tagprefix="Virtualia" %>
<%@ Register src="../../TempsTravail/V2LignesDebitCredit.ascx" tagname="VLigneDC" tagprefix="Virtualia" %>

<%@ Register src="../../Saisies/VListeGrid.ascx" tagname="VListeGrid" tagprefix="Virtualia" %>

<asp:Table ID="CadreInfo" runat="server" BorderStyle="None" BorderWidth="2px" Visible="true"
    BorderColor="#B0E0D7" Height="260px" Width="750px" HorizontalAlign="Center" style="margin-top: 3px;">
  <asp:TableRow>
    <asp:TableCell>
      <asp:Table ID="CadreTitre" runat="server" Height="30px" CellPadding="0" Width="750px" 
         CellSpacing="0" HorizontalAlign="Center" BorderColor="Black" Visible="true" BorderStyle="NotSet">
            <asp:TableRow>
                <asp:TableCell HorizontalAlign="Center">
                    <asp:Label ID="Etiquette" runat="server" Text="Débit - Crédit" Height="20px" Width="320px"
                        BackColor="#216B68" BorderColor="#B0E0D7"  BorderStyle="Groove"
                        BorderWidth="2px" ForeColor="#D7FAF3"
                        Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Small"
                        style="margin-top: 5px; margin-left: 4px; margin-bottom: 0px;
                        font-style: oblique; text-indent: 5px; text-align: center;">
                    </asp:Label>          
                </asp:TableCell>      
            </asp:TableRow>
      </asp:Table>
    </asp:TableCell>
  </asp:TableRow>
  <asp:TableRow>
    <asp:TableCell>
        <asp:Table ID="TableauDebitCredit" runat="server" CellPadding="0" CellSpacing="0" Width="750px"
            BorderStyle= "Solid" BorderWidth="2px" BorderColor="#B0E0D7" HorizontalAlign="Left" Visible="true">
                <asp:TableRow>
                    <asp:TableCell ColumnSpan="13" Height="40px">
                        <Virtualia:VListeCombo ID="LstAnnee" runat="server" EtiVisible="true" EtiText="Annee" EtiWidth="60px"
                                V_NomTable="Semaine" LstWidth="120px" LstBorderColor="#B0E0D7" LstBackColor="#EEECFD"
                                EtiStyle="text-align:center" EtiBackColor="#8DA8A3" EtiForeColor="#E9FDF9" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow ID="Ligne0" Width="748px">
                 <asp:TableCell ID="CellTitreLigne0" BackColor="#D7FAF3" BorderColor="#D7FAF3" BorderStyle="Notset"> 
                  <asp:Label ID="LabelTitreLigne0" runat="server" Height="20px" Width="204px"
                      BackColor="#D7FAF3" BorderColor="#D7FAF3" BorderStyle="Notset" Text=""
                      BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                      Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Smaller"
                      style="margin-top: 2px; margin-left: 0px; margin-bottom: 0px;
                      font-style: normal; text-indent: 2px; text-align:  left; padding-top: 0px;">
                  </asp:Label>          
                 </asp:TableCell>
                 <asp:TableCell ID="CellMoisJanvier" BackColor="#D7FAF3" BorderColor="#D7FAF3" BorderStyle="Notset"> 
                  <asp:Label ID="LabelMoisJanvier" runat="server" Height="20px" Width="43px" 
                      BackColor="#A8BBB8" BorderColor="#D7FAF3" BorderStyle="NotSet" Text="Janv"
                      BorderWidth="1px" ForeColor="White" Font-Italic="False"
                      Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Smaller"
                      style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                      font-style: normal; text-indent: 0px;  text-align:  center; padding-top: 2px;">
                  </asp:Label>          
                 </asp:TableCell>
                 <asp:TableCell ID="CellMoisFevrier" BackColor="#D7FAF3" BorderColor="#D7FAF3" BorderStyle="Notset"> 
                  <asp:Label ID="LabelMoisFevrier" runat="server" Height="20px" Width="43px"
                      BackColor="#A8BBB8" BorderColor="#D7FAF3" BorderStyle="NotSet" Text="Févr"
                      BorderWidth="1px" ForeColor="White" Font-Italic="False"
                      Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Smaller"
                      style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                      font-style: normal; text-indent: 0px; text-align:  center; padding-top: 2px;">
                  </asp:Label>          
                 </asp:TableCell>
                 <asp:TableCell ID="CellMoisMars" BackColor="#D7FAF3" BorderColor="#D7FAF3" BorderStyle="Notset"> 
                  <asp:Label ID="LabelMoisMars" runat="server" Height="20px" Width="43px"
                      BackColor="#A8BBB8" BorderColor="#D7FAF3" BorderStyle="NotSet" Text="Mars"
                      BorderWidth="1px" ForeColor="White" Font-Italic="False"
                      Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Smaller"
                      style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                      font-style: normal; text-indent: 0px; text-align:  center; padding-top: 2px;">
                  </asp:Label>          
                 </asp:TableCell>
                 <asp:TableCell ID="CellMoisAvril" BackColor="#D7FAF3" BorderColor="#D7FAF3" BorderStyle="Notset"> 
                  <asp:Label ID="LabelMoisAvril" runat="server" Height="20px" Width="43px"
                      BackColor="#A8BBB8" BorderColor="#D7FAF3" BorderStyle="NotSet" Text="Avril"
                      BorderWidth="1px" ForeColor="White" Font-Italic="False"
                      Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Smaller"
                      style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                      font-style: normal; text-indent: 0px; text-align:  center; padding-top: 2px;">
                  </asp:Label>          
                 </asp:TableCell>
                 <asp:TableCell ID="CellMoisMai" BackColor="#D7FAF3" BorderColor="#D7FAF3" BorderStyle="Notset"> 
                  <asp:Label ID="LabelMoisMai" runat="server" Height="20px" Width="43px"
                      BackColor="#A8BBB8" BorderColor="#D7FAF3" BorderStyle="NotSet" Text="Mai"
                      BorderWidth="1px" ForeColor="White" Font-Italic="False"
                      Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Smaller"
                      style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                      font-style: normal; text-indent: 0px; text-align:  center; padding-top: 2px;">
                  </asp:Label>          
                 </asp:TableCell>
                 <asp:TableCell ID="CellMoisJuin" BackColor="#D7FAF3" BorderColor="#D7FAF3" BorderStyle="Notset"> 
                  <asp:Label ID="LabelMoisJuin" runat="server" Height="20px" Width="43px"
                      BackColor="#A8BBB8" BorderColor="#D7FAF3" BorderStyle="NotSet" Text="Juin"
                      BorderWidth="1px" ForeColor="White" Font-Italic="False"
                      Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Smaller"
                      style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                      font-style: normal; text-indent: 0px; text-align:  center; padding-top: 2px;">
                  </asp:Label>          
                 </asp:TableCell>
                 <asp:TableCell ID="CellMoisJuillet" BackColor="#D7FAF3" BorderColor="#D7FAF3" BorderStyle="Notset"> 
                  <asp:Label ID="LabelMoisJuillet" runat="server" Height="20px" Width="43px"
                      BackColor="#A8BBB8" BorderColor="#D7FAF3" BorderStyle="NotSet" Text="Juil"
                      BorderWidth="1px" ForeColor="White" Font-Italic="False"
                      Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Smaller"
                      style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                      font-style: normal; text-indent: 0px; text-align:  center; padding-top: 2px;">
                  </asp:Label>          
                 </asp:TableCell>
                 <asp:TableCell ID="CellMoisAout" BackColor="#D7FAF3" BorderColor="#D7FAF3" BorderStyle="Notset"> 
                  <asp:Label ID="LabelMoisAout" runat="server" Height="20px" Width="43px"
                      BackColor="#A8BBB8" BorderColor="#D7FAF3" BorderStyle="NotSet" Text="Août"
                      BorderWidth="1px" ForeColor="White" Font-Italic="False"
                      Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Smaller"
                      style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                      font-style: normal; text-indent: 0px; text-align:  center; padding-top: 2px;">
                  </asp:Label>          
                 </asp:TableCell>
                 <asp:TableCell ID="CellMoisSeptembre" BackColor="#D7FAF3" BorderColor="#D7FAF3" BorderStyle="Notset"> 
                  <asp:Label ID="LabelMoisSeptembre" runat="server" Height="20px" Width="43px"
                      BackColor="#A8BBB8" BorderColor="#D7FAF3" BorderStyle="NotSet" Text="Sept"
                      BorderWidth="1px" ForeColor="White" Font-Italic="False"
                      Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Smaller"
                      style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                      font-style: normal; text-indent: 0px; text-align:  center; padding-top: 2px;">
                  </asp:Label>          
                 </asp:TableCell>
                 <asp:TableCell ID="CellMoisOctobre" BackColor="#D7FAF3" BorderColor="#D7FAF3" BorderStyle="Notset"> 
                  <asp:Label ID="LabelMoisOctobre" runat="server" Height="20px" Width="43px"
                      BackColor="#A8BBB8" BorderColor="#D7FAF3" BorderStyle="NotSet" Text="Oct"
                      BorderWidth="1px" ForeColor="White" Font-Italic="False"
                      Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Smaller"
                      style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                      font-style: normal; text-indent: 0px; text-align:  center; padding-top: 2px;">
                  </asp:Label>          
                 </asp:TableCell>
                 <asp:TableCell ID="CellMoisNovembre" BackColor="#D7FAF3" BorderColor="#D7FAF3" BorderStyle="Notset"> 
                  <asp:Label ID="LabelMoisNovembre" runat="server" Height="20px" Width="43px"
                      BackColor="#A8BBB8" BorderColor="#D7FAF3" BorderStyle="NotSet" Text="Nov"
                      BorderWidth="1px" ForeColor="White" Font-Italic="False"
                      Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Smaller"
                      style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                      font-style: normal; text-indent: 0px; text-align:  center; padding-top: 2px;">
                  </asp:Label>          
                 </asp:TableCell>
                 <asp:TableCell ID="CellMoisDecembre" BackColor="#D7FAF3" BorderColor="#D7FAF3" BorderStyle="Notset"> 
                  <asp:Label ID="LabelMoisDecembre" runat="server" Height="20px" Width="43px"
                      BackColor="#A8BBB8" BorderColor="#D7FAF3" BorderStyle="NotSet" Text="Déc"
                      BorderWidth="1px" ForeColor="White" Font-Italic="False"
                      Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Smaller"
                      style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                      font-style: normal; text-indent: 0px; text-align:  center; padding-top: 2px;">
                  </asp:Label>          
                 </asp:TableCell>      
              </asp:TableRow>
              <asp:TableRow>                
                <asp:TableCell ColumnSpan="13">
                        <Virtualia:VLigneDC ID="Ligne01_02" runat="server" />
                </asp:TableCell>
              </asp:TableRow>
              <asp:TableRow>                
                <asp:TableCell ColumnSpan="13">
                        <Virtualia:VLigneDC ID="Ligne03_04" runat="server" />
                </asp:TableCell>
              </asp:TableRow>
              <asp:TableRow>                
                <asp:TableCell ColumnSpan="13">
                        <Virtualia:VLigneDC ID="Ligne05_06" runat="server" />
                </asp:TableCell>
              </asp:TableRow>
              <asp:TableRow>                
                <asp:TableCell ColumnSpan="13">
                        <Virtualia:VLigneDC ID="Ligne07_08" runat="server" />
                </asp:TableCell>
              </asp:TableRow>
              <asp:TableRow>                
                <asp:TableCell ColumnSpan="13">
                        <Virtualia:VLigneDC ID="Ligne09_10" runat="server" />
                </asp:TableCell>
              </asp:TableRow>
              <asp:TableRow>                
                <asp:TableCell ColumnSpan="13">
                        <Virtualia:VLigneDC ID="Ligne11_12" runat="server" />
                </asp:TableCell>
              </asp:TableRow>
              <asp:TableRow>                
                <asp:TableCell ColumnSpan="13">
                        <Virtualia:VLigneDC ID="Ligne13_14" runat="server" />
                </asp:TableCell>
              </asp:TableRow>
              <asp:TableRow>                
                <asp:TableCell ColumnSpan="13">
                        <Virtualia:VLigneDC ID="Ligne15_16" runat="server" />
                </asp:TableCell>
              </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
      </asp:TableRow>
      <asp:TableRow>
        <asp:TableCell ColumnSpan="13" Height="40px">
            <Virtualia:VListeCombo ID="LstLibelMois" runat="server" EtiVisible="true" EtiText="Mois" EtiWidth="60px"
                    V_NomTable="Mensuel" LstWidth="180px" LstBorderColor="#B0E0D7" LstBackColor="#EEECFD"
                    EtiStyle="text-align:center" EtiBackColor="#8DA8A3" EtiForeColor="#E9FDF9" />
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow> 
     <asp:TableCell> 
       <Virtualia:VListeGrid ID="ListeGrille" runat="server" CadreWidth="740px" SiColonneSelect="false" />
     </asp:TableCell>
   </asp:TableRow>
 </asp:Table>