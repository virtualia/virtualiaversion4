﻿<%@ Control Language="VB" AutoEventWireup="false" Inherits="Virtualia.Net.Fenetre_PER_ADMORIGINE" CodeBehind="PER_ADMORIGINE.ascx.vb" %>

<%@ Register Src="~/Controles/Saisies/VCoupleEtiDonnee.ascx" TagName="VCoupleEtiDonnee" TagPrefix="Virtualia" %>
<%@ Register Src="~/Controles/Saisies/VCoupleVerticalEtiDonnee.ascx" TagName="VCoupleVerticalEtiDonnee" TagPrefix="Virtualia" %>
<%@ Register Src="~/Controles/Saisies/VDuoEtiquetteCommande.ascx" TagName="VDuoEtiquetteCommande" TagPrefix="Virtualia" %>
<%@ Register Src="~/Controles/Saisies/VCocheSimple.ascx" TagName="VCocheSimple" TagPrefix="Virtualia" %>
<%@ Register Src="~/Controles/Saisies/VCoupleEtiquetteExperte.ascx" TagName="VCoupleEtiquetteExperte" TagPrefix="Virtualia" %>

<%@ Register Src="~/ControlsGeneriques/V_CommandeOK.ascx" TagName="VCmdOK" TagPrefix="Generic" %>

<asp:Table ID="CadreInfo" runat="server" BorderStyle="Ridge" BorderWidth="2px"
    BorderColor="#B0E0D7" Height="720px" Width="700px" HorizontalAlign="Center"
    Style="position: relative">
    <asp:TableRow>
        <asp:TableCell>
            <Generic:VCmdOK ID="CtlOK" runat="server" Height="22px" Width="70px" HorizontalAlign="Right" CadreStyle="margin-top: 3px; margin-right: 3px"/>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="CadreDonnee" runat="server" Width="700px" CellPadding="0"
                CellSpacing="0" HorizontalAlign="Center">
                <asp:TableRow>
                    <asp:TableCell ColumnSpan="2" HorizontalAlign="Center">
                        <asp:Label ID="EtiIdentite" runat="server" Text="Services antérieurs" Height="20px" Width="300px" BackColor="#2FA49B" BorderColor="#B0E0D7" BorderStyle="Solid" BorderWidth="2px" ForeColor="#D7FAF3" Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Small" Style="margin-top: 15px; margin-left: 4px; margin-bottom: 10px; font-style: oblique; text-indent: 5px; text-align: center;" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell ColumnSpan="2" HorizontalAlign="Left">
                        <Virtualia:VDuoEtiquetteCommande ID="Dontab01" runat="server" V_PointdeVue="1" V_Objet="25" V_Information="1" V_SiDonneeDico="true" EtiWidth="200px" DonWidth="450px" DonTabIndex="1" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell ColumnSpan="2" HorizontalAlign="Left">
                        <Virtualia:VDuoEtiquetteCommande ID="Dontab23" runat="server" V_PointdeVue="1" V_Objet="25" V_Information="23" V_SiDonneeDico="true" EtiWidth="200px" DonWidth="450px" DonTabIndex="2" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell ColumnSpan="2">
                        <asp:Table ID="EntreesFP" runat="server" HorizontalAlign="Center">
                            <asp:TableRow>
                                <asp:TableCell>
                                    <Virtualia:VCoupleVerticalEtiDonnee ID="InfoV02" runat="server" V_PointdeVue="1" V_Objet="25" V_Information="2" V_SiDonneeDico="true" EtiWidth="150px" EtiHeight="35px" DonWidth="120px" DonTabIndex="3" DonTextMode="false" DonHeight="16px" EtiStyle="text-align:center;margin-top:10px" DonStyle="text-align:center;margin-left:15px" />
                                </asp:TableCell>
                                <asp:TableCell>
                                    <Virtualia:VCoupleVerticalEtiDonnee ID="InfoV03" runat="server" V_PointdeVue="1" V_Objet="25" V_Information="3" V_SiDonneeDico="true" EtiWidth="150px" EtiHeight="35px" DonWidth="120px" DonTabIndex="4" DonTextMode="false" DonHeight="16px" EtiStyle="text-align:center;margin-top:10px" DonStyle="text-align:center;margin-left:15px" />
                                </asp:TableCell>
                                <asp:TableCell>
                                    <Virtualia:VCoupleVerticalEtiDonnee ID="InfoV04" runat="server" V_PointdeVue="1" V_Objet="25" V_Information="4" V_SiDonneeDico="true" EtiWidth="150px" EtiHeight="35px" DonWidth="120px" DonTabIndex="5" DonTextMode="false" DonHeight="16px" EtiStyle="text-align:center;margin-top:10px" DonStyle="text-align:center;margin-left:15px" />
                                </asp:TableCell>
                                <asp:TableCell>
                                    <Virtualia:VCoupleVerticalEtiDonnee ID="InfoV05" runat="server" V_PointdeVue="1" V_Objet="25" V_Information="5" V_SiDonneeDico="true" EtiWidth="150px" EtiHeight="35px" DonWidth="120px" DonTabIndex="6" DonTextMode="false" DonHeight="16px" EtiStyle="text-align:center;margin-top:10px" DonStyle="text-align:center;margin-left:15px" />
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell ColumnSpan="2" HorizontalAlign="Center">
                        <asp:Label ID="EtiServices" runat="server" Text="Services validés" Height="20px" Width="300px" BackColor="#2FA49B" BorderColor="#B0E0D7" BorderStyle="Solid" BorderWidth="2px" ForeColor="#D7FAF3" Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Small" Style="margin-top: 15px; margin-left: 4px; margin-bottom: 5px; font-style: oblique; text-indent: 5px; text-align: center;" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell ColumnSpan="2" HorizontalAlign="Left">
                        <asp:Table ID="CadreSerEffectifs" runat="server">
                            <asp:TableRow>
                                <asp:TableCell>
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH06" runat="server" V_PointdeVue="1" V_Objet="25" V_Information="6" V_SiDonneeDico="true" EtiWidth="300px" DonWidth="40px" DonTabIndex="7" EtiText="Totalité des services antérieurs validés" DonTooltip="Nombre d'années" />
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Left">
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH07" runat="server" EtiVisible="false" V_PointdeVue="1" V_Objet="25" V_Information="7" V_SiDonneeDico="true" DonWidth="40px" DonTabIndex="8" DonTooltip="Nombre de mois" />
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Left">
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH08" runat="server" EtiVisible="false" V_PointdeVue="1" V_Objet="25" V_Information="8" V_SiDonneeDico="true" DonWidth="40px" DonTabIndex="9" DonTooltip="Nombre de jours" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell ColumnSpan="3" HorizontalAlign="Left">
                                    <Virtualia:VDuoEtiquetteCommande ID="Dontab09" runat="server" V_PointdeVue="1" V_Objet="25" V_Information="9" V_SiDonneeDico="true" EtiWidth="300px" DonWidth="150px" DonTabIndex="10" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Height="5px" ColumnSpan="3" />
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell>
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH14" runat="server" V_PointdeVue="1" V_Objet="25" V_Information="14" V_SiDonneeDico="true" EtiWidth="300px" DonWidth="40px" DonTabIndex="10" EtiText="Partie validée au titre des services publics" DonTooltip="Nombre d'années" />
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Left">
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH15" runat="server" EtiVisible="false" V_PointdeVue="1" V_Objet="25" V_Information="15" V_SiDonneeDico="true" DonWidth="40px" DonTabIndex="11" DonTooltip="Nombre de mois" />
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Left">
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH16" runat="server" EtiVisible="false" V_PointdeVue="1" V_Objet="25" V_Information="16" V_SiDonneeDico="true" DonWidth="40px" DonTabIndex="12" DonTooltip="Nombre de jours" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell>
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH17" runat="server" V_PointdeVue="1" V_Objet="25" V_Information="17" V_SiDonneeDico="true" EtiWidth="300px" DonWidth="40px" DonTabIndex="13" EtiText="Partie validée au titre des services privés" DonTooltip="Nombre d'années" />
                                </asp:TableCell>
                                <asp:TableCell RowSpan="1" HorizontalAlign="Left">
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH18" runat="server" EtiVisible="false" V_PointdeVue="1" V_Objet="25" V_Information="18" V_SiDonneeDico="true" DonWidth="40px" DonTabIndex="14" DonTooltip="Nombre de mois" />
                                </asp:TableCell>
                                <asp:TableCell RowSpan="1" HorizontalAlign="Left">
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH19" runat="server" EtiVisible="false" V_PointdeVue="1" V_Objet="25" V_Information="19" V_SiDonneeDico="true" DonWidth="40px" DonTabIndex="15" DonTooltip="Nombre de jours" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Height="15px" ColumnSpan="3" />
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell>
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH20" runat="server" V_PointdeVue="1" V_Objet="25" V_Information="20" V_SiDonneeDico="true" EtiWidth="300px" DonWidth="40px" DonTabIndex="16" EtiText="Services conduisant à Pension" DonTooltip="Nombre d'années" />
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Left">
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH21" runat="server" EtiVisible="false" V_PointdeVue="1" V_Objet="25" V_Information="21" V_SiDonneeDico="true" DonWidth="40px" DonTabIndex="17" DonTooltip="Nombre de mois" />
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Left">
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH22" runat="server" EtiVisible="false" V_PointdeVue="1" V_Objet="25" V_Information="22" V_SiDonneeDico="true" DonWidth="40px" DonTabIndex="18" DonTooltip="Nombre de jours" />
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell ColumnSpan="2" HorizontalAlign="Center">
                        <asp:Label ID="EtiMobilite" runat="server" Text="Mobilité" Height="20px" Width="300px" BackColor="#2FA49B" BorderColor="#B0E0D7" BorderStyle="Solid" BorderWidth="2px" ForeColor="#D7FAF3" Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Small" Style="margin-top: 15px; margin-left: 4px; margin-bottom: 5px; font-style: oblique; text-indent: 5px; text-align: center;" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        <Virtualia:VCocheSimple ID="Coche24" runat="server" V_PointdeVue="1" V_Objet="25" V_Information="24" V_SiDonneeDico="true" V_Width="200px" V_Style="margin-left:5px" />
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VCoupleEtiDonnee ID="InfoH25" runat="server" EtiWidth="200" V_PointdeVue="1" V_Objet="25" V_Information="25" V_SiDonneeDico="true" DonWidth="120px" DonTabIndex="19" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell ColumnSpan="2" HorizontalAlign="Center">
                        <asp:Label ID="EtiEcole" runat="server" Text="Ecole de fonctionnaire" Height="20px" Width="300px" BackColor="#2FA49B" BorderColor="#B0E0D7" BorderStyle="Solid" BorderWidth="2px" ForeColor="#D7FAF3" Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Small" Style="margin-top: 15px; margin-left: 4px; margin-bottom: 5px; font-style: oblique; text-indent: 5px; text-align: center;" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell ColumnSpan="2" HorizontalAlign="Left">
                        <Virtualia:VDuoEtiquetteCommande ID="Dontab11" runat="server" V_PointdeVue="1" V_Objet="25" V_Information="11" V_SiDonneeDico="true" EtiWidth="200px" DonWidth="400px" DonTabIndex="20" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VCoupleEtiDonnee ID="InfoH12" runat="server" EtiWidth="40px" V_PointdeVue="1" V_Objet="25" V_Information="12" V_SiDonneeDico="true" DonWidth="120px" DonTabIndex="21" />
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VCoupleEtiDonnee ID="InfoH13" runat="server" EtiWidth="40px" V_PointdeVue="1" V_Objet="25" V_Information="13" V_SiDonneeDico="true" DonWidth="120px" DonTabIndex="22" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell ColumnSpan="2" HorizontalAlign="Center">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="InfoV10" runat="server" V_PointdeVue="1" V_Objet="25" V_Information="10" V_SiDonneeDico="true" EtiWidth="602px" DonWidth="600px" DonTabIndex="23" DonTextMode="true" DonHeight="60px" EtiStyle="margin-top:10px; text-align:center" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="12px" ColumnSpan="2" />
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <asp:Panel ID="CadreExpert" runat="server" BorderStyle="none" BorderWidth="1px" BorderColor="#B0E0D7" Height="300px" Width="600px" HorizontalAlign="Left" Style="margin-top: 5px; vertical-align: middle; text-align: center">
                <Virtualia:VCoupleEtiquetteExperte ID="ExprH01" runat="server" V_PointdeVue="1" V_Objet="25" V_InfoExperte="612" V_SiDonneeDico="true" EtiHeight="20px" EtiWidth="300px" DonWidth="200px" DonHeight="20px" EtiStyle="margin-left: 50px; margin-top: 5px" DonStyle="margin-top: 9px; text-align: center" />
                <Virtualia:VCoupleEtiquetteExperte ID="ExprH02" runat="server" V_PointdeVue="1" V_Objet="25" V_InfoExperte="616" V_SiDonneeDico="true" EtiHeight="20px" EtiWidth="300px" DonWidth="200px" DonHeight="20px" EtiStyle="margin-left: 50px;" DonStyle="text-align: center;" />
                <Virtualia:VCoupleEtiquetteExperte ID="ExprH03" runat="server" V_PointdeVue="1" V_Objet="25" V_InfoExperte="610" V_SiDonneeDico="true" EtiHeight="20px" EtiWidth="300px" DonWidth="200px" DonHeight="20px" EtiStyle="margin-left: 50px;" DonStyle="text-align: center;" />
                <Virtualia:VCoupleEtiquetteExperte ID="ExprH04" runat="server" V_PointdeVue="1" V_Objet="25" V_InfoExperte="615" V_SiDonneeDico="true" EtiHeight="20px" EtiWidth="300px" DonWidth="200px" DonHeight="20px" EtiStyle="margin-left: 50px;" DonStyle="text-align: center;" />
                <Virtualia:VCoupleEtiquetteExperte ID="ExprH05" runat="server" V_PointdeVue="1" V_Objet="25" V_InfoExperte="850" V_SiDonneeDico="true" EtiHeight="20px" EtiWidth="300px" DonWidth="200px" DonHeight="20px" EtiStyle="margin-left: 50px;" DonStyle="text-align: center;" />
                <Virtualia:VCoupleEtiquetteExperte ID="ExprH06" runat="server" V_PointdeVue="1" V_Objet="25" V_InfoExperte="853" V_SiDonneeDico="true" EtiHeight="20px" EtiWidth="300px" DonWidth="200px" DonHeight="20px" EtiStyle="margin-left: 50px;" DonStyle="text-align: center;" />
                <Virtualia:VCoupleEtiquetteExperte ID="ExprH07" runat="server" V_PointdeVue="1" V_Objet="25" V_InfoExperte="851" V_SiDonneeDico="true" EtiHeight="20px" EtiWidth="300px" DonWidth="200px" DonHeight="20px" EtiStyle="margin-left: 50px;" DonStyle="text-align: center;" />
                <Virtualia:VCoupleEtiquetteExperte ID="ExprH08" runat="server" V_PointdeVue="1" V_Objet="25" V_InfoExperte="852" V_SiDonneeDico="true" EtiHeight="20px" EtiWidth="300px" DonWidth="200px" DonHeight="20px" EtiStyle="margin-left: 50px;" DonStyle="text-align: center;" />
                <Virtualia:VCoupleEtiquetteExperte ID="ExprH09" runat="server" V_PointdeVue="1" V_Objet="25" V_InfoExperte="854" V_SiDonneeDico="true" EtiHeight="20px" EtiWidth="300px" DonWidth="200px" DonHeight="20px" EtiStyle="margin-left: 50px;" DonStyle="text-align: center;" />
            </asp:Panel>
        </asp:TableCell>
    </asp:TableRow>
</asp:Table>
