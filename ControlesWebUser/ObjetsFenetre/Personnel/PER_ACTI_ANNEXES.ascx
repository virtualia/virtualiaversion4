﻿<%@ Control Language="VB" AutoEventWireup="false" Inherits="Virtualia.Net.Fenetre_PER_ACTI_ANNEXES" CodeBehind="PER_ACTI_ANNEXES.ascx.vb" %>

<%@ Register Src="~/Controles/Saisies/VCoupleEtiDonnee.ascx" TagName="VCoupleEtiDonnee" TagPrefix="Virtualia" %>
<%@ Register Src="~/Controles/Saisies/VDuoEtiquetteCommande.ascx" TagName="VDuoEtiquetteCommande" TagPrefix="Virtualia" %>
<%@ Register Src="~/Controles/Saisies/VCocheSimple.ascx" TagName="VCocheSimple" TagPrefix="Virtualia" %>
<%@ Register Src="~/Controles/Saisies/VCoupleVerticalEtiDonnee.ascx" TagName="VCoupleVerticalEtiDonnee" TagPrefix="Virtualia" %>

<%@ Register Src="~/ControlsGeneriques/V_DataGrid.ascx" TagName="VDataGrid" TagPrefix="Generic" %>
<%@ Register Src="~/ControlsGeneriques/V_CommandeCRUD.ascx" TagName="VCrud" TagPrefix="Generic" %>

<asp:Table ID="CadreControle" runat="server" Height="30px" Width="700px" CellPadding="0" CellSpacing="0" HorizontalAlign="Center">
    <asp:TableRow>
        <asp:TableCell>
            <Generic:VDataGrid ID="ListeGrille" runat="server" CadreWidth="680px" SiColonneSelect="true" SiCaseAcocher="false" />
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="CadreInfo" runat="server" BorderStyle="Ridge" BorderWidth="2px" BackColor="#CBF2BE" BorderColor="#B0E0D7" Height="550px" Width="700px" HorizontalAlign="Center" Style="margin-top: 3px;">
                <asp:TableRow>
                    <asp:TableCell>
                        <Generic:VCrud ID="CtlCrud" runat="server" Height="22px" Width="244px" HorizontalAlign="Right" CadreStyle="margin-top: 3px; margin-right: 3px" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Table ID="CadreTitre" runat="server" Height="40px" CellPadding="0" Width="700px" CellSpacing="0" HorizontalAlign="Center">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                    <asp:Label ID="Etiquette" runat="server" Text="Activités annexes" Height="20px" Width="320px" BackColor="#2FA49B" BorderColor="#B0E0D7" BorderStyle="Groove" BorderWidth="2px" ForeColor="#D7FAF3" Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Small" Style="margin-top: 5px; margin-left: 4px; margin-bottom: 10px; font-style: oblique; text-indent: 5px; text-align: center;" />
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Table ID="CadreDon" runat="server" CellPadding="0" CellSpacing="0" Width="700px">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left" Width="300px">
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH00" runat="server" EtiBackColor="#CBF2BE" V_PointdeVue="1" V_Objet="48" V_Information="0" V_SiDonneeDico="true" DonWidth="100px" DonTabIndex="1" V_SiFenetrePer_Bis="true" />
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Left" Width="300px">
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH10" runat="server" EtiBackColor="#CBF2BE" V_PointdeVue="1" V_Objet="48" V_Information="10" V_SiDonneeDico="true" V_SiFenetrePer_Bis="true" EtiWidth="80px" DonWidth="100px" DonTabIndex="2" EtiStyle="margin-left:2px" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left" Width="300px">
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH08" runat="server" EtiBackColor="#CBF2BE" V_PointdeVue="1" V_Objet="48" V_Information="8" V_SiDonneeDico="true" V_SiFenetrePer_Bis="true" DonWidth="100px" DonTabIndex="3" />
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Left" Width="300px">
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH09" runat="server" EtiBackColor="#CBF2BE" V_PointdeVue="1" V_Objet="48" V_Information="9" V_SiDonneeDico="true" V_SiFenetrePer_Bis="true" EtiWidth="80px" DonWidth="100px" DonTabIndex="4" EtiStyle="margin-left:2px" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left" ColumnSpan="2">
                                    <Virtualia:VDuoEtiquetteCommande ID="Dontab01" runat="server" EtiBackColor="#CBF2BE" V_PointdeVue="1" V_Objet="48" V_Information="1" V_SiDonneeDico="true" V_SiFenetrePer_Bis="true" DonTabIndex="5" DonWidth="490px" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <Virtualia:VDuoEtiquetteCommande ID="Dontab02" runat="server" EtiBackColor="#CBF2BE" V_PointdeVue="1" V_Objet="48" V_Information="2" V_SiDonneeDico="true" V_SiFenetrePer_Bis="true" DonTabIndex="6" EtiWidth="120px" DonWidth="150px" />
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Left">
                                    <Virtualia:VDuoEtiquetteCommande ID="Dontab04" runat="server" EtiBackColor="#CBF2BE" V_PointdeVue="1" V_Objet="48" V_Information="4" V_SiDonneeDico="true" V_SiFenetrePer_Bis="true" DonTabIndex="7" EtiWidth="120px" DonWidth="250px" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left" ColumnSpan="2">
                                    <Virtualia:VDuoEtiquetteCommande ID="Dontab06" runat="server" EtiBackColor="#CBF2BE" V_PointdeVue="1" V_Objet="48" V_Information="6" V_SiDonneeDico="true" V_SiFenetrePer_Bis="true" DonTabIndex="8" EtiWidth="250px" DonWidth="400px" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <Virtualia:VDuoEtiquetteCommande ID="Dontab03" runat="server" EtiBackColor="#CBF2BE" V_PointdeVue="1" V_Objet="48" V_Information="3" V_SiDonneeDico="true" V_SiFenetrePer_Bis="true" DonTabIndex="9" EtiWidth="120px" DonWidth="150px" />
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Left">
                                    <Virtualia:VDuoEtiquetteCommande ID="Dontab05" runat="server" EtiBackColor="#CBF2BE" V_PointdeVue="1" V_Objet="48" V_Information="5" V_SiDonneeDico="true" V_SiFenetrePer_Bis="true" DonTabIndex="10" EtiWidth="120px" DonWidth="250px" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left" ColumnSpan="2">
                                    <Virtualia:VCocheSimple ID="Coche12" runat="server" V_BackColor="#CBF2BE" V_PointdeVue="1" V_Objet="48" V_Information="12" V_SiDonneeDico="true" V_SiFenetrePer_Bis="true" V_Width="250px" V_Style="margin-left:4px" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left" ColumnSpan="2">
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH13" runat="server" EtiBackColor="#CBF2BE" V_PointdeVue="1" V_Objet="48" V_Information="13" V_SiDonneeDico="true" V_SiFenetrePer_Bis="true" DonWidth="450px" DonTabIndex="11" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left" ColumnSpan="2">
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH14" runat="server" EtiBackColor="#CBF2BE" V_PointdeVue="1" V_Objet="48" V_Information="14" V_SiDonneeDico="true" V_SiFenetrePer_Bis="true" DonWidth="450px" DonTabIndex="12" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow Height="40px">
                                <asp:TableCell HorizontalAlign="Center" ColumnSpan="2">
                                    <asp:Label ID="LabelEtiTextes" runat="server" Height="20px" Width="350px" Text="Textes applicables" BackColor="#CBF2BE" BorderColor="#B0E0D7" BorderStyle="Outset" BorderWidth="2px" ForeColor="#142425" Font-Italic="true" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left" ColumnSpan="2">
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH07" runat="server" EtiBackColor="#CBF2BE" V_PointdeVue="1" V_Objet="48" V_Information="7" V_SiDonneeDico="true" V_SiFenetrePer_Bis="true" DonWidth="450px" DonTabIndex="13" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH16" runat="server" EtiBackColor="#CBF2BE" V_PointdeVue="1" V_Objet="48" V_Information="16" V_SiDonneeDico="true" V_SiFenetrePer_Bis="true" DonWidth="120px" DonTabIndex="14" />
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Left">
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH15" runat="server" EtiBackColor="#CBF2BE" V_PointdeVue="1" V_Objet="48" V_Information="15" V_SiDonneeDico="true" V_SiFenetrePer_Bis="true" DonWidth="80px" DonTabIndex="15" DonStyle="margin-left:2px" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Height="10px" ColumnSpan="2" />
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center" ColumnSpan="2">
                                    <Virtualia:VCoupleVerticalEtiDonnee ID="InfoV11" runat="server" DonTextMode="true" V_PointdeVue="1" V_Objet="48" V_Information="11" V_SiDonneeDico="true" V_SiFenetrePer_Bis="true" EtiWidth="652px" DonWidth="650px" DonHeight="100px" EtiBackColor="#CBF2BE" DonTabIndex="16" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Height="12px" ColumnSpan="2" />
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>
</asp:Table>
