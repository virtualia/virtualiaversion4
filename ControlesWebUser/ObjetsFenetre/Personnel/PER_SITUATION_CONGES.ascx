﻿<%@ Control Language="VB" AutoEventWireup="false" Inherits="Virtualia.Net.Fenetre_PER_SITUATION_CONGES" Codebehind="PER_SITUATION_CONGES.ascx.vb" %>

<asp:Table ID="CadreInfo" runat="server" BorderStyle="None" BorderWidth="2px" Visible="true"
  BorderColor="#B0E0D7" Height="175px" Width="750px" HorizontalAlign="Center" style="margin-top: 3px;"> 
  <asp:TableRow>
    <asp:TableCell>
        <asp:Table ID="TableauEnteteSituation" runat="server" CellPadding="0" CellSpacing="0" Width="750px"
            BorderStyle="Notset" BorderWidth="1px" BorderColor="#D7FAF3" HorizontalAlign="Left" Visible="true">
            <asp:TableRow ID="LigneEnteteTitre" Height="25px">
                <asp:TableCell HorizontalAlign="Center" ColumnSpan="4">
                    <asp:Label ID="LabelTitreTableau" runat="server" Height="24px" Width="746px"
                        BackColor="#CAEBE4" BorderColor="#D7FAF3"  BorderStyle="Notset"
                        BorderWidth="1px" ForeColor="#1C5151" Text="Situation et reliquats"
                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                        style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                        font-style: normal; text-indent: 0px; text-align: center; padding-top: 3px;">
                    </asp:Label>          
                </asp:TableCell>      
            </asp:TableRow>
            <asp:TableRow ID="LigneEnteteConges" Height="25px">
             <asp:TableCell ID="CellEnteteConges" HorizontalAlign="Center"> 
              <asp:Label ID="LabelEnteteConges" runat="server" Height="24px" Width="200px"
                  BackColor="#A8BBB8" BorderColor="#D7FAF3" BorderStyle="Solid" Text="..."
                  BorderWidth="1px" ForeColor="#A8BBB8" Font-Italic="False"
                  Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                  style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                  font-style: normal; text-indent: 0px; text-align:  center; padding-top: 3px;">
              </asp:Label>          
             </asp:TableCell>
             <asp:TableCell ID="CellEnteteDroits" HorizontalAlign="Center"> 
              <asp:Label ID="LabelEnteteDroits" runat="server" Height="24px" Width="240px" 
                  BackColor="#A8BBB8" BorderColor="#D7FAF3" BorderStyle="Solid" Text="Droits"
                  BorderWidth="1px" ForeColor="#D7FAF3" Font-Italic="False"
                  Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                  style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                  font-style: normal; text-indent: 0px;  text-align:  center; padding-top: 3px;">
              </asp:Label>          
             </asp:TableCell>
             <asp:TableCell ID="CellEnteteCongesPris" HorizontalAlign="Center"> 
              <asp:Label ID="LabelEnteteCongesPris" runat="server" Height="24px" Width="225px"
                  BackColor="#A8BBB8" BorderColor="#D7FAF3" BorderStyle="Solid" Text="Congés"
                  BorderWidth="1px" ForeColor="#D7FAF3" Font-Italic="False"
                  Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                  style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; 
                  font-style: normal; text-indent: 0px; text-align:  center; padding-top: 3px;">
              </asp:Label>          
             </asp:TableCell>
             <asp:TableCell ID="CellEnteteSolde" HorizontalAlign="Center"> 
              <asp:Label ID="LabelEnteteSolde" runat="server" Height="24px" Width="75px"
                  BackColor="#A8BBB8" BorderColor="#D7FAF3" BorderStyle="Solid" Text="Solde"
                  BorderWidth="1px" ForeColor="#D7FAF3" Font-Italic="False"
                  Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                  style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; 
                  font-style: normal; text-indent: 0px; text-align:  center; padding-top: 3px;">
              </asp:Label>          
             </asp:TableCell>
           </asp:TableRow>
        </asp:Table>
    </asp:TableCell>
  </asp:TableRow>
  
  <asp:TableRow>
    <asp:TableCell>
        <asp:Table ID="TableauSituationConges" runat="server" CellPadding="0" CellSpacing="0" Width="750px"
             BorderStyle="Notset" BorderWidth="1px" BorderColor="#B0E0D7" HorizontalAlign="Left" Visible="true">
            <asp:TableRow ID="LigneSituationConges" Height="22px">
             <asp:TableCell ID="CellTypeConge" HorizontalAlign="Center"> 
              <asp:Label ID="LabelTypeConge" runat="server" Height="21px" Width="200px"
                  BackColor="#6C9690" BorderColor="#CAEBE4" BorderStyle="Solid" Text=""
                  BorderWidth="1px" ForeColor="White" Font-Italic="False"
                  Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                  style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                  font-style: normal; text-indent: 0px; text-align:  center; padding-top: 1px;">
              </asp:Label>          
             </asp:TableCell>
             <asp:TableCell ID="CellDroitsConges" HorizontalAlign="Center"> 
              <asp:Label ID="LabelDroitsConges" runat="server" Height="21px" Width="240px" 
                  BackColor="White" BorderColor="#CAEBE4" BorderStyle="Solid" Text=""
                  BorderWidth="1px" ForeColor="#1C5151" Font-Italic="False"
                  Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                  style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                  font-style: normal; text-indent: 0px;  text-align:  center; padding-top: 1px;">
              </asp:Label>          
             </asp:TableCell>
             <asp:TableCell ID="CellTitreCongesPris" HorizontalAlign="Center"> 
              <asp:Label ID="LabelTitreCongesPris" runat="server" Height="21px" Width="46px"
                  BackColor="#CAEBE4" BorderColor="#CAEBE4" BorderStyle="Solid" Text="pris"
                  BorderWidth="1px" ForeColor="#6C9690" Font-Italic="False"
                  Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                  style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; 
                  font-style: normal; text-indent: 0px; text-align:  center; padding-top: 1px;">
              </asp:Label>
             </asp:TableCell> 
             <asp:TableCell ID="CellCongesPris" HorizontalAlign="Center"> 
              <asp:Label ID="LabelCongesPris" runat="server" Height="21px" Width="61px"
                  BackColor="White" BorderColor="#CAEBE4" BorderStyle="Solid" Text=""
                  BorderWidth="1px" ForeColor="#1C5151" Font-Italic="False"
                  Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                  style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; 
                  font-style: normal; text-indent: 0px; text-align:  center; padding-top: 1px;">
              </asp:Label>          
             </asp:TableCell>
             <asp:TableCell ID="CellTitreCongesAvenir" HorizontalAlign="Center"> 
              <asp:Label ID="LabelTitreCongesAvenir" runat="server" Height="21px" Width="51px"
                  BackColor="#CAEBE4" BorderColor="#CAEBE4" BorderStyle="Solid" Text="à venir"
                  BorderWidth="1px" ForeColor="#6C9690" Font-Italic="False"
                  Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                  style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; 
                  font-style: normal; text-indent: 0px; text-align:  center; padding-top: 1px;">
              </asp:Label>          
             </asp:TableCell>
             <asp:TableCell ID="CellCongesAvenir" HorizontalAlign="Center"> 
              <asp:Label ID="LabelCongesAvenir" runat="server" Height="21px" Width="61px"
                  BackColor="White" BorderColor="#CAEBE4" BorderStyle="Solid" Text=""
                  BorderWidth="1px" ForeColor="#1C5151" Font-Italic="False"
                  Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                  style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; 
                  font-style: normal; text-indent: 0px; text-align:  center; padding-top: 1px;">
              </asp:Label>          
             </asp:TableCell>
             <asp:TableCell ID="CellSoldeConges" HorizontalAlign="Center"> 
              <asp:Label ID="LabelSoldeConges" runat="server" Height="21px" Width="75px"
                  BackColor="White" BorderColor="#CAEBE4" BorderStyle="Solid" Text=""
                  BorderWidth="1px" ForeColor="#1C5151" Font-Italic="False"
                  Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                  style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; 
                  font-style: normal; text-indent: 0px; text-align:  center; padding-top: 1px;">
              </asp:Label>          
             </asp:TableCell>
           </asp:TableRow>
           <asp:TableRow ID="LigneDetailsConges" Height="18px">
             <asp:TableCell ID="CellDetailsConges" HorizontalAlign="Center"> 
              <asp:Label ID="LabelDetailsConges" runat="server" Height="17px" Width="200px"
                  BackColor="#f0f0f0" BorderColor="#CAEBE4" BorderStyle="Solid" Text=""
                  BorderWidth="1px" ForeColor="#336562" Font-Italic="False"
                  Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="X-Small"
                  style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                  font-style: normal; text-indent: 0px; text-align:  center; padding-top: 1px;">
              </asp:Label>          
             </asp:TableCell>
             <asp:TableCell ID="CellDetailsDroitsConges" HorizontalAlign="Center"> 
              <asp:Label ID="LabelDetailsDroitsConges" runat="server" Height="17px" Width="240px" 
                  BackColor="#f0f0f0" BorderColor="#CAEBE4" BorderStyle="Solid" Text=""
                  BorderWidth="1px" ForeColor="#336562" Font-Italic="False"
                  Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="X-Small"
                  style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                  font-style: normal; text-indent: 0px;  text-align:  center; padding-top: 1px;">
              </asp:Label>          
             </asp:TableCell>
             <asp:TableCell ID="CellDetailsCongesPris" HorizontalAlign="Center" ColumnSpan="4"> 
              <asp:Label ID="LabelDetailsCongesPris" runat="server" Height="17px" Width="225px"
                  BackColor="#f0f0f0" BorderColor="#CAEBE4" BorderStyle="Solid" Text=""
                  BorderWidth="1px" ForeColor="#336562" Font-Italic="False"
                  Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="X-Small"
                  style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; 
                  font-style: normal; text-indent: 0px; text-align:  center; padding-top: 1px;">
              </asp:Label>          
             </asp:TableCell>
             <asp:TableCell ID="CellDetailSoldeConges" HorizontalAlign="Center"> 
              <asp:Label ID="LabelDetailSoldeConges" runat="server" Height="17px" Width="75px"
                  BackColor="#f0f0f0" BorderColor="#CAEBE4" BorderStyle="Solid" Text="..."
                  BorderWidth="1px" ForeColor="#f0f0f0" Font-Italic="False"
                  Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="X-Small"
                  style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; 
                  font-style: normal; text-indent: 0px; text-align:  center; padding-top: 1px;">
              </asp:Label>          
             </asp:TableCell>
           </asp:TableRow>
        </asp:Table>
    </asp:TableCell>
  </asp:TableRow>

  <asp:TableRow>
    <asp:TableCell>
        <asp:Table ID="TableauSituationRTT" runat="server" CellPadding="0" CellSpacing="0" Width="750px"
        BorderStyle="Notset" BorderWidth="1px" BorderColor="#B0E0D7" HorizontalAlign="Left" Visible="true">
            <asp:TableRow ID="LigneSituationRTT" Height="22px">
             <asp:TableCell ID="CellTypeRTT" HorizontalAlign="Center"> 
              <asp:Label ID="LabelTypeRTT" runat="server" Height="21px" Width="200px"
                  BackColor="#6C9690" BorderColor="#CAEBE4" BorderStyle="Solid" Text=""
                  BorderWidth="1px" ForeColor="White" Font-Italic="False"
                  Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                  style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                  font-style: normal; text-indent: 0px; text-align:  center; padding-top: 1px;">
              </asp:Label>          
             </asp:TableCell>
             <asp:TableCell ID="CellDroitsRTT" HorizontalAlign="Center"> 
              <asp:Label ID="LabelDroitsRTT" runat="server" Height="21px" Width="240px" 
                  BackColor="White" BorderColor="#CAEBE4" BorderStyle="Solid" Text=""
                  BorderWidth="1px" ForeColor="#1C5151" Font-Italic="False"
                  Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                  style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                  font-style: normal; text-indent: 0px;  text-align:  center; padding-top: 1px;">
              </asp:Label>          
             </asp:TableCell>
             <asp:TableCell ID="CellTitreRTTpris" HorizontalAlign="Center"> 
              <asp:Label ID="LabelTitreRTTpris" runat="server" Height="21px" Width="46px"
                  BackColor="#CAEBE4" BorderColor="#CAEBE4" BorderStyle="Solid" Text="pris"
                  BorderWidth="1px" ForeColor="#6C9690" Font-Italic="False"
                  Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                  style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; 
                  font-style: normal; text-indent: 0px; text-align:  center; padding-top: 1px;">
              </asp:Label>          
             </asp:TableCell>
             <asp:TableCell ID="CellRTTpris" HorizontalAlign="Center"> 
              <asp:Label ID="LabelRTTpris" runat="server" Height="21px" Width="61px"
                  BackColor="White" BorderColor="#CAEBE4" BorderStyle="Solid" Text=""
                  BorderWidth="1px" ForeColor="#1C5151" Font-Italic="False"
                  Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                  style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; 
                  font-style: normal; text-indent: 0px; text-align:  center; padding-top: 1px;">
              </asp:Label>          
             </asp:TableCell>
             <asp:TableCell ID="CellTitreRTTAvenir" HorizontalAlign="Center"> 
              <asp:Label ID="LabelTitreRTTAvenir" runat="server" Height="21px" Width="51px"
                  BackColor="#CAEBE4" BorderColor="#CAEBE4" BorderStyle="Solid" Text="à venir"
                  BorderWidth="1px" ForeColor="#6C9690" Font-Italic="False"
                  Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                  style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; 
                  font-style: normal; text-indent: 0px; text-align:  center; padding-top: 1px;">
              </asp:Label>          
             </asp:TableCell>
             <asp:TableCell ID="CellRTTAvenir" HorizontalAlign="Center"> 
              <asp:Label ID="LabelRTTAvenir" runat="server" Height="21px" Width="61px"
                  BackColor="White" BorderColor="#CAEBE4" BorderStyle="Solid" Text=""
                  BorderWidth="1px" ForeColor="#1C5151" Font-Italic="False"
                  Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                  style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; 
                  font-style: normal; text-indent: 0px; text-align:  center; padding-top: 1px;">
              </asp:Label>          
             </asp:TableCell>
             <asp:TableCell ID="CellSoldeRTT" HorizontalAlign="Center"> 
              <asp:Label ID="LabelSoldeRTT" runat="server" Height="21px" Width="75px"
                  BackColor="White" BorderColor="#CAEBE4" BorderStyle="Solid" Text=""
                  BorderWidth="1px" ForeColor="#1C5151" Font-Italic="False"
                  Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                  style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; 
                  font-style: normal; text-indent: 0px; text-align:  center; padding-top: 1px;">
              </asp:Label>          
             </asp:TableCell>
           </asp:TableRow>
           <asp:TableRow ID="LigneDetailsRTT" Height="18px">
             <asp:TableCell ID="CellDetailsRTT" HorizontalAlign="Center"> 
              <asp:Label ID="LabelDetailsRTT" runat="server" Height="17px" Width="200px"
                  BackColor="#f0f0f0" BorderColor="#CAEBE4" BorderStyle="Solid" Text=""
                  BorderWidth="1px" ForeColor="#336562" Font-Italic="False"
                  Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="X-Small"
                  style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                  font-style: normal; text-indent: 0px; text-align:  center; padding-top: 1px;">
              </asp:Label>          
             </asp:TableCell>
             <asp:TableCell ID="CellDetailsDroitsRTT" HorizontalAlign="Center"> 
              <asp:Label ID="LabelDetailsDroitsRTT" runat="server" Height="17px" Width="240px" 
                  BackColor="#f0f0f0" BorderColor="#CAEBE4" BorderStyle="Solid" Text=""
                  BorderWidth="1px" ForeColor="#336562" Font-Italic="False"
                  Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="X-Small"
                  style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                  font-style: normal; text-indent: 0px;  text-align:  center; padding-top: 1px;">
              </asp:Label>          
             </asp:TableCell>
             <asp:TableCell ID="CellDetailsRTTpris" HorizontalAlign="Center" ColumnSpan="4"> 
              <asp:Label ID="LabelDetailsRTTpris" runat="server" Height="17px" Width="225px"
                  BackColor="#f0f0f0" BorderColor="#CAEBE4" BorderStyle="Solid" Text=""
                  BorderWidth="1px" ForeColor="#336562" Font-Italic="False"
                  Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="X-Small"
                  style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; 
                  font-style: normal; text-indent: 0px; text-align:  center; padding-top: 1px;">
              </asp:Label>          
             </asp:TableCell>
             <asp:TableCell ID="CellDetailsSoldeRTT" HorizontalAlign="Center"> 
              <asp:Label ID="LabelDetailsSoldeRTT" runat="server" Height="17px" Width="75px"
                  BackColor="#f0f0f0" BorderColor="#CAEBE4" BorderStyle="Solid" Text="..."
                  BorderWidth="1px" ForeColor="#f0f0f0" Font-Italic="False"
                  Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="X-Small"
                  style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; 
                  font-style: normal; text-indent: 0px; text-align:  center; padding-top: 1px;">
              </asp:Label>          
             </asp:TableCell>
           </asp:TableRow>
        </asp:Table>
    </asp:TableCell>
  </asp:TableRow>
  
  <asp:TableRow>
    <asp:TableCell>
        <asp:Table ID="TableauSituationCompteur1" runat="server" CellPadding="0" CellSpacing="0" Width="750px"
        BorderStyle="Notset" BorderWidth="1px" BorderColor="#B0E0D7" HorizontalAlign="Left" Visible="False">
            <asp:TableRow ID="LigneSituationCompteur1" Height="22px">
             <asp:TableCell ID="CellTypeCompteur1" HorizontalAlign="Center"> 
              <asp:Label ID="LabelTypeCompteur1" runat="server" Height="21px" Width="200px"
                  BackColor="#6C9690" BorderColor="#CAEBE4" BorderStyle="Solid" Text=""
                  BorderWidth="1px" ForeColor="White" Font-Italic="False"
                  Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                  style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                  font-style: normal; text-indent: 0px; text-align:  center; padding-top: 1px;">
              </asp:Label>          
             </asp:TableCell>
             <asp:TableCell ID="CellDroitsCompteur1" HorizontalAlign="Center"> 
              <asp:Label ID="LabelDroitsCompteur1" runat="server" Height="21px" Width="240px" 
                  BackColor="White" BorderColor="#CAEBE4" BorderStyle="Solid" Text=""
                  BorderWidth="1px" ForeColor="#1C5151" Font-Italic="False"
                  Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                  style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                  font-style: normal; text-indent: 0px;  text-align:  center; padding-top: 1px;">
              </asp:Label>          
             </asp:TableCell>
             <asp:TableCell ID="CellTitreCompteur1pris" HorizontalAlign="Center"> 
              <asp:Label ID="LabelTitreCompteur1pris" runat="server" Height="21px" Width="46px"
                  BackColor="#CAEBE4" BorderColor="#CAEBE4" BorderStyle="Solid" Text="pris"
                  BorderWidth="1px" ForeColor="#6C9690" Font-Italic="False"
                  Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                  style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; 
                  font-style: normal; text-indent: 0px; text-align:  center; padding-top: 1px;">
              </asp:Label>          
             </asp:TableCell>
             <asp:TableCell ID="CellCompteur1pris" HorizontalAlign="Center"> 
              <asp:Label ID="LabelCompteur1pris" runat="server" Height="21px" Width="61px"
                  BackColor="White" BorderColor="#CAEBE4" BorderStyle="Solid" Text=""
                  BorderWidth="1px" ForeColor="#1C5151" Font-Italic="False"
                  Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                  style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; 
                  font-style: normal; text-indent: 0px; text-align:  center; padding-top: 1px;">
              </asp:Label>          
             </asp:TableCell>
             <asp:TableCell ID="CellTitreCompteur1Avenir" HorizontalAlign="Center"> 
              <asp:Label ID="LabelTitreCompteur1Avenir" runat="server" Height="21px" Width="51px"
                  BackColor="#CAEBE4" BorderColor="#CAEBE4" BorderStyle="Solid" Text="à venir"
                  BorderWidth="1px" ForeColor="#6C9690" Font-Italic="False"
                  Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                  style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; 
                  font-style: normal; text-indent: 0px; text-align:  center; padding-top: 1px;">
              </asp:Label>          
             </asp:TableCell>
             <asp:TableCell ID="CellCompteur1Avenir" HorizontalAlign="Center"> 
              <asp:Label ID="LabelCompteur1Avenir" runat="server" Height="21px" Width="61px"
                  BackColor="White" BorderColor="#CAEBE4" BorderStyle="Solid" Text=""
                  BorderWidth="1px" ForeColor="#1C5151" Font-Italic="False"
                  Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                  style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; 
                  font-style: normal; text-indent: 0px; text-align:  center; padding-top: 1px;">
              </asp:Label>          
             </asp:TableCell>
             <asp:TableCell ID="CellSoldeCompteur1" HorizontalAlign="Center"> 
              <asp:Label ID="LabelSoldeCompteur1" runat="server" Height="21px" Width="75px"
                  BackColor="White" BorderColor="#CAEBE4" BorderStyle="Solid" Text=""
                  BorderWidth="1px" ForeColor="#1C5151" Font-Italic="False"
                  Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                  style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; 
                  font-style: normal; text-indent: 0px; text-align:  center; padding-top: 1px;">
              </asp:Label>          
             </asp:TableCell>
           </asp:TableRow>
           <asp:TableRow ID="LigneDetailsCompteur1" Height="18px">
             <asp:TableCell ID="CellDetailsCompteur1" HorizontalAlign="Center"> 
              <asp:Label ID="LabelDetailsCompteur1" runat="server" Height="17px" Width="200px"
                  BackColor="#f0f0f0" BorderColor="#CAEBE4" BorderStyle="Solid" Text=""
                  BorderWidth="1px" ForeColor="#336562" Font-Italic="False"
                  Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="X-Small"
                  style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                  font-style: normal; text-indent: 0px; text-align:  center; padding-top: 1px;">
              </asp:Label>          
             </asp:TableCell>
             <asp:TableCell ID="CellDetailsDroitsCompteur1" HorizontalAlign="Center"> 
              <asp:Label ID="LabelDetailsDroitsCompteur1" runat="server" Height="17px" Width="240px" 
                  BackColor="#f0f0f0" BorderColor="#CAEBE4" BorderStyle="Solid" Text=""
                  BorderWidth="1px" ForeColor="#336562" Font-Italic="False"
                  Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="X-Small"
                  style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                  font-style: normal; text-indent: 0px;  text-align:  center; padding-top: 1px;">
              </asp:Label>          
             </asp:TableCell>
             <asp:TableCell ID="CellDetailsCompteur1pris" HorizontalAlign="Center" ColumnSpan="4"> 
              <asp:Label ID="LabelDetailsCompteur1pris" runat="server" Height="17px" Width="225px"
                  BackColor="#f0f0f0" BorderColor="#CAEBE4" BorderStyle="Solid" Text=""
                  BorderWidth="1px" ForeColor="#336562" Font-Italic="False"
                  Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="X-Small"
                  style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; 
                  font-style: normal; text-indent: 0px; text-align:  center; padding-top: 1px;">
              </asp:Label>          
             </asp:TableCell>
             <asp:TableCell ID="CellDetailsSoldeCompteur1" HorizontalAlign="Center"> 
              <asp:Label ID="LabelDetailsSoldeCompteur1" runat="server" Height="17px" Width="75px"
                  BackColor="#f0f0f0" BorderColor="#CAEBE4" BorderStyle="Solid" Text="..."
                  BorderWidth="1px" ForeColor="#f0f0f0" Font-Italic="False"
                  Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="X-Small"
                  style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; 
                  font-style: normal; text-indent: 0px; text-align:  center; padding-top: 1px;">
              </asp:Label>          
             </asp:TableCell>
           </asp:TableRow>
        </asp:Table>
    </asp:TableCell>
  </asp:TableRow> 

  <asp:TableRow>
    <asp:TableCell>
        <asp:Table ID="TableauSituationCompteur2" runat="server" CellPadding="0" CellSpacing="0" Width="750px"
        BorderStyle="Notset" BorderWidth="1px" BorderColor="#B0E0D7" HorizontalAlign="Left" Visible="False">
            <asp:TableRow ID="LigneSituationCompteur2" Height="22px">
             <asp:TableCell ID="CellTypeCompteur2" HorizontalAlign="Center"> 
              <asp:Label ID="LabelTypeCompteur2" runat="server" Height="21px" Width="200px"
                  BackColor="#6C9690" BorderColor="#CAEBE4" BorderStyle="Solid" Text=""
                  BorderWidth="1px" ForeColor="White" Font-Italic="False"
                  Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                  style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                  font-style: normal; text-indent: 0px; text-align:  center; padding-top: 1px;">
              </asp:Label>          
             </asp:TableCell>
             <asp:TableCell ID="CellDroitsCompteur2" HorizontalAlign="Center"> 
              <asp:Label ID="LabelDroitsCompteur2" runat="server" Height="21px" Width="240px" 
                  BackColor="White" BorderColor="#CAEBE4" BorderStyle="Solid" Text=""
                  BorderWidth="1px" ForeColor="#1C5151" Font-Italic="False"
                  Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                  style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                  font-style: normal; text-indent: 0px;  text-align:  center; padding-top: 1px;">
              </asp:Label>          
             </asp:TableCell>
             <asp:TableCell ID="CellTitreCompteur2pris" HorizontalAlign="Center"> 
              <asp:Label ID="LabelTitreCompteur2pris" runat="server" Height="21px" Width="46px"
                  BackColor="#CAEBE4" BorderColor="#CAEBE4" BorderStyle="Solid" Text="pris"
                  BorderWidth="1px" ForeColor="#6C9690" Font-Italic="False"
                  Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                  style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; 
                  font-style: normal; text-indent: 0px; text-align:  center; padding-top: 1px;">
              </asp:Label>          
             </asp:TableCell>
             <asp:TableCell ID="CellCompteur2pris" HorizontalAlign="Center"> 
              <asp:Label ID="LabelCompteur2pris" runat="server" Height="21px" Width="61px"
                  BackColor="White" BorderColor="#CAEBE4" BorderStyle="Solid" Text=""
                  BorderWidth="1px" ForeColor="#1C5151" Font-Italic="False"
                  Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                  style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; 
                  font-style: normal; text-indent: 0px; text-align:  center; padding-top: 1px;">
              </asp:Label>          
             </asp:TableCell>
             <asp:TableCell ID="CellTitreCompteur2Avenir" HorizontalAlign="Center"> 
              <asp:Label ID="LabelTitreCompteur2Avenir" runat="server" Height="21px" Width="51px"
                  BackColor="#CAEBE4" BorderColor="#CAEBE4" BorderStyle="Solid" Text="à venir"
                  BorderWidth="1px" ForeColor="#6C9690" Font-Italic="False"
                  Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                  style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; 
                  font-style: normal; text-indent: 0px; text-align:  center; padding-top: 1px;">
              </asp:Label>          
             </asp:TableCell>
             <asp:TableCell ID="CellCompteur2Avenir" HorizontalAlign="Center"> 
              <asp:Label ID="LabelCompteur2Avenir" runat="server" Height="21px" Width="61px"
                  BackColor="White" BorderColor="#CAEBE4" BorderStyle="Solid" Text=""
                  BorderWidth="1px" ForeColor="#1C5151" Font-Italic="False"
                  Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                  style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; 
                  font-style: normal; text-indent: 0px; text-align:  center; padding-top: 1px;">
              </asp:Label>          
             </asp:TableCell>
             <asp:TableCell ID="CellSoldeCompteur2" HorizontalAlign="Center"> 
              <asp:Label ID="LabelSoldeCompteur2" runat="server" Height="21px" Width="75px"
                  BackColor="White" BorderColor="#CAEBE4" BorderStyle="Solid" Text=""
                  BorderWidth="1px" ForeColor="#1C5151" Font-Italic="False"
                  Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                  style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; 
                  font-style: normal; text-indent: 0px; text-align:  center; padding-top: 1px;">
              </asp:Label>          
             </asp:TableCell>
           </asp:TableRow>
           <asp:TableRow ID="LigneDetailsCompteur2" Height="18px">
             <asp:TableCell ID="CellDetailsCompteur2" HorizontalAlign="Center"> 
              <asp:Label ID="LabelDetailsCompteur2" runat="server" Height="17px" Width="200px"
                  BackColor="#f0f0f0" BorderColor="#CAEBE4" BorderStyle="Solid" Text=""
                  BorderWidth="1px" ForeColor="#336562" Font-Italic="False"
                  Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="X-Small"
                  style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                  font-style: normal; text-indent: 0px; text-align:  center; padding-top: 1px;">
              </asp:Label>          
             </asp:TableCell>
             <asp:TableCell ID="CellDetailsDroitsCompteur2" HorizontalAlign="Center"> 
              <asp:Label ID="LabelDetailsDroitsCompteur2" runat="server" Height="17px" Width="240px" 
                  BackColor="#f0f0f0" BorderColor="#CAEBE4" BorderStyle="Solid" Text=""
                  BorderWidth="1px" ForeColor="#336562" Font-Italic="False"
                  Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="X-Small"
                  style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                  font-style: normal; text-indent: 0px;  text-align:  center; padding-top: 1px;">
              </asp:Label>          
             </asp:TableCell>
             <asp:TableCell ID="CellDetailsCompteur2pris" HorizontalAlign="Center" ColumnSpan="4"> 
              <asp:Label ID="LabelDetailsCompteur2pris" runat="server" Height="17px" Width="225px"
                  BackColor="#f0f0f0" BorderColor="#CAEBE4" BorderStyle="Solid" Text=""
                  BorderWidth="1px" ForeColor="#336562" Font-Italic="False"
                  Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="X-Small"
                  style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; 
                  font-style: normal; text-indent: 0px; text-align:  center; padding-top: 1px;">
              </asp:Label>          
             </asp:TableCell>
             <asp:TableCell ID="CellDetailsSoldeCompteur2" HorizontalAlign="Center"> 
              <asp:Label ID="LabelDetailsSoldeCompteur2" runat="server" Height="17px" Width="75px"
                  BackColor="#f0f0f0" BorderColor="#CAEBE4" BorderStyle="Solid" Text="..."
                  BorderWidth="1px" ForeColor="#f0f0f0" Font-Italic="False"
                  Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="X-Small"
                  style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; 
                  font-style: normal; text-indent: 0px; text-align:  center; padding-top: 1px;">
              </asp:Label>          
             </asp:TableCell>
           </asp:TableRow>
        </asp:Table>
    </asp:TableCell>
  </asp:TableRow>

  <asp:TableRow>
    <asp:TableCell>
        <asp:Table ID="TableauSituationCompteur3" runat="server" CellPadding="0" CellSpacing="0" Width="750px"
        BorderStyle="Notset" BorderWidth="1px" BorderColor="#B0E0D7" HorizontalAlign="Left" Visible="False">
            <asp:TableRow ID="LigneSituationCompteur3" Height="22px">
             <asp:TableCell ID="CellTypeCompteur3" HorizontalAlign="Center"> 
              <asp:Label ID="LabelTypeCompteur3" runat="server" Height="21px" Width="200px"
                  BackColor="#6C9690" BorderColor="#CAEBE4" BorderStyle="Solid" Text=""
                  BorderWidth="1px" ForeColor="White" Font-Italic="False"
                  Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                  style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                  font-style: normal; text-indent: 0px; text-align:  center; padding-top: 1px;">
              </asp:Label>          
             </asp:TableCell>
             <asp:TableCell ID="CellDroitsCompteur3" HorizontalAlign="Center"> 
              <asp:Label ID="LabelDroitsCompteur3" runat="server" Height="21px" Width="240px" 
                  BackColor="White" BorderColor="#CAEBE4" BorderStyle="Solid" Text=""
                  BorderWidth="1px" ForeColor="#1C5151" Font-Italic="False"
                  Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                  style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                  font-style: normal; text-indent: 0px;  text-align:  center; padding-top: 1px;">
              </asp:Label>          
             </asp:TableCell>
             <asp:TableCell ID="CellTitreCompteur3pris" HorizontalAlign="Center"> 
              <asp:Label ID="LabelTitreCompteur3pris" runat="server" Height="21px" Width="46px"
                  BackColor="#CAEBE4" BorderColor="#CAEBE4" BorderStyle="Solid" Text="pris"
                  BorderWidth="1px" ForeColor="#6C9690" Font-Italic="False"
                  Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                  style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; 
                  font-style: normal; text-indent: 0px; text-align:  center; padding-top: 1px;">
              </asp:Label>          
             </asp:TableCell>
             <asp:TableCell ID="CellCompteur3pris" HorizontalAlign="Center"> 
              <asp:Label ID="LabelCompteur3pris" runat="server" Height="21px" Width="61px"
                  BackColor="White" BorderColor="#CAEBE4" BorderStyle="Solid" Text=""
                  BorderWidth="1px" ForeColor="#1C5151" Font-Italic="False"
                  Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                  style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; 
                  font-style: normal; text-indent: 0px; text-align:  center; padding-top: 1px;">
              </asp:Label>          
             </asp:TableCell>
             <asp:TableCell ID="CellTitreCompteur3Avenir" HorizontalAlign="Center"> 
              <asp:Label ID="LabelTitreCompteur3Avenir" runat="server" Height="21px" Width="51px"
                  BackColor="#CAEBE4" BorderColor="#CAEBE4" BorderStyle="Solid" Text="à venir"
                  BorderWidth="1px" ForeColor="#6C9690" Font-Italic="False"
                  Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                  style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; 
                  font-style: normal; text-indent: 0px; text-align:  center; padding-top: 1px;">
              </asp:Label>          
             </asp:TableCell>
             <asp:TableCell ID="CellCompteur3Avenir" HorizontalAlign="Center"> 
              <asp:Label ID="LabelCompteur3Avenir" runat="server" Height="21px" Width="61px"
                  BackColor="White" BorderColor="#CAEBE4" BorderStyle="Solid" Text=""
                  BorderWidth="1px" ForeColor="#1C5151" Font-Italic="False"
                  Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                  style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; 
                  font-style: normal; text-indent: 0px; text-align:  center; padding-top: 1px;">
              </asp:Label>          
             </asp:TableCell>
             <asp:TableCell ID="CellSoldeCompteur3" HorizontalAlign="Center"> 
              <asp:Label ID="LabelSoldeCompteur3" runat="server" Height="21px" Width="75px"
                  BackColor="White" BorderColor="#CAEBE4" BorderStyle="Solid" Text=""
                  BorderWidth="1px" ForeColor="#1C5151" Font-Italic="False"
                  Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                  style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; 
                  font-style: normal; text-indent: 0px; text-align:  center; padding-top: 1px;">
              </asp:Label>          
             </asp:TableCell>
           </asp:TableRow>
           <asp:TableRow ID="LigneDetailsCompteur3" Height="18px">
             <asp:TableCell ID="CellDetailsCompteur3" HorizontalAlign="Center"> 
              <asp:Label ID="LabelDetailsCompteur3" runat="server" Height="17px" Width="200px"
                  BackColor="#f0f0f0" BorderColor="#CAEBE4" BorderStyle="Solid" Text=""
                  BorderWidth="1px" ForeColor="#336562" Font-Italic="False"
                  Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="X-Small"
                  style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                  font-style: normal; text-indent: 0px; text-align:  center; padding-top: 1px;">
              </asp:Label>          
             </asp:TableCell>
             <asp:TableCell ID="CellDetailsDroitsCompteur3" HorizontalAlign="Center"> 
              <asp:Label ID="LabelDetailsDroitsCompteur3" runat="server" Height="17px" Width="240px" 
                  BackColor="#f0f0f0" BorderColor="#CAEBE4" BorderStyle="Solid" Text=""
                  BorderWidth="1px" ForeColor="#336562" Font-Italic="False"
                  Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="X-Small"
                  style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                  font-style: normal; text-indent: 0px;  text-align:  center; padding-top: 1px;">
              </asp:Label>          
             </asp:TableCell>
             <asp:TableCell ID="CellDetailsCompteur3pris" HorizontalAlign="Center" ColumnSpan="4"> 
              <asp:Label ID="LabelDetailsCompteur3pris" runat="server" Height="17px" Width="225px"
                  BackColor="#f0f0f0" BorderColor="#CAEBE4" BorderStyle="Solid" Text=""
                  BorderWidth="1px" ForeColor="#336562" Font-Italic="False"
                  Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="X-Small"
                  style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; 
                  font-style: normal; text-indent: 0px; text-align:  center; padding-top: 1px;">
              </asp:Label>          
             </asp:TableCell>
             <asp:TableCell ID="CellDetailsSoldeCompteur3" HorizontalAlign="Center"> 
              <asp:Label ID="LabelDetailsSoldeCompteur3" runat="server" Height="17px" Width="75px"
                  BackColor="#f0f0f0" BorderColor="#CAEBE4" BorderStyle="Solid" Text="..."
                  BorderWidth="1px" ForeColor="#f0f0f0" Font-Italic="False"
                  Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="X-Small"
                  style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; 
                  font-style: normal; text-indent: 0px; text-align:  center; padding-top: 1px;">
              </asp:Label>          
             </asp:TableCell>
           </asp:TableRow>
        </asp:Table>
    </asp:TableCell>
  </asp:TableRow>

</asp:Table>