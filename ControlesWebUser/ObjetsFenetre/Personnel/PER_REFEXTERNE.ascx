﻿<%@ Control Language="VB" AutoEventWireup="false" Inherits="Virtualia.Net.Fenetre_PER_REFEXTERNE" CodeBehind="PER_REFEXTERNE.ascx.vb" %>

<%@ Register Src="~/Controles/Saisies/VCoupleEtiDonnee.ascx" TagName="VCoupleEtiDonnee" TagPrefix="Virtualia" %>
<%@ Register Src="~/Controles/Saisies/VCoupleVerticalEtiDonnee.ascx" TagName="VCoupleVerticalEtiDonnee" TagPrefix="Virtualia" %>
<%@ Register Src="~/Controles/Saisies/VDuoEtiquetteCommande.ascx" TagName="VDuoEtiquetteCommande" TagPrefix="Virtualia" %>

<%@ Register Src="~/ControlsGeneriques/V_CommandeOK.ascx" TagName="VCmdOK" TagPrefix="Generic" %>

<asp:Table ID="CadreInfo" runat="server" BorderStyle="Ridge" BorderWidth="2px" BorderColor="#B0E0D7" Height="285px" Width="490px" HorizontalAlign="Center">
    <asp:TableRow>
        <asp:TableCell>
            <Generic:VCmdOK ID="CtlOK" runat="server" Height="22px" Width="70px" HorizontalAlign="Right" CadreStyle="margin-top: 3px; margin-right: 3px" />
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="CadreTitre" runat="server" Height="40px" CellPadding="0" CellSpacing="0" HorizontalAlign="Center">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="Etiquette" runat="server" Text="Références" Height="20px" Width="300px" BackColor="#2FA49B" BorderColor="#B0E0D7" BorderStyle="Groove" BorderWidth="2px" ForeColor="#D7FAF3" Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Small" Style="margin-top: 15px; margin-left: 4px; margin-bottom: 10px; font-style: oblique; text-indent: 5px; text-align: center;" />
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="Externe" runat="server" CellPadding="0" CellSpacing="0" Width="490px">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VCoupleEtiDonnee ID="InfoH01" runat="server" V_PointdeVue="1" V_Objet="23" V_Information="1" V_SiDonneeDico="true" EtiWidth="200px" DonWidth="150px" DonTabIndex="1" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VCoupleEtiDonnee ID="InfoH03" runat="server" V_PointdeVue="1" V_Objet="23" V_Information="3" V_SiDonneeDico="true" EtiWidth="200px" DonWidth="150px" DonTabIndex="2" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VDuoEtiquetteCommande ID="Dontab04" runat="server" V_PointdeVue="1" V_Objet="23" V_Information="4" V_SiDonneeDico="true" EtiWidth="200px" DonWidth="265px" DonTabIndex="3" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VDuoEtiquetteCommande ID="Dontab05" runat="server" V_PointdeVue="1" V_Objet="23" V_Information="5" V_SiDonneeDico="true" EtiWidth="200px" DonWidth="265px" DonTabIndex="4" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VCoupleEtiDonnee ID="InfoH06" runat="server" V_PointdeVue="1" V_Objet="23" V_Information="6" V_SiDonneeDico="true" EtiWidth="200px" DonWidth="150px" DonTabIndex="5" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VCoupleEtiDonnee ID="InfoH07" runat="server" V_PointdeVue="1" V_Objet="23" V_Information="7" V_SiDonneeDico="true" EtiWidth="200px" DonWidth="150px" DonTabIndex="6" />
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="CadreObs" runat="server" CellPadding="0" CellSpacing="0" Width="490px">
                <asp:TableRow>
                    <asp:TableCell>
                        <Virtualia:VCoupleVerticalEtiDonnee ID="InfoV02" runat="server" DonTextMode="false" V_PointdeVue="1" V_Objet="23" V_Information="2" V_SiDonneeDico="true" EtiWidth="472px" DonWidth="470px" DonHeight="20px" DonTabIndex="7" EtiStyle="margin-left: 4px;text-align:center" DonStyle="margin-left: 4px;" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="12px" />
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>
</asp:Table>
