﻿'------------------------------------------------------------------------------
' <généré automatiquement>
'     Ce code a été généré par un outil.
'
'     Les modifications apportées à ce fichier peuvent provoquer un comportement incorrect et seront perdues si
'     le code est régénéré.
' </généré automatiquement>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class Fenetre_PER_RETRAITE

    '''<summary>
    '''Contrôle CadreInfo.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CadreInfo As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle CtlOK.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CtlOK As Global.Virtualia.Net.V_CommandeOK

    '''<summary>
    '''Contrôle CadreTitre.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CadreTitre As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle EtiRetraite.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiRetraite As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle TableauRetraite.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents TableauRetraite As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle InfoH01.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoH01 As Global.Virtualia.Net.Controles_VCoupleEtiDonnee

    '''<summary>
    '''Contrôle Dontab02.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Dontab02 As Global.Virtualia.Net.Controles_VDuoEtiquetteCommande

    '''<summary>
    '''Contrôle InfoH07.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoH07 As Global.Virtualia.Net.Controles_VCoupleEtiDonnee

    '''<summary>
    '''Contrôle EtiAns.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiAns As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle InfoH08.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoH08 As Global.Virtualia.Net.Controles_VCoupleEtiDonnee

    '''<summary>
    '''Contrôle EtiPension.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiPension As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle InfoH03.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoH03 As Global.Virtualia.Net.Controles_VCoupleEtiDonnee

    '''<summary>
    '''Contrôle InfoH04.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoH04 As Global.Virtualia.Net.Controles_VCoupleEtiDonnee

    '''<summary>
    '''Contrôle InfoH05.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoH05 As Global.Virtualia.Net.Controles_VCoupleEtiDonnee

    '''<summary>
    '''Contrôle InfoH06.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoH06 As Global.Virtualia.Net.Controles_VCoupleEtiDonnee

    '''<summary>
    '''Contrôle EtiProlongation.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiProlongation As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle InfoH09.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoH09 As Global.Virtualia.Net.Controles_VCoupleEtiDonnee

    '''<summary>
    '''Contrôle InfoH10.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoH10 As Global.Virtualia.Net.Controles_VCoupleEtiDonnee

    '''<summary>
    '''Contrôle Dontab11.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Dontab11 As Global.Virtualia.Net.Controles_VDuoEtiquetteCommande

    '''<summary>
    '''Contrôle TableauRachatEtudes.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents TableauRachatEtudes As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle EtiRachatEtudes.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiRachatEtudes As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle EtiLiquid.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiLiquid As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle InfoV12.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoV12 As Global.Virtualia.Net.Controles_VCoupleVerticalEtiDonnee

    '''<summary>
    '''Contrôle InfoV13.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoV13 As Global.Virtualia.Net.Controles_VCoupleVerticalEtiDonnee

    '''<summary>
    '''Contrôle InfoV14.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoV14 As Global.Virtualia.Net.Controles_VCoupleVerticalEtiDonnee

    '''<summary>
    '''Contrôle InfoV15.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoV15 As Global.Virtualia.Net.Controles_VCoupleVerticalEtiDonnee

    '''<summary>
    '''Contrôle EtiAssur.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiAssur As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle InfoV16.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoV16 As Global.Virtualia.Net.Controles_VCoupleVerticalEtiDonnee

    '''<summary>
    '''Contrôle InfoV17.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoV17 As Global.Virtualia.Net.Controles_VCoupleVerticalEtiDonnee

    '''<summary>
    '''Contrôle InfoV18.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoV18 As Global.Virtualia.Net.Controles_VCoupleVerticalEtiDonnee

    '''<summary>
    '''Contrôle InfoV19.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoV19 As Global.Virtualia.Net.Controles_VCoupleVerticalEtiDonnee

    '''<summary>
    '''Contrôle EtiLiquAssur.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiLiquAssur As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle InfoV20.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoV20 As Global.Virtualia.Net.Controles_VCoupleVerticalEtiDonnee

    '''<summary>
    '''Contrôle InfoV21.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoV21 As Global.Virtualia.Net.Controles_VCoupleVerticalEtiDonnee

    '''<summary>
    '''Contrôle InfoV22.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoV22 As Global.Virtualia.Net.Controles_VCoupleVerticalEtiDonnee

    '''<summary>
    '''Contrôle InfoV23.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoV23 As Global.Virtualia.Net.Controles_VCoupleVerticalEtiDonnee
End Class
