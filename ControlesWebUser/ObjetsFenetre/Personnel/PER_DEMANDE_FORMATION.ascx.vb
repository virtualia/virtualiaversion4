﻿Option Strict On
Option Explicit On
Option Compare Text

Imports VI = Virtualia.Systeme.Constantes
Imports Virtualia.ObjetBase
Imports Virtualia.OutilsVisu
Imports Virtualia.TablesObjet.ShemaPER

Partial Class Fenetre_PER_DEMANDE_FORMATION
    Inherits ObjetWebControleBase(Of PER_DEMANDE_FORMATION)
    Implements IControlCrud

#Region "IControlCrud"
    Public ReadOnly Property ControlCrud As ICtlCommandeCrud Implements IControlCrud.CtlCrud
        Get
            Return Me.CtlCrud
        End Get
    End Property
#End Region

#Region "ObjetWebControleBase"
    Private _webfonction As WebFonctions
    Protected Overrides ReadOnly Property V_WebFonction As IWebFonctions
        Get
            If (_webfonction Is Nothing) Then
                _webfonction = New WebFonctions(Me)
            End If
            Return _webfonction
        End Get
    End Property

    Protected Overrides Function InitCadreInfo() As WebControl
        Return Me.CadreInfo
    End Function

    Protected Overrides Function InitControlDataGrid() As IControlDataGrid
        Return Me.ListeGrille
    End Function

    Protected Overrides Sub InitCacheIde()

        V_CacheIde.TypeVue = WebControlTypeVue.ListeGrid

        Dim captions As List(Of String) = New List(Of String)()
        captions.Add("Aucune demande")
        captions.Add("Une demande")
        captions.Add("demandes")

        Me.ListeGrille.InitCaptions(captions)

        Dim colonnes As ColonneDataGridCollection = New ColonneDataGridCollection()

        colonnes.Add(HorizontalAlign.Center, TypeData.Date, "date de la demande", 0)
        colonnes.Add(HorizontalAlign.Center, TypeData.Numeric, "objet", 1)
        colonnes.Add(HorizontalAlign.Center, TypeData.Numeric, "origine", 3)
        colonnes.Add(HorizontalAlign.Center, TypeData.Numeric, "suite donnée", 5)
        colonnes.Add(HorizontalAlign.Center, TypeData.Numeric, "échéance", 6)
        colonnes.Add(HorizontalAlign.Center, TypeData.Chaine, "Clef", 0, True)

        Me.ListeGrille.InitColonnes(colonnes)

    End Sub

    Protected Overrides Sub OnPreRender(e As EventArgs)
        MyBase.OnPreRender(e)

        CadreInfo.BackColor = _webfonction.CouleurCharte(V_CacheIde.PointDeVue, "Cadre")
        CadreInfo.BorderColor = _webfonction.CouleurCharte(V_CacheIde.PointDeVue, "Bordure")
        Etiquette.BackColor = _webfonction.CouleurCharte(V_CacheIde.PointDeVue, "Titre")
        Etiquette.ForeColor = _webfonction.CouleurCharte(V_CacheIde.PointDeVue, "Police_Claire")
        Etiquette.BorderColor = _webfonction.CouleurCharte(V_CacheIde.PointDeVue, "Bordure")
        EtiDate.BackColor = _webfonction.CouleurCharte(V_CacheIde.PointDeVue, "Etiquette_BackColor")
        EtiDate.ForeColor = _webfonction.CouleurCharte(V_CacheIde.PointDeVue, "Etiquette_ForeColor")
        EtiDate.BorderColor = _webfonction.CouleurCharte(V_CacheIde.PointDeVue, "Bordure")
        EtiDecision.BackColor = _webfonction.CouleurCharte(V_CacheIde.PointDeVue, "Etiquette_BackColor")
        EtiDecision.ForeColor = _webfonction.CouleurCharte(V_CacheIde.PointDeVue, "Etiquette_ForeColor")
        EtiDecision.BorderColor = _webfonction.CouleurCharte(V_CacheIde.PointDeVue, "Bordure")
        EtiIntranet.BackColor = _webfonction.CouleurCharte(V_CacheIde.PointDeVue, "Sous-Titre")
        EtiIntranet.ForeColor = _webfonction.CouleurCharte(V_CacheIde.PointDeVue, "Etiquette_ForeColor")
        EtiIntranet.BorderColor = _webfonction.CouleurCharte(V_CacheIde.PointDeVue, "Bordure")
        EtiValideur.BackColor = _webfonction.CouleurCharte(V_CacheIde.PointDeVue, "Etiquette_BackColor")
        EtiValideur.ForeColor = _webfonction.CouleurCharte(V_CacheIde.PointDeVue, "Etiquette_ForeColor")
        EtiValideur.BorderColor = _webfonction.CouleurCharte(V_CacheIde.PointDeVue, "Bordure")
    End Sub

    Protected Overrides Sub OnInit(e As System.EventArgs)
        MyBase.OnInit(e)

        Dim initwebfonction As IWebFonctions = V_WebFonction
    End Sub
#End Region

End Class
