﻿Option Strict On
Option Explicit On
Option Compare Text

Imports VI = Virtualia.Systeme.Constantes
Imports Virtualia.ObjetBase
Imports Virtualia.TablesObjet.ShemaPER
Imports Virtualia.OutilsVisu

Partial Class Fenetre_PER_BANQUE
    Inherits ObjetWebControleBase(Of PER_BANQUE)
    Implements IControlOK

#Region "IControlOK"
    Public ReadOnly Property CommandOK As ICtlCommandeOK Implements IControlOK.CtlOK
        Get
            Return Me.CtlOK
        End Get
    End Property
#End Region

#Region "ObjetWebControleBase"
    Private _webfonction As WebFonctions
    Protected Overrides ReadOnly Property V_WebFonction As IWebFonctions
        Get
            If (_webfonction Is Nothing) Then
                _webfonction = New WebFonctions(Me)
            End If
            Return _webfonction
        End Get
    End Property

    Protected Overrides Function InitCadreInfo() As WebControl
        Return Me.CadreInfo
    End Function

    Protected Overrides Sub InitCacheIde()
        V_CacheIde.TypeVue = WebControlTypeVue.Simple
    End Sub

    Protected Overrides Sub OnPreRender(e As System.EventArgs)
        MyBase.OnPreRender(e)

        CadreInfo.BackColor = _webfonction.CouleurCharte(V_CacheIde.PointDeVue, "Cadre")
        CadreInfo.BorderColor = _webfonction.CouleurCharte(V_CacheIde.PointDeVue, "Bordure")
        Etiquette.BackColor = _webfonction.CouleurCharte(V_CacheIde.PointDeVue, "Titre")
        Etiquette.ForeColor = _webfonction.CouleurCharte(V_CacheIde.PointDeVue, "Police_Claire")
        Etiquette.BorderColor = _webfonction.CouleurCharte(V_CacheIde.PointDeVue, "Bordure")
    End Sub

    Public Sub New()
        MyBase.New()
    End Sub
#End Region

#Region "Specifique"

    Protected Overrides Sub InitDesignControles()
        MyBase.InitDesignControles()

        If _webfonction.PointeurRegistre Is Nothing Then
            Exit Sub
        End If

        Dim LgAlpha As String = _webfonction.PointeurRegistre.Valeur("Fonctionnel", "Saisie", "Informations personnelles", "Longueur_RIB")
        If IsNumeric(LgAlpha) Then
            InfoV03.DonMaxLength = CInt(LgAlpha)
        End If
    End Sub

    Protected Overrides Sub OnInit(e As System.EventArgs)
        MyBase.OnInit(e)

        Dim initwebfonction As IWebFonctions = V_WebFonction
    End Sub

    Private Function SiErreurSpecifique() As List(Of String)
        If V_CacheIde.EstNothing Then
            Return Nothing
        End If
        Dim TabErreurs As New List(Of String)

        '** Contrôle de cohérence
        Dim CtlFiche As New PER_BANQUE()
        If V_CacheMaj(7).valeur <> "" Then
            CtlFiche.IBAN = V_CacheMaj(7).valeur
            If CtlFiche.SiOK_IBAN = False Then
                TabErreurs.Add("IBAN erroné.")
            End If
        End If
        If V_CacheMaj(3).valeur <> "" And V_CacheMaj(4).valeur <> "" Then
            CtlFiche.Code_Banque = V_CacheMaj(1).valeur
            CtlFiche.Code_Guichet = V_CacheMaj(2).valeur
            CtlFiche.NumeroduCompte = V_CacheMaj(3).valeur
            CtlFiche.CleRIB = V_CacheMaj(4).valeur
            If CtlFiche.SiOK_RIB = False Then
                TabErreurs.Add("Relevé d'Identité Bancaire erronée.")
            End If
        End If
        If TabErreurs.Count > 0 Then
            Return TabErreurs
        Else
            Return Nothing
        End If
    End Function

    Protected Overrides Sub CommandeOK_Click(ByVal sender As Object, ByVal e As System.EventArgs)

        Dim TableauErreur As List(Of String) = SiErreurSpecifique()

        If TableauErreur Is Nothing Then
            MyBase.CommandeOK_Click(sender, e)
            Return
        End If

        Dim msgs As ItemMessage = New ItemMessage()

        msgs.TitreMsg = "Contrôle préalable à l'enregistrement du RIB " & _
                            V_CacheMaj(1).valeur & Strings.Space(1) & V_CacheMaj(2).valeur & _
                            Strings.Space(1) & V_CacheMaj(3).valeur & Strings.Space(1) & V_CacheMaj(4).valeur & _
                            Strings.Space(1) & "IBAN" & Strings.Space(1) & V_CacheMaj(7).valeur
        msgs.Messages.AddRange(TableauErreur)

        AfficheMessageErreurSaisie(msgs)
    End Sub

#End Region

End Class
