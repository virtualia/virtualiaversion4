﻿<%@ Control Language="VB" AutoEventWireup="false" Inherits="Virtualia.Net.Fenetre_PER_STOCKOPTION" CodeBehind="PER_STOCKOPTION.ascx.vb" %>

<%@ Register Src="~/Controles/Saisies/VCoupleEtiDonnee.ascx" TagName="VCoupleEtiDonnee" TagPrefix="Virtualia" %>

<%@ Register Src="~/ControlsGeneriques/V_DataGrid.ascx" TagName="VDataGrid" TagPrefix="Generic" %>
<%@ Register Src="~/ControlsGeneriques/V_CommandeCRUD.ascx" TagName="VCrud" TagPrefix="Generic" %>

<asp:Table ID="CadreControle" runat="server" Height="30px" Width="380px" CellPadding="0" CellSpacing="0" HorizontalAlign="Center">
    <asp:TableRow>
        <asp:TableCell>
            <Generic:VDataGrid ID="ListeGrille" runat="server" CadreWidth="380px" SiColonneSelect="true" SiCaseAcocher="false" />
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="CadreInfo" runat="server" BorderStyle="Ridge" BorderWidth="2px" BorderColor="#B0E0D7" Height="235px" Width="400px" HorizontalAlign="Center" Style="margin-top: 3px;">
                <asp:TableRow>
                    <asp:TableCell>
                        <Generic:VCrud ID="CtlCrud" runat="server" Height="22px" Width="244px" HorizontalAlign="Right" CadreStyle="margin-top: 3px; margin-right: 3px" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Table ID="CadreTitre" runat="server" Height="40px" CellPadding="0" CellSpacing="0" HorizontalAlign="Center">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                    <asp:Label ID="Etiquette" runat="server" Text="Plan d'épargne en actions" Height="20px" Width="280px" BackColor="#2FA49B" BorderColor="#B0E0D7" BorderStyle="Groove" BorderWidth="2px" ForeColor="#D7FAF3" Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Small" Style="margin-top: 15px; margin-left: 4px; margin-bottom: 10px; font-style: oblique; text-indent: 5px; text-align: center;" />
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Table ID="CadreDon" runat="server" Height="22px" Width="400px" CellPadding="0" CellSpacing="0">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left" ColumnSpan="2">
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH00" runat="server" V_PointdeVue="1" V_Objet="55" V_Information="0" V_SiDonneeDico="true" EtiWidth="120px" DonWidth="80px" DonTabIndex="1" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Height="15px" ColumnSpan="2" />
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left" ColumnSpan="2">
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH01" runat="server" V_PointdeVue="1" V_Objet="55" V_Information="1" V_SiDonneeDico="true" EtiWidth="240px" DonWidth="80px" DonTabIndex="2" Etistyle="text-align: left; text-indent: 25px;" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Height="6px" ColumnSpan="2" />
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH02" runat="server" V_PointdeVue="1" V_Objet="55" V_Information="2" V_SiDonneeDico="true" EtiWidth="240px" DonWidth="80px" DonTabIndex="3" Etistyle="text-align: left; text-indent: 25px;" />
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Left">
                                    <asp:Label ID="LabelEuro" runat="server" Text="€" Height="20px" Width="20px" BackColor="Transparent" BorderStyle="none" BorderWidth="0px" ForeColor="#2FA49B" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Style="margin-top: 0px; margin-left: -60px; margin-bottom: 0px; font-style: oblique; text-indent: 2px; text-align: left;" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left" ColumnSpan="2">
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH03" runat="server" V_PointdeVue="1" V_Objet="55" V_Information="3" V_SiDonneeDico="true" EtiWidth="240px" DonWidth="50px" DonTabIndex="4" Etistyle="text-align: left; text-indent: 25px;" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Height="12px" ColumnSpan="2" />
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>
</asp:Table>
