﻿Option Strict On
Option Explicit On
Option Compare Text

Imports VI = Virtualia.Systeme.Constantes
Imports Virtualia.ObjetBase
Imports Virtualia.TablesObjet.ShemaPER
Imports Virtualia.OutilsVisu

Partial Class Fenetre_PER_PAIE_VACATION
    Inherits ObjetWebControleBase(Of PER_PAIE_VACATION)
    Implements IControlCrud

#Region "IControlCrud"
    Public ReadOnly Property ControlCrud As ICtlCommandeCrud Implements IControlCrud.CtlCrud
        Get
            Return Me.CtlCrud
        End Get
    End Property
#End Region

#Region "ObjetWebControleBase"
    Private _webfonction As WebFonctions
    Protected Overrides ReadOnly Property V_WebFonction As IWebFonctions
        Get
            If (_webfonction Is Nothing) Then
                _webfonction = New WebFonctions(Me)
            End If
            Return _webfonction
        End Get
    End Property

    Protected Overrides Function InitCadreInfo() As WebControl
        Return Me.CadreInfo
    End Function

    Protected Overrides Function InitControlDataGrid() As IControlDataGrid

        Me.ListeGrille.Act_InitFiltre = Sub(ide)
                                            Me.ListeGrille.InitFiltre("Aucune situation" & VI.Tild & "Une situation" & VI.Tild & "situations", _webfonction.PointeurDossier(ide).ListeDesNaturesAbsence)
                                        End Sub
        Return Me.ListeGrille
    End Function

    Protected Overrides Sub InitCacheIde()
        V_CacheIde.TypeVue = WebControlTypeVue.ListeGrid

        Dim captions As List(Of String) = New List(Of String)()
        captions.Add("Aucun évènement")
        captions.Add("Un évènement")
        captions.Add("évènements")

        Me.ListeGrille.InitCaptions(captions)

        Dim colonnes As ColonneDataGridCollection = New ColonneDataGridCollection()
        colonnes.Add(HorizontalAlign.Center, TypeData.Date, "date d'effet", 0)
        colonnes.Add(HorizontalAlign.Center, TypeData.Numeric, "nature", 1)
        colonnes.Add(HorizontalAlign.Center, TypeData.Numeric, "Nb jours calendaires", 2)
        colonnes.Add(HorizontalAlign.Center, TypeData.Chaine, "Clef", 0, True)

        Me.ListeGrille.InitColonnes(colonnes)

        Me.ListeGrille.Act_InitFiltre = AddressOf Me.InitFiltreDeListe
    End Sub
#End Region


    Protected Sub ListeHisto_ValeurChange(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.DonneeChangeEventArgs) Handles ListeHisto.ValeurChange
        If e.Valeur = "" Then
            Exit Sub
        End If
        Dim Tableaudata(0) As String
        Tableaudata = Strings.Split(e.Valeur, " --> ", -1)
        V_Occurence = Strings.Trim(Tableaudata(0))
    End Sub

    Private Sub LireLaFiche()
        Dim CacheDonnee As ArrayList = V_CacheMaj
        If CacheDonnee Is Nothing Then
            Exit Sub
        End If
        Dim NumInfo As Integer
        Dim Ctl As Control
        Dim VirControle As Controles_VCoupleEtiDonnee
        Dim IndiceI As Integer = 0

        Do
            Ctl = V_WebFonction.VirWebControle(Me.CadreInfo, "InfoH", IndiceI)
            If Ctl Is Nothing Then
                Exit Do
            End If
            NumInfo = CInt(Strings.Right(Ctl.ID, 2))
            VirControle = CType(Ctl, Controles_VCoupleEtiDonnee)
            If CacheDonnee(NumInfo) Is Nothing Then
                VirControle.DonText = ""
            Else
                VirControle.DonText = CacheDonnee(NumInfo).ToString
            End If
            VirControle.V_SiAutoPostBack = False
            IndiceI += 1
        Loop

        Dim VirVertical As VirtualiaControle_VCoupleVerticalEtiDonnee
        IndiceI = 0
        Do
            Ctl = V_WebFonction.VirWebControle(Me.CadreInfo, "InfoV", IndiceI)
            If Ctl Is Nothing Then
                Exit Do
            End If
            NumInfo = CInt(Strings.Right(Ctl.ID, 2))
            VirVertical = CType(Ctl, VirtualiaControle_VCoupleVerticalEtiDonnee)
            If CacheDonnee(NumInfo) Is Nothing Then
                VirVertical.DonText = ""
            Else
                VirVertical.DonText = CacheDonnee(NumInfo).ToString
            End If
            VirVertical.V_SiAutoPostBack = False
            IndiceI += 1
        Loop

        Dim VirDonneeTable As Controles_VDuoEtiquetteCommande
        IndiceI = 0
        Do
            Ctl = V_WebFonction.VirWebControle(Me.CadreInfo, "Dontab", IndiceI)
            If Ctl Is Nothing Then
                Exit Do
            End If
            NumInfo = CInt(Strings.Right(Ctl.ID, 2))
            VirDonneeTable = CType(Ctl, Controles_VDuoEtiquetteCommande)
            If CacheDonnee(NumInfo) Is Nothing Then
                VirDonneeTable.DonText = ""
            Else
                VirDonneeTable.DonText = CacheDonnee(NumInfo).ToString
            End If
            IndiceI += 1
        Loop
    End Sub

    Protected Sub CommandeNew_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles CommandeNew.Click
        Call InitialiserControles()
        Call V_CommandeNewFiche()
        CadreCmdOK.Visible = True
    End Sub

    Protected Sub CommandeSupp_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles CommandeSupp.Click
        Call V_CommandeSuppFiche()
    End Sub

    Public WriteOnly Property RetourDialogueSupp(ByVal Cmd As String) As String
        Set(ByVal value As String)
            V_RetourDialogueSupp(Cmd) = value
            Call ActualiserListe()
        End Set
    End Property

    Protected Sub CommandeOK_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles CommandeOK.Click
        Call V_MajFiche()
        Call ActualiserListe()
        CadreCmdOK.Visible = False
    End Sub

    Protected Sub InfoH_ValeurChange(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.DonneeChangeEventArgs) Handles InfoH00.ValeurChange, _
    InfoH01.ValeurChange, InfoH02.ValeurChange, InfoH04.ValeurChange, InfoH05.ValeurChange, InfoH06.ValeurChange, _
    InfoH07.ValeurChange, InfoH08.ValeurChange, InfoH09.ValeurChange, InfoH10.ValeurChange, InfoH11.ValeurChange

        Dim CacheDonnee As ArrayList = V_CacheMaj
        Dim NumInfo As Integer = CInt(Strings.Right(CType(sender, Controles_VCoupleEtiDonnee).ID, 2))
        If CacheDonnee IsNot Nothing Then
            If CacheDonnee(NumInfo) Is Nothing Then
                CType(sender, Controles_VCoupleEtiDonnee).DonBackColor = V_WebFonction.CouleurMaj
                CadreCmdOK.Visible = True
            Else
                If CacheDonnee(NumInfo).ToString <> e.Valeur Then
                    CType(sender, Controles_VCoupleEtiDonnee).DonBackColor = V_WebFonction.CouleurMaj
                    CadreCmdOK.Visible = True
                End If
            End If
            Call V_ValeurChange(NumInfo, e.Valeur)
        End If
    End Sub

    Protected Sub InfoVerticale_ValeurChange(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.DonneeChangeEventArgs) Handles InfoV14.ValeurChange

        Dim CacheDonnee As ArrayList = V_CacheMaj
        Dim NumInfo As Integer = CInt(Strings.Right(CType(sender, VirtualiaControle_VCoupleVerticalEtiDonnee).ID, 2))
        If CacheDonnee IsNot Nothing Then
            If CacheDonnee(NumInfo) Is Nothing Then
                CadreCmdOK.Visible = True
            Else
                If CacheDonnee(NumInfo).ToString <> e.Valeur Then
                    CadreCmdOK.Visible = True
                End If
            End If
            Call V_ValeurChange(NumInfo, e.Valeur)
        End If
    End Sub

    Protected Sub Dontab_AppelTable(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.AppelTableEventArgs) Handles Dontab03.AppelTable

        Dim NumInfo As Integer = CInt(Strings.Right(CType(sender, Controles_VDuoEtiquetteCommande).ID, 2))
        Dim Evenement As Virtualia.Systeme.Evenements.AppelTableEventArgs
        Evenement = New Virtualia.Systeme.Evenements.AppelTableEventArgs(e.ControleAppelant, e.ObjetAppelant, e.PointdeVueInverse, e.NomdelaTable)
        Call V_AppelTable(Evenement)
    End Sub

    Public WriteOnly Property Dontab_RetourAppelTable(ByVal IDAppelant As String) As String
        Set(ByVal value As String)
            Dim NumInfo As Integer
            Dim Ctl As Control
            Dim VirDonneeTable As Controles_VDuoEtiquetteCommande
            Dim CacheDonnee As ArrayList = V_CacheMaj

            Ctl = V_WebFonction.VirWebControle(Me.CadreInfo, "Dontab" & Strings.Right(IDAppelant, 2), 0)
            If Ctl Is Nothing Then
                Exit Property
            End If
            NumInfo = CInt(Strings.Right(Ctl.ID, 2))
            VirDonneeTable = CType(Ctl, Controles_VDuoEtiquetteCommande)
            VirDonneeTable.DonText = value
            If CacheDonnee IsNot Nothing Then
                If CacheDonnee(NumInfo) Is Nothing Then
                    VirDonneeTable.DonBackColor = V_WebFonction.CouleurMaj
                    CadreCmdOK.Visible = True
                Else
                    If CacheDonnee(NumInfo).ToString <> value Then
                        VirDonneeTable.DonBackColor = V_WebFonction.CouleurMaj
                        CadreCmdOK.Visible = True
                    End If
                End If
                Call V_ValeurChange(NumInfo, value)
            End If
        End Set
    End Property

    Protected Overrides Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        V_Objet(2) = WsNumObjet
        V_NomTableSgbd = WsNomTable
        V_Fiche = WsFiche
    End Sub

    Protected Overrides Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Call LireLaFiche()

        CadreInfo.BackColor = V_WebFonction.CouleurCharte(V_PointdeVue, "Cadre")
        CadreInfo.BorderColor = V_WebFonction.CouleurCharte(V_PointdeVue, "Bordure")
        Etiquette.BackColor = V_WebFonction.CouleurCharte(V_PointdeVue, "Titre")
        Etiquette.ForeColor = V_WebFonction.CouleurCharte(V_PointdeVue, "Police_Claire")
        Etiquette.BorderColor = V_WebFonction.CouleurCharte(V_PointdeVue, "Bordure")
        LabelEuro.ForeColor = V_WebFonction.CouleurCharte(V_PointdeVue, "Etiquette_BackColor")
        LabelHeures06.ForeColor = V_WebFonction.CouleurCharte(V_PointdeVue, "Etiquette_BackColor")
        LabelHeures07.ForeColor = V_WebFonction.CouleurCharte(V_PointdeVue, "Etiquette_BackColor")
        LabelHeures08.ForeColor = V_WebFonction.CouleurCharte(V_PointdeVue, "Etiquette_BackColor")
        LabelHeures09.ForeColor = V_WebFonction.CouleurCharte(V_PointdeVue, "Etiquette_BackColor")
        ListeHisto.LstBackColor = V_WebFonction.CouleurCharte(V_PointdeVue, "Etiquette_BackColor")
        ListeHisto.LstForeColor = V_WebFonction.CouleurCharte(V_PointdeVue, "Etiquette_ForeColor")
        ListeHisto.LstBorderColor = V_WebFonction.CouleurCharte(V_PointdeVue, "Bordure")

    End Sub

    Private Sub InitialiserControles()
        Dim Ctl As Control
        Dim VirControle As Controles_VCoupleEtiDonnee
        Dim IndiceI As Integer = 0
        Do
            Ctl = V_WebFonction.VirWebControle(Me.CadreInfo, "InfoH", IndiceI)
            If Ctl Is Nothing Then
                Exit Do
            End If
            VirControle = CType(Ctl, Controles_VCoupleEtiDonnee)
            VirControle.DonBackColor = Drawing.Color.White
            VirControle.DonText = ""
            IndiceI += 1
        Loop
        Dim VirVertical As VirtualiaControle_VCoupleVerticalEtiDonnee
        IndiceI = 0
        Do
            Ctl = V_WebFonction.VirWebControle(Me.CadreInfo, "InfoV", IndiceI)
            If Ctl Is Nothing Then
                Exit Do
            End If
            VirVertical = CType(Ctl, VirtualiaControle_VCoupleVerticalEtiDonnee)
            VirVertical.DonBackColor = Drawing.Color.White
            VirVertical.DonText = ""
            IndiceI += 1
        Loop
        Dim VirDonneeTable As Controles_VDuoEtiquetteCommande
        IndiceI = 0
        Do
            Ctl = V_WebFonction.VirWebControle(Me.CadreInfo, "Dontab", IndiceI)
            If Ctl Is Nothing Then
                Exit Do
            End If
            VirDonneeTable = CType(Ctl, Controles_VDuoEtiquetteCommande)
            VirDonneeTable.DonBackColor = Drawing.Color.White
            VirDonneeTable.DonText = ""
            IndiceI += 1
        Loop
    End Sub

End Class
