﻿<%@ Control Language="VB" AutoEventWireup="false" Inherits="Virtualia.Net.Fenetre_PER_PLANNING_HEBDO" Codebehind="PER_PLANNING_HEBDO.ascx.vb" %>

<asp:Table ID="CadreInfo" runat="server" BorderStyle="None" BorderWidth="2px" Visible="true"
  BorderColor="#B0E0D7" Height="260px" Width="750px" HorizontalAlign="Center" style="margin-top: 3px;">
    
  <asp:TableRow>
    <asp:TableCell>
      <asp:Table ID="CadreTitre" runat="server" Height="30px" CellPadding="0" Width="750px" 
         CellSpacing="0" HorizontalAlign="Center" BorderColor="Black" Visible="true" BorderStyle="NotSet">
            <asp:TableRow>
                <asp:TableCell HorizontalAlign="Center">
                    <asp:Label ID="Etiquette" runat="server" Text="Planning hebdomadaire" Height="20px" Width="320px"
                        BackColor="#216B68" BorderColor="#B0E0D7"  BorderStyle="Groove"
                        BorderWidth="2px" ForeColor="#D7FAF3"
                        Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Small"
                        style="margin-top: 5px; margin-left: 4px; margin-bottom: 10px;
                        font-style: oblique; text-indent: 5px; text-align: center;">
                    </asp:Label>          
                </asp:TableCell>      
            </asp:TableRow>
      </asp:Table>
    </asp:TableCell>
  </asp:TableRow>
  
  <asp:TableRow>
    <asp:TableCell>
        <asp:Table ID="TableauPlanningHebdomadaire" runat="server" CellPadding="0" CellSpacing="0" Width="750px"
        BorderStyle="Notset" BorderWidth="1px" BorderColor="#1C5151" HorizontalAlign="Left" Visible="true">
            
            <asp:TableRow ID="LigneTitre" Width="746px" Height="23px">
                <asp:TableCell HorizontalAlign="Center" ColumnSpan="9">
                    <asp:Label ID="LabelTitreTableau" runat="server" Text="Planning semaine 18" Height="23px" Width="746px"
                        BackColor="#D7FAF3" BorderColor="#B0E0D7"  BorderStyle="Notset"
                        BorderWidth="1px" ForeColor="#1C5151"
                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                        style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                        font-style: normal; text-indent: 0px; text-align: center; padding-top: 1px;">
                    </asp:Label>          
                </asp:TableCell>      
            </asp:TableRow>
            <asp:TableRow ID="LigneEnteteJour" Width="746px" Height="23px">
             <asp:TableCell ID="CellEnteteJour" BackColor="#D2D2D2" BorderColor="LightGray" BorderStyle="None" HorizontalAlign="Center" BorderWidth="1px"> 
              <asp:Label ID="LabelEnteteJour" runat="server" Height="23px" Width="189px"
                  BackColor="#D2D2D2" BorderColor="#D2D2D2" BorderStyle="Solid" Text=""
                  BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                  Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Smaller"
                  style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                  font-style: normal; text-indent: 5px; text-align:  left; padding-top: 2px;">
              </asp:Label>          
             </asp:TableCell>
             <asp:TableCell ID="CellLundi" BackColor="#A8BBB8" BorderColor="LightGray" BorderStyle="None" BorderWidth="1px"> 
              <asp:Label ID="LabelLundi" runat="server" Height="23px" Width="70px" 
                  BackColor="#A8BBB8" BorderColor="LightGray" BorderStyle="Solid" Text="Lundi"
                  BorderWidth="1px" ForeColor="#D7FAF3" Font-Italic="False"
                  Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Smaller"
                  style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                  font-style: normal; text-indent: 0px;  text-align:  center; padding-top: 2px;">
              </asp:Label>          
             </asp:TableCell>
             <asp:TableCell ID="CellMardi" BackColor="#A8BBB8" BorderColor="LightGray" BorderStyle="None" BorderWidth="1px"> 
              <asp:Label ID="LabelMardi" runat="server" Height="23px" Width="70px"
                  BackColor="#A8BBB8" BorderColor="LightGray" BorderStyle="Solid" Text="Mardi"
                  BorderWidth="1px" ForeColor="#D7FAF3" Font-Italic="False"
                  Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Smaller"
                  style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; 
                  font-style: normal; text-indent: 0px; text-align:  center; padding-top: 2px;">
              </asp:Label>          
             </asp:TableCell>
             <asp:TableCell ID="CellMercredi" BackColor="#A8BBB8" BorderColor="LightGray" BorderStyle="None" BorderWidth="1px"> 
              <asp:Label ID="LabelMercredi" runat="server" Height="23px" Width="70px"
                  BackColor="#A8BBB8" BorderColor="LightGray" BorderStyle="Solid" Text="Mercredi"
                  BorderWidth="1px" ForeColor="#D7FAF3" Font-Italic="False"
                  Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Smaller"
                  style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; 
                  font-style: normal; text-indent: 0px; text-align:  center; padding-top: 2px;">
              </asp:Label>          
             </asp:TableCell>
             <asp:TableCell ID="CellJeudi" BackColor="#A8BBB8" BorderColor="LightGray" BorderStyle="None" BorderWidth="1px"> 
              <asp:Label ID="LabelJeudi" runat="server" Height="23px" Width="70px"
                  BackColor="#A8BBB8" BorderColor="LightGray" BorderStyle="Solid" Text="Jeudi"
                  BorderWidth="1px" ForeColor="#D7FAF3" Font-Italic="False"
                  Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Smaller"
                  style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                  font-style: normal; text-indent: 0px; text-align:  center; padding-top: 2px;">
              </asp:Label>          
             </asp:TableCell>
             <asp:TableCell ID="CellVendredi" BackColor="#A8BBB8" BorderColor="LightGray" BorderStyle="None" BorderWidth="1px"> 
              <asp:Label ID="LabelVendredi" runat="server" Height="23px" Width="70px"
                  BackColor="#A8BBB8" BorderColor="LightGray" BorderStyle="Solid" Text="Vendredi"
                  BorderWidth="1px" ForeColor="#D7FAF3" Font-Italic="False"
                  Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Smaller"
                  style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                  font-style: normal; text-indent: 0px; text-align:  center; padding-top: 2px;">
              </asp:Label>          
             </asp:TableCell>
             <asp:TableCell ID="CellSamedi" BackColor="#A8BBB8" BorderColor="LightGray" BorderStyle="None" BorderWidth="1px"> 
              <asp:Label ID="LabelSamedi" runat="server" Height="23px" Width="70px"
                  BackColor="#A8BBB8" BorderColor="LightGray" BorderStyle="Solid" Text="Samedi"
                  BorderWidth="1px" ForeColor="#D7FAF3" Font-Italic="False"
                  Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Smaller"
                  style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                  font-style: normal; text-indent: 0px; text-align:  center; padding-top: 2px;">
              </asp:Label>          
             </asp:TableCell>
             <asp:TableCell ID="CellDimanche" BackColor="#A8BBB8" BorderColor="LightGray" BorderStyle="None" BorderWidth="1px"> 
              <asp:Label ID="LabelDimanche" runat="server" Height="23px" Width="70px"
                  BackColor="#A8BBB8" BorderColor="LightGray" BorderStyle="Solid" Text="Dimanche"
                  BorderWidth="1px" ForeColor="#D7FAF3" Font-Italic="False"
                  Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Smaller"
                  style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                  font-style: normal; text-indent: 0px; text-align:  center; padding-top: 2px;">
              </asp:Label>          
             </asp:TableCell>
             <asp:TableCell ID="CellSemaine" BackColor="#D2D2D2" BorderColor="LightGray" BorderStyle="None" BorderWidth="1px"> 
              <asp:Label ID="LabelSemaine" runat="server" Height="23px" Width="41px"
                  BackColor="#D2D2D2" BorderColor="#D2D2D2" BorderStyle="Solid" Text=""
                  BorderWidth="1px" ForeColor="#D7FAF3" Font-Italic="False"
                  Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Smaller"
                  style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                  font-style: normal; text-indent: 0px; text-align:  center; padding-top: 2px;">
              </asp:Label>          
             </asp:TableCell>      
          </asp:TableRow>
          
          <asp:TableRow ID="LigneEnteteDate" Width="746px" Height="23px">
             <asp:TableCell ID="CellDate" BackColor="#D2D2D2" BorderColor="LightGray" BorderStyle="None" HorizontalAlign="Center" BorderWidth="1px"> 
              <asp:Label ID="LabelDate" runat="server" Height="23px" Width="189px"
                  BackColor="#D2D2D2" BorderColor="#D2D2D2" BorderStyle="Solid" Text=""
                  BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                  Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Smaller"
                  style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                  font-style: normal; text-indent: 5px; text-align:  left; padding-top: 4px;">
              </asp:Label>          
             </asp:TableCell>
             <asp:TableCell ID="CellDateLundi" BackColor="#A8BBB8" BorderColor="LightGray" BorderStyle="None" BorderWidth="1px"> 
              <asp:Label ID="LabelDateLundi" runat="server" Height="23px" Width="70px" 
                  BackColor="#A8BBB8" BorderColor="LightGray" BorderStyle="Solid" Text="03/05/2010"
                  BorderWidth="1px" ForeColor="#D7FAF3" Font-Italic="False"
                  Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="11px"
                  style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                  font-style: normal; text-indent: 0px;  text-align:  center; padding-top: 4px;">
              </asp:Label>          
             </asp:TableCell>
             <asp:TableCell ID="CellDateMardi" BackColor="#A8BBB8" BorderColor="LightGray" BorderStyle="None" BorderWidth="1px"> 
              <asp:Label ID="LabelDateMardi" runat="server" Height="23px" Width="70px"
                  BackColor="#A8BBB8" BorderColor="LightGray" BorderStyle="Solid" Text="04/05/2010"
                  BorderWidth="1px" ForeColor="#D7FAF3" Font-Italic="False"
                  Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="11px"
                  style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                  font-style: normal; text-indent: 0px; text-align:  center; padding-top: 4px;">
              </asp:Label>          
             </asp:TableCell>
             <asp:TableCell ID="CellDateMercredi" BackColor="#A8BBB8" BorderColor="LightGray" BorderStyle="None" BorderWidth="1px"> 
              <asp:Label ID="LabelDateMercredi" runat="server" Height="23px" Width="70px"
                  BackColor="#A8BBB8" BorderColor="LightGray" BorderStyle="Solid" Text="05/05/2010"
                  BorderWidth="1px" ForeColor="#D7FAF3" Font-Italic="False"
                  Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="11px"
                  style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                  font-style: normal; text-indent: 0px; text-align:  center; padding-top: 4px;">
              </asp:Label>          
             </asp:TableCell>
             <asp:TableCell ID="CellDateJeudi" BackColor="#A8BBB8" BorderColor="LightGray" BorderStyle="None" BorderWidth="1px"> 
              <asp:Label ID="LabelDateJeudi" runat="server" Height="23px" Width="70px"
                  BackColor="#A8BBB8" BorderColor="LightGray" BorderStyle="Solid" Text="06/05/2010"
                  BorderWidth="1px" ForeColor="#D7FAF3" Font-Italic="False"
                  Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="11px"
                  style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                  font-style: normal; text-indent: 0px; text-align:  center; padding-top: 4px;">
              </asp:Label>          
             </asp:TableCell>
             <asp:TableCell ID="CellDateVendredi" BackColor="#A8BBB8" BorderColor="LightGray" BorderStyle="None" BorderWidth="1px"> 
              <asp:Label ID="LabelDateVendredi" runat="server" Height="23px" Width="70px"
                  BackColor="#A8BBB8" BorderColor="LightGray" BorderStyle="Solid" Text="07/05/2010"
                  BorderWidth="1px" ForeColor="#D7FAF3" Font-Italic="False"
                  Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="11px"
                  style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                  font-style: normal; text-indent: 0px; text-align:  center; padding-top: 4px;">
              </asp:Label>          
             </asp:TableCell>
             <asp:TableCell ID="CellDateSamedi" BackColor="#A8BBB8" BorderColor="LightGray" BorderStyle="None" BorderWidth="1px"> 
              <asp:Label ID="LabelDateSamedi" runat="server" Height="23px" Width="70px"
                  BackColor="#A8BBB8" BorderColor="LightGray" BorderStyle="Solid" Text="08/05/2010"
                  BorderWidth="1px" ForeColor="#D7FAF3" Font-Italic="False"
                  Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="11px"
                  style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                  font-style: normal; text-indent: 0px; text-align:  center; padding-top: 4px;">
              </asp:Label>          
             </asp:TableCell>
             <asp:TableCell ID="CellDateDimanche" BackColor="#A8BBB8" BorderColor="LightGray" BorderStyle="None" BorderWidth="1px"> 
              <asp:Label ID="LabelDateDimanche" runat="server" Height="23px" Width="70px"
                  BackColor="#A8BBB8" BorderColor="LightGray" BorderStyle="Solid" Text="09/05/2010"
                  BorderWidth="1px" ForeColor="#D7FAF3" Font-Italic="False"
                  Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="11px"
                  style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                  font-style: normal; text-indent: 0px; text-align:  center; padding-top: 4px;">
              </asp:Label>          
             </asp:TableCell>
             <asp:TableCell ID="CellDateSemaine" BackColor="#D2D2D2" BorderColor="LightGray" BorderStyle="None" BorderWidth="1px"> 
              <asp:Label ID="LabelDateSemaine" runat="server" Height="23px" Width="41px"
                  BackColor="#D2D2D2" BorderColor="#D2D2D2" BorderStyle="Solid" Text=""
                  BorderWidth="1px" ForeColor="#D7FAF3" Font-Italic="False"
                  Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="11px"
                  style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                  font-style: normal; text-indent: 0px; text-align:  center; padding-top: 4px;">
              </asp:Label>          
             </asp:TableCell>      
          </asp:TableRow>
                        
          <asp:TableRow>
             <asp:TableCell Height="1px" ColumnSpan="9"></asp:TableCell>
          </asp:TableRow> 
                          
          <asp:TableRow ID="LigneSemaine1" Width="746px" Height="20px">
            <asp:TableCell ID="CellPersonne1" BackColor="#f0f0f0" BorderColor="#A8BBB8" BorderStyle="None" HorizontalAlign="Center" Height="20px"> 
              <asp:Label ID="LabelPersonne1" runat="server" Height="20px" Width="189px"
                  BackColor="#f0f0f0" BorderColor="#A8BBB8" BorderStyle="Solid" Text="ANDRIEU Béatrice"
                  BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                  Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="13px"
                  style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                  font-style: normal; text-indent: 4px; text-align:  left; padding-top: 3px;">
              </asp:Label>          
            </asp:TableCell>
            <asp:TableCell ID="CellHoraireLundi1" BackColor="#f0f0f0" BorderColor="#A8BBB8" BorderStyle="None" Visible="true" HorizontalAlign="Center"> 
              <asp:Label ID="LigneHoraireLundi1" runat="server" Height="20px" Width="70px"
                        BackColor="White" BorderColor="#A8BBB8" BorderStyle="Solid" Text="08:30 - 17:30"
                        BorderWidth="1px" ForeColor="Black"
                        Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="11px" Font-Italic="false"
                        style="margin-top: 1px; margin-left: 0px; text-indent: 0px; text-align: center; padding-top: 3px;"> 
              </asp:Label>
            </asp:TableCell>
            <asp:TableCell ID="CellHoraireMardi1" BackColor="#f0f0f0" BorderColor="#A8BBB8" BorderStyle="None" Visible="true" HorizontalAlign="Center"> 
              <asp:Label ID="LigneHoraireMardi1" runat="server" Height="20px" Width="70px"
                        BackColor="White" BorderColor="#A8BBB8" BorderStyle="Solid" Text="08:30 - 17:30"
                        BorderWidth="1px" ForeColor="Black"
                        Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="11px" Font-Italic="false"
                        style="margin-top: 1px; margin-left: 0px; text-indent: 0px; text-align: center; padding-top: 3px;"> 
              </asp:Label> 
            </asp:TableCell>
            <asp:TableCell ID="CellHoraireMercredi1" BackColor="#f0f0f0" BorderColor="#A8BBB8" BorderStyle="None" Visible="true" HorizontalAlign="Center"> 
              <asp:Label ID="LigneHoraireMercredi1" runat="server" Height="20px" Width="69px"
                        BackColor="White" BorderColor="#A8BBB8" BorderStyle="Solid" Text="08:30 - 17:30"
                        BorderWidth="1px" ForeColor="Black"
                        Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="11px" Font-Italic="false"
                        style="margin-top: 1px; margin-left: 0px; text-indent: 0px; text-align: center; padding-top: 3px;"> 
              </asp:Label>         
            </asp:TableCell>
            <asp:TableCell ID="CellHoraireJeudi1" BackColor="#f0f0f0" BorderColor="#A8BBB8" BorderStyle="None" Visible="true" HorizontalAlign="Center"> 
              <asp:Label ID="LigneHoraireJeudi1" runat="server" Height="20px" Width="70px"
                        BackColor="White" BorderColor="#A8BBB8" BorderStyle="Solid" Text="08:30 - 17:30"
                        BorderWidth="1px" ForeColor="Black"
                        Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="11px" Font-Italic="false"
                        style="margin-top: 1px; margin-left: 0px; text-indent: 0px; text-align: center; padding-top: 3px;"> 
              </asp:Label>           
            </asp:TableCell>
            <asp:TableCell ID="CellHoraireVendredi1" BackColor="#f0f0f0" BorderColor="#A8BBB8" BorderStyle="None" Visible="true" HorizontalAlign="Center"> 
              <asp:Label ID="LigneHoraireVendredi1" runat="server" Height="20px" Width="70px"
                        BackColor="White" BorderColor="#A8BBB8" BorderStyle="Solid" Text="08:30 - 17:30"
                        BorderWidth="1px" ForeColor="Black"
                        Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="11px" Font-Italic="false"
                        style="margin-top: 1px; margin-left: 0px; text-indent: 0px; text-align: center; padding-top: 3px;"> 
              </asp:Label>        
            </asp:TableCell>
            <asp:TableCell ID="CellHoraireSamedi1" BackColor="#f0f0f0" BorderColor="#A8BBB8" BorderStyle="None" Visible="true" HorizontalAlign="Center"> 
              <asp:Label ID="LigneHoraireSamedi1" runat="server" Height="20px" Width="70px"
                        BackColor="#73829A" BorderColor="#A8BBB8" BorderStyle="Solid" Text="RH"
                        BorderWidth="1px" ForeColor="White"
                        Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="11px" Font-Italic="false"
                        style="margin-top: 1px; margin-left: 0px; text-indent: 0px; text-align: center; padding-top: 3px;"> 
              </asp:Label>           
            </asp:TableCell>
            <asp:TableCell ID="CellHoraireDimanche1" BackColor="#f0f0f0" BorderColor="#A8BBB8" BorderStyle="None" Visible="true" HorizontalAlign="Center"> 
              <asp:Label ID="LigneHoraireDimanche1" runat="server" Height="20px" Width="70px"
                        BackColor="#73829A" BorderColor="#A8BBB8" BorderStyle="Solid" Text="RH"
                        BorderWidth="1px" ForeColor="White"
                        Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="11px" Font-Italic="false"
                        style="margin-top: 1px; margin-left: 0px; text-indent: 0px; text-align: center; padding-top: 3px;"> 
              </asp:Label>           
            </asp:TableCell>
            <asp:TableCell ID="CellHoraireSemaine1" Visible="true" HorizontalAlign="Center"
            BackColor="#f0f0f0" BorderColor="#A8BBB8" BorderStyle="None" BorderWidth="1px"> 
              <asp:Label ID="LigneHoraireSemaine1" runat="server" Height="20px" Width="42px" 
                        BackColor="White" BorderColor="#A8BBB8" BorderStyle="Solid" Text="35 h 00"
                        BorderWidth="1px" ForeColor="Black"
                        Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="11px" Font-Italic="false"
                        style="margin-top: 1px; margin-left: 0px; 
                        text-indent: 0px; text-align: center; padding-top: 3px;"> 
              </asp:Label>           
            </asp:TableCell>           
          </asp:TableRow>
          
          <asp:TableRow>
             <asp:TableCell Height="1px" ColumnSpan="9"></asp:TableCell>
          </asp:TableRow>
           
          <asp:TableRow ID="LigneSemaine2" Width="746px" >
            <asp:TableCell ID="CellPersonne2" BackColor="#f0f0f0" BorderColor="#A8BBB8" 
            BorderStyle="None" HorizontalAlign="Center" >
               <asp:Table ID="TablePersonne2" runat="server" CellPadding="0" Width="189px" Visible="true"  
               CellSpacing="0" HorizontalAlign="Center" Height="48px"
               BackColor="#f0f0f0" BorderColor="#A8BBB8" BorderStyle="Solid" BorderWidth="1px" >
                 <asp:TableRow> 
                   <asp:TableCell HorizontalAlign="Center" RowSpan="2" ColumnSpan="2">
                     <asp:Label ID="LabelPersonne2" runat="server" Height="20px" Width="189px"
                        BackColor="#f0f0f0" BorderColor="#A8BBB8" BorderStyle="None" Text="AFILANO Alice"
                        BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="13px"
                        style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                        font-style: normal; text-indent: 4px; text-align: left; padding-top: 2px;">
                     </asp:Label>
                   </asp:TableCell>           
                 </asp:TableRow>  
               </asp:Table>                
            </asp:TableCell>
            <asp:TableCell ID="CellHoraireLundi2" BackColor="#f0f0f0" BorderColor="#A8BBB8" BorderStyle="None" Visible="true">
               <asp:Table ID="TableHoraireLundi2" runat="server" CellPadding="0" Width="70px" 
               CellSpacing="0" HorizontalAlign="Center" BorderColor="Black" Visible="true" BorderStyle="None" >
                 <asp:TableRow>
                   <asp:TableCell HorizontalAlign="Center">
                     <asp:Label ID="LabelHoraireLundi2P1" runat="server" Height="20px" Width="70px"
                        BackColor="White" BorderColor="#A8BBB8" BorderStyle="Solid" Text="09:00 - 12:30"
                        BorderWidth="1px" ForeColor="Black"
                        Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="11px" Font-Italic="false"
                        style="margin-top: 1px; margin-left: 0px; text-indent: 0px; text-align: center; padding-top: 3px;"> 
                     </asp:Label>
                   </asp:TableCell>           
                 </asp:TableRow>
                 <asp:TableRow>
                   <asp:TableCell HorizontalAlign="Center">
                     <asp:Label ID="LabelHoraireLundi2P2" runat="server" Height="20px" Width="70px"
                        BackColor="White" BorderColor="#A8BBB8" BorderStyle="Solid" Text="14:00 - 17:30"
                        BorderWidth="1px" ForeColor="Black"
                        Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="11px" Font-Italic="false"
                        style="margin-top: -1px; margin-left: 0px; text-indent: 0px; text-align: center; padding-top: 3px;"> 
                     </asp:Label>
                   </asp:TableCell>           
                 </asp:TableRow>
               </asp:Table>
            </asp:TableCell>
            <asp:TableCell ID="CellHoraireMardi2" BackColor="#f0f0f0" BorderColor="#A8BBB8" BorderStyle="None" Visible="true">
               <asp:Table ID="TableHoraireMardi2" runat="server" CellPadding="0" Width="70px" 
               CellSpacing="0" HorizontalAlign="Center" BorderColor="Black" Visible="true" BorderStyle="None">
                 <asp:TableRow>
                   <asp:TableCell HorizontalAlign="Center">
                     <asp:Label ID="LabelHoraireMardi2P1" runat="server" Height="20px" Width="70px"
                        BackColor="White" BorderColor="#A8BBB8" BorderStyle="Solid" Text="09:00 - 12:30"
                        BorderWidth="1px" ForeColor="Black"
                        Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="11px" Font-Italic="false"
                        style="margin-top: 1px; margin-left: 0px; text-indent: 0px; text-align: center; padding-top: 3px;"> 
                     </asp:Label>
                   </asp:TableCell>           
                 </asp:TableRow>
                 <asp:TableRow>
                   <asp:TableCell HorizontalAlign="Center">
                     <asp:Label ID="LabelHoraireMardi2P2" runat="server" Height="20px" Width="70px"
                        BackColor="White" BorderColor="#A8BBB8" BorderStyle="Solid" Text="14:00 - 17:30"
                        BorderWidth="1px" ForeColor="Black"
                        Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="11px" Font-Italic="false"
                        style="margin-top: -1px; margin-left: 0px; text-indent: 0px; text-align: center; padding-top: 3px;"> 
                     </asp:Label>
                   </asp:TableCell>           
                 </asp:TableRow>
               </asp:Table>
            </asp:TableCell>
            <asp:TableCell ID="CellHoraireMercredi2" BackColor="#f0f0f0" BorderColor="#A8BBB8" BorderStyle="None" Visible="true">
               <asp:Table ID="TableHoraireMercredi2" runat="server" CellPadding="0" Width="69px" 
               CellSpacing="0" HorizontalAlign="Center" BorderColor="Black" Visible="true" BorderStyle="None" >
                 <asp:TableRow>
                   <asp:TableCell HorizontalAlign="Center">
                     <asp:Label ID="LabelHoraireMercredi2P1" runat="server" Height="20px" Width="69px"
                        BackColor="White" BorderColor="#A8BBB8" BorderStyle="Solid" Text="09:00 - 12:30"
                        BorderWidth="1px" ForeColor="Black"
                        Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="11px" Font-Italic="false"
                        style="margin-top: 1px; margin-left: 0px; text-indent: 0px; text-align: center; padding-top: 3px;"> 
                     </asp:Label>
                   </asp:TableCell>           
                 </asp:TableRow>
                 <asp:TableRow>
                   <asp:TableCell HorizontalAlign="Center">
                     <asp:Label ID="LabelHoraireMercredi2P2" runat="server" Height="20px" Width="69px"
                        BackColor="White" BorderColor="#A8BBB8" BorderStyle="Solid" Text="14:00 - 17:30"
                        BorderWidth="1px" ForeColor="Black"
                        Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="11px" Font-Italic="false"
                        style="margin-top: -1px; margin-left: 0px; text-indent: 0px; text-align: center; padding-top: 3px;"> 
                     </asp:Label>
                   </asp:TableCell>           
                 </asp:TableRow>
               </asp:Table>
            </asp:TableCell>
            <asp:TableCell ID="CellHoraireJeudi2" BackColor="#f0f0f0" BorderColor="#A8BBB8" BorderStyle="None" Visible="true">
               <asp:Table ID="TableHoraireJeudi2" runat="server" CellPadding="0" Width="70px" 
               CellSpacing="0" HorizontalAlign="Center" BorderColor="Black" Visible="true" BorderStyle="None" >
                 <asp:TableRow>
                   <asp:TableCell HorizontalAlign="Center">
                     <asp:Label ID="LabelHoraireJeudi2P1" runat="server" Height="20px" Width="70px"
                        BackColor="White" BorderColor="#A8BBB8" BorderStyle="Solid" Text="09:00 - 12:30"
                        BorderWidth="1px" ForeColor="Black"
                        Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="11px" Font-Italic="false"
                        style="margin-top: 1px; margin-left: 0px; text-indent: 0px; text-align: center; padding-top: 3px;"> 
                     </asp:Label>
                   </asp:TableCell>           
                 </asp:TableRow>
                 <asp:TableRow>
                   <asp:TableCell HorizontalAlign="Center">
                     <asp:Label ID="LabelHoraireJeudi2P2" runat="server" Height="20px" Width="70px"
                        BackColor="White" BorderColor="#A8BBB8" BorderStyle="Solid" Text="14:00 - 17:30"
                        BorderWidth="1px" ForeColor="Black"
                        Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="11px" Font-Italic="false"
                        style="margin-top: -1px; margin-left: 0px; text-indent: 0px; text-align: center; padding-top: 3px;"> 
                     </asp:Label>
                   </asp:TableCell>           
                 </asp:TableRow>
               </asp:Table>
            </asp:TableCell>
            <asp:TableCell ID="CellHoraireVendredi2" BackColor="#f0f0f0" BorderColor="#A8BBB8" BorderStyle="None" Visible="true">
               <asp:Table ID="TableHoraireVendredi2" runat="server" CellPadding="0" Width="70px" 
               CellSpacing="0" HorizontalAlign="Center" BorderColor="Black" Visible="true" BorderStyle="None">
                 <asp:TableRow>
                   <asp:TableCell HorizontalAlign="Center">
                     <asp:Label ID="LabelHoraireVendredi2P1" runat="server" Height="20px" Width="70px"
                        BackColor="White" BorderColor="#A8BBB8" BorderStyle="Solid" Text="09:00 - 12:30"
                        BorderWidth="1px" ForeColor="Black"
                        Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="11px" Font-Italic="false"
                        style="margin-top: 1px; margin-left: 0px; text-indent: 0px; text-align: center; padding-top: 3px;"> 
                     </asp:Label>
                   </asp:TableCell>           
                 </asp:TableRow>
                 <asp:TableRow>
                   <asp:TableCell HorizontalAlign="Center">
                     <asp:Label ID="LabelHoraireVendredi2P2" runat="server" Height="20px" Width="70px"
                        BackColor="White" BorderColor="#A8BBB8" BorderStyle="Solid" Text="14:00 - 17:30"
                        BorderWidth="1px" ForeColor="Black"
                        Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="11px" Font-Italic="false"
                        style="margin-top: -1px; margin-left: 0px; text-indent: 0px; text-align: center; padding-top: 3px;"> 
                     </asp:Label>
                   </asp:TableCell>           
                 </asp:TableRow>
               </asp:Table>
            </asp:TableCell>
            <asp:TableCell ID="CellHoraireSamedi2" BackColor="#f0f0f0" BorderColor="#A8BBB8" BorderStyle="None" Visible="true">
               <asp:Table ID="TableHoraireSamedi2" runat="server" CellPadding="0" Width="70px" 
               CellSpacing="0" HorizontalAlign="Center" BorderColor="Black" Visible="true" BorderStyle="None">
                 <asp:TableRow>
                   <asp:TableCell HorizontalAlign="Center">
                     <asp:Label ID="LabelHoraireSamedi2P1" runat="server" Height="20px" Width="70px"
                        BackColor="#73829A" BorderColor="#A8BBB8" BorderStyle="Solid" Text="RH"
                        BorderWidth="1px" ForeColor="White"
                        Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="11px" Font-Italic="false"
                        style="margin-top: 1px; margin-left: 0px; text-indent: 0px; text-align: center; padding-top: 3px;"> 
                     </asp:Label>
                   </asp:TableCell>           
                 </asp:TableRow>
                 <asp:TableRow>
                   <asp:TableCell HorizontalAlign="Center">
                     <asp:Label ID="LabelHoraireSamedi2P2" runat="server" Height="20px" Width="70px"
                        BackColor="#73829A" BorderColor="#A8BBB8" BorderStyle="Solid" Text="RH"
                        BorderWidth="1px" ForeColor="White"
                        Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="11px" Font-Italic="false"
                        style="margin-top: -1px; margin-left: 0px; text-indent: 0px; text-align: center; padding-top: 3px;"> 
                     </asp:Label>
                   </asp:TableCell>           
                 </asp:TableRow>
               </asp:Table>
            </asp:TableCell>
            <asp:TableCell ID="CellHoraireDimanche2" BackColor="#f0f0f0" BorderColor="#A8BBB8" BorderStyle="None" Visible="true">
               <asp:Table ID="TableHoraireDimanche2" runat="server" CellPadding="0" Width="70px" 
               CellSpacing="0" HorizontalAlign="Center" BorderColor="Black" Visible="true" BorderStyle="None" >
                 <asp:TableRow>
                   <asp:TableCell HorizontalAlign="Center">
                     <asp:Label ID="LabelHoraireDimanche2P1" runat="server" Height="20px" Width="70px"
                        BackColor="#73829A" BorderColor="#A8BBB8" BorderStyle="Solid" Text="RH"
                        BorderWidth="1px" ForeColor="White"
                        Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="11px" Font-Italic="false"
                        style="margin-top: 1px; margin-left: 0px; text-indent: 0px; text-align: center; padding-top: 3px;"> 
                     </asp:Label>
                   </asp:TableCell>           
                 </asp:TableRow>
                 <asp:TableRow>
                   <asp:TableCell HorizontalAlign="Center">
                     <asp:Label ID="LabelHoraireDimanche2P2" runat="server" Height="20px" Width="70px"
                        BackColor="#73829A" BorderColor="#A8BBB8" BorderStyle="Solid" Text="RH"
                        BorderWidth="1px" ForeColor="White"
                        Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="11px" Font-Italic="false"
                        style="margin-top: -1px; margin-left: 0px; text-indent: 0px; text-align: center; padding-top: 3px;"> 
                     </asp:Label>
                   </asp:TableCell>           
                 </asp:TableRow>
               </asp:Table>
            </asp:TableCell>
            <asp:TableCell ID="CellHoraireSemaine2" BackColor="#f0f0f0" BorderColor="#A8BBB8" BorderStyle="None" Visible="true">
               <asp:Table ID="TableHoraireSemaine2" runat="server" Height="48px" CellPadding="0" Width="42px" 
               CellSpacing="0" HorizontalAlign="Center" Visible="true"
               BackColor="White" BorderColor="#A8BBB8" BorderStyle="Solid" BorderWidth="1px">
                 <asp:TableRow>
                   <asp:TableCell HorizontalAlign="Center" RowSpan="2" ColumnSpan="2">
                     <asp:Label ID="LabelHoraireSemaine2" runat="server" Height="20px" Width="42px"
                        BackColor="White" BorderColor="#A8BBB8" BorderStyle="None" Text="35 h 00"
                        BorderWidth="1px" ForeColor="Black"
                        Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="11px" Font-Italic="false"
                        style="margin-top: 1px; margin-left: 0px;
                        text-indent: 0px; text-align: center; padding-top: 2px;"> 
                     </asp:Label>  
                   </asp:TableCell>           
                 </asp:TableRow>
                  
               </asp:Table>
            </asp:TableCell>           
          </asp:TableRow>
          
        </asp:Table>
     </asp:TableCell>     
  </asp:TableRow> 
    
  <asp:TableRow>
    <asp:TableCell>
      <asp:Table ID="TableTotal" runat="server" CellPadding="0" CellSpacing="0" Width="750px"
      BorderStyle="Notset" BorderWidth="1px" BorderColor="Black" HorizontalAlign="Left">
        <asp:TableRow ID="LigneTotal" Width="746px" Height="20px">
            <asp:TableCell ID="TitreTotal" BackColor="#f0f0f0" BorderColor="#A8BBB8" BorderStyle="None" HorizontalAlign="Center" Height="20px"> 
              <asp:Label ID="LabelTotal" runat="server" Height="20px" Width="189px"
                  BackColor="#D2D2D2" BorderColor="#A8BBB8" BorderStyle="Solid" Text="Total"
                  BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                  Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="13px"
                  style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                  font-style: normal; text-indent: 0px; text-align:  center; padding-top: 3px;">
              </asp:Label>          
            </asp:TableCell>
            <asp:TableCell ID="TotalLundi" BackColor="#f0f0f0" BorderColor="#A8BBB8" BorderStyle="None" Visible="true" HorizontalAlign="Center"> 
              <asp:Label ID="LabelTotalLundi" runat="server" Height="20px" Width="70px"
                        BackColor="White" BorderColor="#A8BBB8" BorderStyle="Solid" Text="38 h 15"
                        BorderWidth="1px" ForeColor="Black"
                        Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="11px" Font-Italic="false"
                        style="margin-top: 1px; margin-left: 0px; text-indent: 0px; text-align: center; padding-top: 3px;"> 
              </asp:Label>
            </asp:TableCell>
            <asp:TableCell ID="TotalMardi" BackColor="#f0f0f0" BorderColor="#A8BBB8" BorderStyle="None" Visible="true" HorizontalAlign="Center"> 
              <asp:Label ID="LabelTotalMardi" runat="server" Height="20px" Width="70px"
                        BackColor="White" BorderColor="#A8BBB8" BorderStyle="Solid" Text="32 h 12"
                        BorderWidth="1px" ForeColor="Black"
                        Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="11px" Font-Italic="false"
                        style="margin-top: 1px; margin-left: 0px; text-indent: 0px; text-align: center; padding-top: 3px;"> 
              </asp:Label> 
            </asp:TableCell>
            <asp:TableCell ID="TotalMercredi" BackColor="#f0f0f0" BorderColor="#A8BBB8" BorderStyle="None" Visible="true" HorizontalAlign="Center"> 
              <asp:Label ID="LabelTotalMercredi" runat="server" Height="20px" Width="69px"
                        BackColor="White" BorderColor="#A8BBB8" BorderStyle="Solid" Text="28 h 46"
                        BorderWidth="1px" ForeColor="Black"
                        Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="11px" Font-Italic="false"
                        style="margin-top: 1px; margin-left: 0px; text-indent: 0px; text-align: center; padding-top: 3px;"> 
              </asp:Label>         
            </asp:TableCell>
            <asp:TableCell ID="TotalJeudi" BackColor="#f0f0f0" BorderColor="#A8BBB8" BorderStyle="None" Visible="true" HorizontalAlign="Center"> 
              <asp:Label ID="LabelTotalJeudi" runat="server" Height="20px" Width="70px"
                        BackColor="White" BorderColor="#A8BBB8" BorderStyle="Solid" Text="39 h30"
                        BorderWidth="1px" ForeColor="Black"
                        Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="11px" Font-Italic="false"
                        style="margin-top: 1px; margin-left: 0px; text-indent: 0px; text-align: center; padding-top: 3px;"> 
              </asp:Label>           
            </asp:TableCell>
            <asp:TableCell ID="TotalVendredi" BackColor="#f0f0f0" BorderColor="#A8BBB8" BorderStyle="None" Visible="true" HorizontalAlign="Center"> 
              <asp:Label ID="LabelTotalVendredi" runat="server" Height="20px" Width="70px"
                        BackColor="White" BorderColor="#A8BBB8" BorderStyle="Solid" Text="31 h 22"
                        BorderWidth="1px" ForeColor="Black"
                        Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="11px" Font-Italic="false"
                        style="margin-top: 1px; margin-left: 0px; text-indent: 0px; text-align: center; padding-top: 3px;"> 
              </asp:Label>        
            </asp:TableCell>
            <asp:TableCell ID="TotalSamedi" BackColor="#f0f0f0" BorderColor="#A8BBB8" BorderStyle="None" Visible="true" HorizontalAlign="Center"> 
              <asp:Label ID="LabelTotalSamedi" runat="server" Height="20px" Width="70px"
                        BackColor="White" BorderColor="#A8BBB8" BorderStyle="Solid" Text=""
                        BorderWidth="1px" ForeColor="Black"
                        Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="11px" Font-Italic="false"
                        style="margin-top: 1px; margin-left: 0px; text-indent: 0px; text-align: center; padding-top: 3px;"> 
              </asp:Label>           
            </asp:TableCell>
            <asp:TableCell ID="TotalDimanche" BackColor="#f0f0f0" BorderColor="#A8BBB8" BorderStyle="None" Visible="true" HorizontalAlign="Center"> 
              <asp:Label ID="LabelTotalDimanche" runat="server" Height="20px" Width="70px"
                        BackColor="White" BorderColor="#A8BBB8" BorderStyle="Solid" Text=""
                        BorderWidth="1px" ForeColor="Black"
                        Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="11px" Font-Italic="false"
                        style="margin-top: 1px; margin-left: 0px; text-indent: 0px; text-align: center; padding-top: 3px;"> 
              </asp:Label>           
            </asp:TableCell>
            <asp:TableCell ID="TotalGeneral" Visible="true" HorizontalAlign="Center"
            BackColor="#f0f0f0" BorderColor="#A8BBB8" BorderStyle="None" BorderWidth="1px"> 
              <asp:Label ID="LabelTotalGeneral" runat="server" Height="20px" Width="42px" 
                        BackColor="White" BorderColor="#A8BBB8" BorderStyle="Solid" Text="144 h 28"
                        BorderWidth="1px" ForeColor="Black"
                        Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="9px" Font-Italic="false"
                        style="margin-top: 1px; margin-left: 0px; 
                        text-indent: 0px; text-align: center; padding-top: 3px;"> 
              </asp:Label>           
            </asp:TableCell>           
          </asp:TableRow>
      </asp:Table>
    </asp:TableCell>
  </asp:TableRow>    
    
 </asp:Table>