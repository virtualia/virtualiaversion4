﻿Option Strict On
Option Explicit On
Option Compare Text

Imports VI = Virtualia.Systeme.Constantes
Imports Virtualia.ObjetBase
Imports Virtualia.OutilsVisu
Imports Virtualia.TablesObjet.ShemaPER

Partial Class Fenetre_PER_OBJECTIFS
    Inherits ObjetWebControleBase(Of PER_OBJECTIFS)
    Implements IControlCrud

#Region "IControlCrud"
    Public ReadOnly Property ControlCrud As ICtlCommandeCrud Implements IControlCrud.CtlCrud
        Get
            Return Me.CtlCrud
        End Get
    End Property
#End Region

#Region "ObjetWebControleBase"
    Private _webfonction As WebFonctions
    Protected Overrides ReadOnly Property V_WebFonction As IWebFonctions
        Get
            If (_webfonction Is Nothing) Then
                _webfonction = New WebFonctions(Me)
            End If
            Return _webfonction
        End Get
    End Property

    Protected Overrides Function InitCadreInfo() As WebControl
        Return Me.CadreInfo
    End Function

    Protected Overrides Function InitControlDataGrid() As IControlDataGrid
        Return Me.ListeGrille
    End Function

    Protected Overrides Sub InitCacheIde()

        V_CacheIde.TypeVue = WebControlTypeVue.ListeGrid

        Dim captions As List(Of String) = New List(Of String)()
        captions.Add("Aucun objectif")
        captions.Add("Un objectif")
        captions.Add("objectifs")

        Me.ListeGrille.InitCaptions(captions)

        Dim colonnes As ColonneDataGridCollection = New ColonneDataGridCollection()

        colonnes.Add(HorizontalAlign.Center, TypeData.Date, "Année", 0)
        colonnes.Add(HorizontalAlign.Center, TypeData.Numeric, "définition", 1)
        colonnes.Add(HorizontalAlign.Center, TypeData.Numeric, "montant attribué", 8)
        colonnes.Add(HorizontalAlign.Center, TypeData.Numeric, "bonus exceptionnel", 9)
        colonnes.Add(HorizontalAlign.Center, TypeData.Chaine, "Clef", 0, True)

        Me.ListeGrille.InitColonnes(colonnes)

    End Sub

    Protected Overrides Sub OnPreRender(e As EventArgs)
        MyBase.OnPreRender(e)

        CadreInfo.BackColor = _webfonction.CouleurCharte(V_CacheIde.PointDeVue, "Cadre")
        CadreInfo.BorderColor = _webfonction.CouleurCharte(V_CacheIde.PointDeVue, "Bordure")
        Etiquette.BackColor = _webfonction.CouleurCharte(V_CacheIde.PointDeVue, "Titre")
        Etiquette.ForeColor = _webfonction.CouleurCharte(V_CacheIde.PointDeVue, "Police_Claire")
        Etiquette.BorderColor = _webfonction.CouleurCharte(V_CacheIde.PointDeVue, "Bordure")
        LabelRealisation.BackColor = _webfonction.CouleurCharte(V_CacheIde.PointDeVue, "Titre")
        LabelRealisation.ForeColor = _webfonction.CouleurCharte(V_CacheIde.PointDeVue, "Police_Claire")
        LabelRealisation.BorderColor = _webfonction.CouleurCharte(V_CacheIde.PointDeVue, "Bordure")
        LabelEuro8.ForeColor = _webfonction.CouleurCharte(V_CacheIde.PointDeVue, "Etiquette_BackColor")
        LabelEuro9.ForeColor = _webfonction.CouleurCharte(V_CacheIde.PointDeVue, "Etiquette_BackColor")
        LabelPourcent2.ForeColor = _webfonction.CouleurCharte(V_CacheIde.PointDeVue, "Etiquette_BackColor")
        LabelPourcent3.ForeColor = _webfonction.CouleurCharte(V_CacheIde.PointDeVue, "Etiquette_BackColor")
        LabelPourcent4.ForeColor = _webfonction.CouleurCharte(V_CacheIde.PointDeVue, "Etiquette_BackColor")
        LabelPourcent5.ForeColor = _webfonction.CouleurCharte(V_CacheIde.PointDeVue, "Etiquette_BackColor")
        LabelPourcent6.ForeColor = _webfonction.CouleurCharte(V_CacheIde.PointDeVue, "Etiquette_BackColor")
        LabelPourcent7.ForeColor = _webfonction.CouleurCharte(V_CacheIde.PointDeVue, "Etiquette_BackColor")
    End Sub

    Protected Overrides Sub OnInit(e As System.EventArgs)
        MyBase.OnInit(e)

        Dim initwebfonction As IWebFonctions = V_WebFonction
    End Sub
#End Region

End Class
