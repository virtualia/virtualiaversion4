﻿<%@ Control Language="VB" AutoEventWireup="false" Inherits="Virtualia.Net.Fenetre_PER_CUMUL_EMPLOI" CodeBehind="PER_CUMUL_EMPLOI.ascx.vb" %>

<%@ Register Src="~/Controles/Saisies/VCoupleEtiDonnee.ascx" TagName="VCoupleEtiDonnee" TagPrefix="Virtualia" %>
<%@ Register Src="~/Controles/Saisies/VDuoEtiquetteCommande.ascx" TagName="VDuoEtiquetteCommande" TagPrefix="Virtualia" %>
<%@ Register Src="~/Controles/Saisies/VCoupleVerticalEtiDonnee.ascx" TagName="VCoupleVerticalEtiDonnee" TagPrefix="Virtualia" %>

<%@ Register Src="~/ControlsGeneriques/V_DataGrid.ascx" TagName="VDataGrid" TagPrefix="Generic" %>
<%@ Register Src="~/ControlsGeneriques/V_CommandeCRUD.ascx" TagName="VCrud" TagPrefix="Generic" %>

<asp:Table ID="CadreControle" runat="server" Height="30px" Width="550px" CellPadding="0" CellSpacing="0" HorizontalAlign="Center">
    <asp:TableRow>
        <asp:TableCell>
            <Generic:VDataGrid ID="ListeGrille" runat="server" CadreWidth="550px" SiColonneSelect="true" SiCaseAcocher="false" />
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="CadreInfo" runat="server" BorderStyle="Ridge" BorderWidth="2px" BorderColor="#B0E0D7" Height="555px" Width="550px" HorizontalAlign="Center" Style="margin-top: 3px;">
                <asp:TableRow>
                    <asp:TableCell>
                        <Generic:VCrud ID="CtlCrud" runat="server" Height="22px" Width="244px" HorizontalAlign="Right" CadreStyle="margin-top: 3px; margin-right: 3px" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Table ID="CadreTitre" runat="server" Height="40px" CellPadding="0" Width="550px" CellSpacing="0" HorizontalAlign="Center">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                    <asp:Label ID="Etiquette" runat="server" Text="Cumul d'emploi et de rémunération" Height="20px" Width="350px" BackColor="#2FA49B" BorderColor="#B0E0D7" BorderStyle="Groove" BorderWidth="2px" ForeColor="#D7FAF3" Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Small" Style="margin-top: 5px; margin-left: 4px; margin-bottom: 20px; font-style: oblique; text-indent: 5px; text-align: center;" />
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Table ID="CadreDates" runat="server" CellPadding="0" CellSpacing="0" Width="500px">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left" ColumnSpan="2">
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH00" runat="server" V_PointdeVue="1" V_Objet="69" V_Information="0" V_SiDonneeDico="true" EtiWidth="190px" DonWidth="80px" DonTabIndex="1" />
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Left">
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH08" runat="server" V_PointdeVue="1" V_Objet="69" V_Information="8" V_SiDonneeDico="true" EtiWidth="110px" DonWidth="80px" DonTabIndex="2" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Height="10px" ColumnSpan="2" />
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Table ID="CadreDon" runat="server" CellPadding="0" CellSpacing="0" Width="550px">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <Virtualia:VDuoEtiquetteCommande ID="Dontab01" runat="server" V_PointdeVue="1" V_Objet="69" V_Information="1" V_SiDonneeDico="true" DonTabIndex="3" EtiWidth="130px" DonWidth="400px" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <Virtualia:VDuoEtiquetteCommande ID="Dontab02" runat="server" V_PointdeVue="1" V_Objet="69" V_Information="2" V_SiDonneeDico="true" DonTabIndex="4" EtiWidth="130px" DonWidth="400px" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <Virtualia:VDuoEtiquetteCommande ID="Dontab06" runat="server" V_PointdeVue="1" V_Objet="69" V_Information="6" V_SiDonneeDico="true" DonTabIndex="5" EtiWidth="130px" DonWidth="400px" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH05" runat="server" V_PointdeVue="1" V_Objet="69" V_Information="5" V_SiDonneeDico="true" EtiWidth="130px" DonWidth="80px" DonTabIndex="6" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Height="4px" />
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Table ID="TableauTitreHeures" runat="server" CellPadding="0" CellSpacing="0" Width="550px">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                    <asp:Label ID="EtiHeures" runat="server" Height="20px" Width="300px" Text="Heures effectuées" BackColor="#216B68" BorderColor="#B0E0D7" BorderStyle="Outset" BorderWidth="2px" ForeColor="#D7FAF3" Font-Italic="true" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Style="margin-top: 1px; margin-left: 20px; text-indent: 5px; text-align: center" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Height="5px" />
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH07" runat="server" V_PointdeVue="1" V_Objet="69" V_Information="7" V_SiDonneeDico="true" EtiWidth="200px" DonWidth="40px" DonTabIndex="7" Etistyle="margin-left: 0px;" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Height="5px" />
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Table ID="CadreDonHeuresHED" runat="server" CellPadding="0" CellSpacing="0" Width="550px">
                            <asp:TableRow>
                                <asp:TableCell ColumnSpan="2">
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH11" runat="server" V_PointdeVue="1" V_Objet="69" V_Information="11" V_SiDonneeDico="true" EtiWidth="205px" DonWidth="40px" DonTabIndex="8" Etistyle="margin-left: 15px;" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell>
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH12" runat="server" V_PointdeVue="1" V_Objet="69" V_Information="12" V_SiDonneeDico="true" EtiWidth="205px" DonWidth="40px" DonTabIndex="9" Etistyle="margin-left: 15px;" />
                                </asp:TableCell>
                                <asp:TableCell>
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH10" runat="server" V_PointdeVue="1" V_Objet="69" V_Information="10" V_SiDonneeDico="true" EtiWidth="190px" DonWidth="40px" DonTabIndex="10" Etistyle="margin-left: 15px;" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell ColumnSpan="2">
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH13" runat="server" V_PointdeVue="1" V_Objet="69" V_Information="13" V_SiDonneeDico="true" EtiWidth="205px" DonWidth="40px" DonTabIndex="11" Etistyle="margin-left: 15px;" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Height="5px" ColumnSpan="2" />
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Table ID="TableauRemu" runat="server" CellPadding="0" CellSpacing="0" Width="550px">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                    <asp:Label ID="EtiRemu" runat="server" Height="20px" Width="300px" Text="Rémunération perçue" BackColor="#216B68" BorderColor="#B0E0D7" BorderStyle="Outset" BorderWidth="2px" ForeColor="#D7FAF3" Font-Italic="true" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Style="margin-top: 1px; margin-left: 20px; text-indent: 5px; text-align: center" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Height="5px" />
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Table ID="TableauMontants" runat="server" CellPadding="0" CellSpacing="0" Width="450px">
                            <asp:TableRow>
                                <asp:TableCell>
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH04" runat="server" V_PointdeVue="1" V_Objet="69" V_Information="4" V_SiDonneeDico="true" EtiWidth="150px" DonWidth="70px" DonTabIndex="12" Etistyle="margin-left: 5px;" />
                                </asp:TableCell>
                                <asp:TableCell>
                                    <asp:Label ID="LabelEuro1" runat="server" Text="€" Height="20px" Width="15px" BackColor="Transparent" BorderStyle="none" BorderWidth="0px" ForeColor="#2FA49B" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; font-style: oblique; text-indent: 0px; text-align: left;" />
                                </asp:TableCell>
                                <asp:TableCell>
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH03" runat="server" V_PointdeVue="1" V_Objet="69" V_Information="3" V_SiDonneeDico="true" EtiWidth="150px" DonWidth="70px" DonTabIndex="13" Etistyle="margin-left: 0px;" />
                                </asp:TableCell>
                                <asp:TableCell>
                                    <asp:Label ID="LabelEuro2" runat="server" Text="€" Height="20px" Width="15px" BackColor="Transparent" BorderStyle="none" BorderWidth="0px" ForeColor="#2FA49B" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; font-style: oblique; text-indent: 0px; text-align: left;" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Height="10px" ColumnSpan="4" />
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Table ID="CadreObservations" runat="server" CellPadding="0" CellSpacing="0" Width="550px">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <Virtualia:VCoupleVerticalEtiDonnee ID="InfoV09" runat="server" DonTextMode="true" V_PointdeVue="1" V_Objet="69" V_Information="9" V_SiDonneeDico="true" EtiWidth="536px" DonWidth="534px" DonHeight="90px" DonTabIndex="14" EtiStyle="text-align:center; margin-left: 5px;" Donstyle="margin-left: 5px;" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Height="12px" />
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>
</asp:Table>
