﻿Option Strict On
Option Explicit On
Option Compare Text
Imports VI = Virtualia.Systeme.Constantes
Imports Microsoft.VisualBasic
Partial Class Fenetre_PER_SITUATION_CONGES
    Inherits System.Web.UI.UserControl
    Private WebFct As Virtualia.Net.WebFonctions
    Private WsIde_Dossier As Integer = 0
    'Execution
    Private WsNomStateIde As String = "VSituConges"
    Private WsNomStateParam As String = "VSituParam"
    Private WsCacheIde As ArrayList
    Private WsCacheParam As ArrayList
    Private WsDossierRtt As Virtualia.Metier.TempsTravail.DossierRTT

    Public Property Identifiant() As Integer
        Get
            If Me.ViewState(WsNomStateIde) IsNot Nothing Then
                WsCacheIde = CType(Me.ViewState(WsNomStateIde), ArrayList)
                WsIde_Dossier = CInt(WsCacheIde(0))
            End If
            Return WsIde_Dossier
        End Get
        Set(ByVal value As Integer)
            If Me.ViewState(WsNomStateIde) IsNot Nothing Then
                WsCacheIde = CType(Me.ViewState(WsNomStateIde), ArrayList)
                WsIde_Dossier = CInt(WsCacheIde(0))
            End If
            If value = WsIde_Dossier Then
                Exit Property
            End If
            WsIde_Dossier = value
            If WsIde_Dossier = 0 Then
                Exit Property
            End If
            If Me.ViewState(WsNomStateIde) IsNot Nothing Then
                Me.ViewState.Remove(WsNomStateIde)
            End If
            '** MettreEnCache le dossier **
            WsCacheIde = New ArrayList
            WsCacheIde.Add(WsIde_Dossier)
            Me.ViewState.Add(WsNomStateIde, WsCacheIde)
            '**
            WsDossierRtt = WebFct.PointeurUtilisateur.PointeurDllRTT.ItemDossier(WsIde_Dossier)
            If WsDossierRtt Is Nothing Then
                WebFct.PointeurUtilisateur.PointeurDllRTT.Identifiant(WsIde_Dossier) = _
                        WebFct.PointeurDossier(WsIde_Dossier).V_ListeDesFiches( _
                            WebFct.PointeurUtilisateur.PointeurDllRTT.ObjetsDllARTT)

                WsDossierRtt = WebFct.PointeurUtilisateur.PointeurDllRTT.ItemDossier(WsIde_Dossier)
            End If
        End Set
    End Property

    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        WebFct = New Virtualia.Net.WebFonctions(Me, 0)
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Call FaireSituation()
    End Sub

    Private Sub FaireSituation()
        If Identifiant = 0 Then
            Exit Sub
        End If
        WsDossierRtt = WebFct.PointeurUtilisateur.PointeurDllRTT.ItemDossier(WsIde_Dossier)
        If WsDossierRtt Is Nothing Then
            Exit Sub
        End If

        Dim Annee As Integer
        Dim DateDebut As String
        Dim DateFin As String
        Dim DateDebutConge As String
        Dim DateFinConge As String
        Dim IDConge As String
        Dim NbCalc As String
        Dim IndiceA As Integer
        Dim Numero As Integer
        Dim NbTotal As Integer
        Dim NBAVenir As Integer
        Dim Chaine As String
        Dim SiCpt1Visible As Boolean = False
        Dim SiCpt2Visible As Boolean = False
        Dim SiCpt3Visible As Boolean = False

        If Me.ViewState(WsNomStateParam) Is Nothing Then
            Annee = CInt(Strings.Right(WebFct.ViRhDates.DateduJour(False), 4))
            Select Case WebFct.PointeurGlobal.VirModele.InstanceProduit.ClefModele
                Case VI.ModeleRh.FPCollectivite, VI.ModeleRh.FPPompier, VI.ModeleRh.FPEtat, VI.ModeleRh.AfriquePublic
                    DateDebut = "01/01/" & Annee.ToString
                    DateFin = "31/12/" & Annee.ToString
                Case Else
                    If CInt(Strings.Mid(WebFct.ViRhDates.DateduJour(False), 4, 2)) > 5 Then
                        DateDebut = "01/06/" & Annee.ToString
                        DateFin = "31/05/" & (Annee + 1).ToString
                    Else
                        DateDebut = "01/06/" & (Annee - 1).ToString
                        DateFin = "31/05/" & Annee.ToString
                    End If
            End Select
            WsCacheParam = New ArrayList
            WsCacheParam.Add(DateDebut)
            WsCacheParam.Add(DateFin)
            Me.ViewState.Add(WsNomStateParam, WsCacheParam)
        Else
            WsCacheParam = CType(Me.ViewState(WsNomStateParam), ArrayList)
            DateDebut = WsCacheParam(0).ToString
            DateFin = WsCacheParam(1).ToString
        End If

        For IndiceA = 0 To 4
            IDConge = WebFct.PointeurUtilisateur.PointeurParametres.LibelleCompteur_Conge(IndiceA)
            Select Case IndiceA 'Recherche des périodes en fonction des dates de solde
                Case Is = 0
                    LabelTypeConge.Text = IDConge
                    Numero = 1630
                Case Is = 1
                    LabelTypeRTT.Text = IDConge
                    Numero = 1631
                Case Is = 2
                    If IDConge = "" Then
                        Exit For
                    End If
                    LabelTypeCompteur1.Text = IDConge
                    Numero = 1632
                Case Is = 3
                    If IDConge = "" Then
                        Exit For
                    End If
                    LabelTypeCompteur2.Text = IDConge
                    Numero = 1633
                Case Is = 4
                    If IDConge = "" Then
                        Exit For
                    End If
                    LabelTypeCompteur3.Text = IDConge
                    Numero = 1634
            End Select
            '** Périodes
            DateDebutConge = WsDossierRtt.Experte_Absences(DateDebut, DateFin, Numero, IDConge)
            DateFinConge = WsDossierRtt.Experte_Absences(DateDebut, DateFin, Numero + 5, IDConge)
            Select Case IndiceA
                Case Is = 0
                    LabelDetailsConges.Text = "période du " & DateDebutConge & " au " & DateFinConge
                    Numero = 1600
                Case Is = 1
                    LabelDetailsRTT.Text = "période du " & DateDebutConge & " au " & DateFinConge
                    Numero = 1601
                Case Is = 2
                    LabelDetailsCompteur1.Text = "période du " & DateDebutConge & " au " & DateFinConge
                    Numero = 1602
                Case Is = 3
                    If IDConge = "" Then
                        Exit For
                    End If
                    TableauSituationCompteur2.Visible = True
                    LabelTypeCompteur2.Text = IDConge
                    LabelDetailsCompteur2.Text = "période du " & DateDebutConge & " au " & DateFinConge
                    Numero = 1603
                Case Is = 4
                    If IDConge = "" Then
                        Exit For
                    End If
                    TableauSituationCompteur3.Visible = True
                    LabelTypeCompteur3.Text = IDConge
                    LabelDetailsCompteur3.Text = "période du " & DateDebutConge & " au " & DateFinConge
                    Numero = 1604
            End Select
            '** Droits
            NbCalc = WsDossierRtt.Experte_Absences(DateDebutConge, DateFinConge, Numero, IDConge)
            Select Case IndiceA
                Case Is = 0
                    LabelDroitsConges.Text = NbCalc
                    Numero = 1610
                Case Is = 1
                    LabelDroitsRTT.Text = NbCalc
                    Numero = 1611
                Case Is = 2
                    If CInt(NbCalc) > 0 Then
                        SiCpt1Visible = True
                    End If
                    LabelDroitsCompteur1.Text = NbCalc
                    Numero = 0
                Case Is = 3
                    If CInt(NbCalc) > 0 Then
                        SiCpt2Visible = True
                    End If
                    LabelDroitsCompteur2.Text = NbCalc
                    Numero = 0
                Case Is = 4
                    If CInt(NbCalc) > 0 Then
                        SiCpt3Visible = True
                    End If
                    LabelDroitsCompteur3.Text = NbCalc
                    Numero = 0
            End Select
            '** Détails des droits (Reports, fractionnement et anticipations)
            NbCalc = ""
            Chaine = ""
            If Numero > 0 Then
                NbCalc = WsDossierRtt.Experte_Absences(DateDebutConge, DateFinConge, Numero, IDConge) 'Reports
                If CInt(NbCalc) > 0 Then
                    Chaine = "dont " & NbCalc & " j. de report"
                End If
                If IndiceA = 0 Then 'Fractionement CA
                    NbCalc = WsDossierRtt.Experte_Absences(DateDebutConge, DateFinConge, 1570, IDConge) 'Fractionnement
                    If CInt(NbCalc) > 0 Then
                        If Chaine <> "" Then
                            Chaine &= ", "
                        Else
                            Chaine = "dont "
                        End If
                        Chaine &= NbCalc & " j. de fractionnement"
                    End If
                End If
                NbCalc = WsDossierRtt.Experte_Absences(DateDebutConge, DateFinConge, Numero + 10, IDConge) 'Anticipés
                If CInt(NbCalc) > 0 Then
                    If Chaine <> "" Then
                        Chaine &= ", "
                    End If
                    Chaine &= "moins " & NbCalc & " j. pris par anticipation"
                End If
            End If
            If Chaine = "" Then Chaine = "..."
            Select Case IndiceA
                Case Is = 0
                    LabelDetailsDroitsConges.Text = Chaine
                    Numero = 588
                Case Is = 1
                    LabelDetailsDroitsRTT.Text = Chaine
                    Numero = 745
                Case Is = 2
                    LabelDetailsDroitsCompteur1.Text = Chaine
                    If SiCpt1Visible = True Then
                        Numero = 1510
                    Else
                        Numero = 0
                    End If
                Case Is = 3
                    LabelDetailsDroitsCompteur2.Text = Chaine
                    If SiCpt2Visible = True Then
                        Numero = 1530
                    Else
                        Numero = 0
                    End If
                Case Is = 4
                    LabelDetailsDroitsCompteur3.Text = Chaine
                    If SiCpt3Visible = True Then
                        Numero = 1550
                    Else
                        Numero = 0
                    End If
            End Select
            '** Pris
            NbCalc = ""
            If Numero > 0 Then
                NbCalc = WsDossierRtt.Experte_Absences(DateDebutConge, DateFinConge, Numero, IDConge)
                Chaine = "soit " & NbCalc & " j. au total"
                NbTotal = CInt(NbCalc)
                NbCalc = WsDossierRtt.Experte_Absences(WebFct.ViRhDates.DateduJour(False), DateFinConge, Numero, IDConge)
                NBAVenir = CInt(NbCalc)
            End If
            Select Case IndiceA
                Case Is = 0
                    NbCalc = WsDossierRtt.Experte_Absences(DateDebutConge, DateFinConge, 1571, IDConge) '** pour CA hors période
                    If CInt(NbCalc) > 0 Then
                        Chaine &= " (dont " & NbCalc & " j. hors période)"
                    End If
                    LabelCongesPris.Text = Strings.Format(NbTotal - NBAVenir, "0.0")
                    LabelCongesAvenir.Text = Strings.Format(NBAVenir, "0.0")
                    LabelDetailsCongesPris.Text = Chaine
                    Numero = 686
                Case Is = 1
                    LabelRTTpris.Text = Strings.Format(NbTotal - NBAVenir, "0.0")
                    LabelRTTAvenir.Text = Strings.Format(NBAVenir, "0.0")
                    LabelDetailsRTTpris.Text = Chaine
                    Numero = 710
                Case Is = 2
                    If Numero > 0 Then
                        LabelCompteur1pris.Text = Strings.Format(NbTotal - NBAVenir, "0.0")
                        LabelCompteur1Avenir.Text = Strings.Format(NBAVenir, "0.0")
                        LabelDetailsCompteur1pris.Text = Chaine
                        Numero = 1500
                    End If
                Case Is = 3
                    If Numero > 0 Then
                        LabelCompteur2pris.Text = Strings.Format(NbTotal - NBAVenir, "0.0")
                        LabelCompteur2Avenir.Text = Strings.Format(NBAVenir, "0.0")
                        LabelDetailsCompteur2pris.Text = Chaine
                        Numero = 1520
                    End If
                Case Is = 4
                    If Numero > 0 Then
                        LabelCompteur3pris.Text = Strings.Format(NbTotal - NBAVenir, "0.0")
                        LabelCompteur3Avenir.Text = Strings.Format(NBAVenir, "0.0")
                        LabelDetailsCompteur3pris.Text = Chaine
                        Numero = 1540
                    End If
            End Select
            '**Soldes
            NbCalc = ""
            If Numero > 0 Then
                NbCalc = WsDossierRtt.Experte_Absences(DateDebutConge, DateFinConge, Numero, IDConge)
            End If
            Select Case IndiceA
                Case Is = 0
                    LabelSoldeConges.Text = NbCalc
                Case Is = 1
                    LabelSoldeRTT.Text = NbCalc
                Case Is = 2
                    LabelSoldeCompteur1.Text = NbCalc
                Case Is = 3
                    LabelSoldeCompteur2.Text = NbCalc
                Case Is = 4
                    LabelSoldeCompteur3.Text = NbCalc
            End Select

        Next IndiceA

        TableauSituationCompteur1.Visible = SiCpt1Visible
        TableauSituationCompteur2.Visible = SiCpt2Visible
        TableauSituationCompteur3.Visible = SiCpt3Visible

    End Sub
End Class
