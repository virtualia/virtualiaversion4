﻿<%@ Control Language="VB" AutoEventWireup="false" Inherits="Virtualia.Net.Fenetre_PER_STAGE_CV" Codebehind="PER_STAGE_CV.ascx.vb" %>

<%@ Register src="~/Controles/Saisies/VCoupleVerticalEtiDonnee.ascx" tagname="VCoupleVerticalEtiDonnee" tagprefix="Virtualia" %>

<%@ Register Src="~/ControlsGeneriques/V_CommandeOK.ascx" TagName="VCmdOK" TagPrefix="Generic" %>

  <asp:Table ID="CadreInfo" runat="server" BorderStyle="Ridge" BorderWidth="2px"  BorderColor="#B0E0D7" Height="280px" Width="500px" HorizontalAlign="Center">
    <asp:TableRow>
        <asp:TableCell>
            <Generic:VCmdOK ID="CtlOK" runat="server" Height="22px" Width="70px" HorizontalAlign="Right" CadreStyle="margin-top: 3px; margin-right: 3px" />
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
             <asp:Table ID="CadreTitre" runat="server" Height="40px" CellPadding="0"  CellSpacing="0" HorizontalAlign="Center">
                    <asp:TableRow>
                        <asp:TableCell HorizontalAlign="Center">
                            <asp:Label ID="Etiquette" runat="server" Text="Stages de formation" Height="20px" Width="300px" BackColor="#2FA49B" BorderColor="#B0E0D7" BorderStyle="Groove" BorderWidth="2px" ForeColor="#D7FAF3" Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Small" style="margin-top: 15px; margin-left: 4px; margin-bottom: 10px; font-style: oblique; text-indent: 5px; text-align: center;"/>
                        </asp:TableCell>      
                    </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell HorizontalAlign="Center">
            <Virtualia:VCoupleVerticalEtiDonnee ID="InfoV01" runat="server" DonTextMode="true" V_PointdeVue="1" V_Objet="11" V_Information="1" V_SiDonneeDico="true" EtiWidth="452px" DonWidth="450px" DonHeight="200px"  EtiVisible="false"/> 
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell Height="12px"/>
    </asp:TableRow>
  </asp:Table>
