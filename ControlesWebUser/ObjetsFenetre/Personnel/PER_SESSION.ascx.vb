﻿Option Strict On
Option Explicit On
Option Compare Text
Imports VI = Virtualia.Systeme.Constantes
Partial Class Fenetre_PER_SESSION
    Inherits Virtualia.Net.Controles.ObjetWebControle
    '
    Private WsNumObjet As Short = VI.ObjetPer.ObaFormation
    Private WsNomTable As String = "PER_SESSION"
    Private WsFiche As Virtualia.TablesObjet.ShemaPER.PER_SESSION

    Public ReadOnly Property MonCadre() As System.Web.UI.Control
        Get
            Return Me.CadreInfo
        End Get
    End Property

    Public WriteOnly Property CadreStyle() As String
        Set(ByVal value As String)
            Dim TableauData(0) As String
            Dim TableauW(0) As String
            Dim IndiceI As Integer
            TableauData = Strings.Split(value, ";")
            For IndiceI = 0 To TableauData.Count - 1
                If TableauData(IndiceI) = "" Then
                    Exit For
                End If
                TableauW = Strings.Split(TableauData(IndiceI), ":")
                CadreInfo.Style.Remove(Strings.Trim(TableauW(0)))
                CadreInfo.Style.Add(Strings.Trim(TableauW(0)), Strings.Trim(TableauW(1)))
            Next IndiceI
        End Set
    End Property

    Public WriteOnly Property Identifiant() As Integer
        Set(ByVal value As Integer)
            If V_Identifiant <> value Then

                Dim ColHisto As New ArrayList
                ColHisto.Add(0)
                ColHisto.Add(11)
                ColHisto.Add(4)
                ColHisto.Add(2)
                ColHisto.Add(8)
                V_CacheColHisto = ColHisto

                V_Identifiant = value
                Call ActualiserListe()
            End If
        End Set
    End Property

    Private Sub ActualiserListe()
        CadreCmdOK.Visible = False
        V_LibelListe = "Aucune session effecuée" & VI.Tild & "Une session effectuée" & VI.Tild & "sessions effectuées"
        V_LibelColonne = "du" & VI.Tild & "au" & VI.Tild & "intitulé du stage" & VI.Tild & "domaine" & VI.Tild & "jours" & VI.Tild & "Clef"

        If V_Identifiant > 0 Then
            Call InitialiserControles()
            If V_IndexFiche = -1 Then
                ListeGrille.V_Liste(V_LibelColonne, V_LibelListe) = ""
                Exit Sub
            End If
            ListeGrille.V_Liste(V_LibelColonne, V_LibelListe) = V_ListeFiches
        End If
    End Sub

    Protected Sub ListeGrille_ValeurChange(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.DonneeChangeEventArgs) Handles ListeGrille.ValeurChange
        If e.Valeur = "" Then
            Exit Sub
        End If
        V_Occurence = e.Valeur
    End Sub

    Private Sub LireLaFiche()
        Dim CacheDonnee As ArrayList = V_CacheMaj
        If CacheDonnee Is Nothing Then
            Exit Sub
        End If
        Dim NumInfo As Integer
        Dim Ctl As Control
        Dim VirControle As Controles_VCoupleEtiDonnee
        Dim IndiceI As Integer = 0

        Do
            Ctl = V_WebFonction.VirWebControle(Me.CadreInfo, "InfoH", IndiceI)
            If Ctl Is Nothing Then
                Exit Do
            End If
            NumInfo = CInt(Strings.Right(Ctl.ID, 2))
            VirControle = CType(Ctl, Controles_VCoupleEtiDonnee)
            If CacheDonnee(NumInfo) Is Nothing Then
                VirControle.DonText = ""
            Else
                VirControle.DonText = CacheDonnee(NumInfo).ToString
            End If
            VirControle.V_SiAutoPostBack = False
            IndiceI += 1
        Loop

        Dim VirDonneeTable As Controles_VDuoEtiquetteCommande
        IndiceI = 0
        Do
            Ctl = V_WebFonction.VirWebControle(Me.CadreInfo, "Dontab", IndiceI)
            If Ctl Is Nothing Then
                Exit Do
            End If
            NumInfo = CInt(Strings.Right(Ctl.ID, 2))
            VirDonneeTable = CType(Ctl, Controles_VDuoEtiquetteCommande)
            If CacheDonnee(NumInfo) Is Nothing Then
                VirDonneeTable.DonText = ""
            Else
                VirDonneeTable.DonText = CacheDonnee(NumInfo).ToString
            End If
            IndiceI += 1
        Loop

        Dim VirCoche As Controles_VCocheSimple
        IndiceI = 0
        Do
            Ctl = V_WebFonction.VirWebControle(Me.CadreInfo, "Coche", IndiceI)
            If Ctl Is Nothing Then
                Exit Do
            End If
            NumInfo = CInt(Strings.Right(Ctl.ID, 2))
            VirCoche = CType(Ctl, Controles_VCocheSimple)
            VirCoche.V_SiAutoPostBack = Not (CadreCmdOK.Visible)
            If CacheDonnee(NumInfo) Is Nothing Then
                VirCoche.V_Check = False
            Else
                If CacheDonnee(NumInfo).ToString = "Oui" Then
                    VirCoche.V_Check = True
                Else
                    VirCoche.V_Check = False
                End If
            End If
            IndiceI += 1
        Loop
    End Sub

    Protected Sub CommandeNew_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles CommandeNew.Click
        Call InitialiserControles()
        Call V_CommandeNewFiche()
        CadreCmdOK.Visible = True
    End Sub

    Protected Sub CommandeSupp_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles CommandeSupp.Click
        Call V_CommandeSuppFiche()
    End Sub

    Public WriteOnly Property RetourDialogueSupp(ByVal Cmd As String) As String
        Set(ByVal value As String)
            V_RetourDialogueSupp(Cmd) = value
            Call ActualiserListe()
        End Set
    End Property

    Protected Sub CommandeOK_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles CommandeOK.Click
        Call V_MajFiche()
        Call ActualiserListe()
        CadreCmdOK.Visible = False
    End Sub

    Protected Sub InfoH_ValeurChange(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.DonneeChangeEventArgs) Handles InfoH00.ValeurChange, _
    InfoH05.ValeurChange, InfoH08.ValeurChange, InfoH10.ValeurChange, InfoH11.ValeurChange, InfoH12.ValeurChange, _
    InfoH13.ValeurChange, InfoH14.ValeurChange, InfoH15.ValeurChange, InfoH16.ValeurChange, InfoH17.ValeurChange, _
    InfoH30.ValeurChange

        Dim CacheDonnee As ArrayList = V_CacheMaj
        Dim NumInfo As Integer = CInt(Strings.Right(CType(sender, Controles_VCoupleEtiDonnee).ID, 2))
        If CacheDonnee IsNot Nothing Then
            If CacheDonnee(NumInfo) Is Nothing Then
                CType(sender, Controles_VCoupleEtiDonnee).DonBackColor = V_WebFonction.CouleurMaj
                CadreCmdOK.Visible = True
            Else
                If CacheDonnee(NumInfo).ToString <> e.Valeur Then
                    CType(sender, Controles_VCoupleEtiDonnee).DonBackColor = V_WebFonction.CouleurMaj
                    CadreCmdOK.Visible = True
                End If
            End If
            Call V_ValeurChange(NumInfo, e.Valeur)
        End If
    End Sub

    Protected Sub Dontab_AppelTable(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.AppelTableEventArgs) Handles Dontab01.AppelTable, Dontab02.AppelTable, _
            Dontab03.AppelTable, Dontab04.AppelTable, Dontab06.AppelTable, Dontab07.AppelTable, Dontab09.AppelTable, _
            Dontab18.AppelTable, Dontab19.AppelTable, Dontab20.AppelTable, Dontab22.AppelTable, Dontab23.AppelTable, _
            Dontab24.AppelTable, Dontab25.AppelTable, Dontab26.AppelTable, Dontab27.AppelTable, _
            Dontab28.AppelTable, Dontab29.AppelTable, Dontab31.AppelTable, Dontab32.AppelTable

        Dim NumInfo As Integer = CInt(Strings.Right(CType(sender, Controles_VDuoEtiquetteCommande).ID, 2))
        Dim Evenement As Virtualia.Systeme.Evenements.AppelTableEventArgs
        Evenement = New Virtualia.Systeme.Evenements.AppelTableEventArgs(e.ControleAppelant, e.ObjetAppelant, e.PointdeVueInverse, e.NomdelaTable)
        Call V_AppelTable(Evenement)
    End Sub

    Public WriteOnly Property Dontab_RetourAppelTable(ByVal IDAppelant As String) As String
        Set(ByVal value As String)
            Dim NumInfo As Integer
            Dim Ctl As Control
            Dim VirDonneeTable As Controles_VDuoEtiquetteCommande
            Dim CacheDonnee As ArrayList = V_CacheMaj

            Ctl = V_WebFonction.VirWebControle(Me.CadreInfo, "Dontab" & Strings.Right(IDAppelant, 2), 0)
            If Ctl Is Nothing Then
                Exit Property
            End If
            NumInfo = CInt(Strings.Right(Ctl.ID, 2))
            VirDonneeTable = CType(Ctl, Controles_VDuoEtiquetteCommande)
            VirDonneeTable.DonText = value
            If CacheDonnee IsNot Nothing Then
                If CacheDonnee(NumInfo) Is Nothing Then
                    VirDonneeTable.DonBackColor = V_WebFonction.CouleurMaj
                Else
                    If CacheDonnee(NumInfo).ToString <> value Then
                        VirDonneeTable.DonBackColor = V_WebFonction.CouleurMaj
                    End If
                End If
                Call V_ValeurChange(NumInfo, value)
            End If
        End Set
    End Property

    Protected Sub Coche_ValeurChange(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.DonneeChangeEventArgs) Handles Coche33.ValeurChange, Coche34.ValeurChange

        Dim CacheDonnee As ArrayList = V_CacheMaj
        Dim NumInfo As Integer = CInt(Strings.Right(CType(sender, Controles_VCocheSimple).ID, 2))
        If CacheDonnee IsNot Nothing Then
            CadreCmdOK.Visible = True
            Call V_ValeurChange(NumInfo, e.Valeur)
        End If
    End Sub

    Protected Overrides Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        V_Objet(1) = WsNumObjet
        V_NomTableSgbd = WsNomTable
        V_Fiche = WsFiche
        ListeGrille.Centrage_Colonne(0) = 1
        ListeGrille.Centrage_Colonne(1) = 0
        ListeGrille.Centrage_Colonne(2) = 0
        ListeGrille.Centrage_Colonne(3) = 1
        ListeGrille.Centrage_Colonne(4) = 1
    End Sub

    Protected Overrides Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Call LireLaFiche()

        CadreInfo.BackColor = V_WebFonction.CouleurCharte(V_PointdeVue, "Cadre")
        CadreInfo.BorderColor = V_WebFonction.CouleurCharte(V_PointdeVue, "Bordure")
        Etiquette.BackColor = V_WebFonction.CouleurCharte(V_PointdeVue, "Titre")
        Etiquette.ForeColor = V_WebFonction.CouleurCharte(V_PointdeVue, "Police_Claire")
        Etiquette.BorderColor = V_WebFonction.CouleurCharte(V_PointdeVue, "Bordure")
        EtiComplements.BackColor = V_WebFonction.CouleurCharte(V_PointdeVue, "Sous-Titre")
        EtiComplements.ForeColor = V_WebFonction.CouleurCharte(V_PointdeVue, "Etiquette_ForeColor")
        EtiComplements.BorderColor = V_WebFonction.CouleurCharte(V_PointdeVue, "Bordure")
        EtiCouts.BackColor = V_WebFonction.CouleurCharte(V_PointdeVue, "Sous-Titre")
        EtiCouts.ForeColor = V_WebFonction.CouleurCharte(V_PointdeVue, "Etiquette_ForeColor")
        EtiCouts.BorderColor = V_WebFonction.CouleurCharte(V_PointdeVue, "Bordure")
        EtiSuivi.BackColor = V_WebFonction.CouleurCharte(V_PointdeVue, "Sous-Titre")
        EtiSuivi.ForeColor = V_WebFonction.CouleurCharte(V_PointdeVue, "Etiquette_ForeColor")
        EtiSuivi.BorderColor = V_WebFonction.CouleurCharte(V_PointdeVue, "Bordure")

    End Sub

    Private Sub InitialiserControles()
        Dim Ctl As Control
        Dim VirControle As Controles_VCoupleEtiDonnee
        Dim IndiceI As Integer = 0
        Do
            Ctl = V_WebFonction.VirWebControle(Me.CadreInfo, "InfoH", IndiceI)
            If Ctl Is Nothing Then
                Exit Do
            End If
            VirControle = CType(Ctl, Controles_VCoupleEtiDonnee)
            VirControle.DonBackColor = Drawing.Color.White
            VirControle.DonText = ""
            IndiceI += 1
        Loop
        Dim VirDonneeTable As Controles_VDuoEtiquetteCommande
        IndiceI = 0
        Do
            Ctl = V_WebFonction.VirWebControle(Me.CadreInfo, "Dontab", IndiceI)
            If Ctl Is Nothing Then
                Exit Do
            End If
            VirDonneeTable = CType(Ctl, Controles_VDuoEtiquetteCommande)
            VirDonneeTable.DonBackColor = Drawing.Color.White
            VirDonneeTable.DonText = ""
            IndiceI += 1
        Loop
        Dim VirExperte As Controles_VCoupleEtiquetteExperte
        IndiceI = 0
        Do
            Ctl = V_WebFonction.VirWebControle(Me.CadreInfo, "ExprH", IndiceI)
            If Ctl Is Nothing Then
                Exit Do
            End If
            VirExperte = CType(Ctl, Controles_VCoupleEtiquetteExperte)
            VirExperte.DonText = ""
            IndiceI += 1
        Loop
    End Sub

End Class

