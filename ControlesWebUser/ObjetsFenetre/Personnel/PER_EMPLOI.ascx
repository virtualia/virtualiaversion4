﻿<%@ Control Language="VB" AutoEventWireup="false" Inherits="Virtualia.Net.Fenetre_PER_EMPLOI" CodeBehind="PER_EMPLOI.ascx.vb" %>

<%@ Register Src="~/Controles/Saisies/VCoupleEtiDonnee.ascx" TagName="VCoupleEtiDonnee" TagPrefix="Virtualia" %>
<%@ Register Src="~/Controles/Saisies/VDuoEtiquetteCommande.ascx" TagName="VDuoEtiquetteCommande" TagPrefix="Virtualia" %>
<%@ Register Src="~/Controles/Saisies/VCoupleVerticalEtiDonnee.ascx" TagName="VCoupleVerticalEtiDonnee" TagPrefix="Virtualia" %>
<%@ Register Src="~/Controles/Saisies/VCoupleEtiquetteExperte.ascx" TagName="VCoupleEtiquetteExperte" TagPrefix="Virtualia" %>

<%@ Register Src="~/ControlsGeneriques/V_DataGrid.ascx" TagName="VDataGrid" TagPrefix="Generic" %>
<%@ Register Src="~/ControlsGeneriques/V_CommandeCRUD.ascx" TagName="VCrud" TagPrefix="Generic" %>

<asp:Table ID="CadreControle" runat="server" Height="30px" Width="600px" CellPadding="0" CellSpacing="0" HorizontalAlign="Center">
    <asp:TableRow>
        <asp:TableCell>
            <Generic:VDataGrid ID="ListeGrille" runat="server" CadreWidth="580px" SiColonneSelect="true" SiCaseAcocher="false" />
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="CadreInfo" runat="server" BorderStyle="Ridge" BorderWidth="2px" BorderColor="#B0E0D7" Height="520px" Width="700px" HorizontalAlign="Center" Style="margin-top: 3px;">
                <asp:TableRow>
                    <asp:TableCell>
                        <Generic:VCrud ID="CtlCrud" runat="server" Height="22px" Width="244px" HorizontalAlign="Right" CadreStyle="margin-top: 3px; margin-right: 3px" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Table ID="CadreTitre" runat="server" Height="40px" CellPadding="0" Width="550px" CellSpacing="0" HorizontalAlign="Center">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                    <asp:Label ID="Etiquette" runat="server" Text="Emploi conventionnel" Height="20px" Width="300px" BackColor="#2FA49B" BorderColor="#B0E0D7" BorderStyle="Groove" BorderWidth="2px" ForeColor="#D7FAF3" Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Small" Style="margin-top: 5px; margin-left: 4px; margin-bottom: 10px; font-style: oblique; text-indent: 5px; text-align: center;" />
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Table ID="CadreDates" runat="server" CellPadding="0" CellSpacing="0" Width="400px">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH00" runat="server" V_PointdeVue="1" V_Objet="14" V_Information="0" V_SiDonneeDico="true" EtiWidth="100px" DonWidth="80px" DonTabIndex="1" />
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Left">
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH09" runat="server" V_PointdeVue="1" V_Objet="14" V_Information="9" V_SiDonneeDico="true" EtiWidth="100px" DonWidth="100px" DonTabIndex="2" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Height="10px" ColumnSpan="2" />
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Table ID="TableauEmploi" runat="server" CellPadding="0" CellSpacing="0" Width="700px">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <Virtualia:VDuoEtiquetteCommande ID="Dontab01" runat="server" V_PointdeVue="1" V_Objet="14" V_Information="1" V_SiDonneeDico="true" EtiWidth="150px" DonWidth="350px" DonTabIndex="3" />
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Left">
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH12" runat="server" V_PointdeVue="1" V_Objet="14" V_Information="12" V_SiDonneeDico="true" EtiWidth="90px" DonWidth="50px" DonTabIndex="4" Etitext="Coefficient" EtiStyle="margin-left: 0px;" />
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Table ID="TableauEmploiConvention" runat="server" CellPadding="0" CellSpacing="0" Width="700px">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <Virtualia:VDuoEtiquetteCommande ID="Dontab13" runat="server" V_PointdeVue="1" V_Objet="14" V_Information="13" V_SiDonneeDico="true" EtiWidth="100px" DonWidth="220px" DonTabIndex="5" />
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Left">
                                    <Virtualia:VDuoEtiquetteCommande ID="Dontab11" runat="server" V_PointdeVue="1" V_Objet="14" V_Information="11" V_SiDonneeDico="true" EtiWidth="100px" DonWidth="220px" DonTabIndex="6" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <Virtualia:VDuoEtiquetteCommande ID="Dontab05" runat="server" V_PointdeVue="1" V_Objet="14" V_Information="5" V_SiDonneeDico="true" EtiWidth="100px" DonWidth="220px" DonTabIndex="7" />
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Left">
                                    <Virtualia:VDuoEtiquetteCommande ID="Dontab07" runat="server" V_PointdeVue="1" V_Objet="14" V_Information="7" V_SiDonneeDico="true" EtiWidth="100px" DonWidth="220px" DonTabIndex="8" />
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Table ID="TableauRemuneration" runat="server" CellPadding="0" CellSpacing="0" Width="500px">
                            <asp:TableRow>
                                <asp:TableCell Height="10px" ColumnSpan="4" />
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH02" runat="server" V_PointdeVue="1" V_Objet="14" V_Information="2" V_SiDonneeDico="true" EtiWidth="120px" DonWidth="70px" DonTabIndex="9" />
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Left">
                                    <asp:Label ID="LabelEuro2" runat="server" Text="€" Height="20px" Width="70px" BackColor="Transparent" BorderStyle="none" BorderWidth="0px" ForeColor="#2FA49B" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; font-style: oblique; text-indent: 0px; text-align: left;" />
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Left">
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH06" runat="server" V_PointdeVue="1" V_Objet="14" V_Information="6" V_SiDonneeDico="true" EtiWidth="90px" DonWidth="40px" DonTabIndex="10" />
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Left">
                                    <asp:Label ID="LabelNbrMois" runat="server" Text="mois" Height="20px" Width="70px" BackColor="Transparent" BorderStyle="none" BorderWidth="0px" ForeColor="#2FA49B" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; font-style: oblique; text-indent: 0px; text-align: left;" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH03" runat="server" V_PointdeVue="1" V_Objet="14" V_Information="3" V_SiDonneeDico="true" EtiWidth="120px" DonWidth="70px" DonTabIndex="11" />
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Left">
                                    <asp:Label ID="LabelEuro3" runat="server" Text="€" Height="20px" Width="70px" BackColor="Transparent" BorderStyle="none" BorderWidth="0px" ForeColor="#2FA49B" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; font-style: oblique; text-indent: 0px; text-align: left;" />
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Left">
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH04" runat="server" V_PointdeVue="1" V_Objet="14" V_Information="4" V_SiDonneeDico="true" EtiWidth="90px" DonWidth="40px" DonTabIndex="12" />
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Left">
                                    <asp:Label ID="LabelNbrPoints" runat="server" Text="pnts" Height="20px" Width="70px" BackColor="Transparent" BorderStyle="none" BorderWidth="0px" ForeColor="#2FA49B" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; font-style: oblique; text-indent: 0px; text-align: left;" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left" ColumnSpan="4">
                                    <Virtualia:VDuoEtiquetteCommande ID="Dontab14" runat="server" V_PointdeVue="1" V_Objet="14" V_Information="14" V_SiDonneeDico="true" EtiWidth="220px" DonWidth="276px" DonTabIndex="13" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left" ColumnSpan="4">
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH15" runat="server" V_PointdeVue="1" V_Objet="14" V_Information="15" V_SiDonneeDico="true" EtiWidth="220px" DonWidth="270px" DonTabIndex="14" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Height="10px" ColumnSpan="4" />
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left" ColumnSpan="4">
                                    <Virtualia:VDuoEtiquetteCommande ID="Dontab08" runat="server" V_PointdeVue="1" V_Objet="14" V_Information="8" V_SiDonneeDico="true" EtiWidth="140px" DonWidth="356px" DonTabIndex="15" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Height="10px" ColumnSpan="4" />
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Table ID="TableauObservation" runat="server" CellPadding="0" CellSpacing="0" Width="700px">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <Virtualia:VCoupleVerticalEtiDonnee ID="InfoV10" runat="server" DonTextMode="true" V_PointdeVue="1" V_Objet="14" V_Information="10" V_SiDonneeDico="true" EtiWidth="680px" DonWidth="678px" DonHeight="100px" DonTabIndex="16" EtiStyle="text-align:center; margin-left: 5px;" Donstyle="margin-left: 5px;" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Height="12px" />
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Panel ID="CadreExpert" runat="server" BorderStyle="None" BorderWidth="1px" BorderColor="#B0E0D7" Height="90px" Width="702px" HorizontalAlign="Left" Style="margin-top: 5px; vertical-align: middle">
                            <Virtualia:VCoupleEtiquetteExperte ID="ExprH01" runat="server" V_PointdeVue="1" V_Objet="14" V_InfoExperte="554" V_SiDonneeDico="true" EtiHeight="20px" EtiWidth="300px" DonWidth="100px" DonHeight="20px" EtiStyle="margin-left: 50px; margin-top: 5px" DonStyle="margin-top: 5px; text-align: center" />
                            <Virtualia:VCoupleEtiquetteExperte ID="ExprH02" runat="server" V_PointdeVue="1" V_Objet="14" V_InfoExperte="683" V_SiDonneeDico="true" EtiHeight="20px" EtiWidth="300px" DonWidth="100px" DonHeight="20px" EtiStyle="margin-left: 50px; margin-top: 2px" DonStyle="margin-top: 2px; text-align: center" />
                            <Virtualia:VCoupleEtiquetteExperte ID="ExprH03" runat="server" V_PointdeVue="1" V_Objet="14" V_InfoExperte="682" V_SiDonneeDico="true" EtiHeight="20px" EtiWidth="300px" DonWidth="100px" DonHeight="20px" EtiStyle="margin-left: 50px; margin-top: 2px" DonStyle="margin-top: 2px; text-align: center" />
                        </asp:Panel>
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>
</asp:Table>
