﻿<%@ Control Language="VB" AutoEventWireup="false" Inherits="Virtualia.Net.Fenetre_PER_LOLF" CodeBehind="PER_LOLF.ascx.vb" %>

<%@ Register Src="~/Controles/Saisies/VCoupleEtiDonnee.ascx" TagName="VCoupleEtiDonnee" TagPrefix="Virtualia" %>
<%@ Register Src="~/Controles/Saisies/VDuoEtiquetteCommande.ascx" TagName="VDuoEtiquetteCommande" TagPrefix="Virtualia" %>
<%@ Register Src="~/Controles/Saisies/VCoupleEtiquetteExperte.ascx" TagName="VCoupleEtiquetteExperte" TagPrefix="Virtualia" %>

<%@ Register Src="~/ControlsGeneriques/V_DataGrid.ascx" TagName="VDataGrid" TagPrefix="Generic" %>
<%@ Register Src="~/ControlsGeneriques/V_CommandeCRUD.ascx" TagName="VCrud" TagPrefix="Generic" %>

<asp:Table ID="CadreControle" runat="server" Height="30px" Width="600px" CellPadding="0" CellSpacing="0" HorizontalAlign="Center">
    <asp:TableRow>
        <asp:TableCell>
            <Generic:VDataGrid ID="ListeGrille" runat="server" CadreWidth="680px" SiColonneSelect="true" SiCaseAcocher="false" />
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="CadreInfo" runat="server" BorderStyle="Ridge" BorderWidth="2px" BorderColor="#B0E0D7" Height="350px" Width="700px" HorizontalAlign="Center" Style="margin-top: 3px;">
                <asp:TableRow>
                    <asp:TableCell>
                        <Generic:VCrud ID="CtlCrud" runat="server" Height="22px" Width="244px" HorizontalAlign="Right" CadreStyle="margin-top: 3px; margin-right: 3px" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Table ID="CadreTitre" runat="server" Height="40px" CellPadding="0" Width="700px" CellSpacing="0" HorizontalAlign="Center">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                    <asp:Label ID="Etiquette" runat="server" Text="Autorisation d'emploi - LOLF" Height="20px" Width="300px" BackColor="#2FA49B" BorderColor="#B0E0D7" BorderStyle="Solid" BorderWidth="2px" ForeColor="#D7FAF3" Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Small" Style="margin-top: 5px; margin-left: 4px; margin-bottom: 10px; font-style: oblique; text-indent: 5px; text-align: center;" />
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Table ID="CadreDon" runat="server" CellPadding="0" CellSpacing="0" Width="700px">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left" Width="300px">
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH00" runat="server" V_PointdeVue="1" V_Objet="92" V_Information="0" V_SiDonneeDico="true" DonWidth="100px" DonTabIndex="1" />
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Left" Width="300px">
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH05" runat="server" V_PointdeVue="1" V_Objet="92" V_Information="5" V_SiDonneeDico="true" EtiWidth="80px" DonWidth="100px" DonTabIndex="2" EtiStyle="margin-left:2px" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow Height="40px">
                                <asp:TableCell HorizontalAlign="Center" ColumnSpan="2">
                                    <asp:Label ID="EtiAction" runat="server" Height="20px" Width="300px" Text="Action / Sous-action" BackColor="#B0E0D7" BorderColor="#B0E0D7" BorderStyle="Outset" BorderWidth="2px" ForeColor="#142425" Font-Italic="true" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left" ColumnSpan="2">
                                    <Virtualia:VDuoEtiquetteCommande ID="Dontab01" runat="server" V_PointdeVue="1" V_Objet="92" V_Information="1" V_SiDonneeDico="true" DonTabIndex="4" EtiWidth="250px" DonWidth="300px" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left" ColumnSpan="2">
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH02" runat="server" V_PointdeVue="1" V_Objet="92" V_Information="2" V_SiDonneeDico="true" EtiWidth="250px" DonWidth="120px" DonTabIndex="5" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow Height="40px">
                                <asp:TableCell HorizontalAlign="Center" ColumnSpan="2">
                                    <asp:Label ID="EtiBOP" runat="server" Height="20px" Width="300px" Text="Budget Opérationnel de Programme" BackColor="#B0E0D7" BorderColor="#B0E0D7" BorderStyle="Outset" BorderWidth="2px" ForeColor="#142425" Font-Italic="true" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left" ColumnSpan="2">
                                    <Virtualia:VDuoEtiquetteCommande ID="Dontab03" runat="server" V_PointdeVue="1" V_Objet="92" V_Information="3" V_SiDonneeDico="true" DonTabIndex="6" EtiWidth="250px" DonWidth="300px" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left" ColumnSpan="2">
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH04" runat="server" V_PointdeVue="1" V_Objet="92" V_Information="4" V_SiDonneeDico="true" EtiWidth="250px" DonWidth="120px" DonTabIndex="7" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Height="12px" ColumnSpan="2" />
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Panel ID="CadreExpert" runat="server" BorderStyle="None" BorderWidth="1px" BorderColor="#B0E0D7" Height="250px" Width="700px" HorizontalAlign="Left" Style="margin-top: 5px; vertical-align: middle">
                            <Virtualia:VCoupleEtiquetteExperte ID="ExprH01" runat="server" V_PointdeVue="1" V_Objet="92" V_InfoExperte="880" V_SiDonneeDico="true" EtiHeight="20px" EtiWidth="250px" DonWidth="400px" DonHeight="20px" EtiStyle="margin-left: 20px; margin-top: 5px" DonStyle="margin-top: 9px; text-align: center" />
                            <Virtualia:VCoupleEtiquetteExperte ID="ExprH02" runat="server" V_PointdeVue="1" V_Objet="92" V_InfoExperte="881" V_SiDonneeDico="true" EtiHeight="20px" EtiWidth="250px" DonWidth="400px" DonHeight="20px" EtiStyle="margin-left: 20px;" DonStyle="text-align: center;" />
                            <Virtualia:VCoupleEtiquetteExperte ID="ExprH03" runat="server" V_PointdeVue="1" V_Objet="92" V_InfoExperte="882" V_SiDonneeDico="true" EtiHeight="20px" EtiWidth="250px" DonWidth="400px" DonHeight="20px" EtiStyle="margin-left: 20px;" DonStyle="text-align: center;" />
                            <Virtualia:VCoupleEtiquetteExperte ID="ExprH04" runat="server" V_PointdeVue="1" V_Objet="92" V_InfoExperte="883" V_SiDonneeDico="true" EtiHeight="20px" EtiWidth="250px" DonWidth="400px" DonHeight="20px" EtiStyle="margin-left: 20px;" DonStyle="text-align: center;" />
                            <Virtualia:VCoupleEtiquetteExperte ID="ExprH05" runat="server" V_PointdeVue="1" V_Objet="92" V_InfoExperte="884" V_SiDonneeDico="true" EtiHeight="20px" EtiWidth="250px" DonWidth="400px" DonHeight="20px" EtiStyle="margin-left: 20px;" DonStyle="text-align: center;" />
                            <Virtualia:VCoupleEtiquetteExperte ID="ExprH06" runat="server" V_PointdeVue="1" V_Objet="92" V_InfoExperte="885" V_SiDonneeDico="true" EtiHeight="20px" EtiWidth="250px" DonWidth="400px" DonHeight="20px" EtiStyle="margin-left: 20px;" DonStyle="text-align: center;" />
                            <Virtualia:VCoupleEtiquetteExperte ID="ExprH07" runat="server" V_PointdeVue="1" V_Objet="92" V_InfoExperte="886" V_SiDonneeDico="true" EtiHeight="20px" EtiWidth="250px" DonWidth="400px" DonHeight="20px" EtiStyle="margin-left: 20px;" DonStyle="text-align: center;" />
                        </asp:Panel>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="8px" />
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>
</asp:Table>
