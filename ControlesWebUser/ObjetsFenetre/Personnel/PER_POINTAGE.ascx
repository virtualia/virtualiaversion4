﻿<%@ Control Language="VB" AutoEventWireup="false" Inherits="Virtualia.Net.Fenetre_PER_POINTAGE" CodeBehind="PER_POINTAGE.ascx.vb" %>

<%@ Register Src="~/Controles/Saisies/VCoupleEtiDonnee.ascx" TagName="VCoupleEtiDonnee" TagPrefix="Virtualia" %>

<%@ Register Src="~/ControlsGeneriques/V_DataGridListeFiltre.ascx" TagName="DataGridFiltreListe" TagPrefix="Generic" %>
<%@ Register Src="~/ControlsGeneriques/V_CommandeCRUD.ascx" TagName="VCrud" TagPrefix="Generic" %>

<asp:Table ID="CadreControle" runat="server" Height="30px" Width="580px" CellPadding="0" CellSpacing="0" HorizontalAlign="Center">
    <asp:TableRow>
        <asp:TableCell>
            <Generic:DataGridFiltreListe ID="ListeGrille" runat="server" CadreWidth="680px" SiColonneSelect="true" SiListeFiltreVisible="false" />
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="CadreInfo" runat="server" BorderStyle="Ridge" BorderWidth="2px" BorderColor="#B0E0D7" Height="640px" Width="700px" HorizontalAlign="Center" Style="margin-top: 3px;">
                <asp:TableRow>
                    <asp:TableCell>
                        <Generic:VCrud ID="CtlCrud" runat="server" Height="22px" Width="244px" HorizontalAlign="Right" CadreStyle="margin-top: 3px; margin-right: 3px" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Table ID="CadreTitre" runat="server" Height="40px" CellPadding="0" Width="700px" CellSpacing="0" HorizontalAlign="Center">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                    <asp:Label ID="Etiquette" runat="server" Text="Pointages" Height="20px" Width="250px" BackColor="#2FA49B" BorderColor="#B0E0D7" BorderStyle="Groove" BorderWidth="2px" ForeColor="#D7FAF3" Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Small" Style="margin-top: 5px; margin-left: 4px; margin-bottom: 20px; font-style: oblique; text-indent: 5px; text-align: center;" />
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Table ID="CadreDates" runat="server" CellPadding="0" CellSpacing="0" Width="700px">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH00" runat="server" V_SiEnLectureSeule="true" V_PointdeVue="1" V_Objet="79" V_Information="0" V_SiDonneeDico="true" EtiWidth="100px" DonWidth="80px" DonTabIndex="1" />
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Left">
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH21" runat="server" V_SiEnLectureSeule="true" V_PointdeVue="1" V_Objet="79" V_Information="21" V_SiDonneeDico="true" EtiWidth="80px" DonWidth="390px" DonTabIndex="2" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Height="15px" ColumnSpan="2" />
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Table ID="TableauEntete" runat="server" CellPadding="0" CellSpacing="0" Width="655px">
                            <asp:TableRow>
                                <asp:TableCell>
                                    <asp:Label ID="EtiTableau" runat="server" Height="20px" Width="50px" Text="-" BackColor="#B0E0D7" BorderColor="#B0E0D7" BorderStyle="Outset" BorderWidth="2px" ForeColor="#B0E0D7" Font-Italic="true" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Style="margin-top: 1px; margin-left: 20px; text-indent: 1px; text-align: center" />
                                </asp:TableCell>
                                <asp:TableCell>
                                    <asp:Label ID="EtiBadgeage" runat="server" Height="20px" Width="110px" Text="Badgeage" BackColor="#B0E0D7" BorderColor="#B0E0D7" BorderStyle="Outset" BorderWidth="2px" ForeColor="#142425" Font-Italic="true" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Style="margin-top: 1px; margin-left: 4px; text-indent: 1px; text-align: center" />
                                </asp:TableCell>
                                <asp:TableCell>
                                    <asp:Label ID="EtiLieu" runat="server" Height="20px" Width="300px" Text="Lieu" BackColor="#B0E0D7" BorderColor="#B0E0D7" BorderStyle="Outset" BorderWidth="2px" ForeColor="#142425" Font-Italic="true" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Style="margin-top: 1px; margin-left: 5px; text-indent: 1px; text-align: center" />
                                </asp:TableCell>
                                <asp:TableCell>
                                    <asp:Label ID="EtiSens" runat="server" Height="20px" Width="150px" Text="Sens" BackColor="#B0E0D7" BorderColor="#B0E0D7" BorderStyle="Outset" BorderWidth="2px" ForeColor="#142425" Font-Italic="true" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Style="margin-top: 1px; margin-left: 5px; text-indent: 1px; text-align: center" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Height="3px" ColumnSpan="4" />
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Table ID="TableauPointages" runat="server" CellPadding="0" CellSpacing="0" Width="655px">
                            <asp:TableRow>
                                <asp:TableCell>
                                    <asp:Label ID="LabelPointage1" runat="server" Height="20px" Width="50px" Text="1er" BackColor="#B0E0D7" BorderColor="#B0E0D7" BorderStyle="Outset" BorderWidth="2px" ForeColor="#142425" Font-Italic="true" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Style="margin-top: 0px; margin-left: 20px; text-indent: 0px; text-align: center" />
                                </asp:TableCell>
                                <asp:TableCell>
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH01" runat="server" V_SiEnLectureSeule="true" V_PointdeVue="1" V_Objet="79" V_Information="1" V_SiDonneeDico="true" EtiVisible="false" DonWidth="108px" DonTabIndex="3" Donstyle="margin-left: 1px; text-align: center" />
                                </asp:TableCell>
                                <asp:TableCell>
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH02" runat="server" V_SiEnLectureSeule="true" V_PointdeVue="1" V_Objet="79" V_Information="2" V_SiDonneeDico="true" EtiVisible="false" DonWidth="299px" DonTabIndex="4" Donstyle="margin-left: 1px; text-align: center" />
                                </asp:TableCell>
                                <asp:TableCell>
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH03" runat="server" V_SiEnLectureSeule="true" V_PointdeVue="1" V_Objet="79" V_Information="3" V_SiDonneeDico="true" EtiVisible="false" DonWidth="149px" DonTabIndex="5" Donstyle="margin-left: 0px; text-align: center" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell>
                                    <asp:Label ID="LabelPointage2" runat="server" Height="20px" Width="50px" Text="2ème" BackColor="#B0E0D7" BorderColor="#B0E0D7" BorderStyle="Outset" BorderWidth="2px" ForeColor="#142425" Font-Italic="true" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Style="margin-top: 0px; margin-left: 20px; text-indent: 0px; text-align: center" />
                                </asp:TableCell>
                                <asp:TableCell>
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH04" runat="server" V_SiEnLectureSeule="true" V_PointdeVue="1" V_Objet="79" V_Information="4" V_SiDonneeDico="true" EtiVisible="false" DonWidth="108px" DonTabIndex="6" Donstyle="margin-left: 1px; text-align: center" />
                                </asp:TableCell>
                                <asp:TableCell>
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH05" runat="server" V_SiEnLectureSeule="true" V_PointdeVue="1" V_Objet="79" V_Information="5" V_SiDonneeDico="true" EtiVisible="false" DonWidth="299px" DonTabIndex="7" Donstyle="margin-left: 1px; text-align: center" />
                                </asp:TableCell>
                                <asp:TableCell>
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH06" runat="server" V_SiEnLectureSeule="true" V_PointdeVue="1" V_Objet="79" V_Information="6" V_SiDonneeDico="true" EtiVisible="false" DonWidth="149px" DonTabIndex="8" Donstyle="margin-left: 0px; text-align: center" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell>
                                    <asp:Label ID="LabelPointage3" runat="server" Height="20px" Width="50px" Text="3ème" BackColor="#B0E0D7" BorderColor="#B0E0D7" BorderStyle="Outset" BorderWidth="2px" ForeColor="#142425" Font-Italic="true" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Style="margin-top: 0px; margin-left: 20px; text-indent: 0px; text-align: center" />
                                </asp:TableCell>
                                <asp:TableCell>
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH07" runat="server" V_SiEnLectureSeule="true" V_PointdeVue="1" V_Objet="79" V_Information="7" V_SiDonneeDico="true" EtiVisible="false" DonWidth="108px" DonTabIndex="9" Donstyle="margin-left: 1px; text-align: center" />
                                </asp:TableCell>
                                <asp:TableCell>
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH08" runat="server" V_SiEnLectureSeule="true" V_PointdeVue="1" V_Objet="79" V_Information="8" V_SiDonneeDico="true" EtiVisible="false" DonWidth="299px" DonTabIndex="10" Donstyle="margin-left: 1px; text-align: center" />
                                </asp:TableCell>
                                <asp:TableCell>
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH09" runat="server" V_SiEnLectureSeule="true" V_PointdeVue="1" V_Objet="79" V_Information="9" V_SiDonneeDico="true" EtiVisible="false" DonWidth="149px" DonTabIndex="11" Donstyle="margin-left: 0px; text-align: center" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell>
                                    <asp:Label ID="LabelPointage4" runat="server" Height="20px" Width="50px" Text="4ème" BackColor="#B0E0D7" BorderColor="#B0E0D7" BorderStyle="Outset" BorderWidth="2px" ForeColor="#142425" Font-Italic="true" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Style="margin-top: 0px; margin-left: 20px; text-indent: 0px; text-align: center" />
                                </asp:TableCell>
                                <asp:TableCell>
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH10" runat="server" V_SiEnLectureSeule="true" V_PointdeVue="1" V_Objet="79" V_Information="10" V_SiDonneeDico="true" EtiVisible="false" DonWidth="108px" DonTabIndex="12" Donstyle="margin-left: 1px; text-align: center" />
                                </asp:TableCell>
                                <asp:TableCell>
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH11" runat="server" V_SiEnLectureSeule="true" V_PointdeVue="1" V_Objet="79" V_Information="11" V_SiDonneeDico="true" EtiVisible="false" DonWidth="299px" DonTabIndex="13" Donstyle="margin-left: 1px; text-align: center" />
                                </asp:TableCell>
                                <asp:TableCell>
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH12" runat="server" V_SiEnLectureSeule="true" V_PointdeVue="1" V_Objet="79" V_Information="12" V_SiDonneeDico="true" EtiVisible="false" DonWidth="149px" DonTabIndex="14" Donstyle="margin-left: 0px; text-align: center" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell>
                                    <asp:Label ID="LabelPointage5" runat="server" Height="20px" Width="50px" Text="5ème" BackColor="#B0E0D7" BorderColor="#B0E0D7" BorderStyle="Outset" BorderWidth="2px" ForeColor="#142425" Font-Italic="true" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Style="margin-top: 0px; margin-left: 20px; text-indent: 0px; text-align: center" />
                                </asp:TableCell>
                                <asp:TableCell>
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH13" runat="server" V_SiEnLectureSeule="true" V_PointdeVue="1" V_Objet="79" V_Information="13" V_SiDonneeDico="true" EtiVisible="false" DonWidth="108px" DonTabIndex="15" Donstyle="margin-left: 1px; text-align: center" />
                                </asp:TableCell>
                                <asp:TableCell>
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH14" runat="server" V_SiEnLectureSeule="true" V_PointdeVue="1" V_Objet="79" V_Information="14" V_SiDonneeDico="true" EtiVisible="false" DonWidth="299px" DonTabIndex="16" Donstyle="margin-left: 1px; text-align: center" />
                                </asp:TableCell>
                                <asp:TableCell>
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH15" runat="server" V_SiEnLectureSeule="true" V_PointdeVue="1" V_Objet="79" V_Information="15" V_SiDonneeDico="true" EtiVisible="false" DonWidth="149px" DonTabIndex="17" Donstyle="margin-left: 0px; text-align: center" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell>
                                    <asp:Label ID="LabelPointage6" runat="server" Height="20px" Width="50px" Text="6ème" BackColor="#B0E0D7" BorderColor="#B0E0D7" BorderStyle="Outset" BorderWidth="2px" ForeColor="#142425" Font-Italic="true" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Style="margin-top: 0px; margin-left: 20px; text-indent: 0px; text-align: center" />
                                </asp:TableCell>
                                <asp:TableCell>
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH16" runat="server" V_SiEnLectureSeule="true" V_PointdeVue="1" V_Objet="79" V_Information="16" V_SiDonneeDico="true" EtiVisible="false" DonWidth="108px" DonTabIndex="18" Donstyle="margin-left: 1px; text-align: center" />
                                </asp:TableCell>
                                <asp:TableCell>
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH17" runat="server" V_SiEnLectureSeule="true" V_PointdeVue="1" V_Objet="79" V_Information="17" V_SiDonneeDico="true" EtiVisible="false" DonWidth="299px" DonTabIndex="19" Donstyle="margin-left: 1px; text-align: center" />
                                </asp:TableCell>
                                <asp:TableCell>
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH18" runat="server" V_SiEnLectureSeule="true" V_PointdeVue="1" V_Objet="79" V_Information="18" V_SiDonneeDico="true" EtiVisible="false" DonWidth="149px" DonTabIndex="20" Donstyle="margin-left: 0px; text-align: center" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell>
                                    <asp:Label ID="LabelPointage7" runat="server" Height="20px" Width="50px" Text="7ème" BackColor="#B0E0D7" BorderColor="#B0E0D7" BorderStyle="Outset" BorderWidth="2px" ForeColor="#142425" Font-Italic="true" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Style="margin-top: 0px; margin-left: 20px; text-indent: 0px; text-align: center" />
                                </asp:TableCell>
                                <asp:TableCell>
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH28" runat="server" V_SiEnLectureSeule="true" V_PointdeVue="1" V_Objet="79" V_Information="28" V_SiDonneeDico="true" EtiVisible="false" DonWidth="108px" DonTabIndex="21" Donstyle="margin-left: 1px; text-align: center" />
                                </asp:TableCell>
                                <asp:TableCell>
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH29" runat="server" V_SiEnLectureSeule="true" V_PointdeVue="1" V_Objet="79" V_Information="29" V_SiDonneeDico="true" EtiVisible="false" DonWidth="299px" DonTabIndex="22" Donstyle="margin-left: 1px; text-align: center" />
                                </asp:TableCell>
                                <asp:TableCell>
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH30" runat="server" V_SiEnLectureSeule="true" V_PointdeVue="1" V_Objet="79" V_Information="30" V_SiDonneeDico="true" EtiVisible="false" DonWidth="149px" DonTabIndex="23" Donstyle="margin-left: 0px; text-align: center" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell>
                                    <asp:Label ID="LabelPointage8" runat="server" Height="20px" Width="50px" Text="8ème" BackColor="#B0E0D7" BorderColor="#B0E0D7" BorderStyle="Outset" BorderWidth="2px" ForeColor="#142425" Font-Italic="true" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Style="margin-top: 0px; margin-left: 20px; text-indent: 0px; text-align: center" />
                                </asp:TableCell>
                                <asp:TableCell>
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH31" runat="server" V_SiEnLectureSeule="true" V_PointdeVue="1" V_Objet="79" V_Information="31" V_SiDonneeDico="true" EtiVisible="false" DonWidth="108px" DonTabIndex="24" Donstyle="margin-left: 1px; text-align: center" />
                                </asp:TableCell>
                                <asp:TableCell>
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH32" runat="server" V_SiEnLectureSeule="true" V_PointdeVue="1" V_Objet="79" V_Information="32" V_SiDonneeDico="true" EtiVisible="false" DonWidth="299px" DonTabIndex="25" Donstyle="margin-left: 1px; text-align: center" />
                                </asp:TableCell>
                                <asp:TableCell>
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH33" runat="server" V_SiEnLectureSeule="true" V_PointdeVue="1" V_Objet="79" V_Information="33" V_SiDonneeDico="true" EtiVisible="false" DonWidth="149px" DonTabIndex="26" Donstyle="margin-left: 0px; text-align: center" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell>
                                    <asp:Label ID="LabelPointage9" runat="server" Height="20px" Width="50px" Text="9ème" BackColor="#B0E0D7" BorderColor="#B0E0D7" BorderStyle="Outset" BorderWidth="2px" ForeColor="#142425" Font-Italic="true" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Style="margin-top: 0px; margin-left: 20px; text-indent: 0px; text-align: center" />
                                </asp:TableCell>
                                <asp:TableCell>
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH34" runat="server" V_SiEnLectureSeule="true" V_PointdeVue="1" V_Objet="79" V_Information="34" V_SiDonneeDico="true" EtiVisible="false" DonWidth="108px" DonTabIndex="27" Donstyle="margin-left: 1px; text-align: center" />
                                </asp:TableCell>
                                <asp:TableCell>
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH35" runat="server" V_SiEnLectureSeule="true" V_PointdeVue="1" V_Objet="79" V_Information="35" V_SiDonneeDico="true" EtiVisible="false" DonWidth="299px" DonTabIndex="28" Donstyle="margin-left: 1px; text-align: center" />
                                </asp:TableCell>
                                <asp:TableCell>
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH36" runat="server" V_SiEnLectureSeule="true" V_PointdeVue="1" V_Objet="79" V_Information="36" V_SiDonneeDico="true" EtiVisible="false" DonWidth="149px" DonTabIndex="29" Donstyle="margin-left: 0px; text-align: center" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell>
                                    <asp:Label ID="LabelPointage10" runat="server" Height="20px" Width="50px" Text="10ème" BackColor="#B0E0D7" BorderColor="#B0E0D7" BorderStyle="Outset" BorderWidth="2px" ForeColor="#142425" Font-Italic="true" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Style="margin-top: 0px; margin-left: 20px; text-indent: 0px; text-align: center" />
                                </asp:TableCell>
                                <asp:TableCell>
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH37" runat="server" V_SiEnLectureSeule="true" V_PointdeVue="1" V_Objet="79" V_Information="37" V_SiDonneeDico="true" EtiVisible="false" DonWidth="108px" DonTabIndex="30" Donstyle="margin-left: 1px; text-align: center" />
                                </asp:TableCell>
                                <asp:TableCell>
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH38" runat="server" V_SiEnLectureSeule="true" V_PointdeVue="1" V_Objet="79" V_Information="38" V_SiDonneeDico="true" EtiVisible="false" DonWidth="299px" DonTabIndex="31" Donstyle="margin-left: 1px; text-align: center" />
                                </asp:TableCell>
                                <asp:TableCell>
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH39" runat="server" V_SiEnLectureSeule="true" V_PointdeVue="1" V_Objet="79" V_Information="39" V_SiDonneeDico="true" EtiVisible="false" DonWidth="149px" DonTabIndex="32" Donstyle="margin-left: 0px; text-align: center" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Height="15px" ColumnSpan="4" />
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Table ID="CadreLissage" runat="server" CellPadding="0" CellSpacing="0" Width="700px">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH20" runat="server" V_SiEnLectureSeule="true" V_PointdeVue="1" V_Objet="79" V_Information="20" V_SiDonneeDico="true" EtiWidth="130px" DonWidth="80px" DonTabIndex="33" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH19" runat="server" V_SiEnLectureSeule="true" V_PointdeVue="1" V_Objet="79" V_Information="19" V_SiDonneeDico="true" EtiWidth="130px" DonWidth="540px" DonTabIndex="34" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH22" runat="server" V_SiEnLectureSeule="true" V_PointdeVue="1" V_Objet="79" V_Information="22" V_SiDonneeDico="true" EtiWidth="150px" DonWidth="108px" DonTabIndex="35" Etistyle="margin-left: 140px;" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH23" runat="server" V_SiEnLectureSeule="true" V_PointdeVue="1" V_Objet="79" V_Information="23" V_SiDonneeDico="true" EtiWidth="150px" DonWidth="108px" DonTabIndex="36" Etistyle="margin-left: 140px;" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH24" runat="server" V_SiEnLectureSeule="true" V_PointdeVue="1" V_Objet="79" V_Information="24" V_SiDonneeDico="true" EtiWidth="150px" DonWidth="108px" DonTabIndex="37" Etistyle="margin-left: 140px;" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH25" runat="server" V_SiEnLectureSeule="true" V_PointdeVue="1" V_Objet="79" V_Information="25" V_SiDonneeDico="true" EtiWidth="150px" DonWidth="108px" DonTabIndex="38" Etistyle="margin-left: 140px;" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH26" runat="server" V_SiEnLectureSeule="true" V_PointdeVue="1" V_Objet="79" V_Information="26" V_SiDonneeDico="true" EtiWidth="150px" DonWidth="108px" DonTabIndex="39" Etistyle="margin-left: 140px;" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH27" runat="server" V_SiEnLectureSeule="true" V_PointdeVue="1" V_Objet="79" V_Information="27" V_SiDonneeDico="true" EtiWidth="150px" DonWidth="108px" DonTabIndex="40" Etistyle="margin-left: 140px;" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Height="12px" />
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>
</asp:Table>
