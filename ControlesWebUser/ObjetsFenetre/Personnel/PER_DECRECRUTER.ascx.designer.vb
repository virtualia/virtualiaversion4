﻿'------------------------------------------------------------------------------
' <généré automatiquement>
'     Ce code a été généré par un outil.
'
'     Les modifications apportées à ce fichier peuvent provoquer un comportement incorrect et seront perdues si
'     le code est régénéré.
' </généré automatiquement>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class Fenetre_PER_DECRECRUTER

    '''<summary>
    '''Contrôle CadreControle.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CadreControle As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle ListeGrille.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents ListeGrille As Global.Virtualia.Net.V_DataGrid

    '''<summary>
    '''Contrôle CadreInfo.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CadreInfo As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle CtlCrud.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CtlCrud As Global.Virtualia.Net.V_CommandeCRUD

    '''<summary>
    '''Contrôle CadreTitre.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CadreTitre As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle Etiquette.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Etiquette As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle CadreDates.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CadreDates As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle InfoH00.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoH00 As Global.Virtualia.Net.Controles_VCoupleEtiDonnee

    '''<summary>
    '''Contrôle InfoH02.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoH02 As Global.Virtualia.Net.Controles_VCoupleEtiDonnee

    '''<summary>
    '''Contrôle Dontab03.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Dontab03 As Global.Virtualia.Net.Controles_VDuoEtiquetteCommande

    '''<summary>
    '''Contrôle CadreDecision.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CadreDecision As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle InfoH01.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoH01 As Global.Virtualia.Net.Controles_VCoupleEtiDonnee

    '''<summary>
    '''Contrôle Dontab05.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Dontab05 As Global.Virtualia.Net.Controles_VDuoEtiquetteCommande

    '''<summary>
    '''Contrôle InfoH04.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoH04 As Global.Virtualia.Net.Controles_VCoupleEtiDonnee

    '''<summary>
    '''Contrôle Coche07.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Coche07 As Global.Virtualia.Net.Controles_VCocheSimple

    '''<summary>
    '''Contrôle TableauEnvoi.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents TableauEnvoi As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle InfoH06.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoH06 As Global.Virtualia.Net.Controles_VCoupleEtiDonnee

    '''<summary>
    '''Contrôle TableauIrrecevabilite.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents TableauIrrecevabilite As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle Coche08.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Coche08 As Global.Virtualia.Net.Controles_VCocheSimple

    '''<summary>
    '''Contrôle Dontab09.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Dontab09 As Global.Virtualia.Net.Controles_VDuoEtiquetteCommande

    '''<summary>
    '''Contrôle TableauAffectation.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents TableauAffectation As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle Dontab15.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Dontab15 As Global.Virtualia.Net.Controles_VDuoEtiquetteCommande

    '''<summary>
    '''Contrôle Dontab16.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Dontab16 As Global.Virtualia.Net.Controles_VDuoEtiquetteCommande

    '''<summary>
    '''Contrôle InfoH17.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoH17 As Global.Virtualia.Net.Controles_VCoupleEtiDonnee

    '''<summary>
    '''Contrôle TableauActivite.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents TableauActivite As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle InfoH19.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoH19 As Global.Virtualia.Net.Controles_VCoupleEtiDonnee

    '''<summary>
    '''Contrôle Dontab18.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Dontab18 As Global.Virtualia.Net.Controles_VDuoEtiquetteCommande

    '''<summary>
    '''Contrôle TableauAction.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents TableauAction As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle InfoH20.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoH20 As Global.Virtualia.Net.Controles_VCoupleEtiDonnee

    '''<summary>
    '''Contrôle InfoH14.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoH14 As Global.Virtualia.Net.Controles_VCoupleEtiDonnee

    '''<summary>
    '''Contrôle TableauHED.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents TableauHED As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle InfoH11.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoH11 As Global.Virtualia.Net.Controles_VCoupleEtiDonnee

    '''<summary>
    '''Contrôle LabelHeures11.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelHeures11 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle InfoH12.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoH12 As Global.Virtualia.Net.Controles_VCoupleEtiDonnee

    '''<summary>
    '''Contrôle LabelHeures12.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelHeures12 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle InfoH13.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoH13 As Global.Virtualia.Net.Controles_VCoupleEtiDonnee

    '''<summary>
    '''Contrôle LabelHeures13.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelHeures13 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle InfoH10.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoH10 As Global.Virtualia.Net.Controles_VCoupleEtiDonnee

    '''<summary>
    '''Contrôle LabelHeures10.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelHeures10 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle TableauCommission.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents TableauCommission As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle InfoV21.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoV21 As Global.Virtualia.Net.Controles_VCoupleVerticalEtiDonnee

    '''<summary>
    '''Contrôle InfoV22.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoV22 As Global.Virtualia.Net.Controles_VCoupleVerticalEtiDonnee

    '''<summary>
    '''Contrôle TableauAvisCommission.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents TableauAvisCommission As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle InfoH23.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoH23 As Global.Virtualia.Net.Controles_VCoupleEtiDonnee

    '''<summary>
    '''Contrôle Dontab24.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Dontab24 As Global.Virtualia.Net.Controles_VDuoEtiquetteCommande

    '''<summary>
    '''Contrôle TableauObservations.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents TableauObservations As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle InfoV25.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoV25 As Global.Virtualia.Net.Controles_VCoupleVerticalEtiDonnee
End Class
