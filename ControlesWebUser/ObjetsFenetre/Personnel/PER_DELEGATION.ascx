﻿<%@ Control Language="VB" AutoEventWireup="false" Inherits="Virtualia.Net.Fenetre_PER_DELEGATION" Codebehind="PER_DELEGATION.ascx.vb" %>

<%@ Register src="../../Saisies/VTrioVerticalRadio.ascx" tagname="VTrioVerticalRadio" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/Saisies/VCoupleEtiDonnee.ascx" tagname="VCoupleEtiDonnee" tagprefix="Virtualia" %>

<asp:Table ID="CadreInfo" runat="server" BorderStyle="None" BorderWidth="2px" Visible="true"
  BorderColor="#B0E0D7" Width="600px" HorizontalAlign="Center" style="margin-top: 3px;">
    
  <asp:TableRow>
    <asp:TableCell>
      <asp:Table ID="CadreTitre" runat="server" Height="30px" CellPadding="0" Width="600px" 
         CellSpacing="0" HorizontalAlign="Center" BorderColor="Black" Visible="true" BorderStyle="None">
            <asp:TableRow>
                <asp:TableCell HorizontalAlign="Center">
                    <asp:Label ID="Etiquette" runat="server" Text="Affectation d'un délégataire" Height="20px" Width="320px"
                        BackColor="#216B68" BorderColor="#B0E0D7" BorderStyle="Groove"
                        BorderWidth="2px" ForeColor="#D7FAF3"
                        Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Small"
                        style="margin-top: 10px; margin-left: 4px; margin-bottom: 20px;
                        font-style: oblique; text-indent: 0px; text-align: center;">
                    </asp:Label>          
                </asp:TableCell>      
            </asp:TableRow>
      </asp:Table>
    </asp:TableCell>
  </asp:TableRow>
  
  <asp:TableRow>
    <asp:TableCell >
        <asp:Table ID="TableDelegation" runat="server" CellPadding="0" CellSpacing="0" Width="570px" Height="130px"
        BorderStyle="Ridge" BorderWidth="2px" BorderColor="#B0E0D7" HorizontalAlign="Center" Visible="true">
          <asp:TableRow ID="LigneTitreDelegation">
            <asp:TableCell HorizontalAlign="Center">
                <asp:Label ID="LabelDelegataire" runat="server" Text="Délégataire" 
                    BackColor="#D7FAF3" BorderColor="#B0E0D7" BorderStyle="Solid" Height="24px" Width="193px"
                    BorderWidth="1px" ForeColor="#1C5151"
                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                    font-style: normal; text-indent: 0px; text-align: center;">
                </asp:Label>          
            </asp:TableCell> 
            <asp:TableCell HorizontalAlign="Center" ColumnSpan="2">
                <asp:Label ID="LabelDelegation" runat="server" Text="Délégation" 
                    BackColor="#CAEBE4" BorderColor="#B0E0D7" BorderStyle="Solid" Height="24px" Width="366px"
                    BorderWidth="1px" ForeColor="#1C5151"
                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                    font-style: normal; text-indent: 0px; text-align: center;">
                </asp:Label>          
            </asp:TableCell>     
          </asp:TableRow>
          <asp:TableRow ID="LigneDelegation">
            <asp:TableCell HorizontalAlign="Center"  
            BackColor="#D7FAF3" BorderColor="#B0E0D7" BorderStyle="None" BorderWidth="0px">
                <asp:Button ID="Delegataire" runat="server" Height="22px" Width="180px" 
                    BackColor="White" BorderColor="#B0E0D7" BorderStyle="Inset"
                    BorderWidth="2px" ForeColor="Black" Visible="true" Text="AMBROISE Julien"
                    Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Small" Font-Italic="false"
                    style="margin-top: 2px; text-align: left"> 
                </asp:Button>          
            </asp:TableCell> 
            <asp:TableCell HorizontalAlign="Left" ColumnSpan="2"
            BackColor="#CAEBE4" BorderColor="#B0E0D7" BorderStyle="None" BorderWidth="0px">
                <Virtualia:VTrioVerticalRadio ID="OptionDelegation" runat="server"  
                   RadioCentreText="Datée"  RadioGaucheText="Permanente" RadioDroiteText=""
                   V_Groupe="GroupeDelegation" V_SiDonneeDico="False" RadioDroiteVisible="false"
                   RadioGaucheWidth="105px" RadioCentreWidth="105px" RadioDroiteWidth="5px"
                   RadioGaucheBorderColor="#B0E0D7" RadioCentreBorderColor="#B0E0D7"
                   RadioGaucheBackColor="#98D4CA" RadioCentreBackColor="#98D4CA"
                   RadioGaucheStyle="text-align: left" RadioCentreStyle="text-align: left"/>          
            </asp:TableCell>     
          </asp:TableRow>
          <asp:TableRow ID="LigneDatesDelegation">
            <asp:TableCell HorizontalAlign="Left"
            BackColor="#D7FAF3" BorderColor="#B0E0D7" BorderStyle="None" BorderWidth="0px">             
            </asp:TableCell>
            <asp:TableCell HorizontalAlign="Left"
            BackColor="#CAEBE4" BorderColor="#B0E0D7" BorderStyle="None" BorderWidth="0px">
              <Virtualia:VCoupleEtiDonnee ID="DateDebutDelegation" runat="server"
                V_SiDonneeDico="false" EtiWidth="80px" DonWidth="80px" DonTabIndex="1" EtiText="du"/>              
            </asp:TableCell>
            <asp:TableCell HorizontalAlign="Left"
            BackColor="#CAEBE4" BorderColor="#B0E0D7" BorderStyle="None" BorderWidth="0px">
              <Virtualia:VCoupleEtiDonnee ID="DateFinDelegation" runat="server"
                V_SiDonneeDico="false" EtiWidth="80px" DonWidth="80px" DonTabIndex="2" EtiText="au"/>              
            </asp:TableCell>     
          </asp:TableRow>
        </asp:Table>
    </asp:TableCell>
  </asp:TableRow>
</asp:Table>
    