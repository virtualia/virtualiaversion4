﻿<%@ Control Language="VB" AutoEventWireup="false" Inherits="Virtualia.Net.Fenetre_PER_PIECE_IDENTITE" CodeBehind="PER_PIECE_IDENTITE.ascx.vb" %>

<%@ Register Src="~/Controles/Saisies/VCoupleEtiDonnee.ascx" TagName="VCoupleEtiDonnee" TagPrefix="Virtualia" %>
<%@ Register Src="~/Controles/Saisies/VDuoEtiquetteCommande.ascx" TagName="VDuoEtiquetteCommande" TagPrefix="Virtualia" %>

<%@ Register Src="~/ControlsGeneriques/V_CommandeOK.ascx" TagName="VCmdOK" TagPrefix="Generic" %>

<asp:Table ID="CadreInfo" runat="server" BorderStyle="Ridge" BorderWidth="2px" BorderColor="#B0E0D7" Height="600px" Width="750px" HorizontalAlign="Center">
    <asp:TableRow>
        <asp:TableCell>
            <Generic:VCmdOK ID="CtlOK" runat="server" Height="22px" Width="70px" HorizontalAlign="Right" CadreStyle="margin-top: 3px; margin-right: 3px" />
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="CadreTitre" runat="server" Height="40px" CellPadding="0" Width="750px" CellSpacing="0" HorizontalAlign="Center">
                <asp:TableRow>
                    <asp:TableCell>
                        <Virtualia:VCoupleEtiDonnee ID="InfoH18" runat="server" V_PointdeVue="1" V_Objet="31" V_Information="18" V_SiDonneeDico="true" EtiWidth="200px" DonTabIndex="101" EtiStyle="margin-top:30px;margin-left:100px" DonStyle="margin-top:30px" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        <Virtualia:VCoupleEtiDonnee ID="InfoH19" runat="server" V_PointdeVue="1" V_Objet="31" V_Information="19" V_SiDonneeDico="true" EtiWidth="200px" DonTabIndex="102" EtiStyle="margin-left:100px" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        <Virtualia:VCoupleEtiDonnee ID="InfoH16" runat="server" V_PointdeVue="1" V_Objet="31" V_Information="16" V_SiDonneeDico="true" EtiWidth="200px" DonWidth="120px" DonTabIndex="103" EtiStyle="margin-left:100px" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        <Virtualia:VCoupleEtiDonnee ID="InfoH17" runat="server" V_PointdeVue="1" V_Objet="31" V_Information="17" V_SiDonneeDico="true" EtiWidth="200px" DonWidth="250px" DonTabIndex="104" EtiStyle="margin-left:100px" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="EtiIdentite" runat="server" Text="Pièces d'identité" Height="20px" Width="300px" BackColor="#2FA49B" BorderColor="#B0E0D7" BorderStyle="Groove" BorderWidth="2px" ForeColor="#D7FAF3" Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Small" Style="margin-top: 15px; margin-left: 4px; margin-bottom: 10px; font-style: oblique; text-indent: 5px; text-align: center;" />
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="HorsFr" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="EtiHorFr" runat="server" Text="Si salarié né hors du territoire" Height="20px" Width="350px" BackColor="#216B68" BorderColor="#B0E0D7" BorderStyle="Outset" BorderWidth="2px" ForeColor="#D7FAF3" Font-Italic="true" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Style="margin-top: 0px; margin-left: 4px; margin-bottom: 2px; text-indent: 5px; text-align: center" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Width="400px" HorizontalAlign="Left">
                        <Virtualia:VCoupleEtiDonnee ID="InfoH07" runat="server" EtiText="Nom du père" V_PointdeVue="1" V_Objet="31" V_Information="7" V_SiDonneeDico="true" EtiWidth="180px" DonWidth="200px" DonTabIndex="105" />
                    </asp:TableCell>
                    <asp:TableCell Width="350px" HorizontalAlign="Left">
                        <Virtualia:VCoupleEtiDonnee ID="InfoH08" runat="server" EtiText="Prénom du père" V_PointdeVue="1" V_Objet="31" V_Information="8" V_SiDonneeDico="true" EtiWidth="120px" DonWidth="160px" DonTabIndex="106" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Width="400px" HorizontalAlign="Left">
                        <Virtualia:VCoupleEtiDonnee ID="InfoH09" runat="server" EtiText="Nom de naissance de la mère" V_PointdeVue="1" V_Objet="31" V_Information="9" V_SiDonneeDico="true" EtiWidth="180px" DonWidth="200px" DonTabIndex="107" />
                    </asp:TableCell>
                    <asp:TableCell Width="350px" HorizontalAlign="Left">
                        <Virtualia:VCoupleEtiDonnee ID="InfoH10" runat="server" EtiText="Prénom de la mère" V_PointdeVue="1" V_Objet="31" V_Information="10" V_SiDonneeDico="true" EtiWidth="120px" DonWidth="160px" DonTabIndex="108" />
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="Sejour" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                <asp:TableRow>
                    <asp:TableCell Width="375px">
                        <asp:Label ID="EtiCarteTravail" runat="server" Text="Carte de travail" Height="20px" Width="300px" BackColor="#216B68" BorderColor="#B0E0D7" BorderStyle="Outset" BorderWidth="2px" ForeColor="#D7FAF3" Font-Italic="true" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Style="margin-top: 20px; margin-left: 4px; margin-bottom: 2px; text-indent: 5px; text-align: center" />
                    </asp:TableCell>
                    <asp:TableCell Width="375px">
                        <asp:Label ID="EtiCarteSejour" runat="server" Text="Carte de séjour" Height="20px" Width="300px" BackColor="#216B68" BorderColor="#B0E0D7" BorderStyle="Outset" BorderWidth="2px" ForeColor="#D7FAF3" Font-Italic="true" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Style="margin-top: 20px; margin-left: 4px; margin-bottom: 2px; text-indent: 5px; text-align: center" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Width="375px" HorizontalAlign="Left">
                        <Virtualia:VDuoEtiquetteCommande ID="Dontab01" runat="server" V_PointdeVue="1" V_Objet="31" V_Information="1" V_SiDonneeDico="true" EtiWidth="120px" DonWidth="220px" DonTabIndex="109" />
                    </asp:TableCell>
                    <asp:TableCell Width="375px" HorizontalAlign="Left">
                        <Virtualia:VDuoEtiquetteCommande ID="Dontab04" runat="server" V_PointdeVue="1" V_Objet="31" V_Information="4" V_SiDonneeDico="true" EtiWidth="120px" DonWidth="220px" DonTabIndex="110" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Width="375px" HorizontalAlign="Left">
                        <Virtualia:VCoupleEtiDonnee ID="InfoH02" runat="server" V_PointdeVue="1" V_Objet="31" V_Information="2" V_SiDonneeDico="true" EtiWidth="120px" DonWidth="120px" DonTabIndex="111" />
                    </asp:TableCell>
                    <asp:TableCell Width="375px" HorizontalAlign="Left">
                        <Virtualia:VCoupleEtiDonnee ID="InfoH05" runat="server" V_PointdeVue="1" V_Objet="31" V_Information="5" V_SiDonneeDico="true" EtiWidth="120px" DonWidth="120px" DonTabIndex="112" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Width="375px" HorizontalAlign="Left">
                        <Virtualia:VCoupleEtiDonnee ID="InfoH03" runat="server" V_PointdeVue="1" V_Objet="31" V_Information="3" V_SiDonneeDico="true" EtiWidth="120px" DonWidth="150px" DonTabIndex="113" />
                    </asp:TableCell>
                    <asp:TableCell Width="375px" HorizontalAlign="Left">
                        <Virtualia:VCoupleEtiDonnee ID="InfoH06" runat="server" V_PointdeVue="1" V_Objet="31" V_Information="6" V_SiDonneeDico="true" EtiWidth="120px" DonWidth="150px" DonTabIndex="114" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Width="375px" HorizontalAlign="Left">
                        <Virtualia:VCoupleEtiDonnee ID="InfoH14" runat="server" V_PointdeVue="1" V_Objet="31" V_Information="14" V_SiDonneeDico="true" EtiWidth="120px" DonWidth="120px" DonTabIndex="115" />
                    </asp:TableCell>
                    <asp:TableCell Width="375px" HorizontalAlign="Left">
                        <Virtualia:VCoupleEtiDonnee ID="InfoH15" runat="server" V_PointdeVue="1" V_Objet="31" V_Information="15" V_SiDonneeDico="true" EtiWidth="120px" DonWidth="120px" DonTabIndex="116" />
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="Europe" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="EtiCEE" runat="server" Text="Si ressortissant de l'Union Européenne" Height="20px" Width="350px" BackColor="#216B68" BorderColor="#B0E0D7" BorderStyle="Outset" BorderWidth="2px" ForeColor="#D7FAF3" Font-Italic="true" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Style="margin-top: 20px; margin-left: 4px; text-indent: 5px; text-align: center; margin-bottom: 8px;" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        <Virtualia:VCoupleEtiDonnee ID="InfoH11" runat="server" V_PointdeVue="1" V_Objet="31" V_Information="11" V_SiDonneeDico="true" EtiWidth="220px" DonWidth="240px" DonTabIndex="117" EtiStyle="margin-top:10px;margin-left:100px" DonStyle="margin-top:10px" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        <Virtualia:VCoupleEtiDonnee ID="InfoH12" runat="server" V_PointdeVue="1" V_Objet="31" V_Information="12" V_SiDonneeDico="true" EtiWidth="220px" DonWidth="240px" DonTabIndex="118" EtiStyle="margin-left:100px" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        <Virtualia:VCoupleEtiDonnee ID="InfoH13" runat="server" V_PointdeVue="1" V_Objet="31" V_Information="13" V_SiDonneeDico="true" EtiWidth="220px" DonWidth="120px" DonTabIndex="119" EtiStyle="margin-left:100px" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="12px" />
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>
</asp:Table>
