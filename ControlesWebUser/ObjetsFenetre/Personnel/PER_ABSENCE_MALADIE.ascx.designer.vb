﻿'------------------------------------------------------------------------------
' <généré automatiquement>
'     Ce code a été généré par un outil.
'
'     Les modifications apportées à ce fichier peuvent provoquer un comportement incorrect et seront perdues si
'     le code est régénéré.
' </généré automatiquement>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class Fenetre_PER_ABSENCE_MALADIE

    '''<summary>
    '''Contrôle CadreInfo.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CadreInfo As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle TableauEnteteArretMaladie.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents TableauEnteteArretMaladie As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle LigneEnteteArretMaladie.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LigneEnteteArretMaladie As Global.System.Web.UI.WebControls.TableRow

    '''<summary>
    '''Contrôle LabelTitreTableau.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelTitreTableau As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle LigneEnteteAbsence.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LigneEnteteAbsence As Global.System.Web.UI.WebControls.TableRow

    '''<summary>
    '''Contrôle CellEnteteDebutAbsence.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellEnteteDebutAbsence As Global.System.Web.UI.WebControls.TableCell

    '''<summary>
    '''Contrôle LabelDebutAbsence.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelDebutAbsence As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle CellEnteteFinAbsence.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellEnteteFinAbsence As Global.System.Web.UI.WebControls.TableCell

    '''<summary>
    '''Contrôle LabelFinAbsence.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelFinAbsence As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle CellEnteteNbjCalendaires.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellEnteteNbjCalendaires As Global.System.Web.UI.WebControls.TableCell

    '''<summary>
    '''Contrôle LabelEnteteNbjCalendaires.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelEnteteNbjCalendaires As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle CellEntetePleinTrt.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellEntetePleinTrt As Global.System.Web.UI.WebControls.TableCell

    '''<summary>
    '''Contrôle LabelEntetePleinTrt.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelEntetePleinTrt As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle CellEnteteDemiTrt.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellEnteteDemiTrt As Global.System.Web.UI.WebControls.TableCell

    '''<summary>
    '''Contrôle LabelEnteteDemiTrt.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelEnteteDemiTrt As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle CellEnteteSansTrt.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellEnteteSansTrt As Global.System.Web.UI.WebControls.TableCell

    '''<summary>
    '''Contrôle LabelEnteteSansTrt.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelEnteteSansTrt As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle TableauArretMaladie.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents TableauArretMaladie As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle LigneSituationArretMaladie.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LigneSituationArretMaladie As Global.System.Web.UI.WebControls.TableRow

    '''<summary>
    '''Contrôle CellDateDebutArretMaladie.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellDateDebutArretMaladie As Global.System.Web.UI.WebControls.TableCell

    '''<summary>
    '''Contrôle LabelDateDebutArretMaladie.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelDateDebutArretMaladie As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle CellDateFinArretMaladie.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellDateFinArretMaladie As Global.System.Web.UI.WebControls.TableCell

    '''<summary>
    '''Contrôle LabelDateFinArretMaladie.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelDateFinArretMaladie As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle CellNbjCalendaires.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellNbjCalendaires As Global.System.Web.UI.WebControls.TableCell

    '''<summary>
    '''Contrôle LabelNbjCalendaires.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelNbjCalendaires As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle CellPleinTrt.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellPleinTrt As Global.System.Web.UI.WebControls.TableCell

    '''<summary>
    '''Contrôle LabelPleinTrt.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelPleinTrt As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle CellDemiTrt.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellDemiTrt As Global.System.Web.UI.WebControls.TableCell

    '''<summary>
    '''Contrôle LabelDemiTrt.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelDemiTrt As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle CellSansTrt.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellSansTrt As Global.System.Web.UI.WebControls.TableCell

    '''<summary>
    '''Contrôle LabelSansTrt.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelSansTrt As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle TableauRemuMaladie.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents TableauRemuMaladie As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle LigneSituationDebutMaladie.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LigneSituationDebutMaladie As Global.System.Web.UI.WebControls.TableRow

    '''<summary>
    '''Contrôle CellSituationDebutMaladie.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellSituationDebutMaladie As Global.System.Web.UI.WebControls.TableCell

    '''<summary>
    '''Contrôle LabelSituationDebutMaladie.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelSituationDebutMaladie As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle CellTitreDebutBaseRemuArrets.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellTitreDebutBaseRemuArrets As Global.System.Web.UI.WebControls.TableCell

    '''<summary>
    '''Contrôle LabelTitreDebutBaseRemuArrets.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelTitreDebutBaseRemuArrets As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle CellTitreDebutRemuArrets.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellTitreDebutRemuArrets As Global.System.Web.UI.WebControls.TableCell

    '''<summary>
    '''Contrôle LabelTitreDebutRemuArrets.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelTitreDebutRemuArrets As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle CellTitreDebutSolde.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellTitreDebutSolde As Global.System.Web.UI.WebControls.TableCell

    '''<summary>
    '''Contrôle LabelTitreDebutSolde.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelTitreDebutSolde As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle LigneEnteteDebutMaladie.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LigneEnteteDebutMaladie As Global.System.Web.UI.WebControls.TableRow

    '''<summary>
    '''Contrôle CellTitreDebutBase100.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellTitreDebutBase100 As Global.System.Web.UI.WebControls.TableCell

    '''<summary>
    '''Contrôle LabelTitreDebutBase100.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelTitreDebutBase100 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle CellTitreDebutBase50.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellTitreDebutBase50 As Global.System.Web.UI.WebControls.TableCell

    '''<summary>
    '''Contrôle LabelTitreDebutBase50.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelTitreDebutBase50 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle CellTitreDebutUtilTot.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellTitreDebutUtilTot As Global.System.Web.UI.WebControls.TableCell

    '''<summary>
    '''Contrôle LabelTitreDebutUtilTot.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelTitreDebutUtilTot As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle CellTitreDebutUtil100.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellTitreDebutUtil100 As Global.System.Web.UI.WebControls.TableCell

    '''<summary>
    '''Contrôle LabelTitreDebutUtil100.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelTitreDebutUtil100 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle CellTitreDebutUtil50.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellTitreDebutUtil50 As Global.System.Web.UI.WebControls.TableCell

    '''<summary>
    '''Contrôle LabelTitreDebutUtil50.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelTitreDebutUtil50 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle CellTitreDebutSolde100.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellTitreDebutSolde100 As Global.System.Web.UI.WebControls.TableCell

    '''<summary>
    '''Contrôle LabelTitreDebutSolde100.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelTitreDebutSolde100 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle CellTitreDebutSolde50.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellTitreDebutSolde50 As Global.System.Web.UI.WebControls.TableCell

    '''<summary>
    '''Contrôle LabelTitreDebutSolde50.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelTitreDebutSolde50 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle LigneDebutMaladie.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LigneDebutMaladie As Global.System.Web.UI.WebControls.TableRow

    '''<summary>
    '''Contrôle CellDebutBase100.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellDebutBase100 As Global.System.Web.UI.WebControls.TableCell

    '''<summary>
    '''Contrôle LabelDebutBase100.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelDebutBase100 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle CellDebutBase50.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellDebutBase50 As Global.System.Web.UI.WebControls.TableCell

    '''<summary>
    '''Contrôle LabelDebutBase50.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelDebutBase50 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle CellDebutUtilTot.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellDebutUtilTot As Global.System.Web.UI.WebControls.TableCell

    '''<summary>
    '''Contrôle LabelDebutUtilTot.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelDebutUtilTot As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle CellDebutUtil100.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellDebutUtil100 As Global.System.Web.UI.WebControls.TableCell

    '''<summary>
    '''Contrôle LabelDebutUtil100.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelDebutUtil100 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle CellDebutUtil50.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellDebutUtil50 As Global.System.Web.UI.WebControls.TableCell

    '''<summary>
    '''Contrôle LabelDebutUtil50.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelDebutUtil50 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle CellDebutSolde100.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellDebutSolde100 As Global.System.Web.UI.WebControls.TableCell

    '''<summary>
    '''Contrôle LabelDebutSolde100.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelDebutSolde100 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle CellDebutSolde50.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellDebutSolde50 As Global.System.Web.UI.WebControls.TableCell

    '''<summary>
    '''Contrôle LabelDebutSolde50.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelDebutSolde50 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle LigneSituationFinMaladie.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LigneSituationFinMaladie As Global.System.Web.UI.WebControls.TableRow

    '''<summary>
    '''Contrôle CellSituationFinMaladie.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellSituationFinMaladie As Global.System.Web.UI.WebControls.TableCell

    '''<summary>
    '''Contrôle LabelSituationFinMaladie.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelSituationFinMaladie As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle CellTitreFinBaseRemuArrets.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellTitreFinBaseRemuArrets As Global.System.Web.UI.WebControls.TableCell

    '''<summary>
    '''Contrôle LabelTitreFinBaseRemuArrets.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelTitreFinBaseRemuArrets As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle CellTitreFinRemuArrets.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellTitreFinRemuArrets As Global.System.Web.UI.WebControls.TableCell

    '''<summary>
    '''Contrôle LabelTitreFinRemuArrets.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelTitreFinRemuArrets As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle CellTitreFinSolde.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellTitreFinSolde As Global.System.Web.UI.WebControls.TableCell

    '''<summary>
    '''Contrôle LabelTitreFinSolde.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelTitreFinSolde As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle LigneEnteteFinMaladie.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LigneEnteteFinMaladie As Global.System.Web.UI.WebControls.TableRow

    '''<summary>
    '''Contrôle CellTitreFinBase100.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellTitreFinBase100 As Global.System.Web.UI.WebControls.TableCell

    '''<summary>
    '''Contrôle LabelTitreFinBase100.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelTitreFinBase100 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle CellTitreFinBase50.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellTitreFinBase50 As Global.System.Web.UI.WebControls.TableCell

    '''<summary>
    '''Contrôle LabelTitreFinBase50.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelTitreFinBase50 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle CellTitreFinUtilTot.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellTitreFinUtilTot As Global.System.Web.UI.WebControls.TableCell

    '''<summary>
    '''Contrôle LabelTitreFinUtilTot.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelTitreFinUtilTot As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle CellTitreFinUtil100.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellTitreFinUtil100 As Global.System.Web.UI.WebControls.TableCell

    '''<summary>
    '''Contrôle LabelTitreFinUtil100.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelTitreFinUtil100 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle CellTitreFinUtil50.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellTitreFinUtil50 As Global.System.Web.UI.WebControls.TableCell

    '''<summary>
    '''Contrôle LabelTitreFinUtil50.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelTitreFinUtil50 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle CellTitreFinSolde100.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellTitreFinSolde100 As Global.System.Web.UI.WebControls.TableCell

    '''<summary>
    '''Contrôle LabelTitreFinSolde100.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelTitreFinSolde100 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle CellTitreFinSolde50.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellTitreFinSolde50 As Global.System.Web.UI.WebControls.TableCell

    '''<summary>
    '''Contrôle LabelTitreFinSolde50.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelTitreFinSolde50 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle LigneFinMaladie.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LigneFinMaladie As Global.System.Web.UI.WebControls.TableRow

    '''<summary>
    '''Contrôle CellFinBase100.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellFinBase100 As Global.System.Web.UI.WebControls.TableCell

    '''<summary>
    '''Contrôle LabelFinBase100.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelFinBase100 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle CellFinBase50.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellFinBase50 As Global.System.Web.UI.WebControls.TableCell

    '''<summary>
    '''Contrôle LabelFinBase50.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelFinBase50 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle CellFinUtilTot.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellFinUtilTot As Global.System.Web.UI.WebControls.TableCell

    '''<summary>
    '''Contrôle LabelFinUtilTot.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelFinUtilTot As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle CellFinUtil100.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellFinUtil100 As Global.System.Web.UI.WebControls.TableCell

    '''<summary>
    '''Contrôle LabelFinUtil100.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelFinUtil100 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle CellFinUtil50.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellFinUtil50 As Global.System.Web.UI.WebControls.TableCell

    '''<summary>
    '''Contrôle LabelFinUtil50.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelFinUtil50 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle CellFinSolde100.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellFinSolde100 As Global.System.Web.UI.WebControls.TableCell

    '''<summary>
    '''Contrôle LabelFinSolde100.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelFinSolde100 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle CellFinSolde50.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellFinSolde50 As Global.System.Web.UI.WebControls.TableCell

    '''<summary>
    '''Contrôle LabelFinSolde50.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelFinSolde50 As Global.System.Web.UI.WebControls.Label
End Class
