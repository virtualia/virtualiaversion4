﻿<%@ Control Language="VB" AutoEventWireup="false" Inherits="Virtualia.Net.Fenetre_PER_ABSENCE_MALADIE" Codebehind="PER_ABSENCE_MALADIE.ascx.vb" %>
<asp:Table ID="CadreInfo" runat="server" BorderStyle="None" BorderWidth="2px" Visible="true"
  BorderColor="#B0E0D7" Height="190px" Width="750px" HorizontalAlign="Center" style="margin-top: 3px;">
  
  <asp:TableRow>
    <asp:TableCell>
        <asp:Table ID="TableauEnteteArretMaladie" runat="server" CellPadding="0" CellSpacing="0" Width="750px"
        BorderStyle="Notset" BorderWidth="1px" BorderColor="#D7FAF3" HorizontalAlign="Center" Visible="true">
            <asp:TableRow ID="LigneEnteteArretMaladie" Height="22px">
                <asp:TableCell HorizontalAlign="Center" ColumnSpan="6">
                    <asp:Label ID="LabelTitreTableau" runat="server" Height="21px" Width="746px"
                        BackColor="#CAEBE4" BorderColor="#D7FAF3"  BorderStyle="Notset"
                        BorderWidth="1px" ForeColor="#1C5151" Text="Congé de maladie ordinaire"
                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                        style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                        font-style: normal; text-indent: 0px; text-align: center; padding-top: 3px;">
                    </asp:Label>          
                </asp:TableCell>      
            </asp:TableRow>
            <asp:TableRow ID="LigneEnteteAbsence" Height="22px">
             <asp:TableCell ID="CellEnteteDebutAbsence" HorizontalAlign="Center"> 
              <asp:Label ID="LabelDebutAbsence" runat="server" Height="21px" Width="90px"
                  BackColor="#A8BBB8" BorderColor="#D7FAF3" BorderStyle="Solid" Text="Du"
                  BorderWidth="1px" ForeColor="#D7FAF3" Font-Italic="False"
                  Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                  style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                  font-style: normal; text-indent: 0px; text-align:  center; padding-top: 3px;">
              </asp:Label>          
             </asp:TableCell>
             <asp:TableCell ID="CellEnteteFinAbsence" HorizontalAlign="Center"> 
              <asp:Label ID="LabelFinAbsence" runat="server" Height="21px" Width="90px" 
                  BackColor="#A8BBB8" BorderColor="#D7FAF3" BorderStyle="Solid" Text="Au"
                  BorderWidth="1px" ForeColor="#D7FAF3" Font-Italic="False"
                  Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                  style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                  font-style: normal; text-indent: 0px;  text-align:  center; padding-top: 3px;">
              </asp:Label>          
             </asp:TableCell>
             <asp:TableCell ID="CellEnteteNbjCalendaires" HorizontalAlign="Center"> 
              <asp:Label ID="LabelEnteteNbjCalendaires" runat="server" Height="21px" Width="139px"
                  BackColor="#A8BBB8" BorderColor="#D7FAF3" BorderStyle="Solid" Text="Jours calendaires"
                  BorderWidth="1px" ForeColor="#D7FAF3" Font-Italic="False"
                  Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                  style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; 
                  font-style: normal; text-indent: 0px; text-align:  center; padding-top: 3px;">
              </asp:Label>          
             </asp:TableCell>
             <asp:TableCell ID="CellEntetePleinTrt" HorizontalAlign="Center"> 
              <asp:Label ID="LabelEntetePleinTrt" runat="server" Height="21px" Width="139px"
                  BackColor="#A8BBB8" BorderColor="#D7FAF3" BorderStyle="Solid" Text="Plein traitement"
                  BorderWidth="1px" ForeColor="#D7FAF3" Font-Italic="False"
                  Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                  style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; 
                  font-style: normal; text-indent: 0px; text-align:  center; padding-top: 3px;">
              </asp:Label>          
             </asp:TableCell>
             <asp:TableCell ID="CellEnteteDemiTrt" HorizontalAlign="Center"> 
              <asp:Label ID="LabelEnteteDemiTrt" runat="server" Height="21px" Width="139px"
                  BackColor="#A8BBB8" BorderColor="#D7FAF3" BorderStyle="Solid" Text="Demi traitement"
                  BorderWidth="1px" ForeColor="#D7FAF3" Font-Italic="False"
                  Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                  style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; 
                  font-style: normal; text-indent: 0px; text-align:  center; padding-top: 3px;">
              </asp:Label>          
             </asp:TableCell>
             <asp:TableCell ID="CellEnteteSansTrt" HorizontalAlign="Center"> 
              <asp:Label ID="LabelEnteteSansTrt" runat="server" Height="21px" Width="139px"
                  BackColor="#A8BBB8" BorderColor="#D7FAF3" BorderStyle="Solid" Text="Sans traitement"
                  BorderWidth="1px" ForeColor="#D7FAF3" Font-Italic="False"
                  Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                  style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; 
                  font-style: normal; text-indent: 0px; text-align:  center; padding-top: 3px;">
              </asp:Label>          
             </asp:TableCell>
           </asp:TableRow>
        </asp:Table>
    </asp:TableCell>
  </asp:TableRow>
  
  <asp:TableRow>
    <asp:TableCell>
        <asp:Table ID="TableauArretMaladie" runat="server" CellPadding="0" CellSpacing="0" Width="750px"
        BorderStyle="Notset" BorderWidth="1px" BorderColor="#B0E0D7" HorizontalAlign="Left" Visible="true">
            <asp:TableRow ID="LigneSituationArretMaladie" Height="22px">
             <asp:TableCell ID="CellDateDebutArretMaladie" HorizontalAlign="Center" Width="90px"> 
              <asp:Label ID="LabelDateDebutArretMaladie" runat="server" Height="22px" Width="90px"
                  BackColor="#DBF5EF" BorderColor="#CAEBE4" BorderStyle="Solid" Text="04/05/2010"
                  BorderWidth="1px" ForeColor="#1C5151" Font-Italic="False"
                  Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                  style="margin-top: 0px; margin-left: -1px; margin-bottom: 0px;
                  font-style: normal; text-indent: 0px; text-align:  center; padding-top: 1px;">
              </asp:Label>          
             </asp:TableCell>
             <asp:TableCell ID="CellDateFinArretMaladie" HorizontalAlign="Center" Width="90px"> 
              <asp:Label ID="LabelDateFinArretMaladie" runat="server" Height="22px" Width="90px"
                  BackColor="#DBF5EF" BorderColor="#CAEBE4" BorderStyle="Solid" Text="20/05/2010"
                  BorderWidth="1px" ForeColor="#1C5151" Font-Italic="False"
                  Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                  style="margin-top: 0px; margin-left: -1px; margin-bottom: 0px;
                  font-style: normal; text-indent: 0px; text-align:  center; padding-top: 1px;">
              </asp:Label>          
             </asp:TableCell>
             <asp:TableCell ID="CellNbjCalendaires" HorizontalAlign="Center" BackColor="#f0f0f0" Width="139px"> 
              <asp:Label ID="LabelNbjCalendaires" runat="server" Height="22px" Width="50px"
                  BackColor="White" BorderColor="#CAEBE4" BorderStyle="Solid" Text="17,0"
                  BorderWidth="1px" ForeColor="#1C5151" Font-Italic="False"
                  Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                  style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; 
                  font-style: normal; text-indent: 0px; text-align:  center; padding-top: 1px;">
              </asp:Label>          
             </asp:TableCell>
             <asp:TableCell ID="CellPleinTrt" HorizontalAlign="Center" BackColor="#f0f0f0" Width="139px"> 
              <asp:Label ID="LabelPleinTrt" runat="server" Height="22px" Width="50px"
                  BackColor="White" BorderColor="#CAEBE4" BorderStyle="Solid" Text="10,0"
                  BorderWidth="1px" ForeColor="#1C5151" Font-Italic="False"
                  Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                  style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; 
                  font-style: normal; text-indent: 0px; text-align:  center; padding-top: 1px;">
              </asp:Label>          
             </asp:TableCell>
             <asp:TableCell ID="CellDemiTrt" HorizontalAlign="Center" BackColor="#f0f0f0" Width="139px"> 
              <asp:Label ID="LabelDemiTrt" runat="server" Height="22px" Width="50px"
                  BackColor="White" BorderColor="#CAEBE4" BorderStyle="Solid" Text="7,0"
                  BorderWidth="1px" ForeColor="#1C5151" Font-Italic="False"
                  Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                  style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; 
                  font-style: normal; text-indent: 0px; text-align:  center; padding-top: 1px;">
              </asp:Label>          
             </asp:TableCell>
             <asp:TableCell ID="CellSansTrt" HorizontalAlign="Center" BackColor="#f0f0f0" Width="139px"> 
              <asp:Label ID="LabelSansTrt" runat="server" Height="22px" Width="50px"
                  BackColor="White" BorderColor="#CAEBE4" BorderStyle="Solid" Text="0,0"
                  BorderWidth="1px" ForeColor="#1C5151" Font-Italic="False"
                  Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                  style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; 
                  font-style: normal; text-indent: 0px; text-align:  center; padding-top: 1px;">
              </asp:Label>          
             </asp:TableCell>
           </asp:TableRow>
        </asp:Table>
    </asp:TableCell>
  </asp:TableRow>

  <asp:TableRow>
    <asp:TableCell Height="9px"></asp:TableCell>
  </asp:TableRow>

  <asp:TableRow>
    <asp:TableCell>
        <asp:Table ID="TableauRemuMaladie" runat="server" CellPadding="0" CellSpacing="0" Width="750px"
        BorderStyle="Notset" BorderWidth="1px" BorderColor="#B0E0D7" HorizontalAlign="Left" Visible="true">
            <asp:TableRow ID="LigneSituationDebutMaladie" Height="22px">
             <asp:TableCell ID="CellSituationDebutMaladie" HorizontalAlign="Center" Width="170px" RowSpan="3"
             BackColor="#6C9690" BorderColor="#E2FDF7" BorderStyle="Solid" BorderWidth="1px"> 
              <asp:Label ID="LabelSituationDebutMaladie" runat="server" Height="22px" Width="169px"
                  BackColor="#6C9690" BorderColor="#6C9690" BorderStyle="Solid" Text="Situation au 04/05/2010"
                  BorderWidth="1px" ForeColor="White" Font-Italic="False"
                  Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                  style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                  font-style: normal; text-indent: 0px; text-align:  center; padding-top: 1px;">
              </asp:Label>          
             </asp:TableCell>
             <asp:TableCell ID="CellTitreDebutBaseRemuArrets" HorizontalAlign="Center" Width="190px" ColumnSpan="2"> 
              <asp:Label ID="LabelTitreDebutBaseRemuArrets" runat="server" Height="22px" Width="190px"
                  BackColor="#A4E3DB" BorderColor="#E2FDF7" BorderStyle="Solid" Text="Base de rémunération"
                  BorderWidth="1px" ForeColor="#298276" Font-Italic="False"
                  Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                  style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                  font-style: normal; text-indent: 0px; text-align:  center; padding-top: 1px;">
              </asp:Label>          
             </asp:TableCell>
             <asp:TableCell ID="CellTitreDebutRemuArrets" HorizontalAlign="Center" Width="190px" ColumnSpan="3"> 
              <asp:Label ID="LabelTitreDebutRemuArrets" runat="server" Height="22px" Width="190px"
                  BackColor="#A4E3DB" BorderColor="#E2FDF7" BorderStyle="Solid" Text="Rémunération"
                  BorderWidth="1px" ForeColor="#298276" Font-Italic="False"
                  Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                  style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; 
                  font-style: normal; text-indent: 0px; text-align:  center; padding-top: 1px;">
              </asp:Label>          
             </asp:TableCell>
             <asp:TableCell ID="CellTitreDebutSolde" HorizontalAlign="Center" Width="190px" ColumnSpan="2"> 
              <asp:Label ID="LabelTitreDebutSolde" runat="server" Height="22px" Width="190px"
                  BackColor="#A4E3DB" BorderColor="#E2FDF7" BorderStyle="Solid" Text="Solde"
                  BorderWidth="1px" ForeColor="#298276" Font-Italic="False"
                  Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                  style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; 
                  font-style: normal; text-indent: 0px; text-align:  center; padding-top: 1px;">
              </asp:Label>          
             </asp:TableCell>
           </asp:TableRow>

           <asp:TableRow ID="LigneEnteteDebutMaladie" Height="22px">
             <asp:TableCell ID="CellTitreDebutBase100" HorizontalAlign="Center" Width="95px"> 
              <asp:Label ID="LabelTitreDebutBase100" runat="server" Height="22px" Width="94px"
                  BackColor="#B5E3DD" BorderColor="#E9FDF9" BorderStyle="Solid" Text="100%"
                  BorderWidth="1px" ForeColor="#298276" Font-Italic="False"
                  Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Smaller"
                  style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                  font-style: normal; text-indent: 0px; text-align:  center; padding-top: 1px;">
              </asp:Label>          
             </asp:TableCell>
             <asp:TableCell ID="CellTitreDebutBase50" HorizontalAlign="Center" Width="95px"> 
              <asp:Label ID="LabelTitreDebutBase50" runat="server" Height="22px" Width="94px"
                  BackColor="#B5E3DD" BorderColor="#E9FDF9" BorderStyle="Solid" Text="50%"
                  BorderWidth="1px" ForeColor="#298276" Font-Italic="False"
                  Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Smaller"
                  style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                  font-style: normal; text-indent: 0px; text-align:  center; padding-top: 1px;">
              </asp:Label>          
             </asp:TableCell>
             <asp:TableCell ID="CellTitreDebutUtilTot" HorizontalAlign="Center" Width="62px"> 
              <asp:Label ID="LabelTitreDebutUtilTot" runat="server" Height="22px" Width="62px"
                  BackColor="#B5E3DD" BorderColor="#E9FDF9" BorderStyle="Solid" Text="total"
                  BorderWidth="1px" ForeColor="#298276" Font-Italic="False"
                  Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Smaller"
                  style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                  font-style: normal; text-indent: 0px; text-align:  center; padding-top: 1px;">
              </asp:Label>          
             </asp:TableCell>
             <asp:TableCell ID="CellTitreDebutUtil100" HorizontalAlign="Center" Width="62px"> 
              <asp:Label ID="LabelTitreDebutUtil100" runat="server" Height="22px" Width="62px"
                  BackColor="#B5E3DD" BorderColor="#E9FDF9" BorderStyle="Solid" Text="100%"
                  BorderWidth="1px" ForeColor="#298276" Font-Italic="False"
                  Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Smaller"
                  style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                  font-style: normal; text-indent: 0px; text-align:  center; padding-top: 1px;">
              </asp:Label>          
             </asp:TableCell>
             <asp:TableCell ID="CellTitreDebutUtil50" HorizontalAlign="Center" Width="62px"> 
              <asp:Label ID="LabelTitreDebutUtil50" runat="server" Height="22px" Width="62px"
                  BackColor="#B5E3DD" BorderColor="#E9FDF9" BorderStyle="Solid" Text="50%"
                  BorderWidth="1px" ForeColor="#298276" Font-Italic="False"
                  Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Smaller"
                  style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                  font-style: normal; text-indent: 0px; text-align:  center; padding-top: 1px;">
              </asp:Label>          
             </asp:TableCell>
             <asp:TableCell ID="CellTitreDebutSolde100" HorizontalAlign="Center" Width="95px"> 
              <asp:Label ID="LabelTitreDebutSolde100" runat="server" Height="22px" Width="94px"
                  BackColor="#B5E3DD" BorderColor="#E9FDF9" BorderStyle="Solid" Text="100%"
                  BorderWidth="1px" ForeColor="#298276" Font-Italic="False"
                  Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Smaller"
                  style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                  font-style: normal; text-indent: 0px; text-align:  center; padding-top: 1px;">
              </asp:Label>          
             </asp:TableCell>
             <asp:TableCell ID="CellTitreDebutSolde50" HorizontalAlign="Center" Width="95px"> 
              <asp:Label ID="LabelTitreDebutSolde50" runat="server" Height="22px" Width="94px"
                  BackColor="#B5E3DD" BorderColor="#E9FDF9" BorderStyle="Solid" Text="50%"
                  BorderWidth="1px" ForeColor="#298276" Font-Italic="False"
                  Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Smaller"
                  style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                  font-style: normal; text-indent: 0px; text-align:  center; padding-top: 1px;">
              </asp:Label>          
             </asp:TableCell>
           </asp:TableRow>

           <asp:TableRow ID="LigneDebutMaladie" Height="22px">
             <asp:TableCell ID="CellDebutBase100" HorizontalAlign="Center" Width="95px"> 
              <asp:Label ID="LabelDebutBase100" runat="server" Height="22px" Width="94px"
                  BackColor="White" BorderColor="#B5E3DD" BorderStyle="Solid" Text="90"
                  BorderWidth="1px" ForeColor="#1C5151" Font-Italic="False"
                  Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                  style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                  font-style: normal; text-indent: 0px; text-align:  center; padding-top: 1px;">
              </asp:Label>          
             </asp:TableCell>
             <asp:TableCell ID="CellDebutBase50" HorizontalAlign="Center" Width="95px"> 
              <asp:Label ID="LabelDebutBase50" runat="server" Height="22px" Width="94px"
                  BackColor="White" BorderColor="#B5E3DD" BorderStyle="Solid" Text="270"
                  BorderWidth="1px" ForeColor="#1C5151" Font-Italic="False"
                  Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                  style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                  font-style: normal; text-indent: 0px; text-align:  center; padding-top: 1px;">
              </asp:Label>          
             </asp:TableCell>
             <asp:TableCell ID="CellDebutUtilTot" HorizontalAlign="Center" Width="62px"> 
              <asp:Label ID="LabelDebutUtilTot" runat="server" Height="22px" Width="62px"
                  BackColor="White" BorderColor="#B5E3DD" BorderStyle="Solid" Text="10"
                  BorderWidth="1px" ForeColor="#1C5151" Font-Italic="False"
                  Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                  style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                  font-style: normal; text-indent: 0px; text-align:  center; padding-top: 1px;">
              </asp:Label>          
             </asp:TableCell>
             <asp:TableCell ID="CellDebutUtil100" HorizontalAlign="Center" Width="62px"> 
              <asp:Label ID="LabelDebutUtil100" runat="server" Height="22px" Width="62px"
                  BackColor="White" BorderColor="#B5E3DD" BorderStyle="Solid" Text="10"
                  BorderWidth="1px" ForeColor="#1C5151" Font-Italic="False"
                  Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                  style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                  font-style: normal; text-indent: 0px; text-align:  center; padding-top: 1px;">
              </asp:Label>          
             </asp:TableCell>
             <asp:TableCell ID="CellDebutUtil50" HorizontalAlign="Center" Width="62px"> 
              <asp:Label ID="LabelDebutUtil50" runat="server" Height="22px" Width="62px"
                  BackColor="White" BorderColor="#B5E3DD" BorderStyle="Solid" Text="0"
                  BorderWidth="1px" ForeColor="#1C5151" Font-Italic="False"
                  Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                  style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                  font-style: normal; text-indent: 0px; text-align:  center; padding-top: 1px;">
              </asp:Label>          
             </asp:TableCell>
             <asp:TableCell ID="CellDebutSolde100" HorizontalAlign="Center" Width="95px"> 
              <asp:Label ID="LabelDebutSolde100" runat="server" Height="22px" Width="94px"
                  BackColor="White" BorderColor="#B5E3DD" BorderStyle="Solid" Text="80"
                  BorderWidth="1px" ForeColor="#1C5151" Font-Italic="False"
                  Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                  style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                  font-style: normal; text-indent: 0px; text-align:  center; padding-top: 1px;">
              </asp:Label>          
             </asp:TableCell>
             <asp:TableCell ID="CellDebutSolde50" HorizontalAlign="Center" Width="95px"> 
              <asp:Label ID="LabelDebutSolde50" runat="server" Height="22px" Width="94px"
                  BackColor="White" BorderColor="#B5E3DD" BorderStyle="Solid" Text="270"
                  BorderWidth="1px" ForeColor="#1C5151" Font-Italic="False"
                  Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                  style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                  font-style: normal; text-indent: 0px; text-align:  center; padding-top: 1px;">
              </asp:Label>          
             </asp:TableCell>
           </asp:TableRow>

           <asp:TableRow>
             <asp:TableCell Height="15px" ColumnSpan="8" BackColor="#f0f0f0"></asp:TableCell>
           </asp:TableRow>

           <asp:TableRow ID="LigneSituationFinMaladie" Height="22px">
             <asp:TableCell ID="CellSituationFinMaladie" HorizontalAlign="Center" Width="170px" RowSpan="3"
             BackColor="#6C9690" BorderColor="#E2FDF7" BorderStyle="Solid" BorderWidth="1px"> 
              <asp:Label ID="LabelSituationFinMaladie" runat="server" Height="22px" Width="169px"
                  BackColor="#6C9690" BorderColor="#6C9690" BorderStyle="Solid" Text="Situation au 20/05/2010"
                  BorderWidth="1px" ForeColor="White" Font-Italic="False"
                  Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                  style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                  font-style: normal; text-indent: 0px; text-align:  center; padding-top: 1px;">
              </asp:Label>          
             </asp:TableCell>
             <asp:TableCell ID="CellTitreFinBaseRemuArrets" HorizontalAlign="Center" Width="190px" ColumnSpan="2"> 
              <asp:Label ID="LabelTitreFinBaseRemuArrets" runat="server" Height="22px" Width="190px"
                  BackColor="#A4E3DB" BorderColor="#E2FDF7" BorderStyle="Solid" Text="Base de rémunération"
                  BorderWidth="1px" ForeColor="#298276" Font-Italic="False"
                  Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                  style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                  font-style: normal; text-indent: 0px; text-align:  center; padding-top: 1px;">
              </asp:Label>          
             </asp:TableCell>
             <asp:TableCell ID="CellTitreFinRemuArrets" HorizontalAlign="Center" Width="190px" ColumnSpan="3"> 
              <asp:Label ID="LabelTitreFinRemuArrets" runat="server" Height="22px" Width="190px"
                  BackColor="#A4E3DB" BorderColor="#E2FDF7" BorderStyle="Solid" Text="Rémunération"
                  BorderWidth="1px" ForeColor="#298276" Font-Italic="False"
                  Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                  style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; 
                  font-style: normal; text-indent: 0px; text-align:  center; padding-top: 1px;">
              </asp:Label>          
             </asp:TableCell>
             <asp:TableCell ID="CellTitreFinSolde" HorizontalAlign="Center" Width="190px" ColumnSpan="2"> 
              <asp:Label ID="LabelTitreFinSolde" runat="server" Height="22px" Width="190px"
                  BackColor="#A4E3DB" BorderColor="#E2FDF7" BorderStyle="Solid" Text="Solde"
                  BorderWidth="1px" ForeColor="#298276" Font-Italic="False"
                  Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                  style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; 
                  font-style: normal; text-indent: 0px; text-align:  center; padding-top: 1px;">
              </asp:Label>          
             </asp:TableCell>
           </asp:TableRow>

           <asp:TableRow ID="LigneEnteteFinMaladie" Height="22px">
             <asp:TableCell ID="CellTitreFinBase100" HorizontalAlign="Center" Width="95px"> 
              <asp:Label ID="LabelTitreFinBase100" runat="server" Height="22px" Width="94px"
                  BackColor="#B5E3DD" BorderColor="#E9FDF9" BorderStyle="Solid" Text="100%"
                  BorderWidth="1px" ForeColor="#298276" Font-Italic="False"
                  Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Smaller"
                  style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                  font-style: normal; text-indent: 0px; text-align:  center; padding-top: 1px;">
              </asp:Label>          
             </asp:TableCell>
             <asp:TableCell ID="CellTitreFinBase50" HorizontalAlign="Center" Width="95px"> 
              <asp:Label ID="LabelTitreFinBase50" runat="server" Height="22px" Width="94px"
                  BackColor="#B5E3DD" BorderColor="#E9FDF9" BorderStyle="Solid" Text="50%"
                  BorderWidth="1px" ForeColor="#298276" Font-Italic="False"
                  Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Smaller"
                  style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                  font-style: normal; text-indent: 0px; text-align:  center; padding-top: 1px;">
              </asp:Label>          
             </asp:TableCell>
             <asp:TableCell ID="CellTitreFinUtilTot" HorizontalAlign="Center" Width="62px"> 
              <asp:Label ID="LabelTitreFinUtilTot" runat="server" Height="22px" Width="62px"
                  BackColor="#B5E3DD" BorderColor="#E9FDF9" BorderStyle="Solid" Text="total"
                  BorderWidth="1px" ForeColor="#298276" Font-Italic="False"
                  Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Smaller"
                  style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                  font-style: normal; text-indent: 0px; text-align:  center; padding-top: 1px;">
              </asp:Label>          
             </asp:TableCell>
             <asp:TableCell ID="CellTitreFinUtil100" HorizontalAlign="Center" Width="62px"> 
              <asp:Label ID="LabelTitreFinUtil100" runat="server" Height="22px" Width="62px"
                  BackColor="#B5E3DD" BorderColor="#E9FDF9" BorderStyle="Solid" Text="100%"
                  BorderWidth="1px" ForeColor="#298276" Font-Italic="False"
                  Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Smaller"
                  style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                  font-style: normal; text-indent: 0px; text-align:  center; padding-top: 1px;">
              </asp:Label>          
             </asp:TableCell>
             <asp:TableCell ID="CellTitreFinUtil50" HorizontalAlign="Center" Width="62px"> 
              <asp:Label ID="LabelTitreFinUtil50" runat="server" Height="22px" Width="62px"
                  BackColor="#B5E3DD" BorderColor="#E9FDF9" BorderStyle="Solid" Text="50%"
                  BorderWidth="1px" ForeColor="#298276" Font-Italic="False"
                  Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Smaller"
                  style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                  font-style: normal; text-indent: 0px; text-align:  center; padding-top: 1px;">
              </asp:Label>          
             </asp:TableCell>
             <asp:TableCell ID="CellTitreFinSolde100" HorizontalAlign="Center" Width="95px"> 
              <asp:Label ID="LabelTitreFinSolde100" runat="server" Height="22px" Width="94px"
                  BackColor="#B5E3DD" BorderColor="#E9FDF9" BorderStyle="Solid" Text="100%"
                  BorderWidth="1px" ForeColor="#298276" Font-Italic="False"
                  Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Smaller"
                  style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                  font-style: normal; text-indent: 0px; text-align:  center; padding-top: 1px;">
              </asp:Label>          
             </asp:TableCell>
             <asp:TableCell ID="CellTitreFinSolde50" HorizontalAlign="Center" Width="95px"> 
              <asp:Label ID="LabelTitreFinSolde50" runat="server" Height="22px" Width="94px"
                  BackColor="#B5E3DD" BorderColor="#E9FDF9" BorderStyle="Solid" Text="50%"
                  BorderWidth="1px" ForeColor="#298276" Font-Italic="False"
                  Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Smaller"
                  style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                  font-style: normal; text-indent: 0px; text-align:  center; padding-top: 1px;">
              </asp:Label>          
             </asp:TableCell>
           </asp:TableRow>

           <asp:TableRow ID="LigneFinMaladie" Height="22px">
             <asp:TableCell ID="CellFinBase100" HorizontalAlign="Center" Width="95px"> 
              <asp:Label ID="LabelFinBase100" runat="server" Height="22px" Width="94px"
                  BackColor="White" BorderColor="#B5E3DD" BorderStyle="Solid" Text="90"
                  BorderWidth="1px" ForeColor="#1C5151" Font-Italic="False"
                  Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                  style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                  font-style: normal; text-indent: 0px; text-align:  center; padding-top: 1px;">
              </asp:Label>          
             </asp:TableCell>
             <asp:TableCell ID="CellFinBase50" HorizontalAlign="Center" Width="95px"> 
              <asp:Label ID="LabelFinBase50" runat="server" Height="22px" Width="94px"
                  BackColor="White" BorderColor="#B5E3DD" BorderStyle="Solid" Text="270"
                  BorderWidth="1px" ForeColor="#1C5151" Font-Italic="False"
                  Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                  style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                  font-style: normal; text-indent: 0px; text-align:  center; padding-top: 1px;">
              </asp:Label>          
             </asp:TableCell>
             <asp:TableCell ID="CellFinUtilTot" HorizontalAlign="Center" Width="62px"> 
              <asp:Label ID="LabelFinUtilTot" runat="server" Height="22px" Width="62px"
                  BackColor="White" BorderColor="#B5E3DD" BorderStyle="Solid" Text="27"
                  BorderWidth="1px" ForeColor="#1C5151" Font-Italic="False"
                  Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                  style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                  font-style: normal; text-indent: 0px; text-align:  center; padding-top: 1px;">
              </asp:Label>          
             </asp:TableCell>
             <asp:TableCell ID="CellFinUtil100" HorizontalAlign="Center" Width="62px"> 
              <asp:Label ID="LabelFinUtil100" runat="server" Height="22px" Width="62px"
                  BackColor="White" BorderColor="#B5E3DD" BorderStyle="Solid" Text="20"
                  BorderWidth="1px" ForeColor="#1C5151" Font-Italic="False"
                  Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                  style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                  font-style: normal; text-indent: 0px; text-align:  center; padding-top: 1px;">
              </asp:Label>          
             </asp:TableCell>
             <asp:TableCell ID="CellFinUtil50" HorizontalAlign="Center" Width="62px"> 
              <asp:Label ID="LabelFinUtil50" runat="server" Height="22px" Width="62px"
                  BackColor="White" BorderColor="#B5E3DD" BorderStyle="Solid" Text="7"
                  BorderWidth="1px" ForeColor="#1C5151" Font-Italic="False"
                  Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                  style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                  font-style: normal; text-indent: 0px; text-align:  center; padding-top: 1px;">
              </asp:Label>          
             </asp:TableCell>
             <asp:TableCell ID="CellFinSolde100" HorizontalAlign="Center" Width="95px"> 
              <asp:Label ID="LabelFinSolde100" runat="server" Height="22px" Width="94px"
                  BackColor="White" BorderColor="#B5E3DD" BorderStyle="Solid" Text="70"
                  BorderWidth="1px" ForeColor="#1C5151" Font-Italic="False"
                  Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                  style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                  font-style: normal; text-indent: 0px; text-align:  center; padding-top: 1px;">
              </asp:Label>          
             </asp:TableCell>
             <asp:TableCell ID="CellFinSolde50" HorizontalAlign="Center" Width="95px"> 
              <asp:Label ID="LabelFinSolde50" runat="server" Height="22px" Width="94px"
                  BackColor="White" BorderColor="#B5E3DD" BorderStyle="Solid" Text="263"
                  BorderWidth="1px" ForeColor="#1C5151" Font-Italic="False"
                  Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                  style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                  font-style: normal; text-indent: 0px; text-align:  center; padding-top: 1px;">
              </asp:Label>          
             </asp:TableCell>
           </asp:TableRow>

        </asp:Table>
    </asp:TableCell>
  </asp:TableRow>

</asp:Table>