﻿Option Strict On
Option Explicit On
Option Compare Text
Imports VI = Virtualia.Systeme.Constantes
Partial Class Fenetre_PER_POINTAGE
    Inherits Virtualia.Net.Controles.ObjetWebControle
    '
    Private WsNumObjet As Short = VI.ObjetPer.ObaPointage
    Private WsNomTable As String = "PER_POINTAGE"
    Private WsFiche As Virtualia.TablesObjet.ShemaPER.PER_POINTAGE

    Public ReadOnly Property MonCadre() As System.Web.UI.Control
        Get
            Return Me.CadreInfo
        End Get
    End Property

    Public WriteOnly Property CadreStyle() As String
        Set(ByVal value As String)
            Dim TableauData(0) As String
            Dim TableauW(0) As String
            Dim IndiceI As Integer
            TableauData = Strings.Split(value, ";")
            For IndiceI = 0 To TableauData.Count - 1
                If TableauData(IndiceI) = "" Then
                    Exit For
                End If
                TableauW = Strings.Split(TableauData(IndiceI), ":")
                CadreInfo.Style.Remove(Strings.Trim(TableauW(0)))
                CadreInfo.Style.Add(Strings.Trim(TableauW(0)), Strings.Trim(TableauW(1)))
            Next IndiceI
        End Set
    End Property

    Public WriteOnly Property Identifiant() As Integer
        Set(ByVal value As Integer)
            If V_Identifiant <> value Then

                Dim ColHisto As New ArrayList
                ColHisto.Add(0)
                ColHisto.Add(1)
                ColHisto.Add(4)
                ColHisto.Add(7)
                ColHisto.Add(10)
                V_CacheColHisto = ColHisto

                V_Identifiant = value
                Call ActualiserListe()
            End If
        End Set
    End Property

    Private Sub ActualiserListe()
        CadreCmdOK.Visible = False
        V_LibelListe = "Aucune journée badgée" & VI.Tild & "Une journée badgée" & VI.Tild & "journées badgées"
        V_LibelColonne = "le" & VI.Tild & "1er badgeage" & VI.Tild & "2ème badgeage" & VI.Tild & "3ème badgeage" & VI.Tild & "4ème badgeage" & VI.Tild & "Clef"

        If V_Identifiant > 0 Then
            Call InitialiserControles()
            If V_IndexFiche = -1 Then
                ListeGrille.V_Liste(V_LibelColonne, V_LibelListe) = ""
                Exit Sub
            End If
            ListeGrille.V_Filtres(V_LibelListe) = ""
            ListeGrille.V_Liste(V_LibelColonne, V_LibelListe) = V_ListeFiches
        End If
    End Sub

    Protected Sub ListeGrille_ValeurChange(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.DonneeChangeEventArgs) Handles ListeGrille.ValeurChange
        If e.Valeur = "" Then
            Exit Sub
        End If
        V_Occurence = e.Valeur
    End Sub

    Private Sub LireLaFiche()
        Dim CacheDonnee As ArrayList = V_CacheMaj
        If CacheDonnee Is Nothing Then
            Exit Sub
        End If
        Dim NumInfo As Integer
        Dim Ctl As Control
        Dim VirControle As Controles_VCoupleEtiDonnee
        Dim IndiceI As Integer = 0

        Do
            Ctl = V_WebFonction.VirWebControle(Me.CadreInfo, "InfoH", IndiceI)
            If Ctl Is Nothing Then
                Exit Do
            End If
            NumInfo = CInt(Strings.Right(Ctl.ID, 2))
            VirControle = CType(Ctl, Controles_VCoupleEtiDonnee)
            If CacheDonnee(NumInfo) Is Nothing Then
                VirControle.DonText = ""
            Else
                VirControle.DonText = CacheDonnee(NumInfo).ToString
            End If
            VirControle.V_SiAutoPostBack = False
            IndiceI += 1
        Loop
    End Sub

    Protected Sub CommandeNew_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles CommandeNew.Click
        Call InitialiserControles()
        Call V_CommandeNewFiche()
        CadreCmdOK.Visible = True
    End Sub

    Protected Sub CommandeSupp_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles CommandeSupp.Click
        Call V_CommandeSuppFiche()
    End Sub

    Public WriteOnly Property RetourDialogueSupp(ByVal Cmd As String) As String
        Set(ByVal value As String)
            V_RetourDialogueSupp(Cmd) = value
            Call ActualiserListe()
        End Set
    End Property

    Protected Sub CommandeOK_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles CommandeOK.Click
        Call V_MajFiche()
        Call ActualiserListe()
        CadreCmdOK.Visible = False
    End Sub

    Protected Sub InfoH_ValeurChange(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.DonneeChangeEventArgs) Handles InfoH00.ValeurChange, _
        InfoH01.ValeurChange, InfoH02.ValeurChange, InfoH03.ValeurChange, InfoH04.ValeurChange, InfoH05.ValeurChange, _
        InfoH06.ValeurChange, InfoH07.ValeurChange, InfoH08.ValeurChange, InfoH09.ValeurChange, InfoH10.ValeurChange, _
        InfoH11.ValeurChange, InfoH12.ValeurChange, InfoH13.ValeurChange, InfoH14.ValeurChange, InfoH15.ValeurChange, _
        InfoH16.ValeurChange, InfoH17.ValeurChange, InfoH18.ValeurChange, InfoH19.ValeurChange, InfoH20.ValeurChange, _
        InfoH21.ValeurChange, InfoH22.ValeurChange, InfoH23.ValeurChange, InfoH24.ValeurChange, InfoH25.ValeurChange, _
        InfoH26.ValeurChange, InfoH27.ValeurChange, InfoH28.ValeurChange, InfoH29.ValeurChange, InfoH30.ValeurChange, _
        InfoH31.ValeurChange, InfoH32.ValeurChange, InfoH33.ValeurChange, InfoH34.ValeurChange, InfoH35.ValeurChange, _
        InfoH36.ValeurChange, InfoH37.ValeurChange, InfoH38.ValeurChange, InfoH39.ValeurChange

        Dim CacheDonnee As ArrayList = V_CacheMaj
        Dim NumInfo As Integer = CInt(Strings.Right(CType(sender, Controles_VCoupleEtiDonnee).ID, 2))
        If CacheDonnee IsNot Nothing Then
            If CacheDonnee(NumInfo) Is Nothing Then
                CType(sender, Controles_VCoupleEtiDonnee).DonBackColor = V_WebFonction.CouleurMaj
                CadreCmdOK.Visible = True
            Else
                If CacheDonnee(NumInfo).ToString <> e.Valeur Then
                    CType(sender, Controles_VCoupleEtiDonnee).DonBackColor = V_WebFonction.CouleurMaj
                    CadreCmdOK.Visible = True
                End If
            End If
            Call V_ValeurChange(NumInfo, e.Valeur)
        End If
    End Sub

    Protected Overrides Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        V_Objet(1) = WsNumObjet
        V_NomTableSgbd = WsNomTable
        V_Fiche = WsFiche
        ListeGrille.Centrage_Colonne(0) = 1
        ListeGrille.Centrage_Colonne(1) = 1
        ListeGrille.Centrage_Colonne(2) = 1
        ListeGrille.Centrage_Colonne(3) = 1
        ListeGrille.Centrage_Colonne(4) = 1
    End Sub

    Protected Overrides Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Call LireLaFiche()

        CadreInfo.BackColor = V_WebFonction.CouleurCharte(V_PointdeVue, "Cadre")
        CadreInfo.BorderColor = V_WebFonction.CouleurCharte(V_PointdeVue, "Bordure")
        Etiquette.BackColor = V_WebFonction.CouleurCharte(V_PointdeVue, "Titre")
        Etiquette.ForeColor = V_WebFonction.CouleurCharte(V_PointdeVue, "Police_Claire")
        Etiquette.BorderColor = V_WebFonction.CouleurCharte(V_PointdeVue, "Bordure")
        EtiTableau.BackColor = V_WebFonction.CouleurCharte(V_PointdeVue, "Etiquette_BackColor")
        EtiTableau.ForeColor = V_WebFonction.CouleurCharte(V_PointdeVue, "Etiquette_BackColor")
        EtiTableau.BorderColor = V_WebFonction.CouleurCharte(V_PointdeVue, "Bordure")
        EtiBadgeage.BackColor = V_WebFonction.CouleurCharte(V_PointdeVue, "Etiquette_BackColor")
        EtiBadgeage.ForeColor = V_WebFonction.CouleurCharte(V_PointdeVue, "Etiquette_ForeColor")
        EtiBadgeage.BorderColor = V_WebFonction.CouleurCharte(V_PointdeVue, "Bordure")
        EtiLieu.BackColor = V_WebFonction.CouleurCharte(V_PointdeVue, "Etiquette_BackColor")
        EtiLieu.ForeColor = V_WebFonction.CouleurCharte(V_PointdeVue, "Etiquette_ForeColor")
        EtiLieu.BorderColor = V_WebFonction.CouleurCharte(V_PointdeVue, "Bordure")
        EtiSens.BackColor = V_WebFonction.CouleurCharte(V_PointdeVue, "Etiquette_BackColor")
        EtiSens.ForeColor = V_WebFonction.CouleurCharte(V_PointdeVue, "Etiquette_ForeColor")
        EtiSens.BorderColor = V_WebFonction.CouleurCharte(V_PointdeVue, "Bordure")
        LabelPointage1.BackColor = V_WebFonction.CouleurCharte(V_PointdeVue, "Etiquette_BackColor")
        LabelPointage1.ForeColor = V_WebFonction.CouleurCharte(V_PointdeVue, "Etiquette_ForeColor")
        LabelPointage1.BorderColor = V_WebFonction.CouleurCharte(V_PointdeVue, "Bordure")
        LabelPointage2.BackColor = V_WebFonction.CouleurCharte(V_PointdeVue, "Etiquette_BackColor")
        LabelPointage2.ForeColor = V_WebFonction.CouleurCharte(V_PointdeVue, "Etiquette_ForeColor")
        LabelPointage2.BorderColor = V_WebFonction.CouleurCharte(V_PointdeVue, "Bordure")
        LabelPointage3.BackColor = V_WebFonction.CouleurCharte(V_PointdeVue, "Etiquette_BackColor")
        LabelPointage3.ForeColor = V_WebFonction.CouleurCharte(V_PointdeVue, "Etiquette_ForeColor")
        LabelPointage3.BorderColor = V_WebFonction.CouleurCharte(V_PointdeVue, "Bordure")
        LabelPointage4.BackColor = V_WebFonction.CouleurCharte(V_PointdeVue, "Etiquette_BackColor")
        LabelPointage4.ForeColor = V_WebFonction.CouleurCharte(V_PointdeVue, "Etiquette_ForeColor")
        LabelPointage4.BorderColor = V_WebFonction.CouleurCharte(V_PointdeVue, "Bordure")
        LabelPointage5.BackColor = V_WebFonction.CouleurCharte(V_PointdeVue, "Etiquette_BackColor")
        LabelPointage5.ForeColor = V_WebFonction.CouleurCharte(V_PointdeVue, "Etiquette_ForeColor")
        LabelPointage5.BorderColor = V_WebFonction.CouleurCharte(V_PointdeVue, "Bordure")
        LabelPointage6.BackColor = V_WebFonction.CouleurCharte(V_PointdeVue, "Etiquette_BackColor")
        LabelPointage6.ForeColor = V_WebFonction.CouleurCharte(V_PointdeVue, "Etiquette_ForeColor")
        LabelPointage6.BorderColor = V_WebFonction.CouleurCharte(V_PointdeVue, "Bordure")
        LabelPointage7.BackColor = V_WebFonction.CouleurCharte(V_PointdeVue, "Etiquette_BackColor")
        LabelPointage7.ForeColor = V_WebFonction.CouleurCharte(V_PointdeVue, "Etiquette_ForeColor")
        LabelPointage7.BorderColor = V_WebFonction.CouleurCharte(V_PointdeVue, "Bordure")
        LabelPointage8.BackColor = V_WebFonction.CouleurCharte(V_PointdeVue, "Etiquette_BackColor")
        LabelPointage8.ForeColor = V_WebFonction.CouleurCharte(V_PointdeVue, "Etiquette_ForeColor")
        LabelPointage8.BorderColor = V_WebFonction.CouleurCharte(V_PointdeVue, "Bordure")
        LabelPointage9.BackColor = V_WebFonction.CouleurCharte(V_PointdeVue, "Etiquette_BackColor")
        LabelPointage9.ForeColor = V_WebFonction.CouleurCharte(V_PointdeVue, "Etiquette_ForeColor")
        LabelPointage9.BorderColor = V_WebFonction.CouleurCharte(V_PointdeVue, "Bordure")
        LabelPointage10.BackColor = V_WebFonction.CouleurCharte(V_PointdeVue, "Etiquette_BackColor")
        LabelPointage10.ForeColor = V_WebFonction.CouleurCharte(V_PointdeVue, "Etiquette_ForeColor")
        LabelPointage10.BorderColor = V_WebFonction.CouleurCharte(V_PointdeVue, "Bordure")

    End Sub

    Private Sub InitialiserControles()
        Dim Ctl As Control
        Dim VirControle As Controles_VCoupleEtiDonnee
        Dim IndiceI As Integer = 0
        Do
            Ctl = V_WebFonction.VirWebControle(Me.CadreInfo, "InfoH", IndiceI)
            If Ctl Is Nothing Then
                Exit Do
            End If
            VirControle = CType(Ctl, Controles_VCoupleEtiDonnee)
            VirControle.DonBackColor = Drawing.Color.White
            VirControle.DonText = ""
            IndiceI += 1
        Loop
    End Sub

End Class

