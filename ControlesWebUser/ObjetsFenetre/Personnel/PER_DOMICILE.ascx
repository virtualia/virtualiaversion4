﻿<%@ Control Language="VB" AutoEventWireup="false" Inherits="Virtualia.Net.Fenetre_PER_DOMICILE" CodeBehind="PER_DOMICILE.ascx.vb" %>

<%@ Register Src="~/Controles/Saisies/VCoupleEtiDonnee.ascx" TagName="VCoupleEtiDonnee" TagPrefix="Virtualia" %>
<%@ Register Src="~/Controles/Saisies/VDuoEtiquetteCommande.ascx" TagName="VDuoEtiquetteCommande" TagPrefix="Virtualia" %>
<%@ Register Src="~/Controles/Saisies/VCocheSimple.ascx" TagName="VCocheSimple" TagPrefix="Virtualia" %>
<%@ Register Src="~/Controles/Saisies/VCoupleEtiquetteExperte.ascx" TagName="VCoupleEtiquetteExperte" TagPrefix="Virtualia" %>

<%@ Register Src="~/ControlsGeneriques/V_CommandeOK.ascx" TagName="VCmdOK" TagPrefix="Generic" %>

<asp:Table ID="CadreInfo" runat="server" BorderStyle="Ridge" BorderWidth="2px" BorderColor="#B0E0D7" Height="600px" Width="500px" HorizontalAlign="Center">
    <asp:TableRow>
        <asp:TableCell>
            <Generic:VCmdOK ID="CtlOK" runat="server" Height="22px" Width="70px" HorizontalAlign="Right" CadreStyle="margin-top: 3px; margin-right: 3px"/>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="CadreTitre" runat="server" Height="40px" CellPadding="0" CellSpacing="0" HorizontalAlign="Center">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="Etiquette" runat="server" Text="Adresse du domicile" Height="20px" Width="300px" BackColor="#2FA49B" BorderColor="#B0E0D7" BorderStyle="Solid" BorderWidth="2px" ForeColor="#D7FAF3" Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Small" Style="margin-top: 15px; margin-left: 4px; margin-bottom: 10px; font-style: oblique; text-indent: 5px; text-align: center;" />
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="Adresse" runat="server" Height="22px" Width="495px" CellPadding="0" CellSpacing="0">
                <asp:TableRow>
                    <asp:TableCell>
                        <Virtualia:VCoupleEtiDonnee ID="InfoH01" runat="server" V_PointdeVue="1" V_Objet="4" V_Information="1" V_SiDonneeDico="true" DonWidth="300px" DonTabIndex="1" />
                    </asp:TableCell>
                    <asp:TableCell>
                        <Virtualia:VCoupleEtiDonnee ID="InfoH06" runat="server" V_PointdeVue="1" V_Objet="4" V_Information="6" V_SiDonneeDico="true" DonWidth="120px" DonTabIndex="2" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        <Virtualia:VCoupleEtiDonnee ID="InfoH02" runat="server" V_PointdeVue="1" V_Objet="4" V_Information="2" V_SiDonneeDico="true" DonWidth="300px" DonTabIndex="3" />
                    </asp:TableCell>
                    <asp:TableCell>
                        <Virtualia:VCoupleEtiDonnee ID="InfoH11" runat="server" V_PointdeVue="1" V_Objet="4" V_Information="11" V_SiDonneeDico="true" DonWidth="120px" DonTabIndex="4" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        <Virtualia:VCoupleEtiDonnee ID="InfoH03" runat="server" V_PointdeVue="1" V_Objet="4" V_Information="3" V_SiDonneeDico="true" DonWidth="300px" DonTabIndex="5" />
                    </asp:TableCell>
                    <asp:TableCell>
                        <Virtualia:VCoupleEtiDonnee ID="InfoH09" runat="server" V_PointdeVue="1" V_Objet="4" V_Information="9" V_SiDonneeDico="true" DonWidth="120px" DonTabIndex="6" />
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
            <asp:Table ID="CP" runat="server" Height="22px" Width="495px" CellPadding="0" CellSpacing="0">
                <asp:TableRow>
                    <asp:TableCell Width="145px" HorizontalAlign="Left">
                        <Virtualia:VCoupleEtiDonnee ID="InfoH04" runat="server" V_PointdeVue="1" V_Objet="4" V_Information="4" V_SiDonneeDico="true" DonWidth="40px" EtiText="Code postal et ville" DonTabIndex="7" />
                    </asp:TableCell>
                    <asp:TableCell>
                        <Virtualia:VCoupleEtiDonnee ID="InfoH05" runat="server" V_PointdeVue="1" V_Objet="4" V_Information="5" V_SiDonneeDico="true" EtiWidth="0px" EtiVisible="false" DonWidth="250px" DonTabIndex="8" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Width="145px" HorizontalAlign="Left">
                        <Virtualia:VCoupleEtiDonnee ID="InfoH08" runat="server" V_PointdeVue="1" V_Objet="4" V_Information="8" V_SiDonneeDico="true" DonWidth="30px" EtiText="Code pays et pays" DonTabIndex="9" />
                    </asp:TableCell>
                    <asp:TableCell>
                        <Virtualia:VDuoEtiquetteCommande ID="Dontab07" runat="server" V_PointdeVue="1" V_Objet="4" V_Information="7" V_SiDonneeDico="true" EtiWidth="0px" EtiVisible="false" DonWidth="200px" DonTabIndex="10" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell ColumnSpan="2">
                        <Virtualia:VCoupleEtiDonnee ID="InfoH13" runat="server" V_PointdeVue="1" V_Objet="4" V_Information="13" V_SiDonneeDico="true" DonWidth="300px" DonTabIndex="11" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="10px" ColumnSpan="2" />
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell ColumnSpan="2">
                        <Virtualia:VCoupleEtiDonnee ID="InfoH10" runat="server" V_PointdeVue="1" V_Objet="4" V_Information="10" V_SiDonneeDico="true" DonWidth="300px" DonTabIndex="12" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="10px" ColumnSpan="2" />
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell ColumnSpan="2">
                        <Virtualia:VDuoEtiquetteCommande ID="Dontab12" runat="server" V_PointdeVue="1" V_Objet="4" V_Information="12" V_SiDonneeDico="true" DonWidth="280px" DonTabIndex="13" />
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="CadreReseauxSociaux" runat="server" Height="40px" CellPadding="0" CellSpacing="0" HorizontalAlign="Center">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="EtiReseaux" runat="server" Text="Réseaux sociaux" Height="20px" Width="300px" BackColor="#2FA49B" BorderColor="#B0E0D7" BorderStyle="Solid" BorderWidth="2px" ForeColor="#D7FAF3" Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Small" Style="margin-top: 15px; margin-left: 4px; margin-bottom: 10px; font-style: oblique; text-indent: 5px; text-align: center;" />
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="Reseaux" runat="server" Width="550px" CellPadding="0" CellSpacing="0" HorizontalAlign="Center">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VCoupleEtiDonnee ID="InfoH19" runat="server" V_PointdeVue="1" V_Objet="4" V_Information="19" V_SiDonneeDico="true" EtiWidth="200px" DonWidth="340px" DonTabIndex="14" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VCoupleEtiDonnee ID="InfoH18" runat="server" V_PointdeVue="1" V_Objet="4" V_Information="18" V_SiDonneeDico="true" EtiWidth="200px" DonWidth="340px" DonTabIndex="15" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VCoupleEtiDonnee ID="InfoH20" runat="server" V_PointdeVue="1" V_Objet="4" V_Information="20" V_SiDonneeDico="true" EtiWidth="200px" DonWidth="340px" DonTabIndex="16" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VCoupleEtiDonnee ID="InfoH22" runat="server" V_PointdeVue="1" V_Objet="4" V_Information="22" V_SiDonneeDico="true" EtiWidth="200px" DonWidth="340px" DonTabIndex="17" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VCoupleEtiDonnee ID="InfoH21" runat="server" V_PointdeVue="1" V_Objet="4" V_Information="21" V_SiDonneeDico="true" EtiWidth="200px" DonWidth="340px" DonTabIndex="18" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="15px" />
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="Confidentialite" runat="server" Width="550px" CellPadding="0" CellSpacing="0" HorizontalAlign="Center">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VCocheSimple ID="Coche14" runat="server" V_PointdeVue="1" V_Objet="4" V_Information="14" V_SiDonneeDico="true" V_Width="300px" V_Style="margin-left:0px" />
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VCocheSimple ID="Coche15" runat="server" V_PointdeVue="1" V_Objet="4" V_Information="15" V_SiDonneeDico="true" V_Width="300px" V_Style="margin-left:0px" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VCocheSimple ID="Coche16" runat="server" V_PointdeVue="1" V_Objet="4" V_Information="16" V_SiDonneeDico="true" V_Width="300px" V_Style="margin-left:0px" />
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VCocheSimple ID="Coche17" runat="server" V_PointdeVue="1" V_Objet="4" V_Information="17" V_SiDonneeDico="true" V_Width="300px" V_Style="margin-left:0px" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="10px" ColumnSpan="2" />
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <asp:Panel ID="CadreExpert" runat="server" BorderStyle="None" BorderWidth="1px" BorderColor="#B0E0D7" Height="44px" Width="500px" HorizontalAlign="Center" Style="margin-top: 5px; vertical-align: middle">
                <Virtualia:VCoupleEtiquetteExperte ID="ExprH01" runat="server" V_PointdeVue="1" V_Objet="4" V_InfoExperte="517" V_SiDonneeDico="true" EtiHeight="20px" EtiWidth="170px" DonWidth="600px" DonHeight="35px" />
            </asp:Panel>
        </asp:TableCell>
    </asp:TableRow>
</asp:Table>
