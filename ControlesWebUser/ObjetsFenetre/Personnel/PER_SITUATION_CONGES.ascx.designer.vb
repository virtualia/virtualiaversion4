﻿'------------------------------------------------------------------------------
' <généré automatiquement>
'     Ce code a été généré par un outil.
'
'     Les modifications apportées à ce fichier peuvent provoquer un comportement incorrect et seront perdues si
'     le code est régénéré.
' </généré automatiquement>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class Fenetre_PER_SITUATION_CONGES

    '''<summary>
    '''Contrôle CadreInfo.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CadreInfo As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle TableauEnteteSituation.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents TableauEnteteSituation As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle LigneEnteteTitre.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LigneEnteteTitre As Global.System.Web.UI.WebControls.TableRow

    '''<summary>
    '''Contrôle LabelTitreTableau.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelTitreTableau As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle LigneEnteteConges.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LigneEnteteConges As Global.System.Web.UI.WebControls.TableRow

    '''<summary>
    '''Contrôle CellEnteteConges.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellEnteteConges As Global.System.Web.UI.WebControls.TableCell

    '''<summary>
    '''Contrôle LabelEnteteConges.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelEnteteConges As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle CellEnteteDroits.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellEnteteDroits As Global.System.Web.UI.WebControls.TableCell

    '''<summary>
    '''Contrôle LabelEnteteDroits.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelEnteteDroits As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle CellEnteteCongesPris.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellEnteteCongesPris As Global.System.Web.UI.WebControls.TableCell

    '''<summary>
    '''Contrôle LabelEnteteCongesPris.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelEnteteCongesPris As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle CellEnteteSolde.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellEnteteSolde As Global.System.Web.UI.WebControls.TableCell

    '''<summary>
    '''Contrôle LabelEnteteSolde.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelEnteteSolde As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle TableauSituationConges.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents TableauSituationConges As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle LigneSituationConges.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LigneSituationConges As Global.System.Web.UI.WebControls.TableRow

    '''<summary>
    '''Contrôle CellTypeConge.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellTypeConge As Global.System.Web.UI.WebControls.TableCell

    '''<summary>
    '''Contrôle LabelTypeConge.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelTypeConge As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle CellDroitsConges.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellDroitsConges As Global.System.Web.UI.WebControls.TableCell

    '''<summary>
    '''Contrôle LabelDroitsConges.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelDroitsConges As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle CellTitreCongesPris.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellTitreCongesPris As Global.System.Web.UI.WebControls.TableCell

    '''<summary>
    '''Contrôle LabelTitreCongesPris.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelTitreCongesPris As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle CellCongesPris.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellCongesPris As Global.System.Web.UI.WebControls.TableCell

    '''<summary>
    '''Contrôle LabelCongesPris.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelCongesPris As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle CellTitreCongesAvenir.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellTitreCongesAvenir As Global.System.Web.UI.WebControls.TableCell

    '''<summary>
    '''Contrôle LabelTitreCongesAvenir.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelTitreCongesAvenir As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle CellCongesAvenir.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellCongesAvenir As Global.System.Web.UI.WebControls.TableCell

    '''<summary>
    '''Contrôle LabelCongesAvenir.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelCongesAvenir As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle CellSoldeConges.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellSoldeConges As Global.System.Web.UI.WebControls.TableCell

    '''<summary>
    '''Contrôle LabelSoldeConges.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelSoldeConges As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle LigneDetailsConges.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LigneDetailsConges As Global.System.Web.UI.WebControls.TableRow

    '''<summary>
    '''Contrôle CellDetailsConges.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellDetailsConges As Global.System.Web.UI.WebControls.TableCell

    '''<summary>
    '''Contrôle LabelDetailsConges.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelDetailsConges As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle CellDetailsDroitsConges.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellDetailsDroitsConges As Global.System.Web.UI.WebControls.TableCell

    '''<summary>
    '''Contrôle LabelDetailsDroitsConges.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelDetailsDroitsConges As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle CellDetailsCongesPris.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellDetailsCongesPris As Global.System.Web.UI.WebControls.TableCell

    '''<summary>
    '''Contrôle LabelDetailsCongesPris.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelDetailsCongesPris As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle CellDetailSoldeConges.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellDetailSoldeConges As Global.System.Web.UI.WebControls.TableCell

    '''<summary>
    '''Contrôle LabelDetailSoldeConges.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelDetailSoldeConges As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle TableauSituationRTT.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents TableauSituationRTT As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle LigneSituationRTT.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LigneSituationRTT As Global.System.Web.UI.WebControls.TableRow

    '''<summary>
    '''Contrôle CellTypeRTT.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellTypeRTT As Global.System.Web.UI.WebControls.TableCell

    '''<summary>
    '''Contrôle LabelTypeRTT.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelTypeRTT As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle CellDroitsRTT.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellDroitsRTT As Global.System.Web.UI.WebControls.TableCell

    '''<summary>
    '''Contrôle LabelDroitsRTT.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelDroitsRTT As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle CellTitreRTTpris.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellTitreRTTpris As Global.System.Web.UI.WebControls.TableCell

    '''<summary>
    '''Contrôle LabelTitreRTTpris.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelTitreRTTpris As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle CellRTTpris.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellRTTpris As Global.System.Web.UI.WebControls.TableCell

    '''<summary>
    '''Contrôle LabelRTTpris.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelRTTpris As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle CellTitreRTTAvenir.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellTitreRTTAvenir As Global.System.Web.UI.WebControls.TableCell

    '''<summary>
    '''Contrôle LabelTitreRTTAvenir.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelTitreRTTAvenir As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle CellRTTAvenir.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellRTTAvenir As Global.System.Web.UI.WebControls.TableCell

    '''<summary>
    '''Contrôle LabelRTTAvenir.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelRTTAvenir As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle CellSoldeRTT.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellSoldeRTT As Global.System.Web.UI.WebControls.TableCell

    '''<summary>
    '''Contrôle LabelSoldeRTT.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelSoldeRTT As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle LigneDetailsRTT.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LigneDetailsRTT As Global.System.Web.UI.WebControls.TableRow

    '''<summary>
    '''Contrôle CellDetailsRTT.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellDetailsRTT As Global.System.Web.UI.WebControls.TableCell

    '''<summary>
    '''Contrôle LabelDetailsRTT.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelDetailsRTT As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle CellDetailsDroitsRTT.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellDetailsDroitsRTT As Global.System.Web.UI.WebControls.TableCell

    '''<summary>
    '''Contrôle LabelDetailsDroitsRTT.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelDetailsDroitsRTT As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle CellDetailsRTTpris.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellDetailsRTTpris As Global.System.Web.UI.WebControls.TableCell

    '''<summary>
    '''Contrôle LabelDetailsRTTpris.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelDetailsRTTpris As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle CellDetailsSoldeRTT.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellDetailsSoldeRTT As Global.System.Web.UI.WebControls.TableCell

    '''<summary>
    '''Contrôle LabelDetailsSoldeRTT.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelDetailsSoldeRTT As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle TableauSituationCompteur1.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents TableauSituationCompteur1 As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle LigneSituationCompteur1.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LigneSituationCompteur1 As Global.System.Web.UI.WebControls.TableRow

    '''<summary>
    '''Contrôle CellTypeCompteur1.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellTypeCompteur1 As Global.System.Web.UI.WebControls.TableCell

    '''<summary>
    '''Contrôle LabelTypeCompteur1.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelTypeCompteur1 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle CellDroitsCompteur1.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellDroitsCompteur1 As Global.System.Web.UI.WebControls.TableCell

    '''<summary>
    '''Contrôle LabelDroitsCompteur1.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelDroitsCompteur1 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle CellTitreCompteur1pris.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellTitreCompteur1pris As Global.System.Web.UI.WebControls.TableCell

    '''<summary>
    '''Contrôle LabelTitreCompteur1pris.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelTitreCompteur1pris As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle CellCompteur1pris.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellCompteur1pris As Global.System.Web.UI.WebControls.TableCell

    '''<summary>
    '''Contrôle LabelCompteur1pris.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelCompteur1pris As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle CellTitreCompteur1Avenir.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellTitreCompteur1Avenir As Global.System.Web.UI.WebControls.TableCell

    '''<summary>
    '''Contrôle LabelTitreCompteur1Avenir.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelTitreCompteur1Avenir As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle CellCompteur1Avenir.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellCompteur1Avenir As Global.System.Web.UI.WebControls.TableCell

    '''<summary>
    '''Contrôle LabelCompteur1Avenir.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelCompteur1Avenir As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle CellSoldeCompteur1.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellSoldeCompteur1 As Global.System.Web.UI.WebControls.TableCell

    '''<summary>
    '''Contrôle LabelSoldeCompteur1.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelSoldeCompteur1 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle LigneDetailsCompteur1.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LigneDetailsCompteur1 As Global.System.Web.UI.WebControls.TableRow

    '''<summary>
    '''Contrôle CellDetailsCompteur1.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellDetailsCompteur1 As Global.System.Web.UI.WebControls.TableCell

    '''<summary>
    '''Contrôle LabelDetailsCompteur1.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelDetailsCompteur1 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle CellDetailsDroitsCompteur1.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellDetailsDroitsCompteur1 As Global.System.Web.UI.WebControls.TableCell

    '''<summary>
    '''Contrôle LabelDetailsDroitsCompteur1.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelDetailsDroitsCompteur1 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle CellDetailsCompteur1pris.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellDetailsCompteur1pris As Global.System.Web.UI.WebControls.TableCell

    '''<summary>
    '''Contrôle LabelDetailsCompteur1pris.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelDetailsCompteur1pris As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle CellDetailsSoldeCompteur1.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellDetailsSoldeCompteur1 As Global.System.Web.UI.WebControls.TableCell

    '''<summary>
    '''Contrôle LabelDetailsSoldeCompteur1.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelDetailsSoldeCompteur1 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle TableauSituationCompteur2.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents TableauSituationCompteur2 As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle LigneSituationCompteur2.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LigneSituationCompteur2 As Global.System.Web.UI.WebControls.TableRow

    '''<summary>
    '''Contrôle CellTypeCompteur2.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellTypeCompteur2 As Global.System.Web.UI.WebControls.TableCell

    '''<summary>
    '''Contrôle LabelTypeCompteur2.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelTypeCompteur2 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle CellDroitsCompteur2.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellDroitsCompteur2 As Global.System.Web.UI.WebControls.TableCell

    '''<summary>
    '''Contrôle LabelDroitsCompteur2.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelDroitsCompteur2 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle CellTitreCompteur2pris.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellTitreCompteur2pris As Global.System.Web.UI.WebControls.TableCell

    '''<summary>
    '''Contrôle LabelTitreCompteur2pris.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelTitreCompteur2pris As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle CellCompteur2pris.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellCompteur2pris As Global.System.Web.UI.WebControls.TableCell

    '''<summary>
    '''Contrôle LabelCompteur2pris.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelCompteur2pris As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle CellTitreCompteur2Avenir.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellTitreCompteur2Avenir As Global.System.Web.UI.WebControls.TableCell

    '''<summary>
    '''Contrôle LabelTitreCompteur2Avenir.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelTitreCompteur2Avenir As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle CellCompteur2Avenir.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellCompteur2Avenir As Global.System.Web.UI.WebControls.TableCell

    '''<summary>
    '''Contrôle LabelCompteur2Avenir.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelCompteur2Avenir As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle CellSoldeCompteur2.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellSoldeCompteur2 As Global.System.Web.UI.WebControls.TableCell

    '''<summary>
    '''Contrôle LabelSoldeCompteur2.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelSoldeCompteur2 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle LigneDetailsCompteur2.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LigneDetailsCompteur2 As Global.System.Web.UI.WebControls.TableRow

    '''<summary>
    '''Contrôle CellDetailsCompteur2.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellDetailsCompteur2 As Global.System.Web.UI.WebControls.TableCell

    '''<summary>
    '''Contrôle LabelDetailsCompteur2.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelDetailsCompteur2 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle CellDetailsDroitsCompteur2.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellDetailsDroitsCompteur2 As Global.System.Web.UI.WebControls.TableCell

    '''<summary>
    '''Contrôle LabelDetailsDroitsCompteur2.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelDetailsDroitsCompteur2 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle CellDetailsCompteur2pris.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellDetailsCompteur2pris As Global.System.Web.UI.WebControls.TableCell

    '''<summary>
    '''Contrôle LabelDetailsCompteur2pris.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelDetailsCompteur2pris As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle CellDetailsSoldeCompteur2.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellDetailsSoldeCompteur2 As Global.System.Web.UI.WebControls.TableCell

    '''<summary>
    '''Contrôle LabelDetailsSoldeCompteur2.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelDetailsSoldeCompteur2 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle TableauSituationCompteur3.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents TableauSituationCompteur3 As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle LigneSituationCompteur3.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LigneSituationCompteur3 As Global.System.Web.UI.WebControls.TableRow

    '''<summary>
    '''Contrôle CellTypeCompteur3.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellTypeCompteur3 As Global.System.Web.UI.WebControls.TableCell

    '''<summary>
    '''Contrôle LabelTypeCompteur3.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelTypeCompteur3 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle CellDroitsCompteur3.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellDroitsCompteur3 As Global.System.Web.UI.WebControls.TableCell

    '''<summary>
    '''Contrôle LabelDroitsCompteur3.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelDroitsCompteur3 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle CellTitreCompteur3pris.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellTitreCompteur3pris As Global.System.Web.UI.WebControls.TableCell

    '''<summary>
    '''Contrôle LabelTitreCompteur3pris.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelTitreCompteur3pris As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle CellCompteur3pris.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellCompteur3pris As Global.System.Web.UI.WebControls.TableCell

    '''<summary>
    '''Contrôle LabelCompteur3pris.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelCompteur3pris As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle CellTitreCompteur3Avenir.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellTitreCompteur3Avenir As Global.System.Web.UI.WebControls.TableCell

    '''<summary>
    '''Contrôle LabelTitreCompteur3Avenir.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelTitreCompteur3Avenir As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle CellCompteur3Avenir.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellCompteur3Avenir As Global.System.Web.UI.WebControls.TableCell

    '''<summary>
    '''Contrôle LabelCompteur3Avenir.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelCompteur3Avenir As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle CellSoldeCompteur3.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellSoldeCompteur3 As Global.System.Web.UI.WebControls.TableCell

    '''<summary>
    '''Contrôle LabelSoldeCompteur3.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelSoldeCompteur3 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle LigneDetailsCompteur3.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LigneDetailsCompteur3 As Global.System.Web.UI.WebControls.TableRow

    '''<summary>
    '''Contrôle CellDetailsCompteur3.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellDetailsCompteur3 As Global.System.Web.UI.WebControls.TableCell

    '''<summary>
    '''Contrôle LabelDetailsCompteur3.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelDetailsCompteur3 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle CellDetailsDroitsCompteur3.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellDetailsDroitsCompteur3 As Global.System.Web.UI.WebControls.TableCell

    '''<summary>
    '''Contrôle LabelDetailsDroitsCompteur3.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelDetailsDroitsCompteur3 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle CellDetailsCompteur3pris.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellDetailsCompteur3pris As Global.System.Web.UI.WebControls.TableCell

    '''<summary>
    '''Contrôle LabelDetailsCompteur3pris.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelDetailsCompteur3pris As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle CellDetailsSoldeCompteur3.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellDetailsSoldeCompteur3 As Global.System.Web.UI.WebControls.TableCell

    '''<summary>
    '''Contrôle LabelDetailsSoldeCompteur3.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelDetailsSoldeCompteur3 As Global.System.Web.UI.WebControls.Label
End Class
