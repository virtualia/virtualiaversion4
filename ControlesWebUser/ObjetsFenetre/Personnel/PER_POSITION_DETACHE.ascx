﻿<%@ Control Language="VB" AutoEventWireup="false" Inherits="Virtualia.Net.Fenetre_PER_POSITION_DETACHE" CodeBehind="PER_POSITION_DETACHE.ascx.vb" %>

<%@ Register Src="~/Controles/Saisies/VCoupleEtiDonnee.ascx" TagName="VCoupleEtiDonnee" TagPrefix="Virtualia" %>
<%@ Register Src="~/Controles/Saisies/VDuoEtiquetteCommande.ascx" TagName="VDuoEtiquetteCommande" TagPrefix="Virtualia" %>

<%@ Register Src="~/ControlsGeneriques/V_DataGrid.ascx" TagName="VDataGrid" TagPrefix="Generic" %>
<%@ Register Src="~/ControlsGeneriques/V_CommandeCRUD.ascx" TagName="VCrud" TagPrefix="Generic" %>

<asp:Table ID="CadreControle" runat="server" Height="30px" Width="600px" CellPadding="0" CellSpacing="0" HorizontalAlign="Center">
    <asp:TableRow>
        <asp:TableCell Height="20px" />
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <Generic:VDataGrid ID="ListeGrille" runat="server" CadreWidth="530px" V_NiveauCharte="0" SiColonneSelect="true" SiCaseAcocher="false" />
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="CadreInfo" runat="server" BorderStyle="Ridge" BorderWidth="2px" BackColor="#CBF2BE" BorderColor="#B0E0D7" Height="400px" Width="520px" HorizontalAlign="Center" Style="margin-top: 3px;">
                <asp:TableRow>
                    <asp:TableCell>
                        <Generic:VCrud ID="CtlCrud" runat="server" Height="22px" Width="244px" HorizontalAlign="Right" CadreStyle="margin-top: 3px; margin-right: 3px" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Table ID="CadreTitre" runat="server" Height="40px" CellPadding="0" Width="550px" CellSpacing="0" HorizontalAlign="Center">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                    <asp:Label ID="Etiquette" runat="server" Height="20px" Width="400px" Text="Emploi fonctionnel - Statut d'emploi - Détachement" BackColor="#2FA49B" BorderColor="#B0E0D7" BorderStyle="Solid" BorderWidth="2px" ForeColor="#D7FAF3" Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Small" Style="margin-top: 5px; margin-left: 4px; margin-bottom: 10px; font-style: oblique; text-indent: 5px; text-align: center;" />
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Table ID="CadreDon" runat="server" CellPadding="0" CellSpacing="0" Width="550px">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left" Width="250px">
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH00" runat="server" EtiBackColor="#CBF2BE" V_PointdeVue="1" V_Objet="55" V_Information="0" V_SiDonneeDico="true" DonWidth="80px" DonTabIndex="1" V_SiFenetrePer_Bis="true" />
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Left" Width="250px">
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH07" runat="server" EtiBackColor="#CBF2BE" V_PointdeVue="1" V_Objet="55" V_Information="7" V_SiDonneeDico="true" V_SiFenetrePer_Bis="true" EtiWidth="80px" DonWidth="80px" DonTabIndex="2" EtiStyle="margin-left:2px" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left" ColumnSpan="2">
                                    <Virtualia:VDuoEtiquetteCommande ID="Dontab01" runat="server" EtiBackColor="#CBF2BE" V_PointdeVue="1" V_Objet="55" V_Information="1" V_SiDonneeDico="true" DonTabIndex="3" V_SiFenetrePer_Bis="true" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left" ColumnSpan="2">
                                    <Virtualia:VDuoEtiquetteCommande ID="Dontab05" runat="server" EtiBackColor="#CBF2BE" V_PointdeVue="1" V_Objet="55" V_Information="5" V_SiDonneeDico="true" DonTabIndex="4" EtiText="Administration" V_SiFenetrePer_Bis="true" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left" ColumnSpan="2">
                                    <Virtualia:VDuoEtiquetteCommande ID="Dontab11" runat="server" EtiBackColor="#CBF2BE" V_PointdeVue="1" V_Objet="55" V_Information="11" V_SiDonneeDico="true" DonTabIndex="5" V_SiFenetrePer_Bis="true" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left" ColumnSpan="2">
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH02" runat="server" EtiWidth="300px" V_PointdeVue="1" V_Objet="55" V_Information="2" V_SiDonneeDico="true" DonWidth="80px" DonTabIndex="6" EtiStyle="text-align:center" EtiBackColor="#CBF2BE" V_SiFenetrePer_Bis="true" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left" ColumnSpan="2">
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH12" runat="server" EtiWidth="300px" V_PointdeVue="1" V_Objet="55" V_Information="12" V_SiDonneeDico="true" DonWidth="80px" DonTabIndex="7" EtiStyle="text-align:center" EtiBackColor="#CBF2BE" V_SiFenetrePer_Bis="true" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left" ColumnSpan="2">
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH08" runat="server" EtiBackColor="#CBF2BE" V_PointdeVue="1" V_Objet="55" V_Information="8" V_SiDonneeDico="true" DonTabIndex="8" V_SiFenetrePer_Bis="true" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow Height="25px">
                                <asp:TableCell HorizontalAlign="Center" ColumnSpan="2">
                                    <asp:Label ID="EtiArrete" runat="server" Height="20px" Width="350px" Text="Date et référence de l'arrété" BackColor="#CBF2BE" BorderColor="#B0E0D7" BorderStyle="Outset" BorderWidth="2px" ForeColor="#142425" Font-Italic="true" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Right">
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH03" runat="server" EtiBackColor="#CBF2BE" V_PointdeVue="1" V_Objet="55" V_Information="3" V_SiDonneeDico="true" DonWidth="80px" DonTabIndex="9" EtiVisible="false" V_SiFenetrePer_Bis="true" />
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Left">
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH04" runat="server" EtiBackColor="#CBF2BE" V_PointdeVue="1" V_Objet="55" V_Information="4" V_SiDonneeDico="true" V_SiFenetrePer_Bis="true" DonWidth="200px" DonTabIndex="10" EtiVisible="false" DonStyle="margin-left:2px" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center" ColumnSpan="2">
                                    <asp:Label ID="EtiDecret" runat="server" Height="20px" Width="350px" Text="Date et référence de parution au JO du décret" BackColor="#CBF2BE" BorderColor="#B0E0D7" BorderStyle="Outset" BorderWidth="2px" ForeColor="#142425" Font-Italic="true" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Right">
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH09" runat="server" EtiBackColor="#CBF2BE" V_PointdeVue="1" V_Objet="55" V_Information="9" V_SiDonneeDico="true" V_SiFenetrePer_Bis="true" DonWidth="80px" DonTabIndex="11" EtiVisible="false" />
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Left">
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH10" runat="server" EtiBackColor="#CBF2BE" V_PointdeVue="1" V_Objet="55" V_Information="10" V_SiDonneeDico="true" V_SiFenetrePer_Bis="true" DonWidth="200px" DonTabIndex="12" EtiVisible="false" DonStyle="margin-left:2px" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Height="12px" ColumnSpan="2" />
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>
</asp:Table>
