﻿<%@ Control Language="VB" AutoEventWireup="false" Inherits="Virtualia.Net.Fenetre_PER_ACTIVITE" CodeBehind="PER_ACTIVITE.ascx.vb" %>

<%@ Register Src="~/Controles/Saisies/VCoupleEtiDonnee.ascx" TagName="VCoupleEtiDonnee" TagPrefix="Virtualia" %>
<%@ Register Src="~/Controles/Saisies/VDuoEtiquetteCommande.ascx" TagName="VDuoEtiquetteCommande" TagPrefix="Virtualia" %>
<%@ Register Src="~/Controles/Saisies/VCoupleVerticalEtiDonnee.ascx" TagName="VCoupleVerticalEtiDonnee" TagPrefix="Virtualia" %>
<%@ Register Src="~/Controles/Saisies/VCoupleEtiquetteExperte.ascx" TagName="VCoupleEtiquetteExperte" TagPrefix="Virtualia" %>

<%@ Register Src="~/ControlsGeneriques/V_DataGrid.ascx" TagName="VDataGrid" TagPrefix="Generic" %>
<%@ Register Src="~/ControlsGeneriques/V_CommandeCRUD.ascx" TagName="VCrud" TagPrefix="Generic" %>

<asp:Table ID="CadreControle" runat="server" Height="30px" Width="500px" CellPadding="0" CellSpacing="0" HorizontalAlign="Center">
    <asp:TableRow>
        <asp:TableCell>
            <Generic:VDataGrid ID="ListeGrille" runat="server" CadreWidth="480px" SiColonneSelect="true" SiCaseAcocher="false" />
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="CadreInfo" runat="server" BorderStyle="Ridge" BorderWidth="2px" BorderColor="#B0E0D7" Height="360px" Width="550px" HorizontalAlign="Center" Style="margin-top: 3px;">
                <asp:TableRow>
                    <asp:TableCell>
                        <Generic:VCrud ID="CtlCrud" runat="server" Height="22px" Width="244px" HorizontalAlign="Right" CadreStyle="margin-top: 3px; margin-right: 3px" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Table ID="CadreTitre" runat="server" Height="40px" CellPadding="0" Width="550px" CellSpacing="0" HorizontalAlign="Center">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                    <asp:Label ID="Etiquette" runat="server" Text="Activité" Height="20px" Width="280px" BackColor="#2FA49B" BorderColor="#B0E0D7" BorderStyle="Groove" BorderWidth="2px" ForeColor="#D7FAF3" Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Small" Style="margin-top: 5px; margin-left: 4px; margin-bottom: 10px; font-style: oblique; text-indent: 5px; text-align: center;" />
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Table ID="CadreDates" runat="server" CellPadding="0" CellSpacing="0" Width="550px">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH00" runat="server" V_PointdeVue="1" V_Objet="13" V_Information="0" V_SiDonneeDico="true" EtiWidth="60px" DonWidth="80px" DonTabIndex="1" />
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Left">
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH05" runat="server" V_PointdeVue="1" V_Objet="13" V_Information="5" V_SiDonneeDico="true" EtiWidth="60px" DonWidth="80px" DonTabIndex="2" />
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Left">
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH04" runat="server" V_PointdeVue="1" V_Objet="13" V_Information="4" V_SiDonneeDico="true" EtiWidth="90px" DonWidth="100px" DonTabIndex="3" />
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Table ID="CadreActivite" runat="server" CellPadding="0" CellSpacing="0" Width="550px">
                            <asp:TableRow>
                                <asp:TableCell Height="10px" ColumnSpan="2" />
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left" ColumnSpan="2">
                                    <Virtualia:VDuoEtiquetteCommande ID="Dontab01" runat="server" V_PointdeVue="1" V_Objet="13" V_Information="1" V_SiDonneeDico="true" EtiWidth="140px" DonWidth="350px" DonTabIndex="4" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH06" runat="server" V_PointdeVue="1" V_Objet="13" V_Information="6" V_SiDonneeDico="true" EtiWidth="240px" DonWidth="70px" DonTabIndex="5" />
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Left">
                                    <asp:Label ID="LabelHeures6" runat="server" Text="heures" Height="20px" Width="210px" BackColor="Transparent" BorderStyle="none" BorderWidth="0px" ForeColor="#2FA49B" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; font-style: oblique; text-indent: 0px; text-align: left;" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH02" runat="server" V_PointdeVue="1" V_Objet="13" V_Information="2" V_SiDonneeDico="true" EtiWidth="240px" DonWidth="70px" DonTabIndex="6" />
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Left">
                                    <asp:Label ID="LabelHeures2" runat="server" Text="heures" Height="20px" Width="210px" BackColor="Transparent" BorderStyle="none" BorderWidth="0px" ForeColor="#2FA49B" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; font-style: oblique; text-indent: 0px; text-align: left;" />
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Table ID="TableauObservation" runat="server" CellPadding="0" CellSpacing="0" Width="550px">
                            <asp:TableRow>
                                <asp:TableCell Height="10px" />
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <Virtualia:VCoupleVerticalEtiDonnee ID="InfoV03" runat="server" DonTextMode="true" V_PointdeVue="1" V_Objet="13" V_Information="3" V_SiDonneeDico="true" EtiWidth="530px" DonWidth="528px" DonHeight="100px" DonTabIndex="7" EtiStyle="text-align:center; margin-left: 5px;" Donstyle="margin-left: 5px;" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Height="12px" />
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Panel ID="CadreExpert" runat="server" BorderStyle="None" BorderWidth="1px" BorderColor="#B0E0D7" Height="70px" Width="552px" HorizontalAlign="Left" Style="margin-top: 5px; vertical-align: middle">
                            <Virtualia:VCoupleEtiquetteExperte ID="ExprH01" runat="server" V_PointdeVue="1" V_Objet="13" V_InfoExperte="930" V_SiDonneeDico="true" EtiHeight="20px" EtiWidth="200px" DonWidth="100px" DonHeight="20px" EtiStyle="margin-left: 50px; margin-top: 5px" DonStyle="margin-top: 9px; text-align: center" />
                            <Virtualia:VCoupleEtiquetteExperte ID="ExprH02" runat="server" V_PointdeVue="1" V_Objet="13" V_InfoExperte="936" V_SiDonneeDico="true" EtiHeight="20px" EtiWidth="200px" DonWidth="100px" DonHeight="20px" EtiStyle="margin-left: 50px; margin-top: 2px" DonStyle="margin-top: 2px; text-align: center" />
                        </asp:Panel>
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>
</asp:Table>
