﻿'------------------------------------------------------------------------------
' <généré automatiquement>
'     Ce code a été généré par un outil.
'
'     Les modifications apportées à ce fichier peuvent provoquer un comportement incorrect et seront perdues si
'     le code est régénéré.
' </généré automatiquement>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class Fenetre_PER_PLANNING_SEMAINE

    '''<summary>
    '''Contrôle CadreInfo.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CadreInfo As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle CadreTitre.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CadreTitre As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle Etiquette.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Etiquette As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle TableAnnualisation.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents TableAnnualisation As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle LigneContrat.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LigneContrat As Global.System.Web.UI.WebControls.TableRow

    '''<summary>
    '''Contrôle LabelContrat.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelContrat As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle LigneAnnualisation.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LigneAnnualisation As Global.System.Web.UI.WebControls.TableRow

    '''<summary>
    '''Contrôle LabelTempsAnnualise.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelTempsAnnualise As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle LabelTempsValide.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelTempsValide As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle LabelSoldeTemps.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelSoldeTemps As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle LstSemaines.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LstSemaines As Global.Virtualia.Net.Controles_VListeCombo

    '''<summary>
    '''Contrôle TableauHorairesHebdomadaire.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents TableauHorairesHebdomadaire As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle LigneTitre.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LigneTitre As Global.System.Web.UI.WebControls.TableRow

    '''<summary>
    '''Contrôle LabelTitreSemaine.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelTitreSemaine As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle LigneEntete.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LigneEntete As Global.System.Web.UI.WebControls.TableRow

    '''<summary>
    '''Contrôle LabelEnteteJour.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelEnteteJour As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle LabelEnteteBadgeage.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelEnteteBadgeage As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle LabelEnteteValidation.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelEnteteValidation As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle LabelEnteteInfos.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelEnteteInfos As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle Jour00.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Jour00 As Global.Virtualia.Net.Controles_VLigneDetailSemaine

    '''<summary>
    '''Contrôle Jour01.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Jour01 As Global.Virtualia.Net.Controles_VLigneDetailSemaine

    '''<summary>
    '''Contrôle Jour02.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Jour02 As Global.Virtualia.Net.Controles_VLigneDetailSemaine

    '''<summary>
    '''Contrôle Jour03.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Jour03 As Global.Virtualia.Net.Controles_VLigneDetailSemaine

    '''<summary>
    '''Contrôle Jour04.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Jour04 As Global.Virtualia.Net.Controles_VLigneDetailSemaine

    '''<summary>
    '''Contrôle Jour05.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Jour05 As Global.Virtualia.Net.Controles_VLigneDetailSemaine

    '''<summary>
    '''Contrôle Jour06.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Jour06 As Global.Virtualia.Net.Controles_VLigneDetailSemaine

    '''<summary>
    '''Contrôle TableTotaux.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents TableTotaux As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle LigneTotalPrevu.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LigneTotalPrevu As Global.System.Web.UI.WebControls.TableRow

    '''<summary>
    '''Contrôle TitreTotalPrevu.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents TitreTotalPrevu As Global.System.Web.UI.WebControls.TableCell

    '''<summary>
    '''Contrôle LabelTotalPrevu.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelTotalPrevu As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle TotalPrevu.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents TotalPrevu As Global.System.Web.UI.WebControls.TableCell

    '''<summary>
    '''Contrôle LabelPrevu.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelPrevu As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle LigneTotalValide.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LigneTotalValide As Global.System.Web.UI.WebControls.TableRow

    '''<summary>
    '''Contrôle CellTotalValide.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellTotalValide As Global.System.Web.UI.WebControls.TableCell

    '''<summary>
    '''Contrôle LabelTotalValide.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelTotalValide As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle TotalValide.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents TotalValide As Global.System.Web.UI.WebControls.TableCell

    '''<summary>
    '''Contrôle LabelValide.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelValide As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle LigneTotalComptabilise.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LigneTotalComptabilise As Global.System.Web.UI.WebControls.TableRow

    '''<summary>
    '''Contrôle CellTotalComptabilise.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellTotalComptabilise As Global.System.Web.UI.WebControls.TableCell

    '''<summary>
    '''Contrôle LabelTotalComptabilise.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelTotalComptabilise As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle TotalComptabilise.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents TotalComptabilise As Global.System.Web.UI.WebControls.TableCell

    '''<summary>
    '''Contrôle LabelComptabilise.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelComptabilise As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle Separateur.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Separateur As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle LabelCommentaire.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelCommentaire As Global.System.Web.UI.WebControls.Label
End Class
