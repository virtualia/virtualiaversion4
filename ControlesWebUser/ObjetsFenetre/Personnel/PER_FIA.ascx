﻿<%@ Control Language="VB" AutoEventWireup="false" Inherits="Virtualia.Net.Fenetre_PER_FIA" CodeBehind="PER_FIA.ascx.vb" %>

<%@ Register Src="~/Controles/Saisies/VCoupleEtiDonnee.ascx" TagName="VCoupleEtiDonnee" TagPrefix="Virtualia" %>
<%@ Register Src="~/Controles/Saisies/VDuoEtiquetteCommande.ascx" TagName="VDuoEtiquetteCommande" TagPrefix="Virtualia" %>
<%@ Register Src="~/Controles/Saisies/VCoupleVerticalEtiDonnee.ascx" TagName="VCoupleVerticalEtiDonnee" TagPrefix="Virtualia" %>

<%@ Register Src="~/ControlsGeneriques/V_CommandeOK.ascx" TagName="VCmdOK" TagPrefix="Generic" %>

<asp:Table ID="CadreInfo" runat="server" BorderStyle="Ridge" BorderWidth="2px" BorderColor="#B0E0D7" Height="770px" Width="750px" HorizontalAlign="Center" Style="position: relative">
    <asp:TableRow>
        <asp:TableCell>
            <Generic:VCmdOK ID="CtlOK" runat="server" Height="22px" Width="70px" HorizontalAlign="Right" CadreStyle="margin-top: 3px; margin-right: 3px" />
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="CadreTitreFI" runat="server" Height="40px" CellPadding="0" CellSpacing="0" HorizontalAlign="Center">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="EtiFI" runat="server" Text="Formation - Intégration" Height="20px" Width="330px" BackColor="#2FA49B" BorderColor="#B0E0D7" BorderStyle="Groove" BorderWidth="2px" ForeColor="#D7FAF3" Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Small" Style="margin-top: 15px; margin-left: 4px; margin-bottom: 0px; font-style: oblique; text-align: center;" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="15px" />
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="CadreDatesFI" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VCoupleEtiDonnee ID="InfoH01" runat="server" V_PointdeVue="1" V_Objet="70" V_Information="1" V_SiDonneeDico="true" EtiWidth="190px" DonWidth="80px" DonTabIndex="1" />
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VCoupleEtiDonnee ID="InfoH02" runat="server" V_PointdeVue="1" V_Objet="70" V_Information="2" V_SiDonneeDico="true" EtiWidth="190px" DonWidth="80px" DonTabIndex="2" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="10px" ColumnSpan="2" />
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="TableauTitreFI" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Label ID="EtiFiliereFI" runat="server" Height="20px" Width="430px" Text="Filière - Formation Intégration" BackColor="#B0E0D7" BorderColor="#B0E0D7" BorderStyle="Outset" BorderWidth="2px" ForeColor="#142425" Font-Italic="true" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Style="margin-top: 1px; margin-left: 35px; text-indent: 5px; text-align: center" />
                    </asp:TableCell>
                    <asp:TableCell>
                        <asp:Label ID="EtiNbjoursFI" runat="server" Height="20px" Width="70px" Text="Nbr. jours" BackColor="#B0E0D7" BorderColor="#B0E0D7" BorderStyle="Outset" BorderWidth="2px" ForeColor="#142425" Font-Italic="true" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Style="margin-top: 1px; margin-left: 1px; text-indent: 5px; text-align: center" />
                    </asp:TableCell>
                    <asp:TableCell>
                        <asp:Label ID="EtiNbjFaitsFI" runat="server" Height="20px" Width="70px" Text="Fait" BackColor="#B0E0D7" BorderColor="#B0E0D7" BorderStyle="Outset" BorderWidth="2px" ForeColor="#142425" Font-Italic="true" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Style="margin-top: 1px; margin-left: 1px; text-indent: 5px; text-align: center" />
                    </asp:TableCell>
                    <asp:TableCell>
                        <asp:Label ID="EtiNbjResteFI" runat="server" Height="20px" Width="70px" Text="Reste" BackColor="#B0E0D7" BorderColor="#B0E0D7" BorderStyle="Outset" BorderWidth="2px" ForeColor="#142425" Font-Italic="true" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Style="margin-top: 1px; margin-left: 1px; text-indent: 5px; text-align: center" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="5px" ColumnSpan="4" />
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="TableauDonFI1" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Label ID="LabelDebutLigneFI1" runat="server" Height="20px" Width="5px" Text="" BackColor="#B0E0D7" BorderColor="#B0E0D7" BorderStyle="Outset" BorderWidth="2px" ForeColor="#142425" Font-Italic="true" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Style="margin-top: 0px; margin-left: 35px; text-indent: 5px; text-align: center" />
                    </asp:TableCell>
                    <asp:TableCell>
                        <Virtualia:VDuoEtiquetteCommande ID="Dontab03" runat="server" V_PointdeVue="1" V_Objet="70" V_Information="3" V_SiDonneeDico="true" EtiVisible="false" DonWidth="420px" DonTabIndex="3" DonStyle="margin-top:1px" EtiStyle="margin-left:1px" />
                    </asp:TableCell>
                    <asp:TableCell>
                        <Virtualia:VCoupleEtiDonnee ID="InfoH04" runat="server" V_PointdeVue="1" V_Objet="70" V_Information="4" V_SiDonneeDico="true" EtiVisible="false" DonWidth="70px" DonTabIndex="4" />
                    </asp:TableCell>
                    <asp:TableCell>
                        <asp:Label ID="LabelFIFait1" runat="server" Height="18px" Width="70px" Text=" " BackColor="WhiteSmoke" BorderColor="#B0E0D7" BorderStyle="Outset" BorderWidth="2px" ForeColor="#142425" Font-Italic="true" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Style="margin-top: 0px; margin-left: 1px; text-indent: 5px; text-align: center" />
                    </asp:TableCell>
                    <asp:TableCell>
                        <asp:Label ID="LabelFIReste1" runat="server" Height="18px" Width="70px" Text=" " BackColor="WhiteSmoke" BorderColor="#B0E0D7" BorderStyle="Outset" BorderWidth="2px" ForeColor="#142425" Font-Italic="true" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Style="margin-top: 0px; margin-left: 1px; text-indent: 5px; text-align: center" />
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="TableauDonFI2" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Label ID="LabelDebutLigneFI2" runat="server" Height="20px" Width="5px" Text="" BackColor="#B0E0D7" BorderColor="#B0E0D7" BorderStyle="Outset" BorderWidth="2px" ForeColor="#142425" Font-Italic="true" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Style="margin-top: 0px; margin-left: 35px; text-indent: 5px; text-align: center" />
                    </asp:TableCell>
                    <asp:TableCell>
                        <Virtualia:VDuoEtiquetteCommande ID="Dontab05" runat="server" V_PointdeVue="1" V_Objet="70" V_Information="5" V_SiDonneeDico="true" EtiVisible="false" DonWidth="420px" DonTabIndex="5" DonStyle="margin-top:1px" EtiStyle="margin-left:1px" />
                    </asp:TableCell>
                    <asp:TableCell>
                        <Virtualia:VCoupleEtiDonnee ID="InfoH06" runat="server" V_PointdeVue="1" V_Objet="70" V_Information="6" V_SiDonneeDico="true" EtiVisible="false" DonWidth="70px" DonTabIndex="6" />
                    </asp:TableCell>
                    <asp:TableCell>
                        <asp:Label ID="LabelFIFait2" runat="server" Height="18px" Width="70px" Text=" " BackColor="WhiteSmoke" BorderColor="#B0E0D7" BorderStyle="Outset" BorderWidth="2px" ForeColor="#142425" Font-Italic="true" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Style="margin-top: 0px; margin-left: 1px; text-indent: 5px; text-align: center" />
                    </asp:TableCell>
                    <asp:TableCell>
                        <asp:Label ID="LabelFIReste2" runat="server" Height="18px" Width="70px" Text=" " BackColor="WhiteSmoke" BorderColor="#B0E0D7" BorderStyle="Outset" BorderWidth="2px" ForeColor="#142425" Font-Italic="true" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Style="margin-top: 0px; margin-left: 1px; text-indent: 5px; text-align: center" />
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="TableauDonFI3" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Label ID="LabelDebutLigneFI3" runat="server" Height="20px" Width="5px" Text="" BackColor="#B0E0D7" BorderColor="#B0E0D7" BorderStyle="Outset" BorderWidth="2px" ForeColor="#142425" Font-Italic="true" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Style="margin-top: 0px; margin-left: 35px; text-indent: 5px; text-align: center" />
                    </asp:TableCell>
                    <asp:TableCell>
                        <Virtualia:VDuoEtiquetteCommande ID="Dontab07" runat="server" V_PointdeVue="1" V_Objet="70" V_Information="7" V_SiDonneeDico="true" EtiVisible="false" DonWidth="420px" DonTabIndex="7" DonStyle="margin-top:1px" EtiStyle="margin-left:1px" />
                    </asp:TableCell>
                    <asp:TableCell>
                        <Virtualia:VCoupleEtiDonnee ID="InfoH08" runat="server" V_PointdeVue="1" V_Objet="70" V_Information="8" V_SiDonneeDico="true" EtiVisible="false" DonWidth="70px" DonTabIndex="8" />
                    </asp:TableCell>
                    <asp:TableCell>
                        <asp:Label ID="LabelFIFait3" runat="server" Height="18px" Width="70px" Text=" " BackColor="WhiteSmoke" BorderColor="#B0E0D7" BorderStyle="Outset" BorderWidth="2px" ForeColor="#142425" Font-Italic="true" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Style="margin-top: 0px; margin-left: 1px; text-indent: 5px; text-align: center" />
                    </asp:TableCell>
                    <asp:TableCell>
                        <asp:Label ID="LabelFIReste3" runat="server" Height="18px" Width="70px" Text=" " BackColor="WhiteSmoke" BorderColor="#B0E0D7" BorderStyle="Outset" BorderWidth="2px" ForeColor="#142425" Font-Italic="true" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Style="margin-top: 0px; margin-left: 1px; text-indent: 5px; text-align: center" />
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="TableauDonFI4" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Label ID="LabelDebutLigneFI4" runat="server" Height="20px" Width="5px" Text="" BackColor="#B0E0D7" BorderColor="#B0E0D7" BorderStyle="Outset" BorderWidth="2px" ForeColor="#142425" Font-Italic="true" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Style="margin-top: 0px; margin-left: 35px; text-indent: 5px; text-align: center" />
                    </asp:TableCell>
                    <asp:TableCell>
                        <Virtualia:VDuoEtiquetteCommande ID="Dontab09" runat="server" V_PointdeVue="1" V_Objet="70" V_Information="9" V_SiDonneeDico="true" EtiVisible="false" DonWidth="420px" DonTabIndex="9" DonStyle="margin-top:1px" EtiStyle="margin-left:1px" />
                    </asp:TableCell>
                    <asp:TableCell>
                        <Virtualia:VCoupleEtiDonnee ID="InfoH10" runat="server" V_PointdeVue="1" V_Objet="70" V_Information="10" V_SiDonneeDico="true" EtiVisible="false" DonWidth="70px" DonTabIndex="10" />
                    </asp:TableCell>
                    <asp:TableCell>
                        <asp:Label ID="LabelFIFait4" runat="server" Height="18px" Width="70px" Text=" " BackColor="WhiteSmoke" BorderColor="#B0E0D7" BorderStyle="Outset" BorderWidth="2px" ForeColor="#142425" Font-Italic="true" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Style="margin-top: 0px; margin-left: 1px; text-indent: 5px; text-align: center" />
                    </asp:TableCell>
                    <asp:TableCell>
                        <asp:Label ID="LabelFIReste4" runat="server" Height="18px" Width="70px" Text=" " BackColor="WhiteSmoke" BorderColor="#B0E0D7" BorderStyle="Outset" BorderWidth="2px" ForeColor="#142425" Font-Italic="true" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Style="margin-top: 0px; margin-left: 1px; text-indent: 5px; text-align: center" />
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="TableauDonFI5" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Label ID="LabelDebutLigneFI5" runat="server" Height="20px" Width="5px" Text="" BackColor="#B0E0D7" BorderColor="#B0E0D7" BorderStyle="Outset" BorderWidth="2px" ForeColor="#142425" Font-Italic="true" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Style="margin-top: 0px; margin-left: 35px; text-indent: 5px; text-align: center" />
                    </asp:TableCell>
                    <asp:TableCell>
                        <Virtualia:VDuoEtiquetteCommande ID="Dontab11" runat="server" V_PointdeVue="1" V_Objet="70" V_Information="11" V_SiDonneeDico="true" EtiVisible="false" DonWidth="420px" DonTabIndex="11" DonStyle="margin-top:1px" EtiStyle="margin-left:1px" />
                    </asp:TableCell>
                    <asp:TableCell>
                        <Virtualia:VCoupleEtiDonnee ID="InfoH12" runat="server" V_PointdeVue="1" V_Objet="70" V_Information="12" V_SiDonneeDico="true" EtiVisible="false" DonWidth="70px" DonTabIndex="12" />
                    </asp:TableCell>
                    <asp:TableCell>
                        <asp:Label ID="LabelFIFait5" runat="server" Height="18px" Width="70px" Text=" " BackColor="WhiteSmoke" BorderColor="#B0E0D7" BorderStyle="Outset" BorderWidth="2px" ForeColor="#142425" Font-Italic="true" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Style="margin-top: 0px; margin-left: 1px; text-indent: 5px; text-align: center" />
                    </asp:TableCell>
                    <asp:TableCell>
                        <asp:Label ID="LabelFIReste5" runat="server" Height="18px" Width="70px" Text=" " BackColor="WhiteSmoke" BorderColor="#B0E0D7" BorderStyle="Outset" BorderWidth="2px" ForeColor="#142425" Font-Italic="true" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Style="margin-top: 0px; margin-left: 1px; text-indent: 5px; text-align: center" />
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="TableauDonFITotal" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Label ID="LabelDebutLigneTotalFI" runat="server" Height="20px" Width="431px" Text="Total F.I." BackColor="#B0E0D7" BorderColor="#B0E0D7" BorderStyle="Outset" BorderWidth="2px" ForeColor="#142425" Font-Italic="true" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Style="margin-top: 5px; margin-left: 35px; text-indent: 5px; text-align: center" />
                    </asp:TableCell>
                    <asp:TableCell>
                        <Virtualia:VCoupleEtiDonnee ID="InfoH13" runat="server" V_PointdeVue="1" V_Objet="70" V_Information="13" V_SiDonneeDico="true" EtiWidth="180px" EtiVisible="false" DonWidth="70px" DonTabIndex="13" DonStyle="margin-top:5px" />
                    </asp:TableCell>
                    <asp:TableCell>
                        <asp:Label ID="LabelFIFaitTotal" runat="server" Height="18px" Width="70px" Text=" " BackColor="WhiteSmoke" BorderColor="#B0E0D7" BorderStyle="Outset" BorderWidth="2px" ForeColor="#142425" Font-Italic="true" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Style="margin-top: 5px; margin-left: 1px; text-indent: 5px; text-align: center" />
                    </asp:TableCell>
                    <asp:TableCell>
                        <asp:Label ID="LabelFIResteTotal" runat="server" Height="18px" Width="70px" Text=" " BackColor="WhiteSmoke" BorderColor="#B0E0D7" BorderStyle="Outset" BorderWidth="2px" ForeColor="#142425" Font-Italic="true" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Style="margin-top: 5px; margin-left: 1px; text-indent: 5px; text-align: center" />
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="CadreTitreFP" runat="server" Height="40px" CellPadding="0" CellSpacing="0" HorizontalAlign="Center">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="EtiFP" runat="server" Text="Formation - Professionnalisation" Height="20px" Width="330px" BackColor="#2FA49B" BorderColor="#B0E0D7" BorderStyle="Groove" BorderWidth="2px" ForeColor="#D7FAF3" Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Small" Style="margin-top: 25px; margin-left: 4px; margin-bottom: 0px; font-style: oblique; text-align: center;" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="15px" />
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="CadreDatesFP" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left" ColumnSpan="2">
                        <Virtualia:VCoupleEtiDonnee ID="InfoH14" runat="server" V_PointdeVue="1" V_Objet="70" V_Information="14" V_SiDonneeDico="true" EtiWidth="190px" DonWidth="80px" DonTabIndex="14" />
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VCoupleEtiDonnee ID="InfoH15" runat="server" V_PointdeVue="1" V_Objet="70" V_Information="15" V_SiDonneeDico="true" EtiWidth="190px" DonWidth="80px" DonTabIndex="15" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="10px" ColumnSpan="2" />
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="TableauTitreFP" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Label ID="EtiFiliereFP" runat="server" Height="20px" Width="430px" Text="Filière - Formation Professionnalisation" BackColor="#B0E0D7" BorderColor="#B0E0D7" BorderStyle="Outset" BorderWidth="2px" ForeColor="#142425" Font-Italic="true" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Style="margin-top: 1px; margin-left: 35px; text-indent: 5px; text-align: center" />
                    </asp:TableCell>
                    <asp:TableCell>
                        <asp:Label ID="EtiNbjoursFP" runat="server" Height="20px" Width="70px" Text="Nbr. jours" BackColor="#B0E0D7" BorderColor="#B0E0D7" BorderStyle="Outset" BorderWidth="2px" ForeColor="#142425" Font-Italic="true" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Style="margin-top: 1px; margin-left: 1px; text-indent: 5px; text-align: center" />
                    </asp:TableCell>
                    <asp:TableCell>
                        <asp:Label ID="EtiNbjFaitsFP" runat="server" Height="20px" Width="70px" Text="Fait" BackColor="#B0E0D7" BorderColor="#B0E0D7" BorderStyle="Outset" BorderWidth="2px" ForeColor="#142425" Font-Italic="true" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Style="margin-top: 1px; margin-left: 1px; text-indent: 5px; text-align: center" />
                    </asp:TableCell>
                    <asp:TableCell>
                        <asp:Label ID="EtiNbjResteFP" runat="server" Height="20px" Width="70px" Text="Reste" BackColor="#B0E0D7" BorderColor="#B0E0D7" BorderStyle="Outset" BorderWidth="2px" ForeColor="#142425" Font-Italic="true" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Style="margin-top: 1px; margin-left: 1px; text-indent: 5px; text-align: center" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="5px" ColumnSpan="4" />
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="TableauDonFP1" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Label ID="LabelDebutLigneFP1" runat="server" Height="20px" Width="5px" Text="" BackColor="#B0E0D7" BorderColor="#B0E0D7" BorderStyle="Outset" BorderWidth="2px" ForeColor="#142425" Font-Italic="true" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Style="margin-top: 0px; margin-left: 35px; text-indent: 5px; text-align: center" />
                    </asp:TableCell>
                    <asp:TableCell>
                        <Virtualia:VDuoEtiquetteCommande ID="Dontab16" runat="server" V_PointdeVue="1" V_Objet="70" V_Information="16" V_SiDonneeDico="true" EtiVisible="false" DonWidth="420px" DonTabIndex="16" DonStyle="margin-top:1px" EtiStyle="margin-left:1px" />
                    </asp:TableCell>
                    <asp:TableCell>
                        <Virtualia:VCoupleEtiDonnee ID="InfoH17" runat="server" V_PointdeVue="1" V_Objet="70" V_Information="17" V_SiDonneeDico="true" EtiVisible="false" DonWidth="70px" DonTabIndex="17" />
                    </asp:TableCell>
                    <asp:TableCell>
                        <asp:Label ID="LabelFPFait1" runat="server" Height="18px" Width="70px" Text=" " BackColor="WhiteSmoke" BorderColor="#B0E0D7" BorderStyle="Outset" BorderWidth="2px" ForeColor="#142425" Font-Italic="true" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Style="margin-top: 0px; margin-left: 1px; text-indent: 5px; text-align: center" />
                    </asp:TableCell>
                    <asp:TableCell>
                        <asp:Label ID="LabelFPReste1" runat="server" Height="18px" Width="70px" Text=" " BackColor="WhiteSmoke" BorderColor="#B0E0D7" BorderStyle="Outset" BorderWidth="2px" ForeColor="#142425" Font-Italic="true" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Style="margin-top: 0px; margin-left: 1px; text-indent: 5px; text-align: center" />
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="TableauDonFP2" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Label ID="LabelDebutLigneFP2" runat="server" Height="20px" Width="5px" Text="" BackColor="#B0E0D7" BorderColor="#B0E0D7" BorderStyle="Outset" BorderWidth="2px" ForeColor="#142425" Font-Italic="true" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Style="margin-top: 0px; margin-left: 35px; text-indent: 5px; text-align: center" />
                    </asp:TableCell>
                    <asp:TableCell>
                        <Virtualia:VDuoEtiquetteCommande ID="Dontab18" runat="server" V_PointdeVue="1" V_Objet="70" V_Information="18" V_SiDonneeDico="true" EtiVisible="false" DonWidth="420px" DonTabIndex="18" DonStyle="margin-top:1px" EtiStyle="margin-left:1px" />
                    </asp:TableCell>
                    <asp:TableCell>
                        <Virtualia:VCoupleEtiDonnee ID="InfoH19" runat="server" V_PointdeVue="1" V_Objet="70" V_Information="19" V_SiDonneeDico="true" EtiVisible="false" DonWidth="70px" DonTabIndex="19" />
                    </asp:TableCell>
                    <asp:TableCell>
                        <asp:Label ID="LabelFPFait2" runat="server" Height="18px" Width="70px" Text=" " BackColor="WhiteSmoke" BorderColor="#B0E0D7" BorderStyle="Outset" BorderWidth="2px" ForeColor="#142425" Font-Italic="true" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Style="margin-top: 0px; margin-left: 1px; text-indent: 5px; text-align: center" />
                    </asp:TableCell>
                    <asp:TableCell>
                        <asp:Label ID="LabelFPReste2" runat="server" Height="18px" Width="70px" Text=" " BackColor="WhiteSmoke" BorderColor="#B0E0D7" BorderStyle="Outset" BorderWidth="2px" ForeColor="#142425" Font-Italic="true" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Style="margin-top: 0px; margin-left: 1px; text-indent: 5px; text-align: center" />
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="TableauDonFP3" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Label ID="LabelDebutLigneFP3" runat="server" Height="20px" Width="5px" Text="" BackColor="#B0E0D7" BorderColor="#B0E0D7" BorderStyle="Outset" BorderWidth="2px" ForeColor="#142425" Font-Italic="true" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Style="margin-top: 0px; margin-left: 35px; text-indent: 5px; text-align: center" />
                    </asp:TableCell>
                    <asp:TableCell>
                        <Virtualia:VDuoEtiquetteCommande ID="Dontab20" runat="server" V_PointdeVue="1" V_Objet="70" V_Information="20" V_SiDonneeDico="true" EtiVisible="false" DonWidth="420px" DonTabIndex="20" DonStyle="margin-top:1px" EtiStyle="margin-left:1px" />
                    </asp:TableCell>
                    <asp:TableCell>
                        <Virtualia:VCoupleEtiDonnee ID="InfoH21" runat="server" V_PointdeVue="1" V_Objet="70" V_Information="21" V_SiDonneeDico="true" EtiVisible="false" DonWidth="70px" DonTabIndex="21" />
                    </asp:TableCell>
                    <asp:TableCell>
                        <asp:Label ID="LabelFPFait3" runat="server" Height="18px" Width="70px" Text=" " BackColor="WhiteSmoke" BorderColor="#B0E0D7" BorderStyle="Outset" BorderWidth="2px" ForeColor="#142425" Font-Italic="true" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Style="margin-top: 0px; margin-left: 1px; text-indent: 5px; text-align: center" />
                    </asp:TableCell>
                    <asp:TableCell>
                        <asp:Label ID="LabelFPReste3" runat="server" Height="18px" Width="70px" Text=" " BackColor="WhiteSmoke" BorderColor="#B0E0D7" BorderStyle="Outset" BorderWidth="2px" ForeColor="#142425" Font-Italic="true" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Style="margin-top: 0px; margin-left: 1px; text-indent: 5px; text-align: center" />
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="TableauDonFP4" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Label ID="LabelDebutLigneFP4" runat="server" Height="20px" Width="5px" Text="" BackColor="#B0E0D7" BorderColor="#B0E0D7" BorderStyle="Outset" BorderWidth="2px" ForeColor="#142425" Font-Italic="true" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Style="margin-top: 0px; margin-left: 35px; text-indent: 5px; text-align: center" />
                    </asp:TableCell>
                    <asp:TableCell>
                        <Virtualia:VDuoEtiquetteCommande ID="Dontab22" runat="server" V_PointdeVue="1" V_Objet="70" V_Information="22" V_SiDonneeDico="true" EtiVisible="false" DonWidth="420px" DonTabIndex="22" DonStyle="margin-top:1px" EtiStyle="margin-left:1px" />
                    </asp:TableCell>
                    <asp:TableCell>
                        <Virtualia:VCoupleEtiDonnee ID="InfoH23" runat="server" V_PointdeVue="1" V_Objet="70" V_Information="23" V_SiDonneeDico="true" EtiVisible="false" DonWidth="70px" DonTabIndex="23" />
                    </asp:TableCell>
                    <asp:TableCell>
                        <asp:Label ID="LabelFPFait4" runat="server" Height="18px" Width="70px" Text=" " BackColor="WhiteSmoke" BorderColor="#B0E0D7" BorderStyle="Outset" BorderWidth="2px" ForeColor="#142425" Font-Italic="true" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Style="margin-top: 0px; margin-left: 1px; text-indent: 5px; text-align: center" />
                    </asp:TableCell>
                    <asp:TableCell>
                        <asp:Label ID="LabelFPReste4" runat="server" Height="18px" Width="70px" Text=" " BackColor="WhiteSmoke" BorderColor="#B0E0D7" BorderStyle="Outset" BorderWidth="2px" ForeColor="#142425" Font-Italic="true" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Style="margin-top: 0px; margin-left: 1px; text-indent: 5px; text-align: center" />
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="TableauDonFP5" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Label ID="LabelDebutLigneFP5" runat="server" Height="20px" Width="5px" Text="" BackColor="#B0E0D7" BorderColor="#B0E0D7" BorderStyle="Outset" BorderWidth="2px" ForeColor="#142425" Font-Italic="true" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Style="margin-top: 0px; margin-left: 35px; text-indent: 5px; text-align: center" />
                    </asp:TableCell>
                    <asp:TableCell>
                        <Virtualia:VDuoEtiquetteCommande ID="Dontab24" runat="server" V_PointdeVue="1" V_Objet="70" V_Information="24" V_SiDonneeDico="true" EtiVisible="false" DonWidth="420px" DonTabIndex="24" DonStyle="margin-top:1px" EtiStyle="margin-left:1px" />
                    </asp:TableCell>
                    <asp:TableCell>
                        <Virtualia:VCoupleEtiDonnee ID="InfoH25" runat="server" V_PointdeVue="1" V_Objet="70" V_Information="25" V_SiDonneeDico="true" EtiVisible="false" DonWidth="70px" DonTabIndex="12" />
                    </asp:TableCell>
                    <asp:TableCell>
                        <asp:Label ID="LabelFPFait5" runat="server" Height="18px" Width="70px" Text=" " BackColor="WhiteSmoke" BorderColor="#B0E0D7" BorderStyle="Outset" BorderWidth="2px" ForeColor="#142425" Font-Italic="true" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Style="margin-top: 0px; margin-left: 1px; text-indent: 5px; text-align: center" />
                    </asp:TableCell>
                    <asp:TableCell>
                        <asp:Label ID="LabelFPReste5" runat="server" Height="18px" Width="70px" Text=" " BackColor="WhiteSmoke" BorderColor="#B0E0D7" BorderStyle="Outset" BorderWidth="2px" ForeColor="#142425" Font-Italic="true" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Style="margin-top: 0px; margin-left: 1px; text-indent: 5px; text-align: center" />
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="TableauDonFPTotal" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Label ID="LabelDebutLigneTotalFP" runat="server" Height="20px" Width="431px" Text="Total F.P." BackColor="#B0E0D7" BorderColor="#B0E0D7" BorderStyle="Outset" BorderWidth="2px" ForeColor="#142425" Font-Italic="true" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Style="margin-top: 5px; margin-left: 35px; text-indent: 5px; text-align: center" />
                    </asp:TableCell>
                    <asp:TableCell>
                        <Virtualia:VCoupleEtiDonnee ID="InfoH26" runat="server" V_PointdeVue="1" V_Objet="70" V_Information="26" V_SiDonneeDico="true" EtiWidth="180px" EtiVisible="false" DonWidth="70px" DonTabIndex="26" DonStyle="margin-top:5px" />
                    </asp:TableCell>
                    <asp:TableCell>
                        <asp:Label ID="LabelFPFaitTotal" runat="server" Height="18px" Width="70px" Text=" " BackColor="WhiteSmoke" BorderColor="#B0E0D7" BorderStyle="Outset" BorderWidth="2px" ForeColor="#142425" Font-Italic="true" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Style="margin-top: 5px; margin-left: 1px; text-indent: 5px; text-align: center" />
                    </asp:TableCell>
                    <asp:TableCell>
                        <asp:Label ID="LabelFPResteTotal" runat="server" Height="18px" Width="70px" Text=" " BackColor="WhiteSmoke" BorderColor="#B0E0D7" BorderStyle="Outset" BorderWidth="2px" ForeColor="#142425" Font-Italic="true" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Style="margin-top: 5px; margin-left: 1px; text-indent: 5px; text-align: center" />
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="TableauObservations" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                <asp:TableRow>
                    <asp:TableCell Height="25px" />
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="InfoV27" runat="server" DonTextMode="true" V_PointdeVue="1" V_Objet="70" V_Information="27" V_SiDonneeDico="true" EtiWidth="650px" DonWidth="648px" DonHeight="120px" DonTabIndex="27" EtiStyle="margin-left: 0px; text-align:center;" Donstyle="margin-left: 0px;" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="12px" />
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>
</asp:Table>
