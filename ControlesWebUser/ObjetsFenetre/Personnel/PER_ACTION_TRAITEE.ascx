﻿<%@ Control Language="VB" AutoEventWireup="false" Inherits="Virtualia.Net.Fenetre_PER_ACTION_TRAITEE" Codebehind="PER_ACTION_TRAITEE.ascx.vb" %>

 <asp:Table ID="CadreInfo" runat="server" BorderStyle="None" BorderWidth="2px" Visible="true" Height="190px"
    BorderColor="#B0E0D7" Width="500px" HorizontalAlign="Left" style="margin-top: 1px;">
    
    <asp:TableRow>
      <asp:TableCell HorizontalAlign="Left" ColumnSpan="2">
            <asp:Table ID="CadreActionEnCours" runat="server" CellPadding="0" Width="500px" Height="25px"
            CellSpacing="0" HorizontalAlign="Left" BorderStyle="Notset" BorderWidth="1px" Visible="true" BorderColor="#7EC8BE"
            style="margin-top: 1px;margin-bottom: 0px;">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                       <asp:Label ID="LabelObjet" runat="server" Height="25px" Width="500px"
                                BackColor="#CAEBE4" BorderColor="Transparent" BorderStyle="None" Text="Congés annuels du 10/05/2010 au 18/05/2010"
                                BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Small"
                                style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                font-style: normal; text-indent: 1px; text-align:  center; padding-top: 2px;" >
                       </asp:Label>      
                    </asp:TableCell>   
                </asp:TableRow>
            </asp:Table>
      </asp:TableCell>
    </asp:TableRow>
    
    <asp:TableRow>
      <asp:TableCell HorizontalAlign="Left">
            <asp:Table ID="TableDemande" runat="server" CellPadding="0" Width="250px" Height="50px"
            CellSpacing="0" HorizontalAlign="Left" BorderStyle="Notset" BorderWidth="1px" Visible="true" BorderColor="#7EC8BE"
            style="margin-top: -5px;margin-bottom: 0px;">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left" Height="30px">
                       <asp:Label ID="LabelNumeroDemande" runat="server" Height="25px" Width="250px"
                                BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" Text="Demande n°414"
                                BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Font-Underline="true"
                                style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                font-style: normal; text-indent: 4px; text-align:  Left; padding-top: 2px;" >
                       </asp:Label>      
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left" Height="20px">
                       <asp:Label ID="LabelDemande" runat="server" Height="20px" Width="250px"
                                BackColor= "Transparent" BorderColor="Transparent" BorderStyle="None" Text="Faite le 12/04/2010 à 11:25"
                                BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="12px"
                                style="margin-top: 4px; margin-left: 0px; margin-bottom: 0px;
                                font-style: normal; text-indent: 1px; text-align:  center;" >
                       </asp:Label>      
                    </asp:TableCell>   
                </asp:TableRow>
            </asp:Table>
      </asp:TableCell>
      <asp:TableCell HorizontalAlign="Left">
            <asp:Table ID="TableDecision" runat="server" CellPadding="0" Width="249px" Height="50px"
            CellSpacing="0" HorizontalAlign="Left" BorderStyle="Notset" BorderWidth="1px" Visible="true"
            BorderColor="#7EC8BE" BackColor="#DEF7D4"
            style="margin-top: -5px; margin-bottom: 0px; margin-left: -5px;">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left" Height="30px">
                       <asp:Label ID="LabelDecision" runat="server" Height="25px" Width="249px"
                                BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" Text="Décision"
                                BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Font-Underline="true"
                                style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                font-style: normal; text-indent: 4px; text-align:  Left; padding-top: 2px;" >
                       </asp:Label>      
                    </asp:TableCell>  
                </asp:TableRow>
                <asp:TableRow>   
                    <asp:TableCell HorizontalAlign="Left" Height="20px">
                       <asp:Label ID="LabelValidation" runat="server" Height="20px" Width="249px"
                                BackColor= "Transparent" BorderColor="Transparent" BorderStyle="None" Text="Validé le 20/04/2010 à 16:18"
                                BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="12px"
                                style="margin-top: 4px; margin-left: 0px; margin-bottom: 0px;
                                font-style: normal; text-indent: 1px; text-align:  center;" >
                       </asp:Label>      
                    </asp:TableCell>    
                </asp:TableRow>
            </asp:Table>
      </asp:TableCell>
    </asp:TableRow>  
        
    <asp:TableRow>
      <asp:TableCell HorizontalAlign="Left" ColumnSpan="2">
            <asp:Table ID="TableContenu" runat="server" CellPadding="0" Width="500px" Height="75px"
            CellSpacing="0" HorizontalAlign="Left" BorderStyle="Notset" BorderWidth="1px" Visible="true" BorderColor="#7EC8BE"
            style="margin-top: -5px;margin-bottom: 0px;">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left" Height="15px"  ColumnSpan="2">
                       <asp:Label ID="LabelContenu" runat="server" Height="15px" Width="120px"
                                BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" Text="Contenu"
                                BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Font-Underline="true"
                                style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                font-style: normal; text-indent: 4px; text-align:  Left; padding-top: 2px;" >
                       </asp:Label>      
                    </asp:TableCell>   
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left" Height="20px" ColumnSpan="2">
                       <asp:Label ID="LabelLigne1" runat="server" Height="20px" Width="500px"
                                BackColor= "Transparent" BorderColor="Transparent" BorderStyle="None" Text="Congés annuels"
                                BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="12px"
                                style="margin-top: 4px; margin-left: 0px; margin-bottom: 0px;
                                font-style: normal; text-indent: 1px; text-align:  center;" >
                       </asp:Label>      
                    </asp:TableCell>   
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left" Height="20px">
                       <asp:Label ID="LabelLigne2Gauche" runat="server" Height="20px" Width="250px"
                                BackColor= "Transparent" BorderColor="Transparent" BorderStyle="None" Text="Du 10/05/2010"
                                BorderWidth="1px" ForeColor= "#124545" Font-Italic="False"
                                Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="12px"
                                style="margin-top: 4px; margin-left: 0px; margin-bottom: 0px;
                                font-style: normal; text-indent: 1px; text-align:  right;" >
                       </asp:Label>      
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left" Height="20px">
                       <asp:Label ID="LabelLigne2Droite" runat="server" Height="20px" Width="250px"
                                BackColor= "Transparent" BorderColor="Transparent" BorderStyle="None" Text="au 18/05/2010"
                                BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="12px"
                                style="margin-top: 4px; margin-left: 0px; margin-bottom: 0px;
                                font-style: normal; text-indent: 4px; text-align:  left;" >
                       </asp:Label>      
                    </asp:TableCell>    
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left" Height="20px">
                       <asp:Label ID="LabelLigne3Gauche" runat="server" Height="20px" Width="250px"
                                BackColor= "Transparent" BorderColor="Transparent" BorderStyle="None" Text="Date de début incluse -"
                                BorderWidth="1px" ForeColor= "#124545" Font-Italic="False"
                                Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="12px"
                                style="margin-top: 4px; margin-left: 0px; margin-bottom: 0px;
                                font-style: normal; text-indent: 1px; text-align:  right;" >
                       </asp:Label>      
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left" Height="20px">
                       <asp:Label ID="LabelLigne3Droite" runat="server" Height="20px" Width="250px"
                                BackColor= "Transparent" BorderColor="Transparent" BorderStyle="None" Text="Date de fin incluse"
                                BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="12px"
                                style="margin-top: 4px; margin-left: 0px; margin-bottom: 0px;
                                font-style: normal; text-indent: 4px; text-align:  left;" >
                       </asp:Label>      
                    </asp:TableCell>    
                </asp:TableRow>
            </asp:Table>
      </asp:TableCell>
    </asp:TableRow>
      
    <asp:TableRow>
      <asp:TableCell HorizontalAlign="Left">
            <asp:Table ID="TableauObservations" runat="server" CellPadding="0" Width="250px" Height="50px"
            CellSpacing="0" HorizontalAlign="Left" BorderStyle="Notset" BorderWidth="1px" Visible="true" BorderColor="#7EC8BE"
            style="margin-top: -5px;margin-bottom: 0px;">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left" Height="30px">
                       <asp:Label ID="LabelObservations" runat="server" Height="25px" Width="250px"
                                BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" Text="Observations"
                                BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Font-Underline="true"
                                style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                font-style: normal; text-indent: 4px; text-align:  Left; padding-top: 2px;" >
                       </asp:Label>      
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left" Height="20px">
                       <asp:Label ID="LabelObs1" runat="server" Height="20px" Width="250px"
                                BackColor= "Transparent" BorderColor="Transparent" BorderStyle="None" Text="Vu avec Robert en avril"
                                BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="12px"
                                style="margin-top: 4px; margin-left: 0px; margin-bottom: 0px;
                                font-style: normal; text-indent: 1px; text-align:  center;" >
                       </asp:Label>      
                    </asp:TableCell> 
                </asp:TableRow>
            </asp:Table>
      </asp:TableCell>
      <asp:TableCell HorizontalAlign="Left">
            <asp:Table ID="TableJustification" runat="server" CellPadding="0" Width="249px" Height="50px"
            CellSpacing="0" HorizontalAlign="Left" BorderStyle="Notset" BorderWidth="1px" Visible="true" BorderColor="#7EC8BE"
            style="margin-top: -5px;margin-bottom: 0px; margin-left: -5px;">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left" Height="30px">
                       <asp:Label ID="LabelJustification" runat="server" Height="25px" Width="249px"
                                BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" Text="Justification"
                                BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Font-Underline="true"
                                style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                font-style: normal; text-indent: 4px; text-align:  Left; padding-top: 2px;" >
                       </asp:Label>      
                    </asp:TableCell>  
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left" Height="20px">
                       <asp:Label ID="LabelJustif1" runat="server" Height="20px" Width="249px"
                                BackColor= "Transparent" BorderColor="Transparent" BorderStyle="None" Text="D'accord"
                                BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="12px"
                                style="margin-top: 4px; margin-left: 0px; margin-bottom: 0px;
                                font-style: normal; text-indent: 1px; text-align:  center;" >
                       </asp:Label>      
                    </asp:TableCell>     
                </asp:TableRow>   
            </asp:Table>
      </asp:TableCell>
    </asp:TableRow>
  
 </asp:Table>
 
 <asp:Table ID="Table1" runat="server" BorderStyle="None" BorderWidth="2px" Visible="true" Height="190px"
    BorderColor="#B0E0D7" Width="500px" HorizontalAlign="Left" style="margin-top: 1px;">
    
    <asp:TableRow>
      <asp:TableCell HorizontalAlign="Left" ColumnSpan="2">
            <asp:Table ID="Table2" runat="server" CellPadding="0" Width="500px" Height="25px"
            CellSpacing="0" HorizontalAlign="Left" BorderStyle="Notset" BorderWidth="1px" Visible="true" BorderColor="#7EC8BE"
            style="margin-top: 1px;margin-bottom: 0px;">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                       <asp:Label ID="Label1" runat="server" Height="25px" Width="500px"
                                BackColor="#CAEBE4" BorderColor="Transparent" BorderStyle="None" Text="Congés annuels du 10/05/2010 au 18/05/2010"
                                BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Small"
                                style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                font-style: normal; text-indent: 1px; text-align:  center; padding-top: 2px;" >
                       </asp:Label>      
                    </asp:TableCell>   
                </asp:TableRow>
            </asp:Table>
      </asp:TableCell>
    </asp:TableRow>
    
    <asp:TableRow>
      <asp:TableCell HorizontalAlign="Left">
            <asp:Table ID="Table3" runat="server" CellPadding="0" Width="250px" Height="50px"
            CellSpacing="0" HorizontalAlign="Left" BorderStyle="Notset" BorderWidth="1px" Visible="true" BorderColor="#7EC8BE"
            style="margin-top: -5px;margin-bottom: 0px;">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left" Height="30px">
                       <asp:Label ID="Label2" runat="server" Height="25px" Width="250px"
                                BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" Text="Demande n°414"
                                BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Font-Underline="true"
                                style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                font-style: normal; text-indent: 4px; text-align:  Left; padding-top: 2px;" >
                       </asp:Label>      
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left" Height="20px">
                       <asp:Label ID="Label3" runat="server" Height="20px" Width="250px"
                                BackColor= "Transparent" BorderColor="Transparent" BorderStyle="None" Text="Faite le 12/04/2010 à 11:25"
                                BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="12px"
                                style="margin-top: 4px; margin-left: 0px; margin-bottom: 0px;
                                font-style: normal; text-indent: 1px; text-align:  center;" >
                       </asp:Label>      
                    </asp:TableCell>   
                </asp:TableRow>
            </asp:Table>
      </asp:TableCell>
      <asp:TableCell HorizontalAlign="Left">
            <asp:Table ID="Table4" runat="server" CellPadding="0" Width="249px" Height="50px"
            CellSpacing="0" HorizontalAlign="Left" BorderStyle="Notset" BorderWidth="1px" Visible="true"
            BorderColor="#731E1E" BackColor="#731E1E"
            style="margin-top: -5px; margin-bottom: 0px; margin-left: -5px;">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left" Height="30px">
                       <asp:Label ID="Label4" runat="server" Height="25px" Width="249px"
                                BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" Text="Décision"
                                BorderWidth="1px" ForeColor="#FCD9DE" Font-Italic="False"
                                Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Font-Underline="true"
                                style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                font-style: normal; text-indent: 4px; text-align:  Left; padding-top: 2px;" >
                       </asp:Label>      
                    </asp:TableCell>  
                </asp:TableRow>
                <asp:TableRow>   
                    <asp:TableCell HorizontalAlign="Left" Height="20px">
                       <asp:Label ID="Label5" runat="server" Height="20px" Width="249px"
                                BackColor= "Transparent" BorderColor="Transparent" BorderStyle="None" Text="Refusé le 20/04/2010 à 16:18"
                                BorderWidth="1px" ForeColor="#FCD9DE" Font-Italic="False"
                                Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="12px"
                                style="margin-top: 4px; margin-left: 0px; margin-bottom: 0px;
                                font-style: normal; text-indent: 1px; text-align:  center;" >
                       </asp:Label>      
                    </asp:TableCell>    
                </asp:TableRow>
            </asp:Table>
      </asp:TableCell>
    </asp:TableRow>  
        
    <asp:TableRow>
      <asp:TableCell HorizontalAlign="Left" ColumnSpan="2">
            <asp:Table ID="Table5" runat="server" CellPadding="0" Width="500px" Height="75px"
            CellSpacing="0" HorizontalAlign="Left" BorderStyle="Notset" BorderWidth="1px" Visible="true" BorderColor="#7EC8BE"
            style="margin-top: -5px;margin-bottom: 0px;">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left" Height="15px"  ColumnSpan="2">
                       <asp:Label ID="Label6" runat="server" Height="15px" Width="120px"
                                BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" Text="Contenu"
                                BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Font-Underline="true"
                                style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                font-style: normal; text-indent: 4px; text-align:  Left; padding-top: 2px;" >
                       </asp:Label>      
                    </asp:TableCell>   
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left" Height="20px" ColumnSpan="2">
                       <asp:Label ID="Label7" runat="server" Height="20px" Width="500px"
                                BackColor= "Transparent" BorderColor="Transparent" BorderStyle="None" Text="Congés annuels"
                                BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="12px"
                                style="margin-top: 4px; margin-left: 0px; margin-bottom: 0px;
                                font-style: normal; text-indent: 1px; text-align:  center;" >
                       </asp:Label>      
                    </asp:TableCell>   
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left" Height="20px">
                       <asp:Label ID="Label8" runat="server" Height="20px" Width="250px"
                                BackColor= "Transparent" BorderColor="Transparent" BorderStyle="None" Text="Du 10/05/2010"
                                BorderWidth="1px" ForeColor= "#124545" Font-Italic="False"
                                Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="12px"
                                style="margin-top: 4px; margin-left: 0px; margin-bottom: 0px;
                                font-style: normal; text-indent: 1px; text-align:  right;" >
                       </asp:Label>      
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left" Height="20px">
                       <asp:Label ID="Label9" runat="server" Height="20px" Width="250px"
                                BackColor= "Transparent" BorderColor="Transparent" BorderStyle="None" Text="au 18/05/2010"
                                BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="12px"
                                style="margin-top: 4px; margin-left: 0px; margin-bottom: 0px;
                                font-style: normal; text-indent: 4px; text-align:  left;" >
                       </asp:Label>      
                    </asp:TableCell>    
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left" Height="20px">
                       <asp:Label ID="Label10" runat="server" Height="20px" Width="250px"
                                BackColor= "Transparent" BorderColor="Transparent" BorderStyle="None" Text="Date de début incluse -"
                                BorderWidth="1px" ForeColor= "#124545" Font-Italic="False"
                                Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="12px"
                                style="margin-top: 4px; margin-left: 0px; margin-bottom: 0px;
                                font-style: normal; text-indent: 1px; text-align:  right;" >
                       </asp:Label>      
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left" Height="20px">
                       <asp:Label ID="Label11" runat="server" Height="20px" Width="250px"
                                BackColor= "Transparent" BorderColor="Transparent" BorderStyle="None" Text="Date de fin incluse"
                                BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="12px"
                                style="margin-top: 4px; margin-left: 0px; margin-bottom: 0px;
                                font-style: normal; text-indent: 4px; text-align:  left;" >
                       </asp:Label>      
                    </asp:TableCell>    
                </asp:TableRow>
            </asp:Table>
      </asp:TableCell>
    </asp:TableRow>
      
    <asp:TableRow>
      <asp:TableCell HorizontalAlign="Left">
            <asp:Table ID="Table6" runat="server" CellPadding="0" Width="250px" Height="50px"
            CellSpacing="0" HorizontalAlign="Left" BorderStyle="Notset" BorderWidth="1px" Visible="true" BorderColor="#7EC8BE"
            style="margin-top: -5px;margin-bottom: 0px;">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left" Height="30px">
                       <asp:Label ID="Label12" runat="server" Height="25px" Width="250px"
                                BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" Text="Observations"
                                BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Font-Underline="true"
                                style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                font-style: normal; text-indent: 4px; text-align:  Left; padding-top: 2px;" >
                       </asp:Label>      
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left" Height="20px">
                       <asp:Label ID="Label13" runat="server" Height="20px" Width="250px"
                                BackColor= "Transparent" BorderColor="Transparent" BorderStyle="None" Text="Vu avec Robert en avril"
                                BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="12px"
                                style="margin-top: 4px; margin-left: 0px; margin-bottom: 0px;
                                font-style: normal; text-indent: 1px; text-align:  center;" >
                       </asp:Label>      
                    </asp:TableCell> 
                </asp:TableRow>
            </asp:Table>
      </asp:TableCell>
      <asp:TableCell HorizontalAlign="Left">
            <asp:Table ID="Table7" runat="server" CellPadding="0" Width="249px" Height="50px"
            CellSpacing="0" HorizontalAlign="Left" BorderStyle="Notset" BorderWidth="1px" Visible="true" BorderColor="#7EC8BE"
            style="margin-top: -5px;margin-bottom: 0px; margin-left: -5px;">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left" Height="30px">
                       <asp:Label ID="Label14" runat="server" Height="25px" Width="249px"
                                BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" Text="Justification"
                                BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Font-Underline="true"
                                style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                font-style: normal; text-indent: 4px; text-align:  Left; padding-top: 2px;" >
                       </asp:Label>      
                    </asp:TableCell>  
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left" Height="20px">
                       <asp:Label ID="Label15" runat="server" Height="20px" Width="249px"
                                BackColor= "Transparent" BorderColor="Transparent" BorderStyle="None" Text="Non - impossible cette semaine"
                                BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="12px"
                                style="margin-top: 4px; margin-left: 0px; margin-bottom: 0px;
                                font-style: normal; text-indent: 1px; text-align:  center;" >
                       </asp:Label>      
                    </asp:TableCell>     
                </asp:TableRow>   
            </asp:Table>
      </asp:TableCell>
    </asp:TableRow>
  
 </asp:Table>