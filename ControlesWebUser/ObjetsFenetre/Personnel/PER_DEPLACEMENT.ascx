﻿<%@ Control Language="VB" AutoEventWireup="false" Inherits="Virtualia.Net.Fenetre_PER_DEPLACEMENT" CodeBehind="PER_DEPLACEMENT.ascx.vb" %>

<%@ Register Src="~/Controles/Saisies/VCoupleEtiDonnee.ascx" TagName="VCoupleEtiDonnee" TagPrefix="Virtualia" %>
<%@ Register Src="~/Controles/Saisies/VDuoEtiquetteCommande.ascx" TagName="VDuoEtiquetteCommande" TagPrefix="Virtualia" %>
<%@ Register Src="~/Controles/Saisies/VCoupleVerticalEtiDonnee.ascx" TagName="VCoupleVerticalEtiDonnee" TagPrefix="Virtualia" %>
<%@ Register Src="~/Controles/Saisies/VCocheSimple.ascx" TagName="VCocheSimple" TagPrefix="Virtualia" %>

<%@ Register Src="~/ControlsGeneriques/V_DataGrid.ascx" TagName="VDataGrid" TagPrefix="Generic" %>
<%@ Register Src="~/ControlsGeneriques/V_CommandeCRUD.ascx" TagName="VCrud" TagPrefix="Generic" %>

<asp:Table ID="CadreControle" runat="server" Height="30px" Width="650px" CellPadding="0" CellSpacing="0" HorizontalAlign="Center">
    <asp:TableRow>
        <asp:TableCell>
            <Generic:VDataGrid ID="ListeGrille" runat="server" CadreWidth="630px" SiColonneSelect="true" SiCaseAcocher="false" />
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="CadreInfo" runat="server" BorderStyle="Ridge" BorderWidth="2px" BorderColor="#B0E0D7" Height="190px" Width="750px" HorizontalAlign="Center" Style="margin-top: 3px;">
                <asp:TableRow>
                    <asp:TableCell>
                        <Generic:VCrud ID="CtlCrud" runat="server" Height="22px" Width="244px" HorizontalAlign="Right" CadreStyle="margin-top: 3px; margin-right: 3px" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Table ID="CadreTitre" runat="server" Height="40px" CellPadding="0" Width="500px" CellSpacing="0" HorizontalAlign="Center">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                    <asp:Label ID="Etiquette" runat="server" Text="Frais de déplacements" Height="20px" Width="300px" BackColor="#2FA49B" BorderColor="#B0E0D7" BorderStyle="Groove" BorderWidth="2px" ForeColor="#D7FAF3" Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Small" Style="margin-top: 5px; margin-left: 4px; margin-bottom: 12px; font-style: oblique; text-indent: 5px; text-align: center;" />
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Table ID="CadreEnteteMission" runat="server" CellPadding="0" CellSpacing="0" Width="750px" HorizontalAlign="Left">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH32" runat="server" V_PointdeVue="1" V_Objet="47" V_Information="32" V_SiDonneeDico="true" EtiText="Mission n°" EtiWidth="80px" DonWidth="120px" DonTabIndex="1" V_SiEnLectureSeule="true" />
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Left">
                                    <asp:Label ID="LabelMission" runat="server" Height="20px" Width="512px" Text="" BackColor="#B0E0D7" BorderColor="#B0E0D7" BorderStyle="Outset" BorderWidth="2px" ForeColor="#142425" Font-Italic="true" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Style="margin-top: 0px; margin-left: 5px; text-indent: 2px; text-align: center" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <asp:Label ID="LabelDateDebut" runat="server" Height="20px" Width="100px" Text="Du ../../...." BackColor="#B0E0D7" BorderColor="#B0E0D7" BorderStyle="Outset" BorderWidth="2px" ForeColor="#142425" Font-Italic="true" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Style="margin-top: 0px; margin-left: 5px; text-indent: 2px; text-align: center" />
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Left">
                                    <asp:Label ID="LabelDateFin" runat="server" Height="20px" Width="100px" Text="Au ../../...." BackColor="#B0E0D7" BorderColor="#B0E0D7" BorderStyle="Outset" BorderWidth="2px" ForeColor="#142425" Font-Italic="true" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Style="margin-top: 0px; margin-left: 5px; text-indent: 2px; text-align: center" />
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Table ID="CadreEnteteDeplacement" runat="server" CellPadding="0" CellSpacing="0" Width="750px" HorizontalAlign="Left">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH01" runat="server" V_PointdeVue="1" V_Objet="47" V_Information="1" V_SiDonneeDico="true" EtiText="Objet" EtiWidth="50px" DonWidth="300px" DonTabIndex="2" V_SiEnLectureSeule="true" />
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Left">
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH29" runat="server" V_PointdeVue="1" V_Objet="47" V_Information="29" V_SiDonneeDico="true" EtiText="Lieu" EtiWidth="50px" DonWidth="300px" DonTabIndex="3" V_SiEnLectureSeule="true" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left" ColumnSpan="2">
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH02" runat="server" V_PointdeVue="1" V_Objet="47" V_Information="2" V_SiDonneeDico="true" EtiText="Moyen" EtiWidth="50px" DonWidth="300px" DonTabIndex="4" V_SiEnLectureSeule="true" />
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="PanelTitreIndemnites" runat="server" BorderStyle="none" BorderWidth="2px" BorderColor="#B0E0D7" Height="60px" Width="750px" HorizontalAlign="Center" Style="margin-top: 3px;">
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Table ID="TableauPanelIndemnites" runat="server" Height="2px" CellPadding="0" Width="750px" CellSpacing="0" HorizontalAlign="Center" BorderStyle="none" BorderWidth="1px" Visible="true" BorderColor="#B0E0D7">
                            <asp:TableRow>
                                <asp:TableCell Height="20px" />
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                    <asp:Label ID="LabelOngletIndemnites" runat="server" Height="20px" Width="320px" BackColor="#216B68" BorderColor="#B0E0D7" BorderStyle="Outset" Text="INDEMNITES" BorderWidth="2px" ForeColor="#D7FAF3" Font-Italic="False" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Style="margin-top: 0px; margin-left: 5px; margin-bottom: 0px; font-style: oblique; text-indent: 2px; text-align: center;" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Height="20px" />
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="PanelIndemnites" runat="server" BorderStyle="None" BorderWidth="2px" BorderColor="#B0E0D7" Height="400px" Width="750px" HorizontalAlign="Left" Style="margin-top: 3px;">
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Table ID="TableauDepartResidence" runat="server" Height="60px" CellPadding="0" Width="750px" CellSpacing="0" HorizontalAlign="Center" BorderStyle="NotSet" BorderWidth="2px" Visible="true" BorderColor="#B0E0D7">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left" RowSpan="2" BorderStyle="NotSet" BorderWidth="2px" Visible="true" BorderColor="#B0E0D7" Height="35px" Width="170px">
                                    <asp:Label ID="LabelDepartResidence" runat="server" Text="Départ de la résidence" Height="20px" Width="160px" BackColor="Transparent" BorderStyle="none" BorderWidth="0px" ForeColor="#2FA49B" Font-Bold="true" Font-Names="Trebuchet MS" Font-Size="Small" Style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; font-style: oblique; text-indent: 0px; text-align: center;" />
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Left" BorderStyle="none" BorderWidth="2px" Visible="true" BorderColor="#B0E0D7" Height="16px" Width="150px">
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH00" runat="server" V_PointdeVue="1" V_Objet="47" V_Information="0" V_SiDonneeDico="true" EtiText="Date" EtiWidth="50px" DonWidth="80px" DonTabIndex="5" />
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Left" BorderStyle="none" BorderWidth="2px" Visible="true" BorderColor="#B0E0D7" Height="16px" Width="150px">
                                    <asp:Label ID="LabelArriveeMission1" runat="server" Text="Arrivée lieu de mission" Height="20px" Width="150px" BackColor="Transparent" BorderStyle="none" BorderWidth="0px" ForeColor="#2FA49B" Font-Bold="true" Font-Names="Trebuchet MS" Font-Size="Small" Style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; font-style: oblique; text-indent: 0px; text-align: center;" />
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Left" BorderStyle="none" BorderWidth="2px" Visible="true" BorderColor="#B0E0D7" Height="16px" Width="150px">
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH30" runat="server" V_PointdeVue="1" V_Objet="47" V_Information="30" V_SiDonneeDico="true" EtiText="Date" EtiWidth="50px" DonWidth="80px" DonTabIndex="6" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left" BorderStyle="none" BorderWidth="2px" Visible="true" BorderColor="#B0E0D7" Height="20px" Width="150px">
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH03" runat="server" V_PointdeVue="1" V_Objet="47" V_Information="3" V_SiDonneeDico="true" EtiText="Heure" EtiWidth="50px" DonWidth="50px" DonTabIndex="7" />
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Left" BorderStyle="none" BorderWidth="2px" Visible="true" BorderColor="#B0E0D7" Height="20px" Width="150px">
                                    <asp:Label ID="LabelArriveeMission2" runat="server" Text="" Height="20px" Width="150px" BackColor="Transparent" BorderStyle="none" BorderWidth="0px" ForeColor="#2FA49B" Font-Bold="true" Font-Names="Trebuchet MS" Font-Size="Small" Style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; font-style: oblique; text-indent: 0px; text-align: center;" />
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Left" BorderStyle="none" BorderWidth="2px" Visible="true" BorderColor="#B0E0D7" Height="20px" Width="150px">
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH04" runat="server" V_PointdeVue="1" V_Objet="47" V_Information="4" V_SiDonneeDico="true" EtiText="Heure" EtiWidth="50px" DonWidth="50px" DonTabIndex="8" />
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Table ID="Separateur1" runat="server" Height="2px" CellPadding="0" Width="750px" CellSpacing="0" HorizontalAlign="Center" BorderStyle="none" BorderWidth="1px" Visible="true" BorderColor="#B0E0D7" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Table ID="TableauRetourResidence" runat="server" Height="60px" CellPadding="0" Width="750px" CellSpacing="0" HorizontalAlign="Center" BorderStyle="NotSet" BorderWidth="2px" Visible="true" BorderColor="#B0E0D7">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left" RowSpan="2" BorderStyle="NotSet" BorderWidth="2px" Visible="true" BorderColor="#B0E0D7" Height="35px" Width="170px">
                                    <asp:Label ID="LabelRetourResidence" runat="server" Text="Retour à la résidence" Height="20px" Width="160px" BackColor="Transparent" BorderStyle="none" BorderWidth="0px" ForeColor="#2FA49B" Font-Bold="true" Font-Names="Trebuchet MS" Font-Size="Small" Style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; font-style: oblique; text-indent: 0px; text-align: center;" />
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Left" BorderStyle="none" BorderWidth="2px" Visible="true" BorderColor="#B0E0D7" Height="16px" Width="150px">
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH31" runat="server" V_PointdeVue="1" V_Objet="47" V_Information="31" V_SiDonneeDico="true" EtiText="Date" EtiWidth="50px" DonWidth="80px" DonTabIndex="9" />
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Left" BorderStyle="none" BorderWidth="2px" Visible="true" BorderColor="#B0E0D7" Height="16px" Width="150px">
                                    <asp:Label ID="LabelDepartMission1" runat="server" Text="Départ lieu de mission" Height="20px" Width="150px" BackColor="Transparent" BorderStyle="none" BorderWidth="0px" ForeColor="#2FA49B" Font-Bold="true" Font-Names="Trebuchet MS" Font-Size="Small" Style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; font-style: oblique; text-indent: 0px; text-align: center;" />
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Left" BorderStyle="none" BorderWidth="2px" Visible="true" BorderColor="#B0E0D7" Height="16px" Width="150px">
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH05" runat="server" V_PointdeVue="1" V_Objet="47" V_Information="5" V_SiDonneeDico="true" EtiText="Date" EtiWidth="50px" DonWidth="80px" DonTabIndex="10" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left" BorderStyle="none" BorderWidth="2px" Visible="true" BorderColor="#B0E0D7" Height="20px" Width="150px">
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH06" runat="server" V_PointdeVue="1" V_Objet="47" V_Information="6" V_SiDonneeDico="true" EtiText="Heure" EtiWidth="50px" DonWidth="50px" DonTabIndex="11" />
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Left" BorderStyle="none" BorderWidth="2px" Visible="true" BorderColor="#B0E0D7" Height="20px" Width="150px">
                                    <asp:Label ID="LabelDepartMission2" runat="server" Text="" Height="20px" Width="150px" BackColor="Transparent" BorderStyle="none" BorderWidth="0px" ForeColor="#2FA49B" Font-Bold="true" Font-Names="Trebuchet MS" Font-Size="Small" Style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; font-style: oblique; text-indent: 0px; text-align: center;" />
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Left" BorderStyle="none" BorderWidth="2px" Visible="true" BorderColor="#B0E0D7" Height="20px" Width="150px">
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH07" runat="server" V_PointdeVue="1" V_Objet="47" V_Information="7" V_SiDonneeDico="true" EtiText="Heure" EtiWidth="50px" DonWidth="50px" DonTabIndex="12" />
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Table ID="Separateur2" runat="server" Height="2px" CellPadding="0" Width="750px" CellSpacing="0" HorizontalAlign="Center" BorderStyle="none" BorderWidth="1px" Visible="true" BorderColor="#B0E0D7" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Table ID="TableauHebergement" runat="server" Height="60px" CellPadding="0" Width="750px" CellSpacing="0" HorizontalAlign="Center" BorderStyle="NotSet" BorderWidth="2px" Visible="true" BorderColor="#B0E0D7">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left" RowSpan="2" BorderStyle="NotSet" BorderWidth="2px" Visible="true" BorderColor="#B0E0D7" Height="35px" Width="170px">
                                    <asp:Label ID="LabelHebergement" runat="server" Text="Hébergement et repas" Height="20px" Width="160px" BackColor="Transparent" BorderStyle="none" BorderWidth="0px" ForeColor="#2FA49B" Font-Bold="true" Font-Names="Trebuchet MS" Font-Size="Small" Style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; font-style: oblique; text-indent: 0px; text-align: center;" />
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Left" BorderStyle="none" BorderWidth="2px" Visible="true" BorderColor="#B0E0D7" Height="16px" Width="230px">
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH20" runat="server" V_PointdeVue="1" V_Objet="47" V_Information="20" V_SiDonneeDico="true" EtiText="Nbr. repas gratuits" EtiWidth="180px" DonWidth="40px" DonTabIndex="13" />
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Left" BorderStyle="none" BorderWidth="2px" Visible="true" BorderColor="#B0E0D7" Height="16px" Width="208px">
                                    <Virtualia:VCocheSimple ID="Coche33" runat="server" V_PointdeVue="1" V_Objet="47" V_Information="33" V_SiDonneeDico="true" V_Text="Aller et retour quotidien" V_Width="208px" V_Style="margin-left:0px; margin-top: 25px;" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left" BorderStyle="none" BorderWidth="2px" Visible="true" BorderColor="#B0E0D7" Height="20px" Width="230px" ColumnSpan="2">
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH21" runat="server" V_PointdeVue="1" V_Objet="47" V_Information="21" V_SiDonneeDico="true" EtiText="Hébergement à titre gratuit" EtiWidth="180px" DonWidth="40px" DonTabIndex="14" EtiStyle="margin-left:4px; margin-top: -15px;" DonStyle="margin-left:0px; margin-top: -15px;" />
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Table ID="Separateur3" runat="server" Height="2px" CellPadding="0" Width="750px" CellSpacing="0" HorizontalAlign="Center" BorderStyle="none" BorderWidth="1px" Visible="true" BorderColor="#B0E0D7" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Table ID="TableauIndemnites" runat="server" Height="210px" CellPadding="0" Width="750px" CellSpacing="0" HorizontalAlign="Center" BorderStyle="NotSet" BorderWidth="2px" Visible="true" BorderColor="#B0E0D7">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left" RowSpan="7" BorderStyle="NotSet" BorderWidth="2px" Visible="true" BorderColor="#B0E0D7" Height="35px" Width="170px">
                                    <asp:Label ID="LabelIndemnites" runat="server" Text="Indemnités" Height="20px" Width="160px" BackColor="Transparent" BorderStyle="none" BorderWidth="0px" ForeColor="#2FA49B" Font-Bold="true" Font-Names="Trebuchet MS" Font-Size="Small" Style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; font-style: oblique; text-indent: 0px; text-align: center;" />
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Left" BorderStyle="NotSet" BorderWidth="2px" Visible="true" BorderColor="#B0E0D7" Height="16px" Width="235px">
                                    <asp:Label ID="LabelTitreIndemnite" runat="server" Text="" Height="20px" Width="150px" BackColor="Transparent" BorderStyle="none" BorderWidth="0px" ForeColor="#2FA49B" Font-Bold="true" Font-Names="Trebuchet MS" Font-Size="Small" Style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; font-style: oblique; text-indent: 0px; text-align: center;" />
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Left" BorderStyle="NotSet" BorderWidth="2px" Visible="true" BorderColor="#B0E0D7" Height="16px" Width="60px">
                                    <asp:Label ID="LabelTitreNombreIndemnites" runat="server" Text="Nombre" Height="20px" Width="60px" BackColor="Transparent" BorderStyle="None" BorderWidth="0px" ForeColor="#2FA49B" Font-Bold="true" Font-Names="Trebuchet MS" Font-Size="Small" Style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; font-style: oblique; text-indent: 10px; text-align: center;" />
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Left" BorderStyle="NotSet" BorderWidth="2px" Visible="true" BorderColor="#B0E0D7" Height="16px" Width="60px">
                                    <asp:Label ID="LabelTitreTauxIndemnites" runat="server" Text="Taux" Height="20px" Width="60px" BackColor="Transparent" BorderStyle="None" BorderWidth="0px" ForeColor="#2FA49B" Font-Bold="true" Font-Names="Trebuchet MS" Font-Size="Small" Style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; font-style: oblique; text-indent: 10px; text-align: center;" />
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Left" BorderStyle="NotSet" BorderWidth="2px" Visible="true" BorderColor="#B0E0D7" Height="16px" Width="90px">
                                    <asp:Label ID="LabelTitreMontantIndemnites" runat="server" Text="Montant (€)" Height="20px" Width="90px" BackColor="Transparent" BorderStyle="None" BorderWidth="0px" ForeColor="#2FA49B" Font-Bold="true" Font-Names="Trebuchet MS" Font-Size="Small" Style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; font-style: oblique; text-indent: 10px; text-align: center;" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left" BorderStyle="NotSet" BorderWidth="2px" Visible="true" BorderColor="#B0E0D7" Height="16px" Width="235px">
                                    <asp:Label ID="LabelRepas" runat="server" Text="Repas" Height="20px" Width="150px" BackColor="Transparent" BorderStyle="none" BorderWidth="0px" ForeColor="#2FA49B" Font-Bold="true" Font-Names="Trebuchet MS" Font-Size="Small" Style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; font-style: oblique; text-indent: 10px; text-align: left;" />
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Left" BorderStyle="NotSet" BorderWidth="2px" Visible="true" BorderColor="#B0E0D7" Height="20px" Width="60px">
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH17" runat="server" V_PointdeVue="1" V_Objet="47" V_Information="17" V_SiDonneeDico="true" Etivisible="false" DonWidth="40px" DonTabIndex="15" DonStyle="margin-left:10px;" />
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Left" BorderStyle="NotSet" BorderWidth="2px" Visible="true" BorderColor="#B0E0D7" Height="16px" Width="60px">
                                    <asp:Label ID="LabelTauxRepas" runat="server" Text="" Height="20px" Width="60px" BackColor="Transparent" BorderStyle="None" BorderWidth="0px" ForeColor="#2FA49B" Font-Bold="true" Font-Names="Trebuchet MS" Font-Size="Small" Style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; font-style: oblique; text-indent: 0px; text-align: center;" />
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Left" BorderStyle="NotSet" BorderWidth="2px" Visible="true" BorderColor="#B0E0D7" Height="16px" Width="90px">
                                    <asp:Label ID="LabelMontantRepas" runat="server" Text="" Height="20px" Width="90px" ackColor="Transparent" BorderStyle="None" BorderWidth="0px" ForeColor="#2FA49B" Font-Bold="true" Font-Names="Trebuchet MS" Font-Size="Small" Style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; font-style: oblique; text-indent: 0px; text-align: center;" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left" BorderStyle="NotSet" BorderWidth="2px" Visible="true" BorderColor="#B0E0D7" Height="16px" Width="235px">
                                    <asp:Label ID="LabelRestauAdm" runat="server" Text="Restaurant administratif" Height="20px" Width="200px" BackColor="Transparent" BorderStyle="none" BorderWidth="0px" ForeColor="#2FA49B" Font-Bold="true" Font-Names="Trebuchet MS" Font-Size="Small" Style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; font-style: oblique; text-indent: 10px; text-align: left;" />
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Left" BorderStyle="NotSet" BorderWidth="2px" Visible="true" BorderColor="#B0E0D7" Height="20px" Width="60px">
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH16" runat="server" V_PointdeVue="1" V_Objet="47" V_Information="16" V_SiDonneeDico="true" Etivisible="false" DonWidth="40px" DonTabIndex="16" DonStyle="margin-left:10px;" />
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Left" BorderStyle="NotSet" BorderWidth="2px" Visible="true" BorderColor="#B0E0D7" Height="16px" Width="60px">
                                    <asp:Label ID="LabelTauxRestauAdm" runat="server" Text="" Height="20px" Width="60px" BackColor="Transparent" BorderStyle="None" BorderWidth="0px" ForeColor="#2FA49B" Font-Bold="true" Font-Names="Trebuchet MS" Font-Size="Small" Style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; font-style: oblique; text-indent: 0px; text-align: center;" />
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Left" BorderStyle="NotSet" BorderWidth="2px" Visible="true" BorderColor="#B0E0D7" Height="16px" Width="90px">
                                    <asp:Label ID="LabelMontantRestauAdm" runat="server" Text="" Height="20px" Width="90px" BackColor="Transparent" BorderStyle="None" BorderWidth="0px" ForeColor="#2FA49B" Font-Bold="true" Font-Names="Trebuchet MS" Font-Size="Small" Style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; font-style: oblique; text-indent: 0px; text-align: center;" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left" BorderStyle="NotSet" BorderWidth="2px" Visible="true" BorderColor="#B0E0D7" Height="16px" Width="235px">
                                    <asp:Label ID="LabelNuitees" runat="server" Text="Nuitées" Height="20px" Width="150px" BackColor="Transparent" BorderStyle="none" BorderWidth="0px" ForeColor="#2FA49B" Font-Bold="true" Font-Names="Trebuchet MS" Font-Size="Small" Style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; font-style: oblique; text-indent: 10px; text-align: left;" />
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Left" BorderStyle="NotSet" BorderWidth="2px" Visible="true" BorderColor="#B0E0D7" Height="20px" Width="60px">
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH18" runat="server" V_PointdeVue="1" V_Objet="47" V_Information="18" V_SiDonneeDico="true" Etivisible="false" DonWidth="40px" DonTabIndex="17" DonStyle="margin-left:10px;" />
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Left" BorderStyle="NotSet" BorderWidth="2px" Visible="true" BorderColor="#B0E0D7" Height="16px" Width="60px">
                                    <asp:Label ID="LabelTauxNuitees" runat="server" Text="" Height="20px" Width="60px" BackColor="Transparent" BorderStyle="None" BorderWidth="0px" ForeColor="#2FA49B" Font-Bold="true" Font-Names="Trebuchet MS" Font-Size="Small" Style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; font-style: oblique; text-indent: 0px; text-align: center;" />
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Left" BorderStyle="NotSet" BorderWidth="2px" Visible="true" BorderColor="#B0E0D7" Height="16px" Width="90px">
                                    <asp:Label ID="LabelMontantNuitees" runat="server" Text="" Height="20px" Width="90px" BackColor="Transparent" BorderStyle="None" BorderWidth="0px" ForeColor="#2FA49B" Font-Bold="true" Font-Names="Trebuchet MS" Font-Size="Small" Style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; font-style: oblique; text-indent: 0px; text-align: center;" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left" BorderStyle="NotSet" BorderWidth="2px" Visible="true" BorderColor="#B0E0D7" Height="16px" Width="235px">
                                    <asp:Label ID="LabelJournees" runat="server" Text="Journées" Height="20px" Width="150px" BackColor="Transparent" BorderStyle="none" BorderWidth="0px" ForeColor="#2FA49B" Font-Bold="true" Font-Names="Trebuchet MS" Font-Size="Small" Style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; font-style: oblique; text-indent: 10px; text-align: left;" />
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Left" BorderStyle="NotSet" BorderWidth="2px" Visible="true" BorderColor="#B0E0D7" Height="20px" Width="60px">
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH19" runat="server" V_PointdeVue="1" V_Objet="47" V_Information="19" V_SiDonneeDico="true" Etivisible="false" DonWidth="40px" DonTabIndex="18" DonStyle="margin-left:10px;" />
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Left" BorderStyle="NotSet" BorderWidth="2px" Visible="true" BorderColor="#B0E0D7" Height="16px" Width="60px">
                                    <asp:Label ID="LabelTauxJournees" runat="server" Text="" Height="20px" Width="60px" BackColor="Transparent" BorderStyle="None" BorderWidth="0px" ForeColor="#2FA49B" Font-Bold="true" Font-Names="Trebuchet MS" Font-Size="Small" Style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; font-style: oblique; text-indent: 0px; text-align: center;" />
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Left" BorderStyle="NotSet" BorderWidth="2px" Visible="true" BorderColor="#B0E0D7" Height="16px" Width="90px">
                                    <asp:Label ID="LabelMontantJournees" runat="server" Text="" Height="20px" Width="90px" BackColor="Transparent" BorderStyle="None" BorderWidth="0px" ForeColor="#2FA49B" Font-Bold="true" Font-Names="Trebuchet MS" Font-Size="Small" Style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; font-style: oblique; text-indent: 0px; text-align: center;" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left" BorderStyle="NotSet" BorderWidth="2px" Visible="true" BorderColor="#B0E0D7" Height="16px" Width="235px">
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH15" runat="server" V_PointdeVue="1" V_Objet="47" V_Information="15" V_SiDonneeDico="true" EtiWidth="70px" DonWidth="25px" DonTabIndex="19" EtiStyle="margin-left:10px;" />
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Left" BorderStyle="NotSet" BorderWidth="2px" Visible="true" BorderColor="#B0E0D7" Height="20px" Width="60px">
                                    <asp:Label ID="LabelTotalNombre" runat="server" Text="" Height="20px" Width="60px" BackColor="Transparent" BorderStyle="None" BorderWidth="0px" ForeColor="#2FA49B" Font-Bold="true" Font-Names="Trebuchet MS" Font-Size="Small" Style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; font-style: oblique; text-indent: 8px; text-align: center;" />
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Left" BorderStyle="NotSet" BorderWidth="2px" Visible="true" BorderColor="#B0E0D7" Height="16px" Width="60px">
                                    <asp:Label ID="LabelTotalTaux" runat="server" Text="Total" Height="20px" Width="60px" BackColor="Transparent" BorderStyle="None" BorderWidth="0px" ForeColor="#2FA49B" Font-Bold="true" Font-Names="Trebuchet MS" Font-Size="Small" Style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; font-style: oblique; text-indent: 10px; text-align: center;" />
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Left" BorderStyle="NotSet" BorderWidth="2px" Visible="true" BorderColor="#B0E0D7" Height="16px" Width="90px">
                                    <asp:Label ID="LabelTotalMontant" runat="server" Text="" Height="20px" Width="90px" BackColor="Transparent" BorderStyle="None" BorderWidth="0px" ForeColor="#2FA49B" Font-Bold="true" Font-Names="Trebuchet MS" Font-Size="Small" Style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; font-style: oblique; text-indent: 0px; text-align: center;" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left" BorderStyle="NotSet" BorderWidth="2px" Visible="true" BorderColor="#B0E0D7" Height="16px" Width="235px">
                                    <asp:Label ID="LabelTauxChancellerie" runat="server" Text="Taux de change Chancellerie" Height="20px" Width="200px" BackColor="Transparent" BorderStyle="none" BorderWidth="0px" ForeColor="#2FA49B" Font-Bold="true" Font-Names="Trebuchet MS" Font-Size="Small" Style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; font-style: oblique; text-indent: 10px; text-align: left;" />
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Left" BorderStyle="NotSet" BorderWidth="2px" Visible="true" BorderColor="#B0E0D7" Height="20px" Width="60px">
                                    <asp:Label ID="LabelNombreChancellerie" runat="server" Text="" Height="20px" Width="60px" BackColor="Transparent" BorderStyle="None" BorderWidth="0px" ForeColor="#2FA49B" Font-Bold="true" Font-Names="Trebuchet MS" Font-Size="Small" Style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; font-style: oblique; text-indent: 8px; text-align: center;" />
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Left" BorderStyle="NotSet" BorderWidth="2px" Visible="true" BorderColor="#B0E0D7" Height="16px" Width="60px">
                                    <asp:Label ID="LabelTauxChangeChancellerie" runat="server" Text="" Height="20px" Width="60px" BackColor="Transparent" BorderStyle="None" BorderWidth="0px" ForeColor="#2FA49B" Font-Bold="true" Font-Names="Trebuchet MS" Font-Size="Small" Style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; font-style: oblique; text-indent: 10px; text-align: center;" />
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Left" BorderStyle="NotSet" BorderWidth="2px" Visible="true" BorderColor="#B0E0D7" Height="16px" Width="90px">
                                    <asp:Label ID="LabelMontantChancellerie" runat="server" Text="" Height="20px" Width="90px" BackColor="Transparent" BorderStyle="None" BorderWidth="0px" ForeColor="#2FA49B" Font-Bold="true" Font-Names="Trebuchet MS" Font-Size="Small" Style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; font-style: oblique; text-indent: 0px; text-align: center;" />
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Table ID="Separateur4" runat="server" Height="30px" CellPadding="0" Width="750px" CellSpacing="0" HorizontalAlign="Center" BorderStyle="none" BorderWidth="1px" Visible="true" BorderColor="#B0E0D7" />
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="PanelFraisTransport" runat="server" BorderStyle="none" BorderWidth="2px" BorderColor="#B0E0D7" Height="60px" Width="750px" HorizontalAlign="Center" Style="margin-top: 3px;">
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Table ID="TableauFraisTransport" runat="server" Height="30px" CellPadding="0" Width="750px" CellSpacing="0" HorizontalAlign="Center" BorderStyle="none" BorderWidth="1px" Visible="true" BorderColor="#B0E0D7">
                            <asp:TableRow>
                                <asp:TableCell Height="20px" />
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                    <asp:Label ID="LabelTitreFraisTransport" runat="server" Height="20px" Width="320px" BackColor="#216B68" BorderColor="#B0E0D7" BorderStyle="Outset" Text="FRAIS DE TRANSPORT" BorderWidth="2px" ForeColor="#D7FAF3" Font-Italic="False" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Style="margin-top: 0px; margin-left: 5px; margin-bottom: 0px; font-style: oblique; text-indent: 2px; text-align: center;" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Height="20px" />
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="PanelTransports" runat="server" BorderStyle="None" BorderWidth="2px" BorderColor="#B0E0D7" Height="400px" Width="750px" HorizontalAlign="Left" Style="margin-top: 3px;">
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Table ID="TableauTitreTransport" runat="server" Height="30px" CellPadding="0" Width="750px" CellSpacing="0" HorizontalAlign="Center" BorderStyle="NotSet" BorderWidth="2px" Visible="true" BorderColor="#B0E0D7">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center" BorderStyle="NotSet" BorderWidth="2px" Visible="true" BorderColor="#B0E0D7" Height="30px" Width="300px">
                                    <asp:Label ID="LabelTitreModeTransport" runat="server" Text="Mode de transport utilisé" Height="20px" Width="160px" BackColor="Transparent" BorderStyle="none" BorderWidth="0px" ForeColor="#2FA49B" Font-Bold="true" Font-Names="Trebuchet MS" Font-Size="Small" Style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; font-style: oblique; text-indent: 0px; text-align: center;" />
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Center" BorderStyle="NotSet" BorderWidth="2px" Visible="true" BorderColor="#B0E0D7" Height="30px" Width="190px" />
                                <asp:TableCell HorizontalAlign="Center" BorderStyle="NotSet" BorderWidth="2px" Visible="true" BorderColor="#B0E0D7" Height="30px" Width="150px">
                                    <asp:Label ID="LabelTitreMontant" runat="server" Text="Montant (€)" Height="20px" Width="150px" BackColor="Transparent" BorderStyle="none" BorderWidth="0px" ForeColor="#2FA49B" Font-Bold="true" Font-Names="Trebuchet MS" Font-Size="Small" Style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; font-style: oblique; text-indent: 0px; text-align: center;" />
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Table ID="TableauModeTransport" runat="server" Height="350px" CellPadding="0" Width="750px" CellSpacing="0" HorizontalAlign="Center" BorderStyle="None" BorderWidth="2px" Visible="true" BorderColor="#B0E0D7">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left" BorderStyle="NotSet" BorderWidth="1px" Visible="true" BorderColor="#B0E0D7" Height="30px" Width="260px">
                                    <asp:CheckBox ID="CocheAvion" runat="server" Text="Avion" Visible="true" AutoPostBack="true" BackColor="Transparent" BorderColor="White" BorderStyle="none" BorderWidth="2px" ForeColor="#142425" Height="20px" Width="250px" Checked="false" Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Small" Style="margin-top: 0px; margin-left: 0px; font-style: oblique; text-indent: 5px; text-align: left" />
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Center" BorderStyle="NotSet" BorderWidth="1px" Visible="true" BorderColor="#B0E0D7" Height="30px" Width="40px">
                                    <asp:Button ID="BoutonAvion" runat="server" Height="22px" Width="35px" Text="..." BackColor="Transparent" BorderColor="#B0E0D7" BorderStyle="Inset" BorderWidth="2px" oreColor="#142425" Visible="true" Font-Bold="true" Font-Names="Trebuchet MS" Font-Size="Small" Font-Italic="false" Style="padding-left: 2px; padding-bottom: 2px; margin-top: 0px; text-align: center; text-indent: 3px;" />
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Center" BorderStyle="NotSet" BorderWidth="1px" Visible="true" BorderColor="#B0E0D7" Height="30px" Width="190px">
                                    <asp:CheckBox ID="CocheAvionPrisEnCharge" runat="server" Text="Pris en charge" Visible="true" AutoPostBack="true" BackColor="Transparent" BorderColor="White" BorderStyle="none" BorderWidth="2px" ForeColor="#142425" Height="20px" Width="180px" Checked="false" Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Small" Style="margin-top: 0px; margin-left: 5px; font-style: oblique; text-indent: 10px; text-align: center" />
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Center" BorderStyle="NotSet" BorderWidth="1px" Visible="true" BorderColor="#B0E0D7" Height="30px" Width="150px">
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH08" runat="server" V_PointdeVue="1" V_Objet="47" V_Information="8" V_SiDonneeDico="true" Etivisible="false" DonWidth="70px" DonTabIndex="20" DonStyle="margin-left:0px;" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left" BorderStyle="NotSet" BorderWidth="1px" Visible="true" BorderColor="#B0E0D7" Height="30px" Width="260px">
                                    <asp:CheckBox ID="CocheTrain" runat="server" Text="Train" Visible="true" AutoPostBack="true" BackColor="Transparent" BorderColor="White" BorderStyle="none" BorderWidth="2px" ForeColor="#142425" Height="20px" Width="250px" Checked="false" Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Small" Style="margin-top: 0px; margin-left: 0px; font-style: oblique; text-indent: 5px; text-align: left" />
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Center" BorderStyle="NotSet" BorderWidth="1px" Visible="true" BorderColor="#B0E0D7" Height="30px" Width="40px">
                                    <asp:Button ID="BoutonTrain" runat="server" Height="22px" Width="35px" Text="..." BackColor="Transparent" BorderColor="#B0E0D7" BorderStyle="Inset" BorderWidth="2px" oreColor="#142425" Visible="true" Font-Bold="true" Font-Names="Trebuchet MS" Font-Size="Small" Font-Italic="false" Style="padding-left: 2px; padding-bottom: 2px; margin-top: 0px; text-align: center; text-indent: 3px;" />
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Center" BorderStyle="NotSet" BorderWidth="1px" Visible="true" BorderColor="#B0E0D7" Height="30px" Width="190px">
                                    <asp:CheckBox ID="CocheTrainPrisEnCharge" runat="server" Text="Pris en charge" Visible="true" AutoPostBack="true" BackColor="Transparent" BorderColor="White" BorderStyle="none" BorderWidth="2px" ForeColor="#142425" Height="20px" Width="180px" Checked="false" Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Small" Style="margin-top: 0px; margin-left: 5px; font-style: oblique; text-indent: 10px; text-align: center" />
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Center" BorderStyle="NotSet" BorderWidth="1px" Visible="true" BorderColor="#B0E0D7" Height="30px" Width="150px">
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH09" runat="server" V_PointdeVue="1" V_Objet="47" V_Information="9" V_SiDonneeDico="true" Etivisible="false" DonWidth="70px" DonTabIndex="21" DonStyle="margin-left:0px;" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left" BorderStyle="NotSet" BorderWidth="1px" Visible="true" BorderColor="#B0E0D7" Height="30px" Width="260px">
                                    <asp:CheckBox ID="CocheVehiculePersonnel" runat="server" Text="Véhicule personnel" Visible="true" AutoPostBack="true" BackColor="Transparent" BorderColor="White" BorderStyle="none" BorderWidth="2px" ForeColor="#142425" Height="20px" Width="250px" Checked="false" Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Small" Style="margin-top: 0px; margin-left: 0px; font-style: oblique; text-indent: 5px; text-align: left" />
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Center" BorderStyle="NotSet" BorderWidth="1px" Visible="true" BorderColor="#B0E0D7" Height="30px" Width="40px">
                                    <asp:Button ID="BoutonVehiculePersonnel" runat="server" Height="22px" Width="35px" Text="..." BackColor="Transparent" BorderColor="#B0E0D7" BorderStyle="Inset" BorderWidth="2px" oreColor="#142425" Visible="true" Font-Bold="true" Font-Names="Trebuchet MS" Font-Size="Small" Font-Italic="false" Style="padding-left: 2px; padding-bottom: 2px; margin-top: 0px; text-align: center; text-indent: 3px;" />
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Center" BorderStyle="NotSet" BorderWidth="1px" Visible="true" BorderColor="#B0E0D7" Height="30px" Width="190px">
                                    <Virtualia:VDuoEtiquetteCommande ID="Dontab12" runat="server" V_PointdeVue="1" V_Objet="47" V_Information="12" V_SiDonneeDico="true" EtiVisible="false" DonWidth="180px" DonTabIndex="22" />
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Center" BorderStyle="NotSet" BorderWidth="1px" Visible="true" BorderColor="#B0E0D7" Height="30px" Width="150px">
                                    <asp:TextBox ID="IndemniteKmPersonnel" runat="server" Height="16px" Width="70px" MaxLength="0" BackColor="White" BorderColor="#B0E0D7" BorderStyle="Inset" DonTabIndex="23" BorderWidth="2px" ForeColor="Black" Visible="true" AutoPostBack="true" Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Small" Font-Italic="false" Style="margin-left: 2px; margin-top: 1px; text-indent: 1px; text-align: left" TextMode="SingleLine" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left" BorderStyle="NotSet" BorderWidth="1px" Visible="true" BorderColor="#B0E0D7" Height="30px" Width="260px">
                                    <asp:CheckBox ID="CocheVehiculeService" runat="server" Text="Véhicule de service" Visible="true" AutoPostBack="true" BackColor="Transparent" BorderColor="White" BorderStyle="none" BorderWidth="2px" ForeColor="#142425" Height="20px" Width="250px" Checked="false" Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Small" Style="margin-top: 0px; margin-left: 0px; font-style: oblique; text-indent: 5px; text-align: left" />
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Center" BorderStyle="NotSet" BorderWidth="1px" Visible="true" BorderColor="#B0E0D7" Height="30px" Width="40px">
                                    <asp:Button ID="BoutonVehiculeService" runat="server" Height="22px" Width="35px" Text="..." BackColor="Transparent" BorderColor="#B0E0D7" BorderStyle="Inset" BorderWidth="2px" oreColor="#142425" Visible="true" Font-Bold="true" Font-Names="Trebuchet MS" Font-Size="Small" Font-Italic="false" Style="padding-left: 2px; padding-bottom: 2px; margin-top: 0px; text-align: center; text-indent: 3px;" />
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Center" BorderStyle="NotSet" BorderWidth="1px" Visible="true" BorderColor="#B0E0D7" Height="30px" Width="190px" />
                                <asp:TableCell HorizontalAlign="Center" BorderStyle="NotSet" BorderWidth="1px" Visible="true" BorderColor="#B0E0D7" Height="30px" Width="150px">
                                    <asp:TextBox ID="IndemniteKmService" runat="server" Height="16px" Width="70px" MaxLength="0" BackColor="White" BorderColor="#B0E0D7" BorderStyle="Inset" DonTabIndex="24" BorderWidth="2px" ForeColor="Black" Visible="true" AutoPostBack="true" Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Small" Font-Italic="false" Style="margin-left: 2px; margin-top: 1px; text-indent: 1px; text-align: left" TextMode="SingleLine" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left" BorderStyle="NotSet" BorderWidth="1px" Visible="true" BorderColor="#B0E0D7" Height="30px" Width="260px">
                                    <asp:CheckBox ID="CocheVehiculeLocation" runat="server" Text="Véhicule de location" Visible="true" AutoPostBack="true" BackColor="Transparent" BorderColor="White" BorderStyle="none" BorderWidth="2px" ForeColor="#142425" Height="20px" Width="250px" Checked="false" Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Small" Style="margin-top: 0px; margin-left: 0px; font-style: oblique; text-indent: 5px; text-align: left" />
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Center" BorderStyle="NotSet" BorderWidth="1px" Visible="true" BorderColor="#B0E0D7" Height="30px" Width="40px" />
                                <asp:TableCell HorizontalAlign="Center" BorderStyle="NotSet" BorderWidth="1px" Visible="true" BorderColor="#B0E0D7" Height="30px" Width="190px">
                                    <asp:CheckBox ID="CocheLocationPrisEnCharge" runat="server" Text="Pris en charge" Visible="true" AutoPostBack="true" BackColor="Transparent" BorderColor="White" BorderStyle="none" BorderWidth="2px" ForeColor="#142425" Height="20px" Width="180px" Checked="false" Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Small" Style="margin-top: 0px; margin-left: 5px; font-style: oblique; text-indent: 10px; text-align: center" />
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Center" BorderStyle="NotSet" BorderWidth="1px" Visible="true" BorderColor="#B0E0D7" Height="30px" Width="150px">
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH11" runat="server" V_PointdeVue="1" V_Objet="47" V_Information="11" V_SiDonneeDico="true" Etivisible="false" DonWidth="70px" DonTabIndex="25" DonStyle="margin-left:0px;" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left" BorderStyle="NotSet" BorderWidth="1px" Visible="true" BorderColor="#B0E0D7" Height="30px" Width="260px">
                                    <asp:CheckBox ID="CocheTaxi" runat="server" Text="Taxi" Visible="true" AutoPostBack="true" BackColor="Transparent" BorderColor="White" BorderStyle="none" BorderWidth="2px" ForeColor="#142425" Height="20px" Width="250px" Checked="false" Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Small" Style="margin-top: 0px; margin-left: 0px; font-style: oblique; text-indent: 5px; text-align: left" />
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Center" BorderStyle="NotSet" BorderWidth="1px" Visible="true" BorderColor="#B0E0D7" Height="30px" Width="40px" />
                                <asp:TableCell HorizontalAlign="Center" BorderStyle="NotSet" BorderWidth="1px" Visible="true" BorderColor="#B0E0D7" Height="30px" Width="190px" />
                                <asp:TableCell HorizontalAlign="Center" BorderStyle="NotSet" BorderWidth="1px" Visible="true" BorderColor="#B0E0D7" Height="30px" Width="150px">
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH10" runat="server" V_PointdeVue="1" V_Objet="47" V_Information="10" V_SiDonneeDico="true" Etivisible="false" DonWidth="70px" DonTabIndex="26" DonStyle="margin-left:0px;" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left" BorderStyle="NotSet" BorderWidth="1px" Visible="true" BorderColor="#B0E0D7" Height="30px" Width="260px">
                                    <asp:CheckBox ID="CocheParking" runat="server" Text="Parking - Péage" Visible="true" AutoPostBack="true" BackColor="Transparent" BorderColor="White" BorderStyle="none" BorderWidth="2px" ForeColor="#142425" Height="20px" Width="250px" Checked="false" Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Small" Style="margin-top: 0px; margin-left: 0px; font-style: oblique; text-indent: 5px; text-align: left" />
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Center" BorderStyle="NotSet" BorderWidth="1px" Visible="true" BorderColor="#B0E0D7" Height="30px" Width="40px" />
                                <asp:TableCell HorizontalAlign="Center" BorderStyle="NotSet" BorderWidth="1px" Visible="true" BorderColor="#B0E0D7" Height="30px" Width="190px" />
                                <asp:TableCell HorizontalAlign="Center" BorderStyle="NotSet" BorderWidth="1px" Visible="true" BorderColor="#B0E0D7" Height="30px" Width="150px">
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH22" runat="server" V_PointdeVue="1" V_Objet="47" V_Information="22" V_SiDonneeDico="true" Etivisible="false" DonWidth="70px" DonTabIndex="27" DonStyle="margin-left:0px;" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left" BorderStyle="NotSet" BorderWidth="1px" Visible="true" BorderColor="#B0E0D7" Height="30px" Width="260px">
                                    <asp:CheckBox ID="CocheDivers" runat="server" Text="Divers transports publics" Visible="true" AutoPostBack="true" BackColor="Transparent" BorderColor="White" BorderStyle="none" BorderWidth="2px" ForeColor="#142425" Height="20px" Width="250px" Checked="false" Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Small" Style="margin-top: 0px; margin-left: 0px; font-style: oblique; text-indent: 5px; text-align: left" />
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Center" BorderStyle="NotSet" BorderWidth="1px" Visible="true" BorderColor="#B0E0D7" Height="30px" Width="40px" />
                                <asp:TableCell HorizontalAlign="Center" BorderStyle="NotSet" BorderWidth="1px" Visible="true" BorderColor="#B0E0D7" Height="30px" Width="190px" />
                                <asp:TableCell HorizontalAlign="Center" BorderStyle="NotSet" BorderWidth="1px" Visible="true" BorderColor="#B0E0D7" Height="30px" Width="150px">
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH23" runat="server" V_PointdeVue="1" V_Objet="47" V_Information="23" V_SiDonneeDico="true" Etivisible="false" DonWidth="70px" DonTabIndex="28" DonStyle="margin-left:0px;" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left" BorderStyle="NotSet" BorderWidth="2px" Visible="true" BorderColor="#B0E0D7" Height="30px" Width="260px" />
                                <asp:TableCell HorizontalAlign="Center" BorderStyle="NotSet" BorderWidth="2px" Visible="true" BorderColor="#B0E0D7" Height="30px" Width="40px" />
                                <asp:TableCell HorizontalAlign="Center" BorderStyle="NotSet" BorderWidth="2px" Visible="true" BorderColor="#B0E0D7" Height="30px" Width="190px">
                                    <asp:Label ID="LabelTitreTotalTransport" runat="server" Text="Total" Height="20px" Width="50px" BackColor="Transparent" BorderStyle="none" BorderWidth="0px" ForeColor="#2FA49B" Font-Bold="true" Font-Names="Trebuchet MS" Font-Size="Small" Style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; font-style: oblique; text-indent: 0px; text-align: center;" />
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Center" BorderStyle="NotSet" BorderWidth="2px" Visible="true" BorderColor="#B0E0D7" Height="30px" Width="150px">
                                    <asp:Label ID="LabelTotalTransport" runat="server" Text="0,00" Height="20px" Width="50px" BackColor="Transparent" BorderStyle="none" BorderWidth="0px" ForeColor="#142425" Font-Bold="true" Font-Names="Trebuchet MS" Font-Size="Small" Style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; font-style: oblique; text-indent: 0px; text-align: center;" />
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Table ID="Separateur5" runat="server" Height="30px" CellPadding="0" Width="750px" CellSpacing="0" HorizontalAlign="Center" BorderStyle="none" BorderWidth="1px" Visible="true" BorderColor="#B0E0D7" />
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="PanelTitreRecapitulatif" runat="server" BorderStyle="none" BorderWidth="2px" BorderColor="#B0E0D7" Height="60px" Width="750px" HorizontalAlign="Center" Style="margin-top: 3px;">
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Table ID="TableauTitreRecapitulatif" runat="server" Height="30px" CellPadding="0" Width="750px" CellSpacing="0" HorizontalAlign="Center" BorderStyle="none" BorderWidth="1px" Visible="true" BorderColor="#B0E0D7">
                            <asp:TableRow>
                                <asp:TableCell Height="20px" />
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                    <asp:Label ID="LabelTitreRecapitulatif" runat="server" Height="20px" Width="320px" BackColor="#216B68" BorderColor="#B0E0D7" BorderStyle="Outset" Text="RECAPITULATIF" BorderWidth="2px" ForeColor="#D7FAF3" Font-Italic="False" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Style="margin-top: 0px; margin-left: 5px; margin-bottom: 0px; font-style: oblique; text-indent: 2px; text-align: center;" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Height="20px" />
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="PanelRecapitulatif" runat="server" BorderStyle="None" BorderWidth="2px" BorderColor="#B0E0D7" Height="240px" Width="750px" HorizontalAlign="Center" Style="margin-top: 3px;">
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Table ID="TableauRecapitulatif" runat="server" Height="240px" CellPadding="0" Width="650px" CellSpacing="0" HorizontalAlign="Center" BorderStyle="NotSet" BorderWidth="2px" Visible="true" BorderColor="#B0E0D7">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center" BorderStyle="None" BorderWidth="1px" Visible="true" BorderColor="#B0E0D7" Height="30px" Width="350px">
                                    <asp:Label ID="LabelRecapIndemniteJour" runat="server" Text="Indemnités journalières" Height="20px" Width="350px" BackColor="Transparent" BorderStyle="none" BorderWidth="0px" ForeColor="#2FA49B" Font-Bold="true" Font-Names="Trebuchet MS" Font-Size="Small" Style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; font-style: oblique; text-indent: 30px; text-align: Left;" />
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Center" BorderStyle="None" BorderWidth="1px" Visible="true" BorderColor="#B0E0D7" Height="30px" Width="200px">
                                    <asp:Label ID="LabelRecapMontantIndemnitesJour" runat="server" Text="0,00" Height="20px" Width="190px" BackColor="Transparent" BorderStyle="none" BorderWidth="0px" ForeColor="#142425" Font-Bold="true" Font-Names="Trebuchet MS" Font-Size="Small" Style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; font-style: oblique; text-indent: 0px; text-align: right;" />
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Left" BorderStyle="None" BorderWidth="1px" Visible="true" BorderColor="#B0E0D7" Height="30px" Width="100px">
                                    <asp:Label ID="LabelEuroRecapIJ" runat="server" Text="€" Height="20px" Width="40px" BackColor="Transparent" BorderStyle="none" BorderWidth="0px" ForeColor="#2FA49B" Font-Bold="true" Font-Names="Trebuchet MS" Font-Size="Small" Style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; font-style: oblique; text-indent: 0px; text-align: center;" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell ColumnSpan="3" HorizontalAlign="Left" BorderStyle="None" BorderWidth="1px" Visible="true" BorderColor="#B0E0D7" Height="1px" Width="650px" />
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center" BorderStyle="None" BorderWidth="1px" Visible="true" BorderColor="#B0E0D7" Height="30px" Width="350px">
                                    <asp:Label ID="LabelRecapFraisTransport" runat="server" Text="Frais de transport" Height="20px" Width="350px" BackColor="Transparent" BorderStyle="none" BorderWidth="0px" ForeColor="#2FA49B" Font-Bold="true" Font-Names="Trebuchet MS" Font-Size="Small" Style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; font-style: oblique; text-indent: 30px; text-align: Left;" />
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Center" BorderStyle="None" BorderWidth="1px" Visible="true" BorderColor="#B0E0D7" Height="30px" Width="200px">
                                    <asp:Label ID="LabelRecapMontantTransport" runat="server" Text="0,00" Height="20px" Width="190px" BackColor="Transparent" BorderStyle="none" BorderWidth="0px" ForeColor="#142425" Font-Bold="true" Font-Names="Trebuchet MS" Font-Size="Small" Style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; font-style: oblique; text-indent: 0px; text-align: right;" />
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Left" BorderStyle="None" BorderWidth="1px" Visible="true" BorderColor="#B0E0D7" Height="30px" Width="100px">
                                    <asp:Label ID="LabelEuroRecapTransport" runat="server" Text="€" Height="20px" Width="40px" BackColor="Transparent" BorderStyle="none" BorderWidth="0px" ForeColor="#2FA49B" Font-Bold="true" Font-Names="Trebuchet MS" Font-Size="Small" Style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; font-style: oblique; text-indent: 0px; text-align: center;" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell ColumnSpan="3" HorizontalAlign="Left" BorderStyle="Notset" BorderWidth="1px" Visible="true" BorderColor="#B0E0D7" Height="1px" Width="650px" />
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center" BorderStyle="None" BorderWidth="1px" Visible="true" BorderColor="#B0E0D7" Height="30px" Width="350px">
                                    <asp:Label ID="LabelRecapTotal" runat="server" Text="TOTAL" Height="20px" Width="350px" BackColor="Transparent" BorderStyle="none" BorderWidth="0px" ForeColor="#2FA49B" Font-Bold="true" Font-Names="Trebuchet MS" Font-Size="Small" Style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; font-style: oblique; text-indent: 0px; text-align: center;" />
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Center" BorderStyle="None" BorderWidth="1px" Visible="true" BorderColor="#B0E0D7" Height="30px" Width="200px">
                                    <asp:Label ID="LabelRecapMontantTotal" runat="server" Text="0,00" Height="20px" Width="190px" BackColor="Transparent" BorderStyle="none" BorderWidth="0px" ForeColor="#142425" Font-Bold="true" Font-Names="Trebuchet MS" Font-Size="Small" Style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; font-style: oblique; text-indent: 0px; text-align: right;" />
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Left" BorderStyle="None" BorderWidth="1px" Visible="true" BorderColor="#B0E0D7" Height="30px" Width="100px">
                                    <asp:Label ID="LabelEuroRecapTotal" runat="server" Text="€" Height="20px" Width="40px" BackColor="Transparent" BorderStyle="none" BorderWidth="0px" ForeColor="#2FA49B" Font-Bold="true" Font-Names="Trebuchet MS" Font-Size="Small" Style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; font-style: oblique; text-indent: 0px; text-align: center;" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell ColumnSpan="3" HorizontalAlign="Left" BorderStyle="Notset" BorderWidth="1px" Visible="true" BorderColor="#B0E0D7" Height="1px" Width="650px" />
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center" BorderStyle="None" BorderWidth="1px" Visible="true" BorderColor="#B0E0D7" Height="30px" Width="350px">
                                    <asp:Label ID="LabelRecapPrisEnCharge" runat="server" Text="Pris en charge" Height="20px" Width="350px" BackColor="Transparent" BorderStyle="none" BorderWidth="0px" ForeColor="#2FA49B" Font-Bold="true" Font-Names="Trebuchet MS" Font-Size="Small" Style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; font-style: oblique; text-indent: 30px; text-align: Left;" />
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Center" BorderStyle="None" BorderWidth="1px" Visible="true" BorderColor="#B0E0D7" Height="30px" Width="200px">
                                    <asp:Label ID="LabelRecapMontantPrisEnCharge" runat="server" Text="0,00" Height="20px" Width="190px" BackColor="Transparent" BorderStyle="none" BorderWidth="0px" ForeColor="#142425" Font-Bold="true" Font-Names="Trebuchet MS" Font-Size="Small" Style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; font-style: oblique; text-indent: 0px; text-align: right;" />
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Left" BorderStyle="None" BorderWidth="1px" Visible="true" BorderColor="#B0E0D7" Height="30px" Width="100px">
                                    <asp:Label ID="LabelEuroRecapPrisEnCharge" runat="server" Text="€" Height="20px" Width="40px" BackColor="Transparent" BorderStyle="none" BorderWidth="0px" ForeColor="#2FA49B" Font-Bold="true" Font-Names="Trebuchet MS" Font-Size="Small" Style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; font-style: oblique; text-indent: 0px; text-align: center;" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center" BorderStyle="None" BorderWidth="1px" Visible="true" BorderColor="#B0E0D7" Height="30px" Width="350px">
                                    <asp:Label ID="LabelRecapAvance" runat="server" Text="Avance sur frais" Height="20px" Width="350px" BackColor="Transparent" BorderStyle="none" BorderWidth="0px" ForeColor="#2FA49B" Font-Bold="true" Font-Names="Trebuchet MS" Font-Size="Small" Style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; font-style: oblique; text-indent: 30px; text-align: Left;" />
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Center" BorderStyle="None" BorderWidth="1px" Visible="true" BorderColor="#B0E0D7" Height="30px" Width="200px">
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH24" runat="server" V_PointdeVue="1" V_Objet="47" V_Information="24" V_SiDonneeDico="true" Etivisible="false" DonWidth="70px" DonTabIndex="29" DonStyle="margin-left:120px;" />
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Left" BorderStyle="None" BorderWidth="1px" Visible="true" BorderColor="#B0E0D7" Height="30px" Width="100px">
                                    <asp:Label ID="LabelEuroRecapAvance" runat="server" Text="€" Height="20px" Width="40px" BackColor="Transparent" BorderStyle="none" BorderWidth="0px" ForeColor="#2FA49B" Font-Bold="true" Font-Names="Trebuchet MS" Font-Size="Small" Style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; font-style: oblique; text-indent: 0px; text-align: center;" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell ColumnSpan="3" HorizontalAlign="Left" BorderStyle="Notset" BorderWidth="1px" Visible="true" BorderColor="#B0E0D7" Height="1px" Width="650px" />
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center" BorderStyle="None" BorderWidth="1px" Visible="true" BorderColor="#B0E0D7" Height="30px" Width="350px">
                                    <asp:Label ID="LabelRecapTotalRembt" runat="server" Text="TOTAL à rembourser" Height="20px" Width="350px" BackColor="Transparent" BorderStyle="none" BorderWidth="0px" ForeColor="#2FA49B" Font-Bold="true" Font-Names="Trebuchet MS" Font-Size="Small" Style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; font-style: oblique; text-indent: 0px; text-align: center;" />
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Center" BorderStyle="None" BorderWidth="1px" Visible="true" BorderColor="#B0E0D7" Height="30px" Width="200px">
                                    <asp:Label ID="LabelRecapMontantTotalRembt" runat="server" Text="0,00" Height="20px" Width="190px" BackColor="Transparent" BorderStyle="none" BorderWidth="0px" ForeColor="#142425" Font-Bold="true" Font-Names="Trebuchet MS" Font-Size="Small" Style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; font-style: oblique; text-indent: 0px; text-align: right;" />
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Left" BorderStyle="None" BorderWidth="1px" Visible="true" BorderColor="#B0E0D7" Height="30px" Width="100px">
                                    <asp:Label ID="LabelEuroRecapTotalRembt" runat="server" Text="€" Height="20px" Width="40px" BackColor="Transparent" BorderStyle="none" BorderWidth="0px" ForeColor="#2FA49B" Font-Bold="true" Font-Names="Trebuchet MS" Font-Size="Small" Style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; font-style: oblique; text-indent: 0px; text-align: center;" />
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Table ID="Separateur6" runat="server" Height="30px" CellPadding="0" Width="750px" CellSpacing="0" HorizontalAlign="Center" BorderStyle="none" BorderWidth="1px" Visible="true" BorderColor="#B0E0D7">
                            <asp:TableRow>
                                <asp:TableCell Height="20px" ColumnSpan="2" />
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="PanelCertification" runat="server" BorderStyle="None" BorderWidth="2px" BorderColor="#B0E0D7" Height="300px" Width="750px" HorizontalAlign="Center" Style="margin-top: 3px;">
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Table ID="TableauCertification" runat="server" Height="300px" CellPadding="0" Width="650px" CellSpacing="0" HorizontalAlign="Center" BorderStyle="NotSet" BorderWidth="2px" Visible="true" BorderColor="#B0E0D7">
                            <asp:TableRow>
                                <asp:TableCell Height="10px" ColumnSpan="2" />
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH27" runat="server" V_PointdeVue="1" V_Objet="47" V_Information="27" V_SiDonneeDico="true" EtiWidth="160px" DonWidth="80px" DonTabIndex="30" />
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Center">
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH25" runat="server" V_PointdeVue="1" V_Objet="47" V_Information="25" V_SiDonneeDico="true" EtiWidth="160px" DonWidth="80px" DonTabIndex="31" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH34" runat="server" V_PointdeVue="1" V_Objet="47" V_Information="34" V_SiDonneeDico="true" EtiWidth="160px" DonWidth="80px" DonTabIndex="32" />
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Center">
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH26" runat="server" V_PointdeVue="1" V_Objet="47" V_Information="26" V_SiDonneeDico="true" EtiWidth="160px" DonWidth="80px" DonTabIndex="33" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left" ColumnSpan="2">
                                    <Virtualia:VCoupleVerticalEtiDonnee ID="InfoV36" runat="server" DonTextMode="true" V_PointdeVue="1" V_Objet="47" V_Information="36" V_SiDonneeDico="true" EtiWidth="628px" DonWidth="626px" DonHeight="100px" DonTabIndex="34" EtiStyle="text-align:center; margin-left: 5px;" Donstyle="margin-left: 5px;" />
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>
</asp:Table>
