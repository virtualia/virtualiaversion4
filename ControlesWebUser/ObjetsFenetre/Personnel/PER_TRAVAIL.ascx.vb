﻿Option Strict On
Option Explicit On
Option Compare Text

Imports VI = Virtualia.Systeme.Constantes
Imports Virtualia.ObjetBase
Imports Virtualia.OutilsVisu
Imports Virtualia.TablesObjet.ShemaPER

Partial Class Fenetre_PER_TRAVAIL
    Inherits ObjetWebControleBase(Of PER_VISITE_MEDICA)
    Implements IControlCrud

#Region "IControlCrud"
    Public ReadOnly Property ControlCrud As ICtlCommandeCrud Implements IControlCrud.CtlCrud
        Get
            Return Me.CtlCrud
        End Get
    End Property
#End Region

#Region "ObjetWebControleBase"
    Private _webfonction As WebFonctions
    Protected Overrides ReadOnly Property _webfonction As IWebFonctions
        Get
            If (_webfonction Is Nothing) Then
                _webfonction = New WebFonctions(Me)
            End If
            Return _webfonction
        End Get
    End Property

    Protected Overrides Function InitCadreInfo() As WebControl
        Return Me.CadreInfo
    End Function

    Protected Overrides Function InitControlDataGrid() As IControlDataGrid
        Return Me.ListeGrille
    End Function

    Protected Overrides Sub InitCacheIde()

        V_CacheIde.TypeVue = WebControlTypeVue.ListeGrid

        Dim captions As List(Of String) = New List(Of String)()
        captions.Add("Aucune situation")
        captions.Add("Une situation")
        captions.Add("situations")

        Me.ListeGrille.InitCaptions(captions)

        Dim colonnes As ColonneDataGridCollection = New ColonneDataGridCollection()

        colonnes.Add(HorizontalAlign.Center, TypeData.Numeric, "date", 0)
        colonnes.Add(HorizontalAlign.Center, TypeData.Numeric, "cycle", 15)
        colonnes.Add(HorizontalAlign.Center, TypeData.Numeric, "horaire", 16)
        colonnes.Add(HorizontalAlign.Center, TypeData.Numeric, "variable", 19)
        colonnes.Add(HorizontalAlign.Center, TypeData.Numeric, "jours fériés", 18)
        colonnes.Add(HorizontalAlign.Center, TypeData.Chaine, "Clef", 0, True)

        Me.ListeGrille.InitColonnes(colonnes)

    End Sub

    Protected Overrides Sub OnPreRender(e As EventArgs)
        MyBase.OnPreRender(e)

        CadreInfo.BackColor = _webfonction.CouleurCharte(V_CacheIde.PointDeVue, "Cadre")
        CadreInfo.BorderColor = _webfonction.CouleurCharte(V_CacheIde.PointDeVue, "Bordure")
        Etiquette.BackColor = _webfonction.CouleurCharte(V_CacheIde.PointDeVue, "Titre")
        Etiquette.ForeColor = _webfonction.CouleurCharte(V_CacheIde.PointDeVue, "Police_Claire")
        Etiquette.BorderColor = _webfonction.CouleurCharte(V_CacheIde.PointDeVue, "Bordure")
        EtiDateEffet.BackColor = _webfonction.CouleurCharte(V_CacheIde.PointDeVue, "Etiquette_BackColor")
        EtiDateEffet.ForeColor = _webfonction.CouleurCharte(V_CacheIde.PointDeVue, "Etiquette_ForeColor")
        EtiDateEffet.BorderColor = _webfonction.CouleurCharte(V_CacheIde.PointDeVue, "Bordure")

    End Sub

    Protected Overrides Sub OnInit(e As System.EventArgs)
        MyBase.OnInit(e)

        Dim initwebfonction As IWebFonctions = _webfonction
    End Sub
#End Region

#Region "Specifique"
    Protected Overrides Sub SelectDataDansDataGrid(clef As String)
        If (clef Is Nothing) Then
            Return
        End If
        If (clef.Trim() = "") Then
            Return
        End If
        If Not (IsNumeric(clef)) Then
            Return
        End If
        If V_CacheIde.EstNothing Or V_CacheIde.IndexFiche = -1 Then
            Return
        End If
        If (V_CacheIde.Ide_Dossier <= 0) Then
            Return
        End If

        EtiDateEffet.Text = _webfonction.ViRhDates.ClairDate(e.Valeur, True)

        MyBase.SelectDataDansDataGrid(clef)
    End Sub

    Protected Overrides Sub LireLaFiche()
        MyBase.LireLaFiche()

        If V_CacheMaj Is Nothing Then
            Return
        End If

        Select Case V_CacheMaj(15).valeur
            Case "Cycle administratif"
                TableauSemaineAdm.Visible = True
                TableauCycleTravail.Visible = False
                TableauQuinzaine.Visible = False
            Case "Cycle personnalisé"
                TableauSemaineAdm.Visible = False
                TableauCycleTravail.Visible = False
                TableauQuinzaine.Visible = True
            Case Else
                TableauSemaineAdm.Visible = False
                TableauCycleTravail.Visible = True
                TableauQuinzaine.Visible = False
        End Select

        Select Case _WsFiche.TypeduCycle
            Case VI.TypedeCycleTravail.CycledeTravail
                SpecifiqueCycleTravail()
            Case VI.TypedeCycleTravail.EmploiduTemps
                SpecifiqueCyclePerso()
        End Select

    End Sub

    Private Sub SpecifiqueCycleTravail()

        Dim CycleTravail As TRA_IDENTIFICATION
        CycleTravail = _webfonction.PointeurUtilisateur.PointeurParametres.PointeurCyclesTravail.Fiche_Cycle(_WsFiche.CycledeTravail)

        If CycleTravail Is Nothing Then
            Exit Sub
        End If

        Dim IndiceI As Integer
        Dim IndiceK As Integer
        Dim VirCycleEdite As Controles_VCycledeBase = Nothing
        Dim CycleBase As CYC_IDENTIFICATION
        Dim UniteCycle As CYC_UNITE
        Dim FichePre As PRE_IDENTIFICATION
        Dim FicheAbs As ABS_ABSENCE
        Dim ChaineAM As String
        Dim ChainePM As String
        Dim NbCycles As Integer = 0
        Dim IndexRotation As Integer = 0

        For IndiceI = 0 To 7
            CycleBase = _webfonction.PointeurUtilisateur.PointeurParametres.PointeurCyclesTravail.CycledeBase(CycleTravail, IndiceI + 1)
            If CycleBase Is Nothing Then
                IndexRotation = (IndiceI Mod NbCycles) + 1
                CycleBase = _webfonction.PointeurUtilisateur.PointeurParametres.PointeurCyclesTravail.CycledeBase(CycleTravail, IndexRotation)
                If CycleBase Is Nothing Then
                    CycleBase = _webfonction.PointeurUtilisateur.PointeurParametres.PointeurCyclesTravail.CycledeBase(CycleTravail, 1)
                End If
            Else
                NbCycles += 1
            End If

            Select Case IndiceI
                Case 0
                    VirCycleEdite = VCycle_1
                Case 1
                    VirCycleEdite = VCycle_2
                Case 2
                    VirCycleEdite = VCycle_3
                Case 3
                    VirCycleEdite = VCycle_4
                Case 4
                    VirCycleEdite = VCycle_5
                Case 5
                    VirCycleEdite = VCycle_6
                Case 6
                    VirCycleEdite = VCycle_7
                Case 7
                    VirCycleEdite = VCycle_8
            End Select

            VirCycleEdite.Visible = True
            VirCycleEdite.VEtiText = CycleBase.Intitule
            VirCycleEdite.VEtiToolTip = CycleBase.Intitule & " - " & CycleBase.NombreHeures_Total

            For IndiceK = 0 To 6
                VirCycleEdite.SiJourVisible(IndiceK, "JAM") = False
                VirCycleEdite.VText(IndiceK, "JAM") = ""
                VirCycleEdite.VDemiJourToolTip(IndiceK, "JAM") = ""
                VirCycleEdite.VJourStyle(IndiceK, "JAM") = "none"
                VirCycleEdite.SiJourVisible(IndiceK, "JPM") = False
                VirCycleEdite.VText(IndiceK, "JPM") = ""
                VirCycleEdite.VDemiJourToolTip(IndiceK, "JPM") = ""
            Next IndiceK

            For IndiceK = 0 To CycleBase.ListedesCycles.Count - 1
                UniteCycle = CycleBase.Fiche_Cycle(CType(IndiceK + 2, Short))
                If UniteCycle Is Nothing Then
                    Exit For
                End If

                ChaineAM = ""
                If UniteCycle.Denomination_Plage1 <> "" Then
                    FichePre = _webfonction.PointeurUtilisateur.PointeurParametres.PointeurPresences.Fiche_Presence(UniteCycle.Denomination_Plage1)
                    If FichePre Is Nothing Then
                        FicheAbs = _webfonction.PointeurUtilisateur.PointeurParametres.PointeurAbsences.Fiche_Absence(UniteCycle.Denomination_Plage1)
                        If FicheAbs IsNot Nothing Then
                            ChaineAM = FicheAbs.Mnemonique
                        End If
                    Else
                        ChaineAM = FichePre.Mnemonique
                    End If
                End If

                VirCycleEdite.VText(IndiceK, "JAM") = ChaineAM
                VirCycleEdite.SiJourVisible(IndiceK, "JAM") = True

                If UniteCycle.Heure_Debut_Plage1 = "" Then
                    VirCycleEdite.VDemiJourToolTip(IndiceK, "JAM") = UniteCycle.Denomination_Plage1
                    VirCycleEdite.VBackColor(IndiceK, "JAM") = _webfonction.PointeurUtilisateur.PointeurParametres.CouleurPlanning(1)
                Else
                    VirCycleEdite.VDemiJourToolTip(IndiceK, "JAM") = UniteCycle.Denomination_Plage1 & " de " & _
                            UniteCycle.Heure_Debut_Plage1 & " à " & UniteCycle.Heure_Fin_Plage1
                    VirCycleEdite.VBackColor(IndiceK, "JAM") = _webfonction.PointeurUtilisateur.PointeurParametres.CouleurPlanning(0)
                    VirCycleEdite.VBorderColor(IndiceK, "JAM") = _webfonction.ConvertCouleur("#216B68")
                    VirCycleEdite.VJourStyle(IndiceK, "JAM") = "solid"
                End If

                ChainePM = ""
                If UniteCycle.Denomination_Plage2 <> "" Then
                    FichePre = _webfonction.PointeurUtilisateur.PointeurParametres.PointeurPresences.Fiche_Presence(UniteCycle.Denomination_Plage2)
                    If FichePre Is Nothing Then
                        FicheAbs = _webfonction.PointeurUtilisateur.PointeurParametres.PointeurAbsences.Fiche_Absence(UniteCycle.Denomination_Plage2)
                        If FicheAbs IsNot Nothing Then
                            ChainePM = FicheAbs.Mnemonique
                        End If
                    Else
                        ChainePM = FichePre.Mnemonique
                    End If
                End If

                If ChainePM = ChaineAM Then
                    ChainePM = ""
                End If

                VirCycleEdite.VText(IndiceK, "JPM") = ChainePM
                VirCycleEdite.SiJourVisible(IndiceK, "JPM") = True

                If UniteCycle.Heure_Debut_Plage2 = "" Then
                    VirCycleEdite.VDemiJourToolTip(IndiceK, "JPM") = UniteCycle.Denomination_Plage2
                    VirCycleEdite.VBackColor(IndiceK, "JPM") = _webfonction.PointeurUtilisateur.PointeurParametres.CouleurPlanning(1)
                Else
                    VirCycleEdite.VDemiJourToolTip(IndiceK, "JPM") = UniteCycle.Denomination_Plage2 & " de " & _
                           UniteCycle.Heure_Debut_Plage2 & " à " & UniteCycle.Heure_Fin_Plage2
                    VirCycleEdite.VBackColor(IndiceK, "JPM") = _webfonction.PointeurUtilisateur.PointeurParametres.CouleurPlanning(0)
                    VirCycleEdite.VBorderColor(IndiceK, "JPM") = _webfonction.ConvertCouleur("#216B68")
                End If

            Next IndiceK
        Next IndiceI
    End Sub

    Private Sub SpecifiqueCyclePerso()

        Dim TableauData(0) As String = Strings.Split(_WsFiche.ChaineCyclePersonnalisee, VI.Tild, -1)

        Dim CycleTravail As TRA_IDENTIFICATION
        Dim IndiceI As Integer
        Dim IndiceK As Integer
        Dim VirCycleEdite As Controles_VCycledeBase = Nothing
        Dim CycleBase As CYC_IDENTIFICATION
        Dim UniteCycle As CYC_UNITE
        Dim FichePre As PRE_IDENTIFICATION
        Dim FicheAbs As ABS_ABSENCE
        Dim ChaineAM As String
        Dim ChainePM As String

        For IndiceI = 0 To TableauData.Count - 1

            If TableauData(IndiceI) = "" Then
                Exit For
            End If

            CycleTravail = _webfonction.PointeurUtilisateur.PointeurParametres.PointeurCyclesTravail.Fiche_Cycle(TableauData(IndiceI))

            If CycleTravail Is Nothing Then
                Exit For
            End If

            CycleBase = _webfonction.PointeurUtilisateur.PointeurParametres.PointeurCyclesTravail.CycledeBase(CycleTravail, 1)

            If CycleBase Is Nothing Then
                Exit For
            End If

            Select Case IndiceI
                Case 0 To 6
                    VirCycleEdite = VCyclePerso_1
                    IndiceK = IndiceI
                Case Else
                    VirCycleEdite = VCyclePerso_2
                    IndiceK = IndiceI - 7
            End Select

            VirCycleEdite.Visible = True
            VirCycleEdite.VEtiText = "Quinzaine personnalisée"
            VirCycleEdite.VEtiToolTip = CycleBase.Intitule & " - " & CycleBase.NombreHeures_Total

            VirCycleEdite.SiJourVisible(IndiceK, "JAM") = False
            VirCycleEdite.VText(IndiceK, "JAM") = ""
            VirCycleEdite.VDemiJourToolTip(IndiceK, "JAM") = ""
            VirCycleEdite.VJourStyle(IndiceK, "JAM") = "none"
            VirCycleEdite.SiJourVisible(IndiceK, "JPM") = False
            VirCycleEdite.VText(IndiceK, "JPM") = ""
            VirCycleEdite.VDemiJourToolTip(IndiceK, "JPM") = ""

            UniteCycle = CycleBase.Fiche_Cycle(2)

            If UniteCycle IsNot Nothing Then
                ChaineAM = ""
                If UniteCycle.Denomination_Plage1 <> "" Then
                    FichePre = _webfonction.PointeurUtilisateur.PointeurParametres.PointeurPresences.Fiche_Presence(UniteCycle.Denomination_Plage1)
                    If FichePre Is Nothing Then
                        FicheAbs = _webfonction.PointeurUtilisateur.PointeurParametres.PointeurAbsences.Fiche_Absence(UniteCycle.Denomination_Plage1)
                        If FicheAbs IsNot Nothing Then
                            ChaineAM = FicheAbs.Mnemonique
                        End If
                    Else
                        ChaineAM = FichePre.Mnemonique
                    End If
                End If

                VirCycleEdite.VText(IndiceK, "JAM") = ChaineAM
                VirCycleEdite.SiJourVisible(IndiceK, "JAM") = True

                If UniteCycle.Heure_Debut_Plage1 = "" Then
                    VirCycleEdite.VDemiJourToolTip(IndiceK, "JAM") = UniteCycle.Denomination_Plage1
                    VirCycleEdite.VBackColor(IndiceK, "JAM") = _webfonction.PointeurUtilisateur.PointeurParametres.CouleurPlanning(1)
                Else
                    VirCycleEdite.VDemiJourToolTip(IndiceK, "JAM") = UniteCycle.Denomination_Plage1 & " de " & _
                            UniteCycle.Heure_Debut_Plage1 & " à " & UniteCycle.Heure_Fin_Plage1
                    VirCycleEdite.VBackColor(IndiceK, "JAM") = _webfonction.PointeurUtilisateur.PointeurParametres.CouleurPlanning(0)
                    VirCycleEdite.VBorderColor(IndiceK, "JAM") = _webfonction.ConvertCouleur("#216B68")
                    VirCycleEdite.VJourStyle(IndiceK, "JAM") = "solid"
                End If

                ChainePM = ""
                If UniteCycle.Denomination_Plage2 <> "" Then
                    FichePre = _webfonction.PointeurUtilisateur.PointeurParametres.PointeurPresences.Fiche_Presence(UniteCycle.Denomination_Plage2)
                    If FichePre Is Nothing Then
                        FicheAbs = _webfonction.PointeurUtilisateur.PointeurParametres.PointeurAbsences.Fiche_Absence(UniteCycle.Denomination_Plage2)
                        If FicheAbs IsNot Nothing Then
                            ChainePM = FicheAbs.Mnemonique
                        End If
                    Else
                        ChainePM = FichePre.Mnemonique
                    End If
                End If

                If ChainePM = ChaineAM Then
                    ChainePM = ""
                End If

                VirCycleEdite.VText(IndiceK, "JPM") = ChainePM
                VirCycleEdite.SiJourVisible(IndiceK, "JPM") = True
                If UniteCycle.Heure_Debut_Plage2 = "" Then
                    VirCycleEdite.VDemiJourToolTip(IndiceK, "JPM") = UniteCycle.Denomination_Plage2
                    VirCycleEdite.VBackColor(IndiceK, "JPM") = _webfonction.PointeurUtilisateur.PointeurParametres.CouleurPlanning(1)
                Else
                    VirCycleEdite.VDemiJourToolTip(IndiceK, "JPM") = UniteCycle.Denomination_Plage2 & " de " & _
                           UniteCycle.Heure_Debut_Plage2 & " à " & UniteCycle.Heure_Fin_Plage2
                    VirCycleEdite.VBackColor(IndiceK, "JPM") = _webfonction.PointeurUtilisateur.PointeurParametres.CouleurPlanning(0)
                    VirCycleEdite.VBorderColor(IndiceK, "JPM") = _webfonction.ConvertCouleur("#216B68")
                End If

            End If

        Next IndiceI
    End Sub
#End Region

End Class
