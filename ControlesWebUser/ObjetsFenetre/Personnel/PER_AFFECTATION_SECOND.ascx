﻿<%@ Control Language="VB" AutoEventWireup="false" Inherits="Virtualia.Net.Fenetre_PER_AFFECTATION_SECOND" CodeBehind="PER_AFFECTATION_SECOND.ascx.vb" %>

<%@ Register Src="~/Controles/Saisies/VCoupleEtiDonnee.ascx" TagName="VCoupleEtiDonnee" TagPrefix="Virtualia" %>
<%@ Register Src="~/Controles/Saisies/VDuoEtiquetteCommande.ascx" TagName="VDuoEtiquetteCommande" TagPrefix="Virtualia" %>
<%@ Register Src="~/Controles/Saisies/VCocheSimple.ascx" TagName="VCocheSimple" TagPrefix="Virtualia" %>

<%@ Register Src="~/ControlsGeneriques/V_DataGrid.ascx" TagName="VDataGrid" TagPrefix="Generic" %>
<%@ Register Src="~/ControlsGeneriques/V_CommandeCRUD.ascx" TagName="VCrud" TagPrefix="Generic" %>

<asp:Table ID="CadreControle" runat="server" Height="30px" Width="600px" CellPadding="0" CellSpacing="0" HorizontalAlign="Center">
    <asp:TableRow>
        <asp:TableCell>
            <Generic:VDataGrid ID="ListeGrille" runat="server" CadreWidth="680px" SiColonneSelect="true" SiCaseAcocher="false" V_NiveauCharte="0" />
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="CadreInfo" runat="server" BorderStyle="Ridge" BorderWidth="2px" BackColor="#CBF2BE" BorderColor="#B0E0D7" Height="500px" Width="700px" HorizontalAlign="Center" Style="margin-top: 3px;">
                <asp:TableRow>
                    <asp:TableCell>
                        <Generic:VCrud ID="CtlCrud" runat="server" Height="22px" Width="244px" HorizontalAlign="Right" CadreStyle="margin-top: 3px; margin-right: 3px" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Table ID="CadreTitre" runat="server" Height="40px" CellPadding="0" Width="700px" CellSpacing="0" HorizontalAlign="Center">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                    <asp:Label ID="Etiquette" runat="server" Text="Affectation fonctionnelle secondaire" Height="20px" Width="310px" BackColor="#2FA49B" BorderColor="#B0E0D7" BorderStyle="Groove" BorderWidth="2px" ForeColor="#D7FAF3" Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Small" Style="margin-top: 5px; margin-left: 4px; margin-bottom: 10px; font-style: oblique; text-indent: 5px; text-align: center;" />
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Table ID="CadreDon" runat="server" CellPadding="0" CellSpacing="0" Width="700px">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left" Width="300px">
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH00" runat="server" EtiBackColor="#CBF2BE" V_PointdeVue="1" V_Objet="17" V_Information="0" V_SiDonneeDico="true" V_SiFenetrePer_Bis="true" DonWidth="100px" DonTabIndex="1" />
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Left" Width="300px">
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH07" runat="server" EtiBackColor="#CBF2BE" V_PointdeVue="1" V_Objet="17" V_Information="7" V_SiDonneeDico="true" V_SiFenetrePer_Bis="true" EtiWidth="80px" DonWidth="100px" DonTabIndex="2" EtiStyle="margin-left:2px" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left" ColumnSpan="2">
                                    <Virtualia:VDuoEtiquetteCommande ID="Dontab01" runat="server" EtiBackColor="#CBF2BE" V_PointdeVue="1" V_Objet="17" V_Information="1" V_SiDonneeDico="true" V_SiFenetrePer_Bis="true" DonTabIndex="3" DonWidth="490px" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left" ColumnSpan="2">
                                    <Virtualia:VDuoEtiquetteCommande ID="Dontab02" runat="server" EtiBackColor="#CBF2BE" V_PointdeVue="1" V_Objet="17" V_Information="2" V_SiDonneeDico="true" V_SiFenetrePer_Bis="true" DonTabIndex="4" DonWidth="490px" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left" ColumnSpan="2">
                                    <Virtualia:VDuoEtiquetteCommande ID="Dontab03" runat="server" EtiBackColor="#CBF2BE" V_PointdeVue="1" V_Objet="17" V_Information="3" V_SiDonneeDico="true" V_SiFenetrePer_Bis="true" DonTabIndex="5" DonWidth="490px" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left" ColumnSpan="2">
                                    <Virtualia:VDuoEtiquetteCommande ID="Dontab04" runat="server" EtiBackColor="#CBF2BE" V_PointdeVue="1" V_Objet="17" V_Information="4" V_SiDonneeDico="true" V_SiFenetrePer_Bis="true" DonTabIndex="6" DonWidth="490px" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left" ColumnSpan="2">
                                    <Virtualia:VDuoEtiquetteCommande ID="Dontab14" runat="server" EtiBackColor="#CBF2BE" V_PointdeVue="1" V_Objet="17" V_Information="14" V_SiDonneeDico="true" V_SiFenetrePer_Bis="true" DonTabIndex="7" DonWidth="490px" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left" ColumnSpan="2">
                                    <Virtualia:VDuoEtiquetteCommande ID="Dontab15" runat="server" EtiBackColor="#CBF2BE" V_PointdeVue="1" V_Objet="17" V_Information="15" V_SiDonneeDico="true" V_SiFenetrePer_Bis="true" DonTabIndex="8" DonWidth="490px" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow Height="40px">
                                <asp:TableCell HorizontalAlign="Center" ColumnSpan="2">
                                    <asp:Label ID="EtiEmploi" runat="server" Height="20px" Width="350px" Text="Emploi / Fonction / Poste" BackColor="#CBF2BE" BorderColor="#B0E0D7" BorderStyle="Outset" BorderWidth="2px" ForeColor="#142425" Font-Italic="true" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left" ColumnSpan="2">
                                    <Virtualia:VDuoEtiquetteCommande ID="Dontab05" runat="server" EtiBackColor="#CBF2BE" V_PointdeVue="1" V_Objet="17" V_Information="5" V_SiDonneeDico="true" V_SiFenetrePer_Bis="true" DonTabIndex="9" DonWidth="490px" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left" ColumnSpan="2">
                                    <Virtualia:VDuoEtiquetteCommande ID="Dontab17" runat="server" EtiBackColor="#CBF2BE" V_PointdeVue="1" V_Objet="17" V_Information="17" V_SiDonneeDico="true" V_SiFenetrePer_Bis="true" DonTabIndex="10" DonWidth="490px" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH13" runat="server" EtiBackColor="#CBF2BE" V_PointdeVue="1" V_Objet="17" V_Information="13" V_SiDonneeDico="true" V_SiFenetrePer_Bis="true" DonWidth="120px" DonTabIndex="11" />
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Right">
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH06" runat="server" EtiBackColor="#CBF2BE" V_PointdeVue="1" V_Objet="17" V_Information="6" V_SiDonneeDico="true" V_SiFenetrePer_Bis="true" DonWidth="80px" DonTabIndex="12" DonStyle="margin-left:2px" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow Height="40px">
                                <asp:TableCell HorizontalAlign="Center" ColumnSpan="2">
                                    <asp:Label ID="EtiComplement" runat="server" Height="20px" Width="350px" Text="Informations complémentaires" BackColor="#CBF2BE" BorderColor="#B0E0D7" BorderStyle="Outset" BorderWidth="2px" ForeColor="#142425" Font-Italic="true" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left" ColumnSpan="2">
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH11" runat="server" EtiBackColor="#CBF2BE" V_PointdeVue="1" V_Objet="17" V_Information="11" V_SiDonneeDico="true" V_SiFenetrePer_Bis="true" EtiWidth="120px" DonWidth="490px" DonTabIndex="13" EtiStyle="margin-top:5px" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left" ColumnSpan="2">
                                    <Virtualia:VCocheSimple ID="Coche12" runat="server" V_BackColor="#CBF2BE" V_PointdeVue="1" V_Objet="17" V_Information="12" V_SiDonneeDico="true" V_SiFenetrePer_Bis="true" V_Width="320px" V_Style="margin-left:4px" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Height="12px" ColumnSpan="2" />
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>
</asp:Table>
