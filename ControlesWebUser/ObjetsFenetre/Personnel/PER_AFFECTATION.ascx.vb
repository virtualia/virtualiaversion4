﻿Option Strict On
Option Explicit On
Option Compare Text

Imports VI = Virtualia.Systeme.Constantes
Imports Virtualia.ObjetBase
Imports Virtualia.OutilsVisu
Imports Virtualia.TablesObjet.ShemaPER

Partial Class Fenetre_PER_AFFECTATION
    Inherits ObjetWebControleBase(Of PER_AFFECTATION)
    Implements IControlCrud

#Region "IControlCrud"
    Public ReadOnly Property ControlCrud As ICtlCommandeCrud Implements IControlCrud.CtlCrud
        Get
            Return Me.CtlCrud
        End Get
    End Property
#End Region

#Region "ObjetWebControleBase"
    Private _webfonction As WebFonctions
    Protected Overrides ReadOnly Property V_WebFonction As IWebFonctions
        Get
            If (_webfonction Is Nothing) Then
                _webfonction = New WebFonctions(Me)
            End If
            Return _webfonction
        End Get
    End Property

    Protected Overrides Function InitCadreInfo() As WebControl
        Return Me.CadreInfo
    End Function

    Protected Overrides Function InitControlDataGrid() As IControlDataGrid
        Return Me.ListeGrille
    End Function

    Protected Overrides Sub InitCacheIde()

        V_CacheIde.TypeVue = WebControlTypeVue.ListeGrid

        Dim captions As List(Of String) = New List(Of String)()
        captions.Add("Aucune affectation")
        captions.Add("Une affectation")
        captions.Add("affectations")

        Me.ListeGrille.InitCaptions(captions)

        Dim colonnes As ColonneDataGridCollection = New ColonneDataGridCollection()

        colonnes.Add(HorizontalAlign.Center, TypeData.Date, "date d'effet", 0)
        colonnes.Add(HorizontalAlign.Center, TypeData.Numeric, "Niveau N1", 1)
        colonnes.Add(HorizontalAlign.Center, TypeData.Numeric, "Niveau N2", 2)
        colonnes.Add(HorizontalAlign.Center, TypeData.Numeric, "Emploi", 5)
        colonnes.Add(HorizontalAlign.Center, TypeData.Numeric, "date de fin", 99)
        colonnes.Add(HorizontalAlign.Center, TypeData.Chaine, "Clef", 0, True)

        Me.ListeGrille.InitColonnes(colonnes)

    End Sub

    Protected Overrides Sub OnInit(e As System.EventArgs)
        MyBase.OnInit(e)

        Dim initwebfonction As IWebFonctions = V_WebFonction
    End Sub

    Protected Overrides Sub OnPreRender(e As EventArgs)
        MyBase.OnPreRender(e)

        InitStyleCadre(Me.CadreExpert, _CadreExpertStyle)

        CadreInfo.BackColor = _webfonction.CouleurCharte(V_CacheIde.PointDeVue, "Cadre")
        CadreInfo.BorderColor = _webfonction.CouleurCharte(V_CacheIde.PointDeVue, "Bordure")
        Etiquette.BackColor = _webfonction.CouleurCharte(V_CacheIde.PointDeVue, "Titre")
        Etiquette.ForeColor = _webfonction.CouleurCharte(V_CacheIde.PointDeVue, "Police_Claire")
        Etiquette.BorderColor = _webfonction.CouleurCharte(V_CacheIde.PointDeVue, "Bordure")
        EtiEmploi.BackColor = _webfonction.CouleurCharte(V_CacheIde.PointDeVue, "Sous-Titre")
        EtiEmploi.ForeColor = _webfonction.CouleurCharte(V_CacheIde.PointDeVue, "Etiquette_ForeColor")
        EtiEmploi.BorderColor = _webfonction.CouleurCharte(V_CacheIde.PointDeVue, "Bordure")
        EtiComplement.BackColor = _webfonction.CouleurCharte(V_CacheIde.PointDeVue, "Sous-Titre")
        EtiComplement.ForeColor = _webfonction.CouleurCharte(V_CacheIde.PointDeVue, "Etiquette_ForeColor")
        EtiComplement.BorderColor = _webfonction.CouleurCharte(V_CacheIde.PointDeVue, "Bordure")
    End Sub
#End Region

#Region "Specifiques"
    Protected Overrides Sub LireExperte()

        _dicoWebControl(TypeWebControl.VDuoEtiquetteCommande).ForEach(Sub(ct)
                                                                          Dim ctl As Controles_VDuoEtiquetteCommandeBase = DirectCast(ct, Controles_VDuoEtiquetteCommandeBase)
                                                                          Select Case ctl.V_Information
                                                                              Case 1 To 4
                                                                                  ctl.EtiText = _webfonction.PointeurParametres.LibelleNiveau_Organigramme(ctl.V_Information - 1)
                                                                                  If ctl.EtiText = "" Then
                                                                                      ctl.Visible = False
                                                                                  End If
                                                                              Case 14, 15
                                                                                  ctl.EtiText = _webfonction.PointeurParametres.LibelleNiveau_Organigramme(ctl.V_Information - 10)
                                                                                  If ctl.EtiText = "" Then
                                                                                      ctl.Visible = False
                                                                                  End If
                                                                          End Select
                                                                      End Sub)

        If CadreExpert.Visible Then
            MyBase.LireExperte()
        End If
    End Sub

    Private _CadreExpertStyle As String = ""
    Public WriteOnly Property CadreExpertStyle As String
        Set(ByVal value As String)
            _CadreExpertStyle = value
        End Set
    End Property

    Public WriteOnly Property CadreExpertVisible() As Boolean
        Set(ByVal value As Boolean)
            CadreExpert.Visible = value
        End Set
    End Property
#End Region

End Class

