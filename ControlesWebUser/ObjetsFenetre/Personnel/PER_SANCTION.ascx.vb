﻿Option Strict On
Option Explicit On
Option Compare Text

Imports VI = Virtualia.Systeme.Constantes
Imports Virtualia.ObjetBase
Imports Virtualia.OutilsVisu
Imports Virtualia.TablesObjet.ShemaPER

Partial Class Fenetre_PER_SANCTION
    Inherits ObjetWebControleBase(Of PER_SANCTION)
    Implements IControlCrud

#Region "IControlCrud"
    Public ReadOnly Property ControlCrud As ICtlCommandeCrud Implements IControlCrud.CtlCrud
        Get
            Return Me.CtlCrud
        End Get
    End Property
#End Region

#Region "ObjetWebControleBase"
    Private _webfonction As WebFonctions
    Protected Overrides ReadOnly Property _webfonction As IWebFonctions
        Get
            If (_webfonction Is Nothing) Then
                _webfonction = New WebFonctions(Me)
            End If
            Return _webfonction
        End Get
    End Property

    Protected Overrides Function InitCadreInfo() As WebControl
        Return Me.CadreInfo
    End Function

    Protected Overrides Function InitControlDataGrid() As IControlDataGrid
        Return Me.ListeGrille
    End Function

    Protected Overrides Sub InitCacheIde()

        V_CacheIde.TypeVue = WebControlTypeVue.ListeGrid

        Dim captions As List(Of String) = New List(Of String)()
        captions.Add("Aucune sanction disciplinaire")
        captions.Add("Une sanction disciplinaire")
        captions.Add("sanctions disciplinaires")

        Me.ListeGrille.InitCaptions(captions)

        Dim colonnes As ColonneDataGridCollection = New ColonneDataGridCollection()

        colonnes.Add(HorizontalAlign.Center, TypeData.Date, "date d'effet", 0)
        colonnes.Add(HorizontalAlign.Center, TypeData.Numeric, "nature", 1)
        colonnes.Add(HorizontalAlign.Center, TypeData.Numeric, "suspension", 5)
        colonnes.Add(HorizontalAlign.Center, TypeData.Numeric, "date de fin", 99)
        colonnes.Add(HorizontalAlign.Center, TypeData.Chaine, "Clef", 0, True)

        Me.ListeGrille.InitColonnes(colonnes)

    End Sub

    Protected Overrides Sub OnPreRender(e As EventArgs)
        MyBase.OnPreRender(e)

        CadreInfo.BackColor = _webfonction.CouleurCharte(V_CacheIde.PointDeVue, "Cadre")
        CadreInfo.BorderColor = _webfonction.CouleurCharte(V_CacheIde.PointDeVue, "Bordure")
        Etiquette.BackColor = _webfonction.CouleurCharte(V_CacheIde.PointDeVue, "Titre")
        Etiquette.ForeColor = _webfonction.CouleurCharte(V_CacheIde.PointDeVue, "Police_Claire")
        Etiquette.BorderColor = _webfonction.CouleurCharte(V_CacheIde.PointDeVue, "Bordure")
        EtiConseilDiscipline.BackColor = _webfonction.CouleurCharte(V_CacheIde.PointDeVue, "Sous-Titre")
        EtiConseilDiscipline.ForeColor = _webfonction.CouleurCharte(V_CacheIde.PointDeVue, "Etiquette_ForeColor")
        EtiConseilDiscipline.BorderColor = _webfonction.CouleurCharte(V_CacheIde.PointDeVue, "Bordure")
        EtiDecision.BackColor = _webfonction.CouleurCharte(V_CacheIde.PointDeVue, "Sous-Titre")
        EtiDecision.ForeColor = _webfonction.CouleurCharte(V_CacheIde.PointDeVue, "Etiquette_ForeColor")
        EtiDecision.BorderColor = _webfonction.CouleurCharte(V_CacheIde.PointDeVue, "Bordure")
        EtiSanction.BackColor = _webfonction.CouleurCharte(V_CacheIde.PointDeVue, "Sous-Titre")
        EtiSanction.ForeColor = _webfonction.CouleurCharte(V_CacheIde.PointDeVue, "Etiquette_ForeColor")
        EtiSanction.BorderColor = _webfonction.CouleurCharte(V_CacheIde.PointDeVue, "Bordure")

    End Sub

    Protected Overrides Sub OnInit(e As System.EventArgs)
        MyBase.OnInit(e)

        Dim initwebfonction As IWebFonctions = _webfonction
    End Sub
#End Region

End Class
