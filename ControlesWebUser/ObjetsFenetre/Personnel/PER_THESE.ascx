﻿<%@ Control Language="VB" AutoEventWireup="false" Inherits="Virtualia.Net.Fenetre_PER_THESE" CodeBehind="PER_THESE.ascx.vb" %>

<%@ Register Src="~/Controles/Saisies/VCoupleEtiDonnee.ascx" TagName="VCoupleEtiDonnee" TagPrefix="Virtualia" %>
<%@ Register Src="~/Controles/Saisies/VDuoEtiquetteCommande.ascx" TagName="VDuoEtiquetteCommande" TagPrefix="Virtualia" %>
<%@ Register Src="~/Controles/Saisies/VCoupleVerticalEtiDonnee.ascx" TagName="VCoupleVerticalEtiDonnee" TagPrefix="Virtualia" %>

<%@ Register Src="~/ControlsGeneriques/V_CommandeOK.ascx" TagName="VCmdOK" TagPrefix="Generic" %>

<asp:Table ID="CadreInfo" runat="server" BorderStyle="Ridge" BorderWidth="2px" BorderColor="#B0E0D7" Height="725px" Width="630px" HorizontalAlign="Center" Style="position: relative">
    <asp:TableRow>
        <asp:TableCell>
            <Generic:VCmdOK ID="CtlOK" runat="server" Height="22px" Width="70px" HorizontalAlign="Right" CadreStyle="margin-top: 3px; margin-right: 3px" />
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="CadreTitre" runat="server" Height="40px" CellPadding="0" CellSpacing="0" HorizontalAlign="Center">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="Etiquette" runat="server" Text="Thèse" Height="20px" Width="200px" BackColor="#2FA49B" BorderColor="#B0E0D7" BorderStyle="Groove" BorderWidth="2px" ForeColor="#D7FAF3" Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Small" Style="margin-top: 0px; margin-left: 4px; margin-bottom: 0px; font-style: oblique; text-align: center;" />
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="CadreDon" runat="server" Height="40px" CellPadding="0" Width="600px"
                CellSpacing="0" HorizontalAlign="Left">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="InfoV01" runat="server" DonTextMode="true" V_PointdeVue="1" V_Objet="87" V_Information="1" V_SiDonneeDico="true" EtiWidth="586px" DonWidth="584px" DonTabIndex="1" DonHeight="120px" EtiStyle="margin-left: 5px; text-align: center" Donstyle="margin-left: 5px;" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="3px" />
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        <Virtualia:VCoupleEtiDonnee ID="InfoH02" runat="server" V_PointdeVue="1" V_Objet="87" V_Information="2" V_SiDonneeDico="true" EtiWidth="180px" DonWidth="398px" DonTabIndex="2" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        <Virtualia:VCoupleEtiDonnee ID="InfoH03" runat="server" V_PointdeVue="1" V_Objet="87" V_Information="3" V_SiDonneeDico="true" EtiWidth="180px" DonWidth="398px" DonTabIndex="3" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="10px" />
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        <Virtualia:VCoupleEtiDonnee ID="InfoH04" runat="server" V_PointdeVue="1" V_Objet="87" V_Information="4" V_SiDonneeDico="true" EtiWidth="125px" DonWidth="350px" DonTabIndex="4" Etistyle="margin-left: 59px;" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="2px" />
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="InfoV05" runat="server" V_PointdeVue="1" V_Objet="87" V_Information="5" V_SiDonneeDico="true" EtiWidth="351px" DonWidth="349px" DonTabIndex="5" DonHeight="90px" EtiStyle="text-align: center; margin-left: 191px;" Donstyle="margin-left: 191px;" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="10px" />
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        <Virtualia:VDuoEtiquetteCommande ID="Dontab06" runat="server" V_PointdeVue="1" V_Objet="87" V_Information="6" V_SiDonneeDico="true" EtiWidth="180px" DonWidth="355px" DonTabIndex="6" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        <Virtualia:VDuoEtiquetteCommande ID="Dontab07" runat="server" V_PointdeVue="1" V_Objet="87" V_Information="7" V_SiDonneeDico="true" EtiWidth="180px" DonWidth="355px" DonTabIndex="7" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="10px" />
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        <Virtualia:VCoupleEtiDonnee ID="InfoH08" runat="server" V_PointdeVue="1" V_Objet="87" V_Information="8" V_SiDonneeDico="true" EtiWidth="180px" DonWidth="80px" DonTabIndex="8" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="3px" />
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="InfoV09" runat="server" DonTextMode="true" V_PointdeVue="1" V_Objet="87" V_Information="9" V_SiDonneeDico="true" EtiWidth="586px" DonWidth="584px" DonTabIndex="9" DonHeight="80px" EtiStyle="margin-left: 5px; text-align: center" Donstyle="margin-left: 5px;" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="10px" />
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        <Virtualia:VCoupleEtiDonnee ID="InfoH10" runat="server" V_PointdeVue="1" V_Objet="87" V_Information="10" V_SiDonneeDico="true" EtiWidth="180px" DonWidth="80px" DonTabIndex="10" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        <Virtualia:VDuoEtiquetteCommande ID="Dontab11" runat="server" V_PointdeVue="1" V_Objet="87" V_Information="11" V_SiDonneeDico="true" EtiWidth="180px" DonWidth="355px" DonTabIndex="11" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="12px" />
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>
</asp:Table>
