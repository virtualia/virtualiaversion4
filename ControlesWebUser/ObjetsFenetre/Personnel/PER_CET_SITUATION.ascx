﻿<%@ Control Language="VB" AutoEventWireup="false" Inherits="Virtualia.Net.Fenetre_PER_CET_SITUATION" Codebehind="PER_CET_SITUATION.ascx.vb" %>

 <asp:Table ID="CadreInfo" runat="server" BorderStyle="None" BorderWidth="2px" Visible="true" Height="90px"
    BorderColor="#B0E0D7" Width="500px" HorizontalAlign="Center" style="margin-top: 1px;">
    <asp:TableRow>
        <asp:TableCell HorizontalAlign="Center">
            <asp:Table ID="CadreTitre" runat="server" Height="40px" CellPadding="0" Width="700px" 
            CellSpacing="0" HorizontalAlign="Center">
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Label ID="Etiquette" runat="server" Text="Compte épargne temps" Height="20px" Width="300px"
                            BackColor="#216B68" BorderColor="#B0E0D7" BorderStyle="Groove"
                            BorderWidth="2px" ForeColor="#D7FAF3"
                            Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Small"
                            style="margin-top: 5px; margin-left: 4px; margin-bottom: 10px;
                            font-style: oblique; text-indent: 5px; text-align: center;">
                        </asp:Label>          
                    </asp:TableCell>      
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
      <asp:TableCell HorizontalAlign="Center">
            <asp:Table ID="CadreCETNouveau" runat="server" CellPadding="0" Width="430px" Height="25px"
            CellSpacing="0" HorizontalAlign="Center" BorderStyle="Notset" BorderWidth="1px" Visible="true" BorderColor="#9EB0AC"
            style="margin-top: 1px;margin-bottom: 0px;">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                       <asp:Label ID="LabelTitreCETNouveau" runat="server" Height="25px" Width="430px"
                                BackColor="#CAEBE4" BorderColor="Transparent" BorderStyle="None" Text="Situation du compte (nouveau régime)"
                                BorderWidth="1px" ForeColor="#1C5151" Font-Italic="False"
                                Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Small"
                                style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                font-style: normal; text-indent: 1px; text-align:  center;">
                       </asp:Label>      
                    </asp:TableCell>   
                </asp:TableRow>
            </asp:Table>
      </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
      <asp:TableCell HorizontalAlign="Center">
            <asp:Table ID="TableCETNouveau1" runat="server" CellPadding="0" Width="420px" Height="21px"
            CellSpacing="0" HorizontalAlign="Center" BorderStyle="Notset" BorderWidth="1px" Visible="true" BorderColor="#9EB0AC"
            style="margin-top: -5px;margin-bottom: 0px;">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center" Height="21px" BackColor= "#E2F5F1">
                       <asp:Label ID="LabelOuverture" runat="server" Height="21px" Width="311px"
                                BackColor= "#E2F5F1" BorderColor="#9EB0AC" BorderStyle="None" Text="Ouverture du compte"
                                BorderWidth="1px" ForeColor="#6C9690" Font-Italic="False"
                                Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px; margin-right: 10px;
                                font-style: normal; text-indent: 1px; text-align:  right;">
                       </asp:Label>      
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center" Height="21px">
                       <asp:Label ID="LabelDateOuverture" runat="server" Height="21px" Width="109px"
                                BackColor="White" BorderColor="Transparent" BorderStyle="None" Text="01/01/2008"
                                BorderWidth="1px" ForeColor="#1C5151" Font-Italic="False"
                                Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                font-style: normal; text-indent: 1px; text-align:  center;">
                       </asp:Label>      
                    </asp:TableCell>   
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center" Height="21px" BackColor= "#E2F5F1">
                       <asp:Label ID="LabelTitreNewTotalEpargneCP" runat="server" Height="21px" Width="311px"
                                BackColor= "#E2F5F1" BorderColor="#9EB0AC" BorderStyle="None" Text="Total au titre des congés annuels"
                                BorderWidth="1px" ForeColor="#6C9690" Font-Italic="False"
                                Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px; margin-right: 10px;
                                font-style: normal; text-indent: 1px; text-align:  right;">
                       </asp:Label>      
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center" Height="21px">
                       <asp:Label ID="LabelNewTotalEpargneCP" runat="server" Height="21px" Width="109px"
                                BackColor="White" BorderColor="Transparent" BorderStyle="None" Text="10,5"
                                BorderWidth="1px" ForeColor="#1C5151" Font-Italic="False"
                                Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                font-style: normal; text-indent: 1px; text-align:  center;">
                       </asp:Label>      
                    </asp:TableCell>   
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center" Height="21px" BackColor= "#E2F5F1">
                       <asp:Label ID="LabelTitreNewTotalEpargneRTT" runat="server" Height="21px" Width="311px"
                                BackColor= "#E2F5F1" BorderColor="#9EB0AC" BorderStyle="None" Text="Total au titre de la RTT"
                                BorderWidth="1px" ForeColor="#6C9690" Font-Italic="False"
                                Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px; margin-right: 10px;
                                font-style: normal; text-indent: 1px; text-align:  right;">
                       </asp:Label>      
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center" Height="21px">
                       <asp:Label ID="LabelNewTotalEpargneRTT" runat="server" Height="21px" Width="109px"
                                BackColor="White" BorderColor="Transparent" BorderStyle="None" Text="2,0"
                                BorderWidth="1px" ForeColor="#1C5151" Font-Italic="False"
                                Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                font-style: normal; text-indent: 1px; text-align:  center;">
                       </asp:Label>      
                    </asp:TableCell>   
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center" Height="21px" BackColor= "#E2F5F1">
                       <asp:Label ID="LabelTitreNewTotalEpargneRC" runat="server" Height="21px" Width="311px"
                                BackColor= "#E2F5F1" BorderColor="#9EB0AC" BorderStyle="None" Text="Total au titre du repos compensateur"
                                BorderWidth="1px" ForeColor="#6C9690" Font-Italic="False"
                                Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px; margin-right: 10px;
                                font-style: normal; text-indent: 1px; text-align:  right;">
                       </asp:Label>      
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center" Height="21px">
                       <asp:Label ID="LabelNewTotalEpargneRC" runat="server" Height="21px" Width="109px"
                                BackColor="White" BorderColor="Transparent" BorderStyle="None" Text="0,0"
                                BorderWidth="1px" ForeColor="#1C5151" Font-Italic="False"
                                Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                font-style: normal; text-indent: 1px; text-align:  center;">
                       </asp:Label>      
                    </asp:TableCell>   
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center" Height="21px" BackColor= "#E2F5F1">
                       <asp:Label ID="LabelTitreNewTotalConsomme" runat="server" Height="21px" Width="311px"
                                BackColor= "#E2F5F1" BorderColor="#9EB0AC" BorderStyle="None" Text="Total consommé"
                                BorderWidth="1px" ForeColor="#6C9690" Font-Italic="False"
                                Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px; margin-right: 10px;
                                font-style: normal; text-indent: 1px; text-align:  right;">
                       </asp:Label>      
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center" Height="21px">
                       <asp:Label ID="LabelNewTotalConsomme" runat="server" Height="21px" Width="109px"
                                BackColor="White" BorderColor="Transparent" BorderStyle="None" Text="5,0"
                                BorderWidth="1px" ForeColor="#1C5151" Font-Italic="False"
                                Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                font-style: normal; text-indent: 1px; text-align:  center;">
                       </asp:Label>      
                    </asp:TableCell>   
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center" Height="21px" BackColor= "#E2F5F1">
                       <asp:Label ID="LabelTitreNewTotalIndemnise" runat="server" Height="21px" Width="311px"
                                BackColor= "#E2F5F1" BorderColor="#9EB0AC" BorderStyle="None" Text="Total au titre de l'indemnisation"
                                BorderWidth="1px" ForeColor="#6C9690" Font-Italic="False"
                                Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px; margin-right: 10px;
                                font-style: normal; text-indent: 1px; text-align:  right;">
                       </asp:Label>      
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center" Height="21px">
                       <asp:Label ID="LabelNewTotalIndemnise" runat="server" Height="21px" Width="109px"
                                BackColor="White" BorderColor="Transparent" BorderStyle="None" Text="4,0"
                                BorderWidth="1px" ForeColor="#1C5151" Font-Italic="False"
                                Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                font-style: normal; text-indent: 1px; text-align:  center;">
                       </asp:Label>      
                    </asp:TableCell>   
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center" Height="21px" BackColor= "#E2F5F1">
                       <asp:Label ID="LabelTitreNewTotalRAFP" runat="server" Height="21px" Width="311px"
                                BackColor= "#E2F5F1" BorderColor="#9EB0AC" BorderStyle="None" Text="Total au titre de la retraite additionnelle"
                                BorderWidth="1px" ForeColor="#6C9690" Font-Italic="False"
                                Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px; margin-right: 10px;
                                font-style: normal; text-indent: 1px; text-align:  right;">
                       </asp:Label>      
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center" Height="21px">
                       <asp:Label ID="LabelNewTotalRAFP" runat="server" Height="21px" Width="109px"
                                BackColor="White" BorderColor="Transparent" BorderStyle="None" Text="1,0"
                                BorderWidth="1px" ForeColor="#1C5151" Font-Italic="False"
                                Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                font-style: normal; text-indent: 1px; text-align:  center;">
                       </asp:Label>      
                    </asp:TableCell>   
                </asp:TableRow>
            </asp:Table>
      </asp:TableCell>
    </asp:TableRow>    
    <asp:TableRow>
      <asp:TableCell HorizontalAlign="Center">
            <asp:Table ID="TableCETNouveau2" runat="server" CellPadding="0" Width="430px" Height="21px"
            CellSpacing="0" HorizontalAlign="Center" BorderStyle="Notset" BorderWidth="1px" Visible="true" BorderColor="#9EB0AC"
            style="margin-top: -5px;margin-bottom: 0px;">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center" Height="21px" BackColor= "#9EB0AC">
                       <asp:Label ID="LabelTitreNewSolde" runat="server" Height="21px" Width="311px"
                                BackColor= "#9EB0AC" BorderColor="#9EB0AC" BorderStyle="None" Text="Solde"
                                BorderWidth="1px" ForeColor="#E2F5F1" Font-Italic="False"
                                Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Small"
                                style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px; margin-right: 10px;
                                font-style: normal; text-indent: 1px; text-align:  right;">
                       </asp:Label>      
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center" Height="21px" BackColor="#E2F5F1">
                       <asp:Label ID="LabelNewSolde" runat="server" Height="21px" Width="109px"
                                BackColor="#E2F5F1" BorderColor="Transparent" BorderStyle="None" Text="2,5"
                                BorderWidth="1px" ForeColor="#1C5151" Font-Italic="False"
                                Font-Bold="true" Font-Names="Trebuchet MS" Font-Size="Small"
                                style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                font-style: normal; text-indent: 1px; text-align:  center;">
                       </asp:Label>      
                    </asp:TableCell>   
                </asp:TableRow>
            </asp:Table>
      </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
      <asp:TableCell Height="10px"></asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
      <asp:TableCell HorizontalAlign="Center">
            <asp:Table ID="TableCETNouveau3" runat="server" CellPadding="0" Width="630px" Height="21px"
            CellSpacing="0" HorizontalAlign="Center" BorderStyle="Notset" BorderWidth="1px" Visible="true" BorderColor="#9EB0AC"
            style="margin-top: -5px;margin-bottom: 0px;">
                <asp:TableRow>
                  <asp:TableCell HorizontalAlign="Center" ColumnSpan="5">
                       <asp:Label ID="LabelEpargneNouveauCET" runat="server" Height="21px" Width="630px"
                                BackColor="#CAEBE4" BorderColor="Transparent" BorderStyle="None" Text="Epargne annuelle"
                                BorderWidth="1px" ForeColor="#6C9690" Font-Italic="False"
                                Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Small"
                                style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                font-style: normal; text-indent: 1px; text-align:  center;">
                       </asp:Label>      
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center" Height="21px" BackColor= "#DBF5EF">
                       <asp:Label ID="LabelTitreNewPeriode" runat="server" Height="21px" Width="230px"
                                BackColor= "#9EB0AC" BorderColor="#9EB0AC" BorderStyle="None" Text="Période"
                                BorderWidth="1px" ForeColor="#E2F5F1" Font-Italic="False"
                                Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                font-style: normal; text-indent: 1px; text-align:  center;">
                       </asp:Label>      
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center" Height="21px" BackColor= "#DBF5EF">
                       <asp:Label ID="LabelTitreNewCP" runat="server" Height="21px" Width="99px"
                                BackColor= "#9EB0AC" BorderColor="#9EB0AC" BorderStyle="None" Text="Congés annuels"
                                BorderWidth="1px" ForeColor="#E2F5F1" Font-Italic="False"
                                Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                font-style: normal; text-indent: 1px; text-align:  center;">
                       </asp:Label>      
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center" Height="21px" BackColor= "#DBF5EF">
                       <asp:Label ID="LabelTitreNewRTT" runat="server" Height="21px" Width="99px"
                                BackColor= "#9EB0AC" BorderColor="#9EB0AC" BorderStyle="None" Text="RTT"
                                BorderWidth="1px" ForeColor="#E2F5F1" Font-Italic="False"
                                Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                font-style: normal; text-indent: 1px; text-align:  center;">
                       </asp:Label>      
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center" Height="21px" BackColor= "#DBF5EF">
                       <asp:Label ID="LabelTitreNewRC" runat="server" Height="21px" Width="99px"
                                BackColor= "#9EB0AC" BorderColor="#9EB0AC" BorderStyle="None" Text="R.C."
                                BorderWidth="1px" ForeColor="#E2F5F1" Font-Italic="False"
                                Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                font-style: normal; text-indent: 1px; text-align:  center;">
                       </asp:Label>      
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center" Height="21px" BackColor= "#DBF5EF">
                       <asp:Label ID="LabelTitreNewTotal" runat="server" Height="21px" Width="98px"
                                BackColor= "#9EB0AC" BorderColor="#9EB0AC" BorderStyle="None" Text="Total"
                                BorderWidth="1px" ForeColor="#E2F5F1" Font-Italic="False"
                                Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                font-style: normal; text-indent: 1px; text-align:  center;">
                       </asp:Label>      
                    </asp:TableCell>  
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center" Height="21px" BackColor= "#9EB0AC">
                       <asp:Label ID="LabelNewPeriode" runat="server" Height="21px" Width="230px"
                                BackColor= "White" BorderColor="#9EB0AC" BorderStyle="None" Text="01/01/2009 - 31/12/2009"
                                BorderWidth="1px" ForeColor="#1C5151" Font-Italic="False"
                                Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                font-style: normal; text-indent: 1px; text-align:  center;">
                       </asp:Label>      
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center" Height="21px" BackColor= "#9EB0AC">
                       <asp:Label ID="LabelNewCP" runat="server" Height="21px" Width="99px"
                                BackColor= "White" BorderColor="#9EB0AC" BorderStyle="None" Text="4,5"
                                BorderWidth="1px" ForeColor="#1C5151" Font-Italic="False"
                                Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                font-style: normal; text-indent: 1px; text-align:  center;">
                       </asp:Label>      
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center" Height="21px" BackColor= "#9EB0AC">
                       <asp:Label ID="LabelNewRTT" runat="server" Height="21px" Width="99px"
                                BackColor= "White" BorderColor="#9EB0AC" BorderStyle="None" Text="1,5"
                                BorderWidth="1px" ForeColor="#1C5151" Font-Italic="False"
                                Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                font-style: normal; text-indent: 1px; text-align:  center;">
                       </asp:Label>      
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center" Height="21px" BackColor= "#9EB0AC">
                       <asp:Label ID="LabelNewRC" runat="server" Height="21px" Width="99px"
                                BackColor= "White" BorderColor="#9EB0AC" BorderStyle="None" Text="0,0"
                                BorderWidth="1px" ForeColor="#1C5151" Font-Italic="False"
                                Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                font-style: normal; text-indent: 1px; text-align:  center;">
                       </asp:Label>      
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center" Height="21px" BackColor= "#9EB0AC">
                       <asp:Label ID="LabelNewTotal" runat="server" Height="21px" Width="98px"
                                BackColor= "White" BorderColor="#9EB0AC" BorderStyle="None" Text="6,0"
                                BorderWidth="1px" ForeColor="#1C5151" Font-Italic="False"
                                Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                font-style: normal; text-indent: 1px; text-align:  center;">
                       </asp:Label>      
                    </asp:TableCell>  
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center" Height="21px" BackColor= "#9EB0AC">
                       <asp:Label ID="Label1" runat="server" Height="21px" Width="230px"
                                BackColor= "White" BorderColor="#9EB0AC" BorderStyle="None" Text="01/01/2010 - 31/12/2010"
                                BorderWidth="1px" ForeColor="#1C5151" Font-Italic="False"
                                Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                font-style: normal; text-indent: 1px; text-align:  center;">
                       </asp:Label>      
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center" Height="21px" BackColor= "#9EB0AC">
                       <asp:Label ID="Label2" runat="server" Height="21px" Width="99px"
                                BackColor= "White" BorderColor="#9EB0AC" BorderStyle="None" Text="2,5"
                                BorderWidth="1px" ForeColor="#1C5151" Font-Italic="False"
                                Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                font-style: normal; text-indent: 1px; text-align:  center;">
                       </asp:Label>      
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center" Height="21px" BackColor= "#9EB0AC">
                       <asp:Label ID="Label3" runat="server" Height="21px" Width="99px"
                                BackColor= "White" BorderColor="#9EB0AC" BorderStyle="None" Text="2,0"
                                BorderWidth="1px" ForeColor="#1C5151" Font-Italic="False"
                                Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                font-style: normal; text-indent: 1px; text-align:  center;">
                       </asp:Label>      
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center" Height="21px" BackColor= "#9EB0AC">
                       <asp:Label ID="Label4" runat="server" Height="21px" Width="99px"
                                BackColor= "White" BorderColor="#9EB0AC" BorderStyle="None" Text="0,0"
                                BorderWidth="1px" ForeColor="#1C5151" Font-Italic="False"
                                Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                font-style: normal; text-indent: 1px; text-align:  center;">
                       </asp:Label>      
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center" Height="21px" BackColor= "#9EB0AC">
                       <asp:Label ID="Label5" runat="server" Height="21px" Width="98px"
                                BackColor= "White" BorderColor="#9EB0AC" BorderStyle="None" Text="4,5"
                                BorderWidth="1px" ForeColor="#1C5151" Font-Italic="False"
                                Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                font-style: normal; text-indent: 1px; text-align:  center;">
                       </asp:Label>      
                    </asp:TableCell>  
                </asp:TableRow>
            </asp:Table>
      </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
      <asp:TableCell Height="20px"></asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
      <asp:TableCell HorizontalAlign="Center">
            <asp:Table ID="CadreCETAncien" runat="server" CellPadding="0" Width="430px" Height="25px"
            CellSpacing="0" HorizontalAlign="Center" BorderStyle="Notset" BorderWidth="1px" Visible="true" BorderColor="#9EB0AC"
            style="margin-top: 1px;margin-bottom: 0px;">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                       <asp:Label ID="LabelTitreCETAncien" runat="server" Height="25px" Width="430px"
                                BackColor="#CAEBE4" BorderColor="Transparent" BorderStyle="None" Text="Situation du compte fin 2008 (ancien CET)"
                                BorderWidth="1px" ForeColor="#1C5151" Font-Italic="False"
                                Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Small"
                                style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                font-style: normal; text-indent: 1px; text-align:  center;">
                       </asp:Label>      
                    </asp:TableCell>   
                </asp:TableRow>
            </asp:Table>
      </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
      <asp:TableCell HorizontalAlign="Center">
            <asp:Table ID="TableCETAncien1" runat="server" CellPadding="0" Width="420px" Height="21px"
            CellSpacing="0" HorizontalAlign="Center" BorderStyle="Notset" BorderWidth="1px" Visible="true" BorderColor="#9EB0AC"
            style="margin-top: -5px;margin-bottom: 0px;">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center" Height="21px" BackColor= "#E2F5F1">
                       <asp:Label ID="LabelTitreOldTotalEpargne" runat="server" Height="21px" Width="311px"
                                BackColor= "#E2F5F1" BorderColor="#9EB0AC" BorderStyle="None" Text="Total épargné à fin 2008"
                                BorderWidth="1px" ForeColor="#6C9690" Font-Italic="False"
                                Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px; margin-right: 10px;
                                font-style: normal; text-indent: 1px; text-align:  right;">
                       </asp:Label>      
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center" Height="21px">
                       <asp:Label ID="LabelOldTotalEpargne" runat="server" Height="21px" Width="109px"
                                BackColor="White" BorderColor="Transparent" BorderStyle="None" Text="48,0"
                                BorderWidth="1px" ForeColor="#1C5151" Font-Italic="False"
                                Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                font-style: normal; text-indent: 1px; text-align:  center;">
                       </asp:Label>      
                    </asp:TableCell>   
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center" Height="21px" BackColor= "#E2F5F1">
                       <asp:Label ID="LabelTitreOldTotalEpargneCP" runat="server" Height="21px" Width="311px"
                                BackColor= "#E2F5F1" BorderColor="#9EB0AC" BorderStyle="None" Text="Dont au titre des congés"
                                BorderWidth="1px" ForeColor="#6C9690" Font-Italic="False"
                                Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px; margin-right: 10px;
                                font-style: normal; text-indent: 1px; text-align:  right;">
                       </asp:Label>      
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center" Height="21px">
                       <asp:Label ID="LabelOldTotalEpargneCP" runat="server" Height="21px" Width="109px"
                                BackColor="White" BorderColor="Transparent" BorderStyle="None" Text="30,0"
                                BorderWidth="1px" ForeColor="#1C5151" Font-Italic="False"
                                Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                font-style: normal; text-indent: 1px; text-align:  center;">
                       </asp:Label>      
                    </asp:TableCell>   
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center" Height="21px" BackColor= "#E2F5F1">
                       <asp:Label ID="LabelTitreOldTotalEpargnePaye" runat="server" Height="21px" Width="311px"
                                BackColor= "#E2F5F1" BorderColor="#9EB0AC" BorderStyle="None" Text="Dont au titre des jours indemnisés"
                                BorderWidth="1px" ForeColor="#6C9690" Font-Italic="False"
                                Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px; margin-right: 10px;
                                font-style: normal; text-indent: 1px; text-align:  right;">
                       </asp:Label>      
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center" Height="21px">
                       <asp:Label ID="LabelOldTotalEpargnePaye" runat="server" Height="21px" Width="109px"
                                BackColor="White" BorderColor="Transparent" BorderStyle="None" Text="10,0"
                                BorderWidth="1px" ForeColor="#1C5151" Font-Italic="False"
                                Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                font-style: normal; text-indent: 1px; text-align:  center;">
                       </asp:Label>      
                    </asp:TableCell>   
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center" Height="21px" BackColor= "#E2F5F1">
                       <asp:Label ID="LabelTitreOldTotalRAFP" runat="server" Height="21px" Width="311px"
                                BackColor= "#E2F5F1" BorderColor="#9EB0AC" BorderStyle="None" Text="Dont au titre de la RAFP"
                                BorderWidth="1px" ForeColor="#6C9690" Font-Italic="False"
                                Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px; margin-right: 10px;
                                font-style: normal; text-indent: 1px; text-align:  right;">
                       </asp:Label>      
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center" Height="21px">
                       <asp:Label ID="LabelOldTotalRAFP" runat="server" Height="21px" Width="109px"
                                BackColor="White" BorderColor="Transparent" BorderStyle="None" Text="8,0"
                                BorderWidth="1px" ForeColor="#1C5151" Font-Italic="False"
                                Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                font-style: normal; text-indent: 1px; text-align:  center;">
                       </asp:Label>      
                    </asp:TableCell>   
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="1px" ColumnSpan="2" BackColor="#9EB0AC"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center" Height="21px" BackColor= "#E2F5F1">
                       <asp:Label ID="LabelTitreOldCPPris" runat="server" Height="21px" Width="311px"
                                BackColor= "#E2F5F1" BorderColor="#9EB0AC" BorderStyle="None" Text="Congés pris sur ancien CET"
                                BorderWidth="1px" ForeColor="#6C9690" Font-Italic="False"
                                Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px; margin-right: 10px;
                                font-style: normal; text-indent: 1px; text-align:  right;">
                       </asp:Label>      
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center" Height="21px" BackColor="White">
                       <asp:Label ID="LabelOldTotalCPPris" runat="server" Height="21px" Width="109px"
                                BackColor="White" BorderColor="Transparent" BorderStyle="None" Text="14,0"
                                BorderWidth="1px" ForeColor="#1C5151" Font-Italic="False"
                                Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                font-style: normal; text-indent: 1px; text-align:  center;">
                       </asp:Label>      
                    </asp:TableCell>   
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center" Height="21px" BackColor= "#E2F5F1">
                       <asp:Label ID="LabelTitreOldSoldeCP" runat="server" Height="21px" Width="311px"
                                BackColor= "#E2F5F1" BorderColor="#9EB0AC" BorderStyle="None" Text="Solde au titre des congés"
                                BorderWidth="1px" ForeColor="#6C9690" Font-Italic="False"
                                Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px; margin-right: 10px;
                                font-style: normal; text-indent: 1px; text-align:  right;">
                       </asp:Label>      
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center" Height="21px" BackColor="White">
                       <asp:Label ID="LabelOldTotalSoldeCP" runat="server" Height="21px" Width="109px"
                                BackColor="White" BorderColor="Transparent" BorderStyle="None" Text="16,0"
                                BorderWidth="1px" ForeColor="#1C5151" Font-Italic="False"
                                Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                font-style: normal; text-indent: 1px; text-align:  center;">
                       </asp:Label>      
                    </asp:TableCell>   
                </asp:TableRow>
            </asp:Table>
      </asp:TableCell>
    </asp:TableRow>    
    <asp:TableRow>
      <asp:TableCell Height="10px"></asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
      <asp:TableCell HorizontalAlign="Center">
            <asp:Table ID="TableCETAncien3" runat="server" CellPadding="0" Width="630px" Height="21px"
            CellSpacing="0" HorizontalAlign="Center" BorderStyle="Notset" BorderWidth="1px" Visible="true" BorderColor="#9EB0AC"
            style="margin-top: -5px;margin-bottom: 0px;">
                <asp:TableRow>
                  <asp:TableCell HorizontalAlign="Center" ColumnSpan="5">
                       <asp:Label ID="LabelEpargneAncienCET" runat="server" Height="21px" Width="630px"
                                BackColor="#CAEBE4" BorderColor="Transparent" BorderStyle="None" Text="Epargne annuelle"
                                BorderWidth="1px" ForeColor="#6C9690" Font-Italic="False"
                                Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Small"
                                style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                font-style: normal; text-indent: 1px; text-align:  center;">
                       </asp:Label>      
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center" Height="21px" BackColor= "#DBF5EF">
                       <asp:Label ID="LabelTitreOldPeriode" runat="server" Height="21px" Width="230px"
                                BackColor= "#9EB0AC" BorderColor="#9EB0AC" BorderStyle="None" Text="Période"
                                BorderWidth="1px" ForeColor="#E2F5F1" Font-Italic="False"
                                Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                font-style: normal; text-indent: 1px; text-align:  center;">
                       </asp:Label>      
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center" Height="21px" BackColor= "#DBF5EF">
                       <asp:Label ID="LabelTitreOldCP" runat="server" Height="21px" Width="99px"
                                BackColor= "#9EB0AC" BorderColor="#9EB0AC" BorderStyle="None" Text="Congés annuels"
                                BorderWidth="1px" ForeColor="#E2F5F1" Font-Italic="False"
                                Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                font-style: normal; text-indent: 1px; text-align:  center;">
                       </asp:Label>      
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center" Height="21px" BackColor= "#DBF5EF">
                       <asp:Label ID="LabelTitreOldRTT" runat="server" Height="21px" Width="99px"
                                BackColor= "#9EB0AC" BorderColor="#9EB0AC" BorderStyle="None" Text="RTT"
                                BorderWidth="1px" ForeColor="#E2F5F1" Font-Italic="False"
                                Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                font-style: normal; text-indent: 1px; text-align:  center;">
                       </asp:Label>      
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center" Height="21px" BackColor= "#DBF5EF">
                       <asp:Label ID="LabelTitreOldRC" runat="server" Height="21px" Width="99px"
                                BackColor= "#9EB0AC" BorderColor="#9EB0AC" BorderStyle="None" Text="R.C."
                                BorderWidth="1px" ForeColor="#E2F5F1" Font-Italic="False"
                                Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                font-style: normal; text-indent: 1px; text-align:  center;">
                       </asp:Label>      
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center" Height="21px" BackColor= "#DBF5EF">
                       <asp:Label ID="LabelTitreOldTotal" runat="server" Height="21px" Width="98px"
                                BackColor= "#9EB0AC" BorderColor="#9EB0AC" BorderStyle="None" Text="Total"
                                BorderWidth="1px" ForeColor="#E2F5F1" Font-Italic="False"
                                Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                font-style: normal; text-indent: 1px; text-align:  center;">
                       </asp:Label>      
                    </asp:TableCell>  
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center" Height="21px" BackColor= "#9EB0AC">
                       <asp:Label ID="LabelOldPeriode" runat="server" Height="21px" Width="230px"
                                BackColor= "White" BorderColor="#9EB0AC" BorderStyle="None" Text="01/01/2006 - 31/12/2006"
                                BorderWidth="1px" ForeColor="#1C5151" Font-Italic="False"
                                Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                font-style: normal; text-indent: 1px; text-align:  center;">
                       </asp:Label>      
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center" Height="21px" BackColor= "#9EB0AC">
                       <asp:Label ID="LabelOldCP" runat="server" Height="21px" Width="99px"
                                BackColor= "White" BorderColor="#9EB0AC" BorderStyle="None" Text="7,0"
                                BorderWidth="1px" ForeColor="#1C5151" Font-Italic="False"
                                Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                font-style: normal; text-indent: 1px; text-align:  center;">
                       </asp:Label>      
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center" Height="21px" BackColor= "#9EB0AC">
                       <asp:Label ID="LabelOldRTT" runat="server" Height="21px" Width="99px"
                                BackColor= "White" BorderColor="#9EB0AC" BorderStyle="None" Text="1,0"
                                BorderWidth="1px" ForeColor="#1C5151" Font-Italic="False"
                                Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                font-style: normal; text-indent: 1px; text-align:  center;">
                       </asp:Label>      
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center" Height="21px" BackColor= "#9EB0AC">
                       <asp:Label ID="LabelOldRC" runat="server" Height="21px" Width="99px"
                                BackColor= "White" BorderColor="#9EB0AC" BorderStyle="None" Text="0,0"
                                BorderWidth="1px" ForeColor="#1C5151" Font-Italic="False"
                                Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                font-style: normal; text-indent: 1px; text-align:  center;">
                       </asp:Label>      
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center" Height="21px" BackColor= "#9EB0AC">
                       <asp:Label ID="LabelOldTotal" runat="server" Height="21px" Width="98px"
                                BackColor= "White" BorderColor="#9EB0AC" BorderStyle="None" Text="8,0"
                                BorderWidth="1px" ForeColor="#1C5151" Font-Italic="False"
                                Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                font-style: normal; text-indent: 1px; text-align:  center;">
                       </asp:Label>      
                    </asp:TableCell>  
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center" Height="21px" BackColor= "#9EB0AC">
                       <asp:Label ID="Label6" runat="server" Height="21px" Width="230px"
                                BackColor= "White" BorderColor="#9EB0AC" BorderStyle="None" Text="01/01/2007 - 31/12/2007"
                                BorderWidth="1px" ForeColor="#1C5151" Font-Italic="False"
                                Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                font-style: normal; text-indent: 1px; text-align:  center;">
                       </asp:Label>      
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center" Height="21px" BackColor= "#9EB0AC">
                       <asp:Label ID="Label7" runat="server" Height="21px" Width="99px"
                                BackColor= "White" BorderColor="#9EB0AC" BorderStyle="None" Text="10,0"
                                BorderWidth="1px" ForeColor="#1C5151" Font-Italic="False"
                                Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                font-style: normal; text-indent: 1px; text-align:  center;">
                       </asp:Label>      
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center" Height="21px" BackColor= "#9EB0AC">
                       <asp:Label ID="Label8" runat="server" Height="21px" Width="99px"
                                BackColor= "White" BorderColor="#9EB0AC" BorderStyle="None" Text="8,0"
                                BorderWidth="1px" ForeColor="#1C5151" Font-Italic="False"
                                Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                font-style: normal; text-indent: 1px; text-align:  center;">
                       </asp:Label>      
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center" Height="21px" BackColor= "#9EB0AC">
                       <asp:Label ID="Label9" runat="server" Height="21px" Width="99px"
                                BackColor= "White" BorderColor="#9EB0AC" BorderStyle="None" Text="0,0"
                                BorderWidth="1px" ForeColor="#1C5151" Font-Italic="False"
                                Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                font-style: normal; text-indent: 1px; text-align:  center;">
                       </asp:Label>      
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center" Height="21px" BackColor= "#9EB0AC">
                       <asp:Label ID="Label10" runat="server" Height="21px" Width="98px"
                                BackColor= "White" BorderColor="#9EB0AC" BorderStyle="None" Text="18,0"
                                BorderWidth="1px" ForeColor="#1C5151" Font-Italic="False"
                                Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                font-style: normal; text-indent: 1px; text-align:  center;">
                       </asp:Label>      
                    </asp:TableCell>  
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center" Height="21px" BackColor= "#9EB0AC">
                       <asp:Label ID="Label11" runat="server" Height="21px" Width="230px"
                                BackColor= "White" BorderColor="#9EB0AC" BorderStyle="None" Text="01/01/2008 - 31/12/2008"
                                BorderWidth="1px" ForeColor="#1C5151" Font-Italic="False"
                                Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                style="margin-top: 1px; margin-left: -1px; margin-bottom: 0px;
                                font-style: normal; text-indent: 1px; text-align:  center;">
                       </asp:Label>      
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center" Height="21px" BackColor= "#9EB0AC">
                       <asp:Label ID="Label12" runat="server" Height="21px" Width="99px"
                                BackColor= "White" BorderColor="#9EB0AC" BorderStyle="None" Text="15,0"
                                BorderWidth="1px" ForeColor="#1C5151" Font-Italic="False"
                                Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                font-style: normal; text-indent: 1px; text-align:  center;">
                       </asp:Label>      
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center" Height="21px" BackColor= "#9EB0AC">
                       <asp:Label ID="Label13" runat="server" Height="21px" Width="99px"
                                BackColor= "White" BorderColor="#9EB0AC" BorderStyle="None" Text="7,0"
                                BorderWidth="1px" ForeColor="#1C5151" Font-Italic="False"
                                Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                font-style: normal; text-indent: 1px; text-align:  center;">
                       </asp:Label>      
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center" Height="21px" BackColor= "#9EB0AC">
                       <asp:Label ID="Label14" runat="server" Height="21px" Width="99px"
                                BackColor= "White" BorderColor="#9EB0AC" BorderStyle="None" Text="0,0"
                                BorderWidth="1px" ForeColor="#1C5151" Font-Italic="False"
                                Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                font-style: normal; text-indent: 1px; text-align:  center;">
                       </asp:Label>      
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center" Height="21px" BackColor= "#9EB0AC">
                       <asp:Label ID="Label15" runat="server" Height="21px" Width="98px"
                                BackColor= "White" BorderColor="#9EB0AC" BorderStyle="None" Text="22,0"
                                BorderWidth="1px" ForeColor="#1C5151" Font-Italic="False"
                                Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                font-style: normal; text-indent: 1px; text-align:  center;">
                       </asp:Label>      
                    </asp:TableCell>  
                </asp:TableRow>
            </asp:Table>
      </asp:TableCell>
    </asp:TableRow>
 </asp:Table>