﻿<%@ Control Language="VB" AutoEventWireup="false" Inherits="Virtualia.Net.Fenetre_PER_TRAVAIL" CodeBehind="PER_TRAVAIL.ascx.vb" %>

<%@ Register Src="~/Controles/Saisies/VCoupleEtiDonnee.ascx" TagName="VCoupleEtiDonnee" TagPrefix="Virtualia" %>
<%@ Register Src="~/Controles/Saisies/VDuoEtiquetteCommande.ascx" TagName="VDuoEtiquetteCommande" TagPrefix="Virtualia" %>
<%@ Register Src="~/Controles/Saisies/VCocheSimple.ascx" TagName="VCocheSimple" TagPrefix="Virtualia" %>
<%@ Register Src="~/Controles/Saisies/VTrioHorizontalRadio.ascx" TagName="VTrioHorizontalRadio" TagPrefix="Virtualia" %>
<%@ Register Src="~/Controles/Saisies/VCoupleVerticalEtiDonnee.ascx" TagName="VCoupleVerticalEtiDonnee" TagPrefix="Virtualia" %>

<%@ Register Src="~/ControlesWebUser/TempsTravail/VCycledeBase.ascx" TagName="VCycledeBase" TagPrefix="Virtualia" %>

<%@ Register Src="~/ControlsGeneriques/V_DataGrid.ascx" TagName="VDataGrid" TagPrefix="Generic" %>
<%@ Register Src="~/ControlsGeneriques/V_CommandeCRUD.ascx" TagName="VCrud" TagPrefix="Generic" %>

<asp:Table ID="CadreControle" runat="server" Height="30px" Width="650px" CellPadding="0" CellSpacing="0" HorizontalAlign="Center">
    <asp:TableRow>
        <asp:TableCell>
            <Generic:VDataGrid ID="ListeGrille" runat="server" CadreWidth="650px" SiColonneSelect="true" SiCaseAcocher="false" />
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="CadreInfo" runat="server" BorderStyle="Ridge" BorderWidth="2px" Visible="true" BorderColor="#B0E0D7" Width="730px" HorizontalAlign="Center" Style="margin-top: 3px;">
                <asp:TableRow>
                    <asp:TableCell>
                        <Generic:VCrud ID="CtlCrud" runat="server" Height="22px" Width="244px" HorizontalAlign="Right" CadreStyle="margin-top: 3px; margin-right: 3px" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Table ID="CadreTitre" runat="server" Height="40px" CellPadding="0" Width="730px" CellSpacing="0" HorizontalAlign="Center">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                    <asp:Label ID="Etiquette" runat="server" Text="Temps de travail prévisionnel" Height="20px" Width="350px" BackColor="#2FA49B" BorderColor="#B0E0D7" BorderStyle="Groove" BorderWidth="2px" ForeColor="#D7FAF3" Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Small" Style="margin-top: 5px; margin-left: 4px; margin-bottom: 12px; font-style: oblique; text-indent: 5px; text-align: center;" />
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Table ID="CadreDon" runat="server" CellPadding="0" CellSpacing="0" Width="430px" HorizontalAlign="Center">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH00" runat="server" V_PointdeVue="1" V_Objet="33" V_Information="0" V_SiDonneeDico="true" EtiWidth="200px" DonWidth="80px" DonTabIndex="1" />
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Center">
                                    <asp:Label ID="EtiDateEffet" runat="server" Height="20px" Width="250px" Text="" BackColor="#B0E0D7" BorderColor="#B0E0D7" BorderStyle="Outset" BorderWidth="2px" ForeColor="#142425" Font-Italic="true" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Style="text-indent: 1px; text-align: center; margin-left: 10px;" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Height="10px" ColumnSpan="2" />
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH16" runat="server" V_PointdeVue="1" V_Objet="33" V_Information="16" V_SiDonneeDico="true" EtiWidth="200px" DonWidth="80px" DonTabIndex="2" />
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Center">
                                    <Virtualia:VCocheSimple ID="Coche18" runat="server" V_TabIndex="3" V_PointdeVue="1" V_Objet="33" V_Information="18" V_SiDonneeDico="true" V_Width="250px" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Height="2px" ColumnSpan="2" />
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center" ColumnSpan="2">
                                    <Virtualia:VTrioHorizontalRadio ID="RadioH19" runat="server" V_Groupe="TypeHoraire" V_PointdeVue="1" V_Objet="33" V_Information="19" V_SiDonneeDico="true" RadioGaucheWidth="140px" RadioCentreWidth="140px" RadioDroiteWidth="140px" RadioGaucheHeight="23px" RadioCentreHeight="23px" RadioDroiteHeight="23px" RadioGaucheText="Horaire fixe" RadioCentreText="Horaire variable" RadioDroiteText="Mode mixte" RadioGaucheStyle="margin-left: 4px;" Visible="true" V_TabIndex="4" />
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Table ID="TableauSemaineAdm" runat="server" CellPadding="0" Width="690px" CellSpacing="0" HorizontalAlign="Center">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                    <asp:Label ID="LabelSemaineAdministrative" runat="server" Text="Semaine administrative" Height="20px" Width="690px" BackColor="#216B68" BorderColor="#B0E0D7" BorderStyle="Groove" BorderWidth="2px" ForeColor="#D7FAF3" Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Small" Style="margin-top: 5px; margin-left: 0px; margin-bottom: 1px; font-style: oblique; text-indent: 5px; text-align: center;" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell ID="CadreSemaineAdm" HorizontalAlign="Center" Visible="true" Width="690px" BackColor="#E9FDF9" BorderColor="#CAEBE4" BorderStyle="Inset" BorderWidth="1px">
                                    <asp:Table ID="TableSemaine" runat="server" CellPadding="0" CellSpacing="0" HorizontalAlign="Center" Width="630px">
                                        <asp:TableRow>
                                            <asp:TableCell>
                                                <asp:Label ID="EtiJour" runat="server" Height="23px" Width="105px" Text="" Visible="true" BackColor="Transparent" BorderColor="#CAEBE4" BorderStyle="None" BorderWidth="2px" ForeColor="#1C5151" Font-Italic="true" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Style="margin-top: 1px; margin-left: 0px; text-indent: 0px; text-align: center" />
                                            </asp:TableCell>
                                            <asp:TableCell>
                                                <asp:Label ID="EtiLundi" runat="server" Height="23px" Width="75px" Text="Lundi" BackColor="#CAEBE4" BorderColor="#CAEBE4" BorderStyle="Notset" BorderWidth="2px" ForeColor="#1C5151" Font-Italic="true" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Style="margin-top: 1px; margin-left: 0px; text-indent: 0px; text-align: center; padding-top: 3px;" />
                                            </asp:TableCell>
                                            <asp:TableCell>
                                                <asp:Label ID="EtiMardi" runat="server" Height="23px" Width="75px" Text="Mardi" BackColor="#CAEBE4" BorderColor="#CAEBE4" BorderStyle="Notset" BorderWidth="2px" ForeColor="#1C5151" Font-Italic="true" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Style="margin-top: 1px; margin-left: 0px; text-indent: 0px; text-align: center; padding-top: 3px;" />
                                            </asp:TableCell>
                                            <asp:TableCell>
                                                <asp:Label ID="EtiMercredi" runat="server" Height="23px" Width="75px" Text="Mercredi" BackColor="#CAEBE4" BorderColor="#CAEBE4" BorderStyle="Notset" BorderWidth="2px" ForeColor="#1C5151" Font-Italic="true" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Style="margin-top: 1px; margin-left: 0px; text-indent: 0px; text-align: center; padding-top: 3px;" />
                                            </asp:TableCell>
                                            <asp:TableCell>
                                                <asp:Label ID="EtiJeudi" runat="server" Height="23px" Width="75px" Text="Jeudi" BackColor="#CAEBE4" BorderColor="#CAEBE4" BorderStyle="Notset" BorderWidth="2px" ForeColor="#1C5151" Font-Italic="true" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Style="margin-top: 1px; margin-left: 0px; text-indent: 0px; text-align: center; padding-top: 3px;" />
                                            </asp:TableCell>
                                            <asp:TableCell>
                                                <asp:Label ID="EtiVendredi" runat="server" Height="23px" Width="75px" Text="Vendredi" BackColor="#CAEBE4" BorderColor="#CAEBE4" BorderStyle="Notset" BorderWidth="2px" ForeColor="#1C5151" Font-Italic="true" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Style="margin-top: 1px; margin-left: 0px; text-indent: 0px; text-align: center; padding-top: 3px;" />
                                            </asp:TableCell>
                                            <asp:TableCell>
                                                <asp:Label ID="EtiSamedi" runat="server" Height="23px" Width="75px" Text="Samedi" BackColor="#CAEBE4" BorderColor="#CAEBE4" BorderStyle="Notset" BorderWidth="2px" ForeColor="#1C5151" Font-Italic="true" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Style="margin-top: 1px; margin-left: 0px; text-indent: 0px; text-align: center; padding-top: 3px;" />
                                            </asp:TableCell>
                                            <asp:TableCell>
                                                <asp:Label ID="EtiDimanche" runat="server" Height="23px" Width="75px" Text="Dimanche" BackColor="#CAEBE4" BorderColor="#CAEBE4" BorderStyle="Notset" BorderWidth="2px" ForeColor="#1C5151" Font-Italic="true" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Style="margin-top: 1px; margin-left: 0px; text-indent: 0px; text-align: center; padding-top: 3px;" />
                                            </asp:TableCell>
                                        </asp:TableRow>
                                        <asp:TableRow>
                                            <asp:TableCell>
                                                <asp:Label ID="EtiMatin" runat="server" Height="25px" Width="105px" Text="Matin" Visible="true" BackColor="#CAEBE4" BorderColor="#CAEBE4" BorderStyle="Notset" BorderWidth="2px" ForeColor="#1C5151" Font-Italic="true" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Style="margin-top: 0px; margin-left: 2px; text-indent: 0px; text-align: center; padding-top: 5px;" />
                                            </asp:TableCell>
                                            <asp:TableCell>
                                                <Virtualia:VCocheSimple ID="Coche01" runat="server" V_TabIndex="5" V_Text=" " V_BackColor="#CAEBE4" V_BorderColor="#7EB8CE" V_BorderStyle="Notset" V_BorderWidth="2px" V_PointdeVue="1" V_Objet="33" V_Information="1" V_SiDonneeDico="true" V_style="margin-top: 0px; margin-left: 0px; text-indent: 30px; padding-top: 5px;" V_Width="75px" V_Height="25px" />
                                            </asp:TableCell>
                                            <asp:TableCell>
                                                <Virtualia:VCocheSimple ID="Coche03" runat="server" V_TabIndex="6" V_Text=" " V_BackColor="#CAEBE4" V_BorderColor="#7EB8CE" V_BorderStyle="Notset" V_BorderWidth="2px" V_PointdeVue="1" V_Objet="33" V_Information="3" V_SiDonneeDico="true" V_style="margin-top: 0px; margin-left: 0px; text-indent: 30px; padding-top: 5px;" V_Width="75px" V_Height="25px" />
                                            </asp:TableCell>
                                            <asp:TableCell>
                                                <Virtualia:VCocheSimple ID="Coche05" runat="server" V_TabIndex="7" V_Text=" " V_BackColor="#CAEBE4" V_BorderColor="#7EB8CE" V_BorderStyle="Notset" V_BorderWidth="2px" V_PointdeVue="1" V_Objet="33" V_Information="5" V_SiDonneeDico="true" V_style="margin-top: 0px; margin-left: 0px; text-indent: 30px; padding-top: 5px;" V_Width="75px" V_Height="25px" />
                                            </asp:TableCell>
                                            <asp:TableCell>
                                                <Virtualia:VCocheSimple ID="Coche07" runat="server" V_TabIndex="8" V_Text=" " V_BackColor="#CAEBE4" V_BorderColor="#7EB8CE" V_BorderStyle="Notset" V_BorderWidth="2px" V_PointdeVue="1" V_Objet="33" V_Information="7" V_SiDonneeDico="true" V_style="margin-top: 0px; margin-left: 0px; text-indent: 30px; padding-top: 5px;" V_Width="75px" V_Height="25px" />
                                            </asp:TableCell>
                                            <asp:TableCell>
                                                <Virtualia:VCocheSimple ID="Coche09" runat="server" V_TabIndex="9" V_Text=" " V_BackColor="#CAEBE4" V_BorderColor="#7EB8CE" V_BorderStyle="Notset" V_BorderWidth="2px" V_PointdeVue="1" V_Objet="33" V_Information="9" V_SiDonneeDico="true" V_style="margin-top: 0px; margin-left: 0px; text-indent: 30px; padding-top: 5px;" V_Width="75px" V_Height="25px" />
                                            </asp:TableCell>
                                            <asp:TableCell>
                                                <Virtualia:VCocheSimple ID="Coche11" runat="server" V_TabIndex="10" V_Text=" " V_BackColor="#CAEBE4" V_BorderColor="#7EB8CE" V_BorderStyle="Notset" V_BorderWidth="2px" V_PointdeVue="1" V_Objet="33" V_Information="11" V_SiDonneeDico="true" V_style="margin-top: 0px; margin-left: 0px; text-indent: 30px; padding-top: 5px;" V_Width="75px" V_Height="25px" />
                                            </asp:TableCell>
                                            <asp:TableCell>
                                                <Virtualia:VCocheSimple ID="Coche13" runat="server" V_TabIndex="11" V_Text=" " V_BackColor="#CAEBE4" V_BorderColor="#7EB8CE" V_BorderStyle="Notset" V_BorderWidth="2px" V_PointdeVue="1" V_Objet="33" V_Information="13" V_SiDonneeDico="true" V_style="margin-top: 0px; margin-left: 0px; text-indent: 30px; padding-top: 5px;" V_Width="75px" V_Height="25px" />
                                            </asp:TableCell>
                                        </asp:TableRow>
                                        <asp:TableRow>
                                            <asp:TableCell>
                                                <asp:Label ID="EtiApresMidi" runat="server" Height="25px" Width="105px" Text="Après-midi" Visible="true" BackColor="#CAEBE4" BorderColor="#CAEBE4" BorderStyle="Notset" BorderWidth="2px" ForeColor="#1C5151" Font-Italic="true" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Style="margin-top: 0px; margin-left: 2px; text-indent: 0px; text-align: center; padding-top: 5px;" />
                                            </asp:TableCell>
                                            <asp:TableCell>
                                                <Virtualia:VCocheSimple ID="Coche02" runat="server" V_TabIndex="12" V_Text=" " V_BackColor="#CAEBE4" V_BorderColor="#7EB8CE" V_BorderStyle="Notset" V_BorderWidth="2px" V_PointdeVue="1" V_Objet="33" V_Information="2" V_SiDonneeDico="true" V_style="margin-top: 0px; margin-left: 0px; text-indent: 30px; padding-top: 5px;" V_Width="75px" V_Height="25px" />
                                            </asp:TableCell>
                                            <asp:TableCell>
                                                <Virtualia:VCocheSimple ID="Coche04" runat="server" V_TabIndex="13" V_Text=" " V_BackColor="#CAEBE4" V_BorderColor="#7EB8CE" V_BorderStyle="Notset" V_BorderWidth="2px" V_PointdeVue="1" V_Objet="33" V_Information="4" V_SiDonneeDico="true" V_style="margin-top: 0px; margin-left: 0px; text-indent: 30px; padding-top: 5px;" V_Width="75px" V_Height="25px" />
                                            </asp:TableCell>
                                            <asp:TableCell>
                                                <Virtualia:VCocheSimple ID="Coche06" runat="server" V_TabIndex="14" V_Text=" " V_BackColor="#CAEBE4" V_BorderColor="#7EB8CE" V_BorderStyle="Notset" V_BorderWidth="2px" V_PointdeVue="1" V_Objet="33" V_Information="6" V_SiDonneeDico="true" V_style="margin-top: 0px; margin-left: 0px; text-indent: 30px; padding-top: 5px;" V_Width="75px" V_Height="25px" />
                                            </asp:TableCell>
                                            <asp:TableCell>
                                                <Virtualia:VCocheSimple ID="Coche08" runat="server" V_TabIndex="15" V_Text=" " V_BackColor="#CAEBE4" V_BorderColor="#7EB8CE" V_BorderStyle="Notset" V_BorderWidth="2px" V_PointdeVue="1" V_Objet="33" V_Information="8" V_SiDonneeDico="true" V_style="margin-top: 0px; margin-left: 0px; text-indent: 30px; padding-top: 5px;" V_Width="75px" V_Height="25px" />
                                            </asp:TableCell>
                                            <asp:TableCell>
                                                <Virtualia:VCocheSimple ID="Coche10" runat="server" V_TabIndex="16" V_Text=" " V_BackColor="#CAEBE4" V_BorderColor="#7EB8CE" V_BorderStyle="Notset" V_BorderWidth="2px" V_PointdeVue="1" V_Objet="33" V_Information="10" V_SiDonneeDico="true" V_style="margin-top: 0px; margin-left: 0px; text-indent: 30px; padding-top: 5px;" V_Width="75px" V_Height="25px" />
                                            </asp:TableCell>
                                            <asp:TableCell>
                                                <Virtualia:VCocheSimple ID="Coche12" runat="server" V_TabIndex="17" V_Text=" " V_BackColor="#CAEBE4" V_BorderColor="#7EB8CE" V_BorderStyle="Notset" V_BorderWidth="2px" V_PointdeVue="1" V_Objet="33" V_Information="12" V_SiDonneeDico="true" V_style="margin-top: 0px; margin-left: 0px; text-indent: 30px; padding-top: 5px;" V_Width="75px" V_Height="25px" />
                                            </asp:TableCell>
                                            <asp:TableCell>
                                                <Virtualia:VCocheSimple ID="Coche14" runat="server" V_TabIndex="18" V_Text=" " V_BackColor="#CAEBE4" V_BorderColor="#7EB8CE" V_BorderStyle="Notset" V_BorderWidth="2px" V_PointdeVue="1" V_Objet="33" V_Information="14" V_SiDonneeDico="true" V_style="margin-top: 0px; margin-left: 0px; text-indent: 30px; padding-top: 5px;" V_Width="75px" V_Height="25px" />
                                            </asp:TableCell>
                                        </asp:TableRow>
                                    </asp:Table>
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Table ID="TableauCycleTravail" runat="server" CellPadding="0" Width="690px" CellSpacing="0" HorizontalAlign="Center">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                    <asp:Label ID="LabelCycleTravail" runat="server" Text="Cycle de travail" Height="20px" Width="690px" BackColor="#216B68" BorderColor="#B0E0D7" BorderStyle="Groove" BorderWidth="2px" ForeColor="#D7FAF3" Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Small" Style="margin-top: 5px; margin-left: 0px; margin-bottom: 1px; font-style: oblique; text-indent: 5px; text-align: center;" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell ID="CadreCycleTravail" HorizontalAlign="Center" Visible="true" Width="690px" Height="160px" BackColor="#E9FDF9" BorderColor="#CAEBE4" BorderStyle="Inset" BorderWidth="1px">
                                    <asp:Table ID="TableauCycle" runat="server" CellPadding="0" Width="690px" CellSpacing="0" HorizontalAlign="Center">
                                        <asp:TableRow>
                                            <asp:TableCell Height="30px" HorizontalAlign="Center">
                                                <Virtualia:VCycledeBase ID="VCycle_1" runat="server" />
                                            </asp:TableCell>
                                            <asp:TableCell Height="30px" HorizontalAlign="Center">
                                                <Virtualia:VCycledeBase ID="VCycle_2" runat="server" />
                                            </asp:TableCell>
                                            <asp:TableCell Height="30px" HorizontalAlign="Center">
                                                <Virtualia:VCycledeBase ID="VCycle_3" runat="server" />
                                            </asp:TableCell>
                                            <asp:TableCell Height="30px" HorizontalAlign="Center">
                                                <Virtualia:VCycledeBase ID="VCycle_4" runat="server" />
                                            </asp:TableCell>
                                        </asp:TableRow>
                                        <asp:TableRow>
                                            <asp:TableCell Height="30px" HorizontalAlign="Center">
                                                <Virtualia:VCycledeBase ID="VCycle_5" runat="server" />
                                            </asp:TableCell>
                                            <asp:TableCell Height="30px" HorizontalAlign="Center">
                                                <Virtualia:VCycledeBase ID="VCycle_6" runat="server" />
                                            </asp:TableCell>
                                            <asp:TableCell Height="30px" HorizontalAlign="Center">
                                                <Virtualia:VCycledeBase ID="VCycle_7" runat="server" />
                                            </asp:TableCell>
                                            <asp:TableCell Height="30px" HorizontalAlign="Center">
                                                <Virtualia:VCycledeBase ID="VCycle_8" runat="server" />
                                            </asp:TableCell>
                                        </asp:TableRow>
                                        <asp:TableRow>
                                            <asp:TableCell HorizontalAlign="Center" ColumnSpan="4">
                                                <Virtualia:VDuoEtiquetteCommande ID="Dontab15" runat="server" V_PointdeVue="1" V_Objet="33" V_Information="15" V_SiDonneeDico="true" EtiWidth="150px" DonWidth="450px" DonTabIndex="20" />
                                            </asp:TableCell>
                                        </asp:TableRow>
                                        <asp:TableRow>
                                            <asp:TableCell Height="10px" ColumnSpan="4" />
                                        </asp:TableRow>
                                    </asp:Table>
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Table ID="TableauQuinzaine" runat="server" CellPadding="0" Width="690px" CellSpacing="0" HorizontalAlign="Center">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                    <asp:Label ID="LabelQuinzaine" runat="server" Text="Quinzaine personnalisée" Height="20px" Width="690px" BackColor="#216B68" BorderColor="#B0E0D7" BorderStyle="Groove" BorderWidth="2px" ForeColor="#D7FAF3" Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Small" Style="margin-top: 5px; margin-left: 0px; margin-bottom: 1px; font-style: oblique; text-indent: 5px; text-align: center;" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell ID="CadreQuinzaine" HorizontalAlign="Center" Visible="true" Width="690px" Height="200px" BackColor="#E9FDF9" BorderColor="#CAEBE4" BorderStyle="Inset" BorderWidth="1px">
                                    <asp:Table ID="TableauQuinzainePerso" runat="server" CellPadding="0" Width="690px" CellSpacing="0" HorizontalAlign="Center">
                                        <asp:TableRow>
                                            <asp:TableCell Height="30px" HorizontalAlign="Center">
                                                <Virtualia:VCycledeBase ID="VCyclePerso_1" runat="server" />
                                            </asp:TableCell>
                                            <asp:TableCell Height="30px" HorizontalAlign="Center">
                                                <Virtualia:VCycledeBase ID="VCyclePerso_2" runat="server" />
                                            </asp:TableCell>
                                        </asp:TableRow>
                                        <asp:TableRow>
                                            <asp:TableCell HorizontalAlign="Center" ColumnSpan="2">
                                                <asp:Label ID="LabelDatesQuinzaine" runat="server" Text=" " Height="20px" Width="690px" BackColor="#CAEBE4" BorderColor="#B0E0D7" BorderStyle="Notset" BorderWidth="2px" ForeColor="#142425" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; font-style: oblique; text-indent: 2px; text-align: center;" />
                                            </asp:TableCell>
                                        </asp:TableRow>
                                        <asp:TableRow>
                                            <asp:TableCell HorizontalAlign="Center" ColumnSpan="2">
                                                <asp:Label ID="LabelInfosQuinzaine" runat="server" Text=" " Height="20px" Width="690px" BackColor="#CAEBE4" BorderColor="#B0E0D7" BorderStyle="Notset" BorderWidth="2px" ForeColor="#142425" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; font-style: oblique; text-indent: 2px; text-align: center;" />
                                            </asp:TableCell>
                                        </asp:TableRow>
                                    </asp:Table>
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Table ID="TableauObservations" runat="server" CellPadding="0" CellSpacing="0" Width="730px">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                    <Virtualia:VCoupleVerticalEtiDonnee ID="InfoV17" runat="server" DonTextMode="true" V_PointdeVue="1" V_Objet="33" V_Information="17" V_SiDonneeDico="true" EtiWidth="650px" DonWidth="648px" DonHeight="100px" DonTabIndex="25" EtiStyle="text-align:center" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Height="15px" />
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>
</asp:Table>
