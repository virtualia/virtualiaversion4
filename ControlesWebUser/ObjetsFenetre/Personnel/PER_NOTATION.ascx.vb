﻿Option Strict On
Option Explicit On
Option Compare Text

Imports VI = Virtualia.Systeme.Constantes
Imports Virtualia.ObjetBase
Imports Virtualia.OutilsVisu
Imports Virtualia.TablesObjet.ShemaPER

Partial Class Fenetre_PER_NOTATION
    Inherits ObjetWebControleBase(Of PER_NOTATION)
    Implements IControlCrud

#Region "IControlCrud"
    Public ReadOnly Property ControlCrud As ICtlCommandeCrud Implements IControlCrud.CtlCrud
        Get
            Return Me.CtlCrud
        End Get
    End Property
#End Region

#Region "ObjetWebControleBase"
    Private _webfonction As WebFonctions
    Protected Overrides ReadOnly Property V_WebFonction As IWebFonctions
        Get
            If (_webfonction Is Nothing) Then
                _webfonction = New WebFonctions(Me)
            End If
            Return _webfonction
        End Get
    End Property

    Protected Overrides Function InitCadreInfo() As WebControl
        Return Me.CadreInfo
    End Function

    Protected Overrides Function InitControlDataGrid() As IControlDataGrid
        Return Me.ListeGrille
    End Function

    Protected Overrides Sub InitCacheIde()

        V_CacheIde.TypeVue = WebControlTypeVue.ListeGrid

        Dim captions As List(Of String) = New List(Of String)()
        captions.Add("Aucune note")
        captions.Add("Une note")
        captions.Add("notes")

        Me.ListeGrille.InitCaptions(captions)

        Dim colonnes As ColonneDataGridCollection = New ColonneDataGridCollection()

        colonnes.Add(HorizontalAlign.Center, TypeData.Date, "Année", 0)
        colonnes.Add(HorizontalAlign.Center, TypeData.Numeric, "note", 1)
        colonnes.Add(HorizontalAlign.Center, TypeData.Numeric, "notateur", 3)
        colonnes.Add(HorizontalAlign.Center, TypeData.Chaine, "Clef", 0, True)

        Me.ListeGrille.InitColonnes(colonnes)

    End Sub

    Protected Overrides Sub OnPreRender(e As EventArgs)
        MyBase.OnPreRender(e)

        CadreInfo.BackColor = _webfonction.CouleurCharte(V_CacheIde.PointDeVue, "Cadre")
        CadreInfo.BorderColor = _webfonction.CouleurCharte(V_CacheIde.PointDeVue, "Bordure")
        Etiquette.BackColor = _webfonction.CouleurCharte(V_CacheIde.PointDeVue, "Titre")
        Etiquette.ForeColor = _webfonction.CouleurCharte(V_CacheIde.PointDeVue, "Police_Claire")
        Etiquette.BorderColor = _webfonction.CouleurCharte(V_CacheIde.PointDeVue, "Bordure")
        LabelCriteres.BackColor = _webfonction.CouleurCharte(V_CacheIde.PointDeVue, "Sous-Titre")
        LabelCriteres.ForeColor = _webfonction.CouleurCharte(V_CacheIde.PointDeVue, "Etiquette_ForeColor")
        LabelCriteres.BorderColor = _webfonction.CouleurCharte(V_CacheIde.PointDeVue, "Bordure")
        LabelDebutLigne1.BackColor = _webfonction.CouleurCharte(V_CacheIde.PointDeVue, "Etiquette_BackColor")
        LabelDebutLigne1.ForeColor = _webfonction.CouleurCharte(V_CacheIde.PointDeVue, "Etiquette_ForeColor")
        LabelDebutLigne1.BorderColor = _webfonction.CouleurCharte(V_CacheIde.PointDeVue, "Bordure")
        LabelDebutLigne2.BackColor = _webfonction.CouleurCharte(V_CacheIde.PointDeVue, "Etiquette_BackColor")
        LabelDebutLigne2.ForeColor = _webfonction.CouleurCharte(V_CacheIde.PointDeVue, "Etiquette_ForeColor")
        LabelDebutLigne2.BorderColor = _webfonction.CouleurCharte(V_CacheIde.PointDeVue, "Bordure")
        LabelDebutLigne3.BackColor = _webfonction.CouleurCharte(V_CacheIde.PointDeVue, "Etiquette_BackColor")
        LabelDebutLigne3.ForeColor = _webfonction.CouleurCharte(V_CacheIde.PointDeVue, "Etiquette_ForeColor")
        LabelDebutLigne3.BorderColor = _webfonction.CouleurCharte(V_CacheIde.PointDeVue, "Bordure")
        LabelDebutLigne4.BackColor = _webfonction.CouleurCharte(V_CacheIde.PointDeVue, "Etiquette_BackColor")
        LabelDebutLigne4.ForeColor = _webfonction.CouleurCharte(V_CacheIde.PointDeVue, "Etiquette_ForeColor")
        LabelDebutLigne4.BorderColor = _webfonction.CouleurCharte(V_CacheIde.PointDeVue, "Bordure")
        LabelDebutLigne5.BackColor = _webfonction.CouleurCharte(V_CacheIde.PointDeVue, "Etiquette_BackColor")
        LabelDebutLigne5.ForeColor = _webfonction.CouleurCharte(V_CacheIde.PointDeVue, "Etiquette_ForeColor")
        LabelDebutLigne5.BorderColor = _webfonction.CouleurCharte(V_CacheIde.PointDeVue, "Bordure")
    End Sub

    Protected Overrides Sub OnInit(e As System.EventArgs)
        MyBase.OnInit(e)

        Dim initwebfonction As IWebFonctions = V_WebFonction
    End Sub
#End Region

End Class
