﻿<%@ Control Language="VB" AutoEventWireup="false" Inherits="Virtualia.Net.Fenetre_PER_HEURESUP" CodeBehind="PER_HEURESUP.ascx.vb" %>

<%@ Register Src="~/Controles/Saisies/VCoupleEtiDonnee.ascx" TagName="VCoupleEtiDonnee" TagPrefix="Virtualia" %>
<%@ Register Src="~/Controles/Saisies/VCoupleVerticalEtiDonnee.ascx" TagName="VCoupleVerticalEtiDonnee" TagPrefix="Virtualia" %>

<%@ Register Src="~/ControlsGeneriques/V_DataGrid.ascx" TagName="VDataGrid" TagPrefix="Generic" %>
<%@ Register Src="~/ControlsGeneriques/V_CommandeCRUD.ascx" TagName="VCrud" TagPrefix="Generic" %>

<asp:Table ID="CadreControle" runat="server" Height="30px" Width="585px" CellPadding="0" CellSpacing="0" HorizontalAlign="Center">
    <asp:TableRow>
        <asp:TableCell>
            <Generic:VDataGrid ID="ListeGrille" runat="server" CadreWidth="585px" SiColonneSelect="true" SiCaseAcocher="false" />
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="CadreInfo" runat="server" BorderStyle="Ridge" BorderWidth="2px" BorderColor="#B0E0D7" Height="510px" Width="585px" HorizontalAlign="Center" Style="margin-top: 3px;">
                <asp:TableRow>
                    <asp:TableCell>
                        <Generic:VCrud ID="CtlCrud" runat="server" Height="22px" Width="244px" HorizontalAlign="Right" CadreStyle="margin-top: 3px; margin-right: 3px" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Table ID="CadreTitre" runat="server" Height="40px" CellPadding="0" Width="585px" CellSpacing="0" HorizontalAlign="Center">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                    <asp:Label ID="Etiquette" runat="server" Text="Heures supplémentaires" Height="20px" Width="300px" BackColor="#2FA49B" BorderColor="#B0E0D7" BorderStyle="Solid" BorderWidth="2px" ForeColor="#D7FAF3" Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Small" Style="margin-top: 5px; margin-left: 4px; margin-bottom: 20px; font-style: oblique; text-indent: 5px; text-align: center;" />
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Table ID="CadreDates" runat="server" CellPadding="0" CellSpacing="0" Width="585px">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left" ColumnSpan="2">
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH00" runat="server" V_PointdeVue="1" V_Objet="61" V_Information="0" V_SiDonneeDico="true" EtiWidth="120px" DonWidth="80px" DonTabIndex="1" />
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Left">
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH16" runat="server" V_PointdeVue="1" V_Objet="61" V_Information="16" V_SiDonneeDico="true" EtiWidth="120px" DonWidth="80px" DonTabIndex="2" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Height="15px" ColumnSpan="2" />
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Table ID="TableauTitreHeuresup" runat="server" CellPadding="0" CellSpacing="0" Width="450px">
                            <asp:TableRow>
                                <asp:TableCell>
                                    <asp:Label ID="EtiHSUP" runat="server" Height="20px" Width="200px" Text="Tranche" BackColor="#B0E0D7" BorderColor="#B0E0D7" BorderStyle="Outset" BorderWidth="2px" ForeColor="#142425" Font-Italic="true" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Style="margin-top: 1px; margin-left: 21px; text-indent: 5px; text-align: center" />
                                </asp:TableCell>
                                <asp:TableCell>
                                    <asp:Label ID="EtiHSUPNombre" runat="server" Height="20px" Width="60px" Text="Nombre" BackColor="#B0E0D7" BorderColor="#B0E0D7" BorderStyle="Outset" BorderWidth="2px" ForeColor="#142425" Font-Italic="true" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Style="margin-top: 1px; margin-left: 1px; text-indent: 1px; text-align: center" />
                                </asp:TableCell>
                                <asp:TableCell>
                                    <asp:Label ID="EtiHSUPTaux" runat="server" Height="20px" Width="60px" Text="Taux" BackColor="#B0E0D7" BorderColor="#B0E0D7" BorderStyle="Outset" BorderWidth="2px" ForeColor="#142425" Font-Italic="true" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Style="margin-top: 1px; margin-left: 1px; text-indent: 1px; text-align: center" />
                                </asp:TableCell>
                                <asp:TableCell>
                                    <asp:Label ID="EtiHSUPMontant" runat="server" Height="20px" Width="60px" Text="Montant" BackColor="#B0E0D7" BorderColor="#B0E0D7" BorderStyle="Outset" BorderWidth="2px" ForeColor="#142425" Font-Italic="true" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Style="margin-top: 1px; margin-left: 1px; text-indent: 1px; text-align: center" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Height="3px" ColumnSpan="4" />
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Table ID="TableauDonHeures" runat="server" CellPadding="0" CellSpacing="0" Width="450px">
                            <asp:TableRow>
                                <asp:TableCell>
                                    <asp:Label ID="LabelTranche1" runat="server" Height="20px" Width="200px" Text="1ère tranche" BackColor="#B0E0D7" BorderColor="#B0E0D7" BorderStyle="Outset" BorderWidth="2px" ForeColor="#142425" Font-Italic="true" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Style="margin-top: 0px; margin-left: 21px; text-indent: 5px; text-align: left" />
                                </asp:TableCell>
                                <asp:TableCell>
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH01" runat="server" V_SiAutoPostBack="true" V_PointdeVue="1" V_Objet="61" V_Information="1" V_SiDonneeDico="true" EtiVisible="false" DonWidth="55px" DonTabIndex="3" DonStyle="margin-left: 3px;" />
                                </asp:TableCell>
                                <asp:TableCell>
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH02" runat="server" V_PointdeVue="1" V_Objet="61" V_Information="2" V_SiDonneeDico="true" EtiVisible="false" DonWidth="55px" DonTabIndex="4" DonStyle="margin-left: 1px;" />
                                </asp:TableCell>
                                <asp:TableCell>
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH03" runat="server" V_PointdeVue="1" V_Objet="61" V_Information="3" V_SiDonneeDico="true" EtiVisible="false" DonWidth="55px" DonTabIndex="5" DonStyle="margin-left: 0px;" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell>
                                    <asp:Label ID="LabelTranche2" runat="server" Height="20px" Width="200px" Text="2ème tranche" BackColor="#B0E0D7" BorderColor="#B0E0D7" BorderStyle="Outset" BorderWidth="2px" ForeColor="#142425" Font-Italic="true" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Style="margin-top: 0px; margin-left: 21px; text-indent: 5px; text-align: left" />
                                </asp:TableCell>
                                <asp:TableCell>
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH04" runat="server" V_SiAutoPostBack="true" V_PointdeVue="1" V_Objet="61" V_Information="4" V_SiDonneeDico="true" EtiVisible="false" DonWidth="55px" DonTabIndex="6" DonStyle="margin-left: 3px;" />
                                </asp:TableCell>
                                <asp:TableCell>
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH05" runat="server" V_PointdeVue="1" V_Objet="61" V_Information="5" V_SiDonneeDico="true" EtiVisible="false" DonWidth="55px" DonTabIndex="7" DonStyle="margin-left: 1px;" />
                                </asp:TableCell>
                                <asp:TableCell>
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH06" runat="server" V_PointdeVue="1" V_Objet="61" V_Information="6" V_SiDonneeDico="true" EtiVisible="false" DonWidth="55px" DonTabIndex="8" DonStyle="margin-left: 0px;" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell>
                                    <asp:Label ID="LabelTranche3" runat="server" Height="20px" Width="200px" Text="3ème tranche" BackColor="#B0E0D7" BorderColor="#B0E0D7" BorderStyle="Outset" BorderWidth="2px" ForeColor="#142425" Font-Italic="true" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Style="margin-top: 0px; margin-left: 21px; text-indent: 5px; text-align: left" />
                                </asp:TableCell>
                                <asp:TableCell>
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH07" runat="server" V_SiAutoPostBack="true" V_PointdeVue="1" V_Objet="61" V_Information="7" V_SiDonneeDico="true" EtiVisible="false" DonWidth="55px" DonTabIndex="9" DonStyle="margin-left: 3px;" />
                                </asp:TableCell>
                                <asp:TableCell>
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH08" runat="server" V_PointdeVue="1" V_Objet="61" V_Information="8" V_SiDonneeDico="true" EtiVisible="false" DonWidth="55px" DonTabIndex="10" DonStyle="margin-left: 1px;" />
                                </asp:TableCell>
                                <asp:TableCell>
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH09" runat="server" V_PointdeVue="1" V_Objet="61" V_Information="9" V_SiDonneeDico="true" EtiVisible="false" DonWidth="55px" DonTabIndex="11" DonStyle="margin-left: 0px;" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell>
                                    <asp:Label ID="LabelTranche4" runat="server" Height="20px" Width="200px" Text="4ème tranche" BackColor="#B0E0D7" BorderColor="#B0E0D7" BorderStyle="Outset" BorderWidth="2px" ForeColor="#142425" Font-Italic="true" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Style="margin-top: 0px; margin-left: 21px; text-indent: 5px; text-align: left" />
                                </asp:TableCell>
                                <asp:TableCell>
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH10" runat="server" V_SiAutoPostBack="true" V_PointdeVue="1" V_Objet="61" V_Information="10" V_SiDonneeDico="true" EtiVisible="false" DonWidth="55px" DonTabIndex="12" DonStyle="margin-left: 3px;" />
                                </asp:TableCell>
                                <asp:TableCell>
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH11" runat="server" V_PointdeVue="1" V_Objet="61" V_Information="11" V_SiDonneeDico="true" EtiVisible="false" DonWidth="55px" DonTabIndex="13" DonStyle="margin-left: 1px;" />
                                </asp:TableCell>
                                <asp:TableCell>
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH12" runat="server" V_PointdeVue="1" V_Objet="61" V_Information="12" V_SiDonneeDico="true" EtiVisible="false" DonWidth="55px" DonTabIndex="14" DonStyle="margin-left: 0px;" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Height="2px" ColumnSpan="4" />
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left" ColumnSpan="4">
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH13" runat="server" V_PointdeVue="1" V_Objet="61" V_Information="13" V_SiDonneeDico="true" EtiStyle="margin-left: 20px;" DonStyle="margin-left: 4px;" EtiWidth="354px" DonWidth="55px" DonTabIndex="15" />
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Table ID="CadreDon" runat="server" CellPadding="0" CellSpacing="0" Width="585px">
                            <asp:TableRow>
                                <asp:TableCell Height="15px" />
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH14" runat="server" V_PointdeVue="1" V_Objet="61" V_Information="14" V_SiDonneeDico="true" EtiWidth="100px" DonWidth="80px" DonTabIndex="16" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Height="5px" />
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <Virtualia:VCoupleVerticalEtiDonnee ID="InfoV15" runat="server" DonTextMode="true" V_PointdeVue="1" V_Objet="61" V_Information="15" V_SiDonneeDico="true" EtiWidth="540px" DonWidth="538px" DonHeight="120px" DonTabIndex="17" EtiStyle="text-align:center; margin-left: 5px;" DonStyle="margin-left: 5px;" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Height="12px" />
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>
</asp:Table>
