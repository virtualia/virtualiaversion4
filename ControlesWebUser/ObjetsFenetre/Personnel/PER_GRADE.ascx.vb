﻿Option Strict On
Option Explicit On
Option Compare Text

Imports VI = Virtualia.Systeme.Constantes
Imports Virtualia.ObjetBase
Imports Virtualia.TablesObjet.ShemaPER
Imports Virtualia.OutilsVisu
Imports Virtualia.Systeme.Evenements

Partial Class Fenetre_PER_GRADE
    Inherits ObjetWebControleBase(Of PER_GRADE)
    Implements IControlCrud

#Region "IControlCrud"
    Public ReadOnly Property ControlCrud As ICtlCommandeCrud Implements IControlCrud.CtlCrud
        Get
            Return Me.CtlCrud
        End Get
    End Property
#End Region

#Region "ObjetWebControleBase"
    Private _webfonction As WebFonctions
    Protected Overrides ReadOnly Property V_WebFonction As IWebFonctions
        Get
            If (_webfonction Is Nothing) Then
                _webfonction = New WebFonctions(Me)
            End If
            Return _webfonction
        End Get
    End Property

    Protected Overrides Function InitCadreInfo() As WebControl
        Return Me.CadreInfo
    End Function

    Protected Overrides Function InitControlDataGrid() As IControlDataGrid
        Return Me.ListeGrille
    End Function

    Protected Overrides Sub InitCacheIde()

        V_CacheIde.TypeVue = WebControlTypeVue.ListeGrid

        Dim captions As List(Of String) = New List(Of String)()
        captions.Add("Aucun grade")
        captions.Add("Un grade")
        captions.Add("grades")

        Me.ListeGrille.InitCaptions(captions)

        Dim colonnes As ColonneDataGridCollection = New ColonneDataGridCollection()

        colonnes.Add(HorizontalAlign.Center, TypeData.Date, "date d'effet", 0)
        colonnes.Add(HorizontalAlign.Center, TypeData.Numeric, "grade", 1)
        colonnes.Add(HorizontalAlign.Center, TypeData.Numeric, "échelon", 2)
        colonnes.Add(HorizontalAlign.Center, TypeData.Numeric, "indice majoré", 4)
        colonnes.Add(HorizontalAlign.Center, TypeData.Chaine, "Clef", 0, True)

        Me.ListeGrille.InitColonnes(colonnes)

    End Sub

    Protected Overrides Sub OnPreRender(e As EventArgs)
        MyBase.OnPreRender(e)

        InitStyleCadre(Me.CadreExpert, _CadreExpertStyle)

        CadreInfo.BackColor = _webfonction.CouleurCharte(V_CacheIde.PointDeVue, "Cadre")
        CadreInfo.BorderColor = _webfonction.CouleurCharte(V_CacheIde.PointDeVue, "Bordure")
        Etiquette.BackColor = _webfonction.CouleurCharte(V_CacheIde.PointDeVue, "Titre")
        Etiquette.ForeColor = _webfonction.CouleurCharte(V_CacheIde.PointDeVue, "Police_Claire")
        Etiquette.BorderColor = _webfonction.CouleurCharte(V_CacheIde.PointDeVue, "Bordure")
        EtiArrete.BackColor = _webfonction.CouleurCharte(V_CacheIde.PointDeVue, "Sous-Titre")
        EtiArrete.ForeColor = _webfonction.CouleurCharte(V_CacheIde.PointDeVue, "Etiquette_ForeColor")
        EtiArrete.BorderColor = _webfonction.CouleurCharte(V_CacheIde.PointDeVue, "Bordure")
        EtiDecret.BackColor = _webfonction.CouleurCharte(V_CacheIde.PointDeVue, "Sous-Titre")
        EtiDecret.ForeColor = _webfonction.CouleurCharte(V_CacheIde.PointDeVue, "Etiquette_ForeColor")
        EtiDecret.BorderColor = _webfonction.CouleurCharte(V_CacheIde.PointDeVue, "Bordure")
    End Sub

    Protected Overrides Sub OnInit(e As System.EventArgs)
        MyBase.OnInit(e)

        Dim initwebfonction As IWebFonctions = V_WebFonction
    End Sub
#End Region

#Region "Specifique"
    Private _CadreExpertStyle As String = ""
    Public WriteOnly Property CadreExpertStyle As String
        Set(ByVal value As String)
            _CadreExpertStyle = value
        End Set
    End Property


    Protected Overrides Sub AppelListeRef(ByVal sender As Object, ByVal e As AppelTableEventArgs)

        If (_ControlleurListRef Is Nothing) Then
            Return
        End If

        Dim VirDonneeTable As Controles_VDuoEtiquetteCommandeBase = DirectCast(sender, Controles_VDuoEtiquetteCommandeBase)

        If (VirDonneeTable.V_Information <> 1) Then
            MyBase.AppelListeRef(sender, e)
            Return
        End If

        Dim DateValeur As String = InfoH00.DonText
        If DateValeur = "" Then
            DateValeur = _webfonction.ViRhDates.DateduJour(False)
        End If
        _webfonction.PointeurContexte.SysRef_Lettre = Dontab01.DonText

        Dim cacheappel As CacheAppelListeRef = New CacheAppelListeRef()
        cacheappel.ControlBase = Me.ID
        cacheappel.ControleAppelant = e.ControleAppelant
        cacheappel.ObjetAppelant = e.ObjetAppelant
        cacheappel.PointdeVueInverse = e.PointdeVueInverse
        cacheappel.NomdelaTable = e.NomdelaTable
        cacheappel.NomVueSource = _ControlleurListRef.NomVue
        cacheappel.DateValeur = DateValeur

        AfficheListeRefAvecControlleur(cacheappel)

    End Sub

    Public Overrides Sub GereRetourAppelTable(idappelant As String, value As String)
        If (V_CacheMaj Is Nothing) Then
            Return
        End If
        If (V_CacheMaj.Count <= 0) Then
            Return
        End If

        Dim VirDonneeTable As Controles_VDuoEtiquetteCommandeBase = TryCast((From ct In _dicoWebControl(TypeWebControl.VDuoEtiquetteCommande) _
                                                                             Where ct.ID.ToLower() = idappelant.ToLower() _
                                                                             Select ct).First(), Controles_VDuoEtiquetteCommandeBase)
        MyBase.GereRetourAppelTable(idappelant, value)

        If VirDonneeTable.V_Information <> 1 Then
            Return
        End If

        Dim Tampon As ArrayList = _webfonction.PointeurContexte.TsTampon

        If (Tampon Is Nothing) Then
            Return
        End If

        Select Case Tampon.Count
            Case Is > 3
                V_CacheMaj(5).valeur = Tampon(1).ToString 'Catégorie
                V_CacheMaj(12).valeur = Tampon(2).ToString 'Filière
                V_CacheMaj(6).valeur = Tampon(3).ToString 'Corps
        End Select
        Select Case Tampon.Count
            Case Is > 7
                V_CacheMaj(7).valeur = Tampon(4).ToString 'Echelle de rémunération
                V_CacheMaj(2).valeur = Tampon(5).ToString 'Echelon
                V_CacheMaj(4).valeur = Tampon(6).ToString 'Majoré
                V_CacheMaj(3).valeur = Tampon(7).ToString 'Brut
        End Select

    End Sub
#End Region

End Class
