﻿Option Strict On
Option Explicit On
Option Compare Text
Imports VI = Virtualia.Systeme.Constantes
Imports Microsoft.VisualBasic
Public Class Fenetre_PER_PLANNING_SEMAINE
    Inherits System.Web.UI.UserControl
    Private WebFct As Virtualia.Net.WebFonctions
    Private WsIde_Dossier As Integer = 0
    'Execution
    Private WsNomStateIde As String = "VDetailSemIde"
    Private WsNomStateParam As String = "VSemaineParam"
    Private WsCacheIde As ArrayList
    Private WsCacheParam As ArrayList
    Private WsDossierRtt As Virtualia.Metier.TempsTravail.DossierRTT
    Private WsPlanningRtt As Virtualia.Metier.TempsTravail.PlanningIndividuel
    Private WsListeVues As List(Of Virtualia.TablesObjet.ShemaVUE.VUE_JourTravail)
    Private WsTotalPrevu As Integer
    Private WsTotalConstate As Integer
    Private WsTotalCompte As Integer

    Public Property Identifiant() As Integer
        Get
            If Me.ViewState(WsNomStateIde) IsNot Nothing Then
                WsCacheIde = CType(Me.ViewState(WsNomStateIde), ArrayList)
                WsIde_Dossier = CInt(WsCacheIde(0))
            End If
            Return WsIde_Dossier
        End Get
        Set(ByVal value As Integer)
            If Me.ViewState(WsNomStateIde) IsNot Nothing Then
                WsCacheIde = CType(Me.ViewState(WsNomStateIde), ArrayList)
                WsIde_Dossier = CInt(WsCacheIde(0))
            End If
            If value = WsIde_Dossier Then
                Exit Property
            End If
            WsIde_Dossier = value
            If WsIde_Dossier = 0 Then
                Exit Property
            End If
            If Me.ViewState(WsNomStateIde) IsNot Nothing Then
                Me.ViewState.Remove(WsNomStateIde)
            End If
            '** MettreEnCache le dossier **
            WsCacheIde = New ArrayList
            WsCacheIde.Add(WsIde_Dossier)
            Me.ViewState.Add(WsNomStateIde, WsCacheIde)
            '**
            WsDossierRtt = WebFct.PointeurUtilisateur.PointeurDllRTT.ItemDossier(WsIde_Dossier)
            If WsDossierRtt Is Nothing Then
                WebFct.PointeurUtilisateur.PointeurDllRTT.Identifiant(WsIde_Dossier) = _
                        WebFct.PointeurDossier(WsIde_Dossier).V_ListeDesFiches( _
                            WebFct.PointeurUtilisateur.PointeurDllRTT.ObjetsDllARTT)

                WsDossierRtt = WebFct.PointeurUtilisateur.PointeurDllRTT.ItemDossier(WsIde_Dossier)
            End If
        End Set
    End Property

    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        WebFct = New Virtualia.Net.WebFonctions(Me, 0)
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        If Me.ViewState(WsNomStateParam) Is Nothing Then
            Call FaireListeSemaines("", "")
        End If
        Call FairePlanningHebdo()
    End Sub

    Private Sub LstSemaines_ValeurChange(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.DonneeChangeEventArgs) Handles LstSemaines.ValeurChange
        Dim IndiceI As Integer
        Dim Numero As Integer = 0

        If Me.ViewState(WsNomStateParam) Is Nothing Then
            Exit Sub
        End If
        WsCacheParam = CType(Me.ViewState(WsNomStateParam), ArrayList)
        For IndiceI = 1 To WsCacheParam.Count - 1
            If WsCacheParam(IndiceI).ToString = e.Valeur Then
                Numero = IndiceI
                Exit For
            End If
        Next IndiceI
        If Numero = 0 Then
            Exit Sub
        End If
        '** MettreEnCache les paramètres **
        WsCacheParam(0) = Numero
        Me.ViewState.Remove(WsNomStateParam)
        Me.ViewState.Add(WsNomStateParam, WsCacheParam)
    End Sub

    Private Sub FairePlanningHebdo()
        Dim Numero As Integer
        Dim TableauW(0) As String
        Dim DateDebut As String
        Dim DateFin As String
        Dim DateW As String
        Dim NbHeures As Integer

        WsTotalPrevu = 0
        WsTotalConstate = 0
        WsTotalCompte = 0

        If Me.ViewState(WsNomStateIde) Is Nothing Then
            Exit Sub
        End If
        WsCacheIde = CType(Me.ViewState(WsNomStateIde), ArrayList)

        If Me.ViewState(WsNomStateParam) Is Nothing Then
            Exit Sub
        End If
        WsCacheParam = CType(Me.ViewState(WsNomStateParam), ArrayList)
        Numero = CInt(WsCacheParam(0))
        TableauW = Strings.Split(WsCacheParam(Numero).ToString, " - ", -1)

        LabelTitreSemaine.Text = "Horaires du " & TableauW(1)
        LabelEnteteJour.Text = TableauW(0)

        TableauW = Strings.Split(TableauW(1), " au ", -1)
        DateDebut = TableauW(0)
        DateFin = TableauW(1)

        Dim IndiceJ As Integer
        Dim Ctl As Control
        Dim VirControle As Controles_VLigneDetailSemaine

        WsPlanningRtt = Nothing
        If Identifiant > 0 Then
            WsDossierRtt = WebFct.PointeurUtilisateur.PointeurDllRTT.ItemDossier(WsIde_Dossier)
            WsPlanningRtt = WsDossierRtt.PlanningIndividuel(DateDebut, DateFin)
            WsListeVues = WsPlanningRtt.ListedesVues(DateDebut, DateFin)
        Else
            Exit Sub
        End If

        IndiceJ = 0
        DateW = DateDebut

        Do 'Traitement des jours du mois
            Ctl = WebFct.VirWebControle(Me.TableauHorairesHebdomadaire, "Jour", IndiceJ)
            If Ctl IsNot Nothing Then
                VirControle = CType(Ctl, Controles_VLigneDetailSemaine)
                VirControle.Initialiser()
                VirControle.VLibelleJour = Day(CDate(DateW)) & Strings.Space(1) & WebFct.ViRhDates.MoisEnClair(CShort(Month(CDate(DateW)))).ToLower
                VirControle.Visible = True
                If WebFct.ViRhDates.SiJourOuvre(DateW, True) = True Then
                    VirControle.VBackColorPlage(1) = Drawing.Color.White
                    VirControle.VBackColorPlage(2) = Drawing.Color.White
                Else
                    VirControle.VBackColorPlage(1) = WebFct.PointeurUtilisateur.PointeurParametres.CouleurPlanning(1)
                    VirControle.VBackColorPlage(2) = WebFct.PointeurUtilisateur.PointeurParametres.CouleurPlanning(1)
                End If
                If WsIde_Dossier > 0 Then
                    Call FaireJour(DateW, VirControle)
                End If
            End If

            DateW = WebFct.ViRhDates.CalcDateMoinsJour(DateW, "1", "0")
            Select Case WebFct.ViRhDates.ComparerDates(DateW, DateFin)
                Case VI.ComparaisonDates.PlusGrand
                    Exit Do
            End Select
            IndiceJ += 1
            If IndiceJ > 6 Then
                Exit Do
            End If
        Loop

        If WsTotalPrevu >= 0 Then
            NbHeures = WsTotalPrevu \ 60
            LabelPrevu.Text = NbHeures.ToString & " h " & Strings.Format(WsTotalPrevu Mod 60, "00")
        Else
            NbHeures = -WsTotalPrevu \ 60
            LabelPrevu.Text = "-" & NbHeures.ToString & " h " & Strings.Format(-WsTotalPrevu Mod 60, "00")
        End If
        If WsTotalConstate >= 0 Then
            NbHeures = WsTotalConstate \ 60
            LabelValide.Text = NbHeures.ToString & " h " & Strings.Format(WsTotalConstate Mod 60, "00")
        Else
            NbHeures = -WsTotalConstate \ 60
            LabelValide.Text = "-" & NbHeures.ToString & " h " & Strings.Format(-WsTotalConstate Mod 60, "00")
        End If
        If WsTotalCompte >= 0 Then
            NbHeures = WsTotalCompte \ 60
            LabelComptabilise.Text = NbHeures.ToString & " h " & Strings.Format(WsTotalCompte Mod 60, "00")
        Else
            NbHeures = -WsTotalCompte \ 60
            LabelComptabilise.Text = "-" & NbHeures.ToString & " h " & Strings.Format(-WsTotalCompte Mod 60, "00")
        End If
    End Sub

    Private Sub FaireJour(ByVal DateValeur As String, ByVal CtlJour As Controles_VLigneDetailSemaine)
        Dim FicheVue As Virtualia.TablesObjet.ShemaVUE.VUE_JourTravail
        Dim Couleur As Drawing.Color
        Dim IndiceP As Integer
        Dim IndiceC As Integer

        FicheVue = WsListeVues.Find(Function(Recherche) Recherche.Date_de_Valeur = DateValeur)

        If FicheVue Is Nothing Then
            If WsDossierRtt.SiEnActivite(DateValeur) = True Then
                CtlJour.VBackColorPlage(1) = WebFct.PointeurUtilisateur.PointeurParametres.CouleurPlanning(1)
                CtlJour.VBackColorPlage(2) = WebFct.PointeurUtilisateur.PointeurParametres.CouleurPlanning(1)
            Else
                CtlJour.VBackColorPlage(1) = WebFct.ConvertCouleur("#73829A")
                CtlJour.VBackColorPlage(2) = WebFct.ConvertCouleur("#73829A")
            End If
            Exit Sub
        End If

        CtlJour.VTotalValide = FicheVue.DureeConstatee

        WsTotalPrevu += FicheVue.DureePrevueCycleEnMinutes
        WsTotalConstate += FicheVue.DureeConstateeEnMinutes
        WsTotalCompte += FicheVue.DureeComptabiliseeEnMinutes

        For IndiceP = 0 To FicheVue.NombredeFiches - 1
            Couleur = Nothing
            If FicheVue.ItemPlage(IndiceP).EvenementPlage <> "" Then
                Select Case FicheVue.ItemPlage(IndiceP).SiPrevisionIntranet
                    Case Is = 1
                        Couleur = WebFct.PointeurUtilisateur.PointeurParametres.CouleurPlanning(4)
                    Case Else
                        IndiceC = WebFct.PointeurUtilisateur.PointeurParametres.IndexCouleurPlanning(FicheVue.ItemPlage(IndiceP).EvenementPlage)
                        If IndiceC = 0 Then
                            Couleur = Drawing.Color.White
                        Else
                            Couleur = WebFct.PointeurUtilisateur.PointeurParametres.CouleurPlanning(IndiceC)
                        End If
                End Select
            End If

            Select Case FicheVue.ItemPlage(IndiceP).NumeroPlage
                Case Is = VI.NumeroPlage.Plage1_Presence
                    If FicheVue.ItemPlage(IndiceP).HoraireCycle_Prevu_Minutes = 0 Then
                        CtlJour.VBackColorPlage(1) = WebFct.PointeurUtilisateur.PointeurParametres.CouleurPlanning(1)
                    End If
                    CtlJour.VHorairesBadges(1) = FicheVue.ItemPlage(IndiceP).HeureDebut_Badge & " - " & FicheVue.ItemPlage(IndiceP).HeureFin_Badge
                    CtlJour.VHorairesValides(1) = FicheVue.ItemPlage(IndiceP).HeureDebut_Constate & " - " & FicheVue.ItemPlage(IndiceP).HeureFin_Constate
                    CtlJour.VDureesValides(1) = FicheVue.ItemPlage(IndiceP).Horaire_Constate
                Case Is = VI.NumeroPlage.Plage2_Presence
                    If FicheVue.ItemPlage(IndiceP).HoraireCycle_Prevu_Minutes = 0 Then
                        CtlJour.VBackColorPlage(2) = WebFct.PointeurUtilisateur.PointeurParametres.CouleurPlanning(1)
                    End If
                    CtlJour.VHorairesBadges(2) = FicheVue.ItemPlage(IndiceP).HeureDebut_Badge & " - " & FicheVue.ItemPlage(IndiceP).HeureFin_Badge
                    CtlJour.VHorairesValides(2) = FicheVue.ItemPlage(IndiceP).HeureDebut_Constate & " - " & FicheVue.ItemPlage(IndiceP).HeureFin_Constate
                    CtlJour.VDureesValides(2) = FicheVue.ItemPlage(IndiceP).Horaire_Constate
                Case Is = VI.NumeroPlage.Jour_Absence
                    If FicheVue.SiJourFerie = False Or FicheVue.DureePrevueCycleEnMinutes > 0 Then
                        CtlJour.VBackColorPlage(1) = Couleur
                        CtlJour.VBackColorPlage(2) = Couleur
                    End If
                    CtlJour.VHorairesBadges(1) = ""
                    CtlJour.VHorairesValides(1) = ""
                    CtlJour.VDureesValides(1) = ""
                    CtlJour.VHorairesBadges(2) = ""
                    CtlJour.VHorairesValides(2) = ""
                    CtlJour.VDureesValides(2) = ""
                    CtlJour.VCommentaire = FicheVue.ItemPlage(IndiceP).EvenementPlage
                Case Is = VI.NumeroPlage.Plage1_Absence
                    CtlJour.VBackColorPlage(1) = Couleur
                    CtlJour.VHorairesBadges(1) = ""
                    CtlJour.VHorairesValides(1) = ""
                    CtlJour.VDureesValides(1) = ""
                    CtlJour.VCommentaire = FicheVue.ItemPlage(IndiceP).EvenementPlage
                Case Is = VI.NumeroPlage.Plage2_Absence
                    CtlJour.VBackColorPlage(2) = Couleur
                    CtlJour.VHorairesBadges(2) = ""
                    CtlJour.VHorairesValides(2) = ""
                    CtlJour.VDureesValides(2) = ""
                    CtlJour.VCommentaire = FicheVue.ItemPlage(IndiceP).EvenementPlage
                Case Is = VI.NumeroPlage.Jour_Formation
                    Couleur = WebFct.PointeurUtilisateur.PointeurParametres.CouleurPlanning(WebFct.PointeurUtilisateur.PointeurParametres.IndexCouleurPlanning("Formation"))
                    CtlJour.VBackColorPlage(1) = Couleur
                    CtlJour.VBackColorPlage(2) = Couleur
                    CtlJour.VHorairesBadges(1) = ""
                    CtlJour.VHorairesValides(1) = ""
                    CtlJour.VDureesValides(1) = ""
                    CtlJour.VHorairesBadges(2) = ""
                    CtlJour.VHorairesValides(2) = ""
                    CtlJour.VDureesValides(2) = ""
                    If FicheVue.IntituleduJour <> "" Then
                        CtlJour.VCommentaire = FicheVue.IntituleduJour
                    Else
                        CtlJour.VCommentaire = FicheVue.ItemPlage(IndiceP).EvenementPlage
                    End If
                Case Is = VI.NumeroPlage.Plage1_Formation
                    Couleur = WebFct.PointeurUtilisateur.PointeurParametres.CouleurPlanning(WebFct.PointeurUtilisateur.PointeurParametres.IndexCouleurPlanning("Formation"))
                    CtlJour.VBackColorPlage(1) = Couleur
                    CtlJour.VHorairesBadges(1) = ""
                    CtlJour.VHorairesValides(1) = ""
                    CtlJour.VDureesValides(1) = ""
                    If FicheVue.IntituleduJour <> "" Then
                        CtlJour.VCommentaire = FicheVue.IntituleduJour
                    Else
                        CtlJour.VCommentaire = FicheVue.ItemPlage(IndiceP).EvenementPlage
                    End If
                Case Is = VI.NumeroPlage.Plage2_Formation
                    Couleur = WebFct.PointeurUtilisateur.PointeurParametres.CouleurPlanning(WebFct.PointeurUtilisateur.PointeurParametres.IndexCouleurPlanning("Formation"))
                    CtlJour.VBackColorPlage(2) = Couleur
                    CtlJour.VHorairesBadges(2) = ""
                    CtlJour.VHorairesValides(2) = ""
                    CtlJour.VDureesValides(2) = ""
                    If FicheVue.IntituleduJour <> "" Then
                        CtlJour.VCommentaire = FicheVue.IntituleduJour
                    Else
                        CtlJour.VCommentaire = FicheVue.ItemPlage(IndiceP).EvenementPlage
                    End If
                Case Is = VI.NumeroPlage.Jour_Mission
                    Couleur = WebFct.PointeurUtilisateur.PointeurParametres.CouleurPlanning(WebFct.PointeurUtilisateur.PointeurParametres.IndexCouleurPlanning("Mission"))
                    CtlJour.VBackColorPlage(1) = Couleur
                    CtlJour.VBackColorPlage(2) = Couleur
                    CtlJour.VHorairesBadges(1) = ""
                    CtlJour.VHorairesValides(1) = ""
                    CtlJour.VDureesValides(1) = ""
                    CtlJour.VHorairesBadges(2) = ""
                    CtlJour.VHorairesValides(2) = ""
                    CtlJour.VDureesValides(2) = ""
                    If FicheVue.IntituleduJour <> "" Then
                        CtlJour.VCommentaire = FicheVue.IntituleduJour
                    Else
                        CtlJour.VCommentaire = FicheVue.ItemPlage(IndiceP).EvenementPlage
                    End If
                Case Is = VI.NumeroPlage.Plage1_Mission
                    Couleur = WebFct.PointeurUtilisateur.PointeurParametres.CouleurPlanning(WebFct.PointeurUtilisateur.PointeurParametres.IndexCouleurPlanning("Mission"))
                    CtlJour.VBackColorPlage(1) = Couleur
                    CtlJour.VHorairesBadges(1) = ""
                    CtlJour.VHorairesValides(1) = ""
                    CtlJour.VDureesValides(1) = ""
                    If FicheVue.IntituleduJour <> "" Then
                        CtlJour.VCommentaire = FicheVue.IntituleduJour
                    Else
                        CtlJour.VCommentaire = FicheVue.ItemPlage(IndiceP).EvenementPlage
                    End If
                Case Is = VI.NumeroPlage.Plage2_Mission
                    Couleur = WebFct.PointeurUtilisateur.PointeurParametres.CouleurPlanning(WebFct.PointeurUtilisateur.PointeurParametres.IndexCouleurPlanning("Mission"))
                    CtlJour.VBackColorPlage(2) = Couleur
                    CtlJour.VHorairesBadges(2) = ""
                    CtlJour.VHorairesValides(2) = ""
                    CtlJour.VDureesValides(2) = ""
                    If FicheVue.IntituleduJour <> "" Then
                        CtlJour.VCommentaire = FicheVue.IntituleduJour
                    Else
                        CtlJour.VCommentaire = FicheVue.ItemPlage(IndiceP).EvenementPlage
                    End If
            End Select
        Next IndiceP
    End Sub

    Private Sub FaireListeSemaines(ByVal DateDebut As String, ByVal Datefin As String)
        Dim Chaine As New System.Text.StringBuilder
        Dim Libel As String = "Aucune semaine" & VI.Tild & "Une semaine" & VI.Tild & "semaines"
        Dim Annee As Integer
        Dim MoisDebut As Integer
        Dim MoisFin As Integer
        Dim DateW As String
        Dim Numero As Integer = 0
        Dim IndiceJ As Integer
        Dim IndiceK As Integer

        If DateDebut = "" Then
            Annee = CInt(Strings.Right(WebFct.ViRhDates.DateduJour(False), 4))
            MoisDebut = 1
            MoisFin = 12
            Numero = WebFct.ViRhDates.NumerodelaSemaine(WebFct.ViRhDates.DateduJour(False))
        Else
            Annee = CInt(Strings.Right(DateDebut, 4))
            MoisDebut = CInt(Strings.Mid(DateDebut, 4, 2))
            MoisFin = CInt(Strings.Mid(Datefin, 4, 2))
            Numero = WebFct.ViRhDates.NumerodelaSemaine(Datefin)
        End If

        If Me.ViewState(WsNomStateParam) IsNot Nothing Then
            Me.ViewState.Remove(WsNomStateParam)
        End If
        WsCacheParam = New ArrayList
        For IndiceJ = 0 To 55
            WsCacheParam.Add("")
        Next IndiceJ

        IndiceJ = 1
        Do
            DateW = WebFct.ViRhDates.DateSaisieVerifiee(Strings.Format(IndiceJ, "00") & "/" & Strings.Format(MoisDebut, "00") & "/" & Annee.ToString)
            If DateW = "" Then
                Exit Do
            End If

            IndiceK = WebFct.ViRhDates.NumerodelaSemaine(DateW)
            DateDebut = WebFct.ViRhDates.InfoSemaine(DateW, 4, IndiceK)
            Datefin = WebFct.ViRhDates.InfoSemaine(DateW, 5, IndiceK)

            Chaine.Append("Semaine " & IndiceK.ToString & " - " & DateDebut & " au " & Datefin & VI.Tild)
            WsCacheParam(IndiceK) = "Semaine " & IndiceK.ToString & " - " & DateDebut & " au " & Datefin

            IndiceJ += 7

            If IndiceJ > 30 Then
                IndiceJ = 1
                MoisDebut += 1
                Select Case MoisFin
                    Case Is = 12
                        If MoisDebut > MoisFin Then
                            Exit Do
                        End If
                    Case Else
                        If MoisDebut > 12 And Annee = CInt(Strings.Right(DateDebut, 4)) Then
                            MoisDebut = 1
                            Annee += 1
                        End If
                        If MoisDebut = MoisFin And Annee = CInt(Strings.Right(Datefin, 4)) Then
                            Exit Do
                        End If
                End Select
            End If
        Loop
        WsCacheParam(0) = Numero
        Me.ViewState.Add(WsNomStateParam, WsCacheParam)

        LstSemaines.V_Liste(Libel) = Chaine.ToString
        LstSemaines.LstText = WsCacheParam(Numero).ToString
        Chaine.Clear()

    End Sub
End Class