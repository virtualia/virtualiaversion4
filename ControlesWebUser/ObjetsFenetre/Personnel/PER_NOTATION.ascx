﻿<%@ Control Language="VB" AutoEventWireup="false" Inherits="Virtualia.Net.Fenetre_PER_NOTATION" CodeBehind="PER_NOTATION.ascx.vb" %>

<%@ Register Src="~/Controles/Saisies/VCoupleEtiDonnee.ascx" TagName="VCoupleEtiDonnee" TagPrefix="Virtualia" %>
<%@ Register Src="~/Controles/Saisies/VCoupleVerticalEtiDonnee.ascx" TagName="VCoupleVerticalEtiDonnee" TagPrefix="Virtualia" %>
<%@ Register Src="~/Controles/Saisies/VDuoEtiquetteCommande.ascx" TagName="VDuoEtiquetteCommande" TagPrefix="Virtualia" %>
<%@ Register Src="~/Controles/Saisies/VReliquatAnciennete.ascx" TagName="VReliquatAnciennete" TagPrefix="Virtualia" %>

<%@ Register Src="~/ControlsGeneriques/V_DataGrid.ascx" TagName="VDataGrid" TagPrefix="Generic" %>
<%@ Register Src="~/ControlsGeneriques/V_CommandeCRUD.ascx" TagName="VCrud" TagPrefix="Generic" %>

<asp:Table ID="CadreControle" runat="server" Height="30px" Width="750px" CellPadding="0" CellSpacing="0" HorizontalAlign="Center">
    <asp:TableRow>
        <asp:TableCell>
            <Generic:VDataGrid ID="ListeGrille" runat="server" CadreWidth="700px" SiColonneSelect="true" SiCaseAcocher="false" />
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="CadreInfo" runat="server" BorderStyle="Ridge" BorderWidth="2px" BorderColor="#B0E0D7" Height="720px" Width="750px" HorizontalAlign="Center" Style="margin-top: 3px;">
                <asp:TableRow>
                    <asp:TableCell>
                        <Generic:VCrud ID="CtlCrud" runat="server" Height="22px" Width="244px" HorizontalAlign="Right" CadreStyle="margin-top: 3px; margin-right: 3px" />
                    </asp:TableCell>
                </asp:TableRow>

                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Table ID="CadreTitre" runat="server" Height="40px" CellPadding="0" Width="750px" CellSpacing="0" HorizontalAlign="Center">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                    <asp:Label ID="Etiquette" runat="server" Text="Notation" Height="20px" Width="300px" BackColor="#2FA49B" BorderColor="#B0E0D7" BorderStyle="Groove" BorderWidth="2px" ForeColor="#D7FAF3" Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Small" Style="margin-top: 5px; margin-left: 1px; margin-bottom: 10px; font-style: oblique; text-indent: 5px; text-align: center;" />
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Table ID="CadreAnneeNotation" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH00" runat="server" V_PointdeVue="1" V_Objet="19" V_Information="0" V_SiDonneeDico="true" EtiWidth="100px" DonWidth="60px" DonTabIndex="1" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Height="15px" />
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Table ID="TableauNote" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH01" runat="server" V_PointdeVue="1" V_Objet="19" V_Information="1" V_SiDonneeDico="true" EtiWidth="45px" DonWidth="50px" DonTabIndex="2" />
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Left">
                                    <Virtualia:VDuoEtiquetteCommande ID="Dontab14" runat="server" V_PointdeVue="1" V_Objet="19" V_Information="14" V_SiDonneeDico="true" EtiVisible="false" DonWidth="165px" DonTabIndex="3" />
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Left">
                                    <Virtualia:VDuoEtiquetteCommande ID="Dontab02" runat="server" V_PointdeVue="1" V_Objet="19" V_Information="2" V_SiDonneeDico="true" EtiWidth="40px" DonWidth="150px" DonTabIndex="4" />
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Left">
                                    <Virtualia:VDuoEtiquetteCommande ID="Dontab03" runat="server" V_PointdeVue="1" V_Objet="19" V_Information="3" V_SiDonneeDico="true" EtiWidth="65px" DonWidth="170px" DonTabIndex="5" />
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Table ID="TableauReduction" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                            <asp:TableRow>
                                <asp:TableCell Height="2px" />
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <Virtualia:VReliquatAnciennete ID="Reliquat16" runat="server" V_PointdeVue="1" V_Objet="19" V_Information="16" V_SiDonneeDico="true" EtiWidth="150px" EtiText="Réduction d'ancienneté" AncienneteVisible="false" />
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Left">
                                    <Virtualia:VDuoEtiquetteCommande ID="Dontab17" runat="server" V_PointdeVue="1" V_Objet="19" V_Information="17" V_SiDonneeDico="true" EtiText="Sens de la réduction" EtiWidth="157px" DonWidth="165px" DonTabIndex="9" />
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Table ID="TableauReference" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                            <asp:TableRow>
                                <asp:TableCell Height="2px" ColumnSpan="2" />
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <Virtualia:VDuoEtiquetteCommande ID="Dontab27" runat="server" V_PointdeVue="1" V_Objet="19" V_Information="27" V_SiDonneeDico="true" EtiWidth="150px" DonWidth="340px" DonTabIndex="10" />
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Left">
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH18" runat="server" V_PointdeVue="1" V_Objet="19" V_Information="18" V_SiDonneeDico="true" EtiWidth="160px" DonWidth="50px" DonTabIndex="11" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Height="15px" ColumnSpan="2" />
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Table ID="CadreCriteres" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                            <asp:TableRow Height="30px">
                                <asp:TableCell HorizontalAlign="Left">
                                    <asp:Label ID="LabelCriteres" runat="server" Height="20px" Width="433px" Text="Critères de notation" BackColor="#B0E0D7" BorderColor="#B0E0D7" BorderStyle="Outset" BorderWidth="2px" ForeColor="#142425" Font-Italic="true" Style="margin-left: 20px; text-align: center;" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" />
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Table ID="TableauDonCriteres" runat="server" CellPadding="0" CellSpacing="0" Width="410px">
                            <asp:TableRow>
                                <asp:TableCell>
                                    <asp:Label ID="LabelDebutLigne1" runat="server" Height="20px" Width="20px" Text="1" BackColor="#B0E0D7" BorderColor="#B0E0D7" BorderStyle="Outset" BorderWidth="2px" ForeColor="#142425" Font-Italic="true" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Style="margin-top: 0px; margin-left: 20px; text-indent: 0px; text-align: center" />
                                </asp:TableCell>
                                <asp:TableCell>
                                    <Virtualia:VDuoEtiquetteCommande ID="Dontab04" runat="server" V_PointdeVue="1" V_Objet="19" V_Information="4" V_SiDonneeDico="true" EtiVisible="false" DonWidth="350px" DonTabIndex="12" />
                                </asp:TableCell>
                                <asp:TableCell>
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH05" runat="server" V_PointdeVue="1" V_Objet="19" V_Information="5" V_SiDonneeDico="true" EtiVisible="false" DonWidth="50px" DonTabIndex="13" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell>
                                    <asp:Label ID="LabelDebutLigne2" runat="server" Height="20px" Width="20px" Text="2" BackColor="#B0E0D7" BorderColor="#B0E0D7" BorderStyle="Outset" BorderWidth="2px" ForeColor="#142425" Font-Italic="true" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Style="margin-top: 0px; margin-left: 20px; text-indent: 0px; text-align: center" />
                                </asp:TableCell>
                                <asp:TableCell>
                                    <Virtualia:VDuoEtiquetteCommande ID="Dontab06" runat="server" V_PointdeVue="1" V_Objet="19" V_Information="6" V_SiDonneeDico="true" EtiVisible="false" DonWidth="350px" DonTabIndex="14" />
                                </asp:TableCell>
                                <asp:TableCell>
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH07" runat="server" V_PointdeVue="1" V_Objet="19" V_Information="7" V_SiDonneeDico="true" EtiVisible="false" DonWidth="50px" DonTabIndex="15" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell>
                                    <asp:Label ID="LabelDebutLigne3" runat="server" Height="20px" Width="20px" Text="3" BackColor="#B0E0D7" BorderColor="#B0E0D7" BorderStyle="Outset" BorderWidth="2px" ForeColor="#142425" Font-Italic="true" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Style="margin-top: 0px; margin-left: 20px; text-indent: 0px; text-align: center" />
                                </asp:TableCell>
                                <asp:TableCell>
                                    <Virtualia:VDuoEtiquetteCommande ID="Dontab08" runat="server" V_PointdeVue="1" V_Objet="19" V_Information="8" V_SiDonneeDico="true" EtiVisible="false" DonWidth="350px" DonTabIndex="16" />
                                </asp:TableCell>
                                <asp:TableCell>
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH09" runat="server" V_PointdeVue="1" V_Objet="19" V_Information="9" V_SiDonneeDico="true" EtiVisible="false" DonWidth="50px" DonTabIndex="17" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell>
                                    <asp:Label ID="LabelDebutLigne4" runat="server" Height="20px" Width="20px" Text="4" BackColor="#B0E0D7" BorderColor="#B0E0D7" BorderStyle="Outset" BorderWidth="2px" ForeColor="#142425" Font-Italic="true" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Style="margin-top: 0px; margin-left: 20px; text-indent: 0px; text-align: center" />
                                </asp:TableCell>
                                <asp:TableCell>
                                    <Virtualia:VDuoEtiquetteCommande ID="Dontab10" runat="server" V_PointdeVue="1" V_Objet="19" V_Information="10" V_SiDonneeDico="true" EtiVisible="false" DonWidth="350px" DonTabIndex="18" />
                                </asp:TableCell>
                                <asp:TableCell>
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH11" runat="server" V_PointdeVue="1" V_Objet="19" V_Information="11" V_SiDonneeDico="true" EtiVisible="false" DonWidth="50px" DonTabIndex="19" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell>
                                    <asp:Label ID="LabelDebutLigne5" runat="server" Height="20px" Width="20px" Text="5" BackColor="#B0E0D7" BorderColor="#B0E0D7" BorderStyle="Outset" BorderWidth="2px" ForeColor="#142425" Font-Italic="true" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Style="margin-top: 0px; margin-left: 20px; text-indent: 0px; text-align: center" />
                                </asp:TableCell>
                                <asp:TableCell>
                                    <Virtualia:VDuoEtiquetteCommande ID="Dontab12" runat="server" V_PointdeVue="1" V_Objet="19" V_Information="12" V_SiDonneeDico="true" EtiVisible="false" DonWidth="350px" DonTabIndex="20" />
                                </asp:TableCell>
                                <asp:TableCell>
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH13" runat="server" V_PointdeVue="1" V_Objet="19" V_Information="13" V_SiDonneeDico="true" EtiVisible="false" DonWidth="50px" DonTabIndex="21" />
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Table ID="TableauObservations" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                            <asp:TableRow>
                                <asp:TableCell Height="10px" ColumnSpan="2" />
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                    <Virtualia:VCoupleVerticalEtiDonnee ID="InfoV19" runat="server" DonTextMode="true" V_PointdeVue="1" V_Objet="19" V_Information="19" V_SiDonneeDico="true" EtiWidth="361px" DonWidth="359px" DonHeight="150px" DonTabIndex="22" EtiStyle="text-align:center" />
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Center">
                                    <Virtualia:VCoupleVerticalEtiDonnee ID="InfoV20" runat="server" DonTextMode="true" V_PointdeVue="1" V_Objet="19" V_Information="20" V_SiDonneeDico="true" EtiWidth="360px" DonWidth="358px" DonHeight="150px" DonTabIndex="23" EtiStyle="text-align:center" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Height="2px" ColumnSpan="2" />
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Table ID="TableauRapport" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                    <Virtualia:VCoupleVerticalEtiDonnee ID="InfoV21" runat="server" DonTextMode="true" V_PointdeVue="1" V_Objet="19" V_Information="21" V_SiDonneeDico="true" EtiWidth="735px" DonWidth="733px" DonHeight="100px" DonTabIndex="24" EtiStyle="text-align:center" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Height="12px" />
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>
</asp:Table>
