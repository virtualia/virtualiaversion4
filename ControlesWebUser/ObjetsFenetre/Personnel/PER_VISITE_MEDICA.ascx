﻿<%@ Control Language="VB" AutoEventWireup="false" Inherits="Virtualia.Net.Fenetre_PER_VISITE_MEDICA" CodeBehind="PER_VISITE_MEDICA.ascx.vb" %>

<%@ Register Src="~/Controles/Saisies/VCoupleEtiDonnee.ascx" TagName="VCoupleEtiDonnee" TagPrefix="Virtualia" %>
<%@ Register Src="~/Controles/Saisies/VDuoEtiquetteCommande.ascx" TagName="VDuoEtiquetteCommande" TagPrefix="Virtualia" %>

<%@ Register Src="~/ControlsGeneriques/V_DataGrid.ascx" TagName="VDataGrid" TagPrefix="Generic" %>
<%@ Register Src="~/ControlsGeneriques/V_CommandeCRUD.ascx" TagName="VCrud" TagPrefix="Generic" %>

<asp:Table ID="CadreControle" runat="server" Height="30px" Width="450px" CellPadding="0" CellSpacing="0" HorizontalAlign="Center">
    <asp:TableRow>
        <asp:TableCell>
            <Generic:VDataGrid ID="ListeGrille" runat="server" CadreWidth="430px" SiColonneSelect="true" SiCaseAcocher="false" />
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="CadreInfo" runat="server" BorderStyle="Ridge" BorderWidth="2px" BorderColor="#B0E0D7" Height="340px" Width="500px" HorizontalAlign="Center">
                <asp:TableRow>
                    <asp:TableCell>
                        <Generic:VCrud ID="CtlCrud" runat="server" Height="22px" Width="244px" HorizontalAlign="Right" CadreStyle="margin-top: 3px; margin-right: 3px" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Table ID="CadreTitre" runat="server" Height="40px" CellPadding="0" Width="500px" CellSpacing="0" HorizontalAlign="Center">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                    <asp:Label ID="Etiquette" runat="server" Text="Visite médicale" Height="20px" Width="300px" BackColor="#2FA49B" BorderColor="#B0E0D7" BorderStyle="Groove" BorderWidth="2px" ForeColor="#D7FAF3" Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Small" Style="margin-top: 10px; margin-left: 4px; margin-bottom: 10px; font-style: oblique; text-indent: 5px; text-align: center;" />
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Table ID="VisiteMedicale" runat="server" CellPadding="0" CellSpacing="0">
                            <asp:TableRow>
                                <asp:TableCell>
                                    <asp:Table ID="Visite" runat="server">
                                        <asp:TableRow>
                                            <asp:TableCell HorizontalAlign="Left">
                                                <Virtualia:VCoupleEtiDonnee ID="InfoH00" runat="server" V_PointdeVue="1" V_Objet="32" V_Information="0" V_SiDonneeDico="true" EtiWidth="100px" DonWidth="80px" DonTabIndex="1" />
                                            </asp:TableCell>
                                            <asp:TableCell HorizontalAlign="Left">
                                                <Virtualia:VCoupleEtiDonnee ID="InfoH02" runat="server" V_PointdeVue="1" V_Objet="32" V_Information="2" V_SiDonneeDico="true" EtiWidth="20px" DonWidth="60px" DonTabIndex="2" />
                                            </asp:TableCell>
                                            <asp:TableCell>
                                                <asp:Label ID="LabelHeureVisite" runat="server" Text="heure" Height="21px" Width="43px" BackColor="Transparent" BorderStyle="none" BorderWidth="0px" ForeColor="#2FA49B" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; font-style: oblique; text-indent: 2px; text-align: left;" />
                                            </asp:TableCell>
                                        </asp:TableRow>
                                    </asp:Table>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <Virtualia:VDuoEtiquetteCommande ID="Dontab01" runat="server" V_PointdeVue="1" V_Objet="32" V_Information="1" V_SiDonneeDico="true" EtiWidth="160px" DonWidth="300px" DonTabIndex="3" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <Virtualia:VDuoEtiquetteCommande ID="Dontab03" runat="server" V_PointdeVue="1" V_Objet="32" V_Information="3" V_SiDonneeDico="true" EtiWidth="160px" DonWidth="300px" DonTabIndex="4" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <Virtualia:VDuoEtiquetteCommande ID="Dontab04" runat="server" V_PointdeVue="1" V_Objet="32" V_Information="4" V_SiDonneeDico="true" EtiWidth="160px" DonWidth="300px" DonTabIndex="5" />
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Table ID="Convocation" runat="server" CellPadding="0" CellSpacing="0" Width="500px" HorizontalAlign="Center">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                    <asp:Label ID="LabelConvoc" runat="server" Text="Convocation" Height="20px" Width="300px" BackColor="#2FA49B" BorderColor="#B0E0D7" BorderStyle="Groove" BorderWidth="2px" ForeColor="#D7FAF3" Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Small" Style="margin-top: 20px; margin-left: 4px; margin-bottom: 1px; font-style: oblique; text-indent: 5px; text-align: center;" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH05" runat="server" V_PointdeVue="1" V_Objet="32" V_Information="5" V_SiDonneeDico="true" EtiWidth="200px" DonWidth="100px" DonTabIndex="6" EtiStyle="margin-top:10px" DonStyle="margin-top:10px" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <Virtualia:VDuoEtiquetteCommande ID="Dontab06" runat="server" V_PointdeVue="1" V_Objet="32" V_Information="6" V_SiDonneeDico="true" EtiWidth="120px" DonWidth="350px" DonTabIndex="7" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <Virtualia:VDuoEtiquetteCommande ID="Dontab07" runat="server" V_PointdeVue="1" V_Objet="32" V_Information="7" V_SiDonneeDico="true" EtiWidth="120px" DonWidth="350px" DonTabIndex="8" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Height="12px" />
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>
</asp:Table>
