﻿<%@ Control Language="VB" AutoEventWireup="false" Inherits="Virtualia.Net.Fenetre_PER_CET_MANAGER" Codebehind="PER_CET_MANAGER.ascx.vb" %>

 <asp:Table ID="CadreInfo" runat="server" BorderStyle="None" BorderWidth="2px" Visible="true" Height="90px"
    BorderColor="#B0E0D7" Width="500px" HorizontalAlign="Center" style="margin-top: 1px;">
    <asp:TableRow>
      <asp:TableCell HorizontalAlign="Center">
            <asp:Table ID="CadreCETNouveau" runat="server" CellPadding="0" Width="430px" Height="25px"
            CellSpacing="0" HorizontalAlign="Center" BorderStyle="Notset" BorderWidth="1px" Visible="true" BorderColor="#9EB0AC"
            style="margin-top: 1px;margin-bottom: 0px;">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                       <asp:Label ID="LabelTitreCETNouveau" runat="server" Height="25px" Width="430px"
                                BackColor="#CAEBE4" BorderColor="Transparent" BorderStyle="None" Text="Situation du CET (nouveau régime)"
                                BorderWidth="1px" ForeColor="#1C5151" Font-Italic="False"
                                Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Small"
                                style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                font-style: normal; text-indent: 1px; text-align:  center;">
                       </asp:Label>      
                    </asp:TableCell>   
                </asp:TableRow>
            </asp:Table>
      </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
      <asp:TableCell HorizontalAlign="Center">
            <asp:Table ID="TableCETNouveau1" runat="server" CellPadding="0" Width="420px" Height="21px"
            CellSpacing="0" HorizontalAlign="Center" BorderStyle="Notset" BorderWidth="1px" Visible="true" BorderColor="#9EB0AC"
            style="margin-top: -5px;margin-bottom: 0px;">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center" Height="21px" BackColor= "#E2F5F1">
                       <asp:Label ID="LabelOuverture" runat="server" Height="21px" Width="311px"
                                BackColor= "#E2F5F1" BorderColor="#9EB0AC" BorderStyle="None" Text="Ouverture du compte"
                                BorderWidth="1px" ForeColor="#6C9690" Font-Italic="False"
                                Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px; margin-right: 10px;
                                font-style: normal; text-indent: 1px; text-align:  right;">
                       </asp:Label>      
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center" Height="21px">
                       <asp:Label ID="LabelDateOuverture" runat="server" Height="21px" Width="109px"
                                BackColor="White" BorderColor="Transparent" BorderStyle="None" Text="01/01/2008"
                                BorderWidth="1px" ForeColor="#1C5151" Font-Italic="False"
                                Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                font-style: normal; text-indent: 1px; text-align:  center;">
                       </asp:Label>      
                    </asp:TableCell>   
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center" Height="21px" BackColor= "#E2F5F1">
                       <asp:Label ID="LabelTitreNewTotalEpargneCP" runat="server" Height="21px" Width="311px"
                                BackColor= "#E2F5F1" BorderColor="#9EB0AC" BorderStyle="None" Text="Total au titre des congés annuels"
                                BorderWidth="1px" ForeColor="#6C9690" Font-Italic="False"
                                Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px; margin-right: 10px;
                                font-style: normal; text-indent: 1px; text-align:  right;">
                       </asp:Label>      
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center" Height="21px">
                       <asp:Label ID="LabelNewTotalEpargneCP" runat="server" Height="21px" Width="109px"
                                BackColor="White" BorderColor="Transparent" BorderStyle="None" Text="10,5"
                                BorderWidth="1px" ForeColor="#1C5151" Font-Italic="False"
                                Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                font-style: normal; text-indent: 1px; text-align:  center;">
                       </asp:Label>      
                    </asp:TableCell>   
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center" Height="21px" BackColor= "#E2F5F1">
                       <asp:Label ID="LabelTitreNewTotalEpargneRTT" runat="server" Height="21px" Width="311px"
                                BackColor= "#E2F5F1" BorderColor="#9EB0AC" BorderStyle="None" Text="Total au titre de la RTT"
                                BorderWidth="1px" ForeColor="#6C9690" Font-Italic="False"
                                Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px; margin-right: 10px;
                                font-style: normal; text-indent: 1px; text-align:  right;">
                       </asp:Label>      
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center" Height="21px">
                       <asp:Label ID="LabelNewTotalEpargneRTT" runat="server" Height="21px" Width="109px"
                                BackColor="White" BorderColor="Transparent" BorderStyle="None" Text="2,0"
                                BorderWidth="1px" ForeColor="#1C5151" Font-Italic="False"
                                Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                font-style: normal; text-indent: 1px; text-align:  center;">
                       </asp:Label>      
                    </asp:TableCell>   
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center" Height="21px" BackColor= "#E2F5F1">
                       <asp:Label ID="LabelTitreNewTotalEpargne" runat="server" Height="21px" Width="311px"
                                BackColor= "#E2F5F1" BorderColor="#9EB0AC" BorderStyle="None" Text="Total épargné"
                                BorderWidth="1px" ForeColor="#6C9690" Font-Italic="False"
                                Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px; margin-right: 10px;
                                font-style: normal; text-indent: 1px; text-align:  right;">
                       </asp:Label>      
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center" Height="21px">
                       <asp:Label ID="LabelNewTotalEpargne" runat="server" Height="21px" Width="109px"
                                BackColor="White" BorderColor="Transparent" BorderStyle="None" Text="12,5"
                                BorderWidth="1px" ForeColor="#1C5151" Font-Italic="False"
                                Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                font-style: normal; text-indent: 1px; text-align:  center;">
                       </asp:Label>      
                    </asp:TableCell>   
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center" Height="21px" BackColor= "#E2F5F1">
                       <asp:Label ID="LabelTitreNewTotalConsomme" runat="server" Height="21px" Width="311px"
                                BackColor= "#E2F5F1" BorderColor="#9EB0AC" BorderStyle="None" Text="Total consommé"
                                BorderWidth="1px" ForeColor="#6C9690" Font-Italic="False"
                                Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px; margin-right: 10px;
                                font-style: normal; text-indent: 1px; text-align:  right;">
                       </asp:Label>      
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center" Height="21px">
                       <asp:Label ID="LabelNewTotalConsomme" runat="server" Height="21px" Width="109px"
                                BackColor="White" BorderColor="Transparent" BorderStyle="None" Text="5,0"
                                BorderWidth="1px" ForeColor="#1C5151" Font-Italic="False"
                                Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                font-style: normal; text-indent: 1px; text-align:  center;">
                       </asp:Label>      
                    </asp:TableCell>   
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center" Height="21px" BackColor= "#E2F5F1">
                       <asp:Label ID="LabelTitreNewTotalIndemnise" runat="server" Height="21px" Width="311px"
                                BackColor= "#E2F5F1" BorderColor="#9EB0AC" BorderStyle="None" Text="Total au titre de l'indemnisation"
                                BorderWidth="1px" ForeColor="#6C9690" Font-Italic="False"
                                Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px; margin-right: 10px;
                                font-style: normal; text-indent: 1px; text-align:  right;">
                       </asp:Label>      
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center" Height="21px">
                       <asp:Label ID="LabelNewTotalIndemnise" runat="server" Height="21px" Width="109px"
                                BackColor="White" BorderColor="Transparent" BorderStyle="None" Text="4,0"
                                BorderWidth="1px" ForeColor="#1C5151" Font-Italic="False"
                                Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                font-style: normal; text-indent: 1px; text-align:  center;">
                       </asp:Label>      
                    </asp:TableCell>   
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center" Height="21px" BackColor= "#E2F5F1">
                       <asp:Label ID="LabelTitreNewTotalRAFP" runat="server" Height="21px" Width="311px"
                                BackColor= "#E2F5F1" BorderColor="#9EB0AC" BorderStyle="None" Text="Total au titre de la retraite additionnelle"
                                BorderWidth="1px" ForeColor="#6C9690" Font-Italic="False"
                                Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px; margin-right: 10px;
                                font-style: normal; text-indent: 1px; text-align:  right;">
                       </asp:Label>      
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center" Height="21px">
                       <asp:Label ID="LabelNewTotalRAFP" runat="server" Height="21px" Width="109px"
                                BackColor="White" BorderColor="Transparent" BorderStyle="None" Text="1,0"
                                BorderWidth="1px" ForeColor="#1C5151" Font-Italic="False"
                                Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                font-style: normal; text-indent: 1px; text-align:  center;">
                       </asp:Label>      
                    </asp:TableCell>   
                </asp:TableRow>
            </asp:Table>
      </asp:TableCell>
    </asp:TableRow>    
    <asp:TableRow>
      <asp:TableCell HorizontalAlign="Center">
            <asp:Table ID="TableCETNouveau2" runat="server" CellPadding="0" Width="430px" Height="21px"
            CellSpacing="0" HorizontalAlign="Center" BorderStyle="Notset" BorderWidth="1px" Visible="true" BorderColor="#9EB0AC"
            style="margin-top: -5px;margin-bottom: 0px;">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center" Height="21px" BackColor= "#9EB0AC">
                       <asp:Label ID="LabelTitreNewSolde" runat="server" Height="21px" Width="311px"
                                BackColor= "#9EB0AC" BorderColor="#9EB0AC" BorderStyle="None" Text="Solde"
                                BorderWidth="1px" ForeColor="#E2F5F1" Font-Italic="False"
                                Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Small"
                                style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px; margin-right: 10px;
                                font-style: normal; text-indent: 1px; text-align:  right;">
                       </asp:Label>      
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center" Height="21px" BackColor="#E2F5F1">
                       <asp:Label ID="LabelNewSolde" runat="server" Height="21px" Width="109px"
                                BackColor="#E2F5F1" BorderColor="Transparent" BorderStyle="None" Text="2,5"
                                BorderWidth="1px" ForeColor="#1C5151" Font-Italic="False"
                                Font-Bold="true" Font-Names="Trebuchet MS" Font-Size="Small"
                                style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                font-style: normal; text-indent: 1px; text-align:  center;">
                       </asp:Label>      
                    </asp:TableCell>   
                </asp:TableRow>
            </asp:Table>
      </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
      <asp:TableCell Height="20px"></asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
      <asp:TableCell HorizontalAlign="Center">
            <asp:Table ID="CadreCETAncien" runat="server" CellPadding="0" Width="430px" Height="25px"
            CellSpacing="0" HorizontalAlign="Center" BorderStyle="Notset" BorderWidth="1px" Visible="true" BorderColor="#9EB0AC"
            style="margin-top: 1px;margin-bottom: 0px;">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                       <asp:Label ID="LabelTitreCETAncien" runat="server" Height="25px" Width="430px"
                                BackColor="#CAEBE4" BorderColor="Transparent" BorderStyle="None" Text="Situation du CET fin 2008 (ancien régime)"
                                BorderWidth="1px" ForeColor="#1C5151" Font-Italic="False"
                                Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Small"
                                style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                font-style: normal; text-indent: 1px; text-align:  center;">
                       </asp:Label>      
                    </asp:TableCell>   
                </asp:TableRow>
            </asp:Table>
      </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
      <asp:TableCell HorizontalAlign="Center">
            <asp:Table ID="TableCETAncien1" runat="server" CellPadding="0" Width="420px" Height="21px"
            CellSpacing="0" HorizontalAlign="Center" BorderStyle="Notset" BorderWidth="1px" Visible="true" BorderColor="#9EB0AC"
            style="margin-top: -5px;margin-bottom: 0px;">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center" Height="21px" BackColor= "#E2F5F1">
                       <asp:Label ID="LabelTitreOldTotalEpargne" runat="server" Height="21px" Width="311px"
                                BackColor= "#E2F5F1" BorderColor="#9EB0AC" BorderStyle="None" Text="Total épargné à fin 2008"
                                BorderWidth="1px" ForeColor="#6C9690" Font-Italic="False"
                                Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px; margin-right: 10px;
                                font-style: normal; text-indent: 1px; text-align:  right;">
                       </asp:Label>      
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center" Height="21px">
                       <asp:Label ID="LabelOldTotalEpargne" runat="server" Height="21px" Width="109px"
                                BackColor="White" BorderColor="Transparent" BorderStyle="None" Text="48,0"
                                BorderWidth="1px" ForeColor="#1C5151" Font-Italic="False"
                                Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                font-style: normal; text-indent: 1px; text-align:  center;">
                       </asp:Label>      
                    </asp:TableCell>   
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center" Height="21px" BackColor= "#E2F5F1">
                       <asp:Label ID="LabelTitreOldTotalEpargneCP" runat="server" Height="21px" Width="311px"
                                BackColor= "#E2F5F1" BorderColor="#9EB0AC" BorderStyle="None" Text="Dont au titre des congés"
                                BorderWidth="1px" ForeColor="#6C9690" Font-Italic="False"
                                Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px; margin-right: 10px;
                                font-style: normal; text-indent: 1px; text-align:  right;">
                       </asp:Label>      
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center" Height="21px">
                       <asp:Label ID="LabelOldTotalEpargneCP" runat="server" Height="21px" Width="109px"
                                BackColor="White" BorderColor="Transparent" BorderStyle="None" Text="30,0"
                                BorderWidth="1px" ForeColor="#1C5151" Font-Italic="False"
                                Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                font-style: normal; text-indent: 1px; text-align:  center;">
                       </asp:Label>      
                    </asp:TableCell>   
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center" Height="21px" BackColor= "#E2F5F1">
                       <asp:Label ID="LabelTitreOldTotalEpargnePaye" runat="server" Height="21px" Width="311px"
                                BackColor= "#E2F5F1" BorderColor="#9EB0AC" BorderStyle="None" Text="Dont au titre des jours indemnisés"
                                BorderWidth="1px" ForeColor="#6C9690" Font-Italic="False"
                                Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px; margin-right: 10px;
                                font-style: normal; text-indent: 1px; text-align:  right;">
                       </asp:Label>      
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center" Height="21px">
                       <asp:Label ID="LabelOldTotalEpargnePaye" runat="server" Height="21px" Width="109px"
                                BackColor="White" BorderColor="Transparent" BorderStyle="None" Text="10,0"
                                BorderWidth="1px" ForeColor="#1C5151" Font-Italic="False"
                                Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                font-style: normal; text-indent: 1px; text-align:  center;">
                       </asp:Label>      
                    </asp:TableCell>   
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center" Height="21px" BackColor= "#E2F5F1">
                       <asp:Label ID="LabelTitreOldTotalRAFP" runat="server" Height="21px" Width="311px"
                                BackColor= "#E2F5F1" BorderColor="#9EB0AC" BorderStyle="None" Text="Dont au titre de la RAFP"
                                BorderWidth="1px" ForeColor="#6C9690" Font-Italic="False"
                                Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px; margin-right: 10px;
                                font-style: normal; text-indent: 1px; text-align:  right;" >
                       </asp:Label>      
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center" Height="21px">
                       <asp:Label ID="LabelOldTotalRAFP" runat="server" Height="21px" Width="109px"
                                BackColor="White" BorderColor="Transparent" BorderStyle="None" Text="8,0"
                                BorderWidth="1px" ForeColor="#1C5151" Font-Italic="False"
                                Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                font-style: normal; text-indent: 1px; text-align:  center;">
                       </asp:Label>      
                    </asp:TableCell>   
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="1px" ColumnSpan="2" BackColor="#9EB0AC"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center" Height="21px" BackColor= "#E2F5F1">
                       <asp:Label ID="LabelTitreOldCPPris" runat="server" Height="21px" Width="311px"
                                BackColor= "#E2F5F1" BorderColor="#9EB0AC" BorderStyle="None" Text="Congés pris sur ancien CET"
                                BorderWidth="1px" ForeColor="#6C9690" Font-Italic="False"
                                Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px; margin-right: 10px;
                                font-style: normal; text-indent: 1px; text-align:  right;">
                       </asp:Label>      
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center" Height="21px" BackColor="White">
                       <asp:Label ID="LabelOldTotalCPPris" runat="server" Height="21px" Width="109px"
                                BackColor="White" BorderColor="Transparent" BorderStyle="None" Text="14,0"
                                BorderWidth="1px" ForeColor="#1C5151" Font-Italic="False"
                                Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                font-style: normal; text-indent: 1px; text-align:  center;">
                       </asp:Label>      
                    </asp:TableCell>   
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center" Height="21px" BackColor= "#E2F5F1">
                       <asp:Label ID="LabelTitreOldSoldeCP" runat="server" Height="21px" Width="311px"
                                BackColor= "#E2F5F1" BorderColor="#9EB0AC" BorderStyle="None" Text="Solde au titre des congés"
                                BorderWidth="1px" ForeColor="#6C9690" Font-Italic="False"
                                Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px; margin-right: 10px;
                                font-style: normal; text-indent: 1px; text-align:  right;">
                       </asp:Label>      
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center" Height="21px" BackColor="White">
                       <asp:Label ID="LabelOldTotalSoldeCP" runat="server" Height="21px" Width="109px"
                                BackColor="White" BorderColor="Transparent" BorderStyle="None" Text="16,0"
                                BorderWidth="1px" ForeColor="#1C5151" Font-Italic="False"
                                Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                font-style: normal; text-indent: 1px; text-align:  center;">
                       </asp:Label>      
                    </asp:TableCell>   
                </asp:TableRow>
            </asp:Table>
      </asp:TableCell>
    </asp:TableRow>
 </asp:Table>