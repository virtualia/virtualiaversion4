﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="PER_PLANNING_SEMAINE.ascx.vb" Inherits="Virtualia.Net.Fenetre_PER_PLANNING_SEMAINE" %>

<%@ Register src="../../Saisies/VListeCombo.ascx" tagname="VListeCombo" tagprefix="Virtualia" %>
<%@ Register src="../../TempsTravail/VLigneDetailSemaine.ascx" tagname="VLigneSemaine" tagprefix="Virtualia" %>

<asp:Table ID="CadreInfo" runat="server" BorderStyle="None" BorderWidth="2px" Visible="true"
  BorderColor="#B0E0D7" Height="260px" Width="750px" HorizontalAlign="Center" style="margin-top: 13px;">
    
  <asp:TableRow>
    <asp:TableCell>
      <asp:Table ID="CadreTitre" runat="server" Height="30px" CellPadding="0" Width="750px" 
         CellSpacing="0" HorizontalAlign="Center" BorderColor="Black" Visible="true" BorderStyle="NotSet">
            <asp:TableRow>
                <asp:TableCell HorizontalAlign="Center">
                    <asp:Label ID="Etiquette" runat="server" Text="Planning détaillé à la semaine" Height="20px" Width="320px"
                        BackColor="#216B68" BorderColor="#B0E0D7"  BorderStyle="Groove"
                        BorderWidth="2px" ForeColor="#D7FAF3"
                        Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Small"
                        style="margin-top: 5px; margin-left: 4px; margin-bottom: 15px;
                        font-style: oblique; text-indent: 5px; text-align: center;">
                    </asp:Label>          
                </asp:TableCell>      
            </asp:TableRow>
      </asp:Table>
    </asp:TableCell>
  </asp:TableRow>
  <asp:TableRow>
    <asp:TableCell>
        <asp:Table ID="TableAnnualisation" runat="server" CellPadding="0" CellSpacing="0" Width="550px"
        BorderStyle="Notset" BorderWidth="1px" BorderColor="#1C5151" HorizontalAlign="Center" Visible="true">
          <asp:TableRow ID="LigneContrat" Width="550px" Height="23px">
            <asp:TableCell HorizontalAlign="Center" ColumnSpan="3">
                <asp:Label ID="LabelContrat" runat="server" Text="Contrat d'annualisation du 01/01/2010 au 31/12/2010" 
                    BackColor="#B0E0D7" BorderColor="#B0E0D7"  BorderStyle="Notset" Height="23px" Width="550px"
                    BorderWidth="1px" ForeColor="#1C5151"
                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                    font-style: normal; text-indent: 0px; text-align: center; padding-top: 2px;">
                </asp:Label>          
            </asp:TableCell>      
          </asp:TableRow>
          <asp:TableRow ID="LigneAnnualisation" Width="546px" Height="23px">
            <asp:TableCell HorizontalAlign="Center">
                <asp:Label ID="LabelTempsAnnualise" runat="server" Text="Temps annualisé - 1607 h 00" 
                    BackColor="#D7FAF3" BorderColor="#B0E0D7"  BorderStyle="Notset" Height="23px" Width="182px"
                    BorderWidth="1px" ForeColor="#1C5151"
                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; padding-top: 2px;
                    font-style: normal; text-indent: 0px; text-align: center;">
                </asp:Label>          
            </asp:TableCell> 
            <asp:TableCell HorizontalAlign="Center">
                <asp:Label ID="LabelTempsValide" runat="server" Text="Total validé - 1300 h 00" 
                    BackColor="#D7FAF3" BorderColor="#B0E0D7"  BorderStyle="Notset" Height="23px" Width="182px"
                    BorderWidth="1px" ForeColor="#1C5151"
                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; padding-top: 2px;
                    font-style: normal; text-indent: 0px; text-align: center;">
                </asp:Label>          
            </asp:TableCell>
            <asp:TableCell HorizontalAlign="Center">
                <asp:Label ID="LabelSoldeTemps" runat="server" Text="Solde - 307 h 00" 
                    BackColor="#D7FAF3" BorderColor="#B0E0D7"  BorderStyle="Notset" Height="23px" Width="182px"
                    BorderWidth="1px" ForeColor="#1C5151"
                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; padding-top: 2px;
                    font-style: normal; text-indent: 0px; text-align: center;">
                </asp:Label>          
            </asp:TableCell>     
          </asp:TableRow>
        </asp:Table>
    </asp:TableCell>
  </asp:TableRow> 
  
  <asp:TableRow>
    <asp:TableCell Height="30px"></asp:TableCell>
  </asp:TableRow> 
  <asp:TableRow>
    <asp:TableCell>
        <Virtualia:VListeCombo ID="LstSemaines" runat="server" EtiVisible="true" EtiText="Semaine" EtiWidth="90px"
                V_NomTable="Semaine" LstWidth="280px" LstBorderColor="#B0E0D7" LstBackColor="#EEECFD"
                EtiStyle="text-align:center" EtiBackColor="#8DA8A3" EtiForeColor="#E9FDF9" />
    </asp:TableCell>
  </asp:TableRow>
  <asp:TableRow>
    <asp:TableCell>
        <asp:Table ID="TableauHorairesHebdomadaire" runat="server" CellPadding="0" CellSpacing="0" Width="600px"
        BorderStyle="Notset" BorderWidth="1px" BorderColor="#1C5151" HorizontalAlign="Center" Visible="true">
            
          <asp:TableRow ID="LigneTitre" Width="600px" Height="23px">
            <asp:TableCell HorizontalAlign="Center" ColumnSpan="6" BackColor="#D7FAF3">
                <asp:Label ID="LabelTitreSemaine" runat="server" Text=""
                    BackColor="#D7FAF3" BorderColor="Transparent"  BorderStyle="Notset" Height="23px" Width="590px"
                    BorderWidth="1px" ForeColor="#1C5151"
                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; padding-top: 2px;
                    font-style: normal; text-indent: 0px; text-align: center;">
                </asp:Label>          
            </asp:TableCell>      
          </asp:TableRow>
          <asp:TableRow ID="LigneEntete" Width="600px" Height="23px">
            <asp:TableCell HorizontalAlign="Center" ColumnSpan="1" BackColor="#CAEBE4">
                <asp:Label ID="LabelEnteteJour" runat="server" Text=""
                    BackColor="#CAEBE4" BorderColor="White" BorderStyle="Notset" Height="23px" Width="100px"
                    BorderWidth="1px" ForeColor="#6D9092"
                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; padding-top: 2px;
                    font-style: normal; text-indent: 0px; text-align: center;">
                </asp:Label>          
            </asp:TableCell>
            <asp:TableCell HorizontalAlign="Center" ColumnSpan="1" BackColor="#CAEBE4">
                <asp:Label ID="LabelEnteteBadgeage" runat="server" Text="badgeage"
                    BackColor="#CAEBE4" BorderColor="White" BorderStyle="Notset" Height="23px" Width="100px"
                    BorderWidth="1px" ForeColor="#6D9092"
                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; padding-top: 2px;
                    font-style: normal; text-indent: 0px; text-align: center;">
                </asp:Label>          
            </asp:TableCell>
            <asp:TableCell HorizontalAlign="Center" ColumnSpan="3" BackColor="#CAEBE4">
                <asp:Label ID="LabelEnteteValidation" runat="server" Text="horaires validés"
                    BackColor="#CAEBE4" BorderColor="White" BorderStyle="Notset" Height="23px" Width="220px"
                    BorderWidth="1px" ForeColor="#6D9092"
                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; padding-top: 2px;
                    font-style: normal; text-indent: 0px; text-align: center;">
                </asp:Label>          
            </asp:TableCell>
            <asp:TableCell HorizontalAlign="Center" ColumnSpan="1" BackColor="#CAEBE4">
                <asp:Label ID="LabelEnteteInfos" runat="server" Text="informations"
                    BackColor="#CAEBE4" BorderColor="White" BorderStyle="Notset" Height="23px" Width="167px"
                    BorderWidth="1px" ForeColor="#6D9092"
                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; padding-top: 2px;
                    font-style: normal; text-indent: 0px; text-align: center;">
                </asp:Label>          
            </asp:TableCell>        
          </asp:TableRow>
          <asp:TableRow>
            <asp:TableCell ColumnSpan="6">
                <Virtualia:VLigneSemaine ID="Jour00" runat="server" />
            </asp:TableCell>
          </asp:TableRow>  
          <asp:TableRow>
            <asp:TableCell ColumnSpan="6">
                <Virtualia:VLigneSemaine ID="Jour01" runat="server" />
            </asp:TableCell>
          </asp:TableRow>  
          <asp:TableRow>
            <asp:TableCell ColumnSpan="6">
                <Virtualia:VLigneSemaine ID="Jour02" runat="server" />
            </asp:TableCell>
          </asp:TableRow>
          <asp:TableRow>
            <asp:TableCell ColumnSpan="6">
                <Virtualia:VLigneSemaine ID="Jour03" runat="server" />
            </asp:TableCell>
          </asp:TableRow> 
          <asp:TableRow>
            <asp:TableCell ColumnSpan="6">
                <Virtualia:VLigneSemaine ID="Jour04" runat="server" />
            </asp:TableCell>
          </asp:TableRow>
          <asp:TableRow>
            <asp:TableCell ColumnSpan="6">
                <Virtualia:VLigneSemaine ID="Jour05" runat="server" />
            </asp:TableCell>
          </asp:TableRow> 
          <asp:TableRow>
            <asp:TableCell ColumnSpan="6">
                <Virtualia:VLigneSemaine ID="Jour06" runat="server" />
            </asp:TableCell>
          </asp:TableRow>        
        </asp:Table>
     </asp:TableCell>     
  </asp:TableRow> 
    
  <asp:TableRow>
    <asp:TableCell>
      <asp:Table ID="TableTotaux" runat="server" CellPadding="0" CellSpacing="0" Width="600px"
      BorderStyle="Notset" BorderWidth="1px" BorderColor="Black" HorizontalAlign="Center">
        <asp:TableRow ID="LigneTotalPrevu" Width="590px" Height="20px">
            <asp:TableCell ID="TitreTotalPrevu" BackColor="#D7FAF3" BorderColor="#A8BBB8" BorderStyle="None" HorizontalAlign="Left" Height="20px"> 
              <asp:Label ID="LabelTotalPrevu" runat="server" Height="20px" Width="190px"
                  BackColor="#D7FAF3" BorderColor="#A8BBB8" BorderStyle="None" Text="Total prévu"
                  BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                  Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                  style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                  font-style: normal; text-indent: 40px; text-align:  left;">
              </asp:Label>          
            </asp:TableCell>
            <asp:TableCell ID="TotalPrevu" BackColor="#D7FAF3" BorderColor="#A8BBB8" BorderStyle="None" Visible="true" HorizontalAlign="Right"> 
              <asp:Label ID="LabelPrevu" runat="server" Height="20px" Width="70px"
                        BackColor="White" BorderColor="#A8BBB8" BorderStyle="Solid" Text="28 h 00"
                        BorderWidth="1px" ForeColor="#1C5151"
                        Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Small" Font-Italic="false"
                        style="margin-top: 1px; margin-right: 8px; text-indent: 0px; text-align: center;"> 
              </asp:Label>
            </asp:TableCell>           
          </asp:TableRow>
          <asp:TableRow ID="LigneTotalValide" Width="590px" Height="20px">
            <asp:TableCell ID="CellTotalValide" BackColor="#CAEBE4" BorderColor="#A8BBB8" BorderStyle="None" HorizontalAlign="Left" Height="20px"> 
              <asp:Label ID="LabelTotalValide" runat="server" Height="20px" Width="190px"
                  BackColor="#CAEBE4" BorderColor="#A8BBB8" BorderStyle="None" Text="Total validé"
                  BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                  Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                  style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                  font-style: normal; text-indent: 40px; text-align: left;">
              </asp:Label>          
            </asp:TableCell>
            <asp:TableCell ID="TotalValide" BackColor="#CAEBE4" BorderColor="#A8BBB8" BorderStyle="None" Visible="true" HorizontalAlign="Right"> 
              <asp:Label ID="LabelValide" runat="server" Height="20px" Width="70px"
                        BackColor="White" BorderColor="#A8BBB8" BorderStyle="Solid" Text="30 h 22"
                        BorderWidth="1px" ForeColor="#1C5151"
                        Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Small" Font-Italic="false"
                        style="margin-top: 1px; margin-right: 8px; text-indent: 0px; text-align: center;"> 
              </asp:Label>
            </asp:TableCell>           
          </asp:TableRow>
          <asp:TableRow ID="LigneTotalComptabilise" Width="590px" Height="20px">
            <asp:TableCell ID="CellTotalComptabilise" BackColor="#B0E0D7" BorderColor="#A8BBB8" BorderStyle="None" HorizontalAlign="Left" Height="20px"> 
              <asp:Label ID="LabelTotalComptabilise" runat="server" Height="20px" Width="190px"
                  BackColor="#B0E0D7" BorderColor="#A8BBB8" BorderStyle="None" Text="Total comptabilisé"
                  BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                  Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                  style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                  font-style: normal; text-indent: 40px; text-align:  left;">
              </asp:Label>          
            </asp:TableCell>
            <asp:TableCell ID="TotalComptabilise" BackColor="#B0E0D7" BorderColor="#A8BBB8" BorderStyle="None" Visible="true" HorizontalAlign="right"> 
              <asp:Label ID="LabelComptabilise" runat="server" Height="20px" Width="70px"
                        BackColor="White" BorderColor="#A8BBB8" BorderStyle="Solid" Text="33 h 44"
                        BorderWidth="1px" ForeColor="#1C5151"
                        Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Small" Font-Italic="false"
                        style="margin-top: 1px; margin-right: 8px; text-indent: 0px; text-align: center;"> 
              </asp:Label>
            </asp:TableCell>           
          </asp:TableRow>
      </asp:Table>
    </asp:TableCell>
  </asp:TableRow> 
  
  <asp:TableRow>
    <asp:TableCell>
      <asp:Table ID="Separateur" runat="server" CellPadding="0" CellSpacing="0" Width="600px"
      BorderStyle="Notset" BorderWidth="1px" BorderColor="Black" HorizontalAlign="Center">
        <asp:TableRow>
            <asp:TableCell Height="20px" HorizontalAlign="Center">
               <asp:Label ID="LabelCommentaire" runat="server" Height="16px" Width="500px"
                  BackColor="White" BorderColor="Transparent" BorderStyle="Notset" Text=""
                  BorderWidth="1px" ForeColor="Black"
                  Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Small" Font-Italic="false"
                  style="margin-top: 1px; margin-left: 0px; text-indent: 0px; text-align: center;"> 
               </asp:Label>
            </asp:TableCell>
        </asp:TableRow>
      </asp:Table>
    </asp:TableCell>
  </asp:TableRow>

 </asp:Table>