﻿'------------------------------------------------------------------------------
' <généré automatiquement>
'     Ce code a été généré par un outil.
'
'     Les modifications apportées à ce fichier peuvent provoquer un comportement incorrect et seront perdues si
'     le code est régénéré.
' </généré automatiquement>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class Fenetre_PER_CET_MANAGER

    '''<summary>
    '''Contrôle CadreInfo.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CadreInfo As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle CadreCETNouveau.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CadreCETNouveau As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle LabelTitreCETNouveau.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelTitreCETNouveau As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle TableCETNouveau1.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents TableCETNouveau1 As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle LabelOuverture.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelOuverture As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle LabelDateOuverture.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelDateOuverture As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle LabelTitreNewTotalEpargneCP.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelTitreNewTotalEpargneCP As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle LabelNewTotalEpargneCP.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelNewTotalEpargneCP As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle LabelTitreNewTotalEpargneRTT.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelTitreNewTotalEpargneRTT As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle LabelNewTotalEpargneRTT.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelNewTotalEpargneRTT As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle LabelTitreNewTotalEpargne.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelTitreNewTotalEpargne As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle LabelNewTotalEpargne.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelNewTotalEpargne As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle LabelTitreNewTotalConsomme.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelTitreNewTotalConsomme As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle LabelNewTotalConsomme.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelNewTotalConsomme As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle LabelTitreNewTotalIndemnise.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelTitreNewTotalIndemnise As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle LabelNewTotalIndemnise.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelNewTotalIndemnise As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle LabelTitreNewTotalRAFP.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelTitreNewTotalRAFP As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle LabelNewTotalRAFP.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelNewTotalRAFP As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle TableCETNouveau2.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents TableCETNouveau2 As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle LabelTitreNewSolde.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelTitreNewSolde As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle LabelNewSolde.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelNewSolde As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle CadreCETAncien.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CadreCETAncien As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle LabelTitreCETAncien.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelTitreCETAncien As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle TableCETAncien1.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents TableCETAncien1 As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle LabelTitreOldTotalEpargne.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelTitreOldTotalEpargne As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle LabelOldTotalEpargne.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelOldTotalEpargne As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle LabelTitreOldTotalEpargneCP.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelTitreOldTotalEpargneCP As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle LabelOldTotalEpargneCP.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelOldTotalEpargneCP As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle LabelTitreOldTotalEpargnePaye.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelTitreOldTotalEpargnePaye As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle LabelOldTotalEpargnePaye.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelOldTotalEpargnePaye As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle LabelTitreOldTotalRAFP.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelTitreOldTotalRAFP As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle LabelOldTotalRAFP.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelOldTotalRAFP As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle LabelTitreOldCPPris.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelTitreOldCPPris As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle LabelOldTotalCPPris.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelOldTotalCPPris As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle LabelTitreOldSoldeCP.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelTitreOldSoldeCP As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle LabelOldTotalSoldeCP.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelOldTotalSoldeCP As Global.System.Web.UI.WebControls.Label
End Class
