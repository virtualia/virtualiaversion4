﻿<%@ Control Language="VB" AutoEventWireup="false" Inherits="Virtualia.Net.Fenetre_PER_HORSFRANCE" CodeBehind="PER_HORSFRANCE.ascx.vb" %>

<%@ Register Src="~/Controles/Saisies/VCoupleEtiDonnee.ascx" TagName="VCoupleEtiDonnee" TagPrefix="Virtualia" %>
<%@ Register Src="~/Controles/Saisies/VDuoEtiquetteCommande.ascx" TagName="VDuoEtiquetteCommande" TagPrefix="Virtualia" %>
<%@ Register Src="~/Controles/Saisies/VCoupleEtiquetteExperte.ascx" TagName="VCoupleEtiquetteExperte" TagPrefix="Virtualia" %>

<%@ Register Src="~/ControlsGeneriques/V_CommandeOK.ascx" TagName="VCmdOK" TagPrefix="Generic" %>

<asp:Table ID="CadreInfo" runat="server" BorderStyle="Ridge" BorderWidth="2px" BorderColor="#B0E0D7" Height="210px" Width="500px" HorizontalAlign="Center">
    <asp:TableRow>
        <asp:TableCell>
            <Generic:VCmdOK ID="CtlOK" runat="server" Height="22px" Width="70px" HorizontalAlign="Right" CadreStyle="margin-top: 3px; margin-right: 3px" />
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="CadreTitre" runat="server" Height="40px" CellPadding="0" CellSpacing="0" HorizontalAlign="Center">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="Etiquette" runat="server" Text="Adresse à l'étranger" Height="20px" Width="300px" BackColor="#2FA49B" BorderColor="#B0E0D7" BorderStyle="Groove" BorderWidth="2px" ForeColor="#D7FAF3" Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Small" Style="margin-top: 15px; margin-left: 4px; margin-bottom: 10px; font-style: oblique; text-indent: 5px; text-align: center;" />
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="CP" runat="server" Height="22px" Width="495px" CellPadding="0" CellSpacing="0">
                <asp:TableRow>
                    <asp:TableCell ColumnSpan="2">
                        <Virtualia:VCoupleEtiDonnee ID="InfoH01" runat="server" V_PointdeVue="1" V_Objet="44" V_Information="1" V_SiDonneeDico="true" DonWidth="300px" DonTabIndex="301" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell ColumnSpan="2">
                        <Virtualia:VCoupleEtiDonnee ID="InfoH02" runat="server" V_PointdeVue="1" V_Objet="44" V_Information="2" V_SiDonneeDico="true" DonWidth="300px" DonTabIndex="302" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell ColumnSpan="2">
                        <Virtualia:VCoupleEtiDonnee ID="InfoH03" runat="server" V_PointdeVue="1" V_Objet="44" V_Information="3" V_SiDonneeDico="true" DonWidth="300px" DonTabIndex="303" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Width="145px" HorizontalAlign="Left">
                        <Virtualia:VCoupleEtiDonnee ID="InfoH06" runat="server" V_PointdeVue="1" V_Objet="44" V_Information="6" V_SiDonneeDico="true" DonWidth="40px" EtiText="Code pays et Pays" DonTabIndex="304" />
                    </asp:TableCell>
                    <asp:TableCell>
                        <Virtualia:VDuoEtiquetteCommande ID="Dontab04" runat="server" V_PointdeVue="1" V_Objet="44" V_Information="4" V_SiDonneeDico="true" EtiWidth="0px" EtiVisible="false" DonWidth="240px" DonTabIndex="305" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell ColumnSpan="2">
                        <Virtualia:VCoupleEtiDonnee ID="InfoH05" runat="server" V_PointdeVue="1" V_Objet="44" V_Information="5" V_SiDonneeDico="true" DonWidth="150px" DonTabIndex="308" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="12px" ColumnSpan="2" />
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <asp:Panel ID="CadreExpert" runat="server" BorderStyle="None" BorderWidth="1px" BorderColor="#B0E0D7" Height="44px" Width="500px" HorizontalAlign="Center" Style="margin-top: 5px; vertical-align: middle">
                <Virtualia:VCoupleEtiquetteExperte ID="ExprH01" runat="server" V_PointdeVue="1" V_Objet="44" V_InfoExperte="518" V_SiDonneeDico="true" EtiHeight="20px" EtiWidth="170px" DonWidth="300px" DonHeight="35px" />
            </asp:Panel>
        </asp:TableCell>
    </asp:TableRow>
</asp:Table>
