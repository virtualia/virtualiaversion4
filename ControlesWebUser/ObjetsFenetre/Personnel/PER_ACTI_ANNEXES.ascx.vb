﻿Option Strict On
Option Explicit On
Option Compare Text

Imports VI = Virtualia.Systeme.Constantes
Imports Virtualia.ObjetBase
Imports Virtualia.TablesObjet.ShemaPER
Imports Virtualia.OutilsVisu


Partial Class Fenetre_PER_ACTI_ANNEXES
    Inherits ObjetWebControleBase(Of PER_ACTI_ANNEXES)
    Implements IControlCrud

#Region "IControlCrud"
    Public ReadOnly Property ControlCrud As ICtlCommandeCrud Implements IControlCrud.CtlCrud
        Get
            Return Me.CtlCrud
        End Get
    End Property
#End Region

#Region "ObjetWebControleBase"
    Private _webfonction As WebFonctions
    Protected Overrides ReadOnly Property V_WebFonction As IWebFonctions
        Get
            If (_webfonction Is Nothing) Then
                _webfonction = New WebFonctions(Me)
            End If
            Return _webfonction
        End Get
    End Property

    Protected Overrides Function InitCadreInfo() As WebControl
        Return Me.CadreInfo
    End Function

    Protected Overrides Function InitControlDataGrid() As IControlDataGrid
        Return Me.ListeGrille
    End Function

    Protected Overrides Sub InitCacheIde()

        V_CacheIde.TypeVue = WebControlTypeVue.ListeGrid

        Dim captions As List(Of String) = New List(Of String)()
        captions.Add("Aucune activité")
        captions.Add("Une activité")
        captions.Add("activités")

        Me.ListeGrille.InitCaptions(captions)

        Dim colonnes As ColonneDataGridCollection = New ColonneDataGridCollection()

        colonnes.Add(HorizontalAlign.Center, TypeData.Date, "date d'effet", 0)
        colonnes.Add(HorizontalAlign.Center, TypeData.Numeric, "activité", 1)
        colonnes.Add(HorizontalAlign.Center, TypeData.Numeric, "fonction", 3)
        colonnes.Add(HorizontalAlign.Center, TypeData.Numeric, "durée", 8)
        colonnes.Add(HorizontalAlign.Center, TypeData.Numeric, "date de fin", 99)
        colonnes.Add(HorizontalAlign.Center, TypeData.Chaine, "Clef", 0, True)

        Me.ListeGrille.InitColonnes(colonnes)

    End Sub

    Protected Overrides Sub OnPreRender(e As EventArgs)
        MyBase.OnPreRender(e)

        CadreInfo.BackColor = _webfonction.CouleurCharte(0, "Cadre")
        CadreInfo.BorderColor = _webfonction.CouleurCharte(0, "Bordure")
        Etiquette.BackColor = _webfonction.CouleurCharte(0, "Titre")
        Etiquette.ForeColor = _webfonction.CouleurCharte(0, "Police_Claire")
        Etiquette.BorderColor = _webfonction.CouleurCharte(0, "Bordure")
        LabelEtiTextes.BackColor = _webfonction.CouleurCharte(0, "Sous-Titre")
        LabelEtiTextes.ForeColor = _webfonction.CouleurCharte(0, "Police_Fonce")
        LabelEtiTextes.BorderColor = _webfonction.CouleurCharte(0, "Bordure")
    End Sub

    Protected Overrides Sub OnInit(e As System.EventArgs)
        MyBase.OnInit(e)

        Dim initwebfonction As IWebFonctions = V_WebFonction
    End Sub
#End Region

End Class
