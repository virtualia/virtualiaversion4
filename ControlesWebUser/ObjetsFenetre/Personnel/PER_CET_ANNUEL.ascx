﻿<%@ Control Language="VB" AutoEventWireup="false" Inherits="Virtualia.Net.Fenetre_PER_CET_ANNUEL" CodeBehind="PER_CET_ANNUEL.ascx.vb" %>

<%@ Register Src="~/Controles/Saisies/VCoupleEtiDonnee.ascx" TagName="VCoupleEtiDonnee" TagPrefix="Virtualia" %>

<%@ Register Src="~/ControlsGeneriques/V_DataGrid.ascx" TagName="VDataGrid" TagPrefix="Generic" %>
<%@ Register Src="~/ControlsGeneriques/V_CommandeCRUD.ascx" TagName="VCrud" TagPrefix="Generic" %>

<asp:Table ID="CadreControle" runat="server" Height="30px" Width="740px" CellPadding="0" CellSpacing="0" HorizontalAlign="Center">
    <asp:TableRow>
        <asp:TableCell>
            <Generic:VDataGrid ID="ListeGrille" runat="server" CadreWidth="730px" SiColonneSelect="true" SiCaseAcocher="false" />
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="CadreInfo" runat="server" BorderStyle="Ridge" BorderWidth="2px" BorderColor="#B0E0D7" Height="220px" Width="700px" HorizontalAlign="Center" Style="margin-top: 3px;">
                <asp:TableRow>
                    <asp:TableCell>
                        <Generic:VCrud ID="CtlCrud" runat="server" Height="22px" Width="244px" HorizontalAlign="Right" CadreStyle="margin-top: 3px; margin-right: 3px" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="CadreTitre" runat="server" Height="40px" CellPadding="0" Width="700px" CellSpacing="0" HorizontalAlign="Center">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                    <asp:Label ID="Etiquette" runat="server" Text="Compte épargne temps annuel" Height="20px" Width="300px" BackColor="#2FA49B" BorderColor="#B0E0D7" BorderStyle="Groove" BorderWidth="2px" ForeColor="#D7FAF3" Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Small" Style="margin-top: 5px; margin-left: 4px; margin-bottom: 10px; font-style: oblique; text-indent: 5px; text-align: center;" />
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Table ID="CadreDates" runat="server" CellPadding="0" CellSpacing="0" Width="360px" HorizontalAlign="Center">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH00" runat="server" V_PointdeVue="1" V_Objet="42" V_Information="0" V_SiDonneeDico="true" EtiWidth="80px" DonWidth="80px" DonTabIndex="1" EtiStyle="text-align:center;" />
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Center">
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH04" runat="server" V_PointdeVue="1" V_Objet="42" V_Information="4" V_SiDonneeDico="true" EtiWidth="80px" DonWidth="80px" DonTabIndex="2" EtiStyle="text-align:center;" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Height="5px" ColumnSpan="2" />
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Table ID="CadreCETAnnuel" runat="server" CellPadding="0" CellSpacing="0" Width="500px">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center" ColumnSpan="2">
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH11" runat="server" V_PointdeVue="1" V_Objet="42" V_Information="11" V_SiDonneeDico="true" EtiWidth="230px" DonWidth="50px" DonTabIndex="11" />
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Left" ColumnSpan="2">
                                    <asp:Label ID="LabelNbjCETCP" runat="server" Text="jours" Height="20px" Width="35px" BackColor="Transparent" BorderStyle="none" BorderWidth="0px" ForeColor="#2FA49B" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; font-style: oblique; text-indent: 2px; text-align: left;" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center" ColumnSpan="2">
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH12" runat="server" V_PointdeVue="1" V_Objet="42" V_Information="12" V_SiDonneeDico="true" EtiWidth="230px" DonWidth="50px" DonTabIndex="12" />
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Left">
                                    <asp:Label ID="LabelNbjCETRTT" runat="server" Text="jours" Height="20px" Width="35px" BackColor="Transparent" BorderStyle="none" BorderWidth="0px" ForeColor="#2FA49B" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; font-style: oblique; text-indent: 2px; text-align: left;" />
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Center" ColumnSpan="2">
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH02" runat="server" V_PointdeVue="1" V_Objet="42" V_Information="2" V_SiDonneeDico="true" EtiWidth="230px" DonWidth="50px" DonTabIndex="14" Etistyle="margin-left: 30px;" />
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Left">
                                    <asp:Label ID="LabelNbjCET" runat="server" Text="jours" Height="20px" Width="35px" BackColor="Transparent" BorderStyle="none" BorderWidth="0px" ForeColor="#2FA49B" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; font-style: oblique; text-indent: 2px; text-align: left;" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center" ColumnSpan="2">
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH13" runat="server" V_PointdeVue="1" V_Objet="42" V_Information="13" V_SiDonneeDico="true" EtiWidth="230px" DonWidth="50px" DonTabIndex="13" />
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Left" ColumnSpan="2">
                                    <asp:Label ID="LabelNbjCETRC" runat="server" Text="jours" Height="20px" Width="35px" BackColor="Transparent" BorderStyle="none" BorderWidth="0px" ForeColor="#2FA49B" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; font-style: oblique; text-indent: 2px; text-align: left;" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Height="15px" ColumnSpan="4" />
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>
</asp:Table>
