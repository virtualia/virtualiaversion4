﻿<%@ Control Language="VB" AutoEventWireup="false" Inherits="Virtualia.Net.Fenetre_PER_CET_HEBDO" CodeBehind="PER_CET_HEBDO.ascx.vb" %>

<%@ Register Src="~/Controles/Saisies/VCoupleEtiDonnee.ascx" TagName="VCoupleEtiDonnee" TagPrefix="Virtualia" %>
<%@ Register Src="~/Controles/Saisies/VCoupleVerticalEtiDonnee.ascx" TagName="VCoupleVerticalEtiDonnee" TagPrefix="Virtualia" %>
<%@ Register Src="../../TempsTravail/VSaisieHoraires.ascx" TagName="VSaisieHoraires" TagPrefix="Virtualia" %>

<%@ Register Src="~/ControlsGeneriques/V_DataGridListeFiltre.ascx" TagName="DataGridFiltreListe" TagPrefix="Generic" %>
<%@ Register Src="~/ControlsGeneriques/V_CommandeCRUD.ascx" TagName="VCrud" TagPrefix="Generic" %>


<asp:Table ID="CadreControle" runat="server" Height="30px" Width="650px" CellPadding="0" CellSpacing="0" HorizontalAlign="Center">
    <asp:TableRow>
        <asp:TableCell>
            <Generic:DataGridFiltreListe ID="ListeGrille" runat="server" CadreWidth="650px" SiColonneSelect="true" SiListeFiltreVisible="false" />
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="CadreInfo" runat="server" BorderStyle="Ridge" BorderWidth="2px" Visible="true" BorderColor="#B0E0D7" Height="260px" Width="730px" HorizontalAlign="Center" Style="margin-top: 3px;">
                <asp:TableRow>
                    <asp:TableCell>
                        <Generic:VCrud ID="CtlCrud" runat="server" Height="22px" Width="244px" HorizontalAlign="Right" CadreStyle="margin-top: 3px; margin-right: 3px" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Table ID="CadreTitre" runat="server" Height="40px" CellPadding="0" Width="730px" CellSpacing="0" HorizontalAlign="Center">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                    <asp:Label ID="Etiquette" runat="server" Text="Horaire hebdomadaire constaté" Height="20px" Width="350px" BackColor="#2FA49B" BorderColor="#B0E0D7" BorderStyle="Groove" BorderWidth="2px" ForeColor="#D7FAF3" Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Small" Style="margin-top: 5px; margin-left: 4px; margin-bottom: 10px; font-style: oblique; text-indent: 5px; text-align: center;" />
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Table ID="CadreDon" runat="server" CellPadding="0" CellSpacing="0" Width="700px" HorizontalAlign="Left">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH02" runat="server" V_PointdeVue="1" V_Objet="43" V_Information="2" V_SiDonneeDico="true" EtiWidth="172px" DonWidth="40px" DonTabIndex="1" />
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Left">
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH00" runat="server" V_PointdeVue="1" V_Objet="43" V_Information="0" V_SiDonneeDico="true" EtiWidth="50px" DonWidth="80px" DonTabIndex="2" EtiStyle="margin-left: 65px;" />
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Left">
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH01" runat="server" V_PointdeVue="1" V_Objet="43" V_Information="1" V_SiDonneeDico="true" EtiWidth="50px" DonWidth="80px" DonTabIndex="3" EtiStyle="margin-left: 40px;" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Height="15px" ColumnSpan="3" />
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <Virtualia:VCoupleVerticalEtiDonnee ID="InfoV03" runat="server" V_PointdeVue="1" V_Objet="43" V_Information="3" V_SiDonneeDico="true" EtiWidth="220px" DonWidth="80px" DonHeight="16px" DonTabIndex="4" EtiStyle="text-align: center; margin-left: 5px;" DonStyle="text-align: center; margin-left: 75px;" />
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Left">
                                    <Virtualia:VCoupleVerticalEtiDonnee ID="InfoV28" runat="server" V_PointdeVue="1" V_Objet="43" V_Information="28" V_SiDonneeDico="true" EtiWidth="220px" DonWidth="80px" DonHeight="16px" DonTabIndex="5" EtiStyle="text-align: center; margin-left: 20px;" DonStyle="text-align: center; margin-left: 100px;" />
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Left">
                                    <Virtualia:VCoupleVerticalEtiDonnee ID="InfoV04" runat="server" V_PointdeVue="1" V_Objet="43" V_Information="4" V_SiDonneeDico="true" EtiWidth="230px" DonWidth="80px" DonHeight="16px" DonTabIndex="6" EtiStyle="text-align: center" DonStyle="text-align: center; margin-left: 80px;" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Height="10px" ColumnSpan="3" />
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <asp:Label ID="EtiMiseAJour" runat="server" Height="20px" Width="220px" Text="Mise à jour effectuée par" BackColor="#B0E0D7" BorderColor="#B0E0D7" BorderStyle="Outset" BorderWidth="2px" ForeColor="#142425" Font-Italic="true" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Style="text-indent: 1px; text-align: center; margin-left: 5px;" />
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Left">
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH29" runat="server" V_PointdeVue="1" V_Objet="43" V_Information="29" V_SiDonneeDico="true" EtiWidth="89px" DonWidth="150px" DonTabIndex="7" V_SiEnLectureSeule="true" />
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Left">
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH30" runat="server" V_PointdeVue="1" V_Objet="43" V_Information="30" V_SiDonneeDico="true" EtiWidth="30px" DonWidth="80px" Etitext="le" DonTabIndex="8" V_SiEnLectureSeule="true" EtiStyle="margin-left: 43px;" />
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <Virtualia:VSaisieHoraires ID="PER_Horaires" runat="server" />
        </asp:TableCell>
    </asp:TableRow>
</asp:Table>

