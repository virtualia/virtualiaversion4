﻿Option Strict On
Option Explicit On
Option Compare Text

Imports VI = Virtualia.Systeme.Constantes
Imports Virtualia.ObjetBase
Imports Virtualia.TablesObjet.ShemaPER
Imports Virtualia.OutilsVisu

Partial Class Fenetre_PER_VARIABLE_PAIE
    Inherits ObjetWebControleBase(Of PER_VARIABLE_PAIE)
    Implements IControlCrud

#Region "IControlCrud"
    Public ReadOnly Property ControlCrud As ICtlCommandeCrud Implements IControlCrud.CtlCrud
        Get
            Return Me.CtlCrud
        End Get
    End Property
#End Region

#Region "ObjetWebControleBase"
    Private _webfonction As WebFonctions
    Protected Overrides ReadOnly Property V_WebFonction As IWebFonctions
        Get
            If (_webfonction Is Nothing) Then
                _webfonction = New WebFonctions(Me)
            End If
            Return _webfonction
        End Get
    End Property

    Protected Overrides Function InitCadreInfo() As WebControl
        Return Me.CadreInfo
    End Function

    Protected Overrides Function InitControlDataGrid() As IControlDataGrid
        Return Me.ListeGrille
    End Function

    Protected Overrides Sub InitCacheIde()

        V_CacheIde.TypeVue = WebControlTypeVue.ListeGrid

        Dim captions As List(Of String) = New List(Of String)()
        captions.Add("Aucun évènement")
        captions.Add("Un évènement")
        captions.Add("évènements")

        Me.ListeGrille.InitCaptions(captions)

        Dim colonnes As ColonneDataGridCollection = New ColonneDataGridCollection()

        colonnes.Add(HorizontalAlign.Center, TypeData.Date, "date de paie", 8)
        colonnes.Add(HorizontalAlign.Center, TypeData.Numeric, "nature", 4)
        colonnes.Add(HorizontalAlign.Center, TypeData.Numeric, "nombre", 6)
        colonnes.Add(HorizontalAlign.Center, TypeData.Numeric, "taux", 7)
        colonnes.Add(HorizontalAlign.Center, TypeData.Numeric, "montant", 5)
        colonnes.Add(HorizontalAlign.Center, TypeData.Chaine, "Clef", 0, True)

        Me.ListeGrille.InitColonnes(colonnes)
    End Sub

    Protected Overrides Sub OnPreRender(e As EventArgs)
        MyBase.OnPreRender(e)

        CadreInfo.BackColor = _webfonction.CouleurCharte(V_CacheIde.PointDeVue, "Cadre")
        CadreInfo.BackColor = _webfonction.CouleurCharte(V_CacheIde.PointDeVue, "Cadre")
        CadreInfo.BorderColor = _webfonction.CouleurCharte(V_CacheIde.PointDeVue, "Bordure")
        Etiquette.BackColor = _webfonction.CouleurCharte(V_CacheIde.PointDeVue, "Titre")
        Etiquette.ForeColor = _webfonction.CouleurCharte(V_CacheIde.PointDeVue, "Police_Claire")
        Etiquette.BorderColor = _webfonction.CouleurCharte(V_CacheIde.PointDeVue, "Bordure")
    End Sub

    Protected Overrides Sub OnInit(e As System.EventArgs)
        MyBase.OnInit(e)

        Dim initwebfonction As IWebFonctions = V_WebFonction
    End Sub
#End Region

#Region "Specifique"
    Private Sub CalculSpecifique_VariablePaie()
        Dim Zcalcul As Double

        If V_CacheMaj(6).valeur <> "" And V_CacheMaj(7).valeur <> "" Then
            Zcalcul = _webfonction.ViRhFonction.ConversionDouble(V_CacheMaj(6).valeur) * _webfonction.ViRhFonction.ConversionDouble(V_CacheMaj(7).valeur)
            V_CacheMaj(5).valeur = "" & Math.Round(Zcalcul, 2)
        End If
    End Sub

    Protected Overrides Sub CommandeOK_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        If InfoH01.DonText = "" Or Not IsNumeric(InfoH01.DonText) Then
            If _controledatagrid.TotalLignes <= 0 Then
                GereValeurChange(1, "1")
            Else
                GereValeurChange(1, "" & _controledatagrid.TotalLignes + 1)
            End If
        End If
        Call CalculSpecifique_VariablePaie()

        MyBase.CommandeOK_Click(sender, e)
    End Sub
#End Region

End Class
