﻿'------------------------------------------------------------------------------
' <généré automatiquement>
'     Ce code a été généré par un outil.
'
'     Les modifications apportées à ce fichier peuvent provoquer un comportement incorrect et seront perdues si
'     le code est régénéré.
' </généré automatiquement>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class Fenetre_PER_PLANNING_HEBDO

    '''<summary>
    '''Contrôle CadreInfo.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CadreInfo As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle CadreTitre.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CadreTitre As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle Etiquette.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Etiquette As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle TableauPlanningHebdomadaire.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents TableauPlanningHebdomadaire As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle LigneTitre.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LigneTitre As Global.System.Web.UI.WebControls.TableRow

    '''<summary>
    '''Contrôle LabelTitreTableau.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelTitreTableau As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle LigneEnteteJour.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LigneEnteteJour As Global.System.Web.UI.WebControls.TableRow

    '''<summary>
    '''Contrôle CellEnteteJour.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellEnteteJour As Global.System.Web.UI.WebControls.TableCell

    '''<summary>
    '''Contrôle LabelEnteteJour.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelEnteteJour As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle CellLundi.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellLundi As Global.System.Web.UI.WebControls.TableCell

    '''<summary>
    '''Contrôle LabelLundi.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelLundi As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle CellMardi.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellMardi As Global.System.Web.UI.WebControls.TableCell

    '''<summary>
    '''Contrôle LabelMardi.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelMardi As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle CellMercredi.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellMercredi As Global.System.Web.UI.WebControls.TableCell

    '''<summary>
    '''Contrôle LabelMercredi.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelMercredi As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle CellJeudi.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellJeudi As Global.System.Web.UI.WebControls.TableCell

    '''<summary>
    '''Contrôle LabelJeudi.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelJeudi As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle CellVendredi.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellVendredi As Global.System.Web.UI.WebControls.TableCell

    '''<summary>
    '''Contrôle LabelVendredi.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelVendredi As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle CellSamedi.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellSamedi As Global.System.Web.UI.WebControls.TableCell

    '''<summary>
    '''Contrôle LabelSamedi.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelSamedi As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle CellDimanche.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellDimanche As Global.System.Web.UI.WebControls.TableCell

    '''<summary>
    '''Contrôle LabelDimanche.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelDimanche As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle CellSemaine.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellSemaine As Global.System.Web.UI.WebControls.TableCell

    '''<summary>
    '''Contrôle LabelSemaine.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelSemaine As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle LigneEnteteDate.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LigneEnteteDate As Global.System.Web.UI.WebControls.TableRow

    '''<summary>
    '''Contrôle CellDate.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellDate As Global.System.Web.UI.WebControls.TableCell

    '''<summary>
    '''Contrôle LabelDate.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelDate As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle CellDateLundi.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellDateLundi As Global.System.Web.UI.WebControls.TableCell

    '''<summary>
    '''Contrôle LabelDateLundi.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelDateLundi As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle CellDateMardi.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellDateMardi As Global.System.Web.UI.WebControls.TableCell

    '''<summary>
    '''Contrôle LabelDateMardi.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelDateMardi As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle CellDateMercredi.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellDateMercredi As Global.System.Web.UI.WebControls.TableCell

    '''<summary>
    '''Contrôle LabelDateMercredi.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelDateMercredi As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle CellDateJeudi.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellDateJeudi As Global.System.Web.UI.WebControls.TableCell

    '''<summary>
    '''Contrôle LabelDateJeudi.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelDateJeudi As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle CellDateVendredi.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellDateVendredi As Global.System.Web.UI.WebControls.TableCell

    '''<summary>
    '''Contrôle LabelDateVendredi.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelDateVendredi As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle CellDateSamedi.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellDateSamedi As Global.System.Web.UI.WebControls.TableCell

    '''<summary>
    '''Contrôle LabelDateSamedi.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelDateSamedi As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle CellDateDimanche.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellDateDimanche As Global.System.Web.UI.WebControls.TableCell

    '''<summary>
    '''Contrôle LabelDateDimanche.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelDateDimanche As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle CellDateSemaine.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellDateSemaine As Global.System.Web.UI.WebControls.TableCell

    '''<summary>
    '''Contrôle LabelDateSemaine.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelDateSemaine As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle LigneSemaine1.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LigneSemaine1 As Global.System.Web.UI.WebControls.TableRow

    '''<summary>
    '''Contrôle CellPersonne1.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellPersonne1 As Global.System.Web.UI.WebControls.TableCell

    '''<summary>
    '''Contrôle LabelPersonne1.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelPersonne1 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle CellHoraireLundi1.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellHoraireLundi1 As Global.System.Web.UI.WebControls.TableCell

    '''<summary>
    '''Contrôle LigneHoraireLundi1.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LigneHoraireLundi1 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle CellHoraireMardi1.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellHoraireMardi1 As Global.System.Web.UI.WebControls.TableCell

    '''<summary>
    '''Contrôle LigneHoraireMardi1.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LigneHoraireMardi1 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle CellHoraireMercredi1.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellHoraireMercredi1 As Global.System.Web.UI.WebControls.TableCell

    '''<summary>
    '''Contrôle LigneHoraireMercredi1.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LigneHoraireMercredi1 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle CellHoraireJeudi1.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellHoraireJeudi1 As Global.System.Web.UI.WebControls.TableCell

    '''<summary>
    '''Contrôle LigneHoraireJeudi1.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LigneHoraireJeudi1 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle CellHoraireVendredi1.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellHoraireVendredi1 As Global.System.Web.UI.WebControls.TableCell

    '''<summary>
    '''Contrôle LigneHoraireVendredi1.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LigneHoraireVendredi1 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle CellHoraireSamedi1.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellHoraireSamedi1 As Global.System.Web.UI.WebControls.TableCell

    '''<summary>
    '''Contrôle LigneHoraireSamedi1.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LigneHoraireSamedi1 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle CellHoraireDimanche1.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellHoraireDimanche1 As Global.System.Web.UI.WebControls.TableCell

    '''<summary>
    '''Contrôle LigneHoraireDimanche1.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LigneHoraireDimanche1 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle CellHoraireSemaine1.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellHoraireSemaine1 As Global.System.Web.UI.WebControls.TableCell

    '''<summary>
    '''Contrôle LigneHoraireSemaine1.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LigneHoraireSemaine1 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle LigneSemaine2.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LigneSemaine2 As Global.System.Web.UI.WebControls.TableRow

    '''<summary>
    '''Contrôle CellPersonne2.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellPersonne2 As Global.System.Web.UI.WebControls.TableCell

    '''<summary>
    '''Contrôle TablePersonne2.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents TablePersonne2 As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle LabelPersonne2.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelPersonne2 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle CellHoraireLundi2.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellHoraireLundi2 As Global.System.Web.UI.WebControls.TableCell

    '''<summary>
    '''Contrôle TableHoraireLundi2.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents TableHoraireLundi2 As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle LabelHoraireLundi2P1.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelHoraireLundi2P1 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle LabelHoraireLundi2P2.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelHoraireLundi2P2 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle CellHoraireMardi2.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellHoraireMardi2 As Global.System.Web.UI.WebControls.TableCell

    '''<summary>
    '''Contrôle TableHoraireMardi2.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents TableHoraireMardi2 As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle LabelHoraireMardi2P1.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelHoraireMardi2P1 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle LabelHoraireMardi2P2.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelHoraireMardi2P2 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle CellHoraireMercredi2.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellHoraireMercredi2 As Global.System.Web.UI.WebControls.TableCell

    '''<summary>
    '''Contrôle TableHoraireMercredi2.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents TableHoraireMercredi2 As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle LabelHoraireMercredi2P1.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelHoraireMercredi2P1 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle LabelHoraireMercredi2P2.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelHoraireMercredi2P2 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle CellHoraireJeudi2.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellHoraireJeudi2 As Global.System.Web.UI.WebControls.TableCell

    '''<summary>
    '''Contrôle TableHoraireJeudi2.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents TableHoraireJeudi2 As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle LabelHoraireJeudi2P1.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelHoraireJeudi2P1 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle LabelHoraireJeudi2P2.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelHoraireJeudi2P2 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle CellHoraireVendredi2.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellHoraireVendredi2 As Global.System.Web.UI.WebControls.TableCell

    '''<summary>
    '''Contrôle TableHoraireVendredi2.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents TableHoraireVendredi2 As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle LabelHoraireVendredi2P1.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelHoraireVendredi2P1 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle LabelHoraireVendredi2P2.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelHoraireVendredi2P2 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle CellHoraireSamedi2.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellHoraireSamedi2 As Global.System.Web.UI.WebControls.TableCell

    '''<summary>
    '''Contrôle TableHoraireSamedi2.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents TableHoraireSamedi2 As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle LabelHoraireSamedi2P1.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelHoraireSamedi2P1 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle LabelHoraireSamedi2P2.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelHoraireSamedi2P2 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle CellHoraireDimanche2.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellHoraireDimanche2 As Global.System.Web.UI.WebControls.TableCell

    '''<summary>
    '''Contrôle TableHoraireDimanche2.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents TableHoraireDimanche2 As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle LabelHoraireDimanche2P1.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelHoraireDimanche2P1 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle LabelHoraireDimanche2P2.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelHoraireDimanche2P2 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle CellHoraireSemaine2.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellHoraireSemaine2 As Global.System.Web.UI.WebControls.TableCell

    '''<summary>
    '''Contrôle TableHoraireSemaine2.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents TableHoraireSemaine2 As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle LabelHoraireSemaine2.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelHoraireSemaine2 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle TableTotal.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents TableTotal As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle LigneTotal.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LigneTotal As Global.System.Web.UI.WebControls.TableRow

    '''<summary>
    '''Contrôle TitreTotal.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents TitreTotal As Global.System.Web.UI.WebControls.TableCell

    '''<summary>
    '''Contrôle LabelTotal.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelTotal As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle TotalLundi.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents TotalLundi As Global.System.Web.UI.WebControls.TableCell

    '''<summary>
    '''Contrôle LabelTotalLundi.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelTotalLundi As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle TotalMardi.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents TotalMardi As Global.System.Web.UI.WebControls.TableCell

    '''<summary>
    '''Contrôle LabelTotalMardi.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelTotalMardi As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle TotalMercredi.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents TotalMercredi As Global.System.Web.UI.WebControls.TableCell

    '''<summary>
    '''Contrôle LabelTotalMercredi.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelTotalMercredi As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle TotalJeudi.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents TotalJeudi As Global.System.Web.UI.WebControls.TableCell

    '''<summary>
    '''Contrôle LabelTotalJeudi.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelTotalJeudi As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle TotalVendredi.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents TotalVendredi As Global.System.Web.UI.WebControls.TableCell

    '''<summary>
    '''Contrôle LabelTotalVendredi.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelTotalVendredi As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle TotalSamedi.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents TotalSamedi As Global.System.Web.UI.WebControls.TableCell

    '''<summary>
    '''Contrôle LabelTotalSamedi.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelTotalSamedi As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle TotalDimanche.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents TotalDimanche As Global.System.Web.UI.WebControls.TableCell

    '''<summary>
    '''Contrôle LabelTotalDimanche.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelTotalDimanche As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle TotalGeneral.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents TotalGeneral As Global.System.Web.UI.WebControls.TableCell

    '''<summary>
    '''Contrôle LabelTotalGeneral.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelTotalGeneral As Global.System.Web.UI.WebControls.Label
End Class
