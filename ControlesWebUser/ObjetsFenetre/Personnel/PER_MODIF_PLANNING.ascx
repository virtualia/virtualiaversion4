﻿<%@ Control Language="VB" AutoEventWireup="false" Inherits="Virtualia.Net.Fenetre_PER_MODIF_PLANNING" CodeBehind="PER_MODIF_PLANNING.ascx.vb" %>

<%@ Register Src="~/Controles/Saisies/VCoupleEtiDonnee.ascx" TagName="VCoupleEtiDonnee" TagPrefix="Virtualia" %>
<%@ Register Src="~/Controles/Saisies/VDuoEtiquetteCommande.ascx" TagName="VDuoEtiquetteCommande" TagPrefix="Virtualia" %>
<%@ Register Src="~/Controles/Saisies/VCocheSimple.ascx" TagName="VCocheSimple" TagPrefix="Virtualia" %>
<%@ Register Src="~/Controles/Saisies/VTrioHorizontalRadio.ascx" TagName="VTrioHorizontalRadio" TagPrefix="Virtualia" %>
<%@ Register Src="~/Controles/Saisies/VCoupleVerticalEtiDonnee.ascx" TagName="VCoupleVerticalEtiDonnee" TagPrefix="Virtualia" %>

<%@ Register Src="~/ControlsGeneriques/V_DataGrid.ascx" TagName="VDataGrid" TagPrefix="Generic" %>
<%@ Register Src="~/ControlsGeneriques/V_CommandeCRUD.ascx" TagName="VCrud" TagPrefix="Generic" %>

<asp:Table ID="CadreControle" runat="server" Height="30px" Width="650px" CellPadding="0" CellSpacing="0" HorizontalAlign="Center">
    <asp:TableRow>
        <asp:TableCell>
            <Generic:VDataGrid ID="ListeGrille" runat="server" CadreWidth="650px" SiColonneSelect="true" SiCaseAcocher="false" />
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="CadreInfo" runat="server" BorderStyle="Ridge" BorderWidth="2px" Visible="true" BorderColor="#B0E0D7" Height="600px" Width="730px" HorizontalAlign="Center" Style="margin-top: 3px;">
                <asp:TableRow>
                    <asp:TableCell>
                        <Generic:VCrud ID="CtlCrud" runat="server" Height="22px" Width="244px" HorizontalAlign="Right" CadreStyle="margin-top: 3px; margin-right: 3px" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Table ID="CadreTitre" runat="server" Height="40px" CellPadding="0" Width="730px" CellSpacing="0" HorizontalAlign="Center">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                    <asp:Label ID="Etiquette" runat="server" Text="Ajustement du planning prévisionnel" Height="20px" Width="350px" BackColor="#2FA49B" BorderColor="#B0E0D7" BorderStyle="Groove" BorderWidth="2px" ForeColor="#D7FAF3" Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Small" Style="margin-top: 5px; margin-left: 4px; margin-bottom: 12px; font-style: oblique; text-indent: 5px; text-align: center;" />
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Table ID="CadreDon" runat="server" CellPadding="0" CellSpacing="0" Width="430px" HorizontalAlign="Center">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH00" runat="server" V_PointdeVue="1" V_Objet="91" V_Information="0" V_SiDonneeDico="true" EtiWidth="150px" DonWidth="80px" DonTabIndex="1" />
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Center">
                                    <asp:Label ID="EtiDateEffet" runat="server" Height="20px" Width="250px" Text="" BackColor="#B0E0D7" BorderColor="#B0E0D7" BorderStyle="Outset" BorderWidth="2px" ForeColor="#142425" Font-Italic="true" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Style="text-indent: 1px; text-align: center; margin-left: 10px;" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH02" runat="server" V_PointdeVue="1" V_Objet="91" V_Information="2" V_SiDonneeDico="true" EtiWidth="150px" DonWidth="80px" DonTabIndex="2" />
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Center">
                                    <asp:Label ID="EtiDateFin" runat="server" Height="20px" Width="250px" Text="" BackColor="#B0E0D7" BorderColor="#B0E0D7" BorderStyle="Outset" BorderWidth="2px" ForeColor="#142425" Font-Italic="true" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Style="text-indent: 1px; text-align: center; margin-left: 10px;" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Height="10px" ColumnSpan="2" />
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH03" runat="server" V_PointdeVue="1" V_Objet="91" V_Information="3" V_SiDonneeDico="true" EtiWidth="200px" DonWidth="80px" DonTabIndex="3" />
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Center">
                                    <Virtualia:VCocheSimple ID="Coche04" runat="server" V_TabIndex="4" V_PointdeVue="1" V_Objet="91" V_Information="4" V_SiDonneeDico="true" V_Width="250px" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Height="2px" ColumnSpan="2" />
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center" ColumnSpan="2">
                                    <Virtualia:VTrioHorizontalRadio ID="RadioH05" runat="server" V_Groupe="TypeHoraire" V_PointdeVue="1" V_Objet="91" V_Information="5" V_SiDonneeDico="true" RadioGaucheWidth="140px" RadioCentreWidth="140px" RadioDroiteWidth="140px" RadioGaucheHeight="23px" RadioCentreHeight="23px" RadioDroiteHeight="23px" RadioGaucheText="Horaire fixe" RadioCentreText="Horaire variable" RadioDroiteText="Mode mixte" RadioGaucheStyle="margin-left: 4px;" Visible="true" V_TabIndex="5" />
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Table ID="TableauCycleTravail" runat="server" Height="170px" CellPadding="0" Width="690px" CellSpacing="0" HorizontalAlign="Center">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                    <asp:Label ID="LabelCycleTravail" runat="server" Text="Cycle de travail" Height="20px" Width="690px" BackColor="#216B68" BorderColor="#B0E0D7" BorderStyle="Groove" BorderWidth="2px" ForeColor="#D7FAF3" Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Small" Style="margin-top: 5px; margin-left: 0px; margin-bottom: 1px; font-style: oblique; text-indent: 5px; text-align: center;" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell ID="CadreCycleTravail" HorizontalAlign="Center" Visible="true" Width="690px" Height="200px" BackColor="#E9FDF9" BorderColor="#CAEBE4" BorderStyle="Inset" BorderWidth="1px">
                                    <asp:Table ID="TableauCycle" runat="server" CellPadding="0" Width="690px" CellSpacing="0" HorizontalAlign="Center">
                                        <asp:TableRow>
                                            <asp:TableCell Height="130px" HorizontalAlign="Center" ColumnSpan="2" />
                                        </asp:TableRow>
                                        <asp:TableRow>
                                            <asp:TableCell HorizontalAlign="Center" ColumnSpan="2">
                                                <asp:Label ID="LabelDureeCycle" runat="server" Text=" " Height="20px" Width="690px" BackColor="#CAEBE4" BorderColor="#B0E0D7" BorderStyle="Notset" BorderWidth="2px" ForeColor="#142425" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; font-style: oblique; text-indent: 2px; text-align: center;" />
                                            </asp:TableCell>
                                        </asp:TableRow>
                                        <asp:TableRow>
                                            <asp:TableCell HorizontalAlign="Center">
                                                <asp:Label ID="LabelPlage1" runat="server" Text=" " Height="20px" Width="343px" BackColor="#CAEBE4" BorderColor="#B0E0D7" BorderStyle="Notset" BorderWidth="2px" ForeColor="#142425" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Style="margin-top: 0px; margin-left: 0px; margin-bottom: 10px; font-style: oblique; text-indent: 2px; text-align: center;" />
                                            </asp:TableCell>
                                            <asp:TableCell HorizontalAlign="Center">
                                                <asp:Label ID="LabelPlage2" runat="server" Text=" " Height="20px" Width="343px" BackColor="#CAEBE4" BorderColor="#B0E0D7" BorderStyle="Notset" BorderWidth="2px" ForeColor="#142425" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Style="margin-top: 0px; margin-left: 0px; margin-bottom: 10px; font-style: oblique; text-indent: 2px; text-align: center;" />
                                            </asp:TableCell>
                                        </asp:TableRow>
                                        <asp:TableRow>
                                            <asp:TableCell HorizontalAlign="Center" ColumnSpan="2">
                                                <Virtualia:VDuoEtiquetteCommande ID="Dontab01" runat="server" V_PointdeVue="1" V_Objet="91" V_Information="1" V_SiDonneeDico="true" EtiWidth="150px" DonWidth="450px" DonTabIndex="6" />
                                            </asp:TableCell>
                                        </asp:TableRow>
                                        <asp:TableRow>
                                            <asp:TableCell Height="10px" ColumnSpan="2" />
                                        </asp:TableRow>
                                    </asp:Table>
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Table ID="TableauObservations" runat="server" CellPadding="0" CellSpacing="0" Width="730px">
                            <asp:TableRow>
                                <asp:TableCell Height="10px" />
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                    <Virtualia:VCoupleVerticalEtiDonnee ID="InfoV06" runat="server" DonTextMode="true" V_PointdeVue="1" V_Objet="91" V_Information="6" V_SiDonneeDico="true" EtiWidth="650px" DonWidth="648px" DonHeight="100px" DonTabIndex="7" EtiStyle="text-align:center" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Height="15px" />
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>
</asp:Table>
