﻿Option Strict On
Option Explicit On
Option Compare Text

Imports VI = Virtualia.Systeme.Constantes
Imports Virtualia.ObjetBase
Imports Virtualia.TablesObjet.ShemaPER
Imports Virtualia.OutilsVisu
Imports Virtualia.Systeme.Evenements

Partial Class Fenetre_PER_HEURESUP
    Inherits ObjetWebControleBase(Of PER_HEURESUP)
    Implements IControlCrud

#Region "IControlCrud"
    Public ReadOnly Property ControlCrud As ICtlCommandeCrud Implements IControlCrud.CtlCrud
        Get
            Return Me.CtlCrud
        End Get
    End Property
#End Region

#Region "ObjetWebControleBase"
    Private _webfonction As WebFonctions
    Protected Overrides ReadOnly Property V_WebFonction As IWebFonctions
        Get
            If (_webfonction Is Nothing) Then
                _webfonction = New WebFonctions(Me)
            End If
            Return _webfonction
        End Get
    End Property

    Protected Overrides Function InitCadreInfo() As WebControl
        Return Me.CadreInfo
    End Function

    Protected Overrides Function InitControlDataGrid() As IControlDataGrid
        Return Me.ListeGrille
    End Function

    Protected Overrides Sub InitCacheIde()

        V_CacheIde.TypeVue = WebControlTypeVue.ListeGrid

        Dim captions As List(Of String) = New List(Of String)()
        captions.Add("Aucune heure supplémentaire")
        captions.Add("Une heure supplémentaire")
        captions.Add("heures supplémentaires")

        Me.ListeGrille.InitCaptions(captions)

        Dim colonnes As ColonneDataGridCollection = New ColonneDataGridCollection()

        colonnes.Add(HorizontalAlign.Center, TypeData.Date, "date d'effet", 0)
        colonnes.Add(HorizontalAlign.Center, TypeData.Numeric, "montant total", 13)
        colonnes.Add(HorizontalAlign.Center, TypeData.Numeric, "nombre T1", 1)
        colonnes.Add(HorizontalAlign.Center, TypeData.Numeric, "nombre T2", 4)
        colonnes.Add(HorizontalAlign.Center, TypeData.Chaine, "Clef", 0, True)

        Me.ListeGrille.InitColonnes(colonnes)
    End Sub

    Protected Overrides Sub OnPreRender(e As EventArgs)
        MyBase.OnPreRender(e)

        CadreInfo.BackColor = _webfonction.CouleurCharte(V_CacheIde.PointDeVue, "Cadre")
        CadreInfo.BorderColor = _webfonction.CouleurCharte(V_CacheIde.PointDeVue, "Bordure")
        Etiquette.BackColor = _webfonction.CouleurCharte(V_CacheIde.PointDeVue, "Titre")
        Etiquette.ForeColor = _webfonction.CouleurCharte(V_CacheIde.PointDeVue, "Police_Claire")
        Etiquette.BorderColor = _webfonction.CouleurCharte(V_CacheIde.PointDeVue, "Bordure")
        LabelTranche1.BackColor = _webfonction.CouleurCharte(V_CacheIde.PointDeVue, "Etiquette_BackColor")
        LabelTranche1.ForeColor = _webfonction.CouleurCharte(V_CacheIde.PointDeVue, "Etiquette_ForeColor")
        LabelTranche1.BorderColor = _webfonction.CouleurCharte(V_CacheIde.PointDeVue, "Bordure")
        LabelTranche2.BackColor = _webfonction.CouleurCharte(V_CacheIde.PointDeVue, "Etiquette_BackColor")
        LabelTranche2.ForeColor = _webfonction.CouleurCharte(V_CacheIde.PointDeVue, "Etiquette_ForeColor")
        LabelTranche2.BorderColor = _webfonction.CouleurCharte(V_CacheIde.PointDeVue, "Bordure")
        LabelTranche3.BackColor = _webfonction.CouleurCharte(V_CacheIde.PointDeVue, "Etiquette_BackColor")
        LabelTranche3.ForeColor = _webfonction.CouleurCharte(V_CacheIde.PointDeVue, "Etiquette_ForeColor")
        LabelTranche3.BorderColor = _webfonction.CouleurCharte(V_CacheIde.PointDeVue, "Bordure")
        LabelTranche4.BackColor = _webfonction.CouleurCharte(V_CacheIde.PointDeVue, "Etiquette_BackColor")
        LabelTranche4.ForeColor = _webfonction.CouleurCharte(V_CacheIde.PointDeVue, "Etiquette_ForeColor")
        LabelTranche4.BorderColor = _webfonction.CouleurCharte(V_CacheIde.PointDeVue, "Bordure")
        EtiHSUP.BackColor = _webfonction.CouleurCharte(V_CacheIde.PointDeVue, "Etiquette_BackColor")
        EtiHSUP.ForeColor = _webfonction.CouleurCharte(V_CacheIde.PointDeVue, "Etiquette_ForeColor")
        EtiHSUP.BorderColor = _webfonction.CouleurCharte(V_CacheIde.PointDeVue, "Bordure")
        EtiHSUPMontant.BackColor = _webfonction.CouleurCharte(V_CacheIde.PointDeVue, "Etiquette_BackColor")
        EtiHSUPMontant.ForeColor = _webfonction.CouleurCharte(V_CacheIde.PointDeVue, "Etiquette_ForeColor")
        EtiHSUPMontant.BorderColor = _webfonction.CouleurCharte(V_CacheIde.PointDeVue, "Bordure")
        EtiHSUPNombre.BackColor = _webfonction.CouleurCharte(V_CacheIde.PointDeVue, "Etiquette_BackColor")
        EtiHSUPNombre.ForeColor = _webfonction.CouleurCharte(V_CacheIde.PointDeVue, "Etiquette_ForeColor")
        EtiHSUPNombre.BorderColor = _webfonction.CouleurCharte(V_CacheIde.PointDeVue, "Bordure")
        EtiHSUPTaux.BackColor = _webfonction.CouleurCharte(V_CacheIde.PointDeVue, "Etiquette_BackColor")
        EtiHSUPTaux.ForeColor = _webfonction.CouleurCharte(V_CacheIde.PointDeVue, "Etiquette_ForeColor")
        EtiHSUPTaux.BorderColor = _webfonction.CouleurCharte(V_CacheIde.PointDeVue, "Bordure")
    End Sub

    Protected Overrides Sub OnInit(e As System.EventArgs)
        MyBase.OnInit(e)

        Dim initwebfonction As IWebFonctions = V_WebFonction
    End Sub
#End Region

#Region "Specifique"
    Protected Overrides Sub CommandeOK_Click(sender As Object, e As EventArgs)
        CalculSpecifique_HeureSupp()
        MyBase.CommandeOK_Click(sender, e)
    End Sub

    Protected Overrides Sub VCoupleEtiDonnee_ValeurChange(ByVal sender As Object, ByVal e As DonneeChangeEventArgs)
        MyBase.VCoupleEtiDonnee_ValeurChange(sender, e)
        CalculSpecifique_HeureSupp()
    End Sub

    Protected Overrides Sub InitDesignControles()
        MyBase.InitDesignControles()

        Dim lstctl As List(Of ObjetWebControlSaisie) = (From ctl In _dicoWebControl(TypeWebControl.VCoupleEtiDonnee) _
                                                        Where ctl.V_Information = 1 Or _
                                                            ctl.V_Information = 4 Or _
                                                            ctl.V_Information = 7 Or _
                                                            ctl.V_Information = 10 _
                                                        Select ctl).ToList()

        lstctl.ForEach(Sub(ct)
                           DirectCast(ct, Controles_VCoupleEtiDonneeBase).V_SiAutoPostBack = True
                       End Sub)
        
    End Sub

    Protected Overrides Sub ChargeFenetre(value As Integer)
        MyBase.ChargeFenetre(value)

        If (V_CacheMaj.Count <= 0) Then
            Return
        End If

        'TODO : LD A voir, il semblerait que l'on renseigne des valeurs cachees
        InitSpecifique_HeureSupp()
    End Sub
#End Region

#Region "Methodes privees"
    Private Sub CalculSpecifique_HeureSupp()

        Dim Zcalcul As Double
        Dim Ztotal As Double = 0

        If V_CacheMaj(1).valeur <> "" And V_CacheMaj(2).valeur <> "" Then
            Zcalcul = _webfonction.ViRhFonction.ConversionDouble(V_CacheMaj(1).valeur) * _webfonction.ViRhFonction.ConversionDouble(V_CacheMaj(2).valeur)
            V_CacheMaj(3).valeur = "" & Math.Round(Zcalcul, 2)
            Ztotal += Math.Round(Zcalcul, 2)
        End If
        If V_CacheMaj(4).valeur <> "" And V_CacheMaj(5).valeur <> "" Then
            Zcalcul = _webfonction.ViRhFonction.ConversionDouble(V_CacheMaj(4).valeur) * _webfonction.ViRhFonction.ConversionDouble(V_CacheMaj(5).valeur)
            V_CacheMaj(6).valeur = "" & Math.Round(Zcalcul, 2)
            Ztotal += Math.Round(Zcalcul, 2)
        End If
        If V_CacheMaj(7).valeur <> "" And V_CacheMaj(8).valeur <> "" Then
            Zcalcul = _webfonction.ViRhFonction.ConversionDouble(V_CacheMaj(7).valeur) * _webfonction.ViRhFonction.ConversionDouble(V_CacheMaj(8).valeur)
            V_CacheMaj(9).valeur = "" & Math.Round(Zcalcul, 2)
            Ztotal += Math.Round(Zcalcul, 2)
        End If
        If V_CacheMaj(10).valeur <> "" And V_CacheMaj(11).valeur <> "" Then
            Zcalcul = _webfonction.ViRhFonction.ConversionDouble(V_CacheMaj(10).valeur) * _webfonction.ViRhFonction.ConversionDouble(V_CacheMaj(11).valeur)
            V_CacheMaj(12).valeur = "" & Math.Round(Zcalcul, 2)
            Ztotal += Math.Round(Zcalcul, 2)
        End If
        V_CacheMaj(13).valeur = "" & Math.Round(Ztotal, 2)
    End Sub

    Private Sub InitSpecifique_HeureSupp()

        If (V_CacheMaj.Count <= 0) Then
            Return
        End If

        Dim Dossier As Virtualia.Metier.ETP.ObjetDossierPER = Nothing

        Try
            Dossier = _webfonction.PointeurUtilisateur.ObjetEnsemble.ItemDossier(V_CacheIde.Ide_Dossier)
        Catch ex As Exception
            Exit Sub
        End Try

        If Dossier Is Nothing Then
            Exit Sub
        End If


        Dim IMajore As Integer = 0
        Dim FicheVue As Virtualia.Metier.ETP.ObjetVUE_ETP = Dossier.FicheVUE_Valable(False, _webfonction.ViRhDates.DateduJour)
        If FicheVue IsNot Nothing Then
            If IsNumeric(FicheVue.Indice_Remuneration) Then
                IMajore = CInt(FicheVue.Indice_Remuneration)
            End If
        End If
        If IMajore = 0 Then
            Exit Sub
        End If
        Dim DateDebut As String = "01" & Strings.Right(_webfonction.ViRhDates.DateduJour, 8)
        Dim DateFin As String = _webfonction.ViRhDates.DateSaisieVerifiee("31" & Strings.Right(DateDebut, 8))
        Dim ValPoint As Double = _webfonction.PointeurGlobal.ObjetReferentiel.ValeurAnnuelleI100(_webfonction.ViRhDates.DateduJour)
        Dim TauxResid As Double = _webfonction.PointeurGlobal.ObjetReferentiel.TauxZoneResidence
        Dim IPlancher As Integer = _webfonction.PointeurGlobal.ObjetReferentiel.IndicePlancherResidence

        Dim FicheTmp As PER_HEURESUP = New PER_HEURESUP()

        FicheTmp.Indice_SecteurPublic(ValPoint, TauxResid, IPlancher) = IMajore

        If V_CacheMaj(2).valeur = "" Then
            V_CacheMaj(2).valeur = "" & Math.Round(FicheTmp.Tranche1_Taux, 4)
        End If
        If V_CacheMaj(5).valeur = "" Then
            V_CacheMaj(5).valeur = "" & Math.Round(FicheTmp.Tranche2_Taux, 4)
        End If
        If V_CacheMaj(8).valeur = "" Then
            V_CacheMaj(8).valeur = "" & Math.Round(FicheTmp.Tranche3_Taux, 4)
        End If
        If V_CacheMaj(11).valeur = "" Then
            V_CacheMaj(11).valeur = "" & Math.Round(FicheTmp.Tranche4_Taux, 4)
        End If
    End Sub
#End Region

End Class
