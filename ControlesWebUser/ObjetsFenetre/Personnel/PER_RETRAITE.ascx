﻿<%@ Control Language="VB" AutoEventWireup="false" Inherits="Virtualia.Net.Fenetre_PER_RETRAITE" CodeBehind="PER_RETRAITE.ascx.vb" %>

<%@ Register Src="~/Controles/Saisies/VCoupleEtiDonnee.ascx" TagName="VCoupleEtiDonnee" TagPrefix="Virtualia" %>
<%@ Register Src="~/Controles/Saisies/VDuoEtiquetteCommande.ascx" TagName="VDuoEtiquetteCommande" TagPrefix="Virtualia" %>
<%@ Register Src="~/Controles/Saisies/VCoupleVerticalEtiDonnee.ascx" TagName="VCoupleVerticalEtiDonnee" TagPrefix="Virtualia" %>

<%@ Register Src="~/ControlsGeneriques/V_CommandeOK.ascx" TagName="VCmdOK" TagPrefix="Generic" %>

<asp:Table ID="CadreInfo" runat="server" BorderStyle="Ridge" BorderWidth="2px" BorderColor="#B0E0D7" Height="640px" Width="750px" HorizontalAlign="Center" Visible="true">
    <asp:TableRow>
        <asp:TableCell>
            <Generic:VCmdOK id="CtlOK" runat="server" height="22px" width="70px" horizontalalign="Right" cadrestyle="margin-top: 3px; margin-right: 3px" />
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="CadreTitre" runat="server" Height="40px" CellPadding="0" Width="750px" CellSpacing="0" HorizontalAlign="Center">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="EtiRetraite" runat="server" Text="Départ à la retraite" Height="20px" Width="330px" BackColor="#2FA49B" BorderColor="#B0E0D7" BorderStyle="Groove" BorderWidth="2px" ForeColor="#D7FAF3" Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Small" Style="margin-top: 0px; margin-left: 4px; margin-bottom: 0px; font-style: oblique; text-align: center;" />
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="TableauRetraite" runat="server" Height="40px" CellPadding="0" Width="750px" CellSpacing="0" HorizontalAlign="Left">
                <asp:TableRow>
                    <asp:TableCell ColumnSpan="2">
                        <Virtualia:VCoupleEtiDonnee ID="InfoH01" runat="server" V_PointdeVue="1" V_Objet="77" V_Information="1" V_SiDonneeDico="true" EtiWidth="250px" DonWidth="80px" DonTabIndex="1" EtiStyle="margin-top:10px; margin-left:50px;" DonStyle="margin-top:10px" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell ColumnSpan="2">
                        <Virtualia:VDuoEtiquetteCommande ID="Dontab02" runat="server" V_PointdeVue="1" V_Objet="77" V_Information="2" V_SiDonneeDico="true" EtiWidth="250px" DonWidth="335px" DonTabIndex="2" EtiStyle="margin-left:50px" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left" Width="150px">
                        <Virtualia:VCoupleEtiDonnee ID="InfoH07" runat="server" V_PointdeVue="1" V_Objet="77" V_Information="7" V_SiDonneeDico="true" EtiWidth="250px" DonWidth="40px" DonTabIndex="3" EtiStyle="margin-left:50px" />
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="EtiAns" runat="server" Text="ans" Height="20px" Width="43px" BackColor="Transparent" BorderColor="#B0E0D7" BorderStyle="None" ForeColor="#2FA49B" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Style="margin-top: 0px; margin-left: 2px; margin-bottom: 0px; font-style: oblique; text-indent: 5px; text-align: left;" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell ColumnSpan="2">
                        <Virtualia:VCoupleEtiDonnee ID="InfoH08" runat="server" V_PointdeVue="1" V_Objet="77" V_Information="8" V_SiDonneeDico="true" EtiWidth="250px" DonWidth="330px" DonTabIndex="4" EtiStyle="margin-left:50px" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center" ColumnSpan="2">
                        <asp:Label ID="EtiPension" runat="server" Text="Retraite / invalidité et pension" Height="20px" Width="330px" BackColor="#216B68" BorderColor="#B0E0D7" BorderStyle="Outset" BorderWidth="2px" ForeColor="#D7FAF3" Font-Italic="true" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Style="margin-top: 10px; margin-left: 4px; margin-bottom: 10px; text-align: center" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VCoupleEtiDonnee ID="InfoH03" runat="server" V_PointdeVue="1" V_Objet="77" V_Information="3" V_SiDonneeDico="true" EtiWidth="250px" DonWidth="80px" DonTabIndex="5" />
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VCoupleEtiDonnee ID="InfoH04" runat="server" V_PointdeVue="1" V_Objet="77" V_Information="4" V_SiDonneeDico="true" EtiWidth="250px" DonWidth="80px" DonTabIndex="6" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VCoupleEtiDonnee ID="InfoH05" runat="server" V_PointdeVue="1" V_Objet="77" V_Information="5" V_SiDonneeDico="true" EtiWidth="250px" DonWidth="80px" DonTabIndex="7" />
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VCoupleEtiDonnee ID="InfoH06" runat="server" V_PointdeVue="1" V_Objet="77" V_Information="6" V_SiDonneeDico="true" EtiWidth="250px" DonWidth="80px" DonTabIndex="8" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center" ColumnSpan="2">
                        <asp:Label ID="EtiProlongation" runat="server" Text="Prolongation d'activité" Height="20px" Width="330px" BackColor="#216B68" BorderColor="#B0E0D7" BorderStyle="Outset" BorderWidth="2px" ForeColor="#D7FAF3" Font-Italic="true" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Style="margin-top: 15px; margin-left: 4px; margin-bottom: 10px; text-indent: 5px; text-align: center" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VCoupleEtiDonnee ID="InfoH09" runat="server" V_PointdeVue="1" V_Objet="77" V_Information="9" V_SiDonneeDico="true" EtiWidth="250px" DonWidth="80px" DonTabIndex="9" />
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VCoupleEtiDonnee ID="InfoH10" runat="server" V_PointdeVue="1" V_Objet="77" V_Information="10" V_SiDonneeDico="true" EtiWidth="250px" DonWidth="80px" DonTabIndex="10" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left" ColumnSpan="2">
                        <Virtualia:VDuoEtiquetteCommande ID="Dontab11" runat="server" V_PointdeVue="1" V_Objet="77" V_Information="11" V_SiDonneeDico="true" EtiWidth="250px" DonWidth="375px" DonTabIndex="11" />
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="TableauRachatEtudes" runat="server" CellPadding="0" CellSpacing="0" Width="670px" HorizontalAlign="Center">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center" ColumnSpan="5">
                        <asp:Label ID="EtiRachatEtudes" runat="server" Text="Rachat d'études" Height="20px" Width="330px" BackColor="#216B68" BorderColor="#B0E0D7" BorderStyle="Outset" BorderWidth="2px" ForeColor="#D7FAF3" Font-Italic="true" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Style="margin-left: 4px; margin-bottom: 0px; text-indent: 5px; text-align: center" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="EtiLiquid" runat="server" Height="45px" Width="80px" Text="Liquidation" BackColor="#B0E0D7" BorderColor="#B0E0D7" BorderStyle="Outset" BorderWidth="2px" ForeColor="#142425" Font-Italic="true" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Style="margin-top: 35px; margin-left: 5px; text-indent: 5px; text-align: justify" />
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center" VerticalAlign="Middle">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="InfoV12" runat="server" V_PointdeVue="1" V_Objet="77" V_Information="12" V_SiDonneeDico="true" EtiWidth="105px" DonWidth="60px" DonTabIndex="12" DonHeight="16px" EtiStyle="text-align: center; margin-top: -5px" Donstyle="margin-top: 12px" />
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center" VerticalAlign="Middle">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="InfoV13" runat="server" V_PointdeVue="1" V_Objet="77" V_Information="13" V_SiDonneeDico="true" EtiWidth="135px" DonWidth="80px" DonTabIndex="13" DonHeight="16px" EtiStyle="text-align: center; margin-top: -5px" Donstyle="margin-top: 12px" />
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center" VerticalAlign="Middle">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="InfoV14" runat="server" V_PointdeVue="1" V_Objet="77" V_Information="14" V_SiDonneeDico="true" EtiWidth="160px" DonWidth="80px" DonTabIndex="14" DonHeight="16px" EtiStyle="text-align: center; margin-top: -5px" Donstyle="margin-top: 12px" />
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center" VerticalAlign="Middle">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="InfoV15" runat="server" V_PointdeVue="1" V_Objet="77" V_Information="15" V_SiDonneeDico="true" EtiWidth="160px" DonWidth="80px" DonTabIndex="15" DonHeight="16px" EtiStyle="text-align: center; margin-top: -5px" Donstyle="margin-top: 12px" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="EtiAssur" runat="server" Height="45px" Width="80px" Text="Assurance" BackColor="#B0E0D7" BorderColor="#B0E0D7" BorderStyle="Outset" BorderWidth="2px" ForeColor="#142425" Font-Italic="true" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Style="margin-top: 2px; margin-left: 5px; text-indent: 5px; text-align: justify" />
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center" VerticalAlign="Middle">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="InfoV16" runat="server" V_PointdeVue="1" V_Objet="77" V_Information="16" V_SiDonneeDico="true" EtiWidth="105px" DonWidth="60px" DonTabIndex="16" DonHeight="16px" EtiVisible="false" EtiStyle="text-align: center;" />
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center" VerticalAlign="Middle">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="InfoV17" runat="server" V_PointdeVue="1" V_Objet="77" V_Information="17" V_SiDonneeDico="true" EtiWidth="135px" DonWidth="80px" DonTabIndex="17" DonHeight="16px" EtiVisible="false" EtiStyle="text-align: center" />
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center" VerticalAlign="Middle">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="InfoV18" runat="server" V_PointdeVue="1" V_Objet="77" V_Information="18" V_SiDonneeDico="true" EtiWidth="160px" DonWidth="80px" DonTabIndex="18" DonHeight="16px" EtiVisible="false" EtiStyle="text-align: center" />
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center" VerticalAlign="Middle">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="InfoV19" runat="server" V_PointdeVue="1" V_Objet="77" V_Information="19" V_SiDonneeDico="true" EtiWidth="160px" DonWidth="80px" DonTabIndex="19" DonHeight="16px" EtiVisible="false" EtiStyle="text-align: center" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="EtiLiquAssur" runat="server" Height="45px" Width="80px" Text="Liquidation et assurance" BackColor="#B0E0D7" BorderColor="#B0E0D7" BorderStyle="Outset" BorderWidth="2px" ForeColor="#142425" Font-Italic="true" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Style="margin-top: 2px; margin-left: 5px; text-indent: 5px; text-align: justify" />
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center" VerticalAlign="Middle">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="InfoV20" runat="server" V_PointdeVue="1" V_Objet="77" V_Information="20" V_SiDonneeDico="true" EtiWidth="105px" DonWidth="60px" DonTabIndex="20" DonHeight="16px" EtiVisible="false" EtiStyle="text-align: center;" />
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center" VerticalAlign="Middle">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="InfoV21" runat="server" V_PointdeVue="1" V_Objet="77" V_Information="21" V_SiDonneeDico="true" EtiWidth="135px" DonWidth="80px" DonTabIndex="21" DonHeight="16px" EtiVisible="false" EtiStyle="text-align: center" />
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center" VerticalAlign="Middle">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="InfoV22" runat="server" V_PointdeVue="1" V_Objet="77" V_Information="22" V_SiDonneeDico="true" EtiWidth="160px" DonWidth="80px" DonTabIndex="22" DonHeight="16px" EtiVisible="false" EtiStyle="text-align: center" />
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center" VerticalAlign="Middle">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="InfoV23" runat="server" V_PointdeVue="1" V_Objet="77" V_Information="23" V_SiDonneeDico="true" EtiWidth="160px" DonWidth="80px" DonTabIndex="23" DonHeight="16px" EtiVisible="false" EtiStyle="text-align: center" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="12px" ColumnSpan="5" />
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>
</asp:Table>
