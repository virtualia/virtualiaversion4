﻿<%@ Control Language="VB" AutoEventWireup="false" Inherits="Virtualia.Net.Fenetre_PER_ACTION_ENCOURS" Codebehind="PER_ACTION_ENCOURS.ascx.vb" %>

 <asp:Table ID="CadreInfo" runat="server" BorderStyle="None" BorderWidth="2px" Visible="true" Height="190px" BorderColor="#B0E0D7" Width="500px" HorizontalAlign="Left" style="margin-top: 1px;">
    
    <asp:TableRow>
      <asp:TableCell HorizontalAlign="Left">
            <asp:Table ID="CadreActionEnCours" runat="server" CellPadding="0" Width="419px" Height="21px" CellSpacing="0" HorizontalAlign="Left" BorderStyle="Notset" BorderWidth="1px" Visible="true" BorderColor="#7EC8BE" style="margin-top: 1px;margin-bottom: 0px;">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                       <asp:Label ID="LabelObjet" runat="server" Height="21px" Width="419px" BackColor="#CAEBE4" BorderColor="Transparent" BorderStyle="None" Text="Journées ARTT le 25/05/2010" BorderWidth="1px" ForeColor="#124545" Font-Italic="False" Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Small" style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; font-style: normal; text-indent: 1px; text-align:  center; padding-top: 2px;" >
                       </asp:Label>      
                    </asp:TableCell>   
                </asp:TableRow>
            </asp:Table>
      </asp:TableCell>
      <asp:TableCell HorizontalAlign="Left">
           <asp:Table ID="CadreCommande" runat="server" Height="24px" CellPadding="0" 
                CellSpacing="0" BackImageUrl="~/Images/Boutons/SupprimerFiche_Std.bmp"
                BorderWidth="2px" BorderStyle="Outset" BorderColor="#B0E0D7" ForeColor="#D7FAF3"
                Width="68px" HorizontalAlign="Left" style="margin-top: 1px; margin-left: -3px;margin-bottom: 0px;">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                         <asp:Button ID="CommandeSupp" runat="server" Text="Supprimer" Width="68px" Height="20px"
                             BackColor="Transparent" BorderColor="#B0E0D7" ForeColor="#D7FAF3"
                             Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Font-Italic="true"
                             BorderStyle="None" style="margin-top: 0px; margin-left: 8px; text-align: center;"
                             Tooltip="Supprimer la fiche">
                         </asp:Button>
                    </asp:TableCell>
                </asp:TableRow>
           </asp:Table>
      </asp:TableCell>
    </asp:TableRow>
    
    <asp:TableRow>
      <asp:TableCell HorizontalAlign="Left" ColumnSpan="2">
            <asp:Table ID="TableDemande" runat="server" CellPadding="0" Width="500px" Height="45px"
            CellSpacing="0" HorizontalAlign="Left" BorderStyle="Notset" BorderWidth="1px" Visible="true" BorderColor="#7EC8BE"
            style="margin-top: -5px;margin-bottom: 0px;">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left" Height="15px">
                       <asp:Label ID="LabelNumeroDemande" runat="server" Height="15px" Width="120px"
                                BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" Text="Demande n°362"
                                BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Font-Underline="true"
                                style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                font-style: normal; text-indent: 4px; text-align:  Left; padding-top: 2px;" >
                       </asp:Label>      
                    </asp:TableCell>   
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left" Height="28px">
                       <asp:Label ID="LabelDemande" runat="server" Height="28px" Width="500px"
                                BackColor= "Transparent" BorderColor="Transparent" BorderStyle="None" Text="Faite le 10/05/2010 à 10:23"
                                BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                style="margin-top: 4px; margin-left: 0px; margin-bottom: 0px;
                                font-style: normal; text-indent: 1px; text-align:  center;" >
                       </asp:Label>      
                    </asp:TableCell>   
                </asp:TableRow>
            </asp:Table>
      </asp:TableCell>
    </asp:TableRow>  
        
    <asp:TableRow>
      <asp:TableCell HorizontalAlign="Left" ColumnSpan="2">
            <asp:Table ID="TableContenu" runat="server" CellPadding="0" Width="500px" Height="75px"
            CellSpacing="0" HorizontalAlign="Left" BorderStyle="Notset" BorderWidth="1px" Visible="true" BorderColor="#7EC8BE"
            style="margin-top: -5px;margin-bottom: 0px;">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left" Height="15px"  ColumnSpan="2">
                       <asp:Label ID="LabelContenu" runat="server" Height="15px" Width="120px"
                                BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" Text="Contenu"
                                BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Font-Underline="true"
                                style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                font-style: normal; text-indent: 4px; text-align:  Left; padding-top: 2px;" >
                       </asp:Label>      
                    </asp:TableCell>   
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left" Height="20px" ColumnSpan="2">
                       <asp:Label ID="LabelLigne1" runat="server" Height="20px" Width="500px"
                                BackColor= "Transparent" BorderColor="Transparent" BorderStyle="None" Text="Journées ARTT le 25/05/2010 toute la journée"
                                BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                style="margin-top: 4px; margin-left: 0px; margin-bottom: 0px;
                                font-style: normal; text-indent: 1px; text-align:  center;" >
                       </asp:Label>      
                    </asp:TableCell>   
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left" Height="20px">
                       <asp:Label ID="LabelLigne2Gauche" runat="server" Height="20px" Width="250px"
                                BackColor= "Transparent" BorderColor="Transparent" BorderStyle="None" Text="Date de la demande :"
                                BorderWidth="1px" ForeColor= "#9EB0AC" Font-Italic="False"
                                Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="12px"
                                style="margin-top: 4px; margin-left: 0px; margin-bottom: 0px;
                                font-style: normal; text-indent: 1px; text-align:  right;" >
                       </asp:Label>      
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left" Height="20px">
                       <asp:Label ID="LabelLigne2Droite" runat="server" Height="20px" Width="250px"
                                BackColor= "Transparent" BorderColor="Transparent" BorderStyle="None" Text="18/04/2010"
                                BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="12px"
                                style="margin-top: 4px; margin-left: 0px; margin-bottom: 0px;
                                font-style: normal; text-indent: 4px; text-align:  left;" >
                       </asp:Label>      
                    </asp:TableCell>    
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left" Height="20px">
                       <asp:Label ID="LabelLigne3Gauche" runat="server" Height="20px" Width="250px"
                                BackColor= "Transparent" BorderColor="Transparent" BorderStyle="None" Text="Objet :"
                                BorderWidth="1px" ForeColor= "#9EB0AC" Font-Italic="False"
                                Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="12px"
                                style="margin-top: 4px; margin-left: 0px; margin-bottom: 0px;
                                font-style: normal; text-indent: 1px; text-align:  right;" >
                       </asp:Label>      
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left" Height="20px">
                       <asp:Label ID="LabelLigne3Droite" runat="server" Height="20px" Width="250px"
                                BackColor= "Transparent" BorderColor="Transparent" BorderStyle="None" Text="Formation Virtualia"
                                BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="12px"
                                style="margin-top: 4px; margin-left: 0px; margin-bottom: 0px;
                                font-style: normal; text-indent: 4px; text-align:  left;" >
                       </asp:Label>      
                    </asp:TableCell>    
                </asp:TableRow>
            </asp:Table>
      </asp:TableCell>
    </asp:TableRow>
      
    <asp:TableRow>
      <asp:TableCell HorizontalAlign="Left" ColumnSpan="2">
            <asp:Table ID="TableauObservations" runat="server" CellPadding="0" Width="500px" Height="50px"
            CellSpacing="0" HorizontalAlign="Left" BorderStyle="Notset" BorderWidth="1px" Visible="true" BorderColor="#7EC8BE"
            style="margin-top: -5px;margin-bottom: 0px;">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left" Height="15px">
                       <asp:Label ID="LabelObservations" runat="server" Height="15px" Width="120px"
                                BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" Text="Observations"
                                BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Font-Underline="true"
                                style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                font-style: normal; text-indent: 4px; text-align:  Left; padding-top: 2px;" >
                       </asp:Label>      
                    </asp:TableCell>   
                </asp:TableRow>
                <asp:TableRow>
                  <asp:TableCell HorizontalAlign="Left" Height="20px">
                       <asp:Label ID="LabelObs1" runat="server" Height="20px" Width="500px"
                                BackColor= "Transparent" BorderColor="Transparent" BorderStyle="None" Text="Comme convenu la semaine dernière en réunion"
                                BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                style="margin-top: 4px; margin-left: 0px; margin-bottom: 0px;
                                font-style: normal; text-indent: 1px; text-align:  center;" >
                       </asp:Label>      
                    </asp:TableCell>     
                </asp:TableRow>   
            </asp:Table>
      </asp:TableCell>
    </asp:TableRow>
  
 </asp:Table>