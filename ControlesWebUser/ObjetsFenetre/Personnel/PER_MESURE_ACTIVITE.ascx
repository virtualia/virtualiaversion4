﻿<%@ Control Language="VB" AutoEventWireup="false" Inherits="Virtualia.Net.Fenetre_PER_MESURE_ACTIVITE" Codebehind="PER_MESURE_ACTIVITE.ascx.vb" %>

<%@ Register src="~/Controles/Saisies/VCoupleEtiDonnee.ascx" tagname="VCoupleEtiDonnee" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/Saisies/VDuoEtiquetteCommande.ascx" tagname="VDuoEtiquetteCommande" tagprefix="Virtualia" %>


 <asp:Table ID="CadreInfo" runat="server" BorderStyle="None" BorderWidth="2px" BorderColor="#B0E0D7" Height="500px" Width="750px" HorizontalAlign="Center" style="margin-top: 3px;">
    <asp:TableRow>
      <asp:TableCell>
        <asp:Table ID="CadreCommandes" runat="server" Height="22px" CellPadding="0" CellSpacing="0"
            Width="244px" HorizontalAlign="Right" style="margin-top: 3px;margin-right: 3px">
            <asp:TableRow>
                <asp:TableCell VerticalAlign="Bottom">
                    <asp:Table ID="CadreCmdNewSupp" runat="server" BackImageUrl="~/Images/Boutons/NewSupp_Std.bmp" 
                        Width="160px" CellPadding="0" CellSpacing="0"
                        BorderWidth="2px" BorderStyle="Outset" BorderColor="#B0E0D7" ForeColor="#D7FAF3">
                        <asp:TableRow>
                            <asp:TableCell>
                                <asp:Button ID="CommandeNew" runat="server" Text="Nouveau" Width="68px" Height="20px"
                                    BackColor="Transparent" BorderColor="#B0E0D7" ForeColor="#D7FAF3"
                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Font-Italic="true"
                                    BorderStyle="None" style=" margin-left: 10px; text-align: right;"
                                    Tooltip="Nouvelle fiche">
                                </asp:Button>
                            </asp:TableCell>
                            <asp:TableCell>
                                <asp:Button ID="CommandeSupp" runat="server" Text="Supprimer" Width="68px" Height="20px"
                                    BackColor="Transparent" BorderColor="#B0E0D7" ForeColor="#D7FAF3"
                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Font-Italic="true"
                                    BorderStyle="None" style=" margin-left: 10px; text-align: right;"
                                    Tooltip="Supprimer la fiche">
                                </asp:Button>
                            </asp:TableCell>
                        </asp:TableRow>
                    </asp:Table>    
                </asp:TableCell>
                <asp:TableCell VerticalAlign="Bottom">
                    <asp:Table ID="CadreCmdOK" runat="server" BackImageUrl="~/Images/Boutons/OK_Std.bmp" Width="70px"
                        BorderWidth="2px" BorderStyle="Outset" BorderColor="#B0E0D7" ForeColor="#D7FAF3"
                        CellPadding="0" CellSpacing="0" Visible="false">
                        <asp:TableRow>
                            <asp:TableCell>
                                <asp:Button ID="CommandeOK" runat="server" Text="Valider" Width="65px" Height="20px"
                                    BackColor="Transparent" BorderColor="#B0E0D7" ForeColor="#D7FAF3" 
                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Font-Italic="true"
                                    BorderStyle="None" style=" margin-left: 6px; text-align: right;">
                                    </asp:Button>
                            </asp:TableCell>
                        </asp:TableRow>
                    </asp:Table>  
                </asp:TableCell>
            </asp:TableRow>
          </asp:Table>
      </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
      <asp:TableCell>
          <asp:Table ID="CadreTitre" runat="server" Height="40px" CellPadding="0" Width="750px" 
            CellSpacing="0" HorizontalAlign="Center">
            <asp:TableRow>
                <asp:TableCell HorizontalAlign="Center">
                    <asp:Label ID="Etiquette" runat="server" Text="Déclaration des activités" Height="20px" Width="400px"
                        BackColor="#2FA49B" BorderColor="#B0E0D7" BorderStyle="Groove"
                        BorderWidth="2px" ForeColor="#D7FAF3"
                        Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Small"
                        style="margin-top: 5px; margin-left: 4px; margin-bottom: 20px;
                        font-style: oblique; text-indent: 5px; text-align: center;">
                    </asp:Label>          
                </asp:TableCell>      
            </asp:TableRow>
          </asp:Table>
      </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
      <asp:TableCell>
        <asp:Table ID="CadreActivite" runat="server" CellPadding="0" CellSpacing="0" Width="750px"
        BorderStyle="Ridge" BorderWidth="2px" BorderColor="#B0E0D7" HorizontalAlign="Left">
          <asp:TableRow Width="740px">
            <asp:TableCell BackColor="#D7FAF3" BorderColor="#D7FAF3" BorderStyle="Notset"> 
              <asp:Label ID="LabelMission" runat="server" Height="22px" Width="50px"
                  BackColor="#D7FAF3" BorderColor="#D7FAF3" BorderStyle="Notset" Text="mission"
                  BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                  Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                  style="margin-top: 5px; margin-left: 0px; margin-bottom: 1px;
                  font-style: normal; text-indent: 2px; text-align:  left; padding-top: 3px;">
              </asp:Label>          
            </asp:TableCell>
            <asp:TableCell BackColor="#D7FAF3" BorderColor="#D7FAF3" BorderStyle="Notset"> 
              <asp:Button ID="DonMission" runat="server" Height="22px" Width="270px" 
                  BackColor="White" BorderColor="#B0E0D7"  BorderStyle="Inset"
                  BorderWidth="1px" ForeColor="Black" Visible="true"
                  Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Small" Font-Italic="false"
                  style="padding-left: 1px; margin-top: 5px; text-align: left"> 
              </asp:Button>        
            </asp:TableCell>
            <asp:TableCell BackColor="#D7FAF3" BorderColor="#D7FAF3" BorderStyle="Notset"> 
              <asp:Label ID="LabelJanvier" runat="server" Height="23px" Width="30px" 
                  BackColor="#A8BBB8" BorderColor="#D7FAF3" BorderStyle="NotSet" Text="Janv"
                  BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                  Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "X-Small"
                  style="margin-top: 3px; margin-left: 0px; margin-bottom: 1px;
                  font-style: normal; text-indent: 0px;  text-align:  center; padding-top: 5px;">
              </asp:Label>          
            </asp:TableCell>
            <asp:TableCell BackColor="#D7FAF3" BorderColor="#D7FAF3" BorderStyle="Notset"> 
              <asp:Label ID="LabelFevrier" runat="server" Height="23px" Width="30px"
                  BackColor="#A8BBB8" BorderColor="#D7FAF3" BorderStyle="NotSet" Text="Févr"
                  BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                  Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "X-Small"
                  style="margin-top: 3px; margin-left: 0px; margin-bottom: 1px;
                  font-style: normal; text-indent: 0px; text-align:  center; padding-top: 5px;">
              </asp:Label>          
            </asp:TableCell>
            <asp:TableCell BackColor="#D7FAF3" BorderColor="#D7FAF3" BorderStyle="Notset"> 
              <asp:Label ID="LabelMars" runat="server" Height="23px" Width="30px"
                  BackColor="#A8BBB8" BorderColor="#D7FAF3" BorderStyle="NotSet" Text="Mars"
                  BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                  Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "X-Small"
                  style="margin-top: 3px; margin-left: 0px; margin-bottom: 1px;
                  font-style: normal; text-indent: 0px; text-align:  center; padding-top: 5px;">
              </asp:Label>          
            </asp:TableCell>
            <asp:TableCell BackColor="#D7FAF3" BorderColor="#D7FAF3" BorderStyle="Notset"> 
              <asp:Label ID="LabelAvril" runat="server" Height="23px" Width="30px"
                  BackColor="#A8BBB8" BorderColor="#D7FAF3" BorderStyle="NotSet" Text="Avr"
                  BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                  Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "X-Small"
                  style="margin-top: 3px; margin-left: 0px; margin-bottom: 1px;
                  font-style: normal; text-indent: 0px; text-align:  center; padding-top: 5px;">
              </asp:Label>          
            </asp:TableCell>
            <asp:TableCell BackColor="#D7FAF3" BorderColor="#D7FAF3" BorderStyle="Notset"> 
              <asp:Label ID="LabelMai" runat="server" Height="23px" Width="30px"
                  BackColor="#A8BBB8" BorderColor="#D7FAF3" BorderStyle="NotSet" Text="Mai"
                  BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                  Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "X-Small"
                  style="margin-top: 3px; margin-left: 0px; margin-bottom: 1px;
                  font-style: normal; text-indent: 0px; text-align:  center; padding-top: 5px;">
              </asp:Label>          
            </asp:TableCell>
            <asp:TableCell BackColor="#D7FAF3" BorderColor="#D7FAF3" BorderStyle="Notset"> 
              <asp:Label ID="LabelJuin" runat="server" Height="23px" Width="30px"
                  BackColor="#A8BBB8" BorderColor="#D7FAF3" BorderStyle="NotSet" Text="Juin"
                  BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                  Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "X-Small"
                  style="margin-top: 3px; margin-left: 0px; margin-bottom: 1px;
                  font-style: normal; text-indent: 0px; text-align:  center; padding-top: 5px;">
              </asp:Label>          
            </asp:TableCell>
            <asp:TableCell BackColor="#D7FAF3" BorderColor="#D7FAF3" BorderStyle="Notset"> 
              <asp:Label ID="LabelJuillet" runat="server" Height="23px" Width="30px"
                  BackColor="#A8BBB8" BorderColor="#D7FAF3" BorderStyle="NotSet" Text="Juil"
                  BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                  Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "X-Small"
                  style="margin-top: 3px; margin-left: 0px; margin-bottom: 1px;
                  font-style: normal; text-indent: 0px; text-align:  center; padding-top: 5px;">
              </asp:Label>          
            </asp:TableCell>
            <asp:TableCell BackColor="#D7FAF3" BorderColor="#D7FAF3" BorderStyle="Notset"> 
              <asp:Label ID="LabelAout" runat="server" Height="23px" Width="30px"
                  BackColor="#A8BBB8" BorderColor="#D7FAF3" BorderStyle="NotSet" Text="Août"
                  BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                  Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "X-Small"
                  style="margin-top: 3px; margin-left: 0px; margin-bottom: 1px;
                  font-style: normal; text-indent: 0px; text-align:  center; padding-top: 5px;">
              </asp:Label>          
            </asp:TableCell>
            <asp:TableCell BackColor="#D7FAF3" BorderColor="#D7FAF3" BorderStyle="Notset"> 
              <asp:Label ID="LabelSeptembre" runat="server" Height="23px" Width="30px"
                  BackColor="#A8BBB8" BorderColor="#D7FAF3" BorderStyle="NotSet" Text="Sept"
                  BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                  Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "X-Small"
                  style="margin-top: 3px; margin-left: 0px; margin-bottom: 1px;
                  font-style: normal; text-indent: 0px; text-align:  center; padding-top: 5px;">
              </asp:Label>          
            </asp:TableCell>
            <asp:TableCell BackColor="#D7FAF3" BorderColor="#D7FAF3" BorderStyle="Notset"> 
              <asp:Label ID="LabelOctobre" runat="server" Height="23px" Width="30px"
                  BackColor="#A8BBB8" BorderColor="#D7FAF3" BorderStyle="NotSet" Text="Oct"
                  BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                  Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "X-Small"
                  style="margin-top: 3px; margin-left: 0px; margin-bottom: 1px;
                  font-style: normal; text-indent: 0px; text-align:  center; padding-top: 5px;">
              </asp:Label>          
            </asp:TableCell>
            <asp:TableCell BackColor="#D7FAF3" BorderColor="#D7FAF3" BorderStyle="Notset"> 
              <asp:Label ID="LabelNovembre" runat="server" Height="23px" Width="30px"
                  BackColor="#A8BBB8" BorderColor="#D7FAF3" BorderStyle="NotSet" Text="Nov"
                  BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                  Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "X-Small"
                  style="margin-top: 3px; margin-left: 0px; margin-bottom: 1px;
                  font-style: normal; text-indent: 0px; text-align:  center; padding-top: 5px;">
              </asp:Label>          
            </asp:TableCell>
            <asp:TableCell BackColor="#D7FAF3" BorderColor="#D7FAF3" BorderStyle="Notset"> 
              <asp:Label ID="LabelDecembre" runat="server" Height="23px" Width="30px"
                  BackColor="#A8BBB8" BorderColor="#D7FAF3" BorderStyle="NotSet" Text="Déc"
                  BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                  Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "X-Small"
                  style="margin-top: 3px; margin-left: 0px; margin-bottom: 1px;
                  font-style: normal; text-indent: 0px; text-align:  center; padding-top: 5px;">
              </asp:Label>          
            </asp:TableCell>
            <asp:TableCell BackColor="#D7FAF3" BorderColor="#D7FAF3" BorderStyle="Notset"> 
              <asp:Label ID="LabelAnnee" runat="server" Height="23px" Width="36px"
                  BackColor="#A8BBB8" BorderColor="#D7FAF3" BorderStyle="NotSet" Text="Annuel"
                  BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                  Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "X-Small"
                  style="margin-top: 3px; margin-left: 0px; margin-bottom: 1px;
                  font-style: normal; text-indent: 0px; text-align:  center; padding-top: 5px;">
              </asp:Label>          
            </asp:TableCell>      
          </asp:TableRow>
          <asp:TableRow Width="748px">
            <asp:TableCell BackColor="#CAEBE4" BorderColor="#CAEBE4" BorderStyle="Notset"> 
              <asp:Label ID="LabelActvite" runat="server" Height="22px" Width="50px"
                  BackColor="#CAEBE4" BorderColor="#CAEBE4" BorderStyle="Notset" Text="activité"
                  BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                  Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                  style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                  font-style: normal; text-indent: 2px; text-align:  left; padding-top: 3px;">
              </asp:Label>          
            </asp:TableCell>
            <asp:TableCell BackColor="#CAEBE4" BorderColor="#CAEBE4" BorderStyle="Notset"> 
              <asp:Button ID="DonActivite" runat="server" Height="22px" Width="270px" 
                  BackColor="White" BorderColor="#B0E0D7"  BorderStyle="Inset"
                  BorderWidth="1px" ForeColor="Black" Visible="true"
                  Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Small" Font-Italic="false"
                  style="padding-left: 1px; margin-top: 0px; text-align: left"> 
              </asp:Button>        
            </asp:TableCell>
            <asp:TableCell RowSpan="1" BackColor="#CAEBE4" BorderColor="#CAEBE4" BorderStyle="Notset"> 
              <asp:TextBox ID="DonM01" runat="server" Height="14px" Width="24px" MaxLength="0"
                  BackColor="White" BorderColor="#B0E0D7" BorderStyle="Inset" Text=""
                  BorderWidth="1px" ForeColor="Black" Visible="true" AutoPostBack="true"
                  Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                  style="margin-top: 1px; margin-left: 2px; text-indent: 1px; text-align: center" TextMode="SingleLine"> 
              </asp:TextBox>
            </asp:TableCell>
            <asp:TableCell RowSpan="1" BackColor="#CAEBE4" BorderColor="#CAEBE4" BorderStyle="Notset"> 
              <asp:TextBox ID="DonM02" runat="server" Height="14px" Width="24px" MaxLength="0"
                  BackColor="White" BorderColor="#B0E0D7" BorderStyle="Inset" Text=""
                  BorderWidth="1px" ForeColor="Black" Visible="true" AutoPostBack="true"
                  Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                  style="margin-top: 1px; margin-left: 2px; text-indent: 1px; text-align: center" TextMode="SingleLine"> 
              </asp:TextBox> 
            </asp:TableCell>
            <asp:TableCell RowSpan="1" BackColor="#CAEBE4" BorderColor="#CAEBE4" BorderStyle="Notset"> 
              <asp:TextBox ID="DonM03" runat="server" Height="14px" Width="24px" MaxLength="0"
                  BackColor="White" BorderColor="#B0E0D7" BorderStyle="Inset" Text=""
                  BorderWidth="1px" ForeColor="Black" Visible="true" AutoPostBack="true"
                  Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                  style="margin-top: 1px; margin-left: 2px; text-indent: 1px; text-align: center" TextMode="SingleLine"> 
              </asp:TextBox>         
            </asp:TableCell>
            <asp:TableCell RowSpan="1" BackColor="#CAEBE4" BorderColor="#CAEBE4" BorderStyle="Notset"> 
              <asp:TextBox ID="DonM04" runat="server" Height="14px" Width="24px" MaxLength="0"
                  BackColor="White" BorderColor="#B0E0D7" BorderStyle="Inset" Text=""
                  BorderWidth="1px" ForeColor="Black" Visible="true" AutoPostBack="true"
                  Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                  style="margin-top: 1px; margin-left: 2px; text-indent: 1px; text-align: center" TextMode="SingleLine"> 
              </asp:TextBox>           
            </asp:TableCell>
            <asp:TableCell RowSpan="1" BackColor="#CAEBE4" BorderColor="#CAEBE4" BorderStyle="Notset"> 
              <asp:TextBox ID="DonM05" runat="server" Height="14px" Width="24px" MaxLength="0"
                  BackColor="White" BorderColor="#B0E0D7" BorderStyle="Inset" Text=""
                  BorderWidth="1px" ForeColor="Black" Visible="true" AutoPostBack="true"
                  Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                  style="margin-top: 1px; margin-left: 2px; text-indent: 1px; text-align: center" TextMode="SingleLine"> 
              </asp:TextBox>           
            </asp:TableCell>
            <asp:TableCell RowSpan="1" BackColor="#CAEBE4" BorderColor="#CAEBE4" BorderStyle="Notset"> 
              <asp:TextBox ID="DonM06" runat="server" Height="14px" Width="24px" MaxLength="0"
                  BackColor="White" BorderColor="#B0E0D7" BorderStyle="Inset" Text=""
                  BorderWidth="1px" ForeColor="Black" Visible="true" AutoPostBack="true"
                  Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                  style="margin-top: 1px; margin-left: 2px; text-indent: 1px; text-align: center" TextMode="SingleLine"> 
              </asp:TextBox>        
            </asp:TableCell>
            <asp:TableCell RowSpan="1" BackColor="#CAEBE4" BorderColor="#CAEBE4" BorderStyle="Notset"> 
              <asp:TextBox ID="DonM07" runat="server" Height="14px" Width="24px" MaxLength="0"
                  BackColor="White" BorderColor="#B0E0D7" BorderStyle="Inset" Text=""
                  BorderWidth="1px" ForeColor="Black" Visible="true" AutoPostBack="true"
                  Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                  style="margin-top: 1px; margin-left: 2px; text-indent: 1px; text-align: center" TextMode="SingleLine"> 
              </asp:TextBox>           
            </asp:TableCell>
            <asp:TableCell RowSpan="1" BackColor="#CAEBE4" BorderColor="#CAEBE4" BorderStyle="Notset"> 
              <asp:TextBox ID="DonM08" runat="server" Height="14px" Width="24px" MaxLength="0"
                  BackColor="White" BorderColor="#B0E0D7" BorderStyle="Inset" Text=""
                  BorderWidth="1px" ForeColor="Black" Visible="true" AutoPostBack="true"
                  Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                  style="margin-top: 1px; margin-left: 2px; text-indent: 1px; text-align: center" TextMode="SingleLine"> 
              </asp:TextBox>           
            </asp:TableCell>
            <asp:TableCell RowSpan="1" BackColor="#CAEBE4" BorderColor="#CAEBE4" BorderStyle="Notset"> 
              <asp:TextBox ID="DonM09" runat="server" Height="14px" Width="24px" MaxLength="0"
                  BackColor="White" BorderColor="#B0E0D7" BorderStyle="Inset" Text=""
                  BorderWidth="1px" ForeColor="Black" Visible="true" AutoPostBack="true"
                  Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                  style="margin-top: 1px; margin-left: 2px; text-indent: 1px; text-align: center" TextMode="SingleLine"> 
              </asp:TextBox>           
            </asp:TableCell>
            <asp:TableCell RowSpan="1" BackColor="#CAEBE4" BorderColor="#CAEBE4" BorderStyle="Notset"> 
              <asp:TextBox ID="DonM10" runat="server" Height="14px" Width="24px" MaxLength="0"
                  BackColor="White" BorderColor="#B0E0D7" BorderStyle="Inset" Text=""
                  BorderWidth="1px" ForeColor="Black" Visible="true" AutoPostBack="true"
                  Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                  style="margin-top: 1px; margin-left: 2px; text-indent: 1px; text-align: center" TextMode="SingleLine"> 
              </asp:TextBox>           
            </asp:TableCell>
            <asp:TableCell RowSpan="1" BackColor="#CAEBE4" BorderColor="#CAEBE4" BorderStyle="Notset"> 
              <asp:TextBox ID="DonM11" runat="server" Height="14px" Width="24px" MaxLength="0"
                  BackColor="White" BorderColor="#B0E0D7" BorderStyle="Inset" Text=""
                  BorderWidth="1px" ForeColor="Black" Visible="true" AutoPostBack="true"
                  Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                  style="margin-top: 1px; margin-left: 2px; text-indent: 1px; text-align: center" TextMode="SingleLine"> 
              </asp:TextBox>           
            </asp:TableCell>
            <asp:TableCell RowSpan="1" BackColor="#CAEBE4" BorderColor="#CAEBE4" BorderStyle="Notset"> 
              <asp:TextBox ID="DonM12" runat="server" Height="14px" Width="24px" MaxLength="0"
                  BackColor="White" BorderColor="#B0E0D7" BorderStyle="Inset" Text=""
                  BorderWidth="1px" ForeColor="Black" Visible="true" AutoPostBack="true"
                  Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                  style="margin-top: 1px; margin-left: 2px; text-indent: 1px; text-align: center" TextMode="SingleLine"> 
              </asp:TextBox>           
            </asp:TableCell>
            <asp:TableCell RowSpan="1" BackColor="#CAEBE4" BorderColor="#CAEBE4" BorderStyle="Notset"> 
              <asp:TextBox ID="DonAnnee" runat="server" Height="14px" Width="30px" MaxLength="0"
                  BackColor="White" BorderColor="#B0E0D7" BorderStyle="Inset" Text=""
                  BorderWidth="1px" ForeColor="Black" Visible="true" AutoPostBack="true"
                  Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                  style="margin-top: 1px; margin-left: 3px; text-indent: 1px; text-align: center" TextMode="SingleLine"> 
              </asp:TextBox>         
            </asp:TableCell>      
          </asp:TableRow>
          <asp:TableRow Width="748px">
            <asp:TableCell ColumnSpan="1" BackColor="#B0E0D7" BorderColor="#B0E0D7" BorderStyle="Notset"> 
              <asp:Label ID="LabelMesure" runat="server" Height="22px" Width="50px"
                  BackColor="#B0E0D7" BorderColor="#B0E0D7" BorderStyle="Notset" Text="mesure"
                  BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                  Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                  style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                  font-style: normal; text-indent: 2px; text-align:  left; padding-top: 3px;">
              </asp:Label>          
            </asp:TableCell>
            <asp:TableCell ColumnSpan="1" BackColor="#B0E0D7" BorderColor="#B0E0D7" BorderStyle="Notset"> 
              <asp:Button ID="DonMesure" runat="server" Height="22px" Width="270px" 
                  BackColor="White" BorderColor="#B0E0D7"  BorderStyle="Inset"
                  BorderWidth="1px" ForeColor="Black" Visible="true"
                  Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Small" Font-Italic="false"
                  style="padding-left: 1px; margin-top: 0px; text-align: left"> 
              </asp:Button>        
            </asp:TableCell>
            <asp:TableCell ColumnSpan="13" BackColor="#B0E0D7" BorderColor="#B0E0D7" BorderStyle="Notset"> 
              <asp:Label ID="LabelLigneMesure" runat="server" Height="22px" Width="5px"
                  BackColor="Transparent" BorderColor="White" BorderStyle="None" Text=""
                  BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                  Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                  style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                  font-style: normal; text-indent: 17px; text-align:  left; padding-top: 3px;">
              </asp:Label> 
            </asp:TableCell>
          </asp:TableRow>
          <asp:TableRow Width="748px">
            <asp:TableCell ColumnSpan="1" BackColor="#98D4CA" BorderColor="#98D4CA" BorderStyle="Notset"> 
              <asp:Label ID="LabelFonction" runat="server" Height="22px" Width="50px"
                  BackColor="#98D4CA" BorderColor="#98D4CA" BorderStyle="Notset" Text="fonction"
                  BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                  Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                  style="margin-top: 5px; margin-left: 0px; margin-bottom: 3px;
                  font-style: normal; text-indent: 2px; text-align:  left; padding-top: 3px;">
              </asp:Label>          
            </asp:TableCell>
            <asp:TableCell ColumnSpan="1" BackColor="#98D4CA" BorderColor="#98D4CA" BorderStyle="Notset"> 
              <asp:Button ID="DonFonction" runat="server" Height="22px" Width="270px" 
                  BackColor="White" BorderColor="#B0E0D7"  BorderStyle="Inset"
                  BorderWidth="1px" ForeColor="Black" Visible="true"
                  Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Small" Font-Italic="false"
                  style="padding-left: 1px; margin-top: 5px; text-align: left; margin-bottom: 3px;"> 
              </asp:Button>        
            </asp:TableCell>
            <asp:TableCell ColumnSpan="13" BackColor="#98D4CA" BorderColor="#98D4CA" BorderStyle="Notset"> 
              <asp:Label ID="LabelLigneFonction" runat="server" Height="22px" Width="5px"
                  BackColor="Transparent" BorderColor="White" BorderStyle="None" Text=""
                  BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                  Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                  style="margin-top: 5px; margin-left: 0px; margin-bottom: 3px;
                  font-style: normal; text-indent: 18px; text-align:  left; padding-top: 3px;">
              </asp:Label> 
            </asp:TableCell>
          </asp:TableRow>
        </asp:Table>
      </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
      <asp:TableCell>
          <asp:Table ID="Separateur" runat="server" CellPadding="0" CellSpacing="0" Width="750px"
          BorderStyle="None" BorderWidth="2px" BorderColor="#B0E0D7" HorizontalAlign="Left">
            <asp:TableRow>
                <asp:TableCell Height="20px"></asp:TableCell>
            </asp:TableRow>
          </asp:Table>
      </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
      <asp:TableCell>
        <asp:Table ID="TableauTotauxActivites" runat="server" CellPadding="0" CellSpacing="0" Width="750px"
        BorderStyle="Ridge" BorderWidth="2px" BorderColor="#B0E0D7" HorizontalAlign="Left">
            <asp:TableRow Width="740px">
             <asp:TableCell BackColor="#D7FAF3" BorderColor="#D7FAF3" BorderStyle="Notset"> 
              <asp:Label ID="LabelMois1" runat="server" Height="22px" Width="50px"
                  BackColor="#D7FAF3" BorderColor="#D7FAF3" BorderStyle="Notset" Text=""
                  BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                  Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                  style="margin-top: 5px; margin-left: 0px; margin-bottom: 1px;
                  font-style: normal; text-indent: 2px; text-align:  left; padding-top: 3px;">
              </asp:Label>          
             </asp:TableCell>
             <asp:TableCell BackColor="#D7FAF3" BorderColor="#D7FAF3" BorderStyle="Notset"> 
              <asp:Label ID="LabelMois2" runat="server" Height="22px" Width="270px"
                  BackColor="#D7FAF3" BorderColor="#D7FAF3" BorderStyle="Notset" Text=""
                  BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                  Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                  style="margin-top: 5px; margin-left: 0px; margin-bottom: 1px;
                  font-style: normal; text-indent: 2px; text-align:  left; padding-top: 3px;">
              </asp:Label>        
             </asp:TableCell>
             <asp:TableCell BackColor="#D7FAF3" BorderColor="#D7FAF3" BorderStyle="Notset"> 
              <asp:Label ID="LabelMoisJanvier" runat="server" Height="23px" Width="30px" 
                  BackColor="#A8BBB8" BorderColor="#D7FAF3" BorderStyle="NotSet" Text="Janv"
                  BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                  Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "X-Small"
                  style="margin-top: 3px; margin-left: 0px; margin-bottom: 1px;
                  font-style: normal; text-indent: 0px;  text-align:  center; padding-top: 5px;">
              </asp:Label>          
             </asp:TableCell>
             <asp:TableCell BackColor="#D7FAF3" BorderColor="#D7FAF3" BorderStyle="Notset"> 
              <asp:Label ID="LabelMoisFevrier" runat="server" Height="23px" Width="30px"
                  BackColor="#A8BBB8" BorderColor="#D7FAF3" BorderStyle="NotSet" Text="Févr"
                  BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                  Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "X-Small"
                  style="margin-top: 3px; margin-left: 0px; margin-bottom: 1px;
                  font-style: normal; text-indent: 0px; text-align:  center; padding-top: 5px;">
              </asp:Label>          
             </asp:TableCell>
             <asp:TableCell BackColor="#D7FAF3" BorderColor="#D7FAF3" BorderStyle="Notset"> 
              <asp:Label ID="LabelMoisMars" runat="server" Height="23px" Width="30px"
                  BackColor="#A8BBB8" BorderColor="#D7FAF3" BorderStyle="NotSet" Text="Mars"
                  BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                  Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "X-Small"
                  style="margin-top: 3px; margin-left: 0px; margin-bottom: 1px;
                  font-style: normal; text-indent: 0px; text-align:  center; padding-top: 5px;">
              </asp:Label>          
             </asp:TableCell>
             <asp:TableCell BackColor="#D7FAF3" BorderColor="#D7FAF3" BorderStyle="Notset"> 
              <asp:Label ID="LabelMoisAvril" runat="server" Height="23px" Width="30px"
                  BackColor="#A8BBB8" BorderColor="#D7FAF3" BorderStyle="NotSet" Text="Avr"
                  BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                  Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "X-Small"
                  style="margin-top: 3px; margin-left: 0px; margin-bottom: 1px;
                  font-style: normal; text-indent: 0px; text-align:  center; padding-top: 5px;">
              </asp:Label>          
             </asp:TableCell>
             <asp:TableCell BackColor="#D7FAF3" BorderColor="#D7FAF3" BorderStyle="Notset"> 
              <asp:Label ID="LabelMoisMai" runat="server" Height="23px" Width="30px"
                  BackColor="#A8BBB8" BorderColor="#D7FAF3" BorderStyle="NotSet" Text="Mai"
                  BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                  Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "X-Small"
                  style="margin-top: 3px; margin-left: 0px; margin-bottom: 1px;
                  font-style: normal; text-indent: 0px; text-align:  center; padding-top: 5px;">
              </asp:Label>          
             </asp:TableCell>
             <asp:TableCell BackColor="#D7FAF3" BorderColor="#D7FAF3" BorderStyle="Notset"> 
              <asp:Label ID="LabelMoisJuin" runat="server" Height="23px" Width="30px"
                  BackColor="#A8BBB8" BorderColor="#D7FAF3" BorderStyle="NotSet" Text="Juin"
                  BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                  Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "X-Small"
                  style="margin-top: 3px; margin-left: 0px; margin-bottom: 1px;
                  font-style: normal; text-indent: 0px; text-align:  center; padding-top: 5px;">
              </asp:Label>          
             </asp:TableCell>
             <asp:TableCell BackColor="#D7FAF3" BorderColor="#D7FAF3" BorderStyle="Notset"> 
              <asp:Label ID="LabelMoisJuillet" runat="server" Height="23px" Width="30px"
                  BackColor="#A8BBB8" BorderColor="#D7FAF3" BorderStyle="NotSet" Text="Juil"
                  BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                  Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "X-Small"
                  style="margin-top: 3px; margin-left: 0px; margin-bottom: 1px;
                  font-style: normal; text-indent: 0px; text-align:  center; padding-top: 5px;">
              </asp:Label>          
             </asp:TableCell>
             <asp:TableCell BackColor="#D7FAF3" BorderColor="#D7FAF3" BorderStyle="Notset"> 
              <asp:Label ID="LabelMoisAout" runat="server" Height="23px" Width="30px"
                  BackColor="#A8BBB8" BorderColor="#D7FAF3" BorderStyle="NotSet" Text="Août"
                  BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                  Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "X-Small"
                  style="margin-top: 3px; margin-left: 0px; margin-bottom: 1px;
                  font-style: normal; text-indent: 0px; text-align:  center; padding-top: 5px;">
              </asp:Label>          
             </asp:TableCell>
             <asp:TableCell BackColor="#D7FAF3" BorderColor="#D7FAF3" BorderStyle="Notset"> 
              <asp:Label ID="LabelMoisSeptembre" runat="server" Height="23px" Width="30px"
                  BackColor="#A8BBB8" BorderColor="#D7FAF3" BorderStyle="NotSet" Text="Sept"
                  BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                  Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "X-Small"
                  style="margin-top: 3px; margin-left: 0px; margin-bottom: 1px;
                  font-style: normal; text-indent: 0px; text-align:  center; padding-top: 5px;">
              </asp:Label>          
             </asp:TableCell>
             <asp:TableCell BackColor="#D7FAF3" BorderColor="#D7FAF3" BorderStyle="Notset"> 
              <asp:Label ID="LabelMoisOctobre" runat="server" Height="23px" Width="30px"
                  BackColor="#A8BBB8" BorderColor="#D7FAF3" BorderStyle="NotSet" Text="Oct"
                  BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                  Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "X-Small"
                  style="margin-top: 3px; margin-left: 0px; margin-bottom: 1px;
                  font-style: normal; text-indent: 0px; text-align:  center; padding-top: 5px;">
              </asp:Label>          
             </asp:TableCell>
             <asp:TableCell BackColor="#D7FAF3" BorderColor="#D7FAF3" BorderStyle="Notset"> 
              <asp:Label ID="LabelMoisNovembre" runat="server" Height="23px" Width="30px"
                  BackColor="#A8BBB8" BorderColor="#D7FAF3" BorderStyle="NotSet" Text="Nov"
                  BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                  Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "X-Small"
                  style="margin-top: 3px; margin-left: 0px; margin-bottom: 1px;
                  font-style: normal; text-indent: 0px; text-align:  center; padding-top: 5px;">
              </asp:Label>          
             </asp:TableCell>
             <asp:TableCell BackColor="#D7FAF3" BorderColor="#D7FAF3" BorderStyle="Notset"> 
              <asp:Label ID="LabelMoisDecembre" runat="server" Height="23px" Width="30px"
                  BackColor="#A8BBB8" BorderColor="#D7FAF3" BorderStyle="NotSet" Text="Déc"
                  BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                  Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "X-Small"
                  style="margin-top: 3px; margin-left: 0px; margin-bottom: 1px;
                  font-style: normal; text-indent: 0px; text-align:  center; padding-top: 5px;">
              </asp:Label>          
             </asp:TableCell>
             <asp:TableCell BackColor="#D7FAF3" BorderColor="#D7FAF3" BorderStyle="Notset"> 
              <asp:Label ID="LabelMoisAnnee" runat="server" Height="23px" Width="36px"
                  BackColor="#A8BBB8" BorderColor="#D7FAF3" BorderStyle="NotSet" Text="Année"
                  BorderWidth="1px" ForeColor="#A8BBB8" Font-Italic="False"
                  Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "X-Small"
                  style="margin-top: 3px; margin-left: 0px; margin-bottom: 1px;
                  font-style: normal; text-indent: 0px; text-align:  center; padding-top: 5px;">
              </asp:Label>          
             </asp:TableCell>      
          </asp:TableRow>
          <asp:TableRow Width="748px">
            <asp:TableCell BackColor="#CAEBE4" BorderColor="#CAEBE4" BorderStyle="Notset"> 
              <asp:Label ID="LabelTotaux1" runat="server" Height="22px" Width="50px"
                  BackColor="#CAEBE4" BorderColor="#CAEBE4" BorderStyle="Notset" Text=""
                  BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                  Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                  style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                  font-style: normal; text-indent: 2px; text-align:  left; padding-top: 3px;">
              </asp:Label>          
            </asp:TableCell>
            <asp:TableCell BackColor="#CAEBE4" BorderColor="#CAEBE4" BorderStyle="Notset"> 
              <asp:Label ID="LabelTotaux2" runat="server" Height="22px" Width="270px"
                  BackColor="#CAEBE4" BorderColor="#CAEBE4" BorderStyle="Notset" Text="Totaux"
                  BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                  Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                  style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                  font-style: normal; text-indent: 5px; text-align: right; padding-top: 3px;">
              </asp:Label>        
            </asp:TableCell>
            <asp:TableCell RowSpan="1" BackColor="#CAEBE4" BorderColor="#CAEBE4" BorderStyle="Notset"> 
              <asp:TextBox ID="DonTotalM01" runat="server" Height="14px" Width="24px" MaxLength="0"
                  BackColor="LightGray" BorderColor="#B0E0D7" BorderStyle="Inset" Text=""
                  BorderWidth="1px" ForeColor="Black" Visible="true" AutoPostBack="true"
                  Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                  style="margin-top: 1px; margin-left: 2px; text-indent: 1px; text-align: center" TextMode="SingleLine"> 
              </asp:TextBox>
            </asp:TableCell>
            <asp:TableCell RowSpan="1" BackColor="#CAEBE4" BorderColor="#CAEBE4" BorderStyle="Notset"> 
              <asp:TextBox ID="DonTotalM02" runat="server" Height="14px" Width="24px" MaxLength="0"
                  BackColor="LightGray" BorderColor="#B0E0D7" BorderStyle="Inset" Text=""
                  BorderWidth="1px" ForeColor="Black" Visible="true" AutoPostBack="true"
                  Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                  style="margin-top: 1px; margin-left: 2px; text-indent: 1px; text-align: center" TextMode="SingleLine"> 
              </asp:TextBox> 
            </asp:TableCell>
            <asp:TableCell RowSpan="1" BackColor="#CAEBE4" BorderColor="#CAEBE4" BorderStyle="Notset"> 
              <asp:TextBox ID="DonTotalM03" runat="server" Height="14px" Width="24px" MaxLength="0"
                  BackColor="White" BorderColor="#B0E0D7" BorderStyle="Inset" Text=""
                  BorderWidth="1px" ForeColor="Black" Visible="true" AutoPostBack="true"
                  Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                  style="margin-top: 1px; margin-left: 2px; text-indent: 1px; text-align: center" TextMode="SingleLine"> 
              </asp:TextBox>         
            </asp:TableCell>
            <asp:TableCell RowSpan="1" BackColor="#CAEBE4" BorderColor="#CAEBE4" BorderStyle="Notset"> 
              <asp:TextBox ID="DonTotalM04" runat="server" Height="14px" Width="24px" MaxLength="0"
                  BackColor="White" BorderColor="#B0E0D7" BorderStyle="Inset" Text=""
                  BorderWidth="1px" ForeColor="Black" Visible="true" AutoPostBack="true"
                  Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                  style="margin-top: 1px; margin-left: 2px; text-indent: 1px; text-align: center" TextMode="SingleLine"> 
              </asp:TextBox>           
            </asp:TableCell>
            <asp:TableCell RowSpan="1" BackColor="#CAEBE4" BorderColor="#CAEBE4" BorderStyle="Notset"> 
              <asp:TextBox ID="DonTotalM05" runat="server" Height="14px" Width="24px" MaxLength="0"
                  BackColor="White" BorderColor="#B0E0D7" BorderStyle="Inset" Text=""
                  BorderWidth="1px" ForeColor="Black" Visible="true" AutoPostBack="true"
                  Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                  style="margin-top: 1px; margin-left: 2px; text-indent: 1px; text-align: center" TextMode="SingleLine"> 
              </asp:TextBox>           
            </asp:TableCell>
            <asp:TableCell RowSpan="1" BackColor="#CAEBE4" BorderColor="#CAEBE4" BorderStyle="Notset"> 
              <asp:TextBox ID="DonTotalM06" runat="server" Height="14px" Width="24px" MaxLength="0"
                  BackColor="White" BorderColor="#B0E0D7" BorderStyle="Inset" Text=""
                  BorderWidth="1px" ForeColor="Black" Visible="true" AutoPostBack="true"
                  Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                  style="margin-top: 1px; margin-left: 2px; text-indent: 1px; text-align: center" TextMode="SingleLine"> 
              </asp:TextBox>        
            </asp:TableCell>
            <asp:TableCell RowSpan="1" BackColor="#CAEBE4" BorderColor="#CAEBE4" BorderStyle="Notset"> 
              <asp:TextBox ID="DonTotalM07" runat="server" Height="14px" Width="24px" MaxLength="0"
                  BackColor="White" BorderColor="#B0E0D7" BorderStyle="Inset" Text=""
                  BorderWidth="1px" ForeColor="Black" Visible="true" AutoPostBack="true"
                  Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                  style="margin-top: 1px; margin-left: 2px; text-indent: 1px; text-align: center" TextMode="SingleLine"> 
              </asp:TextBox>           
            </asp:TableCell>
            <asp:TableCell RowSpan="1" BackColor="#CAEBE4" BorderColor="#CAEBE4" BorderStyle="Notset"> 
              <asp:TextBox ID="DonTotalM08" runat="server" Height="14px" Width="24px" MaxLength="0"
                  BackColor="White" BorderColor="#B0E0D7" BorderStyle="Inset" Text=""
                  BorderWidth="1px" ForeColor="Black" Visible="true" AutoPostBack="true"
                  Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                  style="margin-top: 1px; margin-left: 2px; text-indent: 1px; text-align: center" TextMode="SingleLine"> 
              </asp:TextBox>           
            </asp:TableCell>
            <asp:TableCell RowSpan="1" BackColor="#CAEBE4" BorderColor="#CAEBE4" BorderStyle="Notset"> 
              <asp:TextBox ID="DonTotalM09" runat="server" Height="14px" Width="24px" MaxLength="0"
                  BackColor="White" BorderColor="#B0E0D7" BorderStyle="Inset" Text=""
                  BorderWidth="1px" ForeColor="Black" Visible="true" AutoPostBack="true"
                  Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                  style="margin-top: 1px; margin-left: 2px; text-indent: 1px; text-align: center" TextMode="SingleLine"> 
              </asp:TextBox>           
            </asp:TableCell>
            <asp:TableCell RowSpan="1" BackColor="#CAEBE4" BorderColor="#CAEBE4" BorderStyle="Notset"> 
              <asp:TextBox ID="DonTotalM10" runat="server" Height="14px" Width="24px" MaxLength="0"
                  BackColor="White" BorderColor="#B0E0D7" BorderStyle="Inset" Text=""
                  BorderWidth="1px" ForeColor="Black" Visible="true" AutoPostBack="true"
                  Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                  style="margin-top: 1px; margin-left: 2px; text-indent: 1px; text-align: center" TextMode="SingleLine"> 
              </asp:TextBox>           
            </asp:TableCell>
            <asp:TableCell RowSpan="1" BackColor="#CAEBE4" BorderColor="#CAEBE4" BorderStyle="Notset"> 
              <asp:TextBox ID="DonTotalM11" runat="server" Height="14px" Width="24px" MaxLength="0"
                  BackColor="White" BorderColor="#B0E0D7" BorderStyle="Inset" Text=""
                  BorderWidth="1px" ForeColor="Black" Visible="true" AutoPostBack="true"
                  Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                  style="margin-top: 1px; margin-left: 2px; text-indent: 1px; text-align: center" TextMode="SingleLine"> 
              </asp:TextBox>           
            </asp:TableCell>
            <asp:TableCell RowSpan="1" BackColor="#CAEBE4" BorderColor="#CAEBE4" BorderStyle="Notset"> 
              <asp:TextBox ID="DonTotalM12" runat="server" Height="14px" Width="24px" MaxLength="0"
                  BackColor="White" BorderColor="#B0E0D7" BorderStyle="Inset" Text=""
                  BorderWidth="1px" ForeColor="Black" Visible="true" AutoPostBack="true"
                  Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                  style="margin-top: 1px; margin-left: 2px; text-indent: 1px; text-align: center" TextMode="SingleLine"> 
              </asp:TextBox>           
            </asp:TableCell>
            <asp:TableCell RowSpan="1" BackColor="#CAEBE4" BorderColor="#CAEBE4" BorderStyle="Notset">
              <asp:Label ID="LabelTotalAnnee" runat="server" Height="23px" Width="36px"
                  BackColor="#CAEBE4" BorderColor="#CAEBE4" BorderStyle="NotSet" Text=""
                  BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                  Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "X-Small"
                  style="margin-top: 3px; margin-left: 0px; margin-bottom: 1px;
                  font-style: normal; text-indent: 0px; text-align:  center; padding-top: 5px;">
              </asp:Label>          
            </asp:TableCell>      
          </asp:TableRow>
          <asp:TableRow Width="748px">
            <asp:TableCell BackColor="#B0E0D7" BorderColor="#B0E0D7" BorderStyle="Notset"> 
              <asp:Label ID="LabelValidation1" runat="server" Height="22px" Width="50px"
                  BackColor="#B0E0D7" BorderColor="#B0E0D7" BorderStyle="Notset" Text=""
                  BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                  Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                  style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                  font-style: normal; text-indent: 2px; text-align:  left; padding-top: 3px;">
              </asp:Label>          
            </asp:TableCell>
            <asp:TableCell BackColor="#B0E0D7" BorderColor="#B0E0D7" BorderStyle="Notset"> 
              <asp:Label ID="LabelValidation2" runat="server" Height="22px" Width="270px"
                  BackColor="#B0E0D7" BorderColor="#B0E0D7" BorderStyle="Notset" Text="Validation"
                  BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                  Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                  style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                  font-style: normal; text-indent: 5px; text-align: right; padding-top: 3px;">
              </asp:Label>        
            </asp:TableCell>
            <asp:TableCell RowSpan="1" BackColor="#B0E0D7" BorderColor="#B0E0D7" BorderStyle="Notset"> 
              <asp:TextBox ID="DonValidM01" runat="server" Height="14px" Width="24px" MaxLength="0"
                  BackColor="LightGray" BorderColor="#B0E0D7" BorderStyle="Inset" Text=""
                  BorderWidth="1px" ForeColor="Black" Visible="true" AutoPostBack="true"
                  Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                  style="margin-top: 1px; margin-left: 2px; text-indent: 1px; text-align: center" TextMode="SingleLine"> 
              </asp:TextBox>
            </asp:TableCell>
            <asp:TableCell RowSpan="1" BackColor="#B0E0D7" BorderColor="#B0E0D7" BorderStyle="Notset"> 
              <asp:TextBox ID="DonValidM02" runat="server" Height="14px" Width="24px" MaxLength="0"
                  BackColor="LightGray" BorderColor="#B0E0D7" BorderStyle="Inset" Text=""
                  BorderWidth="1px" ForeColor="Black" Visible="true" AutoPostBack="true"
                  Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                  style="margin-top: 1px; margin-left: 2px; text-indent: 1px; text-align: center" TextMode="SingleLine"> 
              </asp:TextBox> 
            </asp:TableCell>
            <asp:TableCell RowSpan="1" BackColor="#B0E0D7" BorderColor="#B0E0D7" BorderStyle="Notset"> 
              <asp:TextBox ID="DonValidM03" runat="server" Height="14px" Width="24px" MaxLength="0"
                  BackColor="White" BorderColor="#B0E0D7" BorderStyle="Inset" Text=""
                  BorderWidth="1px" ForeColor="Black" Visible="true" AutoPostBack="true"
                  Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                  style="margin-top: 1px; margin-left: 2px; text-indent: 1px; text-align: center" TextMode="SingleLine"> 
              </asp:TextBox>         
            </asp:TableCell>
            <asp:TableCell RowSpan="1" BackColor="#B0E0D7" BorderColor="#B0E0D7" BorderStyle="Notset"> 
              <asp:TextBox ID="DonValidM04" runat="server" Height="14px" Width="24px" MaxLength="0"
                  BackColor="White" BorderColor="#B0E0D7" BorderStyle="Inset" Text=""
                  BorderWidth="1px" ForeColor="Black" Visible="true" AutoPostBack="true"
                  Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                  style="margin-top: 1px; margin-left: 2px; text-indent: 1px; text-align: center" TextMode="SingleLine"> 
              </asp:TextBox>           
            </asp:TableCell>
            <asp:TableCell RowSpan="1" BackColor="#B0E0D7" BorderColor="#B0E0D7" BorderStyle="Notset"> 
              <asp:TextBox ID="DonValidM05" runat="server" Height="14px" Width="24px" MaxLength="0"
                  BackColor="White" BorderColor="#B0E0D7" BorderStyle="Inset" Text=""
                  BorderWidth="1px" ForeColor="Black" Visible="true" AutoPostBack="true"
                  Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                  style="margin-top: 1px; margin-left: 2px; text-indent: 1px; text-align: center" TextMode="SingleLine"> 
              </asp:TextBox>           
            </asp:TableCell>
            <asp:TableCell RowSpan="1" BackColor="#B0E0D7" BorderColor="#B0E0D7" BorderStyle="Notset"> 
              <asp:TextBox ID="DonValidM06" runat="server" Height="14px" Width="24px" MaxLength="0"
                  BackColor="White" BorderColor="#B0E0D7" BorderStyle="Inset" Text=""
                  BorderWidth="1px" ForeColor="Black" Visible="true" AutoPostBack="true"
                  Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                  style="margin-top: 1px; margin-left: 2px; text-indent: 1px; text-align: center" TextMode="SingleLine"> 
              </asp:TextBox>        
            </asp:TableCell>
            <asp:TableCell RowSpan="1" BackColor="#B0E0D7" BorderColor="#B0E0D7" BorderStyle="Notset"> 
              <asp:TextBox ID="DonValidM07" runat="server" Height="14px" Width="24px" MaxLength="0"
                  BackColor="White" BorderColor="#B0E0D7" BorderStyle="Inset" Text=""
                  BorderWidth="1px" ForeColor="Black" Visible="true" AutoPostBack="true"
                  Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                  style="margin-top: 1px; margin-left: 2px; text-indent: 1px; text-align: center" TextMode="SingleLine"> 
              </asp:TextBox>           
            </asp:TableCell>
            <asp:TableCell RowSpan="1" BackColor="#B0E0D7" BorderColor="#B0E0D7" BorderStyle="Notset"> 
              <asp:TextBox ID="DonValidM08" runat="server" Height="14px" Width="24px" MaxLength="0"
                  BackColor="White" BorderColor="#B0E0D7" BorderStyle="Inset" Text=""
                  BorderWidth="1px" ForeColor="Black" Visible="true" AutoPostBack="true"
                  Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                  style="margin-top: 1px; margin-left: 2px; text-indent: 1px; text-align: center" TextMode="SingleLine"> 
              </asp:TextBox>           
            </asp:TableCell>
            <asp:TableCell RowSpan="1" BackColor="#B0E0D7" BorderColor="#B0E0D7" BorderStyle="Notset"> 
              <asp:TextBox ID="DonValidM09" runat="server" Height="14px" Width="24px" MaxLength="0"
                  BackColor="White" BorderColor="#B0E0D7" BorderStyle="Inset" Text=""
                  BorderWidth="1px" ForeColor="Black" Visible="true" AutoPostBack="true"
                  Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                  style="margin-top: 1px; margin-left: 2px; text-indent: 1px; text-align: center" TextMode="SingleLine"> 
              </asp:TextBox>           
            </asp:TableCell>
            <asp:TableCell RowSpan="1" BackColor="#B0E0D7" BorderColor="#B0E0D7" BorderStyle="Notset"> 
              <asp:TextBox ID="DonValidM10" runat="server" Height="14px" Width="24px" MaxLength="0"
                  BackColor="White" BorderColor="#B0E0D7" BorderStyle="Inset" Text=""
                  BorderWidth="1px" ForeColor="Black" Visible="true" AutoPostBack="true"
                  Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                  style="margin-top: 1px; margin-left: 2px; text-indent: 1px; text-align: center" TextMode="SingleLine"> 
              </asp:TextBox>           
            </asp:TableCell>
            <asp:TableCell RowSpan="1" BackColor="#B0E0D7" BorderColor="#B0E0D7" BorderStyle="Notset"> 
              <asp:TextBox ID="DonValidM11" runat="server" Height="14px" Width="24px" MaxLength="0"
                  BackColor="White" BorderColor="#B0E0D7" BorderStyle="Inset" Text=""
                  BorderWidth="1px" ForeColor="Black" Visible="true" AutoPostBack="true"
                  Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                  style="margin-top: 1px; margin-left: 2px; text-indent: 1px; text-align: center" TextMode="SingleLine"> 
              </asp:TextBox>           
            </asp:TableCell>
            <asp:TableCell RowSpan="1" BackColor="#B0E0D7" BorderColor="#B0E0D7" BorderStyle="Notset"> 
              <asp:TextBox ID="DonValidM12" runat="server" Height="14px" Width="24px" MaxLength="0"
                  BackColor="White" BorderColor="#B0E0D7" BorderStyle="Inset" Text=""
                  BorderWidth="1px" ForeColor="Black" Visible="true" AutoPostBack="true"
                  Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                  style="margin-top: 1px; margin-left: 2px; text-indent: 1px; text-align: center" TextMode="SingleLine"> 
              </asp:TextBox>           
            </asp:TableCell>
            <asp:TableCell RowSpan="1" BackColor="#B0E0D7" BorderColor="#B0E0D7" BorderStyle="Notset">
              <asp:Label ID="LabelValidAnnee" runat="server" Height="23px" Width="36px"
                  BackColor="#B0E0D7" BorderColor="#B0E0D7" BorderStyle="NotSet" Text=""
                  BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                  Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "X-Small"
                  style="margin-top: 3px; margin-left: 0px; margin-bottom: 1px;
                  font-style: normal; text-indent: 0px; text-align:  center; padding-top: 5px;">
              </asp:Label>         
            </asp:TableCell>      
          </asp:TableRow>  
        </asp:Table>
      </asp:TableCell>
    </asp:TableRow>
 </asp:Table>