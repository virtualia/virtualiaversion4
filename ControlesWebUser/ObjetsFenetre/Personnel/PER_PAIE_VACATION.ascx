﻿<%@ Control Language="VB" AutoEventWireup="false" Inherits="Virtualia.Net.Fenetre_PER_PAIE_VACATION" CodeBehind="PER_PAIE_VACATION.ascx.vb" %>

<%@ Register Src="~/Controles/Saisies/VCoupleEtiDonnee.ascx" TagName="VCoupleEtiDonnee" TagPrefix="Virtualia" %>
<%@ Register Src="~/Controles/Saisies/VDuoEtiquetteCommande.ascx" TagName="VDuoEtiquetteCommande" TagPrefix="Virtualia" %>
<%@ Register Src="~/Controles/Saisies/VCoupleVerticalEtiDonnee.ascx" TagName="VCoupleVerticalEtiDonnee" TagPrefix="Virtualia" %>
<%@ Register Src="~/Controles/Saisies/VCocheSimple.ascx" TagName="VCocheSimple" TagPrefix="Virtualia" %>

<%@ Register Src="~/ControlsGeneriques/V_DataGridListeFiltre.ascx" TagName="DataGridFiltreListe" TagPrefix="Generic" %>
<%@ Register Src="~/ControlsGeneriques/V_CommandeCRUD.ascx" TagName="VCrud" TagPrefix="Generic" %>

<asp:Table ID="CadreControle" runat="server" Height="30px" Width="720px" CellPadding="0" CellSpacing="0" HorizontalAlign="Center">
    <asp:TableRow>
        <asp:TableCell HorizontalAlign="Center" VerticalAlign="Middle">
            <Generic:DataGridFiltreListe ID="ListeGrille" runat="server" CadreWidth="700px" SiColonneSelect="true" />
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="CadreInfo" runat="server" BorderStyle="Ridge" BorderWidth="2px" BorderColor="#B0E0D7" Height="530px" Width="720px" HorizontalAlign="Center" Style="margin-top: 12px;">
                <asp:TableRow>
                    <asp:TableCell>
                        <Generic:VCrud ID="CtlCrud" runat="server" Height="22px" Width="244px" HorizontalAlign="Right" CadreStyle="margin-top: 3px; margin-right: 3px" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Table ID="CadreTitre" runat="server" Height="40px" CellPadding="0" Width="720px" CellSpacing="0" HorizontalAlign="Center">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                    <asp:Label ID="Etiquette" runat="server" Text="Paiement des vacations" Height="20px" Width="260px" BackColor="#2FA49B" BorderColor="#B0E0D7" BorderStyle="Groove" BorderWidth="2px" ForeColor="#D7FAF3" Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Small" Style="margin-top: 5px; margin-left: 4px; margin-bottom: 20px; font-style: oblique; text-indent: 5px; text-align: center;" />
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Table ID="CadreDates" runat="server" CellPadding="0" CellSpacing="0" Width="720px">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH00" runat="server" V_PointdeVue="1" V_Objet="84" V_Information="0" V_SiDonneeDico="true" EtiWidth="110px" DonWidth="80px" DonTabIndex="1" />
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Left">
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH02" runat="server" V_PointdeVue="1" V_Objet="84" V_Information="2" V_SiDonneeDico="true" EtiWidth="150px" DonWidth="80px" DonTabIndex="2" />
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Left">
                                    <Virtualia:VDuoEtiquetteCommande ID="Dontab03" runat="server" V_PointdeVue="1" V_Objet="84" V_Information="3" V_SiDonneeDico="true" EtiWidth="130px" DonWidth="100px" DonTabIndex="3" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Height="15px" ColumnSpan="3" />
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Table ID="TableauPaiement" runat="server" CellPadding="0" CellSpacing="0" Width="500px">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH11" runat="server" V_PointdeVue="1" V_Objet="84" V_Information="11" V_SiDonneeDico="true" EtiWidth="160px" DonWidth="80px" DonTabIndex="4" />
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Left">
                                    <Virtualia:VCocheSimple ID="Coche12" runat="server" V_PointdeVue="1" V_Objet="84" V_Information="12" V_SiDonneeDico="true" V_Width="130px" />
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Left">
                                    <Virtualia:VCocheSimple ID="Coche13" runat="server" V_PointdeVue="1" V_Objet="84" V_Information="13" V_SiDonneeDico="true" V_Width="100px" V_Style="margin-left:30px" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Height="15px" ColumnSpan="3" />
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Table ID="TableauAnalytique" runat="server" CellPadding="0" CellSpacing="0" Width="720px">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH01" runat="server" V_PointdeVue="1" V_Objet="84" V_Information="1" V_SiDonneeDico="true" EtiWidth="230px" DonWidth="80px" DonTabIndex="5" EtiStyle="margin-left:30px" />
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Table ID="TableauEnseignement" runat="server" CellPadding="0" CellSpacing="0" Width="520px">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH04" runat="server" V_PointdeVue="1" V_Objet="84" V_Information="4" V_SiDonneeDico="true" EtiWidth="60px" DonWidth="80px" DonTabIndex="6" Etitext="Code" EtiStyle="margin-left:30px" />
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Left">
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH05" runat="server" V_PointdeVue="1" V_Objet="84" V_Information="5" V_SiDonneeDico="true" EtiWidth="90px" DonWidth="340px" DonTabIndex="7" />
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Table ID="TableauHED" runat="server" CellPadding="0" CellSpacing="0" Width="500px">
                            <asp:TableRow>
                                <asp:TableCell Height="12px" ColumnSpan="2" />
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH06" runat="server" V_PointdeVue="1" V_Objet="84" V_Information="6" V_SiDonneeDico="true" EtiWidth="280px" DonWidth="70px" DonTabIndex="8" EtiStyle="margin-left:100px" />
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Left">
                                    <asp:Label ID="LabelHeures06" runat="server" Text="heures" Height="20px" Width="35px" BackColor="Transparent" BorderStyle="none" BorderWidth="0px" ForeColor="#2FA49B" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; font-style: oblique; text-indent: 0px; text-align: left;" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH07" runat="server" V_PointdeVue="1" V_Objet="84" V_Information="7" V_SiDonneeDico="true" EtiWidth="280px" DonWidth="70px" DonTabIndex="9" EtiStyle="margin-left:100px" />
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Left">
                                    <asp:Label ID="LabelHeures07" runat="server" Text="heures" Height="20px" Width="35px" BackColor="Transparent" BorderStyle="none" BorderWidth="0px" ForeColor="#2FA49B" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; font-style: oblique; text-indent: 0px; text-align: left;" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH08" runat="server" V_PointdeVue="1" V_Objet="84" V_Information="8" V_SiDonneeDico="true" EtiWidth="280px" DonWidth="70px" DonTabIndex="10" EtiStyle="margin-left:100px" />
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Left">
                                    <asp:Label ID="LabelHeures08" runat="server" Text="heures" Height="20px" Width="35px" BackColor="Transparent" BorderStyle="none" BorderWidth="0px" ForeColor="#2FA49B" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; font-style: oblique; text-indent: 0px; text-align: left;" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Height="5px" ColumnSpan="2" />
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH09" runat="server" V_PointdeVue="1" V_Objet="84" V_Information="9" V_SiDonneeDico="true" EtiWidth="280px" DonWidth="70px" DonTabIndex="11" EtiStyle="margin-left:100px" />
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Left">
                                    <asp:Label ID="LabelHeures09" runat="server" Text="heures" Height="20px" Width="35px" BackColor="Transparent" BorderStyle="none" BorderWidth="0px" ForeColor="#2FA49B" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; font-style: oblique; text-indent: 0px; text-align: left;" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Height="12px" ColumnSpan="2" />
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH10" runat="server" V_PointdeVue="1" V_Objet="84" V_Information="10" V_SiDonneeDico="true" EtiWidth="280px" DonWidth="70px" DonTabIndex="12" EtiStyle="margin-left:100px" />
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Left">
                                    <asp:Label ID="LabelEuro" runat="server" Text="€" Height="20px" Width="15px" BackColor="Transparent" BorderStyle="none" BorderWidth="0px" ForeColor="#2FA49B" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; font-style: oblique; text-indent: 0px; text-align: left;" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Height="8px" ColumnSpan="2" />
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Table ID="TableauObservations" runat="server" CellPadding="0" CellSpacing="0" Width="720px">
                            <asp:TableRow>
                                <asp:TableCell Height="12px" />
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                    <Virtualia:VCoupleVerticalEtiDonnee ID="InfoV14" runat="server" DonTextMode="true" V_PointdeVue="1" V_Objet="84" V_Information="14" V_SiDonneeDico="true" EtiWidth="687px" DonWidth="685px" DonHeight="80px" DonTabIndex="13" EtiStyle="text-align:center" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Height="12px" />
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>
</asp:Table>
