﻿Option Strict On
Option Explicit On
Option Compare Text

Imports VI = Virtualia.Systeme.Constantes
Imports Virtualia.ObjetBase
Imports Virtualia.TablesObjet.ShemaPER
Imports Virtualia.OutilsVisu

Partial Class Fenetre_PER_ETATCIVIL
    Inherits ObjetWebControleBase(Of PER_ETATCIVIL)
    Implements IControlOK

#Region "IControlOK"
    Public ReadOnly Property CommandOK As ICtlCommandeOK Implements IControlOK.CtlOK
        Get
            Return Me.CtlOK
        End Get
    End Property
#End Region

#Region "ObjetWebControleBase"
    Private _webfonction As WebFonctions
    Protected Overrides ReadOnly Property V_WebFonction As IWebFonctions
        Get
            If (_webfonction Is Nothing) Then
                _webfonction = New WebFonctions(Me)
            End If
            Return _webfonction
        End Get
    End Property

    Protected Overrides Function InitCadreInfo() As WebControl
        Return Me.CadreInfo
    End Function

    Protected Overrides Sub InitCacheIde()
        V_CacheIde.TypeVue = WebControlTypeVue.Simple
    End Sub

    Public Sub New()
        MyBase.New()

        _SiPrincipal = True

    End Sub

    Protected Overrides Sub OnInit(e As System.EventArgs)
        MyBase.OnInit(e)

        Dim initwebfonction As IWebFonctions = V_WebFonction
    End Sub
#End Region

#Region "Specifique"
    Protected Overrides Sub OnPreRender(e As EventArgs)

        InitStyleCadre()
        InitStyleCadre(Me.CadreExpert, _CadreExpertStyle)

        ChargeDicoControlSaisie()

        LireLaFiche()
        InitAutoPostBackControlSaisie()

        'If V_CacheIde.EstNothing Or V_CacheIde.Ide_Dossier <= 0 Then
        '    AjouteNouveauDossier(VI.PointdeVue.PVueApplicatif)
        'End If

        CadreInfo.BackColor = _webfonction.CouleurCharte(V_CacheIde.PointDeVue, "Cadre")
        CadreInfo.BorderColor = _webfonction.CouleurCharte(V_CacheIde.PointDeVue, "Bordure")
        Etiquette.BackColor = _webfonction.CouleurCharte(V_CacheIde.PointDeVue, "Titre")
        Etiquette.ForeColor = _webfonction.CouleurCharte(V_CacheIde.PointDeVue, "Police_Claire")
        Etiquette.BorderColor = _webfonction.CouleurCharte(V_CacheIde.PointDeVue, "Bordure")
    End Sub

    Protected Sub InfoH11_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles InfoH11.Load
        If _webfonction.PointeurRegistre Is Nothing Then
            Exit Sub
        End If
        Dim LgAlpha As String = _webfonction.PointeurRegistre.Valeur("Fonctionnel", "Saisie", "Informations_Personnelles", "Longueur_NIR")
        If IsNumeric(LgAlpha) Then
            InfoH11.DonMaxLength = CInt(LgAlpha)
        End If
    End Sub

    Private Function SiErreurSpecifique() As List(Of String)
        If V_CacheIde Is Nothing Then
            Return Nothing
        End If

        Dim TabErreurs As List(Of String) = New List(Of String)
        Dim CacheDoublon As ArrayList

        Dim Dossier As Virtualia.Ressources.Datas.ObjetDossierPER

        If V_CacheIde.Ide_Dossier > 0 Then
            Dossier = _webfonction.PointeurDossier(V_CacheIde.Ide_Dossier)
        Else
            Dossier = Nothing
        End If

        If V_CacheMaj(2).valeur = "" Or V_CacheMaj(3).valeur = "" Or V_CacheMaj(4).valeur = "" Then
            TabErreurs.Add("Le Nom, le prénom et la date de naissance sont obligatoires.")
            Return TabErreurs
        End If

        '** Mise en Forme Nom et Prénom
        If _webfonction.PointeurRegistre IsNot Nothing Then
            If _webfonction.PointeurRegistre.Valeur("Fonctionnel", "Saisie", "Informations_Personnelles", "Nom_MAJUSCULE") = "Oui" Then
                V_CacheMaj(2).valeur = V_CacheMaj(2).valeur.ToUpper()
            Else
                V_CacheMaj(2).valeur = _webfonction.ViRhFonction.Lettre1Capi(V_CacheMaj(2).valeur, 2)
            End If
        Else
            V_CacheMaj(2).valeur = _webfonction.ViRhFonction.Lettre1Capi(V_CacheMaj(2).valeur, 2)
        End If
        V_CacheMaj(3).valeur = _webfonction.ViRhFonction.Lettre1Capi(V_CacheMaj(3).valeur, 1)

        '** Contrôle Unicité Nom, Prénom, Date de naissance
        CacheDoublon = _webfonction.PointeurGlobal.ControleDoublon( _
                                                                VI.PointdeVue.PVueApplicatif, _
                                                                V_CacheIde.Ide_Dossier, _
                                                                2, _
                                                                V_CacheMaj(2).valeur, _
                                                                V_CacheMaj(3).valeur, _
                                                                V_CacheMaj(4).valeur _
                                                                )
        If CacheDoublon IsNot Nothing Then
            TabErreurs.Add("Il existe déjà un dossier ayant la même identification sous le N° " & CacheDoublon(0).ToString)
            Return TabErreurs
        End If

        '** Contrôle Unicité N° NIR
        If V_CacheMaj(11).valeur <> "" Then
            CacheDoublon = _webfonction.PointeurGlobal.ControleDoublon( _
                                                                        VI.PointdeVue.PVueApplicatif, _
                                                                        V_CacheIde.Ide_Dossier, _
                                                                        11, _
                                                                        V_CacheMaj(11).valeur)
            If CacheDoublon IsNot Nothing Then
                TabErreurs.Add("Il existe déjà un dossier ayant le même NIR sous le N° " & CacheDoublon(0).ToString)
                Return TabErreurs
            End If
        End If

        '** Contrôle de cohérence
        If V_CacheMaj(1).valeur = "Monsieur" Then
            V_CacheMaj(8).valeur = "Masculin"
        ElseIf V_CacheMaj(1).valeur <> "" Then
            V_CacheMaj(8).valeur = "Féminin"
        End If

        If V_CacheMaj(8).valeur = "Masculin" Then
            V_CacheMaj(1).valeur = "Monsieur"
        ElseIf V_CacheMaj(8).valeur = "Féminin" Then
            Select Case V_CacheMaj(1).valeur
                Case Is = "Madame", "Mademoiselle"
                    Exit Select
                Case Else
                    V_CacheMaj(1).valeur = "Madame"
            End Select
        End If

        If V_CacheIde.IndexFiche < 0 Then
            Return Nothing
        End If

        'WsFiche = DirectCast(Dossier.Item(V_CacheIde.IndexFiche), PER_ETATCIVIL)

        If _WsFiche Is Nothing Then
            Return Nothing
        End If

        If _WsFiche.SiOK_DateNaissance(V_CacheMaj(4).valeur) = False Then
            TabErreurs.Add("Date de naissance erronée.")
        End If

        If _webfonction.PointeurRegistre IsNot Nothing Then
            If V_CacheMaj(11).valeur <> "" And V_CacheMaj(12).valeur <> "" Then
                If _webfonction.PointeurRegistre.Valeur("Fonctionnel", "Saisie", "Informations_Personnelles", "Contrôle_NIR") = "Oui" Then
                    If Not _WsFiche.SiOK_NIR(V_CacheMaj(11).valeur, V_CacheMaj(12).valeur) Then
                        TabErreurs.Add("Numéro d'Identification NIR erronée ou Clef NIR erronée.")
                    End If
                End If
            End If
        Else
            If Not _WsFiche.SiOK_NIR(V_CacheMaj(11).valeur, V_CacheMaj(12).valeur) Then
                TabErreurs.Add("Numéro d'Identification NIR erronée ou Clef NIR erronée.")
            End If
        End If

        If TabErreurs.Count > 0 Then
            Return TabErreurs
        Else
            Return Nothing
        End If

    End Function

    Protected Overrides Sub CommandeOK_Click(ByVal sender As Object, ByVal e As System.EventArgs)

        Dim TableauErreur As List(Of String) = SiErreurSpecifique()

        If Not (TableauErreur Is Nothing) Then

            Dim msgs As ItemMessage = New ItemMessage()

            msgs.TitreMsg = "Contrôle préalable à l'enregistrement du dossier de " & V_CacheMaj(2).valeur & Strings.Space(1) & V_CacheMaj(3).valeur
            msgs.Messages.AddRange(TableauErreur)

            AfficheMessageErreurSaisie(msgs)

            Return
        End If

        MetAJourFiche()

        'Dim urlRelative As String
        'urlRelative = Request.RawUrl

        'If InStr(urlRelative, "NouveauDossier") > 0 Then
        '    Dim CacheData As ArrayList = V_CacheMaj
        '    Dim TabOK As New ArrayList
        '    TabOK.Add("Le dossier de " & CacheData(2).ToString & Strings.Space(1) & CacheData(3).ToString)
        '    TabOK.Add(" a été créé.")
        '    Dim TitreMsg As String = "Nouveau dossier"
        '    Call V_MessageDialog(V_WebFonction.Evt_MessageInformatif(WsNumObjet, TitreMsg, TabOK, V_Identifiant))
        'End If
    End Sub

    Public WriteOnly Property CadreExpertVisible As Boolean
        Set(ByVal value As Boolean)
            CadreExpert.Visible = value
        End Set
    End Property

    Private _CadreExpertStyle As String = ""
    Public WriteOnly Property CadreExpertStyle As String
        Set(ByVal value As String)
            _CadreExpertStyle = value
        End Set
    End Property

    Protected Overrides Sub LireExperte()
        If CadreExpert.Visible Then
            MyBase.LireExperte()
        End If
    End Sub
#End Region

End Class
