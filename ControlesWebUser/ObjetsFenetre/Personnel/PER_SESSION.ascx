﻿<%@ Control Language="VB" AutoEventWireup="false" Inherits="Virtualia.Net.Fenetre_PER_SESSION" CodeBehind="PER_SESSION.ascx.vb" %>

<%@ Register Src="~/Controles/Saisies/VCoupleEtiDonnee.ascx" TagName="VCoupleEtiDonnee" TagPrefix="Virtualia" %>
<%@ Register Src="~/Controles/Saisies/VDuoEtiquetteCommande.ascx" TagName="VDuoEtiquetteCommande" TagPrefix="Virtualia" %>
<%@ Register Src="~/Controles/Saisies/VCocheSimple.ascx" TagName="VCocheSimple" TagPrefix="Virtualia" %>

<%@ Register Src="~/ControlsGeneriques/V_DataGridListeFiltre.ascx" TagName="DataGridFiltreListe" TagPrefix="Generic" %>
<%@ Register Src="~/ControlsGeneriques/V_CommandeCRUD.ascx" TagName="VCrud" TagPrefix="Generic" %>

<asp:Table ID="CadreControle" runat="server" Height="30px" Width="700px" CellPadding="0" CellSpacing="0" HorizontalAlign="Center">
    <asp:TableRow>
        <asp:TableCell>
            <Generic:DataGridFiltreListe ID="ListeGrille" runat="server" CadreWidth="680px" SiColonneSelect="true" SiListeFiltreVisible="false" />
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="CadreInfo" runat="server" BorderStyle="Ridge" BorderWidth="2px" BorderColor="#B0E0D7" Height="1050px" Width="700px" HorizontalAlign="Center" Style="margin-top: 3px;">
                <asp:TableRow>
                    <asp:TableCell>
                        <Generic:VCrud ID="CtlCrud" runat="server" Height="22px" Width="244px" HorizontalAlign="Right" CadreStyle="margin-top: 3px; margin-right: 3px" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Table ID="CadreTitre" runat="server" Height="40px" CellPadding="0" Width="700px" CellSpacing="0" HorizontalAlign="Center">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                    <asp:Label ID="Etiquette" runat="server" Text="Formation continue" Height="20px" Width="300px" BackColor="#2FA49B" BorderColor="#B0E0D7" BorderStyle="Groove" BorderWidth="2px" ForeColor="#D7FAF3" Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Small" Style="margin-top: 5px; margin-left: 4px; margin-bottom: 10px; font-style: oblique; text-indent: 5px; text-align: center;" />
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Table ID="CadreDon" runat="server" CellPadding="0" CellSpacing="0" Width="700px">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH00" runat="server" V_PointdeVue="1" V_Objet="29" V_Information="0" V_SiDonneeDico="true" DonWidth="100px" DonTabIndex="1" />
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Left">
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH11" runat="server" V_PointdeVue="1" V_Objet="29" V_Information="11" V_SiDonneeDico="true" DonWidth="100px" DonTabIndex="2" EtiStyle="margin-left:2px" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH08" runat="server" V_PointdeVue="1" V_Objet="29" V_Information="8" V_SiDonneeDico="true" DonWidth="80px" DonTabIndex="3" />
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Left">
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH10" runat="server" V_PointdeVue="1" V_Objet="29" V_Information="10" V_SiDonneeDico="true" DonWidth="80px" DonTabIndex="4" EtiStyle="margin-left:2px" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left" ColumnSpan="2">
                                    <Virtualia:VDuoEtiquetteCommande ID="Dontab04" runat="server" V_PointdeVue="1" V_Objet="29" V_Information="4" V_SiDonneeDico="true" DonTabIndex="5" DonWidth="480px" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left" ColumnSpan="2">
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH17" runat="server" V_PointdeVue="1" V_Objet="29" V_Information="17" V_SiDonneeDico="true" DonWidth="100px" DonTabIndex="6" EtiText="Référence du stage" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center" ColumnSpan="2">
                                    <Virtualia:VDuoEtiquetteCommande ID="Dontab01" runat="server" V_PointdeVue="1" V_Objet="29" V_Information="1" V_SiDonneeDico="true" DonTabIndex="7" DonWidth="400px" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center" ColumnSpan="2">
                                    <Virtualia:VDuoEtiquetteCommande ID="Dontab02" runat="server" V_PointdeVue="1" V_Objet="29" V_Information="2" V_SiDonneeDico="true" DonTabIndex="8" DonWidth="400px" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center" ColumnSpan="2">
                                    <Virtualia:VDuoEtiquetteCommande ID="Dontab03" runat="server" V_PointdeVue="1" V_Objet="29" V_Information="3" V_SiDonneeDico="true" DonTabIndex="9" DonWidth="400px" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left" ColumnSpan="2">
                                    <Virtualia:VDuoEtiquetteCommande ID="Dontab09" runat="server" V_PointdeVue="1" V_Objet="29" V_Information="9" V_SiDonneeDico="true" DonTabIndex="10" DonWidth="400px" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow Height="40px">
                                <asp:TableCell HorizontalAlign="Center" ColumnSpan="2">
                                    <asp:Label ID="EtiSuivi" runat="server" Height="20px" Width="350px" Text="Déroulement" BackColor="#B0E0D7" BorderColor="#B0E0D7" BorderStyle="Outset" BorderWidth="2px" ForeColor="#142425" Font-Italic="true" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow Height="40px">
                                <asp:TableCell HorizontalAlign="Left">
                                    <Virtualia:VCocheSimple ID="Coche33" runat="server" V_PointdeVue="1" V_Objet="29" V_Information="33" V_SiDonneeDico="true" V_Width="250px" V_Style="margin-left:10px" />
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Left">
                                    <Virtualia:VCocheSimple ID="Coche34" runat="server" V_PointdeVue="1" V_Objet="29" V_Information="34" V_SiDonneeDico="true" V_Width="250px" V_Style="margin-left:10px" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH05" runat="server" V_PointdeVue="1" V_Objet="29" V_Information="5" V_SiDonneeDico="true" EtiWidth="120px" DonWidth="100px" DonTabIndex="11" EtiStyle="margin-left:2px" />
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Left" ColumnSpan="2">
                                    <Virtualia:VDuoEtiquetteCommande ID="Dontab23" runat="server" V_PointdeVue="1" V_Objet="29" V_Information="23" V_SiDonneeDico="true" DonTabIndex="12" DonWidth="150px" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left" ColumnSpan="2">
                                    <Virtualia:VDuoEtiquetteCommande ID="Dontab06" runat="server" V_PointdeVue="1" V_Objet="29" V_Information="6" V_SiDonneeDico="true" DonTabIndex="13" DonWidth="250px" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left" ColumnSpan="2">
                                    <Virtualia:VDuoEtiquetteCommande ID="Dontab18" runat="server" V_PointdeVue="1" V_Objet="29" V_Information="18" V_SiDonneeDico="true" DonTabIndex="14" DonWidth="250px" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left" ColumnSpan="2">
                                    <Virtualia:VDuoEtiquetteCommande ID="Dontab07" runat="server" V_PointdeVue="1" V_Objet="29" V_Information="7" V_SiDonneeDico="true" DonTabIndex="15" DonWidth="350px" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow Height="40px">
                                <asp:TableCell HorizontalAlign="Center" ColumnSpan="2">
                                    <asp:Label ID="EtiCouts" runat="server" Height="20px" Width="350px" Text="Coûts et frais" BackColor="#B0E0D7" BorderColor="#B0E0D7" BorderStyle="Outset" BorderWidth="2px" ForeColor="#142425" Font-Italic="true" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH12" runat="server" V_PointdeVue="1" V_Objet="29" V_Information="12" V_SiDonneeDico="true" DonWidth="120px" DonTabIndex="16" />
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Left">
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH14" runat="server" V_PointdeVue="1" V_Objet="29" V_Information="14" V_SiDonneeDico="true" DonWidth="80px" DonTabIndex="17" DonStyle="margin-left:2px" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH30" runat="server" V_PointdeVue="1" V_Objet="29" V_Information="30" V_SiDonneeDico="true" DonWidth="120px" DonTabIndex="18" />
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Left">
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH16" runat="server" V_PointdeVue="1" V_Objet="29" V_Information="16" V_SiDonneeDico="true" DonWidth="80px" DonTabIndex="19" DonStyle="margin-left:2px" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH15" runat="server" V_PointdeVue="1" V_Objet="29" V_Information="15" V_SiDonneeDico="true" DonWidth="120px" DonTabIndex="20" />
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Left">
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH13" runat="server" V_PointdeVue="1" V_Objet="29" V_Information="13" V_SiDonneeDico="true" DonWidth="80px" DonTabIndex="21" DonStyle="margin-left:2px" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow Height="40px">
                                <asp:TableCell HorizontalAlign="Center" ColumnSpan="2">
                                    <asp:Label ID="EtiComplements" runat="server" Height="20px" Width="350px" Text="Compléments d'information" BackColor="#B0E0D7" BorderColor="#B0E0D7" BorderStyle="Outset" BorderWidth="2px" ForeColor="#142425" Font-Italic="true" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center" ColumnSpan="2">
                                    <Virtualia:VDuoEtiquetteCommande ID="Dontab19" runat="server" V_PointdeVue="1" V_Objet="29" V_Information="19" V_SiDonneeDico="true" DonTabIndex="22" DonWidth="400px" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center" ColumnSpan="2">
                                    <Virtualia:VDuoEtiquetteCommande ID="Dontab20" runat="server" V_PointdeVue="1" V_Objet="29" V_Information="20" V_SiDonneeDico="true" DonTabIndex="23" DonWidth="400px" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center" ColumnSpan="2">
                                    <Virtualia:VDuoEtiquetteCommande ID="Dontab22" runat="server" V_PointdeVue="1" V_Objet="29" V_Information="22" V_SiDonneeDico="true" DonTabIndex="24" DonWidth="400px" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center" ColumnSpan="2">
                                    <Virtualia:VDuoEtiquetteCommande ID="Dontab24" runat="server" V_PointdeVue="1" V_Objet="29" V_Information="24" V_SiDonneeDico="true" DonTabIndex="25" DonWidth="400px" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center" ColumnSpan="2">
                                    <Virtualia:VDuoEtiquetteCommande ID="Dontab25" runat="server" V_PointdeVue="1" V_Objet="29" V_Information="25" V_SiDonneeDico="true" DonTabIndex="26" DonWidth="400px" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center" ColumnSpan="2">
                                    <Virtualia:VDuoEtiquetteCommande ID="Dontab26" runat="server" V_PointdeVue="1" V_Objet="29" V_Information="26" V_SiDonneeDico="true" DonTabIndex="27" DonWidth="400px" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center" ColumnSpan="2">
                                    <Virtualia:VDuoEtiquetteCommande ID="Dontab27" runat="server" V_PointdeVue="1" V_Objet="29" V_Information="27" V_SiDonneeDico="true" DonTabIndex="28" DonWidth="400px" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center" ColumnSpan="2">
                                    <Virtualia:VDuoEtiquetteCommande ID="Dontab28" runat="server" V_PointdeVue="1" V_Objet="29" V_Information="28" V_SiDonneeDico="true" DonTabIndex="29" DonWidth="400px" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center" ColumnSpan="2">
                                    <Virtualia:VDuoEtiquetteCommande ID="Dontab29" runat="server" V_PointdeVue="1" V_Objet="29" V_Information="29" V_SiDonneeDico="true" DonTabIndex="30" DonWidth="400px" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center" ColumnSpan="2">
                                    <Virtualia:VDuoEtiquetteCommande ID="Dontab31" runat="server" V_PointdeVue="1" V_Objet="29" V_Information="31" V_SiDonneeDico="true" DonTabIndex="31" DonWidth="400px" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center" ColumnSpan="2">
                                    <Virtualia:VDuoEtiquetteCommande ID="Dontab32" runat="server" V_PointdeVue="1" V_Objet="29" V_Information="32" V_SiDonneeDico="true" DonTabIndex="32" DonWidth="400px" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Height="12px" ColumnSpan="2" />
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>
</asp:Table>
