﻿'------------------------------------------------------------------------------
' <généré automatiquement>
'     Ce code a été généré par un outil.
'
'     Les modifications apportées à ce fichier peuvent provoquer un comportement incorrect et seront perdues si
'     le code est régénéré.
' </généré automatiquement>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class Fenetre_PER_CUMUL_EMPLOI

    '''<summary>
    '''Contrôle CadreControle.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CadreControle As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle ListeGrille.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents ListeGrille As Global.Virtualia.Net.V_DataGrid

    '''<summary>
    '''Contrôle CadreInfo.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CadreInfo As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle CtlCrud.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CtlCrud As Global.Virtualia.Net.V_CommandeCRUD

    '''<summary>
    '''Contrôle CadreTitre.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CadreTitre As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle Etiquette.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Etiquette As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle CadreDates.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CadreDates As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle InfoH00.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoH00 As Global.Virtualia.Net.Controles_VCoupleEtiDonnee

    '''<summary>
    '''Contrôle InfoH08.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoH08 As Global.Virtualia.Net.Controles_VCoupleEtiDonnee

    '''<summary>
    '''Contrôle CadreDon.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CadreDon As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle Dontab01.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Dontab01 As Global.Virtualia.Net.Controles_VDuoEtiquetteCommande

    '''<summary>
    '''Contrôle Dontab02.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Dontab02 As Global.Virtualia.Net.Controles_VDuoEtiquetteCommande

    '''<summary>
    '''Contrôle Dontab06.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Dontab06 As Global.Virtualia.Net.Controles_VDuoEtiquetteCommande

    '''<summary>
    '''Contrôle InfoH05.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoH05 As Global.Virtualia.Net.Controles_VCoupleEtiDonnee

    '''<summary>
    '''Contrôle TableauTitreHeures.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents TableauTitreHeures As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle EtiHeures.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiHeures As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle InfoH07.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoH07 As Global.Virtualia.Net.Controles_VCoupleEtiDonnee

    '''<summary>
    '''Contrôle CadreDonHeuresHED.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CadreDonHeuresHED As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle InfoH11.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoH11 As Global.Virtualia.Net.Controles_VCoupleEtiDonnee

    '''<summary>
    '''Contrôle InfoH12.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoH12 As Global.Virtualia.Net.Controles_VCoupleEtiDonnee

    '''<summary>
    '''Contrôle InfoH10.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoH10 As Global.Virtualia.Net.Controles_VCoupleEtiDonnee

    '''<summary>
    '''Contrôle InfoH13.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoH13 As Global.Virtualia.Net.Controles_VCoupleEtiDonnee

    '''<summary>
    '''Contrôle TableauRemu.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents TableauRemu As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle EtiRemu.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiRemu As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle TableauMontants.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents TableauMontants As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle InfoH04.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoH04 As Global.Virtualia.Net.Controles_VCoupleEtiDonnee

    '''<summary>
    '''Contrôle LabelEuro1.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelEuro1 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle InfoH03.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoH03 As Global.Virtualia.Net.Controles_VCoupleEtiDonnee

    '''<summary>
    '''Contrôle LabelEuro2.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelEuro2 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle CadreObservations.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CadreObservations As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle InfoV09.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoV09 As Global.Virtualia.Net.Controles_VCoupleVerticalEtiDonnee
End Class
