﻿Option Strict Off
Option Explicit On
Option Compare Text

Imports VI = Virtualia.Systeme.Constantes
Imports Virtualia.ObjetBase
Imports Virtualia.OutilsVisu
Imports Virtualia.TablesObjet.ShemaPER

Partial Class Fenetre_PER_AFFECTATION_SECOND
    Inherits ObjetWebControleBase(Of PER_AFFECTATION_SECOND)
    Implements IControlCrud

#Region "IControlCrud"
    Public ReadOnly Property ControlCrud As ICtlCommandeCrud Implements IControlCrud.CtlCrud
        Get
            Return Me.CtlCrud
        End Get
    End Property
#End Region

#Region "ObjetWebControleBase"
    Private _webfonction As WebFonctions
    Protected Overrides ReadOnly Property V_WebFonction As IWebFonctions
        Get
            If (_webfonction Is Nothing) Then
                _webfonction = New WebFonctions(Me)
            End If
            Return _webfonction
        End Get
    End Property

    Protected Overrides Function InitCadreInfo() As WebControl
        Return Me.CadreInfo
    End Function

    Protected Overrides Function InitControlDataGrid() As IControlDataGrid
        Return Me.ListeGrille
    End Function

    Protected Overrides Sub InitCacheIde()

        V_CacheIde.TypeVue = WebControlTypeVue.ListeGrid

        Dim captions As List(Of String) = New List(Of String)()
        captions.Add("Aucune situation")
        captions.Add("Une situation")
        captions.Add("situations")

        Me.ListeGrille.InitCaptions(captions)

        Dim colonnes As ColonneDataGridCollection = New ColonneDataGridCollection()

        colonnes.Add(HorizontalAlign.Center, TypeData.Date, "date d'effet", 0)
        colonnes.Add(HorizontalAlign.Center, TypeData.Numeric, "Niveau N1", 1)
        colonnes.Add(HorizontalAlign.Center, TypeData.Numeric, "Niveau N2", 2)
        colonnes.Add(HorizontalAlign.Center, TypeData.Numeric, "Emploi", 5)
        colonnes.Add(HorizontalAlign.Center, TypeData.Numeric, "date de fin", 99)
        colonnes.Add(HorizontalAlign.Center, TypeData.Chaine, "Clef", 0, True)

        Me.ListeGrille.InitColonnes(colonnes)

    End Sub

    Protected Overrides Sub OnPreRender(e As EventArgs)
        MyBase.OnPreRender(e)

        CadreInfo.BackColor = _webfonction.CouleurCharte(0, "Cadre")
        CadreInfo.BorderColor = _webfonction.CouleurCharte(0, "Bordure")
        Etiquette.BackColor = _webfonction.CouleurCharte(0, "Titre")
        Etiquette.ForeColor = _webfonction.CouleurCharte(0, "Police_Claire")
        Etiquette.BorderColor = _webfonction.CouleurCharte(0, "Bordure")
        EtiEmploi.BackColor = _webfonction.CouleurCharte(0, "Sous-Titre")
        EtiEmploi.ForeColor = _webfonction.CouleurCharte(0, "Etiquette_ForeColor")
        EtiEmploi.BorderColor = _webfonction.CouleurCharte(0, "Bordure")
        EtiComplement.BackColor = _webfonction.CouleurCharte(0, "Sous-Titre")
        EtiComplement.ForeColor = _webfonction.CouleurCharte(0, "Etiquette_ForeColor")
        EtiComplement.BorderColor = _webfonction.CouleurCharte(0, "Bordure")
    End Sub

    Protected Overrides Sub OnInit(e As System.EventArgs)
        MyBase.OnInit(e)

        Dim initwebfonction As IWebFonctions = V_WebFonction
    End Sub
#End Region

#Region "Specifique"
    Public WriteOnly Property V_EtiTitreText As String
        'Get
        '    Return Etiquette.Text
        'End Get
        Set(ByVal value As String)
            Etiquette.Text = value
        End Set
    End Property

    Protected Overrides Sub LireLaFiche()
        MyBase.LireLaFiche()

        If V_CacheMaj Is Nothing Then
            Return
        End If

        Dim lst As List(Of Controles_VDuoEtiquetteCommandeBase) = (From ct In _dicoWebControl(TypeWebControl.VDuoEtiquetteCommande) _
                                                                   Where (ct.V_Information = 1 Or _
                                                                         ct.V_Information = 2 Or _
                                                                          ct.V_Information = 3 Or _
                                                                          ct.V_Information = 4 Or _
                                                                          ct.V_Information = 14 Or _
                                                                          ct.V_Information = 15) _
                                                                      Select DirectCast(ct, Controles_VDuoEtiquetteCommandeBase)).ToList()

        Dim num As Integer = 0
        lst.ForEach(Sub(ct)

                        If (ct.V_Information <= 4) Then
                            num = ct.V_Information - 1
                        Else
                            num = ct.V_Information - 10
                        End If

                        ct.Visible = Not (_webfonction.PointeurUtilisateur.PointeurParametres.LibelleNiveau_Organigramme(num))

                    End Sub)
    End Sub
#End Region

End Class

