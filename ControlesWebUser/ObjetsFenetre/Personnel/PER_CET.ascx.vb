﻿Option Strict On
Option Explicit On
Option Compare Text

Imports VI = Virtualia.Systeme.Constantes
Imports Virtualia.ObjetBase
Imports Virtualia.TablesObjet.ShemaPER
Imports Virtualia.OutilsVisu

Partial Class Fenetre_PER_CET
    Inherits ObjetWebControleBase(Of PER_CET)
    Implements IControlOK

#Region "IControlOK"
    Public ReadOnly Property CommandOK As ICtlCommandeOK Implements IControlOK.CtlOK
        Get
            Return Me.CtlOK
        End Get
    End Property
#End Region

#Region "ObjetWebControleBase"
    Private _webfonction As WebFonctions
    Protected Overrides ReadOnly Property V_WebFonction As IWebFonctions
        Get
            If (_webfonction Is Nothing) Then
                _webfonction = New WebFonctions(Me)
            End If
            Return _webfonction
        End Get
    End Property

    Protected Overrides Function InitCadreInfo() As WebControl
        Return Me.CadreInfo
    End Function

    Protected Overrides Sub InitCacheIde()
        V_CacheIde.TypeVue = WebControlTypeVue.Simple
    End Sub

    Protected Overrides Sub OnPreRender(e As EventArgs)
        MyBase.OnPreRender(e)

        CadreInfo.BackColor = _webfonction.CouleurCharte(V_CacheIde.PointDeVue, "Cadre")
        CadreInfo.BorderColor = _webfonction.CouleurCharte(V_CacheIde.PointDeVue, "Bordure")
        Etiquette.BackColor = _webfonction.CouleurCharte(V_CacheIde.PointDeVue, "Titre")
        Etiquette.ForeColor = _webfonction.CouleurCharte(V_CacheIde.PointDeVue, "Police_Claire")
        Etiquette.BorderColor = _webfonction.CouleurCharte(V_CacheIde.PointDeVue, "Bordure")
        LabelAncienCET.BackColor = _webfonction.CouleurCharte(V_CacheIde.PointDeVue, "Sous-Titre")
        LabelAncienCET.ForeColor = _webfonction.CouleurCharte(V_CacheIde.PointDeVue, "Etiquette_ForeColor")
        LabelAncienCET.BorderColor = _webfonction.CouleurCharte(V_CacheIde.PointDeVue, "Bordure")
        LabelCET.BackColor = _webfonction.CouleurCharte(V_CacheIde.PointDeVue, "Sous-Titre")
        LabelCET.ForeColor = _webfonction.CouleurCharte(V_CacheIde.PointDeVue, "Etiquette_ForeColor")
        LabelCET.BorderColor = _webfonction.CouleurCharte(V_CacheIde.PointDeVue, "Bordure")
        LabelNbjCETCP.ForeColor = _webfonction.CouleurCharte(V_CacheIde.PointDeVue, "Etiquette_BackColor")
        LabelNbjCETPris.ForeColor = _webfonction.CouleurCharte(V_CacheIde.PointDeVue, "Etiquette_BackColor")
        LabelNbjCETRTT.ForeColor = _webfonction.CouleurCharte(V_CacheIde.PointDeVue, "Etiquette_BackColor")
        LabelNbjCETIndemnise.ForeColor = _webfonction.CouleurCharte(V_CacheIde.PointDeVue, "Etiquette_BackColor")
        LabelNbjCETRC.ForeColor = _webfonction.CouleurCharte(V_CacheIde.PointDeVue, "Etiquette_BackColor")
        LabelNbjCETTotal.ForeColor = _webfonction.CouleurCharte(V_CacheIde.PointDeVue, "Etiquette_BackColor")
        LabelNbjCETRAFP.ForeColor = _webfonction.CouleurCharte(V_CacheIde.PointDeVue, "Etiquette_BackColor")
        LabelNbjCETSolde.ForeColor = _webfonction.CouleurCharte(V_CacheIde.PointDeVue, "Etiquette_BackColor")
        LabelNbjAncienCETSolde.ForeColor = _webfonction.CouleurCharte(V_CacheIde.PointDeVue, "Etiquette_BackColor")
        LabelNbjAncienCPAprendre.ForeColor = _webfonction.CouleurCharte(V_CacheIde.PointDeVue, "Etiquette_BackColor")
        LabelNbjAncienCETPaye.ForeColor = _webfonction.CouleurCharte(V_CacheIde.PointDeVue, "Etiquette_BackColor")
        LabelNbjAncienCPPris.ForeColor = _webfonction.CouleurCharte(V_CacheIde.PointDeVue, "Etiquette_BackColor")
        LabelNbjAncienRAFP.ForeColor = _webfonction.CouleurCharte(V_CacheIde.PointDeVue, "Etiquette_BackColor")
        LabelNbjAncienSoldeCP.ForeColor = _webfonction.CouleurCharte(V_CacheIde.PointDeVue, "Etiquette_BackColor")
    End Sub

    Protected Overrides Sub OnInit(e As System.EventArgs)
        MyBase.OnInit(e)

        Dim initwebfonction As IWebFonctions = V_WebFonction
    End Sub
#End Region

End Class
