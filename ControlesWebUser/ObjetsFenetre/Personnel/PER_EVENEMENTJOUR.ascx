﻿<%@ Control Language="VB" AutoEventWireup="false" Inherits="Virtualia.Net.Fenetre_PER_EVENEMENTJOUR" Codebehind="PER_EVENEMENTJOUR.ascx.vb" %>

<%@ Register src="~/Controles/Saisies/VCoupleEtiDonnee.ascx" tagname="VCoupleEtiDonnee" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/Saisies/VCoupleVerticalEtiDonnee.ascx" tagname="VCoupleVerticalEtiDonnee" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/Saisies/VDuoEtiquetteCommande.ascx" tagname="VDuoEtiquetteCommande" tagprefix="Virtualia" %>
<%@ Register src="../../Saisies/VListeCombo.ascx" tagname="VListeCombo" tagprefix="Virtualia" %>
 
 <asp:Table ID="CadreControle" runat="server" Height="35px" Width="620px" CellPadding="0" CellSpacing="0" HorizontalAlign="Center">
   <asp:TableRow> 
     <asp:TableCell HorizontalAlign="Center" VerticalAlign="Middle"> 
       <Virtualia:VListeCombo ID="ListeHisto" runat="server"
         V_PointdeVue="1" V_Objet="40" EtiText="Liste des évènements journaliers" EtiWidth="210px" LstWidth="400px" LstBorderStyle="None"/>
     </asp:TableCell>
   </asp:TableRow>
   <asp:TableRow>
     <asp:TableCell>
       <asp:Table ID="CadreInfo" runat="server" BorderStyle="Ridge" BorderWidth="2px"
        BorderColor="#B0E0D7" Height="390px" Width="530px" HorizontalAlign="Center"
        style="margin-top: 12px;">
        <asp:TableRow>
          <asp:TableCell>
            <asp:Table ID="CadreCommandes" runat="server" Height="22px" CellPadding="0" CellSpacing="0"
                Width="244px" HorizontalAlign="Right" style="margin-top: 3px;margin-right: 3px">
                <asp:TableRow>
                    <asp:TableCell VerticalAlign="Bottom">
                        <asp:Table ID="CadreCmdNewSupp" runat="server" BackImageUrl="~/Images/Boutons/NewSupp_Std.bmp" 
                            Width="160px" CellPadding="0" CellSpacing="0"
                            BorderWidth="2px" BorderStyle="Outset" BorderColor="#B0E0D7" ForeColor="#D7FAF3">
                            <asp:TableRow>
                                <asp:TableCell>
                                    <asp:Button ID="CommandeNew" runat="server" Text="Nouveau" Width="68px" Height="20px"
                                        BackColor="Transparent" BorderColor="#B0E0D7" ForeColor="#D7FAF3"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Font-Italic="true"
                                        BorderStyle="None" style=" margin-left: 10px; text-align: right;"
                                        Tooltip="Nouvelle fiche">
                                    </asp:Button>
                                </asp:TableCell>
                                <asp:TableCell>
                                    <asp:Button ID="CommandeSupp" runat="server" Text="Supprimer" Width="68px" Height="20px"
                                        BackColor="Transparent" BorderColor="#B0E0D7" ForeColor="#D7FAF3"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Font-Italic="true"
                                        BorderStyle="None" style=" margin-left: 10px; text-align: right;"
                                        Tooltip="Supprimer la fiche">
                                    </asp:Button>
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>    
                    </asp:TableCell>
                    <asp:TableCell VerticalAlign="Bottom">
                        <asp:Table ID="CadreCmdOK" runat="server" BackImageUrl="~/Images/Boutons/OK_Std.bmp" Width="70px"
                            BorderWidth="2px" BorderStyle="Outset" BorderColor="#B0E0D7" ForeColor="#D7FAF3"
                            CellPadding="0" CellSpacing="0" Visible="false">
                            <asp:TableRow>
                                <asp:TableCell>
                                    <asp:Button ID="CommandeOK" runat="server" Text="Valider" Width="65px" Height="20px"
                                        BackColor="Transparent" BorderColor="#B0E0D7" ForeColor="#D7FAF3" 
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Font-Italic="true"
                                        BorderStyle="None" style=" margin-left: 6px; text-align: right;">
                                        </asp:Button>
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>  
                    </asp:TableCell>
                </asp:TableRow>
              </asp:Table>
          </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
          <asp:TableCell>
              <asp:Table ID="CadreTitre" runat="server" Height="40px" CellPadding="0" Width="500px" 
                CellSpacing="0" HorizontalAlign="Center">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="Etiquette" runat="server" Text="Evènement journalier" Height="20px" Width="300px"
                            BackColor="#2FA49B" BorderColor="#B0E0D7" BorderStyle="Groove"
                            BorderWidth="2px" ForeColor="#D7FAF3"
                            Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Small"
                            style="margin-top: 5px; margin-left: 4px; margin-bottom: 10px;
                            font-style: oblique; text-indent: 5px; text-align: center;">
                        </asp:Label>          
                    </asp:TableCell>      
                </asp:TableRow>
              </asp:Table>
          </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
          <asp:TableCell>
              <asp:Table ID="TableauDate" runat="server" CellPadding="0" CellSpacing="0" Width="500px">
                 <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VCoupleEtiDonnee ID="InfoH00" runat="server"
                           V_PointdeVue="1" V_Objet="40" V_Information="0" V_SiDonneeDico="true"
                           EtiWidth="50px" DonWidth="80px" DonTabIndex="1"/>
                    </asp:TableCell>
                 </asp:TableRow>
                 <asp:TableRow>
                    <asp:TableCell Height="10px"></asp:TableCell>
                 </asp:TableRow> 
              </asp:Table>
          </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
          <asp:TableCell>
              <asp:Table ID="TableauHoraires" runat="server" CellPadding="0" CellSpacing="0" Width="500px">
                 <asp:TableRow>   
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VCoupleEtiDonnee ID="InfoH01" runat="server"
                           V_PointdeVue="1" V_Objet="40" V_Information="1" V_SiDonneeDico="true"
                           EtiWidth="50px" DonWidth="70px" DonTabIndex="2"/>
                    </asp:TableCell>              
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VCoupleEtiDonnee ID="InfoH06" runat="server"
                           V_PointdeVue="1" V_Objet="40" V_Information="6" V_SiDonneeDico="true"
                           EtiWidth="50px" DonWidth="70px" DonTabIndex="3"/>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VCoupleEtiDonnee ID="InfoH03" runat="server"
                           V_PointdeVue="1" V_Objet="40" V_Information="3" V_SiDonneeDico="true"
                           EtiWidth="100px" DonWidth="80px" DonTabIndex="4"/>
                    </asp:TableCell>
                 </asp:TableRow>
                 <asp:TableRow>
                    <asp:TableCell Height="10px" Columnspan="3"></asp:TableCell>
                 </asp:TableRow> 
              </asp:Table>
          </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
          <asp:TableCell>
              <asp:Table ID="TableauDon" runat="server" CellPadding="0" CellSpacing="0" Width="500px">
                 <asp:TableRow>   
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VDuoEtiquetteCommande ID="Dontab02" runat="server"
                           V_PointdeVue="1" V_Objet="40" V_Information="2" V_SiDonneeDico="true"
                           EtiWidth="170px" DonWidth="305px" DonTabIndex="5"/>
                    </asp:TableCell>
                 </asp:TableRow> 
                 <asp:TableRow>             
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VCoupleEtiDonnee ID="InfoH04" runat="server"
                           V_PointdeVue="1" V_Objet="40" V_Information="4" V_SiDonneeDico="true"
                           EtiWidth="170px" DonWidth="80px" DonTabIndex="6"/>
                    </asp:TableCell>          
                 </asp:TableRow>
                 <asp:TableRow>
                    <asp:TableCell Height="10px"></asp:TableCell>
                 </asp:TableRow> 
              </asp:Table>
          </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
          <asp:TableCell>
              <asp:Table ID="Memo" runat="server" CellPadding="0" CellSpacing="0" Width="500px" HorizontalAlign="Center">
                  <asp:TableRow> 
                    <asp:TableCell>
                        <Virtualia:VCoupleVerticalEtiDonnee ID="InfoV05" runat="server" DonTextMode="true"
                            V_PointdeVue="1" V_Objet="40" V_Information="5" V_SiDonneeDico="true"
                            EtiWidth="482px" DonWidth="480px" DonHeight="120px" DonStyle="margin-left: 5px;" DonTabIndex="7" 
                            EtiStyle="margin-left: 5px; text-align:center;"/>
                    </asp:TableCell>
                  </asp:TableRow>
                  <asp:TableRow>
                    <asp:TableCell Height="12px"></asp:TableCell>
                  </asp:TableRow> 
              </asp:Table>
          </asp:TableCell>
        </asp:TableRow>
      </asp:Table>
     </asp:TableCell>
   </asp:TableRow>
 </asp:Table>