﻿Option Strict On
Option Explicit On
Option Compare Text

Imports VI = Virtualia.Systeme.Constantes
Imports Virtualia.ObjetBase
Imports Virtualia.TablesObjet.ShemaPER
Imports Virtualia.OutilsVisu

Partial Class Fenetre_PER_MUTUELLE
    Inherits ObjetWebControleBase(Of PER_MUTUELLE)
    Implements IControlOK

#Region "IControlOK"
    Public ReadOnly Property CommandOK As ICtlCommandeOK Implements IControlOK.CtlOK
        Get
            Return Me.CtlOK
        End Get
    End Property
#End Region

#Region "ObjetWebControleBase"
    Private _webfonction As WebFonctions
    Protected Overrides ReadOnly Property V_WebFonction As IWebFonctions
        Get
            If (_webfonction Is Nothing) Then
                _webfonction = New WebFonctions(Me)
            End If
            Return _webfonction
        End Get
    End Property

    Protected Overrides Function InitCadreInfo() As WebControl
        Return Me.CadreInfo
    End Function

    Protected Overrides Sub InitCacheIde()
        V_CacheIde.TypeVue = WebControlTypeVue.Simple
    End Sub

    Protected Overrides Sub OnPreRender(e As EventArgs)
        MyBase.OnPreRender(e)

        CadreInfo.BackColor = _webfonction.CouleurCharte(V_CacheIde.PointDeVue, "Cadre")
        CadreInfo.BorderColor = _webfonction.CouleurCharte(V_CacheIde.PointDeVue, "Bordure")
        CadreInfo.BackColor = _webfonction.CouleurCharte(V_CacheIde.PointDeVue, "Cadre")
        CadreInfo.BorderColor = _webfonction.CouleurCharte(V_CacheIde.PointDeVue, "Bordure")
        LabelTitre.BackColor = _webfonction.CouleurCharte(V_CacheIde.PointDeVue, "Titre")
        LabelTitre.ForeColor = _webfonction.CouleurCharte(V_CacheIde.PointDeVue, "Police_Claire")
        LabelTitre.BorderColor = _webfonction.CouleurCharte(V_CacheIde.PointDeVue, "Bordure")
        EtiGarantie.BackColor = _webfonction.CouleurCharte(V_CacheIde.PointDeVue, "Sous-Titre")
        EtiGarantie.ForeColor = _webfonction.CouleurCharte(V_CacheIde.PointDeVue, "Etiquette_ForeColor")
        EtiGarantie.BorderColor = _webfonction.CouleurCharte(V_CacheIde.PointDeVue, "Bordure")
    End Sub

    Protected Overrides Sub OnInit(e As System.EventArgs)
        MyBase.OnInit(e)

        Dim initwebfonction As IWebFonctions = V_WebFonction
    End Sub


    Public Sub New()
        MyBase.New()
    End Sub
#End Region

End Class


