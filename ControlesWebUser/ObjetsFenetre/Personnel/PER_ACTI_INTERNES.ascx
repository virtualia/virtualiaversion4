﻿<%@ Control Language="VB" AutoEventWireup="false" Inherits="Virtualia.Net.Fenetre_PER_ACTI_INTERNES" CodeBehind="PER_ACTI_INTERNES.ascx.vb" %>

<%@ Register Src="~/Controles/Saisies/VCoupleEtiDonnee.ascx" TagName="VCoupleEtiDonnee" TagPrefix="Virtualia" %>
<%@ Register Src="~/Controles/Saisies/VDuoEtiquetteCommande.ascx" TagName="VDuoEtiquetteCommande" TagPrefix="Virtualia" %>
<%@ Register Src="~/Controles/Saisies/VCoupleEtiquetteExperte.ascx" TagName="VCoupleEtiquetteExperte" TagPrefix="Virtualia" %>
<%@ Register Src="~/Controles/Saisies/VCoupleVerticalEtiDonnee.ascx" TagName="VCoupleVerticalEtiDonnee" TagPrefix="Virtualia" %>

<%@ Register Src="~/ControlsGeneriques/V_DataGrid.ascx" TagName="VDataGrid" TagPrefix="Generic" %>
<%@ Register Src="~/ControlsGeneriques/V_CommandeCRUD.ascx" TagName="VCrud" TagPrefix="Generic" %>

<asp:Table ID="CadreControle" runat="server" Height="30px" Width="710px" CellPadding="0" CellSpacing="0" HorizontalAlign="Center">
    <asp:TableRow>
        <asp:TableCell>
            <Generic:VDataGrid ID="ListeGrille" runat="server" CadreWidth="650px" SiColonneSelect="true" SiCaseAcocher="false" />
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="CadreInfo" runat="server" BorderStyle="Ridge" BorderWidth="2px" BorderColor="#B0E0D7" Height="500px" Width="700px" HorizontalAlign="Center" Style="margin-top: 3px;">
                <asp:TableRow>
                    <asp:TableCell>
                        <Generic:VCrud ID="CtlCrud" runat="server" Height="22px" Width="244px" HorizontalAlign="Right" CadreStyle="margin-top: 3px; margin-right: 3px" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Table ID="CadreTitre" runat="server" Height="40px" CellPadding="0" Width="680px" CellSpacing="0" HorizontalAlign="Center">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                    <asp:Label ID="Etiquette" runat="server" Text="Activités internes" Height="20px" Width="320px" BackColor="#2FA49B" BorderColor="#B0E0D7" BorderStyle="Groove" BorderWidth="2px" ForeColor="#D7FAF3" Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Small" Style="margin-top: 5px; margin-left: 4px; margin-bottom: 10px; font-style: oblique; text-indent: 5px; text-align: center;" />
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Table ID="CadreDon" runat="server" CellPadding="0" CellSpacing="0" Width="680px">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left" Width="300px">
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH00" runat="server" V_PointdeVue="1" V_Objet="75" V_Information="0" V_SiDonneeDico="true" DonWidth="100px" DonTabIndex="1" />
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Left" Width="300px">
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH05" runat="server" V_PointdeVue="1" V_Objet="75" V_Information="5" V_SiDonneeDico="true" EtiWidth="80px" DonWidth="100px" DonTabIndex="2" EtiStyle="margin-left:2px" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left" Width="300px" ColumnSpan="2">
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH04" runat="server" V_PointdeVue="1" V_Objet="75" V_Information="8" V_SiDonneeDico="true" DonWidth="100px" DonTabIndex="3" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left" ColumnSpan="2">
                                    <Virtualia:VDuoEtiquetteCommande ID="Dontab01" runat="server" V_PointdeVue="1" V_Objet="75" V_Information="1" V_SiDonneeDico="true" DonTabIndex="4" DonWidth="490px" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left" ColumnSpan="2">
                                    <Virtualia:VDuoEtiquetteCommande ID="Dontab02" runat="server" V_PointdeVue="1" V_Objet="75" V_Information="2" V_SiDonneeDico="true" DonTabIndex="4" EtiWidth="120px" DonWidth="150px" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left" ColumnSpan="2">
                                    <Virtualia:VDuoEtiquetteCommande ID="Dontab03" runat="server" V_PointdeVue="1" V_Objet="75" V_Information="3" V_SiDonneeDico="true" DonTabIndex="5" EtiWidth="250px" DonWidth="400px" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left" ColumnSpan="2">
                                    <Virtualia:VCoupleVerticalEtiDonnee ID="InfoV06" runat="server" DonTextMode="true" V_PointdeVue="1" V_Objet="75" V_Information="6" V_SiDonneeDico="true" EtiWidth="652px" DonWidth="650px" DonHeight="80px" DonTabIndex="6" DonStyle="margin-left:5px" EtiStyle="margin-left:5px" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left" ColumnSpan="2">
                                    <Virtualia:VDuoEtiquetteCommande ID="Dontab07" runat="server" V_PointdeVue="1" V_Objet="75" V_Information="7" V_SiDonneeDico="true" DonTabIndex="7" EtiWidth="250px" DonWidth="400px" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left" ColumnSpan="2">
                                    <Virtualia:VDuoEtiquetteCommande ID="Dontab08" runat="server" V_PointdeVue="1" V_Objet="75" V_Information="8" V_SiDonneeDico="true" DonTabIndex="8" EtiWidth="250px" DonWidth="400px" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left" ColumnSpan="2">
                                    <Virtualia:VDuoEtiquetteCommande ID="Dontab09" runat="server" V_PointdeVue="1" V_Objet="75" V_Information="9" V_SiDonneeDico="true" DonTabIndex="9" EtiWidth="250px" DonWidth="400px" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left" ColumnSpan="2">
                                    <Virtualia:VDuoEtiquetteCommande ID="Dontab10" runat="server" V_PointdeVue="1" V_Objet="75" V_Information="10" V_SiDonneeDico="true" DonTabIndex="10" EtiWidth="250px" DonWidth="400px" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Height="12px" ColumnSpan="2" />
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Panel ID="CadreExpert" runat="server" BorderStyle="None" BorderWidth="1px" BorderColor="#B0E0D7" Height="80px" Width="650px" HorizontalAlign="Left" Style="margin-top: 5px; vertical-align: middle">
                            <Virtualia:VCoupleEtiquetteExperte ID="ExprH01" runat="server" V_PointdeVue="1" V_Objet="75" V_InfoExperte="780" V_SiDonneeDico="true" EtiHeight="20px" EtiWidth="300px" DonWidth="200px" DonHeight="20px" EtiStyle="margin-left: 20px; margin-top: 5px" DonStyle="margin-top: 9px; text-align: center" />
                            <Virtualia:VCoupleEtiquetteExperte ID="ExprH02" runat="server" V_PointdeVue="1" V_Objet="75" V_InfoExperte="781" V_SiDonneeDico="true" EtiHeight="20px" EtiWidth="300px" DonWidth="200px" DonHeight="20px" EtiStyle="margin-left: 20px;" DonStyle="text-align: center;" />
                        </asp:Panel>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="5px" />
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>
</asp:Table>
