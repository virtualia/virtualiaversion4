﻿<%@ Control Language="VB" AutoEventWireup="false" Inherits="Virtualia.Net.Fenetre_PER_DROIT_CONGES" CodeBehind="PER_DROIT_CONGES.ascx.vb" %>

<%@ Register Src="~/Controles/Saisies/VCoupleEtiDonnee.ascx" TagName="VCoupleEtiDonnee" TagPrefix="Virtualia" %>
<%@ Register Src="~/Controles/Saisies/VCocheSimple.ascx" TagName="VCocheSimple" TagPrefix="Virtualia" %>
<%@ Register Src="~/Controles/Saisies/VCoupleVerticalEtiDonnee.ascx" TagName="VCoupleVerticalEtiDonnee" TagPrefix="Virtualia" %>

<%@ Register Src="~/ControlsGeneriques/V_DataGrid.ascx" TagName="VDataGrid" TagPrefix="Generic" %>
<%@ Register Src="~/ControlsGeneriques/V_CommandeCRUD.ascx" TagName="VCrud" TagPrefix="Generic" %>

<asp:Table ID="CadreControle" runat="server" Height="30px" Width="660px" CellPadding="0" CellSpacing="0" HorizontalAlign="Center">
    <asp:TableRow>
        <asp:TableCell>
            <Generic:VDataGrid ID="ListeGrille" runat="server" CadreWidth="650px" SiColonneSelect="true" SiCaseAcocher="false" />
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="CadreInfo" runat="server" BorderStyle="Ridge" BorderWidth="2px" Visible="true" BorderColor="#B0E0D7" Height="770px" Width="700px" HorizontalAlign="Center">
                <asp:TableRow>
                    <asp:TableCell>
                        <Generic:VCrud ID="CtlCrud" runat="server" Height="22px" Width="244px" HorizontalAlign="Right" CadreStyle="margin-top: 3px; margin-right: 3px" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow HorizontalAlign="Center">
                    <asp:TableCell>
                        <asp:Table ID="CadreTitre" runat="server" Height="20px" CellPadding="0" Width="700px" CellSpacing="0" HorizontalAlign="Center">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                    <asp:Label ID="Etiquette" runat="server" Text="Droits à congés" Height="20px" Width="400px" BackColor="#2FA49B" BorderColor="#B0E0D7" BorderStyle="Groove" BorderWidth="2px" ForeColor="#D7FAF3" Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Small" Style="margin-top: 5px; margin-left: 1px; margin-bottom: 5px; font-style: oblique; text-indent: 5px; text-align: center;" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Height="8px" />
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH00" runat="server" V_PointdeVue="1" V_Objet="16" V_Information="0" V_SiDonneeDico="true" EtiWidth="170px" DonWidth="80px" DonTabIndex="1" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Height="8px" />
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell>
                                    <asp:Label ID="EtiConges" runat="server" Height="20px" Width="350px" Text="Congés annuels" BackColor="#B0E0D7" BorderColor="#B0E0D7" BorderStyle="Outset" BorderWidth="2px" ForeColor="#142425" Font-Italic="true" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Style="text-indent: 1px; text-align: center;" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Height="6px" />
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow HorizontalAlign="Center">
                    <asp:TableCell>
                        <asp:Table ID="CadreAnneeDroits" runat="server" CellPadding="0" CellSpacing="0" Width="680px" HorizontalAlign="Center">
                            <asp:TableRow>
                                <asp:TableCell>
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH01" runat="server" V_PointdeVue="1" V_Objet="16" V_Information="1" V_SiDonneeDico="true" EtiWidth="172px" DonWidth="50px" DonTabIndex="2" />
                                </asp:TableCell>
                                <asp:TableCell>
                                    <asp:Label ID="LabelJour01" runat="server" Text="jours" Height="20px" Width="70px" BackColor="Transparent" BorderStyle="none" BorderWidth="0px" ForeColor="#2FA49B" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; font-style: oblique; text-indent: 1px; text-align: left;" />
                                </asp:TableCell>
                                <asp:TableCell>
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH10" runat="server" V_PointdeVue="1" V_Objet="16" V_Information="10" V_SiDonneeDico="true" EtiWidth="150px" DonWidth="70px" DonTabIndex="3" />
                                </asp:TableCell>
                                <asp:TableCell>
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH11" runat="server" V_PointdeVue="1" V_Objet="16" V_Information="11" V_SiDonneeDico="true" EtiWidth="30px" DonWidth="70px" DonTabIndex="4" />
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow HorizontalAlign="Center">
                    <asp:TableCell>
                        <asp:Table ID="TableauReportCA" runat="server" CellPadding="0" CellSpacing="0" Width="680px" HorizontalAlign="Center">
                            <asp:TableRow>
                                <asp:TableCell Height="1px" ColumnSpan="3" />
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH02" runat="server" V_PointdeVue="1" V_Objet="16" V_Information="2" V_SiDonneeDico="true" EtiWidth="172px" DonWidth="50px" DonTabIndex="5" />
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Left">
                                    <asp:Label ID="LabelJour02" runat="server" Text="jours" Height="20px" Width="70px" BackColor="Transparent" BorderStyle="none" BorderWidth="0px" ForeColor="#2FA49B" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; font-style: oblique; text-indent: 3px; text-align: left;" />
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Left">
                                    <Virtualia:VCocheSimple ID="Coche14" runat="server" V_PointdeVue="1" V_Objet="16" V_Information="14" V_SiDonneeDico="true" V_Width="351px" V_Style="margin-left:7px" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH17" runat="server" V_PointdeVue="1" V_Objet="16" V_Information="17" V_SiDonneeDico="true" EtiWidth="172px" DonWidth="50px" DonTabIndex="6" />
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Left">
                                    <asp:Label ID="LabelJour17" runat="server" Text="jours" Height="20px" Width="70px" BackColor="Transparent" BorderStyle="none" BorderWidth="0px" ForeColor="#2FA49B" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; font-style: oblique; text-indent: 3px; text-align: left;" />
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Left">
                                    <Virtualia:VCocheSimple ID="Coche15" runat="server" V_PointdeVue="1" V_Objet="16" V_Information="15" V_SiDonneeDico="true" V_Width="351px" V_Style="margin-left:7px" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left" ColumnSpan="3">
                                    <Virtualia:VCocheSimple ID="Coche18" runat="server" V_PointdeVue="1" V_Objet="16" V_Information="18" V_SiDonneeDico="true" V_Width="351px" V_Style="margin-left:320px" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Height="10px" ColumnSpan="3" />
                            </asp:TableRow>
                            <asp:TableRow Height="35px">
                                <asp:TableCell HorizontalAlign="Center" ColumnSpan="3">
                                    <asp:Label ID="LabelARTT" runat="server" Height="20px" Width="350px" Text="ARTT" BackColor="#B0E0D7" BorderColor="#B0E0D7" BorderStyle="Outset" BorderWidth="2px" ForeColor="#142425" Font-Italic="true" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" />
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow HorizontalAlign="Center">
                    <asp:TableCell>
                        <asp:Table ID="TableauJRTT" runat="server" CellPadding="0" CellSpacing="0" Width="680px" HorizontalAlign="Center">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH08" runat="server" V_PointdeVue="1" V_Objet="16" V_Information="8" V_SiDonneeDico="true" EtiWidth="172px" DonWidth="50px" DonTabIndex="7" />
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Left">
                                    <asp:Label ID="LabelJour08" runat="server" Text="jours" Height="20px" Width="70px" BackColor="Transparent" BorderStyle="none" BorderWidth="0px" ForeColor="#2FA49B" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; font-style: oblique; text-indent: 1px; text-align: left;" />
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Left">
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH12" runat="server" V_PointdeVue="1" V_Objet="16" V_Information="12" V_SiDonneeDico="true" EtiWidth="150px" DonWidth="70px" DonTabIndex="8" />
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Left">
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH13" runat="server" V_PointdeVue="1" V_Objet="16" V_Information="13" V_SiDonneeDico="true" EtiWidth="30px" DonWidth="70px" DonTabIndex="9" />
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow HorizontalAlign="Center">
                    <asp:TableCell>
                        <asp:Table ID="TableauReportRTT" runat="server" CellPadding="0" CellSpacing="0" Width="680px" HorizontalAlign="Center">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH22" runat="server" V_PointdeVue="1" V_Objet="16" V_Information="22" V_SiDonneeDico="true" EtiWidth="172px" DonWidth="50px" DonTabIndex="10" />
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Left">
                                    <asp:Label ID="LabelJour22" runat="server" Text="jours" Height="20px" Width="70px" BackColor="Transparent" BorderStyle="none" BorderWidth="0px" ForeColor="#2FA49B" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; font-style: oblique; text-indent: 3px; text-align: left;" />
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Left">
                                    <Virtualia:VCocheSimple ID="Coche16" runat="server" V_PointdeVue="1" V_Objet="16" V_Information="16" V_SiDonneeDico="true" V_Width="351px" V_Style="margin-left:7px" />
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow HorizontalAlign="Center">
                    <asp:TableCell>
                        <asp:Table ID="TableauRTTAnticipation" runat="server" CellPadding="0" CellSpacing="0" Width="680px" HorizontalAlign="Center">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH23" runat="server" V_PointdeVue="1" V_Objet="16" V_Information="23" V_SiDonneeDico="true" EtiWidth="172px" DonWidth="50px" DonTabIndex="10" />
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Left">
                                    <asp:Label ID="LabelJour23" runat="server" Text="jours" Height="20px" Width="435px" BackColor="Transparent" BorderStyle="none" BorderWidth="0px" ForeColor="#2FA49B" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; font-style: oblique; text-indent: 3px; text-align: left;" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Height="10px" ColumnSpan="2" />
                            </asp:TableRow>
                            <asp:TableRow Height="30px">
                                <asp:TableCell HorizontalAlign="Center" ColumnSpan="2">
                                    <asp:Label ID="EtiAutreConges" runat="server" Height="20px" Width="350px" Text="Autres droits à congés" BackColor="#B0E0D7" BorderColor="#B0E0D7" BorderStyle="Outset" BorderWidth="2px" ForeColor="#142425" Font-Italic="true" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Height="5px" ColumnSpan="2" />
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow HorizontalAlign="Center">
                    <asp:TableCell>
                        <asp:Table ID="TableeauAutresConges" runat="server" CellPadding="0" CellSpacing="0" Width="680px" HorizontalAlign="Center">
                            <asp:TableRow>
                                <asp:TableCell Height="1px" ColumnSpan="3" />
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH03" runat="server" V_PointdeVue="1" V_Objet="16" V_Information="3" V_SiDonneeDico="true" EtiWidth="252px" DonWidth="50px" DonTabIndex="11" />
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Left">
                                    <asp:Label ID="LabelJour03" runat="server" Text="jours" Height="20px" Width="50px" BackColor="Transparent" BorderStyle="none" BorderWidth="0px" ForeColor="#2FA49B" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; font-style: oblique; text-indent: 3px; text-align: left;" />
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Left">
                                    <Virtualia:VCocheSimple ID="Coche19" runat="server" V_PointdeVue="1" V_Objet="16" V_Information="19" V_SiDonneeDico="true" V_Width="271px" V_Style="margin-left:7px" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH04" runat="server" V_PointdeVue="1" V_Objet="16" V_Information="4" V_SiDonneeDico="true" EtiWidth="252px" DonWidth="50px" DonTabIndex="12" />
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Left">
                                    <asp:Label ID="LabelJour04" runat="server" Text="jours" Height="20px" Width="50px" BackColor="Transparent" BorderStyle="none" BorderWidth="0px" ForeColor="#2FA49B" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; font-style: oblique; text-indent: 3px; text-align: left;" />
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Left">
                                    <Virtualia:VCocheSimple ID="Coche20" runat="server" V_PointdeVue="1" V_Objet="16" V_Information="20" V_SiDonneeDico="true" V_Width="271px" V_Style="margin-left:7px" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH05" runat="server" V_PointdeVue="1" V_Objet="16" V_Information="5" V_SiDonneeDico="true" EtiWidth="252px" DonWidth="50px" DonTabIndex="13" />
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Left">
                                    <asp:Label ID="LabelJour05" runat="server" Text="jours" Height="20px" Width="50px" BackColor="Transparent" BorderStyle="none" BorderWidth="0px" ForeColor="#2FA49B" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; font-style: oblique; text-indent: 3px; text-align: left;" />
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Left">
                                    <Virtualia:VCocheSimple ID="Coche21" runat="server" V_PointdeVue="1" V_Objet="16" V_Information="21" V_SiDonneeDico="true" V_Width="271px" V_Style="margin-left:7px" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Height="5px" ColumnSpan="3" />
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow HorizontalAlign="Center">
                    <asp:TableCell>
                        <asp:Table ID="TableauDroitsRepos" runat="server" CellPadding="0" CellSpacing="0" Width="680px" HorizontalAlign="Center">
                            <asp:TableRow>
                                <asp:TableCell Height="1px" ColumnSpan="2" />
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH06" runat="server" V_PointdeVue="1" V_Objet="16" V_Information="6" V_SiDonneeDico="true" EtiWidth="370px" DonWidth="60px" DonTabIndex="14" />
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Left">
                                    <asp:Label ID="LabelHeure06" runat="server" Text="heures" Height="20px" Width="220px" BackColor="Transparent" BorderStyle="none" BorderWidth="0px" ForeColor="#2FA49B" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; font-style: oblique; text-indent: 3px; text-align: left;" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH07" runat="server" V_PointdeVue="1" V_Objet="16" V_Information="7" V_SiDonneeDico="true" EtiWidth="370px" DonWidth="60px" DonTabIndex="15" />
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Left">
                                    <asp:Label ID="LabelHeure07" runat="server" Text="heures" Height="20px" Width="220px" BackColor="Transparent" BorderStyle="none" BorderWidth="0px" ForeColor="#2FA49B" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; font-style: oblique; text-indent: 3px; text-align: left;" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Height="10px" ColumnSpan="2" />
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center" ColumnSpan="2">
                                    <Virtualia:VCoupleVerticalEtiDonnee ID="InfoV09" runat="server" DonTextMode="true" V_PointdeVue="1" V_Objet="16" V_Information="9" V_SiDonneeDico="true" EtiWidth="645px" DonWidth="643px" DonHeight="100px" DonTabIndex="16" EtiStyle="margin-left: 0px; text-align:center;" Donstyle="margin-left: 0px;" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Height="12px" ColumnSpan="2" />
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>
</asp:Table>
