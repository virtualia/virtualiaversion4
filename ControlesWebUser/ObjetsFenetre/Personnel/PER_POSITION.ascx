﻿<%@ Control Language="VB" AutoEventWireup="false" Inherits="Virtualia.Net.Fenetre_PER_POSITION" CodeBehind="PER_POSITION.ascx.vb" %>

<%@ Register Src="~/Controles/Saisies/VCoupleEtiDonnee.ascx" TagName="VCoupleEtiDonnee" TagPrefix="Virtualia" %>
<%@ Register Src="~/Controles/Saisies/VDuoEtiquetteCommande.ascx" TagName="VDuoEtiquetteCommande" TagPrefix="Virtualia" %>
<%@ Register Src="~/Controles/Saisies/VCoupleEtiquetteExperte.ascx" TagName="VCoupleEtiquetteExperte" TagPrefix="Virtualia" %>
<%@ Register Src="~/Controles/Saisies/VCocheSimple.ascx" TagName="VCocheSimple" TagPrefix="Virtualia" %>

<%@ Register Src="~/ControlsGeneriques/V_DataGrid.ascx" TagName="VDataGrid" TagPrefix="Generic" %>
<%@ Register Src="~/ControlsGeneriques/V_CommandeCRUD.ascx" TagName="VCrud" TagPrefix="Generic" %>

<asp:Table ID="CadreControle" runat="server" Height="30px" Width="600px" CellPadding="0" CellSpacing="0" HorizontalAlign="Center">
    <asp:TableRow>
        <asp:TableCell>
            <Generic:VDataGrid ID="ListeGrille" runat="server" CadreWidth="530px" SiColonneSelect="true" SiCaseAcocher="false" />
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="CadreInfo" runat="server" BorderStyle="Ridge" BorderWidth="2px" BorderColor="#B0E0D7" Height="750px" Width="520px" HorizontalAlign="Center" Style="margin-top: 3px;">
                <asp:TableRow>
                    <asp:TableCell>
                        <Generic:VCrud ID="CtlCrud" runat="server" Height="22px" Width="244px" HorizontalAlign="Right" CadreStyle="margin-top: 3px; margin-right: 3px" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Table ID="CadreTitre" runat="server" Height="40px" CellPadding="0" Width="550px" CellSpacing="0" HorizontalAlign="Center">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                    <asp:Label ID="Etiquette" runat="server" Text="Position administrative" Height="20px" Width="300px" BackColor="#2FA49B" BorderColor="#B0E0D7" BorderStyle="Solid" BorderWidth="2px" ForeColor="#D7FAF3" Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Small" Style="margin-top: 5px; margin-left: 4px; margin-bottom: 10px; font-style: oblique; text-indent: 5px; text-align: center;" />
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Table ID="CadreDon" runat="server" CellPadding="0" CellSpacing="0" Width="550px">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left" Width="250px">
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH00" runat="server" V_PointdeVue="1" V_Objet="13" V_Information="0" V_SiDonneeDico="true" DonWidth="80px" DonTabIndex="1" />
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Left" Width="250px">
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH07" runat="server" V_PointdeVue="1" V_Objet="13" V_Information="7" V_SiDonneeDico="true" EtiWidth="80px" DonWidth="80px" DonTabIndex="2" EtiStyle="margin-left:2px" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left" ColumnSpan="2">
                                    <Virtualia:VDuoEtiquetteCommande ID="Dontab01" runat="server" V_PointdeVue="1" V_Objet="13" V_Information="1" V_SiDonneeDico="true" DonTabIndex="3" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left" ColumnSpan="2">
                                    <Virtualia:VDuoEtiquetteCommande ID="Dontab05" runat="server" V_PointdeVue="1" V_Objet="13" V_Information="5" V_SiDonneeDico="true" DonTabIndex="4" EtiText="Administration" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left" ColumnSpan="2">
                                    <Virtualia:VDuoEtiquetteCommande ID="Dontab11" runat="server" V_PointdeVue="1" V_Objet="13" V_Information="11" V_SiDonneeDico="true" DonTabIndex="5" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left" ColumnSpan="2">
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH02" runat="server" EtiWidth="300px" V_PointdeVue="1" V_Objet="13" V_Information="2" V_SiDonneeDico="true" DonWidth="80px" DonTabIndex="6" EtiStyle="text-align:center" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left" ColumnSpan="2">
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH12" runat="server" EtiWidth="300px" V_PointdeVue="1" V_Objet="13" V_Information="12" V_SiDonneeDico="true" DonWidth="80px" DonTabIndex="7" EtiStyle="text-align:center" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left" ColumnSpan="2">
                                    <Virtualia:VDuoEtiquetteCommande ID="Dontab08" runat="server" V_PointdeVue="1" V_Objet="13" V_Information="8" V_SiDonneeDico="true" DonTabIndex="8" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow Height="25px">
                                <asp:TableCell HorizontalAlign="Center" ColumnSpan="2">
                                    <asp:Label ID="EtiArrete" runat="server" Height="20px" Width="300px" Text="Date et référence de l'arrété" BackColor="#B0E0D7" BorderColor="#B0E0D7" BorderStyle="Outset" BorderWidth="2px" ForeColor="#142425" Font-Italic="true" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Right">
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH03" runat="server" V_PointdeVue="1" V_Objet="13" V_Information="3" V_SiDonneeDico="true" DonWidth="80px" DonTabIndex="9" EtiVisible="false" />
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Left">
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH04" runat="server" V_PointdeVue="1" V_Objet="13" V_Information="4" V_SiDonneeDico="true" DonWidth="200px" DonTabIndex="10" EtiVisible="false" DonStyle="margin-left:2px" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center" ColumnSpan="2">
                                    <asp:Label ID="EtiDecret" runat="server" Height="20px" Width="300px" Text="Date et référence de parution au JO du décret" BackColor="#B0E0D7" BorderColor="#B0E0D7" BorderStyle="Outset" BorderWidth="2px" ForeColor="#142425" Font-Italic="true" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Right">
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH09" runat="server" V_PointdeVue="1" V_Objet="13" V_Information="9" V_SiDonneeDico="true" DonWidth="80px" DonTabIndex="11" EtiVisible="false" />
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Left">
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH10" runat="server" V_PointdeVue="1" V_Objet="13" V_Information="10" V_SiDonneeDico="true" DonWidth="200px" DonTabIndex="12" EtiVisible="false" DonStyle="margin-left:2px" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center" ColumnSpan="2">
                                    <asp:Label ID="EtiGestion" runat="server" Height="20px" Width="350px" Text="Compléments d'information - Mode de gestion" BackColor="#216B68" BorderColor="#B0E0D7" BorderStyle="Outset" BorderWidth="2px" ForeColor="#D7FAF3" Font-Italic="true" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Style="margin-top: 10px; margin-bottom: 10px" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left" ColumnSpan="2">
                                    <Virtualia:VCocheSimple ID="Coche18" runat="server" V_PointdeVue="1" V_Objet="13" V_Information="18" V_SiDonneeDico="true" V_Width="300px" V_Style="margin-left:120px" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left" ColumnSpan="2">
                                    <Virtualia:VCocheSimple ID="Coche19" runat="server" V_PointdeVue="1" V_Objet="13" V_Information="19" V_SiDonneeDico="true" V_Width="300px" V_Style="margin-left:120px" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left" ColumnSpan="2">
                                    <Virtualia:VCocheSimple ID="Coche20" runat="server" V_PointdeVue="1" V_Objet="13" V_Information="20" V_SiDonneeDico="true" V_Width="300px" V_Style="margin-left:120px" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center" ColumnSpan="2">
                                    <asp:Label ID="EtiRetraite" runat="server" Height="20px" Width="350px" Text="Compléments d'information - Retraite et pension" BackColor="#216B68" BorderColor="#B0E0D7" BorderStyle="Outset" BorderWidth="2px" ForeColor="#D7FAF3" Font-Italic="true" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Style="margin-top: 10px" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left" ColumnSpan="2">
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH17" runat="server" V_PointdeVue="1" V_Objet="13" V_Information="17" V_SiDonneeDico="true" EtiWidth="200px" DonWidth="80px" DonTabIndex="13" EtiStyle="margin-left:30px; margin-top:5px" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left" ColumnSpan="2">
                                    <Virtualia:VCocheSimple ID="Coche13" runat="server" V_PointdeVue="1" V_Objet="13" V_Information="13" V_SiDonneeDico="true" V_Width="300px" V_Style="margin-left:30px" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left" ColumnSpan="2">
                                    <Virtualia:VCocheSimple ID="Coche14" runat="server" V_PointdeVue="1" V_Objet="13" V_Information="14" V_SiDonneeDico="true" V_Width="300px" V_Style="margin-left:30px" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left" ColumnSpan="2">
                                    <Virtualia:VCocheSimple ID="Coche15" runat="server" V_PointdeVue="1" V_Objet="13" V_Information="15" V_SiDonneeDico="true" V_Width="300px" V_Style="margin-left:30px" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left" ColumnSpan="2">
                                    <Virtualia:VCocheSimple ID="Coche16" runat="server" V_PointdeVue="1" V_Objet="13" V_Information="16" V_SiDonneeDico="true" V_Width="300px" V_Style="margin-left:30px" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Height="12px" ColumnSpan="2" />
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Panel ID="CadreExpert" runat="server" BorderStyle="None" BorderWidth="1px" BorderColor="#B0E0D7" Height="200px" Width="520px" HorizontalAlign="Left" Style="margin-top: 5px; vertical-align: middle">
                            <Virtualia:VCoupleEtiquetteExperte ID="ExprH01" runat="server" V_PointdeVue="1" V_Objet="13" V_InfoExperte="930" V_SiDonneeDico="true" EtiHeight="20px" EtiWidth="250px" DonWidth="80px" DonHeight="20px" EtiStyle="margin-left: 40px; margin-top: 5px" DonStyle="margin-top: 9px; text-align: center" />
                            <Virtualia:VCoupleEtiquetteExperte ID="ExprH02" runat="server" V_PointdeVue="1" V_Objet="13" V_InfoExperte="931" V_SiDonneeDico="true" EtiHeight="20px" EtiWidth="250px" DonWidth="80px" DonHeight="20px" EtiStyle="margin-left: 40px;" DonStyle="text-align: center;" />
                            <Virtualia:VCoupleEtiquetteExperte ID="ExprH03" runat="server" V_PointdeVue="1" V_Objet="13" V_InfoExperte="777" V_SiDonneeDico="true" EtiHeight="20px" EtiWidth="250px" DonWidth="200px" DonHeight="20px" EtiStyle="margin-left: 40px;" DonStyle="text-align: center;" />
                            <Virtualia:VCoupleEtiquetteExperte ID="ExprH04" runat="server" V_PointdeVue="1" V_Objet="13" V_InfoExperte="670" V_SiDonneeDico="true" EtiHeight="20px" EtiWidth="250px" DonWidth="120px" DonHeight="20px" EtiStyle="margin-left: 40px;" DonStyle="text-align: center;" />
                            <Virtualia:VCoupleEtiquetteExperte ID="ExprH05" runat="server" V_PointdeVue="1" V_Objet="13" V_InfoExperte="932" V_SiDonneeDico="true" EtiHeight="20px" EtiWidth="250px" DonWidth="120px" DonHeight="20px" EtiStyle="margin-left: 40px;" DonStyle="text-align: center;" />
                            <Virtualia:VCoupleEtiquetteExperte ID="ExprH06" runat="server" V_PointdeVue="1" V_Objet="13" V_InfoExperte="933" V_SiDonneeDico="true" EtiHeight="20px" EtiWidth="250px" DonWidth="120px" DonHeight="20px" EtiStyle="margin-left: 40px;" DonStyle="text-align: center;" />
                        </asp:Panel>
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>
</asp:Table>
