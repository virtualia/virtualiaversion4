﻿Option Strict On
Option Explicit On
Option Compare Text
Imports VI = Virtualia.Systeme.Constantes
Imports Microsoft.VisualBasic
Partial Class Fenetre_PER_DEBIT_CREDIT
    Inherits System.Web.UI.UserControl
    Private WebFct As Virtualia.Net.WebFonctions
    Private WsIde_Dossier As Integer = 0
    'Execution
    Private WsNomStateIde As String = "VDebitCreditIde"
    Private WsNomStateParam As String = "VDCParam"
    Private WsCacheIde As ArrayList
    Private WsCacheParam As ArrayList
    Private WsDossierRtt As Virtualia.Metier.TempsTravail.DossierRTT
    Private WsCreditAnnuel As Virtualia.Metier.TempsTravail.TableauDebitCredit
    Private WsListeVues As List(Of Virtualia.TablesObjet.ShemaVUE.VUE_JourTravail)

    Private WsLibelGrid As String = "Aucune valeur" & VI.Tild & "Une valeur" & VI.Tild & "valeurs"
    Private WsColonneGrid As String = "Journée" & VI.Tild & "Service fait" & VI.Tild & "Crédit" & VI.Tild & "Débit" & VI.Tild & "Solde" & VI.Tild & "Cumul" & VI.Tild & "Clef"
    Private WsCumSolde As Integer
    Private WsTotalConstatee As Integer
    Private WsTotalRecup As Integer
    Private WsTotalCredit As Integer
    Private WsTotalDebit As Integer

    Public Property Identifiant() As Integer
        Get
            If Me.ViewState(WsNomStateIde) IsNot Nothing Then
                WsCacheIde = CType(Me.ViewState(WsNomStateIde), ArrayList)
                WsIde_Dossier = CInt(WsCacheIde(0))
            End If
            Return WsIde_Dossier
        End Get
        Set(ByVal value As Integer)
            If Me.ViewState(WsNomStateIde) IsNot Nothing Then
                WsCacheIde = CType(Me.ViewState(WsNomStateIde), ArrayList)
                WsIde_Dossier = CInt(WsCacheIde(0))
            End If
            If value = WsIde_Dossier Then
                Exit Property
            End If
            WsIde_Dossier = value
            If WsIde_Dossier = 0 Then
                Exit Property
            End If
            If Me.ViewState(WsNomStateIde) IsNot Nothing Then
                Me.ViewState.Remove(WsNomStateIde)
            End If
            '** MettreEnCache le dossier **
            WsCacheIde = New ArrayList
            WsCacheIde.Add(WsIde_Dossier)
            Me.ViewState.Add(WsNomStateIde, WsCacheIde)
            '**
            WsDossierRtt = WebFct.PointeurUtilisateur.PointeurDllRTT.ItemDossier(WsIde_Dossier)
            If WsDossierRtt Is Nothing Then
                WebFct.PointeurUtilisateur.PointeurDllRTT.Identifiant(WsIde_Dossier) = _
                        WebFct.PointeurDossier(WsIde_Dossier).V_ListeDesFiches( _
                            WebFct.PointeurUtilisateur.PointeurDllRTT.ObjetsDllARTT)

                WsDossierRtt = WebFct.PointeurUtilisateur.PointeurDllRTT.ItemDossier(WsIde_Dossier)
            End If
        End Set
    End Property

    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        WebFct = New Virtualia.Net.WebFonctions(Me, 0)
        ListeGrille.Centrage_Colonne(0) = 0
        ListeGrille.Centrage_Colonne(1) = 1
        ListeGrille.Centrage_Colonne(2) = 1
        ListeGrille.Centrage_Colonne(3) = 1
        ListeGrille.Centrage_Colonne(4) = 1
        ListeGrille.Centrage_Colonne(5) = 1
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        If Me.ViewState(WsNomStateParam) Is Nothing Then
            Call FaireListeAnnees()
        End If
        Call AfficherTableau()
    End Sub

    Private Sub LstAnnee_ValeurChange(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.DonneeChangeEventArgs) Handles LstAnnee.ValeurChange
        If Me.ViewState(WsNomStateParam) IsNot Nothing Then
            Me.ViewState.Remove(WsNomStateParam)
        End If
        '** MettreEnCache les paramètres **
        WsCacheParam = New ArrayList
        WsCacheParam.Add(e.Valeur)
        If e.Valeur = Strings.Right(WebFct.ViRhDates.DateduJour(False), 4) Then
            WsCacheParam.Add(CInt(Strings.Mid(WebFct.ViRhDates.DateduJour(False), 4, 2)))
        Else
            WsCacheParam.Add(12)
        End If
        Me.ViewState.Add(WsNomStateParam, WsCacheParam)
        Call FaireListeMois(CInt(WsCacheParam(1)))
    End Sub

    Private Sub LstLibelMois_ValeurChange(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.DonneeChangeEventArgs) Handles LstLibelMois.ValeurChange
        Dim Annee As Integer
        Dim Mois As Integer

        Mois = WebFct.ViRhDates.IndexduMois(e.Valeur)
        If Me.ViewState(WsNomStateParam) IsNot Nothing Then
            WsCacheParam = CType(Me.ViewState(WsNomStateParam), ArrayList)
            Annee = CInt(WsCacheParam(0))
            Me.ViewState.Remove(WsNomStateParam)
        End If
        '** MettreEnCache les paramètres **
        WsCacheParam = New ArrayList
        WsCacheParam.Add(Annee)
        WsCacheParam.Add(Mois)
        Me.ViewState.Add(WsNomStateParam, WsCacheParam)
    End Sub

    Private Sub AfficherTableau()
        Dim RetourChariot As String = Chr(13) & Chr(10)
        Dim Annee As Integer
        Dim MoisAffiche As Integer
        Dim MoisFin As Integer
        Dim IndiceM As Integer

        '** A et B
        Ligne01_02.Initialiser(0)
        Ligne01_02.VText_Ligne(0) = "A. Report du reliquat débit / crédit"
        Ligne01_02.VTooltip_Ligne(0) = "Reliquat débit / crédit à reporter du mois précédent." & RetourChariot & "Ligne F. du mois -1"

        Ligne01_02.Initialiser(1)
        Ligne01_02.VText_Ligne(1) = "B. Crédit d'heures récupérées"
        Ligne01_02.VTooltip_Ligne(1) = "Récupération de crédit d'heures du mois." & RetourChariot & "Equivalent en heures de la ligne L."

        '** C et D
        Ligne03_04.Initialiser(0)
        Ligne03_04.VText_Ligne(0) = "C. Débit / crédit du mois en cours"
        Ligne03_04.VTooltip_Ligne(0) = "Total des heures créditrices du mois hors récupérations."

        Ligne03_04.Initialiser(1)
        Ligne03_04.VText_Ligne(1) = "D. Débit / crédit du mois en cours écrété"
        Ligne03_04.VTooltip_Ligne(1) = "Ligne C écrétée en fonction de la règle d'écrétage de l'accord temmps de travail"

        '** E et F
        Ligne05_06.Initialiser(0)
        Ligne05_06.VText_Ligne(0) = "E. Total débit / crédit"
        Ligne05_06.VTooltip_Ligne(0) = "Total des heures créditrices du mois tenant compte du report et des récupérations prises." & RetourChariot & "Ligne A. + Ligne D. - Ligne B."

        Ligne05_06.Initialiser(1)
        Ligne05_06.VText_Ligne(1) = "F. Reliquat débit / crédit à reporter"
        Ligne05_06.VTooltip_Ligne(1) = "Débit ou crédit à reporter sur le mois suivant." & RetourChariot & "Ligne E. écrétée en fonction du plafonds d'acquisition de l'accord temps de travail."

        '** G
        Ligne07_08.Initialiser(0)

        Ligne07_08.Initialiser(1)
        Ligne07_08.VText_Ligne(1) = "G. Heures non reportées"
        Ligne07_08.VTooltip_Ligne(1) = "Total des heures supèrieures à l'écrètement." & RetourChariot & "Ligne A. - Ligne B. + Ligne C. - Ligne F."

        '** H
        Ligne09_10.Initialiser(0)
        Ligne09_10.VText_Ligne(0) = "H. Cumul des heures non reportées"
        Ligne09_10.VTooltip_Ligne(0) = "Cumul de la ligne G. depuis le début de l'année."

        Ligne09_10.Initialiser(1)

        '** I et J
        Ligne11_12.Initialiser(0)
        Ligne11_12.VText_Ligne(0) = "I. Crédit d'heures du mois récupérables."
        Ligne11_12.VTooltip_Ligne(0) = "Crédit d'heures ouvrant droit à récupération."

        Ligne11_12.Initialiser(1)
        Ligne11_12.VText_Ligne(1) = "J. Crédit d'heures en équivalent jours."
        Ligne11_12.VTooltip_Ligne(1) = "Ligne I. en jours"

        '** K et L
        Ligne13_14.Initialiser(0)
        Ligne13_14.VText_Ligne(0) = "K. Cumul Crédit d'heures récupérables."
        Ligne13_14.VTooltip_Ligne(0) = "Total des jours générés en récupération." & RetourChariot & "Cumul de la ligne J. depuis le début de l'année."

        Ligne13_14.Initialiser(1)
        Ligne13_14.VText_Ligne(1) = "L. Récupérations"
        Ligne13_14.VTooltip_Ligne(1) = "Nombre de jours de récupération de crédit d'heures pris." & RetourChariot & "Equivalent en jours de la ligne B."

        '** M
        Ligne15_16.Initialiser(0)
        Ligne15_16.VText_Ligne(0) = "M. Solde Crédit d'heures"
        Ligne15_16.VTooltip_Ligne(0) = "Nombre de jours de crédit d'heures restant à prendre." & RetourChariot & "Ligne K. - Cumul(Ligne L.)"

        Ligne15_16.Initialiser(1)
        Ligne15_16.SiLigneVisible(1) = False

        '*********************************************************************************************
        If Me.ViewState(WsNomStateParam) Is Nothing Then
            Annee = CInt(Strings.Right(WebFct.ViRhDates.DateduJour(False), 4))

            WsCacheParam = New ArrayList
            WsCacheParam.Add(Annee)
            MoisFin = CInt(Strings.Mid(WebFct.ViRhDates.DateduJour(False), 4, 2))
            WsCacheParam.Add(MoisFin)
            Me.ViewState.Add(WsNomStateParam, WsCacheParam)
            LstAnnee.LstText = Annee.ToString
            MoisAffiche = MoisFin
        Else
            WsCacheParam = CType(Me.ViewState(WsNomStateParam), ArrayList)
            Annee = CInt(WsCacheParam(0))
            MoisAffiche = CInt(WsCacheParam(1))
            MoisFin = 12
        End If

        If Identifiant > 0 Then
            WsDossierRtt = WebFct.PointeurUtilisateur.PointeurDllRTT.ItemDossier(WsIde_Dossier)
        Else
            Exit Sub
        End If
        If WsDossierRtt Is Nothing Then
            Exit Sub
        End If

        For IndiceM = 1 To MoisFin
            Ligne01_02.VText_Colonne(0, IndiceM) = WsDossierRtt.Experte_DebitCredit(Annee, IndiceM, "A")
            Ligne01_02.VText_Colonne(1, IndiceM) = WsDossierRtt.Experte_DebitCredit(Annee, IndiceM, "B")

            Ligne03_04.VText_Colonne(0, IndiceM) = WsDossierRtt.Experte_DebitCredit(Annee, IndiceM, "C")
            Ligne03_04.VText_Colonne(1, IndiceM) = WsDossierRtt.Experte_DebitCredit(Annee, IndiceM, "D")

            Ligne05_06.VText_Colonne(0, IndiceM) = WsDossierRtt.Experte_DebitCredit(Annee, IndiceM, "E")
            Ligne05_06.VText_Colonne(1, IndiceM) = WsDossierRtt.Experte_DebitCredit(Annee, IndiceM, "F")

            Ligne07_08.VText_Colonne(1, IndiceM) = WsDossierRtt.Experte_DebitCredit(Annee, IndiceM, "G")

            Ligne09_10.VText_Colonne(0, IndiceM) = WsDossierRtt.Experte_DebitCredit(Annee, IndiceM, "H")

            Ligne11_12.VText_Colonne(0, IndiceM) = WsDossierRtt.Experte_DebitCredit(Annee, IndiceM, "I")
            Ligne11_12.VText_Colonne(1, IndiceM) = WsDossierRtt.Experte_DebitCredit(Annee, IndiceM, "J")

            Ligne13_14.VText_Colonne(0, IndiceM) = WsDossierRtt.Experte_DebitCredit(Annee, IndiceM, "K")
            Ligne13_14.VText_Colonne(1, IndiceM) = WsDossierRtt.Experte_DebitCredit(Annee, IndiceM, "L")

            Ligne15_16.VText_Colonne(0, IndiceM) = WsDossierRtt.Experte_DebitCredit(Annee, IndiceM, "M")

        Next IndiceM

        '** Mensuel
        Dim PlanningRtt As Virtualia.Metier.TempsTravail.PlanningIndividuel
        Dim ChaineW As String = ""
        Dim DateW As String = "01/" & Strings.Format(MoisAffiche, "00") & "/" & Annee.ToString
        Dim DateFin As String = WebFct.ViRhDates.DateSaisieVerifiee("31/" & Strings.Format(MoisAffiche, "00") & "/" & Annee.ToString)
        WsCumSolde = 0
        WsTotalConstatee = 0
        WsTotalRecup = 0
        WsTotalCredit = 0
        WsTotalDebit = 0

        PlanningRtt = WsDossierRtt.PlanningIndividuel(DateW, DateFin)
        WsListeVues = PlanningRtt.ListedesVues(DateW, DateFin)

        Do 'Traitement des jours du mois
            ChaineW &= ChaineJour(DateW) & VI.SigneBarre

            DateW = WebFct.ViRhDates.CalcDateMoinsJour(DateW, "1", "0")
            Select Case WebFct.ViRhDates.ComparerDates(DateW, DateFin)
                Case VI.ComparaisonDates.PlusGrand
                    Exit Do
            End Select
        Loop
        ChaineW &= VI.Tild & VI.Tild & VI.Tild & VI.Tild & VI.Tild & VI.Tild & MoisAffiche & VI.SigneBarre 'Ligne blanche

        ChaineW &= "Total du mois" & VI.Tild & WebFct.ViRhDates.CalcHeure(WsTotalConstatee.ToString, "", 2) & VI.Tild
        ChaineW &= WebFct.ViRhDates.CalcHeure(WsTotalCredit.ToString, "", 2) & VI.Tild
        ChaineW &= WebFct.ViRhDates.CalcHeure(WsTotalDebit.ToString, "", 2) & VI.Tild
        If WsCumSolde >= 0 Then
            ChaineW &= VI.Tild & WebFct.ViRhDates.CalcHeure(WsCumSolde.ToString, "", 2) & VI.Tild
        Else
            ChaineW &= VI.Tild & "-" & WebFct.ViRhDates.CalcHeure((-WsCumSolde).ToString, "", 2) & VI.Tild
        End If
        ChaineW &= "Total" & VI.SigneBarre

        ChaineW &= VI.Tild & VI.Tild & VI.Tild & VI.Tild & VI.Tild & VI.Tild & MoisAffiche & VI.SigneBarre 'Ligne blanche

        ChaineW &= "Total hors récupérations (Ligne C.)" & VI.Tild & WebFct.ViRhDates.CalcHeure(WsTotalConstatee.ToString, "", 2) & VI.Tild
        ChaineW &= WebFct.ViRhDates.CalcHeure(WsTotalCredit.ToString, "", 2) & VI.Tild
        If WsTotalDebit - WsTotalRecup >= 0 Then
            ChaineW &= WebFct.ViRhDates.CalcHeure((WsTotalDebit - WsTotalRecup).ToString, "", 2) & VI.Tild
        Else
            ChaineW &= "-" & WebFct.ViRhDates.CalcHeure((-(WsTotalDebit - WsTotalRecup)).ToString, "", 2) & VI.Tild
        End If
        If WsCumSolde + WsTotalRecup >= 0 Then
            ChaineW &= VI.Tild & WebFct.ViRhDates.CalcHeure((WsCumSolde + WsTotalRecup).ToString, "", 2) & VI.Tild
        Else
            ChaineW &= VI.Tild & "-" & WebFct.ViRhDates.CalcHeure((-(WsCumSolde + WsTotalRecup)).ToString, "", 2) & VI.Tild
        End If
        ChaineW &= "TotalRecup" & VI.SigneBarre

        ListeGrille.V_Liste(WsColonneGrid, WsLibelGrid) = ChaineW
    End Sub

    Private ReadOnly Property ChaineJour(ByVal DateValeur As String) As String
        Get
            Dim FicheVue As Virtualia.TablesObjet.ShemaVUE.VUE_JourTravail
            Dim Ligne As String = WebFct.ViRhDates.ClairDate(DateValeur, True) & VI.Tild
            Dim SiComptabilise As Boolean = False
            Dim IndiceP As Integer

            FicheVue = WsListeVues.Find(Function(Recherche) Recherche.Date_de_Valeur = DateValeur)

            If FicheVue Is Nothing Then
                Return Ligne & VI.Tild & VI.Tild & VI.Tild & VI.Tild & VI.Tild & DateValeur
            End If

            For IndiceP = 0 To FicheVue.NombredeFiches - 1
                Select Case FicheVue.ItemPlage(IndiceP).NumeroPlage
                    Case VI.NumeroPlage.Jour_Absence
                        If FicheVue.ItemPlage(IndiceP).EvenementPlage = "Récupération de crédit d'heures" Then
                            WsTotalRecup += -FicheVue.ItemPlage(IndiceP).DebitCredit_Minutes
                        End If
                    Case VI.NumeroPlage.Plage1_Absence, VI.NumeroPlage.Plage2_Absence
                        If FicheVue.ItemPlage(IndiceP).EvenementPlage = "Récupération de crédit d'heures" Then
                            WsTotalRecup += -FicheVue.ItemPlage(IndiceP).DebitCredit_Minutes
                        End If
                    Case VI.NumeroPlage.Jour_Formation, VI.NumeroPlage.Jour_Mission
                        SiComptabilise = True
                        Exit For
                    Case VI.NumeroPlage.Plage1_Formation, VI.NumeroPlage.Plage2_Formation
                        SiComptabilise = True
                        Exit For
                    Case VI.NumeroPlage.Plage1_Mission, VI.NumeroPlage.Plage2_Mission
                        SiComptabilise = True
                        Exit For
                End Select
            Next IndiceP

            If SiComptabilise = True Then
                Ligne &= FicheVue.DureeComptabilisee & VI.Tild
                WsTotalConstatee += FicheVue.DureeComptabiliseeEnMinutes
            Else
                Ligne &= FicheVue.DureeConstatee & VI.Tild
                WsTotalConstatee += FicheVue.DureeConstateeEnMinutes
            End If
            WsCumSolde += FicheVue.DebitCredit_Minutes
            Select Case FicheVue.DebitCredit_Minutes
                Case Is = 0
                    Ligne &= VI.Tild & VI.Tild & VI.Tild
                Case Is > 0
                    Ligne &= FicheVue.DebitCredit & VI.Tild & VI.Tild & FicheVue.DebitCredit & VI.Tild
                    WsTotalCredit += FicheVue.DebitCredit_Minutes
                Case Is < 0
                    Ligne &= VI.Tild & Strings.Right(FicheVue.DebitCredit, FicheVue.DebitCredit.Length - 1) & VI.Tild & FicheVue.DebitCredit & VI.Tild
                    WsTotalDebit += -FicheVue.DebitCredit_Minutes
            End Select
            Select Case WsCumSolde
                Case Is = 0
                    Ligne &= VI.Tild
                Case Is > 0
                    Ligne &= WebFct.ViRhDates.CalcHeure(WsCumSolde.ToString, "", 2) & VI.Tild
                Case Is < 0
                    Ligne &= "-" & WebFct.ViRhDates.CalcHeure((-WsCumSolde).ToString, "", 2) & VI.Tild
            End Select
            Ligne &= DateValeur

            Return Ligne
        End Get
    End Property

    Private Sub FaireListeAnnees()
        Dim Chaine As New System.Text.StringBuilder
        Dim Libel As String = "Aucune possibilité" & VI.Tild & "Une possibilité" & VI.Tild & "possibilités"
        Dim Annee As Integer

        Annee = CInt(Strings.Right(WebFct.ViRhDates.DateduJour(False), 4))
        Chaine.Append(Annee.ToString & VI.Tild)
        Chaine.Append((Annee - 1).ToString & VI.Tild)
        LstAnnee.V_Liste(Libel) = Chaine.ToString
        Chaine.Clear()
        LstAnnee.LstText = Annee.ToString

        Call FaireListeMois(CInt(Strings.Mid(WebFct.ViRhDates.DateduJour(False), 4, 2)))
    End Sub

    Private Sub FaireListeMois(ByVal NbMois As Integer)
        Dim Chaine As New System.Text.StringBuilder
        Dim Libel As String = "Aucun mois" & VI.Tild & "Un mois" & VI.Tild & "moiss"
        Dim IndiceI As Integer

        For IndiceI = 1 To NbMois
            Chaine.Append(WebFct.ViRhDates.MoisEnClair(CShort(IndiceI)) & VI.Tild)
        Next IndiceI

        LstLibelMois.V_Liste(Libel) = Chaine.ToString
        Chaine.Clear()
        LstLibelMois.LstText = WebFct.ViRhDates.MoisEnClair(CShort(NbMois))
    End Sub

End Class
