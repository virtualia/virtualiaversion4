﻿Option Strict On
Option Explicit On
Option Compare Text

Imports VI = Virtualia.Systeme.Constantes
Imports Virtualia.ObjetBase
Imports Virtualia.TablesObjet.ShemaPER
Imports Virtualia.OutilsVisu


Partial Class Fenetre_PER_ABSENCE
    Inherits ObjetWebControleBase(Of PER_ABSENCE)
    Implements IControlCrud

#Region "IControlCrud"
    Public ReadOnly Property ControlCrud As ICtlCommandeCrud Implements IControlCrud.CtlCrud
        Get
            Return Me.CtlCrud
        End Get
    End Property
#End Region

#Region "ObjetWebControleBase"
    Private _webfonction As WebFonctions
    Protected Overrides ReadOnly Property V_WebFonction As IWebFonctions
        Get
            If (_webfonction Is Nothing) Then
                _webfonction = New WebFonctions(Me)
            End If
            Return _webfonction
        End Get
    End Property

    Protected Overrides Function InitCadreInfo() As WebControl
        Return Me.CadreInfo
    End Function

    Protected Overrides Function InitControlDataGrid() As IControlDataGrid

        Me.ListeGrille.Act_InitFiltre = Sub(ide)
                                            Me.ListeGrille.InitFiltre(_webfonction.PointeurDossier(ide).ListeDesNaturesAbsence)
                                        End Sub


        Return Me.ListeGrille
    End Function

    Protected Overrides Sub InitCacheIde()
        V_CacheIde.TypeVue = WebControlTypeVue.ListeGrid

        Dim captions As List(Of String) = New List(Of String)()
        captions.Add("Aucune abscence")
        captions.Add("Une abscence")
        captions.Add("abscences")

        Me.ListeGrille.InitCaptions(captions)

        Dim colonnes As ColonneDataGridCollection = New ColonneDataGridCollection()
        colonnes.Add(HorizontalAlign.Center, TypeData.Date, "date d'effet", 0)
        colonnes.Add(HorizontalAlign.Center, TypeData.Numeric, "nature", 1)
        colonnes.Add(HorizontalAlign.Center, TypeData.Numeric, "Nb jours calendaires", 2)
        colonnes.Add(HorizontalAlign.Center, TypeData.Numeric, "Nb jours ouvrés", 3)
        colonnes.Add(HorizontalAlign.Center, TypeData.Numeric, "date de fin", 99)
        colonnes.Add(HorizontalAlign.Center, TypeData.Chaine, "Clef", 0, True)

        Me.ListeGrille.InitColonnes(colonnes)

        Me.ListeGrille.Act_InitFiltre = AddressOf Me.InitFiltreDeListe
    End Sub
#End Region

#Region "Specifique"
    Private Const DATABS_DEBUTINCLUS = "Date de début incluse"
    Private Const DATABS_DEBUTAPRESMIDI = "Date de début après-midi"
    Private Const DATABS_FININCLUS = "Date de fin incluse"
    Private Const DATABS_FINMATIN = "Date de fin matin"

    Private _CadreExpertStyle As String = ""
    Public WriteOnly Property CadreExpertStyle As String
        Set(ByVal value As String)
            _CadreExpertStyle = value
        End Set
    End Property

    Protected Overrides Sub LireLaFiche()
        MyBase.LireLaFiche()

        LabelCarence.Visible = False
        LabelCarence.Text = ""
        LabelDateSaisie.Text = ""
        CheckAbsenceDebut.Checked = True
        CheckAbsenceFin.Checked = True

        If V_CacheMaj.Count <= 0 Then
            Return
        End If

        If V_CacheMaj(9).valeur = "" Then
            Exit Sub
        End If
        
        LabelDateSaisie.Text = V_CacheMaj(18).valeur
        Select Case V_CacheMaj(14).valeur
            Case DATABS_DEBUTINCLUS & " - " & DATABS_FININCLUS
                CheckAbsenceDebut.Checked = True
                CheckAbsenceDebut.Text = "Inclus"
                CheckAbsenceFin.Checked = True
                CheckAbsenceFin.Text = "Inclus"
            Case DATABS_DEBUTAPRESMIDI & " - " & DATABS_FININCLUS
                CheckAbsenceDebut.Checked = False
                CheckAbsenceDebut.Text = "Après-midi"
                CheckAbsenceFin.Checked = True
                CheckAbsenceFin.Text = "Inclus"
            Case DATABS_DEBUTAPRESMIDI & " - " & DATABS_FINMATIN
                CheckAbsenceDebut.Checked = False
                CheckAbsenceDebut.Text = "Après-midi"
                CheckAbsenceFin.Checked = False
                CheckAbsenceFin.Text = "Matin"
            Case DATABS_DEBUTINCLUS & " - " & DATABS_FINMATIN
                CheckAbsenceDebut.Checked = True
                CheckAbsenceDebut.Text = "Inclus"
                CheckAbsenceFin.Checked = False
                CheckAbsenceFin.Text = "Matin"
        End Select

        Dim SiNatureMaladie As Boolean = False

        If V_CacheMaj(1).valeur.Contains("Maladie") Then
            SiNatureMaladie = True
        End If

        If (SiNatureMaladie) Then
            If V_CacheMaj(1).valeur.Contains("Longue") Or V_CacheMaj(1).valeur.Contains("Grave") Then
                SiNatureMaladie = False
            End If
        End If

        If SiNatureMaladie Then
            LabelCarence.Visible = True
            If CDate(V_CacheMaj(0).valeur) > CDate("01/01/2012") Then
                If V_CacheMaj(16).valeur = "Oui" Then
                    LabelCarence.Text = "sans jour de carence"
                Else
                    LabelCarence.Text = "dont 1 jour de carence"
                End If
            End If
        End If

    End Sub

    Private Sub InitFiltreDeListe(ide As Integer)
        If (ide <= 0) Then
            Return
        End If

        Dim libelles As String = "Aucune situation" & VI.Tild & "Une situation" & VI.Tild & "situations"

        ListeGrille.InitFiltre(_webfonction.PointeurDossier(ide).ListeDesNaturesAbsence)
    End Sub

    Protected Overrides Sub OnInit(e As System.EventArgs)
        MyBase.OnInit(e)

        Dim initwebfonction As IWebFonctions = V_WebFonction
    End Sub

    Protected Overrides Sub OnPreRender(ByVal e As System.EventArgs)
        MyBase.OnPreRender(e)

        InitStyleCadre(Me.CadreExpert, _CadreExpertStyle)

        CadreInfo.BackColor = _webfonction.CouleurCharte(V_CacheIde.PointDeVue, "Cadre")
        CadreInfo.BorderColor = _webfonction.CouleurCharte(V_CacheIde.PointDeVue, "Bordure")
        Etiquette.BackColor = _webfonction.CouleurCharte(V_CacheIde.PointDeVue, "Titre")
        Etiquette.ForeColor = _webfonction.CouleurCharte(V_CacheIde.PointDeVue, "Police_Claire")
        Etiquette.BorderColor = _webfonction.CouleurCharte(V_CacheIde.PointDeVue, "Bordure")
        LabelAccident.BackColor = _webfonction.CouleurCharte(V_CacheIde.PointDeVue, "Sous-Titre")
        LabelAccident.ForeColor = _webfonction.CouleurCharte(V_CacheIde.PointDeVue, "Etiquette_ForeColor")
        LabelAccident.BorderColor = _webfonction.CouleurCharte(V_CacheIde.PointDeVue, "Bordure")
        LabelDateSaisie.BackColor = _webfonction.CouleurCharte(V_CacheIde.PointDeVue, "Etiquette_BackColor")
        LabelDateSaisie.ForeColor = _webfonction.CouleurCharte(V_CacheIde.PointDeVue, "Etiquette_ForeColor")
        LabelDateSaisie.BorderColor = _webfonction.CouleurCharte(V_CacheIde.PointDeVue, "Bordure")
        LabelDuree.BackColor = _webfonction.CouleurCharte(V_CacheIde.PointDeVue, "Etiquette_BackColor")
        LabelDuree.ForeColor = _webfonction.CouleurCharte(V_CacheIde.PointDeVue, "Etiquette_ForeColor")
        LabelDuree.BorderColor = _webfonction.CouleurCharte(V_CacheIde.PointDeVue, "Bordure")
        LabelJourDT.ForeColor = _webfonction.CouleurCharte(V_CacheIde.PointDeVue, "Etiquette_BackColor")
        LabelJourPT.ForeColor = _webfonction.CouleurCharte(V_CacheIde.PointDeVue, "Etiquette_BackColor")
        LabelMaladie.BackColor = _webfonction.CouleurCharte(V_CacheIde.PointDeVue, "Sous-Titre")
        LabelMaladie.ForeColor = _webfonction.CouleurCharte(V_CacheIde.PointDeVue, "Etiquette_ForeColor")
        LabelMaladie.BorderColor = _webfonction.CouleurCharte(V_CacheIde.PointDeVue, "Bordure")
        LabelNbjCalendaires.ForeColor = _webfonction.CouleurCharte(V_CacheIde.PointDeVue, "Etiquette_BackColor")
        LabelNbjOuvrables.ForeColor = _webfonction.CouleurCharte(V_CacheIde.PointDeVue, "Etiquette_BackColor")
        LabelNbjOuvres.ForeColor = _webfonction.CouleurCharte(V_CacheIde.PointDeVue, "Etiquette_BackColor")
        LabelNbjTravailles.ForeColor = _webfonction.CouleurCharte(V_CacheIde.PointDeVue, "Etiquette_BackColor")
        LabelRemuneration.BackColor = _webfonction.CouleurCharte(V_CacheIde.PointDeVue, "Etiquette_BackColor")
        LabelRemuneration.ForeColor = _webfonction.CouleurCharte(V_CacheIde.PointDeVue, "Etiquette_ForeColor")
        LabelRemuneration.BorderColor = _webfonction.CouleurCharte(V_CacheIde.PointDeVue, "Bordure")
        CheckAbsenceDebut.BackColor = _webfonction.CouleurCharte(V_CacheIde.PointDeVue, "Etiquette_BackColor")
        CheckAbsenceDebut.ForeColor = _webfonction.CouleurCharte(V_CacheIde.PointDeVue, "Etiquette_ForeColor")
        CheckAbsenceDebut.BorderColor = _webfonction.CouleurCharte(V_CacheIde.PointDeVue, "Bordure")
        CheckAbsenceFin.BackColor = _webfonction.CouleurCharte(V_CacheIde.PointDeVue, "Etiquette_BackColor")
        CheckAbsenceFin.ForeColor = _webfonction.CouleurCharte(V_CacheIde.PointDeVue, "Etiquette_ForeColor")
        CheckAbsenceFin.BorderColor = _webfonction.CouleurCharte(V_CacheIde.PointDeVue, "Bordure")

    End Sub

#End Region

End Class
