﻿<%@ Control Language="VB" AutoEventWireup="false" Inherits="Virtualia.Net.Fenetre_PER_CET_MAJ" Codebehind="PER_CET_MAJ.ascx.vb" %>

 <asp:Table ID="CadreInfo" runat="server" BorderStyle="None" BorderWidth="2px" Visible="true" Height="90px"
    BorderColor="#B0E0D7" Width="630px" HorizontalAlign="Center" style="margin-top: 1px;">
    <asp:TableRow>
      <asp:TableCell HorizontalAlign="Center">
            <asp:Table ID="CadreTitreSoldeConges" runat="server" CellPadding="0" Width="630px" Height="35px" BackColor="Transparent"
            CellSpacing="0" HorizontalAlign="Center" BorderStyle="None" BorderWidth="1px" Visible="true" BorderColor="#9EB0AC"
            style="margin-top: 1px;margin-bottom: 0px;">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                       <asp:Label ID="LabelTitreSoldeConges" runat="server" Height="30px" Width="630px"
                            BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" Text="Solde des congés de l'année précédente"
                            BorderWidth="1px" ForeColor="#6D9092" Font-Italic="False"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Medium"
                            style="margin-top: 20px; margin-left: 0px; margin-bottom: 10px;
                            font-style: normal; text-indent: 1px; text-align:  center;">
                       </asp:Label>      
                    </asp:TableCell>   
                </asp:TableRow>
            </asp:Table>
      </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
      <asp:TableCell HorizontalAlign="Center">
            <asp:Table ID="TableSoldeCP" runat="server" CellPadding="0" Width="340px" Height="21px"
            CellSpacing="0" HorizontalAlign="Center" BorderStyle="Notset" BorderWidth="1px" Visible="true" BorderColor="#9EB0AC"
            style="margin-top: -5px;margin-bottom: 0px;">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center" Height="21px" BackColor= "#E2F5F1">
                       <asp:Label ID="LabelTitreSoldeCP" runat="server" Height="21px" Width="251px"
                                BackColor= "#E2F5F1" BorderColor="#9EB0AC" BorderStyle="None" Text="Total au titre des congés annuels"
                                BorderWidth="1px" ForeColor="#216B68" Font-Italic="False"
                                Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px; margin-right: 10px;
                                font-style: normal; text-indent: 1px; text-align:  right;">
                       </asp:Label>      
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center" Height="21px">
                       <asp:Label ID="LabelSoldeCP" runat="server" Height="21px" Width="89px"
                                BackColor="White" BorderColor="Transparent" BorderStyle="None" Text="10,0"
                                BorderWidth="1px" ForeColor="#1C5151" Font-Italic="False"
                                Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                font-style: normal; text-indent: 1px; text-align:  center;">
                       </asp:Label>      
                    </asp:TableCell>   
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center" Height="21px" BackColor= "#E2F5F1">
                       <asp:Label ID="LabelTitreSoldeRTT" runat="server" Height="21px" Width="251px"
                                BackColor= "#E2F5F1" BorderColor="#9EB0AC" BorderStyle="None" Text="Total au titre de la RTT"
                                BorderWidth="1px" ForeColor="#216B68" Font-Italic="False"
                                Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px; margin-right: 10px;
                                font-style: normal; text-indent: 1px; text-align:  right;">
                       </asp:Label>      
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center" Height="21px">
                       <asp:Label ID="LabelSoldeRTT" runat="server" Height="21px" Width="89px"
                                BackColor="White" BorderColor="Transparent" BorderStyle="None" Text="2,0"
                                BorderWidth="1px" ForeColor="#1C5151" Font-Italic="False"
                                Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                font-style: normal; text-indent: 1px; text-align:  center;">
                       </asp:Label>      
                    </asp:TableCell>   
                </asp:TableRow>
            </asp:Table>
      </asp:TableCell>
    </asp:TableRow>    
    <asp:TableRow>
      <asp:TableCell Height="30px"></asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
      <asp:TableCell HorizontalAlign="Center">
            <asp:Table ID="CadreMajCET" runat="server" CellPadding="0" Width="630px" Height="35px" BackColor="Transparent"
            CellSpacing="0" HorizontalAlign="Center" BorderStyle="None" BorderWidth="1px" Visible="true" BorderColor="#9EB0AC"
            style="margin-top: 1px;margin-bottom: 0px;">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center" ColumnSpan="3">
                       <asp:Label ID="LabelTitreMajCET" runat="server" Height="30px" Width="630px"
                                BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" Text="Mise à jour du compte épargne temps"
                                BorderWidth="1px" ForeColor="#6D9092" Font-Italic="False"
                                Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Medium"
                                style="margin-top: 0px; margin-left: 0px; margin-bottom: 15px;
                                font-style: normal; text-indent: 1px; text-align:  center;">
                       </asp:Label>      
                    </asp:TableCell>   
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center" ColumnSpan="3">
                       <asp:Label ID="LabelMajCET1" runat="server" Height="20px" Width="630px"
                                BackColor="Transparent" BorderColor="Transparent" BorderStyle="None"
                                Text="Je, sousigné(e) DUPONT Jean, demande l'alimentation de mon compte épargne temps,"
                                BorderWidth="1px" ForeColor="#216B68" Font-Italic="False"
                                Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Small" 
                                style="margin-top: 0px; margin-left: 0px; margin-bottom: 1px;
                                font-style: oblique; text-indent: 1px; text-align:  center;">
                       </asp:Label>      
                    </asp:TableCell>          
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center" ColumnSpan="3">
                       <asp:Label ID="LabelMajCET2" runat="server" Height="20px" Width="630px"
                                BackColor="Transparent" BorderColor="Transparent" BorderStyle="None"
                                Text="au titre de l'année écoulée à raison de :"
                                BorderWidth="1px" ForeColor="#216B68" Font-Italic="False"
                                Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Small" 
                                style="margin-top: 0px; margin-left: 0px; margin-bottom: 15px;
                                font-style: oblique; text-indent: 1px; text-align:  center;">
                       </asp:Label>      
                    </asp:TableCell>          
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelEpargneCP" runat="server" Height="20px" Width="150px"
                                BackColor="#B0E0D7" BorderColor="#B0E0D7" BorderStyle="Outset"
                                BorderWidth="2px" ForeColor="#142425" Font-Italic="true"
                                Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                Text="Congés annuels"
                                style="margin-top: 0px; margin-left: 200px; text-indent: 5px; text-align: left">
                        </asp:Label>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:TextBox ID="EpargneCP" runat="server" Height="16px" Width="40px" MaxLength="0"
                                BackColor="White" BorderColor="#B0E0D7"  BorderStyle="Inset"
                                BorderWidth="2px" ForeColor="Black" Visible="true" AutoPostBack="false"
                                Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Small" Font-Italic="false"
                                style="margin-top: 1px; text-indent: 1px; text-align: center" TextMode="SingleLine"> 
                        </asp:TextBox>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="EtiNbjoursCP" runat="server" Height="20px" Width="220px" Text="jours"
                                BackColor="Transparent" BorderColor="Transparent" BorderStyle="None"
                                ForeColor="#7EC8BE" Font-Italic="true"
                                Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                style="margin-top: 1px; margin-left: 1px; text-indent: 2px; text-align: left">
                        </asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="4px" ColumnSpan="3"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LabelEpargneRTT" runat="server" Height="20px" Width="150px"
                                BackColor="#B0E0D7" BorderColor="#B0E0D7" BorderStyle="Outset"
                                BorderWidth="2px" ForeColor="#142425" Font-Italic="true"
                                Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                Text="RTT"
                                style="margin-top: 0px; margin-left: 200px; text-indent: 5px; text-align: left">
                        </asp:Label>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:TextBox ID="EpargneRTT" runat="server" Height="16px" Width="40px" MaxLength="0"
                                BackColor="White" BorderColor="#B0E0D7"  BorderStyle="Inset"
                                BorderWidth="2px" ForeColor="Black" Visible="true" AutoPostBack="false"
                                Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Small" Font-Italic="false"
                                style="margin-top: 1px; text-indent: 1px; text-align: center" TextMode="SingleLine"> 
                        </asp:TextBox>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="EtiNbjoursRTT" runat="server" Height="20px" Width="220px" Text="jours"
                                BackColor="Transparent" BorderColor="Transparent" BorderStyle="None"
                                ForeColor="#7EC8BE" Font-Italic="true"
                                Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                style="margin-top: 1px; margin-left: 1px; text-indent: 2px; text-align: left">
                        </asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="15px" ColumnSpan="3"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell ColumnSpan="3" HorizontalAlign="Center">
                        <asp:Table ID="CadreCmdOK" runat="server" Height="22px" CellPadding="0" 
                          CellSpacing="0" BackImageUrl="~/Images/Boutons/OK_Std.bmp" Visible="true"
                          BorderWidth="2px" BorderStyle="Outset" BorderColor="#B0E0D7" ForeColor="#D7FAF3"
                          Width="70px" HorizontalAlign="Center" style="margin-top: 3px; margin-right:3px">
                            <asp:TableRow>
                              <asp:TableCell VerticalAlign="Bottom">
                                 <asp:Button ID="CommandeOK" runat="server" Text="Valider" Width="65px" Height="20px"
                                     BackColor="Transparent" BorderColor="#B0E0D7" ForeColor="#D7FAF3"
                                     Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Font-Italic="true"
                                     BorderStyle="None" style=" margin-left: 6px; text-align: center;">
                                 </asp:Button>
                              </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
      </asp:TableCell>
    </asp:TableRow>
 </asp:Table>