﻿<%@ Control Language="VB" AutoEventWireup="false" Inherits="Virtualia.Net.Fenetre_PER_PLANNING_JOURNALIER" Codebehind="PER_PLANNING_JOURNALIER.ascx.vb" %>

<asp:Table ID="CadreInfo" runat="server" BorderStyle="None" BorderWidth="2px" Visible="true"
    BorderColor="#B0E0D7" Height="250px" Width="750px" HorizontalAlign="Center" style="margin-top: 3px;">
  <asp:TableRow>
    <asp:TableCell>
      <asp:Table ID="CadreTitre" runat="server" Height="30px" CellPadding="0" Width="750px" 
         CellSpacing="0" HorizontalAlign="Center" BorderColor="Black" Visible="true" BorderStyle="NotSet">
            <asp:TableRow>
                <asp:TableCell HorizontalAlign="Center">
                    <asp:Label ID="Etiquette" runat="server" Text="Planning journalier" Height="20px" Width="320px"
                        BackColor="#216B68" BorderColor="#B0E0D7"  BorderStyle="Groove"
                        BorderWidth="2px" ForeColor="#D7FAF3"
                        Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Small"
                        style="margin-top: 5px; margin-left: 4px; margin-bottom: 20px;
                        font-style: oblique; text-indent: 5px; text-align: center;">
                    </asp:Label>          
                </asp:TableCell>      
            </asp:TableRow>
      </asp:Table>
    </asp:TableCell>
  </asp:TableRow>
  <asp:TableRow>
    <asp:TableCell>
        <asp:Table ID="TableauPlanningDetaille" runat="server" CellPadding="0" CellSpacing="0" Width="750px"
        BorderStyle="Notset" BorderWidth="1px" BorderColor="#1C5151" HorizontalAlign="Left" Visible="true">

            <asp:TableRow ID="LigneDate" Width="746px" Height="25px">
              <asp:TableCell HorizontalAlign="Center" ColumnSpan="9">
                  <asp:Label ID="LabelContrat" runat="server" Text="Lundi 10 mai 2010" 
                      BackColor="#D7FAF3" BorderColor="#B0E0D7" BorderStyle="Notset" Height="25px" Width="746px"
                      BorderWidth="1px" ForeColor="#1C5151"
                      Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Medium"
                      style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                      font-style: normal; text-indent: 0px; text-align: center; padding-top: 0px;">
                  </asp:Label>          
              </asp:TableCell>      
            </asp:TableRow>

            <asp:TableRow ID="LigneEntete" Width="746px" Height="23px" VerticalAlign="Bottom">
             <asp:TableCell ID="CellTitreEntete" BackColor="#A8BBB8" BorderColor="LightGray" BorderStyle="None" HorizontalAlign="Center" BorderWidth="1px"> 
              <asp:Label ID="LabelTitreEntete" runat="server" Height="23px" Width="234px"
                  BackColor="#A8BBB8" BorderColor="LightGray" BorderStyle="Solid" Text="liste"
                  BorderWidth="1px" ForeColor="#A8BBB8" Font-Italic="False"
                  Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                  style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                  font-style: normal; text-indent: 5px; text-align:  center; padding-top: 2px;">
              </asp:Label>          
             </asp:TableCell>
             <asp:TableCell ID="CellMatin" BackColor="#A8BBB8" BorderColor="LightGray" BorderStyle="None" BorderWidth="1px"> 
              <asp:Label ID="LabelMatin" runat="server" Height="23px" Width="71px" 
                  BackColor="#A8BBB8" BorderColor="LightGray" BorderStyle="Solid" Text="Matin"
                  BorderWidth="1px" ForeColor="#D7FAF3" Font-Italic="False"
                  Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Smaller"
                  style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                  font-style: normal; text-indent: 0px;  text-align:  center; padding-top: 2px;">
              </asp:Label>          
             </asp:TableCell>
             <asp:TableCell ID="CellMatinHDebut" BackColor="#A8BBB8" BorderColor="LightGray" BorderStyle="None" BorderWidth="1px"> 
              <asp:Label ID="LabelMatinHDebut" runat="server" Height="23px" Width="55px"
                  BackColor="#A8BBB8" BorderColor="LightGray" BorderStyle="Solid" Text="H.début"
                  BorderWidth="1px" ForeColor="#D7FAF3" Font-Italic="False"
                  Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Smaller"
                  style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                  font-style: normal; text-indent: 0px; text-align:  center; padding-top: 2px;">
              </asp:Label>          
             </asp:TableCell>
             <asp:TableCell ID="CellMatinHFin" BackColor="#A8BBB8" BorderColor="LightGray" BorderStyle="None" BorderWidth="1px"> 
              <asp:Label ID="LabelMatinHFin" runat="server" Height="23px" Width="55px"
                  BackColor="#A8BBB8" BorderColor="LightGray" BorderStyle="Solid" Text="H.fin"
                  BorderWidth="1px" ForeColor="#D7FAF3" Font-Italic="False"
                  Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Smaller"
                  style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                  font-style: normal; text-indent: 0px; text-align:  center; padding-top: 2px;">
              </asp:Label>          
             </asp:TableCell>
             <asp:TableCell ID="CellSeparateur" BackColor="#A8BBB8" BorderColor="LightGray" BorderStyle="None" BorderWidth="1px"> 
              <asp:Label ID="LabelSeparateur" runat="server" Height="23px" Width="61px"
                  BackColor="#A8BBB8" BorderColor="LightGray" BorderStyle="Solid" Text="-"
                  BorderWidth="1px" ForeColor="#A8BBB8" Font-Italic="False"
                  Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Smaller"
                  style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                  font-style: normal; text-indent: 0px; text-align:  center; padding-top: 2px;" >
              </asp:Label>          
             </asp:TableCell>
             <asp:TableCell ID="CellApresmidi" BackColor="#A8BBB8" BorderColor="LightGray" BorderStyle="None" BorderWidth="1px"> 
              <asp:Label ID="LabelApresmidi" runat="server" Height="23px" Width="71px"
                  BackColor="#A8BBB8" BorderColor="LightGray" BorderStyle="Solid" Text="Après-midi"
                  BorderWidth="1px" ForeColor="#D7FAF3" Font-Italic="False"
                  Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Smaller"
                  style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                  font-style: normal; text-indent: 0px; text-align:  center; padding-top: 2px;">
              </asp:Label>          
             </asp:TableCell>
             <asp:TableCell ID="CellApresmidiHDebut" BackColor="#A8BBB8" BorderColor="LightGray" BorderStyle="None" BorderWidth="1px"> 
              <asp:Label ID="LabelApresmidiHDebut" runat="server" Height="23px" Width="56px"
                  BackColor="#A8BBB8" BorderColor="LightGray" BorderStyle="Solid" Text="H.début"
                  BorderWidth="1px" ForeColor="#D7FAF3" Font-Italic="False"
                  Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Smaller"
                  style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                  font-style: normal; text-indent: 0px; text-align:  center; padding-top: 2px;">
              </asp:Label>          
             </asp:TableCell>
             <asp:TableCell ID="CellApresmidiHFin" BackColor="#A8BBB8" BorderColor="LightGray" BorderStyle="None" BorderWidth="1px"> 
              <asp:Label ID="LabelApresmidiHFin" runat="server" Height="23px" Width="55px"
                  BackColor="#A8BBB8" BorderColor="LightGray" BorderStyle="Solid" Text="H.fin"
                  BorderWidth="1px" ForeColor="#D7FAF3" Font-Italic="False"
                  Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Smaller"
                  style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                  font-style: normal; text-indent: 0px; text-align:  center; padding-top: 2px;">
              </asp:Label>          
             </asp:TableCell>
             <asp:TableCell ID="CellTotalJour" BackColor="#A8BBB8" BorderColor="LightGray" BorderStyle="None" BorderWidth="1px"> 
              <asp:Label ID="LabelTotalJour" runat="server" Height="23px" Width="60px"
                  BackColor="#A8BBB8" BorderColor="LightGray" BorderStyle="Solid" Text="Tot.jour"
                  BorderWidth="1px" ForeColor="#D7FAF3" Font-Italic="False"
                  Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Smaller"
                  style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                  font-style: normal; text-indent: 0px; text-align:  center; padding-top: 2px;">
              </asp:Label>          
             </asp:TableCell>      
          </asp:TableRow>
                              
          <asp:TableRow ID="LignePersonne" Width="746px" Height="20px">
            <asp:TableCell ID="CellNomPersonne" BackColor="#f0f0f0" BorderColor="#A8BBB8" BorderStyle="Notset" HorizontalAlign="Center" Height="20px"> 
              <asp:Label ID="LabelPersonne" runat="server" Height="20px" Width="234px"
                  BackColor="#f0f0f0" BorderColor="#A8BBB8" BorderStyle="Solid" Text="ANDRIEU Béatrice"
                  BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                  Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                  style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                  font-style: normal; text-indent: 15px; text-align:  left; padding-top: 1px;">
              </asp:Label>          
            </asp:TableCell>
            <asp:TableCell ID="CellLigneMatin" BackColor="#f0f0f0" BorderColor="#A8BBB8" BorderStyle="Notset" Visible="true"> 
              <asp:Label ID="LigneMatin" runat="server" Height="20px" Width="71px"
                        BackColor="White" BorderColor="#A8BBB8" BorderStyle="Solid" Text="TR"
                        BorderWidth="1px" ForeColor="Black"
                        Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Smaller" Font-Italic="false"
                        style="margin-top: 0px; margin-left: 0px; text-indent: 0px; text-align: center; padding-top: 1px;"> 
              </asp:Label>
            </asp:TableCell>
            <asp:TableCell ID="CellLigneMatinHDebut" BackColor="#f0f0f0" BorderColor="#A8BBB8" BorderStyle="Notset" Visible="true"> 
              <asp:Label ID="LigneMatinHDebut" runat="server" Height="20px" Width="55px"
                        BackColor="White" BorderColor="#A8BBB8" BorderStyle="Solid" Text="08:30"
                        BorderWidth="1px" ForeColor="Black"
                        Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Smaller" Font-Italic="false"
                        style="margin-top: 0px; margin-left: 0px; text-indent: 0px; text-align: center; padding-top: 1px;"> 
              </asp:Label> 
            </asp:TableCell>
            <asp:TableCell ID="CellLigneMatinHFin" BackColor="#f0f0f0" BorderColor="#A8BBB8" BorderStyle="Notset" Visible="true"> 
              <asp:Label ID="LigneMatinHFin" runat="server" Height="20px" Width="55px"
                        BackColor="White" BorderColor="#A8BBB8" BorderStyle="Solid" Text="12:00"
                        BorderWidth="1px" ForeColor="Black"
                        Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Smaller" Font-Italic="false"
                        style="margin-top: 0px; margin-left: 0px; text-indent: 0px; text-align: center; padding-top: 1px;"> 
              </asp:Label>         
            </asp:TableCell>
            <asp:TableCell ID="CellLigneSeparateur" BackColor="#f0f0f0" BorderColor="#A8BBB8" BorderStyle="Notset" Visible="true"> 
              <asp:Label ID="LigneSeparateur" runat="server" Height="20px" Width="61px"
                        BackColor="White" BorderColor="#A8BBB8" BorderStyle="Solid" Text=""
                        BorderWidth="1px" ForeColor="Black"
                        Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Smaller" Font-Italic="false"
                        style="margin-top: 0px; margin-left: 0px; text-indent: 0px; text-align: center; padding-top: 1px;"> 
              </asp:Label>           
            </asp:TableCell>
            <asp:TableCell ID="CellLigneApresMidi" BackColor="#f0f0f0" BorderColor="#A8BBB8" BorderStyle="Notset" Visible="true"> 
              <asp:Label ID="LigneApresMidi" runat="server" Height="20px" Width="71px"
                        BackColor="White" BorderColor="#A8BBB8" BorderStyle="Solid" Text="TR"
                        BorderWidth="1px" ForeColor="Black"
                        Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Smaller" Font-Italic="false"
                        style="margin-top: 0px; margin-left: 0px; text-indent: 0px; text-align: center; padding-top: 1px;"> 
              </asp:Label>           
            </asp:TableCell>
            <asp:TableCell ID="CellLigneApresMidiHDebut" BackColor="#f0f0f0" BorderColor="#A8BBB8" BorderStyle="Notset" Visible="true"> 
              <asp:Label ID="LigneApresMidiHDebut" runat="server" Height="20px" Width="56px"
                        BackColor="White" BorderColor="#A8BBB8" BorderStyle="Solid" Text="14:00"
                        BorderWidth="1px" ForeColor="Black"
                        Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Smaller" Font-Italic="false"
                        style="margin-top: 0px; margin-left: 0px; text-indent: 0px; text-align: center; padding-top: 1px;"> 
              </asp:Label>        
            </asp:TableCell>
            <asp:TableCell ID="CellLigneApresMidiHFin" BackColor="#f0f0f0" BorderColor="#A8BBB8" BorderStyle="Notset" Visible="true"> 
              <asp:Label ID="LigneApresMidiHFin" runat="server" Height="20px" Width="55px"
                        BackColor="White" BorderColor="#A8BBB8" BorderStyle="Solid" Text="17:30"
                        BorderWidth="1px" ForeColor="Black"
                        Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Smaller" Font-Italic="false"
                        style="margin-top: 0px; margin-left: 0px; text-indent: 0px; text-align: center; padding-top: 1px;"> 
              </asp:Label>           
            </asp:TableCell>
            <asp:TableCell ID="CellLigneTotalJour" BackColor="#f0f0f0" BorderColor="#A8BBB8" BorderStyle="Notset" Visible="true"> 
              <asp:Label ID="LigneTotalJour" runat="server" Height="20px" Width="60px"
                        BackColor="White" BorderColor="#A8BBB8" BorderStyle="Solid" Text="7 h 00"
                        BorderWidth="1px" ForeColor="Black"
                        Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Smaller" Font-Italic="false"
                        style="margin-top: 0px; margin-left: 0px; text-indent: 0px; text-align: center; padding-top: 1px;"> 
              </asp:Label>           
            </asp:TableCell>           
          </asp:TableRow>
          <asp:TableRow ID="LignePersonne2" Width="746px" Height="20px">
            <asp:TableCell ID="TableCell1" BackColor="#f0f0f0" BorderColor="#A8BBB8" BorderStyle="Notset" HorizontalAlign="Center" Height="20px"> 
              <asp:Label ID="Label1" runat="server" Height="20px" Width="234px"
                  BackColor="#f0f0f0" BorderColor="#A8BBB8" BorderStyle="Solid" Text="AFILANO Alice"
                  BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                  Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                  style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                  font-style: normal; text-indent: 15px; text-align:  left; padding-top: 1px;">
              </asp:Label>          
            </asp:TableCell>
            <asp:TableCell ID="TableCell2" BackColor="#f0f0f0" BorderColor="#A8BBB8" BorderStyle="Notset" Visible="true"> 
              <asp:Label ID="Label2" runat="server" Height="20px" Width="71px"
                        BackColor="White" BorderColor="#A8BBB8" BorderStyle="Solid" Text="TR"
                        BorderWidth="1px" ForeColor="Black"
                        Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Smaller" Font-Italic="false"
                        style="margin-top: 0px; margin-left: 0px; text-indent: 0px; text-align: center; padding-top: 1px;"> 
              </asp:Label>
            </asp:TableCell>
            <asp:TableCell ID="TableCell3" BackColor="#f0f0f0" BorderColor="#A8BBB8" BorderStyle="Notset" Visible="true"> 
              <asp:Label ID="Label3" runat="server" Height="20px" Width="55px"
                        BackColor="White" BorderColor="#A8BBB8" BorderStyle="Solid" Text="08:30"
                        BorderWidth="1px" ForeColor="Black"
                        Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Smaller" Font-Italic="false"
                        style="margin-top: 0px; margin-left: 0px; text-indent: 0px; text-align: center; padding-top: 1px;"> 
              </asp:Label> 
            </asp:TableCell>
            <asp:TableCell ID="TableCell4" BackColor="#f0f0f0" BorderColor="#A8BBB8" BorderStyle="Notset" Visible="true"> 
              <asp:Label ID="Label4" runat="server" Height="20px" Width="55px"
                        BackColor="White" BorderColor="#A8BBB8" BorderStyle="Solid" Text="12:00"
                        BorderWidth="1px" ForeColor="Black"
                        Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Smaller" Font-Italic="false"
                        style="margin-top: 0px; margin-left: 0px; text-indent: 0px; text-align: center; padding-top: 1px;"> 
              </asp:Label>         
            </asp:TableCell>
            <asp:TableCell ID="TableCell9" BackColor="#f0f0f0" BorderColor="#A8BBB8" BorderStyle="Notset" Visible="true"> 
              <asp:Label ID="Label9" runat="server" Height="20px" Width="61px"
                        BackColor="White" BorderColor="#A8BBB8" BorderStyle="Solid" Text=""
                        BorderWidth="1px" ForeColor="Black"
                        Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Smaller" Font-Italic="false"
                        style="margin-top: 0px; margin-left: 0px; text-indent: 0px; text-align: center; padding-top: 1px;"> 
              </asp:Label>           
            </asp:TableCell>
            <asp:TableCell ID="TableCell5" BackColor="#f0f0f0" BorderColor="#A8BBB8" BorderStyle="Notset" Visible="true"> 
              <asp:Label ID="Label5" runat="server" Height="20px" Width="71px"
                        BackColor="White" BorderColor="#A8BBB8" BorderStyle="Solid" Text="TR"
                        BorderWidth="1px" ForeColor="Black"
                        Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Smaller" Font-Italic="false"
                        style="margin-top: 0px; margin-left: 0px; text-indent: 0px; text-align: center; padding-top: 1px;"> 
              </asp:Label>           
            </asp:TableCell>
            <asp:TableCell ID="TableCell6" BackColor="#f0f0f0" BorderColor="#A8BBB8" BorderStyle="Notset" Visible="true"> 
              <asp:Label ID="Label6" runat="server" Height="20px" Width="56px"
                        BackColor="White" BorderColor="#A8BBB8" BorderStyle="Solid" Text="14:00"
                        BorderWidth="1px" ForeColor="Black"
                        Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Smaller" Font-Italic="false"
                        style="margin-top: 0px; margin-left: 0px; text-indent: 0px; text-align: center; padding-top: 1px;"> 
              </asp:Label>        
            </asp:TableCell>
            <asp:TableCell ID="TableCell7" BackColor="#f0f0f0" BorderColor="#A8BBB8" BorderStyle="Notset" Visible="true"> 
              <asp:Label ID="Label7" runat="server" Height="20px" Width="55px"
                        BackColor="White" BorderColor="#A8BBB8" BorderStyle="Solid" Text="17:30"
                        BorderWidth="1px" ForeColor="Black"
                        Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Smaller" Font-Italic="false"
                        style="margin-top: 0px; margin-left: 0px; text-indent: 0px; text-align: center; padding-top: 1px;"> 
              </asp:Label>           
            </asp:TableCell>
            <asp:TableCell ID="TableCell8" BackColor="#f0f0f0" BorderColor="#A8BBB8" BorderStyle="Notset" Visible="true"> 
              <asp:Label ID="Label8" runat="server" Height="20px" Width="60px"
                        BackColor="White" BorderColor="#A8BBB8" BorderStyle="Solid" Text="7 h 00"
                        BorderWidth="1px" ForeColor="Black"
                        Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Smaller" Font-Italic="false"
                        style="margin-top: 0px; margin-left: 0px; text-indent: 0px; text-align: center; padding-top: 1px;"> 
              </asp:Label>           
            </asp:TableCell>           
          </asp:TableRow>
          <asp:TableRow ID="LignePersonne3" Width="746px" Height="20px">
            <asp:TableCell ID="TableCell10" BackColor="#f0f0f0" BorderColor="#A8BBB8" BorderStyle="Notset" HorizontalAlign="Center" Height="20px"> 
              <asp:Label ID="Label10" runat="server" Height="20px" Width="234px"
                  BackColor="#f0f0f0" BorderColor="#A8BBB8" BorderStyle="Solid" Text="DUPONT Jean"
                  BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                  Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                  style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                  font-style: normal; text-indent: 15px; text-align:  left; padding-top: 1px;" >
              </asp:Label>          
            </asp:TableCell>
            <asp:TableCell ID="TableCell11" BackColor="#f0f0f0" BorderColor="#A8BBB8" BorderStyle="Notset" Visible="true"> 
              <asp:Label ID="Label11" runat="server" Height="20px" Width="71px"
                        BackColor="#FFFC6D" BorderColor="#A8BBB8" BorderStyle="Solid" Text="MA"
                        BorderWidth="1px" ForeColor="Black"
                        Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Smaller" Font-Italic="false"
                        style="margin-top: 0px; margin-left: 0px; text-indent: 0px; text-align: center; padding-top: 1px;"> 
              </asp:Label>
            </asp:TableCell>
            <asp:TableCell ID="TableCell12" BackColor="#f0f0f0" BorderColor="#A8BBB8" BorderStyle="Notset" Visible="true"> 
              <asp:Label ID="Label12" runat="server" Height="20px" Width="55px"
                        BackColor="#FFFC6D" BorderColor="#A8BBB8" BorderStyle="Solid" Text=""
                        BorderWidth="1px" ForeColor="Black"
                        Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Smaller" Font-Italic="false"
                        style="margin-top: 0px; margin-left: 0px; text-indent: 0px; text-align: center; padding-top: 1px;"> 
              </asp:Label> 
            </asp:TableCell>
            <asp:TableCell ID="TableCell13" BackColor="#f0f0f0" BorderColor="#A8BBB8" BorderStyle="Notset" Visible="true"> 
              <asp:Label ID="Label13" runat="server" Height="20px" Width="55px"
                        BackColor="#FFFC6D" BorderColor="#A8BBB8" BorderStyle="Solid" Text=""
                        BorderWidth="1px" ForeColor="Black"
                        Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Smaller" Font-Italic="false"
                        style="margin-top: 0px; margin-left: 0px; text-indent: 0px; text-align: center; padding-top: 1px;"> 
              </asp:Label>         
            </asp:TableCell>
            <asp:TableCell ID="TableCell18" BackColor="#f0f0f0" BorderColor="#A8BBB8" BorderStyle="Notset" Visible="true"> 
              <asp:Label ID="Label18" runat="server" Height="20px" Width="61px"
                        BackColor="White" BorderColor="#A8BBB8" BorderStyle="Solid" Text=""
                        BorderWidth="1px" ForeColor="Black"
                        Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Smaller" Font-Italic="false"
                        style="margin-top: 0px; margin-left: 0px; text-indent: 0px; text-align: center; padding-top: 1px;"> 
              </asp:Label>           
            </asp:TableCell>
            <asp:TableCell ID="TableCell14" BackColor="#f0f0f0" BorderColor="#A8BBB8" BorderStyle="Notset" Visible="true"> 
              <asp:Label ID="Label14" runat="server" Height="20px" Width="71px"
                        BackColor="#FFFC6D" BorderColor="#A8BBB8" BorderStyle="Solid" Text="MA"
                        BorderWidth="1px" ForeColor="Black"
                        Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Smaller" Font-Italic="false"
                        style="margin-top: 0px; margin-left: 0px; text-indent: 0px; text-align: center; padding-top: 1px;"> 
              </asp:Label>           
            </asp:TableCell>
            <asp:TableCell ID="TableCell15" BackColor="#f0f0f0" BorderColor="#A8BBB8" BorderStyle="Notset" Visible="true"> 
              <asp:Label ID="Label15" runat="server" Height="20px" Width="56px"
                        BackColor="#FFFC6D" BorderColor="#A8BBB8" BorderStyle="Solid" Text=""
                        BorderWidth="1px" ForeColor="Black"
                        Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Smaller" Font-Italic="false"
                        style="margin-top: 0px; margin-left: 0px; text-indent: 0px; text-align: center; padding-top: 1px;"> 
              </asp:Label>        
            </asp:TableCell>
            <asp:TableCell ID="TableCell16" BackColor="#f0f0f0" BorderColor="#A8BBB8" BorderStyle="Notset" Visible="true"> 
              <asp:Label ID="Label16" runat="server" Height="20px" Width="55px"
                        BackColor="#FFFC6D" BorderColor="#A8BBB8" BorderStyle="Solid" Text=""
                        BorderWidth="1px" ForeColor="Black"
                        Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Smaller" Font-Italic="false"
                        style="margin-top: 0px; margin-left: 0px; text-indent: 0px; text-align: center; padding-top: 1px;"> 
              </asp:Label>           
            </asp:TableCell>
            <asp:TableCell ID="TableCell17" BackColor="#f0f0f0" BorderColor="#A8BBB8" BorderStyle="Notset" Visible="true"> 
              <asp:Label ID="Label17" runat="server" Height="20px" Width="60px"
                        BackColor="White" BorderColor="#A8BBB8" BorderStyle="Solid" Text=""
                        BorderWidth="1px" ForeColor="Black"
                        Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Smaller" Font-Italic="false"
                        style="margin-top: 0px; margin-left: 0px; text-indent: 0px; text-align: center; padding-top: 1px;"> 
              </asp:Label>           
            </asp:TableCell>           
          </asp:TableRow>
          <asp:TableRow ID="LignePersonne4" Width="746px" Height="20px">
            <asp:TableCell ID="TableCell19" BackColor="#f0f0f0" BorderColor="#A8BBB8" BorderStyle="Notset" HorizontalAlign="Center" Height="20px"> 
              <asp:Label ID="Label19" runat="server" Height="20px" Width="234px"
                  BackColor="#f0f0f0" BorderColor="#A8BBB8" BorderStyle="Solid" Text="ABENOU Nina"
                  BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                  Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                  style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                  font-style: normal; text-indent: 15px; text-align:  left; padding-top: 1px;">
              </asp:Label>          
            </asp:TableCell>
            <asp:TableCell ID="TableCell20" BackColor="#f0f0f0" BorderColor="#A8BBB8" BorderStyle="Notset" Visible="true"> 
              <asp:Label ID="Label20" runat="server" Height="20px" Width="71px"
                        BackColor="White" BorderColor="#A8BBB8" BorderStyle="Solid" Text="TR"
                        BorderWidth="1px" ForeColor="Black"
                        Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Smaller" Font-Italic="false"
                        style="margin-top: 0px; margin-left: 0px; text-indent: 0px; text-align: center; padding-top: 1px;"> 
              </asp:Label>
            </asp:TableCell>
            <asp:TableCell ID="TableCell21" BackColor="#f0f0f0" BorderColor="#A8BBB8" BorderStyle="Notset" Visible="true"> 
              <asp:Label ID="Label21" runat="server" Height="20px" Width="55px"
                        BackColor="White" BorderColor="#A8BBB8" BorderStyle="Solid" Text="08:30"
                        BorderWidth="1px" ForeColor="Black"
                        Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Smaller" Font-Italic="false"
                        style="margin-top: 0px; margin-left: 0px; text-indent: 0px; text-align: center; padding-top: 1px;"> 
              </asp:Label> 
            </asp:TableCell>
            <asp:TableCell ID="TableCell22" BackColor="#f0f0f0" BorderColor="#A8BBB8" BorderStyle="Notset" Visible="true"> 
              <asp:Label ID="Label22" runat="server" Height="20px" Width="55px"
                        BackColor="White" BorderColor="#A8BBB8" BorderStyle="Solid" Text="12:00"
                        BorderWidth="1px" ForeColor="Black"
                        Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Smaller" Font-Italic="false"
                        style="margin-top: 0px; margin-left: 0px; text-indent: 0px; text-align: center; padding-top: 1px;"> 
              </asp:Label>         
            </asp:TableCell>
            <asp:TableCell ID="TableCell27" BackColor="#f0f0f0" BorderColor="#A8BBB8" BorderStyle="Notset" Visible="true"> 
              <asp:Label ID="Label27" runat="server" Height="20px" Width="61px"
                        BackColor="White" BorderColor="#A8BBB8" BorderStyle="Solid" Text=""
                        BorderWidth="1px" ForeColor="Black"
                        Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Smaller" Font-Italic="false"
                        style="margin-top: 0px; margin-left: 0px; text-indent: 0px; text-align: center; padding-top: 1px;"> 
              </asp:Label>           
            </asp:TableCell>
            <asp:TableCell ID="TableCell23" BackColor="#f0f0f0" BorderColor="#A8BBB8" BorderStyle="Notset" Visible="true"> 
              <asp:Label ID="Label23" runat="server" Height="20px" Width="71px"
                        BackColor="White" BorderColor="#A8BBB8" BorderStyle="Solid" Text="TR"
                        BorderWidth="1px" ForeColor="Black"
                        Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Smaller" Font-Italic="false"
                        style="margin-top: 0px; margin-left: 0px; text-indent: 0px; text-align: center; padding-top: 1px;"> 
              </asp:Label>           
            </asp:TableCell>
            <asp:TableCell ID="TableCell24" BackColor="#f0f0f0" BorderColor="#A8BBB8" BorderStyle="Notset" Visible="true"> 
              <asp:Label ID="Label24" runat="server" Height="20px" Width="56px"
                        BackColor="White" BorderColor="#A8BBB8" BorderStyle="Solid" Text="14:00"
                        BorderWidth="1px" ForeColor="Black"
                        Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Smaller" Font-Italic="false"
                        style="margin-top: 0px; margin-left: 0px; text-indent: 0px; text-align: center; padding-top: 1px;"> 
              </asp:Label>        
            </asp:TableCell>
            <asp:TableCell ID="TableCell25" BackColor="#f0f0f0" BorderColor="#A8BBB8" BorderStyle="Notset" Visible="true"> 
              <asp:Label ID="Label25" runat="server" Height="20px" Width="55px"
                        BackColor="White" BorderColor="#A8BBB8" BorderStyle="Solid" Text="17:30"
                        BorderWidth="1px" ForeColor="Black"
                        Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Smaller" Font-Italic="false"
                        style="margin-top: 0px; margin-left: 0px; text-indent: 0px; text-align: center; padding-top: 1px;"> 
              </asp:Label>           
            </asp:TableCell>
            <asp:TableCell ID="TableCell26" BackColor="#f0f0f0" BorderColor="#A8BBB8" BorderStyle="Notset" Visible="true"> 
              <asp:Label ID="Label26" runat="server" Height="20px" Width="60px"
                        BackColor="White" BorderColor="#A8BBB8" BorderStyle="Solid" Text="7 h 00"
                        BorderWidth="1px" ForeColor="Black"
                        Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Smaller" Font-Italic="false"
                        style="margin-top: 0px; margin-left: 0px; text-indent: 0px; text-align: center; padding-top: 1px;"> 
              </asp:Label>           
            </asp:TableCell>           
          </asp:TableRow>
          <asp:TableRow ID="LignePersonne5" Width="746px" Height="20px">
            <asp:TableCell ID="TableCell28" BackColor="#f0f0f0" BorderColor="#A8BBB8" BorderStyle="Notset" HorizontalAlign="Center" Height="20px"> 
              <asp:Label ID="Label28" runat="server" Height="20px" Width="234px"
                  BackColor="#f0f0f0" BorderColor="#A8BBB8" BorderStyle="Solid" Text="ADAM Juliette"
                  BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                  Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                  style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                  font-style: normal; text-indent: 15px; text-align:  left; padding-top: 1px;">
              </asp:Label>          
            </asp:TableCell>
            <asp:TableCell ID="TableCell29" BackColor="#f0f0f0" BorderColor="#A8BBB8" BorderStyle="Notset" Visible="true"> 
              <asp:Label ID="Label29" runat="server" Height="20px" Width="71px"
                        BackColor="#A0E791" BorderColor="#A8BBB8" BorderStyle="Solid" Text="F"
                        BorderWidth="1px" ForeColor="Black"
                        Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Smaller" Font-Italic="false"
                        style="margin-top: 0px; margin-left: 0px; text-indent: 0px; text-align: center; padding-top: 1px;"> 
              </asp:Label>
            </asp:TableCell>
            <asp:TableCell ID="TableCell30" BackColor="#f0f0f0" BorderColor="#A8BBB8" BorderStyle="Notset" Visible="true"> 
              <asp:Label ID="Label30" runat="server" Height="20px" Width="55px"
                        BackColor="#A0E791" BorderColor="#A8BBB8" BorderStyle="Solid" Text=""
                        BorderWidth="1px" ForeColor="Black"
                        Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Smaller" Font-Italic="false"
                        style="margin-top: 0px; margin-left: 0px; text-indent: 0px; text-align: center; padding-top: 1px;"> 
              </asp:Label> 
            </asp:TableCell>
            <asp:TableCell ID="TableCell31" BackColor="#f0f0f0" BorderColor="#A8BBB8" BorderStyle="Notset" Visible="true"> 
              <asp:Label ID="Label31" runat="server" Height="20px" Width="55px"
                        BackColor="#A0E791" BorderColor="#A8BBB8" BorderStyle="Solid" Text=""
                        BorderWidth="1px" ForeColor="Black"
                        Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Smaller" Font-Italic="false"
                        style="margin-top: 0px; margin-left: 0px; text-indent: 0px; text-align: center; padding-top: 1px;"> 
              </asp:Label>         
            </asp:TableCell>
            <asp:TableCell ID="TableCell36" BackColor="#f0f0f0" BorderColor="#A8BBB8" BorderStyle="Notset" Visible="true"> 
              <asp:Label ID="Label36" runat="server" Height="20px" Width="61px"
                        BackColor="White" BorderColor="#A8BBB8" BorderStyle="Solid" Text=""
                        BorderWidth="1px" ForeColor="Black"
                        Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Smaller" Font-Italic="false"
                        style="margin-top: 0px; margin-left: 0px; text-indent: 0px; text-align: center; padding-top: 1px;"> 
              </asp:Label>           
            </asp:TableCell>
            <asp:TableCell ID="TableCell32" BackColor="#f0f0f0" BorderColor="#A8BBB8" BorderStyle="Notset" Visible="true"> 
              <asp:Label ID="Label32" runat="server" Height="20px" Width="71px"
                        BackColor="#88A2CD" BorderColor="#A8BBB8" BorderStyle="Solid" Text="CA"
                        BorderWidth="1px" ForeColor="Black"
                        Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Smaller" Font-Italic="false"
                        style="margin-top: 0px; margin-left: 0px; text-indent: 0px; text-align: center; padding-top: 1px;"> 
              </asp:Label>           
            </asp:TableCell>
            <asp:TableCell ID="TableCell33" BackColor="#f0f0f0" BorderColor="#A8BBB8" BorderStyle="Notset" Visible="true"> 
              <asp:Label ID="Label33" runat="server" Height="20px" Width="56px"
                        BackColor="#88A2CD" BorderColor="#A8BBB8" BorderStyle="Solid" Text=""
                        BorderWidth="1px" ForeColor="Black"
                        Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Smaller" Font-Italic="false"
                        style="margin-top: 0px; margin-left: 0px; text-indent: 0px; text-align: center; padding-top: 1px;"> 
              </asp:Label>        
            </asp:TableCell>
            <asp:TableCell ID="TableCell34" BackColor="#f0f0f0" BorderColor="#A8BBB8" BorderStyle="Notset" Visible="true"> 
              <asp:Label ID="Label34" runat="server" Height="20px" Width="55px"
                        BackColor="#88A2CD" BorderColor="#A8BBB8" BorderStyle="Solid" Text=""
                        BorderWidth="1px" ForeColor="Black"
                        Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Smaller" Font-Italic="false"
                        style="margin-top: 0px; margin-left: 0px; text-indent: 0px; text-align: center; padding-top: 1px;"> 
              </asp:Label>           
            </asp:TableCell>
            <asp:TableCell ID="TableCell35" BackColor="#f0f0f0" BorderColor="#A8BBB8" BorderStyle="Notset" Visible="true"> 
              <asp:Label ID="Label35" runat="server" Height="20px" Width="60px"
                        BackColor="White" BorderColor="#A8BBB8" BorderStyle="Solid" Text=""
                        BorderWidth="1px" ForeColor="Black"
                        Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Smaller" Font-Italic="false"
                        style="margin-top: 0px; margin-left: 0px; text-indent: 0px; text-align: center; padding-top: 1px;"> 
              </asp:Label>           
            </asp:TableCell>           
          </asp:TableRow>
          <asp:TableRow ID="LignePersonne6" Width="746px" Height="20px">
            <asp:TableCell ID="TableCell37" BackColor="#f0f0f0" BorderColor="#A8BBB8" BorderStyle="Notset" HorizontalAlign="Center" Height="20px"> 
              <asp:Label ID="Label37" runat="server" Height="20px" Width="234px"
                  BackColor="#f0f0f0" BorderColor="#A8BBB8" BorderStyle="Solid" Text="AMBROISE Julien"
                  BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                  Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                  style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                  font-style: normal; text-indent: 15px; text-align:  left; padding-top: 1px;" >
              </asp:Label>          
            </asp:TableCell>
            <asp:TableCell ID="TableCell38" BackColor="#f0f0f0" BorderColor="#A8BBB8" BorderStyle="Notset" Visible="true"> 
              <asp:Label ID="Label38" runat="server" Height="20px" Width="71px"
                        BackColor="#73829A" BorderColor="#A8BBB8" BorderStyle="Solid" Text="RH"
                        BorderWidth="1px" ForeColor="White"
                        Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Smaller" Font-Italic="false"
                        style="margin-top: 0px; margin-left: 0px; text-indent: 0px; text-align: center; padding-top: 1px;"> 
              </asp:Label>
            </asp:TableCell>
            <asp:TableCell ID="TableCell39" BackColor="#f0f0f0" BorderColor="#A8BBB8" BorderStyle="Notset" Visible="true"> 
              <asp:Label ID="Label39" runat="server" Height="20px" Width="55px"
                        BackColor="#73829A" BorderColor="#A8BBB8" BorderStyle="Solid" Text=""
                        BorderWidth="1px" ForeColor="Black"
                        Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Smaller" Font-Italic="false"
                        style="margin-top: 0px; margin-left: 0px; text-indent: 0px; text-align: center; padding-top: 1px;"> 
              </asp:Label> 
            </asp:TableCell>
            <asp:TableCell ID="TableCell40" BackColor="#f0f0f0" BorderColor="#A8BBB8" BorderStyle="Notset" Visible="true"> 
              <asp:Label ID="Label40" runat="server" Height="20px" Width="55px"
                        BackColor="#73829A" BorderColor="#A8BBB8" BorderStyle="Solid" Text=""
                        BorderWidth="1px" ForeColor="Black"
                        Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Smaller" Font-Italic="false"
                        style="margin-top: 0px; margin-left: 0px; text-indent: 0px; text-align: center; padding-top: 1px;"> 
              </asp:Label>         
            </asp:TableCell>
            <asp:TableCell ID="TableCell45" BackColor="#f0f0f0" BorderColor="#A8BBB8" BorderStyle="Notset" Visible="true"> 
              <asp:Label ID="Label45" runat="server" Height="20px" Width="61px"
                        BackColor="White" BorderColor="#A8BBB8" BorderStyle="Solid" Text=""
                        BorderWidth="1px" ForeColor="Black"
                        Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Smaller" Font-Italic="false"
                        style="margin-top: 0px; margin-left: 0px; text-indent: 0px; text-align: center; padding-top: 1px;"> 
              </asp:Label>           
            </asp:TableCell>
            <asp:TableCell ID="TableCell41" BackColor="#f0f0f0" BorderColor="#A8BBB8" BorderStyle="Notset" Visible="true"> 
              <asp:Label ID="Label41" runat="server" Height="20px" Width="71px"
                        BackColor="#73829A" BorderColor="#A8BBB8" BorderStyle="Solid" Text="RH"
                        BorderWidth="1px" ForeColor="White"
                        Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Smaller" Font-Italic="false"
                        style="margin-top: 0px; margin-left: 0px; text-indent: 0px; text-align: center; padding-top: 1px;"> 
              </asp:Label>           
            </asp:TableCell>
            <asp:TableCell ID="TableCell42" BackColor="#f0f0f0" BorderColor="#A8BBB8" BorderStyle="Notset" Visible="true"> 
              <asp:Label ID="Label42" runat="server" Height="20px" Width="56px"
                        BackColor="#73829A" BorderColor="#A8BBB8" BorderStyle="Solid" Text=""
                        BorderWidth="1px" ForeColor="Black"
                        Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Smaller" Font-Italic="false"
                        style="margin-top: 0px; margin-left: 0px; text-indent: 0px; text-align: center; padding-top: 1px;"> 
              </asp:Label>        
            </asp:TableCell>
            <asp:TableCell ID="TableCell43" BackColor="#f0f0f0" BorderColor="#A8BBB8" BorderStyle="Notset" Visible="true"> 
              <asp:Label ID="Label43" runat="server" Height="20px" Width="55px"
                        BackColor="#73829A" BorderColor="#A8BBB8" BorderStyle="Solid" Text=""
                        BorderWidth="1px" ForeColor="Black"
                        Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Smaller" Font-Italic="false"
                        style="margin-top: 0px; margin-left: 0px; text-indent: 0px; text-align: center; padding-top: 1px;"> 
              </asp:Label>           
            </asp:TableCell>
            <asp:TableCell ID="TableCell44" BackColor="#f0f0f0" BorderColor="#A8BBB8" BorderStyle="Notset" Visible="true"> 
              <asp:Label ID="Label44" runat="server" Height="20px" Width="60px"
                        BackColor="White" BorderColor="#A8BBB8" BorderStyle="Solid" Text=""
                        BorderWidth="1px" ForeColor="Black"
                        Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Smaller" Font-Italic="false"
                        style="margin-top: 0px; margin-left: 0px; text-indent: 0px; text-align: center; padding-top: 1px;"> 
              </asp:Label>           
            </asp:TableCell>           
          </asp:TableRow>
          <asp:TableRow ID="LignePersonne7" Width="746px" Height="20px">
            <asp:TableCell ID="TableCell46" BackColor="#f0f0f0" BorderColor="#A8BBB8" BorderStyle="Notset" HorizontalAlign="Center" Height="20px"> 
              <asp:Label ID="Label46" runat="server" Height="20px" Width="234px"
                  BackColor="#f0f0f0" BorderColor="#A8BBB8" BorderStyle="Solid" Text="ABAL Lucie"
                  BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                  Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                  style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                  font-style: normal; text-indent: 15px; text-align:  left; padding-top: 1px;">
              </asp:Label>          
            </asp:TableCell>
            <asp:TableCell ID="TableCell47" BackColor="#f0f0f0" BorderColor="#A8BBB8" BorderStyle="Notset" Visible="true"> 
              <asp:Label ID="Label47" runat="server" Height="20px" Width="71px"
                        BackColor="#73829A" BorderColor="#A8BBB8" BorderStyle="Solid" Text="RH"
                        BorderWidth="1px" ForeColor="White"
                        Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Smaller" Font-Italic="false"
                        style="margin-top: 0px; margin-left: 0px; text-indent: 0px; text-align: center; padding-top: 1px;"> 
              </asp:Label>
            </asp:TableCell>
            <asp:TableCell ID="TableCell48" BackColor="#f0f0f0" BorderColor="#A8BBB8" BorderStyle="Notset" Visible="true"> 
              <asp:Label ID="Label48" runat="server" Height="20px" Width="55px"
                        BackColor="#73829A" BorderColor="#A8BBB8" BorderStyle="Solid" Text=""
                        BorderWidth="1px" ForeColor="Black"
                        Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Smaller" Font-Italic="false"
                        style="margin-top: 0px; margin-left: 0px; text-indent: 0px; text-align: center; padding-top: 1px;"> 
              </asp:Label> 
            </asp:TableCell>
            <asp:TableCell ID="TableCell49" BackColor="#f0f0f0" BorderColor="#A8BBB8" BorderStyle="Notset" Visible="true"> 
              <asp:Label ID="Label49" runat="server" Height="20px" Width="55px"
                        BackColor="#73829A" BorderColor="#A8BBB8" BorderStyle="Solid" Text=""
                        BorderWidth="1px" ForeColor="Black"
                        Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Smaller" Font-Italic="false"
                        style="margin-top: 0px; margin-left: 0px; text-indent: 0px; text-align: center; padding-top: 1px;"> 
              </asp:Label>         
            </asp:TableCell>
            <asp:TableCell ID="TableCell54" BackColor="#f0f0f0" BorderColor="#A8BBB8" BorderStyle="Notset" Visible="true"> 
              <asp:Label ID="Label54" runat="server" Height="20px" Width="61px"
                        BackColor="White" BorderColor="#A8BBB8" BorderStyle="Solid" Text=""
                        BorderWidth="1px" ForeColor="Black"
                        Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Smaller" Font-Italic="false"
                        style="margin-top: 0px; margin-left: 0px; text-indent: 0px; text-align: center; padding-top: 1px;"> 
              </asp:Label>           
            </asp:TableCell>
            <asp:TableCell ID="TableCell50" BackColor="#f0f0f0" BorderColor="#A8BBB8" BorderStyle="Notset" Visible="true"> 
              <asp:Label ID="Label50" runat="server" Height="20px" Width="71px"
                        BackColor="#73829A" BorderColor="#A8BBB8" BorderStyle="Solid" Text="RH"
                        BorderWidth="1px" ForeColor="White"
                        Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Smaller" Font-Italic="false"
                        style="margin-top: 0px; margin-left: 0px; text-indent: 0px; text-align: center; padding-top: 1px;"> 
              </asp:Label>           
            </asp:TableCell>
            <asp:TableCell ID="TableCell51" BackColor="#f0f0f0" BorderColor="#A8BBB8" BorderStyle="Notset" Visible="true"> 
              <asp:Label ID="Label51" runat="server" Height="20px" Width="56px"
                        BackColor="#73829A" BorderColor="#A8BBB8" BorderStyle="Solid" Text=""
                        BorderWidth="1px" ForeColor="Black"
                        Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Smaller" Font-Italic="false"
                        style="margin-top: 0px; margin-left: 0px; text-indent: 0px; text-align: center; padding-top: 1px;"> 
              </asp:Label>        
            </asp:TableCell>
            <asp:TableCell ID="TableCell52" BackColor="#f0f0f0" BorderColor="#A8BBB8" BorderStyle="Notset" Visible="true"> 
              <asp:Label ID="Label52" runat="server" Height="20px" Width="55px"
                        BackColor="#73829A" BorderColor="#A8BBB8" BorderStyle="Solid" Text=""
                        BorderWidth="1px" ForeColor="Black"
                        Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Smaller" Font-Italic="false"
                        style="margin-top: 0px; margin-left: 0px; text-indent: 0px; text-align: center; padding-top: 1px;"> 
              </asp:Label>           
            </asp:TableCell>
            <asp:TableCell ID="TableCell53" BackColor="#f0f0f0" BorderColor="#A8BBB8" BorderStyle="Notset" Visible="true"> 
              <asp:Label ID="Label53" runat="server" Height="20px" Width="60px"
                        BackColor="White" BorderColor="#A8BBB8" BorderStyle="Solid" Text=""
                        BorderWidth="1px" ForeColor="Black"
                        Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Smaller" Font-Italic="false"
                        style="margin-top: 0px; margin-left: 0px; text-indent: 0px; text-align: center; padding-top: 1px;"> 
              </asp:Label>           
            </asp:TableCell>           
          </asp:TableRow>
        </asp:Table>
     </asp:TableCell>     
  </asp:TableRow>     
  
  <asp:TableRow>
    <asp:TableCell>
      <asp:Table ID="TableTotaux" runat="server" CellPadding="0" CellSpacing="0" Width="750px"
      BorderStyle="Notset" BorderWidth="1px" BorderColor="Black" HorizontalAlign="Center">
        <asp:TableRow ID="LigneTotauxPlages" Width="746px" Height="20px">
            <asp:TableCell ID="CellTitreTotalJournee" BackColor="#CAEBE4" BorderColor="#A8BBB8" BorderStyle="None" Visible="true"> 
              <asp:Label ID="Label55" runat="server" Height="20px" Width="71px"
                        BackColor="#CAEBE4" BorderColor="#A8BBB8" BorderStyle="None" Text="Journée"
                        BorderWidth="1px" ForeColor="#124545"
                        Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Smaller" Font-Italic="false"
                        style="margin-top: 0px; margin-left: 0px; text-indent: 0px; text-align: center; padding-top: 1px;"> 
              </asp:Label> 
            </asp:TableCell>
            <asp:TableCell ID="CellTotalJournee" BackColor="#A8BBB8" BorderColor="#A8BBB8" BorderStyle="None" Visible="true"> 
              <asp:Label ID="LabelTotalJournee" runat="server" Height="20px" Width="113px"
                        BackColor="#A8BBB8" BorderColor="#A8BBB8" BorderStyle="None" Text="124 h 18"
                        BorderWidth="1px" ForeColor="#1C5151"
                        Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Smaller" Font-Italic="false"
                        style="margin-top: 0px; margin-left: 0px; text-indent: 0px; text-align: center; padding-top: 1px;"> 
              </asp:Label>           
            </asp:TableCell>
            <asp:TableCell ID="CellSeparateurTotalJour" BackColor="#f0f0f0" BorderColor="#A8BBB8" BorderStyle="None" HorizontalAlign="Center" Height="20px"> 
              <asp:Label ID="LabelTotauxPlages" runat="server" Height="20px" Width="50px"
                  BackColor="White" BorderColor="#A8BBB8" BorderStyle="None" Text=""
                  BorderWidth="1px" ForeColor="#1C5151" Font-Italic="False"
                  Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                  style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                  font-style: normal; text-indent: 15px; text-align:  left; padding-top: 1px;" >
              </asp:Label>          
            </asp:TableCell>
            <asp:TableCell ID="CellTitreTotalMatin" BackColor="#CAEBE4" BorderColor="#A8BBB8" BorderStyle="None" Visible="true"> 
              <asp:Label ID="LabelTitreTotalMatin" runat="server" Height="20px" Width="71px"
                        BackColor="#CAEBE4" BorderColor="#A8BBB8" BorderStyle="None" Text="Matin"
                        BorderWidth="1px" ForeColor="#1C5151"
                        Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Smaller" Font-Italic="false"
                        style="margin-top: 0px; margin-left: 0px; text-indent: 0px; text-align: center; padding-top: 1px;"> 
              </asp:Label>
            </asp:TableCell>
            <asp:TableCell ID="CellTotalMatin" BackColor="#A8BBB8" BorderColor="#A8BBB8" BorderStyle="Notset" Visible="true"> 
              <asp:Label ID="LabelTotalMatin" runat="server" Height="20px" Width="113px"
                        BackColor="#A8BBB8" BorderColor="#A8BBB8" BorderStyle="Solid" Text="52 h 32"
                        BorderWidth="1px" ForeColor="#124545"
                        Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Smaller" Font-Italic="false"
                        style="margin-top: 0px; margin-left: 0px; text-indent: 0px; text-align: center; padding-top: 1px;"> 
              </asp:Label> 
            </asp:TableCell>
            <asp:TableCell ID="CellSeparateurTotauxPlages" BackColor="#f0f0f0" BorderColor="#A8BBB8" BorderStyle="None" Visible="true"> 
              <asp:Label ID="LabelSeparateurTotauxPlages" runat="server" Height="20px" Width="61px"
                        BackColor="White" BorderColor="#A8BBB8" BorderStyle="None" Text=""
                        BorderWidth="1px" ForeColor="#1C5151"
                        Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Smaller" Font-Italic="false"
                        style="margin-top: 0px; margin-left: 0px; text-indent: 0px; text-align: center; padding-top: 1px;"> 
              </asp:Label>           
            </asp:TableCell>
            <asp:TableCell ID="CellTitreTotalApresMidi" BackColor="#CAEBE4" BorderColor="#A8BBB8" BorderStyle="None" Visible="true"> 
              <asp:Label ID="LabelTitreTotalApresMidi" runat="server" Height="20px" Width="71px"
                        BackColor="#CAEBE4" BorderColor="#A8BBB8" BorderStyle="None" Text="Après-midi"
                        BorderWidth="1px" ForeColor="#1C5151"
                        Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Smaller" Font-Italic="false"
                        style="margin-top: 0px; margin-left: 0px; text-indent: 0px; text-align: center; padding-top: 1px;"> 
              </asp:Label>           
            </asp:TableCell>
            <asp:TableCell ID="CellTotalApresMidi" BackColor="#A8BBB8" BorderColor="#A8BBB8" BorderStyle="Notset" Visible="true"> 
              <asp:Label ID="LabelTotalApresMidi" runat="server" Height="20px" Width="114px"
                        BackColor="#A8BBB8" BorderColor="#A8BBB8" BorderStyle="Solid" Text="44 h 12"
                        BorderWidth="1px" ForeColor="#124545"
                        Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Smaller" Font-Italic="false"
                        style="margin-top: 0px; margin-left: 0px; text-indent: 0px; text-align: center; padding-top: 1px;"> 
              </asp:Label>        
            </asp:TableCell>
            <asp:TableCell ID="CellTotauxPlagesFin" BackColor="#f0f0f0" BorderColor="#A8BBB8" BorderStyle="None" Visible="true"> 
              <asp:Label ID="LabelTotauxPlagesFin" runat="server" Height="20px" Width="60px"
                        BackColor="White" BorderColor="#A8BBB8" BorderStyle="None" Text=""
                        BorderWidth="1px" ForeColor="#1C5151"
                        Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Smaller" Font-Italic="false"
                        style="margin-top: 0px; margin-left: 0px; text-indent: 0px; text-align: center; padding-top: 1px;"> 
              </asp:Label>           
            </asp:TableCell>           
        </asp:TableRow>

      </asp:Table>
    </asp:TableCell>     
  </asp:TableRow>
  
 </asp:Table>