﻿Option Strict On
Option Explicit On
Option Compare Text

Imports VI = Virtualia.Systeme.Constantes
Imports Virtualia.ObjetBase
Imports Virtualia.OutilsVisu
Imports Virtualia.TablesObjet.ShemaPER

Partial Class Fenetre_PER_DROIT_CONGES
    Inherits ObjetWebControleBase(Of PER_DROIT_CONGES)
    Implements IControlCrud

#Region "IControlCrud"
    Public ReadOnly Property ControlCrud As ICtlCommandeCrud Implements IControlCrud.CtlCrud
        Get
            Return Me.CtlCrud
        End Get
    End Property
#End Region

#Region "ObjetWebControleBase"
    Private _webfonction As WebFonctions
    Protected Overrides ReadOnly Property V_WebFonction As IWebFonctions
        Get
            If (_webfonction Is Nothing) Then
                _webfonction = New WebFonctions(Me)
            End If
            Return _webfonction
        End Get
    End Property

    Protected Overrides Function InitCadreInfo() As WebControl
        Return Me.CadreInfo
    End Function

    Protected Overrides Function InitControlDataGrid() As IControlDataGrid
        Return Me.ListeGrille
    End Function

    Protected Overrides Sub InitCacheIde()

        V_CacheIde.TypeVue = WebControlTypeVue.ListeGrid

        Dim captions As List(Of String) = New List(Of String)()
        captions.Add("Aucune situation")
        captions.Add("Une situation")
        captions.Add("situations")

        Me.ListeGrille.InitCaptions(captions)

        Dim colonnes As ColonneDataGridCollection = New ColonneDataGridCollection()

        colonnes.Add(HorizontalAlign.Center, TypeData.Date, "année", 0)
        colonnes.Add(HorizontalAlign.Center, TypeData.Numeric, "congés annuels", 1)
        colonnes.Add(HorizontalAlign.Center, TypeData.Numeric, "rtt", 8)
        colonnes.Add(HorizontalAlign.Center, TypeData.Numeric, "Compteur 1", 4)
        colonnes.Add(HorizontalAlign.Center, TypeData.Numeric, "Compteur 2", 5)
        colonnes.Add(HorizontalAlign.Center, TypeData.Numeric, "Compteur 3", 6)
        colonnes.Add(HorizontalAlign.Center, TypeData.Chaine, "Clef", 0, True)

        Me.ListeGrille.InitColonnes(colonnes)

    End Sub

    Protected Overrides Sub OnPreRender(e As EventArgs)
        MyBase.OnPreRender(e)

        CadreInfo.BackColor = _webfonction.CouleurCharte(V_CacheIde.PointDeVue, "Cadre")
        CadreInfo.BorderColor = _webfonction.CouleurCharte(V_CacheIde.PointDeVue, "Bordure")
        Etiquette.BackColor = _webfonction.CouleurCharte(V_CacheIde.PointDeVue, "Titre")
        Etiquette.ForeColor = _webfonction.CouleurCharte(V_CacheIde.PointDeVue, "Police_Claire")
        Etiquette.BorderColor = _webfonction.CouleurCharte(V_CacheIde.PointDeVue, "Bordure")
        EtiConges.BackColor = _webfonction.CouleurCharte(V_CacheIde.PointDeVue, "Sous-Titre")
        EtiConges.ForeColor = _webfonction.CouleurCharte(V_CacheIde.PointDeVue, "Etiquette_ForeColor")
        EtiConges.BorderColor = _webfonction.CouleurCharte(V_CacheIde.PointDeVue, "Bordure")
        EtiAutreConges.BackColor = _webfonction.CouleurCharte(V_CacheIde.PointDeVue, "Sous-Titre")
        EtiAutreConges.ForeColor = _webfonction.CouleurCharte(V_CacheIde.PointDeVue, "Etiquette_ForeColor")
        EtiAutreConges.BorderColor = _webfonction.CouleurCharte(V_CacheIde.PointDeVue, "Bordure")
        LabelARTT.BackColor = _webfonction.CouleurCharte(V_CacheIde.PointDeVue, "Sous-Titre")
        LabelARTT.ForeColor = _webfonction.CouleurCharte(V_CacheIde.PointDeVue, "Etiquette_ForeColor")
        LabelARTT.BorderColor = _webfonction.CouleurCharte(V_CacheIde.PointDeVue, "Bordure")
        LabelJour01.ForeColor = _webfonction.CouleurCharte(V_CacheIde.PointDeVue, "Etiquette_BackColor")
        LabelJour02.ForeColor = _webfonction.CouleurCharte(V_CacheIde.PointDeVue, "Etiquette_BackColor")
        LabelJour17.ForeColor = _webfonction.CouleurCharte(V_CacheIde.PointDeVue, "Etiquette_BackColor")
        LabelJour08.ForeColor = _webfonction.CouleurCharte(V_CacheIde.PointDeVue, "Etiquette_BackColor")
        LabelJour22.ForeColor = _webfonction.CouleurCharte(V_CacheIde.PointDeVue, "Etiquette_BackColor")
        LabelJour23.ForeColor = _webfonction.CouleurCharte(V_CacheIde.PointDeVue, "Etiquette_BackColor")
        LabelJour03.ForeColor = _webfonction.CouleurCharte(V_CacheIde.PointDeVue, "Etiquette_BackColor")
        LabelJour04.ForeColor = _webfonction.CouleurCharte(V_CacheIde.PointDeVue, "Etiquette_BackColor")
        LabelJour05.ForeColor = _webfonction.CouleurCharte(V_CacheIde.PointDeVue, "Etiquette_BackColor")
        LabelHeure06.ForeColor = _webfonction.CouleurCharte(V_CacheIde.PointDeVue, "Etiquette_BackColor")
        LabelHeure07.ForeColor = _webfonction.CouleurCharte(V_CacheIde.PointDeVue, "Etiquette_BackColor")
    End Sub

    Protected Overrides Sub OnInit(e As System.EventArgs)
        MyBase.OnInit(e)

        Dim initwebfonction As IWebFonctions = V_WebFonction

    End Sub
#End Region

#Region "Specifique"
    Protected Overrides Sub LireLaFiche()
        MyBase.LireLaFiche()

        If V_CacheMaj Is Nothing Then
            Return
        End If

        For Each ct As ObjetWebControlSaisie In _dicoWebControl(TypeWebControl.VCoupleEtiDonnee)

            Dim etdon As Controles_VCoupleEtiDonnee = DirectCast(ct, Controles_VCoupleEtiDonnee)

            Select Case etdon.V_Information
                Case 1
                    etdon.EtiText = _webfonction.PointeurUtilisateur.PointeurParametres.LibelleCompteur_Conge(0)
                Case 3 To 7
                    etdon.EtiText = _webfonction.PointeurUtilisateur.PointeurParametres.LibelleCompteur_Conge(etdon.V_Information - 1)
                    If etdon.EtiText = "" Then
                        etdon.Visible = False
                        Select Case etdon.V_Information
                            Case 3
                                etdon.Visible = False
                                Coche19.Visible = False
                            Case 4
                                etdon.Visible = False
                                Coche20.Visible = False
                            Case 5
                                etdon.Visible = False
                                Coche21.Visible = False
                            Case 6
                                etdon.Visible = False
                            Case 7
                                etdon.Visible = False
                        End Select
                    End If
                Case 8
                    etdon.EtiText = _webfonction.PointeurUtilisateur.PointeurParametres.LibelleCompteur_Conge(1)
                    If etdon.EtiText = "" Then
                        LabelARTT.Visible = False
                        TableauJRTT.Visible = False
                        TableauReportRTT.Visible = False
                        LabelJour23.Visible = False
                        InfoH23.Visible = False
                    End If
            End Select
        Next
    End Sub
#End Region

    'Private Sub LireLaFiche()
    '    Dim CacheDonnee As ArrayList = V_CacheMaj
    '    If CacheDonnee Is Nothing Then
    '        Exit Sub
    '    End If
    '    Dim NumInfo As Integer
    '    Dim Ctl As Control
    '    Dim VirControle As Controles_VCoupleEtiDonnee
    '    Dim IndiceI As Integer = 0

    '    Do
    '        Ctl = V_WebFonction.VirWebControle(Me.CadreInfo, "InfoH", IndiceI)
    '        If Ctl Is Nothing Then
    '            Exit Do
    '        End If
    '        NumInfo = CInt(Strings.Right(Ctl.ID, 2))
    '        VirControle = CType(Ctl, Controles_VCoupleEtiDonnee)
    '        If CacheDonnee(NumInfo) Is Nothing Then
    '            VirControle.DonText = ""
    '        Else
    '            VirControle.DonText = CacheDonnee(NumInfo).ToString
    '        End If
    '        VirControle.V_SiAutoPostBack = False
    '        '** Spécifique Libellé des Compteurs
    '        Select Case NumInfo
    '            Case 1
    '                VirControle.EtiText = V_WebFonction.PointeurUtilisateur.PointeurParametres.LibelleCompteur_Conge(0)
    '            Case 3 To 7
    '                VirControle.EtiText = V_WebFonction.PointeurUtilisateur.PointeurParametres.LibelleCompteur_Conge(NumInfo - 1)
    '                If VirControle.EtiText = "" Then
    '                    VirControle.Visible = False
    '                    Select Case NumInfo
    '                        Case 3
    '                            LabelJour03.Visible = False
    '                            Coche19.Visible = False
    '                        Case 4
    '                            LabelJour04.Visible = False
    '                            Coche20.Visible = False
    '                        Case 5
    '                            LabelJour05.Visible = False
    '                            Coche21.Visible = False
    '                        Case 6
    '                            LabelHeure06.Visible = False
    '                        Case 7
    '                            LabelHeure07.Visible = False
    '                    End Select
    '                End If
    '            Case 8
    '                VirControle.EtiText = V_WebFonction.PointeurUtilisateur.PointeurParametres.LibelleCompteur_Conge(1)
    '                If VirControle.EtiText = "" Then
    '                    LabelARTT.Visible = False
    '                    TableauJRTT.Visible = False
    '                    TableauReportRTT.Visible = False
    '                    LabelJour23.Visible = False
    '                    InfoH23.Visible = False
    '                End If
    '        End Select
    '        IndiceI += 1
    '    Loop

    '    Dim VirVertical As VirtualiaControle_VCoupleVerticalEtiDonnee
    '    IndiceI = 0
    '    Do
    '        Ctl = V_WebFonction.VirWebControle(Me.CadreInfo, "InfoV", IndiceI)
    '        If Ctl Is Nothing Then
    '            Exit Do
    '        End If
    '        NumInfo = CInt(Strings.Right(Ctl.ID, 2))
    '        VirVertical = CType(Ctl, VirtualiaControle_VCoupleVerticalEtiDonnee)
    '        If CacheDonnee(NumInfo) Is Nothing Then
    '            VirVertical.DonText = ""
    '        Else
    '            VirVertical.DonText = CacheDonnee(NumInfo).ToString
    '        End If
    '        VirVertical.V_SiAutoPostBack = Not (CommandeOK.Visible)
    '        IndiceI += 1
    '    Loop

    '    Dim VirCoche As Controles_VCocheSimple
    '    IndiceI = 0
    '    Do
    '        Ctl = V_WebFonction.VirWebControle(Me.CadreInfo, "Coche", IndiceI)
    '        If Ctl Is Nothing Then
    '            Exit Do
    '        End If
    '        NumInfo = CInt(Strings.Right(Ctl.ID, 2))
    '        VirCoche = CType(Ctl, Controles_VCocheSimple)
    '        VirCoche.V_SiAutoPostBack = Not (CommandeOK.Visible)
    '        If CacheDonnee(NumInfo) Is Nothing Then
    '            VirCoche.V_Check = False
    '        Else
    '            If CacheDonnee(NumInfo).ToString = "Oui" Then
    '                VirCoche.V_Check = True
    '            Else
    '                VirCoche.V_Check = False
    '            End If
    '        End If
    '        IndiceI += 1
    '    Loop

    'End Sub

End Class
