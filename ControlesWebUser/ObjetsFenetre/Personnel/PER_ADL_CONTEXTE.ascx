﻿<%@ Control Language="VB" AutoEventWireup="false" Inherits="Virtualia.Net.Fenetre_PER_ADL_CONTEXTE" CodeBehind="PER_ADL_CONTEXTE.ascx.vb" %>

<%@ Register Src="~/Controles/Saisies/VCoupleEtiDonnee.ascx" TagName="VCoupleEtiDonnee" TagPrefix="Virtualia" %>
<%@ Register Src="~/Controles/Saisies/VDuoEtiquetteCommande.ascx" TagName="VDuoEtiquetteCommande" TagPrefix="Virtualia" %>
<%@ Register Src="~/Controles/Saisies/VCoupleVerticalEtiDonnee.ascx" TagName="VCoupleVerticalEtiDonnee" TagPrefix="Virtualia" %>

<%@ Register Src="~/ControlsGeneriques/V_DataGrid.ascx" TagName="VDataGrid" TagPrefix="Generic" %>
<%@ Register Src="~/ControlsGeneriques/V_CommandeCRUD.ascx" TagName="VCrud" TagPrefix="Generic" %>

<asp:Table ID="CadreControle" runat="server" Height="30px" Width="500px" CellPadding="0" CellSpacing="0" HorizontalAlign="Center">
    <asp:TableRow>
        <asp:TableCell>
            <Generic:VDataGrid ID="ListeGrille" runat="server" CadreWidth="430px" SiColonneSelect="true" SiCaseAcocher="false" />
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="CadreInfo" runat="server" BorderStyle="Ridge" BorderWidth="2px" BorderColor="#B0E0D7" Height="725px" Width="650px" HorizontalAlign="Center" Style="margin-top: 3px;">
                <asp:TableRow>
                    <asp:TableCell>
                        <Generic:VCrud ID="CtlCrud" runat="server" Height="22px" Width="244px" HorizontalAlign="Right" CadreStyle="margin-top: 3px; margin-right: 3px" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Table ID="CadreTitre" runat="server" Height="40px" CellPadding="0" Width="500px" CellSpacing="0" HorizontalAlign="Center">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                    <asp:Label ID="Etiquette" runat="server" Text="Contexte de la paie ADL" Height="20px" Width="320px" BackColor="#2FA49B" BorderColor="#B0E0D7" BorderStyle="Groove" BorderWidth="2px" ForeColor="#D7FAF3" Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Small" Style="margin-top: 5px; margin-left: 4px; margin-bottom: 20px; font-style: oblique; text-indent: 5px; text-align: center;" />
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Table ID="CadreDates" runat="server" CellPadding="0" CellSpacing="0" Width="550px">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH00" runat="server" V_PointdeVue="1" V_Objet="189" V_Information="0" V_SiDonneeDico="false" EtiText="Date de valeur" EtiWidth="110px" DonWidth="80px" DonTabIndex="1" />
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Left">
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH29" runat="server" V_PointdeVue="1" V_Objet="189" V_Information="29" V_SiDonneeDico="false" EtiText="Date de fin" EtiWidth="110px" DonWidth="80px" DonTabIndex="2" />
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Table ID="CadreDon" runat="server" CellPadding="0" CellSpacing="0" Width="650px">
                            <asp:TableRow>
                                <asp:TableCell Height="12px" />
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <Virtualia:VDuoEtiquetteCommande ID="Dontab01" runat="server" V_PointdeVue="1" V_Objet="189" V_Information="1" V_SiDonneeDico="false" EtiText="Monnaie de compte principale" EtiWidth="240px" DonWidth="280px" DonTabIndex="3" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <Virtualia:VDuoEtiquetteCommande ID="Dontab02" runat="server" V_PointdeVue="1" V_Objet="189" V_Information="2" V_SiDonneeDico="false" EtiText="Monnaie de compte alternative" EtiWidth="240px" DonWidth="280px" DonTabIndex="4" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Height="12px" />
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Table ID="CadreEnteteContextePaie" runat="server" Height="25px" CellPadding="0" CellSpacing="0" Width="650px">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <asp:Label ID="LabelRubriquePaie" runat="server" Height="20px" Width="340px" BackColor="#216B68" BorderColor="#B0E0D7" BorderStyle="Outset" Text="Montant" BorderWidth="2px" ForeColor="#D7FAF3" Font-Italic="False" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Style="margin-top: 0px; margin-left: 5px; margin-bottom: 0px; font-style: oblique; text-indent: 2px; text-align: center;" />
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Left">
                                    <asp:Label ID="LabelMonnaieCompte" runat="server" Height="20px" Width="285px" BackColor="#216B68" BorderColor="#B0E0D7" BorderStyle="Outset" Text="Monnaie de compte" BorderWidth="2px" ForeColor="#D7FAF3" Font-Italic="False" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Style="margin-top: 0px; margin-left: 2px; margin-bottom: 0px; font-style: oblique; text-indent: 2px; text-align: center;" />
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Table ID="TableauContextePaie" runat="server" Height="25px" CellPadding="0" CellSpacing="0" Width="650px">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH03" runat="server" V_PointdeVue="1" V_Objet="189" V_Information="3" V_SiDonneeDico="false" EtiText="Salaire mensuel brut" EtiWidth="250px" DonWidth="82px" DonTabIndex="5" />
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Left">
                                    <Virtualia:VDuoEtiquetteCommande ID="Dontab04" runat="server" V_PointdeVue="1" V_Objet="189" V_Information="4" V_SiDonneeDico="false" Etivisible="false" DonWidth="280px" DonTabIndex="6" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH05" runat="server" V_PointdeVue="1" V_Objet="189" V_Information="5" V_SiDonneeDico="false" EtiText="Cotisations salariales mensuelles" EtiWidth="250px" DonWidth="82px" DonTabIndex="7" />
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Left">
                                    <Virtualia:VDuoEtiquetteCommande ID="Dontab06" runat="server" V_PointdeVue="1" V_Objet="189" V_Information="6" V_SiDonneeDico="false" Etivisible="false" DonWidth="280px" DonTabIndex="8" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH07" runat="server" V_PointdeVue="1" V_Objet="189" V_Information="7" V_SiDonneeDico="false" EtiText="Cotisations patronales mensuelles" EtiWidth="250px" DonWidth="82px" DonTabIndex="9" />
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Left">
                                    <Virtualia:VDuoEtiquetteCommande ID="Dontab08" runat="server" V_PointdeVue="1" V_Objet="189" V_Information="8" V_SiDonneeDico="false" Etivisible="false" DonWidth="280px" DonTabIndex="10" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH09" runat="server" V_PointdeVue="1" V_Objet="189" V_Information="9" V_SiDonneeDico="false" EtiText="Primes mensuelles" EtiWidth="250px" DonWidth="82px" DonTabIndex="11" />
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Left">
                                    <Virtualia:VDuoEtiquetteCommande ID="Dontab10" runat="server" V_PointdeVue="1" V_Objet="189" V_Information="10" V_SiDonneeDico="false" Etivisible="false" DonWidth="280px" DonTabIndex="12" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH11" runat="server" V_PointdeVue="1" V_Objet="189" V_Information="11" V_SiDonneeDico="false" EtiText="Rémunération annuelle brute" EtiWidth="250px" DonWidth="82px" DonTabIndex="13" />
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Left">
                                    <Virtualia:VDuoEtiquetteCommande ID="Dontab12" runat="server" V_PointdeVue="1" V_Objet="189" V_Information="12" V_SiDonneeDico="false" Etivisible="false" DonWidth="280px" DonTabIndex="14" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH13" runat="server" V_PointdeVue="1" V_Objet="189" V_Information="13" V_SiDonneeDico="false" EtiText="Cotisations salariales annuelles" EtiWidth="250px" DonWidth="82px" DonTabIndex="15" />
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Left">
                                    <Virtualia:VDuoEtiquetteCommande ID="Dontab14" runat="server" V_PointdeVue="1" V_Objet="189" V_Information="14" V_SiDonneeDico="false" Etivisible="false" DonWidth="280px" DonTabIndex="16" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH15" runat="server" V_PointdeVue="1" V_Objet="189" V_Information="15" V_SiDonneeDico="false" EtiText="Cotisations patronales annuelles" EtiWidth="250px" DonWidth="82px" DonTabIndex="17" />
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Left">
                                    <Virtualia:VDuoEtiquetteCommande ID="Dontab16" runat="server" V_PointdeVue="1" V_Objet="189" V_Information="16" V_SiDonneeDico="false" Etivisible="false" DonWidth="280px" DonTabIndex="18" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH17" runat="server" V_PointdeVue="1" V_Objet="189" V_Information="17" V_SiDonneeDico="false" EtiText="Primes annuelles" EtiWidth="250px" DonWidth="82px" DonTabIndex="19" />
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Left">
                                    <Virtualia:VDuoEtiquetteCommande ID="Dontab18" runat="server" V_PointdeVue="1" V_Objet="189" V_Information="18" V_SiDonneeDico="false" Etivisible="false" DonWidth="280px" DonTabIndex="20" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH19" runat="server" V_PointdeVue="1" V_Objet="189" V_Information="19" V_SiDonneeDico="false" EtiText="Heures supplémentaires annuelles" EtiWidth="250px" DonWidth="82px" DonTabIndex="21" />
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Left">
                                    <Virtualia:VDuoEtiquetteCommande ID="Dontab20" runat="server" V_PointdeVue="1" V_Objet="189" V_Information="20" V_SiDonneeDico="false" Etivisible="false" DonWidth="280px" DonTabIndex="22" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH21" runat="server" V_PointdeVue="1" V_Objet="189" V_Information="21" V_SiDonneeDico="false" EtiText="Dépenses médicales annuelles" EtiWidth="250px" DonWidth="82px" DonTabIndex="23" />
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Left">
                                    <Virtualia:VDuoEtiquetteCommande ID="Dontab22" runat="server" V_PointdeVue="1" V_Objet="189" V_Information="22" V_SiDonneeDico="false" Etivisible="false" DonWidth="280px" DonTabIndex="24" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH23" runat="server" V_PointdeVue="1" V_Objet="189" V_Information="23" V_SiDonneeDico="false" EtiText="Complémentaire accident du travail" EtiWidth="250px" DonWidth="82px" DonTabIndex="25" />
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Left">
                                    <Virtualia:VDuoEtiquetteCommande ID="Dontab24" runat="server" V_PointdeVue="1" V_Objet="189" V_Information="24" V_SiDonneeDico="false" Etivisible="false" DonWidth="280px" DonTabIndex="26" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH25" runat="server" V_PointdeVue="1" V_Objet="189" V_Information="25" V_SiDonneeDico="false" EtiText="Complémentaire maladie" EtiWidth="250px" DonWidth="82px" DonTabIndex="27" />
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Left">
                                    <Virtualia:VDuoEtiquetteCommande ID="Dontab26" runat="server" V_PointdeVue="1" V_Objet="189" V_Information="26" V_SiDonneeDico="false" Etivisible="false" DonWidth="280px" DonTabIndex="28" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH27" runat="server" V_PointdeVue="1" V_Objet="189" V_Information="27" V_SiDonneeDico="false" EtiText="Complémentaire retraite" EtiWidth="250px" DonWidth="82px" DonTabIndex="29" />
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Left">
                                    <Virtualia:VDuoEtiquetteCommande ID="Dontab28" runat="server" V_PointdeVue="1" V_Objet="189" V_Information="28" V_SiDonneeDico="false" Etivisible="false" DonWidth="280px" DonTabIndex="30" />
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Table ID="TableauObservations" runat="server" CellPadding="0" CellSpacing="0" Width="640px">
                            <asp:TableRow>
                                <asp:TableCell Height="10px" />
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <Virtualia:VDuoEtiquetteCommande ID="Dontab30" runat="server" V_PointdeVue="1" V_Objet="189" V_Information="30" V_SiDonneeDico="false" EtiText="Motif de la modification" DonTabIndex="31" EtiWidth="231px" DonWidth="400px" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <Virtualia:VCoupleVerticalEtiDonnee ID="InfoV31" runat="server" DonTextMode="true" V_PointdeVue="1" V_Objet="189" V_Information="31" V_SiDonneeDico="false" EtiText="Observations" EtiWidth="633px" DonWidth="631px" DonHeight="80px" DonTabIndex="32" EtiStyle="text-align:center; margin-left: 5px;" Donstyle="margin-left: 5px;" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Height="12px" />
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>
</asp:Table>

