﻿<%@ Control Language="VB" AutoEventWireup="false" Inherits="Virtualia.Net.Fenetre_PER_ENTRETIEN" CodeBehind="PER_ENTRETIEN.ascx.vb" %>

<%@ Register Src="~/Controles/Saisies/VCoupleEtiDonnee.ascx" TagName="VCoupleEtiDonnee" TagPrefix="Virtualia" %>
<%@ Register Src="~/Controles/Saisies/VDuoEtiquetteCommande.ascx" TagName="VDuoEtiquetteCommande" TagPrefix="Virtualia" %>
<%@ Register Src="~/Controles/Saisies/VCoupleVerticalEtiDonnee.ascx" TagName="VCoupleVerticalEtiDonnee" TagPrefix="Virtualia" %>
<%@ Register Src="~/Controles/Saisies/VCocheSimple.ascx" TagName="VCocheSimple" TagPrefix="Virtualia" %>

<%@ Register Src="~/ControlsGeneriques/V_DataGrid.ascx" TagName="VDataGrid" TagPrefix="Generic" %>
<%@ Register Src="~/ControlsGeneriques/V_CommandeCRUD.ascx" TagName="VCrud" TagPrefix="Generic" %>

<asp:Table ID="CadreControle" runat="server" Height="30px" Width="600px" CellPadding="0" CellSpacing="0" HorizontalAlign="Center">
    <asp:TableRow>
        <asp:TableCell>
            <Generic:VDataGrid ID="ListeGrille" runat="server" CadreWidth="600px" SiColonneSelect="true" SiCaseAcocher="false" />
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="CadreInfo" runat="server" BorderStyle="Ridge" BorderWidth="2px" BorderColor="#B0E0D7" Height="600px" Width="750px" HorizontalAlign="Center" Style="margin-top: 3px;">
                <asp:TableRow>
                    <asp:TableCell>
                        <Generic:VCrud ID="CtlCrud" runat="server" Height="22px" Width="244px" HorizontalAlign="Right" CadreStyle="margin-top: 3px; margin-right: 3px" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Table ID="CadreTitre" runat="server" Height="40px" CellPadding="0" Width="500px" CellSpacing="0" HorizontalAlign="Center">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                    <asp:Label ID="Etiquette" runat="server" Text="Synthèse de l'entretien" Height="20px" Width="300px" BackColor="#2FA49B" BorderColor="#B0E0D7" BorderStyle="Groove" BorderWidth="2px" ForeColor="#D7FAF3" Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Small" Style="margin-top: 5px; margin-left: 4px; margin-bottom: 20px; font-style: oblique; text-indent: 5px; text-align: center;" />
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Table ID="CadreDon" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH00" runat="server" V_PointdeVue="1" V_Objet="50" V_Information="0" V_SiDonneeDico="true" EtiWidth="200px" DonWidth="80px" DonTabIndex="1" />
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Left">
                                    <Virtualia:VDuoEtiquetteCommande ID="Dontab17" runat="server" V_PointdeVue="1" V_Objet="50" V_Information="17" V_SiDonneeDico="true" EtiWidth="80px" DonWidth="250px" DonTabIndex="2" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left" ColumnSpan="2">
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH01" runat="server" V_PointdeVue="1" V_Objet="50" V_Information="1" V_SiDonneeDico="true" EtiWidth="200px" DonWidth="250px" DonTabIndex="3" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <Virtualia:VDuoEtiquetteCommande ID="Dontab18" runat="server" V_PointdeVue="1" V_Objet="50" V_Information="18" V_SiDonneeDico="true" EtiWidth="140px" DonWidth="220px" DonTabIndex="4" EtiStyle="margin-top: 10px;" Donstyle="margin-top: 10px;" />
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Left">
                                    <Virtualia:VDuoEtiquetteCommande ID="Dontab20" runat="server" V_PointdeVue="1" V_Objet="50" V_Information="20" V_SiDonneeDico="true" EtiWidth="140px" DonWidth="220px" DonTabIndex="5" EtiStyle="margin-top: 10px;" Donstyle="margin-top: 10px;" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <Virtualia:VDuoEtiquetteCommande ID="Dontab19" runat="server" V_PointdeVue="1" V_Objet="50" V_Information="19" V_SiDonneeDico="true" EtiWidth="140px" DonWidth="220px" DonTabIndex="6" />
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Left">
                                    <Virtualia:VDuoEtiquetteCommande ID="Dontab21" runat="server" V_PointdeVue="1" V_Objet="50" V_Information="21" V_SiDonneeDico="true" EtiWidth="140px" DonWidth="220px" DonTabIndex="7" />
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Table ID="TableauDemandes" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                            <asp:TableRow>
                                <asp:TableCell RowSpan="2" HorizontalAlign="Left" Width="145px">
                                    <Virtualia:VCocheSimple ID="Coche02" runat="server" V_PointdeVue="1" V_Objet="50" V_Information="2" V_SiDonneeDico="true" V_Width="120px" V_Height="58px" V_Style="margin-left:5px; margin-top: 10px;" />
                                </asp:TableCell>
                                <asp:TableCell ColumnSpan="3" HorizontalAlign="Left" Width="540px">
                                    <Virtualia:VDuoEtiquetteCommande ID="Dontab03" runat="server" V_PointdeVue="1" V_Objet="50" V_Information="3" V_SiDonneeDico="true" EtiWidth="120px" DonWidth="420px" DonTabIndex="8" EtiStyle="margin-top: 10px;" DonStyle="margin-top: 10px;" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH04" runat="server" V_PointdeVue="1" V_Objet="50" V_Information="4" V_SiDonneeDico="true" EtiWidth="80px" DonWidth="80px" DonTabIndex="9" EtiStyle="margin-top: -10px;" DonStyle="margin-top: -10px;" />
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Left">
                                    <Virtualia:VDuoEtiquetteCommande ID="DonTab05" runat="server" V_PointdeVue="1" V_Objet="50" V_Information="5" V_SiDonneeDico="true" EtiWidth="50px" DonWidth="225px" DonTabIndex="10" EtiStyle="margin-top: -10px;" DonStyle="margin-top: -10px;" />
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Left">
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH06" runat="server" V_PointdeVue="1" V_Objet="50" V_Information="6" V_SiDonneeDico="true" EtiWidth="30px" DonWidth="80px" DonTabIndex="11" EtiStyle="margin-top: -10px;" DonStyle="margin-top: -10px;" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell RowSpan="2" HorizontalAlign="Left" Width="145px">
                                    <Virtualia:VCocheSimple ID="Coche07" runat="server" V_PointdeVue="1" V_Objet="50" V_Information="7" V_SiDonneeDico="true" V_Width="120px" V_Height="58px" V_Style="margin-left:5px; margin-top: 10px;" />
                                </asp:TableCell>
                                <asp:TableCell ColumnSpan="3" HorizontalAlign="Left" Width="540px">
                                    <Virtualia:VDuoEtiquetteCommande ID="Dontab08" runat="server" V_PointdeVue="1" V_Objet="50" V_Information="8" V_SiDonneeDico="true" EtiWidth="120px" DonWidth="420px" DonTabIndex="12" EtiStyle="margin-top: 10px;" DonStyle="margin-top: 10px;" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH09" runat="server" V_PointdeVue="1" V_Objet="50" V_Information="9" V_SiDonneeDico="true" EtiWidth="80px" DonWidth="80px" DonTabIndex="13" EtiStyle="margin-top: -10px;" DonStyle="margin-top: -10px;" />
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Left">
                                    <Virtualia:VDuoEtiquetteCommande ID="Dontab10" runat="server" V_PointdeVue="1" V_Objet="50" V_Information="10" V_SiDonneeDico="true" EtiWidth="50px" DonWidth="225px" DonTabIndex="14" EtiStyle="margin-top: -10px;" DonStyle="margin-top: -10px;" />
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Left">
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH11" runat="server" V_PointdeVue="1" V_Objet="50" V_Information="11" V_SiDonneeDico="true" EtiWidth="30px" DonWidth="80px" DonTabIndex="15" EtiStyle="margin-top: -10px;" DonStyle="margin-top: -10px;" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell RowSpan="2" HorizontalAlign="Left" Width="145px">
                                    <Virtualia:VCocheSimple ID="Coche12" runat="server" V_PointdeVue="1" V_Objet="50" V_Information="12" V_SiDonneeDico="true" V_Width="120px" V_Height="58px" V_Style="margin-left:5px; margin-top: 10px;" />
                                </asp:TableCell>
                                <asp:TableCell ColumnSpan="3" HorizontalAlign="Left" Width="540px">
                                    <Virtualia:VDuoEtiquetteCommande ID="Dontab13" runat="server" V_PointdeVue="1" V_Objet="50" V_Information="13" V_SiDonneeDico="true" EtiWidth="120px" DonWidth="420px" DonTabIndex="16" EtiStyle="margin-top: 10px;" DonStyle="margin-top: 10px;" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH14" runat="server" V_PointdeVue="1" V_Objet="50" V_Information="14" V_SiDonneeDico="true" EtiWidth="80px" DonWidth="80px" DonTabIndex="17" EtiStyle="margin-top: -10px;" DonStyle="margin-top: -10px;" />
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Left">
                                    <Virtualia:VDuoEtiquetteCommande ID="Dontab15" runat="server" V_PointdeVue="1" V_Objet="50" V_Information="15" V_SiDonneeDico="true" EtiWidth="50px" DonWidth="225px" DonTabIndex="18" EtiStyle="margin-top: -10px;" DonStyle="margin-top: -10px;" />
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Left">
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH16" runat="server" V_PointdeVue="1" V_Objet="50" V_Information="16" V_SiDonneeDico="true" EtiWidth="30px" DonWidth="80px" DonTabIndex="19" EtiStyle="margin-top: -10px;" DonStyle="margin-top: -10px;" />
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Table ID="TableauCommentaires" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                            <asp:TableRow>
                                <asp:TableCell Height="10px" ColumnSpan="2" />
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <Virtualia:VCoupleVerticalEtiDonnee ID="InfoV22" runat="server" DonTextMode="true" V_PointdeVue="1" V_Objet="50" V_Information="22" V_SiDonneeDico="true" EtiWidth="365px" DonWidth="363px" DonHeight="120px" DonTabIndex="20" EtiStyle="text-align:center; margin-left: 5px;" Donstyle="margin-left: 5px;" />
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Left">
                                    <Virtualia:VCoupleVerticalEtiDonnee ID="InfoV23" runat="server" DonTextMode="true" V_PointdeVue="1" V_Objet="50" V_Information="23" V_SiDonneeDico="true" EtiWidth="365px" DonWidth="363px" DonHeight="120px" DonTabIndex="21" EtiStyle="text-align:center; margin-left: 0px;" Donstyle="margin-left: 0px;" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Height="12px" ColumnSpan="2" />
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>
</asp:Table>
