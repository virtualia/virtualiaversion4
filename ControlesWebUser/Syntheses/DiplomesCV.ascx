﻿<%@ Control Language="VB" AutoEventWireup="false" Inherits="Virtualia.Net.Controles_DiplomesCV" Codebehind="DiplomesCV.ascx.vb" %>

<asp:ListView ID="FichePersonne" runat="server">
    <LayoutTemplate>
        <table id="Cadre" runat="server" width="600px" cellpadding="1" cellspacing="0"
                style=" border-style:ridge; border-color:#B0E0D7; border-width:3px; max-width:800px; 
                background-color:White">
            <tr runat="server" id="itemPlaceholder">
            </tr>
        </table>
        <asp:DataPager runat="server" ID="VirtuDataPager" PageSize="1" >
            <Fields>
                <asp:NumericPagerField ButtonCount="5" PreviousPageText="<--" NextPageText="-->" />
            </Fields>
        </asp:DataPager>
    </LayoutTemplate>
    <ItemTemplate>
        <tr id="TitreL1" runat="server" align="left" style="margin-top:2px; height:20px">
            <td valign="top" align="center">
               <asp:Label ID="EtiFiche" runat="server" Height="20px" Width="400px" Text="CURRICULUM-VITAE"
                    BackColor="Transparent" BorderColor="#B0E0D7"  BorderStyle="None"
                    BorderWidth="2px" ForeColor="#121B1C" Font-Italic="false"
                    Font-Bold="true" Font-Names="Trebuchet MS" Font-Size="Small"
                    style="margin-top: 10px; margin-left: 4px; text-indent: 5px; text-align: center" >
                </asp:Label>
            </td>
        </tr>
        <tr id="TR00" runat="server" style="margin-top:5px; height:20px; padding:0px;">
            <td valign="top" align="left">
                <asp:Label ID="Identite" runat="server" Height="20px" Width="300px" Text='<%#Eval("Colonne_1") %>'
                    BorderStyle="None" ForeColor="#142425"  BackColor="Transparent"
                    Font-Italic="false" Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Small"
                    style="margin-top: 10px; margin-left: 5px; text-indent: 5px; text-align: left" >
                </asp:Label>
            </td>
        </tr>
        <tr id="TR01" runat="server" style="margin-top:2px; height:15px; padding:0px">
            <td valign="top" align="left" >
                <asp:Label ID="Naissance" runat="server" Height="15px" Width="300px" Text='<%#Eval("Colonne_2") %>'
                    BorderStyle="None" ForeColor="#142425"  BackColor="Transparent"
                    Font-Italic="false" Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Small"
                    style="margin-top: 2px; margin-left: 5px; text-indent: 5px; text-align: left" >
                </asp:Label>
            </td>
        </tr>
        <tr id="TR02" runat="server" style="margin-top:5px; height:15px; padding:0px">
            <td valign="top" align="left" >
                <asp:Label ID="NomRue" runat="server" Height="15px" Width="300px" Text='<%#Eval("Colonne_3") %>'
                    BorderStyle="None" ForeColor="#142425"  BackColor="Transparent"
                    Font-Italic="false" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                    style="margin-top: 5px; margin-left: 5px; text-indent: 5px; text-align: left" >
                </asp:Label>
            </td>
        </tr>
        <tr id="TR03" runat="server" style="margin-top:2px; height:15px; padding:0px">
            <td valign="top" align="left">
                <asp:Label ID="Complement" runat="server" Height="15px" Width="300px" Text='<%#Eval("Colonne_4") %>'
                    BorderStyle="None" ForeColor="#142425"  BackColor="Transparent"
                    Font-Italic="false" Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Small"
                    style="margin-top: 0px; margin-left: 5px; text-indent: 5px; text-align: left" >
                </asp:Label>
            </td>
        </tr>
        <tr id="TR04" runat="server" style="margin-top:2px; height:15px; padding:0px">
            <td valign="top" align="left" >
                <asp:Label ID="CPVille" runat="server" Height="15px" Width="300px" Text='<%#Eval("Colonne_5") %>'
                    BorderStyle="None" ForeColor="#142425"  BackColor="Transparent"
                    Font-Italic="false" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                    style="margin-top: 0px; margin-left: 5px; text-indent: 5px; text-align: left" >
                </asp:Label>
            </td>
        </tr>
        <tr id="TR05" runat="server" style="margin-top:2px; height:15px; padding:0px">
            <td valign="top" align="left">
                <asp:Label ID="Email" runat="server" Height="15px" Width="300px" Text='<%#Eval("Colonne_6") %>'
                    BorderStyle="None" ForeColor="#142425"  BackColor="Transparent"
                    Font-Italic="false" Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Small"
                    style="margin-top: 5px; margin-left: 5px; text-indent: 5px; text-align: left" >
                </asp:Label>
            </td>
        </tr>
        <tr id="TR15" runat="server" style="margin-top:10px; padding:0px;">
            <td valign="top" align="left" >
                <asp:Label ID="EtiDiplomes" runat="server" Height="15px" Width="300px" Text="DIPLOMES"
                    BorderStyle="None" ForeColor="#121B1C"  BackColor="Transparent"
                    Font-Italic="false" Font-Bold="true" Font-Names="Trebuchet MS" Font-Size="Small"
                    style="margin-top: 10px; margin-left: 5px; text-indent: 5px; text-align: left" >
                </asp:Label>
            </td>
        </tr>
        <tr id="TR06" runat="server" style="margin-top:2px; height:15px; padding:0px">
            <td valign="top" align="left" >
                <asp:Label ID="Diplome1" runat="server" Height="15px" Width="300px" Text='<%#Eval("Colonne_7") %>'
                    BorderStyle="None" ForeColor="#142425"  BackColor="Transparent"
                    Font-Italic="false" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                    style="margin-top: 5px; margin-left: 5px; text-indent: 5px; text-align: left" >
                </asp:Label>
            </td>
        </tr>
        <tr id="TR07" runat="server" style="margin-top:2px; height:15px; padding:0px">
            <td valign="top" align="left" >
                <asp:Label ID="Diplome2" runat="server" Height="15px" Width="300px" Text='<%#Eval("Colonne_8") %>'
                    BorderStyle="None" ForeColor="#142425"  BackColor="Transparent"
                    Font-Italic="false" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                    style="margin-top: 1px; margin-left: 5px; text-indent: 5px; text-align: left" >
                </asp:Label>
            </td>
        </tr>
        <tr id="TR08" runat="server" style="margin-top:2px; height:15px; padding:0px">
            <td valign="top" align="left" >
                <asp:Label ID="Diplome3" runat="server" Height="15px" Width="300px" Text='<%#Eval("Colonne_9") %>'
                    BorderStyle="None" ForeColor="#142425"  BackColor="Transparent"
                    Font-Italic="false" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                    style="margin-top: 1px; margin-left: 5px; text-indent: 5px; text-align: left" >
                </asp:Label>
            </td>
        </tr>
        <tr id="TR09" runat="server" style="margin-top:2px; height:15px; padding:0px">
            <td valign="top" align="left" >
                <asp:Label ID="Diplome4" runat="server" Height="15px" Width="300px" Text='<%#Eval("Colonne_10") %>'
                    BorderStyle="None" ForeColor="#142425"  BackColor="Transparent"
                    Font-Italic="false" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                    style="margin-top: 1px; margin-left: 5px; text-indent: 5px; text-align: left" >
                </asp:Label>
            </td>
        </tr>
        <tr id="TR10" runat="server" style="margin-top:2px; height:15px; padding:0px">
            <td valign="top" align="left" >
                <asp:Label ID="Diplome5" runat="server" Height="15px" Width="300px" Text='<%#Eval("Colonne_11") %>'
                    BorderStyle="None" ForeColor="#142425"  BackColor="Transparent"
                    Font-Italic="false" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                    style="margin-top: 1px; margin-left: 5px; text-indent: 5px; text-align: left" >
                </asp:Label>
            </td>
        </tr>
        <tr id="TR11" runat="server" style="margin-top:10px; padding:0px;">
            <td valign="top" align="left" >
                <asp:Label ID="EtiExperience" runat="server" Height="15px" Width="300px" Text="EXPERIENCE PROFESSIONNELLE"
                    BorderStyle="None" ForeColor="#121B1C"  BackColor="Transparent"
                    Font-Italic="false" Font-Bold="true" Font-Names="Trebuchet MS" Font-Size="Small"
                    style="margin-top: 10px; margin-left: 5px; text-indent: 5px; text-align: left" >
                </asp:Label>
            </td>
        </tr>
        <tr id="TR12" runat="server" style="margin-top:5px; padding:0px;">
            <td valign="top" align="center" >
                <asp:TextBox ID="Experience" runat="server" Height="300px" Width="500px" Text='<%#Eval("Colonne_12") %>'
                    BorderStyle="None" ForeColor="#142425"  BackColor="Transparent" ReadOnly="true" TextMode="MultiLine"
                    Font-Italic="false" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                    style="margin-top: 2px; text-indent: 5px; text-align: left" >
                </asp:TextBox>
            </td>
        </tr>
        <tr id="TR13" runat="server" style="margin-top:10px; padding:0px;">
            <td valign="top" align="left" >
                <asp:Label ID="EtiAcquis" runat="server" Height="15px" Width="300px" Text="ACQUIS TECHNIQUES"
                    BorderStyle="None" ForeColor="#121B1C"  BackColor="Transparent"
                    Font-Italic="false" Font-Bold="true" Font-Names="Trebuchet MS" Font-Size="Small"
                    style="margin-top: 10px; margin-left: 5px; text-indent: 5px; text-align: left" >
                </asp:Label>
            </td>
        </tr>
        <tr id="TR14" runat="server" style="margin-top:5px; padding:0px;">
            <td valign="top" align="center" >
                <asp:TextBox ID="Acquis" runat="server" Height="200px" Width="500px" Text='<%#Eval("Colonne_13") %>'
                    BorderStyle="None" ForeColor="#142425"  BackColor="Transparent" ReadOnly="true" TextMode="MultiLine"
                    Font-Italic="false" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                    style="margin-top: 2px; text-indent: 5px; text-align: left" >
                </asp:TextBox>
            </td>
        </tr>
    </ItemTemplate>
</asp:ListView>