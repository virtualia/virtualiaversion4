﻿Option Strict On
Option Explicit On
Option Compare Text
Imports VI = Virtualia.Systeme.Constantes
Partial Class Controles_DiplomesCV
    Inherits System.Web.UI.UserControl
    Private WebFct As Virtualia.Net.WebFonctions

    Public Property V_Liste() As ArrayList
        Get
            If Me.ViewState("EtatCourant") Is Nothing Then
                Return Nothing
                Exit Property
            End If
            Dim VCache As ArrayList
            VCache = CType(Me.ViewState("EtatCourant"), ArrayList)
            Return VCache
        End Get
        Set(ByVal value As ArrayList)
            If value Is Nothing Then
                Exit Property
            End If
            If Me.ViewState("EtatCourant") IsNot Nothing Then
                Me.ViewState.Remove("EtatCourant")
            End If
            Me.ViewState.Add("EtatCourant", value)

            Call FaireListe(0)
        End Set
    End Property

    Private Sub FaireListe(ByVal NoPage As Integer)
        Dim VCache As ArrayList
        Dim IndiceO As Integer
        Dim TableauObjet(0) As String
        Dim TableauData(0) As String
        Dim IndiceA As Integer
        Dim IndiceC As Integer
        Dim VClasseData As Virtualia.Systeme.Datas.ObjetIlistGrid
        Dim Datas As ArrayList
        Dim Idebut As Integer

        If Me.ViewState("EtatCourant") Is Nothing Then
            Exit Sub
        End If
        VCache = CType(Me.ViewState("EtatCourant"), ArrayList)
        IndiceO = -1
        Datas = New ArrayList
        Do
            IndiceO += 1
            If IndiceO > VCache.Count - 1 Then
                Exit Do
            End If
            TableauObjet = Strings.Split(VCache(IndiceO).ToString, VI.SigneBarre, -1)
            For IndiceA = 0 To TableauObjet.Count - 1
                If TableauObjet(IndiceA) = "" Then
                    Exit For
                End If
                TableauData = Strings.Split(TableauObjet(IndiceA), VI.Tild, -1)
                VClasseData = New Virtualia.Systeme.Datas.ObjetIlistGrid()
                For IndiceC = Idebut To TableauData.Count - 1
                    VClasseData.Valeur(IndiceC) = TableauData(IndiceC)
                Next IndiceC
                Datas.Add(VClasseData)
            Next IndiceA
        Loop
        FichePersonne.DataSource = Datas
        FichePersonne.DataBind()

        Dim CtlTr As System.Web.UI.HtmlControls.HtmlTableRow = Nothing
        Dim CtlTd As System.Web.UI.HtmlControls.HtmlTableCell = Nothing
        Dim CtlInfo As System.Web.UI.WebControls.Label
        Dim CtlTxt As System.Web.UI.WebControls.TextBox = Nothing
        Dim Ctl As Control
        Dim IndiceK As Integer
        Dim Rupture As String = ""
        For IndiceA = 0 To FichePersonne.Items.Count - 1
            For IndiceC = 0 To FichePersonne.Items.Item(IndiceA).Controls.Count - 1
                If TypeOf (FichePersonne.Items.Item(IndiceA).Controls.Item(IndiceC)) Is System.Web.UI.HtmlControls.HtmlTableRow Then
                    CtlTr = CType(FichePersonne.Items.Item(IndiceA).Controls.Item(IndiceC), System.Web.UI.HtmlControls.HtmlTableRow)
                    For IndiceO = 0 To CtlTr.Controls.Count - 1
                        Ctl = CtlTr.Controls.Item(IndiceO)
                        If TypeOf (Ctl) Is System.Web.UI.HtmlControls.HtmlTableCell Then
                            CtlTd = CType(Ctl, System.Web.UI.HtmlControls.HtmlTableCell)
                            For IndiceK = 0 To CtlTd.Controls.Count - 1
                                Ctl = CtlTd.Controls.Item(IndiceK)
                                If TypeOf (Ctl) Is System.Web.UI.WebControls.Label Then
                                    CtlInfo = CType(Ctl, System.Web.UI.WebControls.Label)
                                    If CtlInfo.Text = "" Then
                                        CtlTr.Visible = False
                                    Else
                                        CtlTr.Visible = True
                                    End If
                                End If
                                If TypeOf (Ctl) Is System.Web.UI.WebControls.TextBox Then
                                    CtlTxt = CType(Ctl, System.Web.UI.WebControls.TextBox)
                                    If CtlTxt.Text Is Nothing Or CtlTxt.Text = "" Then
                                        CtlTxt.Visible = False
                                        CtlTr.Visible = False
                                    Else
                                        CtlTxt.Visible = True
                                        CtlTr.Visible = True
                                    End If
                                End If
                            Next IndiceK
                        End If
                    Next IndiceO
                End If
            Next IndiceC
        Next IndiceA

    End Sub

    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        WebFct = New Virtualia.Net.WebFonctions(Me, 0)
    End Sub

    Protected Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
        WebFct.Dispose()
    End Sub
End Class
