﻿<%@ Control Language="VB" AutoEventWireup="false" Inherits="Virtualia.Net.Controles_FicheAffectation" Codebehind="FicheAffectation.ascx.vb" %>

<asp:ListView ID="FicheAffectation" runat="server" >
    <LayoutTemplate>
        <table id="Cadre" runat="server" width="700px" cellpadding="1" cellspacing="1"
                style=" border-style:ridge; border-color:#B0E0D7; border-width:4px; max-width:700px; 
                background-color:White; min-height:220px; max-height:300px; 
                vertical-align:top; text-align:left; margin-top:30px ">
            <tr runat="server" id="itemPlaceholder">
            </tr>
        </table>
        <asp:DataPager runat="server" ID="VirtuDataPager" PageSize="1">
            <Fields>
                <asp:NumericPagerField ButtonCount="5" PreviousPageText="<--" NextPageText="-->" />
            </Fields>
        </asp:DataPager>
    </LayoutTemplate>
    <ItemTemplate>
        <tr id="CadreAnnuaire" runat="server" align="left" valign="top" 
            style="border-width:2px; border-color: Gray;" >
            <td>
                <table id="TablePhoto" runat="server" width="150px"
                       style="height:200px; background-color:white; border-style:solid; 
                              border-width:2px; border-color: Gray; margin-left:15px; margin-top:10px">
                    <tr id="TR01" runat="server" align="center" style="margin-top:5px; padding:0px; vertical-align:middle">
                        <td>
                            <asp:Image ID="Photo" runat="server" ImageAlign="Middle" ImageUrl='<%#Eval("Colonne_1") %>' />
                        </td>
                    </tr>
                </table>
            </td>
            <td>
                <table id="CadreAffectation" runat="server" cellpadding="0" cellspacing="0"
                        style="height:200px; background-color:white; margin-left:15px; margin-top:10px;
                                vertical-align:top; text-align:left ">
                    <tr id="TR02" runat="server" style="margin-top:5px; padding:0px;">
                        <td>
                            <asp:Label ID="NomPrenom" runat="server" Width="530px" Height="15px" Text='<%#Eval("Colonne_2") %>'
                                BorderStyle="None" ForeColor="Blue" BackColor="Transparent"
                                Font-Italic="false" Font-Bold="true" Font-Names="Trebuchet MS" Font-Size="Small"
                                style="margin-top: 0px; margin-left: 5px; text-indent: 5px; text-align: left" >
                            </asp:Label>
                        </td>
                    </tr>
                    <tr id="TR03" runat="server" >
                        <td>
                            <asp:Label ID="Emploi" runat="server" Height="15px" Text='<%#Eval("Colonne_3") %>'
                                BorderStyle="None" ForeColor="#142425"  BackColor="Transparent"
                                Font-Italic="false" Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Small"
                                style="margin-top: 0px; margin-left: 5px; text-indent: 5px; text-align: left" >
                            </asp:Label>
                        </td>
                    </tr>    
                    <tr id="TR04" runat="server" >
                        <td>
                            <asp:Label ID="Etablissement" runat="server" Height="15px" Text='<%#Eval("Colonne_4") %>'
                                BorderStyle="None" ForeColor="Green"  BackColor="Transparent"
                                Font-Italic="false" Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Small"
                                style="margin-top: 0px; margin-left: 5px; text-indent: 5px; text-align: left" >
                            </asp:Label>
                        </td>
                    </tr>    
                    <tr id="TR05" runat="server" >    
                        <td>
                            <asp:Label ID="Niveau1" runat="server" Height="15px" Text='<%#Eval("Colonne_5") %>'
                                BorderStyle="None" ForeColor="Green"  BackColor="Transparent"
                                Font-Italic="false" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                style="margin-top: 0px; margin-left: 15px; text-indent: 5px; text-align: left" >
                            </asp:Label>
                        </td>
                    </tr>
                    <tr id="TR06" runat="server" >
                        <td>
                            <asp:Label ID="Niveau2" runat="server" Height="15px" Text='<%#Eval("Colonne_6") %>'
                                BorderStyle="None" ForeColor="Green"  BackColor="Transparent"
                                Font-Italic="false" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                style="margin-top: 0px; margin-left: 25px; text-indent: 5px; text-align: left" >
                            </asp:Label>
                        </td>
                     </tr>
                     <tr id="TR07" runat="server" >
                        <td>
                            <asp:Label ID="Niveau3" runat="server" Height="15px" Text='<%#Eval("Colonne_7") %>'
                                BorderStyle="None" ForeColor="Green"  BackColor="Transparent"
                                Font-Italic="false" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                style="margin-top: 0px; margin-left: 35px; text-indent: 5px; text-align: left" >
                            </asp:Label>
                        </td>
                     </tr>
                     <tr id="TR08" runat="server" >
                        <td>
                            <asp:Label ID="Niveau4" runat="server" Height="15px" Text='<%#Eval("Colonne_8") %>'
                                BorderStyle="None" ForeColor="Green"  BackColor="Transparent"
                                Font-Italic="false" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                style="margin-top: 0px; margin-left: 45px; text-indent: 5px; text-align: left" >
                            </asp:Label>
                        </td>
                    </tr>
                    <tr id="TR09" runat="server" >
                        <td>
                            <asp:Label ID="Niveau5" runat="server" Height="15px" Text='<%#Eval("Colonne_9") %>'
                                BorderStyle="None" ForeColor="Green"  BackColor="Transparent"
                                Font-Italic="false" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                style="margin-top: 0px; margin-left: 55px; text-indent: 5px; text-align: left" >
                            </asp:Label>
                        </td>
                    </tr>
                    <tr id="TR10" runat="server" >
                        <td>
                            <asp:Label ID="Niveau6" runat="server" Height="15px" Text='<%#Eval("Colonne_10") %>'
                                BorderStyle="None" ForeColor="Green"  BackColor="Transparent"
                                Font-Italic="false" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                style="margin-top: 0px; margin-left: 65px; text-indent: 5px; text-align: left" >
                            </asp:Label>
                        </td>
                    </tr>
                    <tr id="TR11" runat="server" >
                        <td>
                            <asp:Label ID="Mail" runat="server" Height="15px" Text='<%#Eval("Colonne_11") %>'
                                BorderStyle="None" ForeColor="Blue"  BackColor="Transparent"
                                Font-Italic="false" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                style="margin-top: 0px; margin-left: 5px; text-indent: 5px; text-align: left" >
                            </asp:Label>
                        </td>
                    </tr>
                    <tr id="TR12" runat="server" >
                        <td>
                            <asp:Label ID="Telephone" runat="server" Height="15px" Text='<%#Eval("Colonne_12") %>'
                                BorderStyle="None" ForeColor="#142425"  BackColor="Transparent"
                                Font-Italic="false" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                style="margin-top: 0px; margin-left: 5px; text-indent: 5px; text-align: left" >
                            </asp:Label>
                        </td>
                    </tr>
                    <tr id="TR13" runat="server" >
                        <td>
                            <asp:Label ID="Bureau" runat="server" Height="15px" Text='<%#Eval("Colonne_13") %>'
                                BorderStyle="None" ForeColor="#142425"  BackColor="Transparent"
                                Font-Italic="false" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                style="margin-top: 0px; margin-left: 5px; text-indent: 5px; text-align: left" >
                            </asp:Label>
                        </td>
                    </tr>
                </table>
            </td>
         </tr>
    </ItemTemplate>
</asp:ListView>