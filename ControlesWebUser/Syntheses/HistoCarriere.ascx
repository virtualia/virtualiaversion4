﻿<%@ Control Language="VB" AutoEventWireup="false" Inherits="Virtualia.Net.Controles_HistoCarriere" Codebehind="HistoCarriere.ascx.vb" %>

<%@ Register src="../Saisies/VListeGrid.ascx" tagname="VListeGrid" tagprefix="Virtualia" %>
 
 <asp:Table ID="CadreHisto" runat="server" Height="30px" Width="780px" CellPadding="0" CellSpacing="0"
                  HorizontalAlign="Center" BorderColor="#B0E0D7"  BorderStyle="Inset" BorderWidth="1px">
    <asp:TableRow>
        <asp:TableCell BackColor="#0E5F5C" Height="30px" VerticalAlign="Middle">
            <asp:Label ID="EtiPrincipal" runat="server" Height="25px" Width="780px" Visible="false"
                    BackColor="Transparent" BorderStyle="None" BorderWidth="1px" ForeColor="White" 
                    Font-Italic="true" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Medium"
                    style="margin-top: 0px; margin-left: 10px; text-indent: 5px; text-align: left"
                    Text="Historique de carriere" >
            </asp:Label>
        </asp:TableCell>
     </asp:TableRow>
    <asp:TableRow> 
         <asp:TableCell> 
            <Virtualia:VListeGrid ID="HPrincipal" runat="server"  CadreWidth="780px"
                SiColonneSelect="false" />
         </asp:TableCell>
     </asp:TableRow>
     <asp:TableRow>
        <asp:TableCell Height="10px"></asp:TableCell>
     </asp:TableRow>
     <asp:TableRow>
        <asp:TableCell BackColor="#0E5F5C" Height="30px" VerticalAlign="Middle">
            <asp:Label ID="EtiSecondaire" runat="server" Height="25px" Width="780px" Visible="false"
                    BackColor="Transparent" BorderStyle="None" BorderWidth="1px" ForeColor="White" 
                    Font-Italic="true" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Medium"
                    style="margin-top: 0px; margin-left: 10px; text-indent: 5px; text-align: left"
                    Text="Emploi fonctionnel - Statut d'emploi - Détachement" >
            </asp:Label>
        </asp:TableCell>
     </asp:TableRow>
     <asp:TableRow> 
         <asp:TableCell> 
            <Virtualia:VListeGrid ID="HDoubleCarriere" runat="server"  CadreWidth="780px"
                SiColonneSelect="false" BackColorCaption="#CBF2BE" BackColorRow="#E2FDD9"  />
         </asp:TableCell>
     </asp:TableRow>
 </asp:Table>