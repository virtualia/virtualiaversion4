﻿<%@ Control Language="VB" AutoEventWireup="false" Inherits="Virtualia.Net.Controles_ActionIntranet" Codebehind="ActionIntranet.ascx.vb" %>

<asp:ListView ID="FicheAction" runat="server">
    <LayoutTemplate>
        <table id="Cadre" runat="server" width="600px" cellpadding="0" cellspacing="0"
                style="border-style:ridge; border-color:#B0E0D7; border-width:3px; 
                background-color:Transparent;">
            <tr runat="server" id="itemPlaceholder">
            </tr>
        </table>
        <asp:DataPager runat="server" ID="VirtuDataPager" PageSize="1">
            <Fields>
                <asp:NumericPagerField ButtonCount="5" PreviousPageText="<--" NextPageText="-->" />
            </Fields>
        </asp:DataPager>
    </LayoutTemplate>
    <ItemTemplate>
        <tr id="Ligne1" runat="server">
            <td valign="top" colspan="2">
               <asp:Label ID="EtiFiche" runat="server" Height="30px" Width="600px" Text="Titre de la fiche"
                    BackColor="#B0E0D7" BorderStyle="None" ForeColor="#142425" 
                    Font-Italic="false" Font-Bold="true" Font-Names="Trebuchet MS" Font-Size="Small"
                    style="margin-top: 1px; text-indent: 2px; text-align: center; vertical-align: middle;
                            border-bottom-width: 2px; border-bottom-style: solid; border-bottom-color: #399280" >
                </asp:Label>
            </td>
        </tr>
        <tr id="Ligne2" runat="server">
            <td valign="top">
               <asp:Label ID="EtiNoDemande" runat="server" Height="20px" Width="300px" Text='<%#Eval("Colonne_1") %>'
                    BackColor="Transparent" BorderStyle="None" ForeColor="#142425"
                    Font-Italic="True" Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Smaller"
                    style="margin-top: 1px; text-indent: 5px; text-align: left;">
                </asp:Label>
            </td>
            <td valign="top">
               <asp:Label ID="EtiDecision" runat="server" Height="20px" Width="300px" Text="Décision"
                    BackColor="Transparent" BorderStyle="None" ForeColor="#142425" 
                    Font-Italic="True" Font-Bold="true" Font-Names="Trebuchet MS" Font-Size="Smaller"
                    style="margin-left: 0px; margin-top: 5px; text-indent: 5px; text-align: left;
                            border-left-width: 2px; border-left-style: solid; border-left-color: #399280;">
                </asp:Label>
            </td>
        </tr>
        <tr id="Ligne3" runat="server">
            <td valign="top">
               <asp:Label ID="EtiDateDemande" runat="server" Height="40px" Width="300px" Text='<%#Eval("Colonne_2") %>'
                    BackColor="Transparent" BorderStyle="None" ForeColor="#142425"
                    Font-Italic="False" Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Smaller"
                    style="margin-top: 1px; text-indent: 5px; text-align: center;">
                </asp:Label>
            </td>
            <td valign="top">
               <asp:Label ID="EtiDateDecision" runat="server" Height="40px" Width="300px" Text='<%#Eval("Colonne_3") %>'
                   BackColor="Transparent" BorderStyle="None" ForeColor="#142425" 
                    Font-Italic="False" Font-Bold="true" Font-Names="Trebuchet MS" Font-Size="Smaller"
                    style="margin-left: 0px; margin-top: 5px; text-indent: 5px; text-align: center;
                            border-left-width: 2px; border-left-style: solid; border-left-color: #399280;">
                </asp:Label>
            </td>
        </tr>
        <tr id="Ligne4" runat="server">
            <td valign="top" colspan="2">
               <asp:Label ID="EtiContenu" runat="server" Height="20px" Width="600px" Text="Contenu"
                    BackColor="Transparent" BorderColor="#399280"  BorderStyle="None"
                    BorderWidth="2px" ForeColor="#142425" Font-Italic="True"
                    Font-Bold="true" Font-Names="Trebuchet MS" Font-Size="Smaller"
                    style="margin-top: 5px; text-indent: 5px; text-align: left; border-top-style: solid" >
                </asp:Label>
            </td>
        </tr>
        <tr id="Ligne5" runat="server">
            <td valign="top" colspan="2" align="center" style="height: 140px;">
               <asp:TextBox ID="TxtContenu" runat="server" Height="120px" Width="590px" 
                     TextMode="MultiLine" ReadOnly="true" BorderStyle="None" Text='<%#Eval("Colonne_4") %>'
                     Font-Bold="true" Font-Names="Trebuchet MS" Font-Size="Smaller"
                     style="margin-top: 10px; text-align: center">
               </asp:TextBox>
            </td>
        </tr>
        <tr id="Ligne6" runat="server">
            <td valign="top">
               <asp:Label ID="TitreObservation" runat="server" Height="20px" Width="300px" Text="Observations"
                    BackColor="Transparent" BorderStyle="None" ForeColor="#142425"
                    Font-Italic="True" Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Smaller"
                    style="margin-top: 2px; text-indent: 5px; text-align: left;
                            border-top-width: 2px; border-top-style: solid; border-top-color: #399280;">
                </asp:Label>
            </td>
            <td valign="top">
               <asp:Label ID="TitreJustification" runat="server" Height="20px" Width="300px" Text="Justification"
                    BackColor="Transparent" BorderStyle="None" ForeColor="#142425" 
                    Font-Italic="True" Font-Bold="true" Font-Names="Trebuchet MS" Font-Size="Smaller"
                    style="margin-top: 2px; text-indent: 5px; text-align: left;
                            border-top-width: 2px; border-top-style: solid; border-top-color: #399280;
                            border-left-width: 2px; border-left-style: solid; border-left-color: #399280;">
                </asp:Label>
            </td>
        </tr>
        <tr id="Ligne7" runat="server">
            <td valign="top">
               <asp:Label ID="EtiObservation" runat="server" Height="40px" Width="300px" Text='<%#Eval("Colonne_5") %>'
                   BackColor="Transparent" BorderStyle="None" ForeColor="#142425"
                    Font-Italic="False" Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Smaller"
                    style="text-align: center;">
                </asp:Label>
            </td>
            <td valign="top">
               <asp:Label ID="EtiJustification" runat="server" Height="40px" Width="300px" Text='<%#Eval("Colonne_6") %>'
                    BackColor="Transparent" BorderStyle="None" ForeColor="#142425" 
                    Font-Italic="False" Font-Bold="true" Font-Names="Trebuchet MS" Font-Size="Smaller"
                    style="text-align: center; border-left-width: 2px; border-left-style: solid; 
                            border-left-color: #399280;">
                </asp:Label>
            </td>
        </tr>
    </ItemTemplate>
</asp:ListView>