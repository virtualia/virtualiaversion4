﻿Option Strict On
Option Explicit On
Option Compare Text
Imports VI = Virtualia.Systeme.Constantes
Partial Class Controles_HistoCarriere
    Inherits System.Web.UI.UserControl
    Private WebFct As Virtualia.Net.WebFonctions

    Public WriteOnly Property Identifiant As Integer
        Set(ByVal value As Integer)
            If value = 0 Then
                Exit Property
            End If

            WebFct = New Virtualia.Net.WebFonctions(Me, 0)

            Dim Dossier As Virtualia.Ressources.Datas.ObjetDossierPER = WebFct.PointeurDossier(value)
            Dim ListeObjet = New System.Text.StringBuilder
            Dim Cretour As Boolean
            ListeObjet.Append(VI.ObjetPer.ObaCivil & VI.PointVirgule)
            ListeObjet.Append(VI.ObjetPer.ObaStatut & VI.PointVirgule)
            ListeObjet.Append(VI.ObjetPer.ObaActivite & VI.PointVirgule)
            ListeObjet.Append(VI.ObjetPer.ObaGrade & VI.PointVirgule)
            ListeObjet.Append(VI.ObjetPer.ObaStatutDetache & VI.PointVirgule)
            ListeObjet.Append(VI.ObjetPer.ObaPositionDetache & VI.PointVirgule)
            ListeObjet.Append(VI.ObjetPer.ObaGradeDetache & VI.PointVirgule)
            Cretour = Dossier.LireDossier(WebFct.PointeurUtilisateur.V_NomdUtilisateurSgbd, ListeObjet.ToString, VI.CategorieRH.SituationAdministrative)

            Dim Libel As String = "Aucune situation" & VI.Tild & "Une situation" & VI.Tild & "situations"
            Dim LibCol As String = "date d'effet" & VI.Tild & "statut" & VI.Tild & "situation" & VI.Tild & "position" & VI.Tild & "modalité" & VI.Tild & "Taux" & VI.Tild & "grade" & VI.Tild & "échelon" & VI.Tild & "indice majoré" & VI.Tild & "Clef"
            Dim ListeH As ArrayList
            Dim I As Integer
            Dim LignesPr As New System.Text.StringBuilder

            HPrincipal.Centrage_Colonne(0) = 1
            HPrincipal.Centrage_Colonne(5) = 1
            HPrincipal.Centrage_Colonne(7) = 1
            HPrincipal.Centrage_Colonne(8) = 1

            ListeH = Dossier.HistoriquedeCarriere(WebFct.PointeurUtilisateur.V_NomdUtilisateurSgbd, _
                                                  WebFct.PointeurUtilisateur.TableActivite, False)
            If ListeH IsNot Nothing Then
                If ListeH.Count > 0 Then
                    For I = 0 To ListeH.Count - 1
                        LignesPr.Append(ListeH(I).ToString)
                    Next I
                    HPrincipal.V_Liste(LibCol, Libel) = LignesPr.ToString
                    EtiPrincipal.Visible = True
                    HPrincipal.Visible = True
                Else
                    HPrincipal.V_Liste(LibCol, Libel) = ""
                    EtiPrincipal.Visible = False
                    HPrincipal.Visible = False
                End If
            Else
                HPrincipal.V_Liste(LibCol, Libel) = ""
                EtiPrincipal.Visible = False
                HPrincipal.Visible = False
            End If

            Dim Lignes2nd As New System.Text.StringBuilder
            HDoubleCarriere.Centrage_Colonne(0) = 1
            HDoubleCarriere.Centrage_Colonne(5) = 1
            HDoubleCarriere.Centrage_Colonne(7) = 1
            HDoubleCarriere.Centrage_Colonne(8) = 1
            ListeH = Dossier.HistoriquedeCarriere(WebFct.PointeurUtilisateur.V_NomdUtilisateurSgbd, _
                                                  WebFct.PointeurUtilisateur.TableActivite, True)
            If ListeH IsNot Nothing Then
                If ListeH.Count > 0 Then
                    For I = 0 To ListeH.Count - 1
                        Lignes2nd.Append(ListeH(I).ToString)
                    Next I
                    HDoubleCarriere.V_Liste(LibCol, Libel) = Lignes2nd.ToString
                    EtiSecondaire.Visible = True
                    HDoubleCarriere.Visible = True
                Else
                    EtiSecondaire.Visible = False
                    HDoubleCarriere.Visible = False
                End If
            Else
                EtiSecondaire.Visible = False
                HDoubleCarriere.Visible = False
            End If

            WebFct.Dispose()
        End Set
    End Property
End Class
