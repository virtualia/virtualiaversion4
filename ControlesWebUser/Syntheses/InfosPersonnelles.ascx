﻿<%@ Control Language="VB" AutoEventWireup="false" Inherits="Virtualia.Net.Controles_InfosPersonnelles" Codebehind="InfosPersonnelles.ascx.vb" %>

<asp:ListView ID="FichePersonne" runat="server">
    <LayoutTemplate>
        <table id="Cadre" runat="server" width="750px" cellpadding="1" cellspacing="0"
                style=" border-style:ridge; border-color:#B0E0D7; border-width:3px; max-width:750px; 
                background-color:Transparent; height:430px">
            <tr runat="server" id="itemPlaceholder">
            </tr>
        </table>
        <asp:DataPager runat="server" ID="VirtuDataPager" PageSize="1" Visible="false" >
            <Fields>
                <asp:NumericPagerField ButtonCount="5" PreviousPageText="<--" NextPageText="-->" />
            </Fields>
        </asp:DataPager>
    </LayoutTemplate>
    <ItemTemplate>
        <tr id="TitreL1" runat="server" align="left" style="margin-top:2px; height:20px">
            <td valign="top" colspan="3" align="center">
               <asp:Label ID="EtiFiche" runat="server" Height="20px" Width="700px" Text="FICHE DE SITUATION"
                    BackColor="Transparent" BorderColor="#B0E0D7"  BorderStyle="None"
                    BorderWidth="2px" ForeColor="#121B1C" Font-Italic="false"
                    Font-Bold="true" Font-Names="Trebuchet MS" Font-Size="Small"
                    style="margin-top: 5px; text-indent: 5px; text-align: center" >
                </asp:Label>
            </td>
        </tr>
        <tr id="CadrePhoto" runat="server" align="left" valign="top">
            <td rowspan="3">
                <table id="TablePhoto" runat="server" width="150px"
                       style="height:205px; background-color:white; border-style:solid; 
                              border-width:2px; border-color: Gray; margin-left:5px; margin-top:30px">
                    <tr id="TRL2" runat="server" align="center">
                        <td valign="top">
                           <asp:Label ID="EtiNom" runat="server" Height="20px" Width="120px" Text='<%#Eval("Colonne_1") %>'
                                BackColor="Transparent" BorderColor="#B0E0D7"  BorderStyle="None"
                                BorderWidth="2px" ForeColor="#142425" Font-Italic="false"
                                Font-Bold="true" Font-Names="Trebuchet MS" Font-Size="Small"
                                style="margin-top: 0px; margin-left: 4px; text-indent: 5px; text-align: center" >
                            </asp:Label>
                        </td>
                    </tr>
                    <tr id="TR12" runat="server" align="center" style="margin-top:15px; padding:0px; vertical-align:middle">
                        <td>
                            <asp:Image ID="Photo" runat="server" ImageAlign="Middle" ImageUrl='<%#Eval("Colonne_18") %>' />
                        </td>
                    </tr>
                </table>
            </td>
            <td rowspan="1">
                <table id="CadreEtatCivil" runat="server" width="300px" 
                       style="height:120px; background-color:white; border-style:solid; border-color: Gray; 
                              border-width:2px; margin-left:2px; margin-top:5px;">
                    <tr id="Titre" runat="server" style="margin-top:2px; height:15px; line-height:15px">
                        <td valign="top" colspan="3" align="left">
                            <asp:Label ID="EtatCivil" runat="server" Height="20px" Width="300px" Text="ETAT-CIVIL"
                                BorderStyle="None" ForeColor="#121B1C" BackColor="Transparent"
                                Font-Italic="false" Font-Bold="true" Font-Names="Trebuchet MS" Font-Size="Small"
                                style="margin-top: 0px; margin-left: 5px; text-indent: 5px; text-align: center" >
                            </asp:Label>
                        </td>
                    </tr>
                    <tr id="TR00" runat="server" style="margin-top:5px; height:15px; padding:0px; line-height:15px">
                        <td valign="top" align="left" >
                            <asp:Label ID="Qualite" runat="server" Height="15px" Width="80px" Text='<%#Eval("Colonne_2") %>'
                                BorderStyle="None" ForeColor="#142425"  BackColor="Transparent"
                                Font-Italic="false" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                style="margin-top: 0px; margin-left: 5px; text-indent: 5px; text-align: left" >
                            </asp:Label>
                        </td>
                        <td valign="top" align="left">
                            <asp:Label ID="Nom" runat="server" Height="15px" Width="120px" Text='<%#Eval("Colonne_3") %>'
                                BorderStyle="None" ForeColor="#142425"  BackColor="Transparent"
                                Font-Italic="false" Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Small"
                                style="margin-top: 0px; margin-left: 2px; text-indent: 5px; text-align: left" >
                            </asp:Label>
                        </td>
                        <td valign="top" align="left" >
                            <asp:Label ID="Prenom" runat="server" Height="15px" Width="98px" Text='<%#Eval("Colonne_4") %>'
                                BorderStyle="None" ForeColor="#142425"  BackColor="Transparent"
                                Font-Italic="false" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                style="margin-top: 0px; margin-left: 2px; text-indent: 5px; text-align: left" >
                            </asp:Label>
                        </td>
                    </tr>
                    <tr id="TR01" runat="server" style="margin-top:2px; height:15px; padding:0px; line-height:15px">
                        <td valign="top" align="left" >
                            <asp:Label ID="EtiAutrePrenom" runat="server" Height="15px" Width="80px" Text='<%#Eval("Colonne_50") %>'
                                BorderStyle="None" ForeColor="GrayText"  BackColor="Transparent"
                                Font-Italic="false" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                style="margin-top: 0px; margin-left: 5px; text-indent: 5px; text-align: left" >
                            </asp:Label>
                        </td>
                        <td valign="top" align="left" colspan="2">
                            <asp:Label ID="AutrePrenom" runat="server" Height="15px" Width="218px" Text='<%#Eval("Colonne_5") %>'
                                BorderStyle="None" ForeColor="#142425"  BackColor="Transparent"
                                Font-Italic="false" Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Small"
                                style="margin-top: 0px; margin-left: 2px; text-indent: 5px; text-align: left" >
                            </asp:Label>
                        </td>
                    </tr>
                    <tr id="TR02" runat="server" style="margin-top:2px; height:15px; padding:0px; line-height:15px">
                        <td valign="top" align="left" >
                            <asp:Label ID="EtiDateNai" runat="server" Height="15px" Width="80px" Text='<%#Eval("Colonne_51") %>'
                                BorderStyle="None" ForeColor="GrayText"  BackColor="Transparent"
                                Font-Italic="false" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                style="margin-top: 0px; margin-left: 5px; text-indent: 5px; text-align: left" >
                            </asp:Label>
                        </td>
                        <td valign="top" align="left" colspan="2">
                            <asp:Label ID="DateNai" runat="server" Height="15px" Width="218px" Text='<%#Eval("Colonne_6") %>'
                                BorderStyle="None" ForeColor="#142425"  BackColor="Transparent"
                                Font-Italic="false" Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Small"
                                style="margin-top: 0px; margin-left: 2px; text-indent: 5px; text-align: left" >
                            </asp:Label>
                        </td>
                    </tr>
                    <tr id="TR03" runat="server" style="margin-top:2px; height:15px; padding:0px; line-height:15px">
                        <td valign="top" align="left" >
                            <asp:Label ID="EtiLieuNai" runat="server" Height="15px" Width="80px" Text='<%#Eval("Colonne_52") %>'
                                BorderStyle="None" ForeColor="GrayText"  BackColor="Transparent"
                                Font-Italic="false" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                style="margin-top: 0px; margin-left: 5px; text-indent: 5px; text-align: left" >
                            </asp:Label>
                        </td>
                        <td valign="top" align="left">
                            <asp:Label ID="LieuNai" runat="server" Height="15px" Width="120px" Text='<%#Eval("Colonne_7") %>'
                                BorderStyle="None" ForeColor="#142425"  BackColor="Transparent"
                                Font-Italic="false" Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Small"
                                style="margin-top: 0px; margin-left: 2px; text-indent: 5px; text-align: left" >
                            </asp:Label>
                        </td>
                        <td valign="top" align="left">
                            <asp:Label ID="DeptNai" runat="server" Height="15px" Width="98px" Text='<%#Eval("Colonne_8") %>'
                                BorderStyle="None" ForeColor="#142425"  BackColor="Transparent"
                                Font-Italic="false" Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Small"
                                style="margin-top: 0px; margin-left: 2px; text-indent: 5px; text-align: left" >
                            </asp:Label>
                        </td>
                    </tr>
                    <tr id="TR04" runat="server" style="margin-top:2px; height:15px; padding:0px; line-height:15px">
                        <td valign="top" align="left" >
                            <asp:Label ID="EtiNIR" runat="server" Height="15px" Width="80px" Text="NIR"
                                BorderStyle="None" ForeColor="GrayText" BackColor="Transparent"
                                Font-Italic="false" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                style="margin-top: 0px; margin-left: 5px; text-indent: 5px; text-align: left" >
                            </asp:Label>
                        </td>
                        <td valign="top" align="left">
                            <asp:Label ID="NIR" runat="server" Height="15px" Width="120px" Text='<%#Eval("Colonne_9") %>'
                                BorderStyle="None" ForeColor="#142425"  BackColor="Transparent"
                                Font-Italic="false" Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Small"
                                style="margin-top: 0px; margin-left: 2px; text-indent: 5px; text-align: left" >
                            </asp:Label>
                        </td>
                        <td valign="top" align="left">
                            <asp:Label ID="CleNir" runat="server" Height="20px" Width="98px" Text='<%#Eval("Colonne_10") %>'
                                BorderStyle="None" ForeColor="#142425"  BackColor="Transparent"
                                Font-Italic="false" Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Small"
                                style="margin-top: 0px; margin-left: 2px; text-indent: 5px; text-align: left" >
                            </asp:Label>
                        </td>
                    </tr>
                </table>
            </td>
            <td rowspan="1" align="left">
                <table id="CadreAdresse" runat="server" width="300px"
                       style="height:120px; background-color:white; border-style:solid; border-color: Gray;
                              border-width:2px; margin-left:2px; margin-top:5px">
                    <tr id="TR05" runat="server" style="margin-top:2px; height:20px">
                        <td valign="top" colspan="2" align="left">
                            <asp:Label ID="Adresse" runat="server" Height="20px" Width="300px" Text="ADRESSE"
                                BorderStyle="None" ForeColor="#121B1C" BackColor="Transparent"
                                Font-Italic="false" Font-Bold="true" Font-Names="Trebuchet MS" Font-Size="Small"
                                style="margin-top: 0px; margin-left: 5px; text-indent: 5px; text-align: center" >
                            </asp:Label>
                        </td>
                    </tr>
                    <tr id="TR06" runat="server" style="margin-top:5px; height:15px; padding:0px; line-height:15px">
                        <td valign="top" colspan="2" align="left" >
                            <asp:Label ID="NomRue" runat="server" Height="15px" Width="300px" Text='<%#Eval("Colonne_11") %>'
                                BorderStyle="None" ForeColor="#142425"  BackColor="Transparent"
                                Font-Italic="false" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                style="margin-top: 0px; margin-left: 5px; text-indent: 5px; text-align: left" >
                            </asp:Label>
                        </td>
                    </tr>
                    <tr id="TR07" runat="server" style="margin-top:2px; height:15px; padding:0px; line-height:15px">
                        <td valign="top" align="left" colspan="2">
                            <asp:Label ID="Complement" runat="server" Height="15px" Width="300px" Text='<%#Eval("Colonne_12") %>'
                                BorderStyle="None" ForeColor="#142425"  BackColor="Transparent"
                                Font-Italic="false" Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Small"
                                style="margin-top: 0px; margin-left: 5px; text-indent: 5px; text-align: left" >
                            </asp:Label>
                        </td>
                    </tr>
                    <tr id="TR08" runat="server" style="margin-top:2px; height:15px; padding:0px; line-height:15px">
                        <td valign="top" align="left" >
                            <asp:Label ID="CodePostal" runat="server" Height="15px" Width="100px" Text='<%#Eval("Colonne_13") %>'
                                BorderStyle="None" ForeColor="#142425"  BackColor="Transparent"
                                Font-Italic="false" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                style="margin-top: 0px; margin-left: 5px; text-indent: 5px; text-align: left" >
                            </asp:Label>
                        </td>
                        <td valign="top" align="left" colspan="2">
                            <asp:Label ID="Ville" runat="server" Height="15px" Width="200px" Text='<%#Eval("Colonne_14") %>'
                                BorderStyle="None" ForeColor="#142425"  BackColor="Transparent"
                                Font-Italic="false" Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Small"
                                style="margin-top: 0px; margin-left: 2px; text-indent: 5px; text-align: left" >
                            </asp:Label>
                        </td>
                    </tr>
                    <tr id="TR09" runat="server" style="margin-top:20px; height:15px; padding:0px; line-height:15px">
                        <td valign="top" align="left" >
                            <asp:Label ID="EtiTelephone" runat="server" Height="15px" Width="100px" Text='<%#Eval("Colonne_53") %>'
                                BorderStyle="None" ForeColor="GrayText"  BackColor="Transparent"
                                Font-Italic="false" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                style="margin-top: 0px; margin-left: 5px; text-indent: 5px; text-align: left" >
                            </asp:Label>
                        </td>
                        <td valign="top" align="left">
                            <asp:Label ID="Telephone" runat="server" Height="15px" Width="200px" Text='<%#Eval("Colonne_15") %>'
                                BorderStyle="None" ForeColor="#142425"  BackColor="Transparent"
                                Font-Italic="false" Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Small"
                                style="margin-top: 0px; margin-left: 2px; text-indent: 5px; text-align: left" >
                            </asp:Label>
                        </td>
                    </tr>
                    <tr id="TR10" runat="server" style="margin-top:2px; height:15px; padding:0px; line-height:15px">
                        <td valign="top" align="left" >
                            <asp:Label ID="EtiPortable" runat="server" Height="15px" Width="100px" Text='<%#Eval("Colonne_54") %>'
                                BorderStyle="None" ForeColor="GrayText"  BackColor="Transparent"
                                Font-Italic="false" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                style="margin-top: 0px; margin-left: 5px; text-indent: 5px; text-align: left" >
                            </asp:Label>
                        </td>
                        <td valign="top" align="left">
                            <asp:Label ID="Portable" runat="server" Height="15px" Width="200px" Text='<%#Eval("Colonne_16") %>'
                                BorderStyle="None" ForeColor="#142425"  BackColor="Transparent"
                                Font-Italic="false" Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Small"
                                style="margin-top: 0px; margin-left: 2px; text-indent: 5px; text-align: left" >
                            </asp:Label>
                        </td>
                    </tr>
                    <tr id="TR11" runat="server" style="margin-top:2px; height:15px; padding:0px; line-height:15px">
                        <td valign="top" align="left" >
                            <asp:Label ID="EtiMail" runat="server" Height="15px" Width="100px" Text='<%#Eval("Colonne_55") %>'
                                BorderStyle="None" ForeColor="GrayText"  BackColor="Transparent"
                                Font-Italic="false" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                style="margin-top: 0px; margin-left: 5px; text-indent: 5px; text-align: left" >
                            </asp:Label>
                        </td>
                        <td valign="top" align="left">
                            <asp:Label ID="Email" runat="server" Height="15px" Width="200px" Text='<%#Eval("Colonne_17") %>'
                                BorderStyle="None" ForeColor="#142425"  BackColor="Transparent"
                                Font-Italic="false" Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Small"
                                style="margin-top: 0px; margin-left: 2px; text-indent: 5px; text-align: left" >
                            </asp:Label>
                        </td>
                    </tr>
                </table>
            </td>
         </tr>
         <tr>
            <td rowspan="2" align="left" style="vertical-align:top">
                <table id="Enfants" runat="server" width="300px"
                       style="height:250px; background-color:white; border-style:solid; border-color: Gray;
                              border-width:2px; margin-left:2px; margin-top:0px">
                    <tr id="TR13" runat="server" style="margin-top:2px; height:15px; line-height:15px">
                        <td valign="top" colspan="2" align="left">
                            <asp:Label ID="Enfant" runat="server" Height="15px" Width="290px" Text="SITUATION FAMILIALE"
                                BorderStyle="None" ForeColor="#121B1C" BackColor="Transparent"
                                Font-Italic="false" Font-Bold="true" Font-Names="Trebuchet MS" Font-Size="Small"
                                style="margin-top: 0px; margin-left: 5px; text-indent: 5px; text-align: center" >
                            </asp:Label>
                        </td>
                    </tr>
                    <tr id="TR14" runat="server" style="margin-top:5px; height:15px; padding:0px; line-height:15px">
                        <td valign="top" align="left" >
                            <asp:Label ID="EtiSituation" runat="server" Height="15px" Width="150px" Text="Situation familiale"
                                BorderStyle="None" ForeColor="GrayText"  BackColor="Transparent"
                                Font-Italic="false" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                style="margin-top: 0px; margin-left: 5px; text-indent: 5px; text-align: left" >
                            </asp:Label>
                        </td>
                        <td valign="top" align="left">
                            <asp:Label ID="Situation" runat="server" Height="15px" Width="150px" Text='<%#Eval("Colonne_19") %>'
                                BorderStyle="None" ForeColor="#142425"  BackColor="Transparent"
                                Font-Italic="false" Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Small"
                                style="margin-top: 0px; margin-left: 2px; text-indent: 5px; text-align: left" >
                            </asp:Label>
                        </td>
                    </tr>
                    <tr id="TR15" runat="server" style="margin-top:5px; height:15px; padding:0px; line-height:15px">
                        <td valign="top" colspan="2" align="left" >
                            <asp:Label ID="EtiEnfants" runat="server" Height="15px" Width="300px" Text="ENFANTS"
                                BorderStyle="None" ForeColor="GrayText"  BackColor="Transparent"
                                Font-Italic="false" Font-Bold="true" Font-Names="Trebuchet MS" Font-Size="Small"
                                style="margin-top: 0px; margin-left: 5px; text-indent: 5px; text-align: left" >
                            </asp:Label>
                        </td>
                    </tr>
                    <tr id="TR16" runat="server" style="margin-top:2px; height:15px; padding:0px; line-height:15px">
                        <td valign="top" align="left" >
                            <asp:Label ID="PrenomEnfant1" runat="server" Height="15px" Width="150px" Text='<%#Eval("Colonne_20") %>'
                                BorderStyle="None" ForeColor="#142425"  BackColor="Transparent"
                                Font-Italic="false" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                style="margin-top: 0px; margin-left: 5px; text-indent: 5px; text-align: left" >
                            </asp:Label>
                        </td>
                        <td valign="top" align="left" colspan="2">
                            <asp:Label ID="DateNaiEnfant1" runat="server" Height="15px" Width="150px" Text='<%#Eval("Colonne_21") %>'
                                BorderStyle="None" ForeColor="#142425"  BackColor="Transparent"
                                Font-Italic="false" Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Small"
                                style="margin-top: 0px; margin-left: 2px; text-indent: 5px; text-align: left" >
                            </asp:Label>
                        </td>
                    </tr>
                    <tr id="TR17" runat="server" style="margin-top:2px; height:15px; padding:0px; line-height:15px">
                        <td valign="top" align="left" >
                            <asp:Label ID="PrenomEnfant2" runat="server" Height="15px" Width="150px" Text='<%#Eval("Colonne_22") %>'
                                BorderStyle="None" ForeColor="#142425"  BackColor="Transparent"
                                Font-Italic="false" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                style="margin-top: 0px; margin-left: 5px; text-indent: 5px; text-align: left" >
                            </asp:Label>
                        </td>
                        <td valign="top" align="left" colspan="2">
                            <asp:Label ID="DateNaiEnfant2" runat="server" Height="15px" Width="150px" Text='<%#Eval("Colonne_23") %>'
                                BorderStyle="None" ForeColor="#142425"  BackColor="Transparent"
                                Font-Italic="false" Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Small"
                                style="margin-top: 0px; margin-left: 2px; text-indent: 5px; text-align: left" >
                            </asp:Label>
                        </td>
                    </tr>
                    <tr id="TR18" runat="server" style="margin-top:2px; height:15px; padding:0px; line-height:15px">
                        <td valign="top" align="left" >
                            <asp:Label ID="PrenomEnfant3" runat="server" Height="15px" Width="150px" Text='<%#Eval("Colonne_24") %>'
                                BorderStyle="None" ForeColor="#142425"  BackColor="Transparent"
                                Font-Italic="false" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                style="margin-top: 0px; margin-left: 5px; text-indent: 5px; text-align: left" >
                            </asp:Label>
                        </td>
                        <td valign="top" align="left" colspan="2">
                            <asp:Label ID="DateNaiEnfant3" runat="server" Height="15px" Width="150px" Text='<%#Eval("Colonne_25") %>'
                                BorderStyle="None" ForeColor="#142425"  BackColor="Transparent"
                                Font-Italic="false" Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Small"
                                style="margin-top: 0px; margin-left: 2px; text-indent: 5px; text-align: left" >
                            </asp:Label>
                        </td>
                    </tr>
                    <tr id="TR19" runat="server" style="margin-top:2px; height:15px; padding:0px; line-height:15px">
                        <td valign="top" align="left" >
                            <asp:Label ID="PrenomEnfant4" runat="server" Height="15px" Width="150px" Text='<%#Eval("Colonne_26") %>'
                                BorderStyle="None" ForeColor="#142425"  BackColor="Transparent"
                                Font-Italic="false" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                style="margin-top: 0px; margin-left: 5px; text-indent: 5px; text-align: left" >
                            </asp:Label>
                        </td>
                        <td valign="top" align="left" colspan="2">
                            <asp:Label ID="DateNaiEnfant4" runat="server" Height="15px" Width="150px" Text='<%#Eval("Colonne_27") %>'
                                BorderStyle="None" ForeColor="#142425"  BackColor="Transparent"
                                Font-Italic="false" Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Small"
                                style="margin-top: 0px; margin-left: 2px; text-indent: 5px; text-align: left" >
                            </asp:Label>
                        </td>
                    </tr>
                    <tr id="TR20" runat="server" style="margin-top:2px; height:15px; padding:0px; line-height:15px">
                        <td valign="top" align="left" >
                            <asp:Label ID="PrenomEnfant5" runat="server" Height="15px" Width="150px" Text='<%#Eval("Colonne_28") %>'
                                BorderStyle="None" ForeColor="#142425"  BackColor="Transparent"
                                Font-Italic="false" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                style="margin-top: 0px; margin-left: 5px; text-indent: 5px; text-align: left" >
                            </asp:Label>
                        </td>
                        <td valign="top" align="left" colspan="2">
                            <asp:Label ID="DateNaiEnfant5" runat="server" Height="15px" Width="150px" Text='<%#Eval("Colonne_29") %>'
                                BorderStyle="None" ForeColor="#142425"  BackColor="Transparent"
                                Font-Italic="false" Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Small"
                                style="margin-top: 0px; margin-left: 2px; text-indent: 5px; text-align: left" >
                            </asp:Label>
                        </td>
                    </tr>
                    <tr id="TR21" runat="server" style="margin-top:2px; height:15px; padding:0px; line-height:15px">
                        <td valign="top" align="left" >
                            <asp:Label ID="PrenomEnfant6" runat="server" Height="15px" Width="150px" Text='<%#Eval("Colonne_30") %>'
                                BorderStyle="None" ForeColor="#142425"  BackColor="Transparent"
                                Font-Italic="false" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                style="margin-top: 0px; margin-left: 5px; text-indent: 5px; text-align: left" >
                            </asp:Label>
                        </td>
                        <td valign="top" align="left" colspan="2">
                            <asp:Label ID="DateNaiEnfant6" runat="server" Height="15px" Width="150px" Text='<%#Eval("Colonne_31") %>'
                                BorderStyle="None" ForeColor="#142425"  BackColor="Transparent"
                                Font-Italic="false" Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Small"
                                style="margin-top: 0px; margin-left: 2px; text-indent: 5px; text-align: left" >
                            </asp:Label>
                        </td>
                    </tr>
                    <tr id="TR22" runat="server" style="margin-top:2px; height:15px; padding:0px; line-height:15px">
                        <td valign="top" align="left" >
                            <asp:Label ID="PrenomEnfant7" runat="server" Height="15px" Width="150px" Text='<%#Eval("Colonne_32") %>'
                                BorderStyle="None" ForeColor="#142425"  BackColor="Transparent"
                                Font-Italic="false" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                style="margin-top: 0px; margin-left: 5px; text-indent: 5px; text-align: left" >
                            </asp:Label>
                        </td>
                        <td valign="top" align="left" colspan="2">
                            <asp:Label ID="DateNaiEnfant7" runat="server" Height="15px" Width="150px" Text='<%#Eval("Colonne_33") %>'
                                BorderStyle="None" ForeColor="#142425"  BackColor="Transparent"
                                Font-Italic="false" Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Small"
                                style="margin-top: 0px; margin-left: 2px; text-indent: 5px; text-align: left" >
                            </asp:Label>
                        </td>
                    </tr>
                    <tr id="TR23" runat="server" style="margin-top:2px; height:15px; padding:0px; line-height:15px">
                        <td valign="top" align="left" >
                            <asp:Label ID="PrenomEnfant8" runat="server" Height="15px" Width="150px" Text='<%#Eval("Colonne_34") %>'
                                BorderStyle="None" ForeColor="#142425"  BackColor="Transparent"
                                Font-Italic="false" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                style="margin-top: 0px; margin-left: 5px; text-indent: 5px; text-align: left" >
                            </asp:Label>
                        </td>
                        <td valign="top" align="left" colspan="2">
                            <asp:Label ID="DateNaiEnfant8" runat="server" Height="15px" Width="150px" Text='<%#Eval("Colonne_35") %>'
                                BorderStyle="None" ForeColor="#142425"  BackColor="Transparent"
                                Font-Italic="false" Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Small"
                                style="margin-top: 0px; margin-left: 2px; text-indent: 5px; text-align: left" >
                            </asp:Label>
                        </td>
                    </tr>
                    <tr id="TR24" runat="server" style="margin-top:2px; height:15px; padding:0px; line-height:15px">
                        <td valign="top" align="left" >
                            <asp:Label ID="PrenomEnfant9" runat="server" Height="15px" Width="150px" Text='<%#Eval("Colonne_36") %>'
                                BorderStyle="None" ForeColor="#142425"  BackColor="Transparent"
                                Font-Italic="false" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                style="margin-top: 0px; margin-left: 5px; text-indent: 5px; text-align: left" >
                            </asp:Label>
                        </td>
                        <td valign="top" align="left" colspan="2">
                            <asp:Label ID="DateNaiEnfant9" runat="server" Height="15px" Width="150px" Text='<%#Eval("Colonne_37") %>'
                                BorderStyle="None" ForeColor="#142425"  BackColor="Transparent"
                                Font-Italic="false" Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Small"
                                style="margin-top: 0px; margin-left: 2px; text-indent: 5px; text-align: left" >
                            </asp:Label>
                        </td>
                    </tr>
                    <tr id="TR25" runat="server" style="margin-top:2px; height:15px; padding:0px; line-height:15px">
                        <td valign="top" align="left" >
                            <asp:Label ID="PrenomEnfant10" runat="server" Height="15px" Width="150px" Text='<%#Eval("Colonne_38") %>'
                                BorderStyle="None" ForeColor="#142425"  BackColor="Transparent"
                                Font-Italic="false" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                style="margin-top: 0px; margin-left: 5px; text-indent: 5px; text-align: left" >
                            </asp:Label>
                        </td>
                        <td valign="top" align="left" colspan="2">
                            <asp:Label ID="DateNaiEnfant10" runat="server" Height="15px" Width="150px" Text='<%#Eval("Colonne_39") %>'
                                BorderStyle="None" ForeColor="#142425"  BackColor="Transparent"
                                Font-Italic="false" Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Small"
                                style="margin-top: 0px; margin-left: 2px; text-indent: 5px; text-align: left" >
                            </asp:Label>
                        </td>
                    </tr>
                </table>
            </td>
            <td align="left" colspan="2" style="vertical-align:top">
                <table id="RIB" runat="server" width="300px"
                       style="height:120px; background-color:white; border-style:solid; border-color: Gray;
                              border-width:2px; margin-left:2px; margin-top:0px; vertical-align:top">
                    <tr id="TR29" runat="server" style="margin-top:2px; height:15px">
                        <td valign="top" colspan="4" align="left">
                            <asp:Label ID="EtiRIB" runat="server" Height="15px" Width="300px" Text="RELEVE D'IDENTITE BANCAIRE"
                                BorderStyle="None" ForeColor="#121B1C" BackColor="Transparent"
                                Font-Italic="false" Font-Bold="true" Font-Names="Trebuchet MS" Font-Size="Small"
                                style="margin-top: 0px; margin-left: 5px; text-indent: 5px; text-align: center" >
                            </asp:Label>
                        </td>
                    </tr>
                    <tr id="TR30" runat="server" style="margin-top:2px; height:15px; padding:0px; line-height:15px">
                        <td valign="top" align="left">
                            <asp:Label ID="EtiBanque" runat="server" Height="15px" Width="50px" Text="Banque"
                                BorderStyle="None" ForeColor="GrayText" BackColor="Transparent"
                                Font-Italic="false" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                style="margin-top: 0px; margin-left: 5px; text-indent: 5px; text-align: center" >
                            </asp:Label>
                        </td>
                        <td valign="top" align="left">
                            <asp:Label ID="EtiGuichet" runat="server" Height="15px" Width="50px" Text="Guichet"
                                BorderStyle="None" ForeColor="GrayText" BackColor="Transparent"
                                Font-Italic="false" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                style="margin-top: 0px; margin-left: 5px; text-indent: 5px; text-align: center" >
                            </asp:Label>
                        </td>
                        <td valign="top" align="left">
                            <asp:Label ID="EtiCompte" runat="server" Height="15px" Width="140px" Text="No de compte"
                                BorderStyle="None" ForeColor="GrayText" BackColor="Transparent"
                                Font-Italic="false" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                style="margin-top: 0px; margin-left: 5px; text-indent: 5px; text-align: center" >
                            </asp:Label>
                        </td>
                        <td valign="top" align="left">
                            <asp:Label ID="EtiClef" runat="server" Height="15px" Width="40px" Text="Clé"
                                BorderStyle="None" ForeColor="GrayText" BackColor="Transparent"
                                Font-Italic="false" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                style="margin-top: 0px; margin-left: 5px; text-indent: 5px; text-align: center" >
                            </asp:Label>
                        </td>
                    </tr>
                    <tr id="TR31" runat="server" style="margin-top:5px; height:15px; padding:0px; line-height:15px">
                        <td valign="top" align="left">
                            <asp:Label ID="Banque" runat="server" Height="15px" Width="50px" Text='<%#Eval("Colonne_40") %>'
                                BorderStyle="None" ForeColor="#142425" BackColor="Transparent"
                                Font-Italic="false" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                style="margin-top: 0px; margin-left: 5px; text-indent: 5px; text-align: center" >
                            </asp:Label>
                        </td>
                        <td valign="top" align="left">
                            <asp:Label ID="Guichet" runat="server" Height="15px" Width="50px" Text='<%#Eval("Colonne_41") %>'
                                BorderStyle="None" ForeColor="#142425" BackColor="Transparent"
                                Font-Italic="false" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                style="margin-top: 0px; margin-left: 5px; text-indent: 5px; text-align: center" >
                            </asp:Label>
                        </td>
                        <td valign="top" align="left">
                            <asp:Label ID="NoCompte" runat="server" Height="15px" Width="140px" Text='<%#Eval("Colonne_42") %>'
                                BorderStyle="None" ForeColor="#142425" BackColor="Transparent"
                                Font-Italic="false" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                style="margin-top: 0px; margin-left: 5px; text-indent: 5px; text-align: center" >
                            </asp:Label>
                        </td>
                        <td valign="top" align="left">
                            <asp:Label ID="CleRib" runat="server" Height="15px" Width="40px" Text='<%#Eval("Colonne_43") %>'
                                BorderStyle="None" ForeColor="#142425" BackColor="Transparent"
                                Font-Italic="false" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                style="margin-top: 0px; margin-left: 5px; text-indent: 5px; text-align: center" >
                            </asp:Label>
                        </td>
                    </tr>
                    <tr id="TR32" runat="server" style="margin-top:2px; height:20px; padding:0px; line-height:15px">
                        <td valign="top" align="left" colspan="2">
                            <asp:Label ID="EtiDomRib" runat="server" Height="20px" Width="100px" Text="Domiciliation"
                                BorderStyle="None" ForeColor="GrayText"  BackColor="Transparent"
                                Font-Italic="false" Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Small"
                                style="margin-top: 0px; margin-left: 5px; text-indent: 5px; text-align: left" >
                            </asp:Label>
                        </td>
                        <td valign="top" align="left" colspan="2">
                            <asp:Label ID="DomRib" runat="server" Height="20px" Width="190px" Text='<%#Eval("Colonne_44") %>'
                                BorderStyle="None" ForeColor="#142425"  BackColor="Transparent"
                                Font-Italic="false" Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Small"
                                style="margin-top: 0px; margin-left: 5px; text-indent: 5px; text-align: left" >
                            </asp:Label>
                        </td>
                    </tr>
                </table>
             </td>
         </tr>
         <tr>
             <td> 
                <table id="CadreExterne" runat="server" width="300px"
                       style="height:120px; background-color:white; border-style:solid; border-color: Gray;
                              border-width:2px; margin-left:2px; margin-top:0px; vertical-align:top">
                    <tr id="TR26" runat="server" style="margin-top:2px; height:20px">
                        <td valign="top" align="center" colspan="2">
                            <asp:Label ID="EtiExterne" runat="server" Height="20px" Width="300px" Text="NUMEROS DE REFERENCE"
                                BorderStyle="None" ForeColor="#121B1C" BackColor="Transparent"
                                Font-Italic="false" Font-Bold="true" Font-Names="Trebuchet MS" Font-Size="Small"
                                style="margin-top: 0px; margin-left: 5px; text-indent: 5px; text-align: center" >
                            </asp:Label>
                        </td>
                    </tr>
                    <tr id="TR27" runat="server" style="margin-top:2px; height:20px; padding:0px">
                        <td valign="top" align="left">
                            <asp:Label ID="EtiIde" runat="server" Height="20px" Width="150px" Text="Identifiant Virtualia"
                                BorderStyle="None" ForeColor="GrayText"  BackColor="Transparent"
                                Font-Italic="false" Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Small"
                                style="margin-top: 0px; margin-left: 5px; text-indent: 5px; text-align: left" >
                            </asp:Label>
                        </td>
                        <td valign="top" align="left">
                            <asp:Label ID="Ide" runat="server" Height="20px" Width="150px" Text='<%#Eval("Colonne_45") %>'
                                BorderStyle="None" ForeColor="#142425"  BackColor="Transparent"
                                Font-Italic="false" Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Small"
                                style="margin-top: 0px; margin-left: 5px; text-indent: 5px; text-align: left" >
                            </asp:Label>
                        </td>
                    </tr>
                    <tr id="TR28" runat="server" style="margin-top:2px; height:20px; padding:0px" >
                        <td valign="top" align="left">
                            <asp:Label ID="EtiBadge" runat="server" Height="20px" Width="150px" Text='<%#Eval("Colonne_56") %>'
                                BorderStyle="None" ForeColor="GrayText"  BackColor="Transparent"
                                Font-Italic="false" Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Small"
                                style="margin-top: 0px; margin-left: 5px; text-indent: 5px; text-align: left" >
                            </asp:Label>
                        </td>
                        <td valign="top" align="left">
                            <asp:Label ID="Badge" runat="server" Height="20px" Width="150px" Text='<%#Eval("Colonne_46") %>'
                                BorderStyle="None" ForeColor="#142425"  BackColor="Transparent"
                                Font-Italic="false" Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Small"
                                style="margin-top: 0px; margin-left: 5px; text-indent: 5px; text-align: left" >
                            </asp:Label>
                        </td>
                    </tr>
                    <tr id="TR33" runat="server" style="margin-top:2px; height:20px; padding:0px">
                        <td valign="top" align="left">
                            <asp:Label ID="EtiPaie" runat="server" Height="20px" Width="150px" Text='<%#Eval("Colonne_57") %>'
                                BorderStyle="None" ForeColor="GrayText"  BackColor="Transparent"
                                Font-Italic="false" Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Small"
                                style="margin-top: 0px; margin-left: 5px; text-indent: 5px; text-align: left" >
                            </asp:Label>
                        </td>
                        <td valign="top" align="left">
                            <asp:Label ID="Paie" runat="server" Height="20px" Width="150px" Text='<%#Eval("Colonne_47") %>'
                                BorderStyle="None" ForeColor="#142425"  BackColor="Transparent"
                                Font-Italic="false" Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Small"
                                style="margin-top: 0px; margin-left: 5px; text-indent: 5px; text-align: left" >
                            </asp:Label>
                        </td>
                    </tr>
                    <tr id="TR34" runat="server" style="margin-top:2px; height:15px; padding:0px; line-height:15px">
                        <td valign="top" align="left">
                            <asp:Label ID="EtiNumen" runat="server" Height="15px" Width="150px" Text='<%#Eval("Colonne_58") %>'
                                BorderStyle="None" ForeColor="GrayText"  BackColor="Transparent"
                                Font-Italic="false" Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Small"
                                style="margin-top: 0px; margin-left: 5px; text-indent: 5px; text-align: left" >
                            </asp:Label>
                        </td>
                        <td valign="top" align="left">
                            <asp:Label ID="Numen" runat="server" Height="15px" Width="150px" Text='<%#Eval("Colonne_48") %>'
                                BorderStyle="None" ForeColor="#142425"  BackColor="Transparent"
                                Font-Italic="false" Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Small"
                                style="margin-top: 0px; margin-left: 5px; text-indent: 5px; text-align: left" >
                            </asp:Label>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </ItemTemplate>
</asp:ListView>