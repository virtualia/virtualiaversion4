﻿Option Strict On
Option Explicit On
Option Compare Text
Imports VI = Virtualia.Systeme.Constantes
Partial Class Controles_Dictionnaire
    Inherits System.Web.UI.UserControl
    Public Delegate Sub Dictionnaire_ClickEventHandler(ByVal sender As Object, ByVal e As DictionnaireEventArgs)
    Public Event Dictionnaire_Click As Dictionnaire_ClickEventHandler
    Private WebFct As Virtualia.Net.WebFonctions
    Private WsPointdeVue As Short = 1
    Private WsOutil As Short = VI.OptionInfo.DicoCmc
    Private WsNomState As String = "Recherche"
    '
    Protected Overridable Sub V_OnClick(ByVal e As DictionnaireEventArgs)
        RaiseEvent Dictionnaire_Click(Me, e)
    End Sub

    Public Property V_PointdeVue() As Short
        Get
            Return WsPointdeVue
        End Get
        Set(ByVal value As Short)
            WsPointdeVue = value
        End Set
    End Property

    Public Property V_Outil() As Short
        Get
            Return WsOutil
        End Get
        Set(ByVal value As Short)
            WsOutil = value
        End Set
    End Property

    Protected Sub TreeDictionnaire_SelectedNodeChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles TreeDictionnaire.SelectedNodeChanged
        Select Case CType(sender, System.Web.UI.WebControls.TreeView).SelectedNode.Depth
            Case Is < 3
                Exit Sub
        End Select
        Dim SelInfo As String = CType(sender, System.Web.UI.WebControls.TreeView).SelectedNode.Value
        Dim Predicat As New Virtualia.Systeme.MetaModele.Predicats.PredicateDictionnaire(Convert.ToInt32(SelInfo))
        Dim Evenement As DictionnaireEventArgs = Nothing

        Select Case CType(sender, System.Web.UI.WebControls.TreeView).SelectedNode.Parent.Value
            Case Is = "Info"
                Dim Dico As List(Of Virtualia.Systeme.MetaModele.Outils.FicheInfoDictionnaire)
                Dico = WebFct.PointeurGlobal.VirListeInfosDico.FindAll(AddressOf Predicat.InformationParIndex)
                If Dico.Count = 0 Then
                    Exit Sub
                End If
                Evenement = New DictionnaireEventArgs(Dico.Item(0).PointdeVue, Dico.Item(0).Objet, Dico.Item(0).Information)
            Case Is = "Experte"
                Dim Dico As List(Of Virtualia.Systeme.MetaModele.Outils.FicheInfoExperte)
                Dico = WebFct.PointeurGlobal.VirListeExpertesDico.FindAll(AddressOf Predicat.InformationParIndexExperte)
                If Dico.Count = 0 Then
                    Exit Sub
                End If
                Evenement = New DictionnaireEventArgs(Dico.Item(0).PointdeVue, Dico.Item(0).Objet, Dico.Item(0).Information)
        End Select
        CType(sender, System.Web.UI.WebControls.TreeView).SelectedNode.Selected = False

        V_OnClick(Evenement)
    End Sub

    Protected Sub TreeDictionnaire_TreeNodeExpanded(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.TreeNodeEventArgs) Handles TreeDictionnaire.TreeNodeExpanded
        e.Node.Expand()
    End Sub

    Protected Sub TreeDictionnaire_TreeNodePopulate(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.TreeNodeEventArgs) Handles TreeDictionnaire.TreeNodePopulate
        TreeDictionnaire.ImageSet = System.Web.UI.WebControls.TreeViewImageSet.XPFileExplorer
        If e.Node.ChildNodes.Count = 0 Then
            Select Case e.Node.Depth
                Case Is = 0
                    Call FaireListeCategories(e.Node)
                Case Is = 1
                    Call FaireListeObjets(e.Node)
                Case Is = 2
                    Call FaireListeInfos(e.Node)
            End Select
        End If
        e.Node.Expand()
    End Sub

    Private Sub FaireListeCategories(ByVal Noeud As TreeNode)
        Dim NoCat As Integer = 1
        Dim NewNoeud As TreeNode

        Do
            NewNoeud = New TreeNode(WebFct.PointeurGlobal.LibelleCategorie(NoCat), NoCat.ToString)
            NewNoeud.PopulateOnDemand = True
            NewNoeud.ImageUrl = "~\Images\Armoire\RougeCarminFermer16.bmp"
            NewNoeud.SelectAction = TreeNodeSelectAction.Expand
            Noeud.ChildNodes.Add(NewNoeud)
            NoCat += 1
            If NoCat > 12 Then
                Exit Do
            End If
        Loop
    End Sub

    Private Sub FaireListeObjets(ByVal Noeud As TreeNode)
        Dim NoObjet As Integer = 0
        Dim NewNoeud As TreeNode
        Dim NoCat As Short = CShort(Noeud.Value)

        Dim Dico As List(Of Virtualia.Systeme.MetaModele.Outils.FicheObjetDictionnaire) = WebFct.PointeurGlobal.VirListeObjetsDico
        Dim ListeObjets = New List(Of Virtualia.Systeme.MetaModele.Outils.FicheObjetDictionnaire)
        Dim FicheDico As Virtualia.Systeme.MetaModele.Outils.FicheObjetDictionnaire
        ListeObjets = (From instance In Dico Select instance Where instance.PointdeVue = WsPointdeVue And instance.Categorie = NoCat Order By instance.Intitule Ascending).ToList
        If ListeObjets.Count = 0 Then
            Exit Sub
        End If
        Dim IndiceC As IEnumerator = ListeObjets.GetEnumerator
        While IndiceC.MoveNext
            FicheDico = CType(IndiceC.Current, Virtualia.Systeme.MetaModele.Outils.FicheObjetDictionnaire)
            NewNoeud = New TreeNode(FicheDico.Intitule, FicheDico.Objet.ToString)
            NewNoeud.PopulateOnDemand = True
            NewNoeud.ImageUrl = "~\Images\Armoire\OrangeFonceFermer16.bmp"
            NewNoeud.SelectAction = TreeNodeSelectAction.Expand
            Noeud.ChildNodes.Add(NewNoeud)
        End While
    End Sub

    Private Sub FaireListeInfos(ByVal Noeud As TreeNode)
        Dim NoObjet As Short
        Dim Libelle As String

        NoObjet = CShort(Noeud.Value)

        Dim Dico As List(Of Virtualia.Systeme.MetaModele.Outils.FicheInfoDictionnaire) = WebFct.PointeurGlobal.VirListeInfosDico
        Dim ListeInfos = New List(Of Virtualia.Systeme.MetaModele.Outils.FicheInfoDictionnaire)
        Dim FicheDico As Virtualia.Systeme.MetaModele.Outils.FicheInfoDictionnaire
        ListeInfos = (From instance In Dico Select instance Where instance.PointdeVue = WsPointdeVue And instance.Objet = NoObjet And instance.SiEditable(WsOutil) = True Order By instance.Intitule Ascending).ToList
        If ListeInfos.Count = 0 Then
            Exit Sub
        End If
        Dim NoeudSaisie As TreeNode = New TreeNode("Informations disponibles", "Info")
        NoeudSaisie.PopulateOnDemand = False
        NoeudSaisie.ImageUrl = "~\Images\Armoire\BleuClairFermer16.bmp"
        NoeudSaisie.SelectAction = TreeNodeSelectAction.Expand
        Noeud.ChildNodes.Add(NoeudSaisie)

        Dim IndiceC As IEnumerator = ListeInfos.GetEnumerator
        While IndiceC.MoveNext
            FicheDico = CType(IndiceC.Current, Virtualia.Systeme.MetaModele.Outils.FicheInfoDictionnaire)
            Libelle = FicheDico.Intitule
            '** Spécifique Libellé des niveaux, Congés *********************************************************
            Select Case V_PointdeVue
                Case VI.PointdeVue.PVueApplicatif
                    If FicheDico.Objet = VI.ObjetPer.ObaOrganigramme Then
                        Select Case FicheDico.Information
                            Case 1 To 4
                                Libelle = WebFct.PointeurUtilisateur.PointeurParametres.LibelleNiveau_Organigramme(FicheDico.Information - 1)
                            Case 14, 15
                                Libelle = WebFct.PointeurUtilisateur.PointeurParametres.LibelleNiveau_Organigramme(FicheDico.Information - 10)
                        End Select
                    End If
                    If FicheDico.Objet = VI.ObjetPer.ObaDroits Then
                        Select Case FicheDico.Information
                            Case 1
                                Libelle = WebFct.PointeurUtilisateur.PointeurParametres.LibelleCompteur_Conge(0)
                            Case 3 To 7
                                Libelle = WebFct.PointeurUtilisateur.PointeurParametres.LibelleCompteur_Conge(FicheDico.Information - 1)
                            Case 8
                                Libelle = WebFct.PointeurUtilisateur.PointeurParametres.LibelleCompteur_Conge(1)
                        End Select
                    End If
            End Select
            '**********************************************************************************************
            If Libelle <> "" Then
                Dim NewNoeud As TreeNode = New TreeNode(Libelle, FicheDico.VIndex.ToString)
                NewNoeud.PopulateOnDemand = False
                NewNoeud.ImageUrl = "~\Images\Armoire\FicheBleue.bmp"
                NewNoeud.SelectAction = TreeNodeSelectAction.Select
                NoeudSaisie.ChildNodes.Add(NewNoeud)
            End If
        End While

        Dim DicoExp As List(Of Virtualia.Systeme.MetaModele.Outils.FicheInfoExperte) = WebFct.PointeurGlobal.VirListeExpertesDico
        Dim ListeExp = New List(Of Virtualia.Systeme.MetaModele.Outils.FicheInfoExperte)
        Dim FicheExp As Virtualia.Systeme.MetaModele.Outils.FicheInfoExperte
        ListeExp = (From instance In DicoExp Select instance Where instance.PointdeVue = WsPointdeVue And instance.Objet = NoObjet And instance.SiEditable(WsOutil) = True Order By instance.Intitule Ascending).ToList
        If ListeExp.Count = 0 Then
            Exit Sub
        End If
        Dim NoeudExpertes As TreeNode = New TreeNode("Services RH", "Experte")
        NoeudExpertes.PopulateOnDemand = False
        NoeudExpertes.ImageUrl = "~\Images\Armoire\VertFonceFermer16.bmp"
        NoeudExpertes.SelectAction = TreeNodeSelectAction.Expand
        Noeud.ChildNodes.Add(NoeudExpertes)

        Dim IndiceD As IEnumerator = ListeExp.GetEnumerator
        While IndiceD.MoveNext
            FicheExp = CType(IndiceD.Current, Virtualia.Systeme.MetaModele.Outils.FicheInfoExperte)
            Dim NewNoeud As TreeNode = New TreeNode(FicheExp.Intitule, FicheExp.Information.ToString)
            NewNoeud.PopulateOnDemand = False
            NewNoeud.ImageUrl = "~\Images\Armoire\FicheVerte.bmp"
            NewNoeud.SelectAction = TreeNodeSelectAction.Select
            NewNoeud.ToolTip = FicheExp.Descriptif
            NoeudExpertes.ChildNodes.Add(NewNoeud)
        End While

        Noeud.ExpandAll()
    End Sub

    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        WebFct = New Virtualia.Net.WebFonctions(Me, 0)
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        TreeDictionnaire.ExpandDepth = 1
        TreeDictionnaire.Nodes(0).Expanded = True
    End Sub

    Protected Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
        WebFct.Dispose()
    End Sub

    Protected Sub CmdRecherche_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles CmdRecherche.Click
        If VRecherche.Text = "" Or VRecherche.Text = "*" Then
            Exit Sub
        End If
        Dim CacheMaj As ArrayList
        Dim ValCourante As String = ""
        Dim NoCourant As Integer = -1

        If Me.ViewState(WsNomState) IsNot Nothing Then
            CacheMaj = CType(Me.ViewState(WsNomState), ArrayList)
            If CacheMaj(0) IsNot Nothing Then
                ValCourante = CacheMaj(0).ToString
            End If
            If CacheMaj(1) IsNot Nothing Then
                NoCourant = CInt(CacheMaj(1))
            End If
            If ValCourante <> VRecherche.Text Then
                NoCourant = -1
            End If
            Me.ViewState.Remove(WsNomState)
            CacheMaj.Clear()
        End If
        NoCourant += 1
        CacheMaj = New ArrayList(1)
        CacheMaj.Add(VRecherche.Text)
        CacheMaj.Add(NoCourant)
        Me.ViewState.Add(WsNomState, CacheMaj)

        Dim SiOk As Boolean
        SiOk = SiInfoSaisieTrouvee(NoCourant, VRecherche.Text)
        If SiOk = True Then
            Exit Sub
        End If
        SiOk = SiInfoExperteTrouvee(NoCourant, VRecherche.Text)
    End Sub

    Private Function SiInfoSaisieTrouvee(ByVal Index As Integer, ByVal Valeur As String) As Boolean
        Dim Predicat As New Virtualia.Systeme.MetaModele.Predicats.PredicateDictionnaire(Valeur)
        Dim Dico As List(Of Virtualia.Systeme.MetaModele.Outils.FicheInfoDictionnaire)
        Dico = WebFct.PointeurGlobal.VirListeInfosDico.FindAll(AddressOf Predicat.AllerAIntitule)
        If Dico.Count = 0 Then
            Return False
            Exit Function
        End If

        If Index > Dico.Count - 1 Then
            Return False
            Exit Function
        End If
        Dim Noeud0 As TreeNode
        Dim NoeudCat As TreeNode
        Dim NoeudObj As TreeNode
        Dim NoeudServ As TreeNode
        Dim IndiceA As Integer
        Dim IndiceB As Integer
        Dim IndiceC As Integer
        Dim IndiceD As Integer

        Noeud0 = TreeDictionnaire.Nodes.Item(0)
        For IndiceA = 0 To Noeud0.ChildNodes.Count - 1
            NoeudCat = Noeud0.ChildNodes.Item(IndiceA)
            If NoeudCat.Value = Dico.Item(Index).Categorie.ToString Then
                NoeudCat.Expand()
                For IndiceB = 0 To NoeudCat.ChildNodes.Count - 1
                    NoeudObj = NoeudCat.ChildNodes.Item(IndiceB)
                    If NoeudObj.Value = Dico.Item(Index).Objet.ToString Then
                        NoeudObj.Expand()
                        For IndiceC = 0 To NoeudObj.ChildNodes.Count - 1
                            NoeudServ = NoeudObj.ChildNodes.Item(IndiceC)
                            NoeudServ.Expand()
                            For IndiceD = 0 To NoeudServ.ChildNodes.Count - 1
                                If NoeudServ.ChildNodes.Item(IndiceD).Text = Dico.Item(Index).Intitule Then
                                    NoeudServ.ChildNodes.Item(IndiceD).Select()
                                    TreeDictionnaire.FindNode(NoeudServ.ChildNodes.Item(IndiceD).ValuePath)
                                    Return True
                                    Exit Function
                                End If
                            Next IndiceD
                            NoeudServ.Collapse()
                        Next IndiceC
                        NoeudObj.Collapse()
                    Else
                        NoeudObj.Collapse()
                    End If
                Next IndiceB
                NoeudCat.Collapse()
            Else
                NoeudCat.Collapse()
            End If
        Next IndiceA
        Return False
    End Function

    Private Function SiInfoExperteTrouvee(ByVal Index As Integer, ByVal Valeur As String) As Boolean
        Dim Predicat As New Virtualia.Systeme.MetaModele.Predicats.PredicateDictionnaire(Valeur)
        Dim Dico As List(Of Virtualia.Systeme.MetaModele.Outils.FicheInfoExperte)
        Dico = WebFct.PointeurGlobal.VirListeExpertesDico.FindAll(AddressOf Predicat.AllerAIntituleExperte)
        If Dico.Count = 0 Then
            Return False
        End If

        If Index > Dico.Count - 1 Then
            Return False
            Exit Function
        End If
        Dim Noeud0 As TreeNode
        Dim NoeudCat As TreeNode
        Dim NoeudObj As TreeNode
        Dim NoeudServ As TreeNode
        Dim IndiceA As Integer
        Dim IndiceB As Integer
        Dim IndiceC As Integer
        Dim IndiceD As Integer

        Noeud0 = TreeDictionnaire.Nodes.Item(0)
        For IndiceA = 0 To Noeud0.ChildNodes.Count - 1
            NoeudCat = Noeud0.ChildNodes.Item(IndiceA)
            NoeudCat.Expand()
            For IndiceB = 0 To NoeudCat.ChildNodes.Count - 1
                NoeudObj = NoeudCat.ChildNodes.Item(IndiceB)
                If NoeudObj.Value = Dico.Item(Index).Objet.ToString Then
                    NoeudObj.Expand()
                    For IndiceC = 0 To NoeudObj.ChildNodes.Count - 1
                        NoeudServ = NoeudObj.ChildNodes.Item(IndiceC)
                        NoeudServ.Expand()
                        For IndiceD = 0 To NoeudServ.ChildNodes.Count - 1
                            If NoeudServ.ChildNodes.Item(IndiceD).Text = Dico.Item(Index).Intitule Then
                                NoeudServ.ChildNodes.Item(IndiceD).Select()
                                TreeDictionnaire.FindNode(NoeudServ.ChildNodes.Item(IndiceD).ValuePath)
                                Return True
                                Exit Function
                            End If
                        Next IndiceD
                        NoeudServ.Collapse()
                    Next IndiceC
                    NoeudObj.Collapse()
                Else
                    NoeudObj.Collapse()
                End If
            Next IndiceB
            NoeudCat.Collapse()
        Next IndiceA
        Return False
    End Function

End Class

Public Class DictionnaireEventArgs
    Inherits EventArgs
    Private WsVIndex As Integer
    Private WsPointdeVue As Short
    Private WsNumObjet As Short
    Private WsNumInfo As Short

    Public ReadOnly Property PointdeVue() As Short
        Get
            Return WsPointdeVue
        End Get
    End Property

    Public ReadOnly Property NumeroObjet() As Short
        Get
            Return WsNumObjet
        End Get
    End Property

    Public ReadOnly Property NumeroInfo() As Short
        Get
            Return WsNumInfo
        End Get
    End Property

    Public ReadOnly Property IndexListe() As Integer
        Get
            Return WsVIndex
        End Get
    End Property

    Public Sub New(ByVal ValeurIndex As Integer)
        WsVIndex = ValeurIndex
    End Sub

    Public Sub New(ByVal Ptdevue As Short, ByVal Objet As Short, ByVal Info As Short)
        WsPointdeVue = Ptdevue
        WsNumObjet = Objet
        WsNumInfo = Info
    End Sub

End Class
