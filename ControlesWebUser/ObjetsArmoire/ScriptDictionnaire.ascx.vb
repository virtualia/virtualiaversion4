﻿Option Strict On
Option Explicit On
Option Compare Text
Imports VI = Virtualia.Systeme.Constantes
Partial Class Controles_ScriptDictionnaire
    Inherits System.Web.UI.UserControl
    Public Delegate Sub ExecutionEventHandler(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.ScriptExecutionEventArgs)
    Public Event ExecutionScript As ExecutionEventHandler
    Public Delegate Sub AppelChoixEventHandler(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.AppelChoixEventArgs)
    Public Event AppelChoix As AppelChoixEventHandler
    Public Delegate Sub Valeur_ChangeEventHandler(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.DonneeChangeEventArgs)
    Public Event ValeurChange As Valeur_ChangeEventHandler
    Public Delegate Sub AppelTableEventHandler(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.AppelTableEventArgs)
    Public Event AppelTable As AppelTableEventHandler
    Private WebFct As Virtualia.Net.WebFonctions
    Private WsPointdeVue As Short = 1
    Private WsOutil As Short = VI.OptionInfo.DicoCmc
    Private WsNomStateScript As String = "Script"
    Private WsNomStateExe As String = "ExeScript"
    '
    Protected Overridable Sub Script_Executer(ByVal e As Virtualia.Systeme.Evenements.ScriptExecutionEventArgs)
        RaiseEvent ExecutionScript(Me, e)
    End Sub

    Protected Overridable Sub Appel_Choix(ByVal e As Virtualia.Systeme.Evenements.AppelChoixEventArgs)
        RaiseEvent AppelChoix(Me, e)
    End Sub

    Protected Overridable Sub Saisie_Change(ByVal e As Virtualia.Systeme.Evenements.DonneeChangeEventArgs)
        RaiseEvent ValeurChange(Me, e)
    End Sub

    Protected Overridable Sub Appel_Table(ByVal e As Virtualia.Systeme.Evenements.AppelTableEventArgs)
        RaiseEvent AppelTable(Me, e)
    End Sub

    Public WriteOnly Property Dontab_RetourAppelTable(ByVal IDAppelant As String) As String
        Set(ByVal value As String)
            CategorieRH.DonText = value
        End Set
    End Property

    Public Property V_PointdeVue() As Short
        Get
            Return WsPointdeVue
        End Get
        Set(ByVal value As Short)
            WsPointdeVue = value
        End Set
    End Property

    Public Property V_BackColor() As System.Drawing.Color
        Get
            Return TreeScript.BackColor
        End Get
        Set(ByVal value As System.Drawing.Color)
            TreeAlbum.BackColor = value
            TreeScript.BackColor = value
        End Set
    End Property

    Public Property V_Width() As System.Web.UI.WebControls.Unit
        Get
            Return TreeScript.Width
        End Get
        Set(ByVal value As System.Web.UI.WebControls.Unit)
            TreeAlbum.Width = value
            TreeScript.Width = value
        End Set
    End Property

    Public Property V_PanelWidth() As System.Web.UI.WebControls.Unit
        Get
            Return PanelCmdScript.Width
        End Get
        Set(ByVal value As System.Web.UI.WebControls.Unit)
            PanelCmdScript.Width = value
            PanelCmdAlbum.Width = value
            PanelDatesScript.Width = value
        End Set
    End Property

    Public Property V_Outil() As Short
        Get
            Return WsOutil
        End Get
        Set(ByVal value As Short)
            WsOutil = value
            Select Case WsOutil
                Case Is = VI.OptionInfo.DicoCmc
                    OptionET.Width = New Unit(40)
                    OptionET.Text = "ET"
                    OptionOU.Width = New Unit(40)
                    OptionOU.Text = "OU"
                    CocheHisto.Visible = False
                    Cmd04.Visible = True
                    Cmd60.Visible = False
                    Cmd61.Visible = False
                    Cmd62.Visible = False
                Case Is = VI.OptionInfo.DicoExport
                    OptionET.Width = New Unit(150)
                    OptionET.Text = "Mode simplifié"
                    OptionOU.Width = New Unit(150)
                    OptionOU.Text = "Mode détaillé"
                    CocheHisto.Visible = True
                    Cmd04.Visible = False
                    Cmd60.Visible = True
                    Cmd61.Visible = True
                    Cmd62.Visible = True
            End Select
        End Set
    End Property

    Public ReadOnly Property VTitre() As String
        Get
            Return IntituleScript.DonText
        End Get
    End Property

    Public ReadOnly Property Script(ByVal Index As Integer) As ArrayList
        Get
            Dim CacheScripts As ArrayList
            If Me.ViewState(WsNomStateScript) Is Nothing Then
                Return Nothing
            End If
            CacheScripts = CType(Me.ViewState(WsNomStateScript), ArrayList)
            Select Case Index
                Case Is > CacheScripts.Count - 1
                    Return Nothing
            End Select
            Return CType(CacheScripts(Index), ArrayList)
        End Get
    End Property

    Public WriteOnly Property AjouterNoeud(ByVal NumLigne As Integer) As ArrayList
        Set(ByVal value As ArrayList)
            Dim Noeud_N0 As TreeNode = Nothing
            Dim Noeud_N1 As TreeNode
            Dim Noeud_N2 As TreeNode
            Dim IndiceI As Integer
            Dim LibelOperateur As String = ""
            Dim Clef As String
            'Cache Script 
            Dim NumObjet As Short 'Position 1
            Dim NumInfo As Short 'Position 2
            Dim OpeComparaison As Short 'Position 3
            Dim OpeInclu As Short 'Position 4
            Dim Intitule As String 'Position 5
            'Position 6 = Lg, Position 7 = Répétition, Position 8 = Format, Position 9 = Libel Format
            'Cache Exe
            Dim OpeETOU As Short = 0 'Position 0
            Dim DateDebut As String = "" 'Position 1
            Dim DateFin As String = WebFct.ViRhDates.DateduJour(False) 'Position 2
            Dim SiHisto As Short = 0  'Position 3
            Dim SiModif As Boolean = False

            If value Is Nothing Then
                Exit Property
            End If
            If value.Count < 4 Then
                Exit Property
            End If
            Select Case WsOutil
                Case VI.OptionInfo.DicoCmc
                    OpeComparaison = CShort(value(3))
                    Select Case OpeComparaison
                        Case VI.Operateurs.Egalite
                            LibelOperateur = "Egalité avec"
                        Case Else
                            LibelOperateur = "Différence avec"
                    End Select
                    OpeInclu = CShort(value(4))
                    Select Case OpeInclu
                        Case VI.Operateurs.Inclu
                            LibelOperateur = "Compris entre"
                    End Select
            End Select
            V_PointdeVue = CShort(value(0))
            NumObjet = CShort(value(1))
            NumInfo = CShort(value(2))
            Try
                Select Case NumInfo
                    Case Is < 500
                        Intitule = WebFct.PointeurDicoInfo(V_PointdeVue, NumObjet, NumInfo).Intitule
                    Case Is > 9000
                        Intitule = value(5).ToString
                    Case Else
                        Intitule = WebFct.PointeurDicoExperte(V_PointdeVue, NumObjet, NumInfo).Intitule
                End Select
            Catch ex As Exception
                Exit Property
            End Try
            Clef = value(1).ToString & VI.Tild & value(2).ToString

            If NumLigne > 0 Then
                If NumLigne < TreeScript.Nodes.Count Then
                    For IndiceI = 0 To TreeScript.Nodes.Count - 1
                        If TreeScript.Nodes.Item(IndiceI).Value = Clef Then
                            Noeud_N0 = TreeScript.Nodes.Item(IndiceI)
                            SiModif = True
                            Exit For
                        End If
                    Next IndiceI
                End If
            Else
                If TreeScript.Nodes.Count > 0 Then
                    If TreeScript.Nodes.Item(0).Value = Clef Then
                        Noeud_N0 = TreeScript.Nodes.Item(0)
                        SiModif = True
                    End If
                End If
                If TreeScript.SelectedNode IsNot Nothing Then
                    If TreeScript.SelectedNode.Text = "?" And CShort(value(2)) < 9000 Then
                        Noeud_N0 = TreeScript.SelectedNode
                        Noeud_N0.Text = Intitule
                        Noeud_N0.Value = Clef
                        NumLigne = TreeScript.Nodes.IndexOf(TreeScript.SelectedNode)
                        SiModif = True
                    End If
                End If
            End If
            If SiModif = False Then
                Noeud_N0 = New TreeNode(Intitule, Clef)
                Noeud_N0.PopulateOnDemand = False
            End If

            Select Case NumInfo
                Case Is < 500
                    Noeud_N0.ImageUrl = "~\Images\Armoire\FicheBleue_BleuClair.bmp"
                Case Else
                    Noeud_N0.ImageUrl = "~\Images\Armoire\FicheVerte_BleuClair.bmp"
            End Select
            Noeud_N0.SelectAction = TreeNodeSelectAction.SelectExpand

            If SiModif = False Then
                If NumLigne > 0 And NumLigne < TreeScript.Nodes.Count Then
                    TreeScript.Nodes.AddAt(NumLigne, Noeud_N0)
                Else
                    TreeScript.Nodes.Add(Noeud_N0)
                End If
            Else
                Noeud_N0.ChildNodes.Clear()
            End If

            Select Case WsOutil
                Case VI.OptionInfo.DicoCmc
                    Noeud_N1 = New TreeNode(LibelOperateur, Noeud_N0.ChildNodes.Count.ToString)
                    Noeud_N1.PopulateOnDemand = False
                    Noeud_N1.ImageUrl = "~\Images\Icones\Fleche_BleuClair.bmp"
                    Noeud_N1.SelectAction = TreeNodeSelectAction.SelectExpand
                    Noeud_N0.ChildNodes.Add(Noeud_N1)

                    For IndiceI = 5 To value.Count - 1
                        If value(IndiceI).ToString = "" Then
                            Exit For
                        End If
                        Noeud_N2 = New TreeNode(value(IndiceI).ToString, IndiceI.ToString)
                        Noeud_N2.PopulateOnDemand = False
                        Noeud_N2.ImageUrl = "~\Images\Icones\Choix_BleuClair.bmp"
                        Noeud_N2.SelectAction = TreeNodeSelectAction.Select
                        Noeud_N1.ChildNodes.Add(Noeud_N2)
                    Next IndiceI
                Case VI.OptionInfo.DicoExport
                    Dim Libel As String
                    Noeud_N1 = Nothing
                    For IndiceI = 5 To value.Count - 1
                        Libel = ""
                        Select Case IndiceI
                            Case 5
                                Libel = "Nom de la colonne = " & value(IndiceI).ToString
                            Case 6
                                If value(IndiceI).ToString <> "0" Then
                                    Libel = "Longueur = " & value(IndiceI).ToString
                                End If
                            Case 7
                                If value(IndiceI).ToString <> "0" Then
                                    Libel = "Répétition de la colonne = " & value(IndiceI).ToString
                                End If
                            Case 8
                                Select Case NumInfo
                                    Case Is < 500
                                        Libel = "Format = " & FormatEnClair(WebFct.PointeurDicoInfo(V_PointdeVue, NumObjet, NumInfo).VNature, CShort(value(IndiceI)))
                                    Case Is > 9000
                                        Libel = ""
                                    Case Else
                                        Libel = "Format = " & FormatEnClair(WebFct.PointeurDicoExperte(V_PointdeVue, NumObjet, NumInfo).VNature, CShort(value(IndiceI)))
                                End Select
                            Case 9
                                If value(IndiceI).ToString <> "" Then
                                    Libel = "Filtre sur "
                                End If
                            Case Else
                                Libel = "ValeursFiltre"
                        End Select
                        If Libel <> "" Then
                            Select Case IndiceI
                                Case 5 To 9
                                    Noeud_N1 = New TreeNode(Libel, Noeud_N0.ChildNodes.Count.ToString)
                                    Noeud_N1.PopulateOnDemand = False
                                    Noeud_N1.ImageUrl = "~\Images\Icones\Fleche_BleuClair.bmp"
                                    Noeud_N1.SelectAction = TreeNodeSelectAction.SelectExpand
                                    Noeud_N0.ChildNodes.Add(Noeud_N1)
                            End Select
                            Select Case IndiceI
                                Case Is >= 9
                                    Noeud_N2 = New TreeNode(value(IndiceI).ToString, IndiceI.ToString)
                                    Noeud_N2.PopulateOnDemand = False
                                    Noeud_N2.ImageUrl = "~\Images\Icones\Choix_BleuClair.bmp"
                                    Noeud_N2.SelectAction = TreeNodeSelectAction.Select
                                    If Noeud_N1 IsNot Nothing Then
                                        Noeud_N1.ChildNodes.Add(Noeud_N2)
                                    End If
                            End Select
                        End If
                    Next IndiceI
            End Select

            If NumLigne > 0 Then
                Noeud_N0.Selected = True
            End If
            Noeud_N0.ExpandAll()

            Dim CacheScripts As ArrayList
            If Me.ViewState(WsNomStateScript) IsNot Nothing Then
                CacheScripts = CType(Me.ViewState(WsNomStateScript), ArrayList)
            Else
                CacheScripts = New ArrayList
            End If
            If SiModif = True Then
                CacheScripts(NumLigne) = value
            Else
                CacheScripts.Add(value)
            End If
            Me.ViewState.Remove(WsNomStateScript)
            Me.ViewState.Add(WsNomStateScript, CacheScripts)

            Dim CacheExe As ArrayList
            If Me.ViewState(WsNomStateExe) IsNot Nothing Then
                CacheExe = CType(Me.ViewState(WsNomStateExe), ArrayList)
                OpeETOU = CShort(CacheExe(0))
                DateDebut = CacheExe(1).ToString
                DateFin = CacheExe(2).ToString
                SiHisto = CShort(CacheExe(3))
            Else
                CacheExe = New ArrayList
            End If
            Select Case OpeETOU
                Case VI.Operateurs.ET
                    OptionET.Checked = True
                    OptionET.Font.Bold = True
                Case Else
                    OptionOU.Checked = True
                    OptionOU.Font.Bold = True
            End Select
            ExeDateDebut.Text = DateDebut
            ExeDateFin.Text = DateFin
            Select Case SiHisto
                Case 1
                    CocheHisto.Checked = True
                    CocheHisto.Font.Bold = True
                Case Else
                    CocheHisto.Checked = False
                    CocheHisto.Font.Bold = False
            End Select
            CacheExe.Add(OpeETOU)
            CacheExe.Add(DateDebut)
            CacheExe.Add(DateFin)
            CacheExe.Add(SiHisto)
            Me.ViewState.Remove(WsNomStateExe)
            Me.ViewState.Add(WsNomStateExe, CacheExe)

        End Set
    End Property

    Protected Sub OptionV_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles OptionET.CheckedChanged, OptionOU.CheckedChanged
        Select Case OptionET.Checked
            Case True
                OptionET.Font.Bold = True
                OptionOU.Font.Bold = False
            Case False
                OptionET.Font.Bold = False
                OptionOU.Font.Bold = True
        End Select
        Dim CacheExe As ArrayList
        If Me.ViewState(WsNomStateExe) IsNot Nothing Then
            CacheExe = CType(Me.ViewState(WsNomStateExe), ArrayList)
        Else
            Exit Sub
        End If
        Select Case OptionET.Checked
            Case True
                CacheExe(0) = VI.Operateurs.ET
            Case False
                CacheExe(0) = VI.Operateurs.OU
        End Select
        Me.ViewState.Remove(WsNomStateExe)
        Me.ViewState.Add(WsNomStateExe, CacheExe)

        Dim Evenement As Virtualia.Systeme.Evenements.DonneeChangeEventArgs
        Evenement = New Virtualia.Systeme.Evenements.DonneeChangeEventArgs(Strings.Join(CacheExe.ToArray, VI.PointVirgule))
        Saisie_Change(Evenement)
    End Sub

    Protected Sub ExeDateDebut_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ExeDateDebut.TextChanged
        Dim CacheExe As ArrayList
        If Me.ViewState(WsNomStateExe) IsNot Nothing Then
            CacheExe = CType(Me.ViewState(WsNomStateExe), ArrayList)
        Else
            Exit Sub
        End If
        Dim Chaine As String
        Chaine = WebFct.ViRhDates.DateSaisieVerifiee(ExeDateDebut.Text)
        CacheExe(1) = Chaine
        Me.ViewState.Remove(WsNomStateExe)
        Me.ViewState.Add(WsNomStateExe, CacheExe)

        Dim Evenement As Virtualia.Systeme.Evenements.DonneeChangeEventArgs
        Evenement = New Virtualia.Systeme.Evenements.DonneeChangeEventArgs(Strings.Join(CacheExe.ToArray, VI.PointVirgule))
        Saisie_Change(Evenement)
    End Sub

    Protected Sub ExeDateFin_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ExeDateFin.TextChanged
        Dim CacheExe As ArrayList
        If Me.ViewState(WsNomStateExe) IsNot Nothing Then
            CacheExe = CType(Me.ViewState(WsNomStateExe), ArrayList)
        Else
            Exit Sub
        End If
        Dim Chaine As String
        Chaine = WebFct.ViRhDates.DateSaisieVerifiee(ExeDateFin.Text)
        CacheExe(2) = Chaine
        Me.ViewState.Remove(WsNomStateExe)
        Me.ViewState.Add(WsNomStateExe, CacheExe)

        Dim Evenement As Virtualia.Systeme.Evenements.DonneeChangeEventArgs
        Evenement = New Virtualia.Systeme.Evenements.DonneeChangeEventArgs(Strings.Join(CacheExe.ToArray, VI.PointVirgule))
        Saisie_Change(Evenement)
    End Sub

    Protected Sub CocheHisto_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles CocheHisto.CheckedChanged
        Select Case CocheHisto.Checked
            Case True
                CocheHisto.Font.Bold = True
            Case False
                CocheHisto.Font.Bold = False
        End Select
        Dim CacheExe As ArrayList
        If Me.ViewState(WsNomStateExe) IsNot Nothing Then
            CacheExe = CType(Me.ViewState(WsNomStateExe), ArrayList)
        Else
            Exit Sub
        End If
        Select Case CocheHisto.Checked
            Case True
                CacheExe(3) = 1
            Case False
                CacheExe(3) = 0
        End Select
        Me.ViewState.Remove(WsNomStateExe)
        Me.ViewState.Add(WsNomStateExe, CacheExe)

        Dim Evenement As Virtualia.Systeme.Evenements.DonneeChangeEventArgs
        Evenement = New Virtualia.Systeme.Evenements.DonneeChangeEventArgs(Strings.Join(CacheExe.ToArray, VI.PointVirgule))
        Saisie_Change(Evenement)
    End Sub

    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        WebFct = New Virtualia.Net.WebFonctions(Me, 0)
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Dim CacheExe As ArrayList
        'Cache Exe
        Dim OpeETOU As Short = 0 'Position 0
        Dim DateDebut As String = "" 'Position 1
        Dim DateFin As String = WebFct.ViRhDates.DateduJour(False) 'Position 2
        Dim SiHisto As Short = 0  'Position 3

        If Me.ViewState(WsNomStateExe) IsNot Nothing Then
            CacheExe = CType(Me.ViewState(WsNomStateExe), ArrayList)
            OpeETOU = CShort(CacheExe(0))
            DateDebut = CacheExe(1).ToString
            DateFin = CacheExe(2).ToString
            SiHisto = CShort(CacheExe(3))
            Me.ViewState.Remove(WsNomStateExe)
        End If
        CacheExe = New ArrayList

        Select Case OpeETOU
            Case VI.Operateurs.ET
                OptionET.Checked = True
                OptionET.Font.Bold = True
            Case Else
                OptionOU.Checked = True
                OptionOU.Font.Bold = True
        End Select
        ExeDateDebut.Text = DateDebut
        ExeDateFin.Text = DateFin
        Select Case SiHisto
            Case 1
                CocheHisto.Checked = True
                CocheHisto.Font.Bold = True
            Case Else
                CocheHisto.Checked = False
                CocheHisto.Font.Bold = False
        End Select
        CacheExe.Add(OpeETOU)
        CacheExe.Add(DateDebut)
        CacheExe.Add(DateFin)
        CacheExe.Add(SiHisto)
        Me.ViewState.Add(WsNomStateExe, CacheExe)

        TreeAlbum.ExpandDepth = 1
        TreeAlbum.Nodes(0).Expanded = True
    End Sub

    Protected Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
        WebFct.Dispose()
    End Sub

    Protected Sub Cmd01_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Cmd01.Click
        'Nouveau Script
        TreeScript.Nodes.Clear()
        If Me.ViewState(WsNomStateScript) IsNot Nothing Then
            Me.ViewState.Remove(WsNomStateScript)
        End If
        SelIdeW.Value = "0"
        NoeudW.Value = ""
        IntituleScript.DonText = ""
        CategorieRH.DonText = ""
        Dim Evenement As Virtualia.Systeme.Evenements.DonneeChangeEventArgs
        Evenement = New Virtualia.Systeme.Evenements.DonneeChangeEventArgs("Nouveau")
        Saisie_Change(Evenement)
    End Sub

    Protected Sub Cmd02_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Cmd02.Click
        'Modifier le script
        If Me.ViewState(WsNomStateScript) Is Nothing Then
            Exit Sub
        End If

        Dim CacheScripts As ArrayList
        Dim Evenement As Virtualia.Systeme.Evenements.AppelChoixEventArgs

        CacheScripts = CType(Me.ViewState(WsNomStateScript), ArrayList)
        Dim I As Integer = 0
        Dim Ligne As ArrayList
        Do
            Ligne = CType(CacheScripts(I), ArrayList)
            If Ligne Is Nothing Then
                Exit Do
            End If
            Select Case TreeScript.Nodes.Item(I).Selected
                Case True
                    Exit Do
            End Select
            I += 1
            If I > CacheScripts.Count - 1 Then
                Ligne = Nothing
                Exit Do
            End If
        Loop
        If Ligne IsNot Nothing Then
            Evenement = New Virtualia.Systeme.Evenements.AppelChoixEventArgs(I, Ligne)
            Appel_Choix(Evenement)
        End If
    End Sub

    Protected Sub Cmd03_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Cmd03.Click
        'Effacer une ligne
        If Me.ViewState(WsNomStateScript) Is Nothing Then
            Exit Sub
        End If
        Dim CacheScripts As ArrayList
        CacheScripts = CType(Me.ViewState(WsNomStateScript), ArrayList)
        Dim I As Integer = 0
        Dim Ligne As ArrayList
        Do
            Ligne = CType(CacheScripts(I), ArrayList)
            If Ligne Is Nothing Then
                Exit Do
            End If
            Select Case TreeScript.Nodes.Item(I).Selected
                Case True
                    TreeScript.Nodes.RemoveAt(I)
                    CacheScripts.RemoveAt(I)
                    Exit Do
            End Select
            I += 1
            If I > CacheScripts.Count - 1 Then
                Ligne = Nothing
                Exit Do
            End If
        Loop
        Me.ViewState.Remove(WsNomStateScript)
        Me.ViewState.Add(WsNomStateScript, CacheScripts)
    End Sub

    Protected Sub Cmd05_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Cmd05.Click
        'Exécuter
        If Me.ViewState(WsNomStateScript) Is Nothing Then
            Exit Sub
        End If
        If Me.ViewState(WsNomStateExe) Is Nothing Then
            Exit Sub
        End If
        Dim CacheScripts As ArrayList
        Dim CacheExe As ArrayList
        Dim Evenement As Virtualia.Systeme.Evenements.ScriptExecutionEventArgs

        CacheScripts = CType(Me.ViewState(WsNomStateScript), ArrayList)
        CacheExe = CType(Me.ViewState(WsNomStateExe), ArrayList)
        Select Case V_Outil
            Case VI.OptionInfo.DicoCmc
                Evenement = New Virtualia.Systeme.Evenements.ScriptExecutionEventArgs(V_PointdeVue, VI.OptionInfo.DicoCmc, CacheExe, CacheScripts)
                Call Script_Executer(Evenement)
            Case VI.OptionInfo.DicoExport
                Evenement = New Virtualia.Systeme.Evenements.ScriptExecutionEventArgs(V_PointdeVue, VI.OptionInfo.DicoExport, CacheExe, CacheScripts)
                Call Script_Executer(Evenement)
        End Select
    End Sub

    Protected Sub Cmd11_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Cmd11.Click
        'Enregistrer dans l'Album
        Dim FicheScript As Virtualia.Net.Script.ObjetScript
        Dim PvueOutil As Short
        Dim ColScripts As Virtualia.Net.Script.EnsembleScripts
        Dim SelIde As Integer = 0

        If IsNumeric(SelIdeW.Value) Then
            SelIde = CInt(SelIdeW.Value)
        End If
        Select Case WsOutil
            Case VI.OptionInfo.DicoCmc
                PvueOutil = VI.PointdeVue.PVueScriptCmc
            Case VI.OptionInfo.DicoExport
                PvueOutil = VI.PointdeVue.PVueScriptExport
        End Select
        ColScripts = WebFct.PointeurUtilisateur.PointeurScripts(PvueOutil, V_PointdeVue)
        If SelIde > 0 Then
            FicheScript = ColScripts.ListeDesScripts.Find(Function(Recherche) Recherche.Identifiant = CInt(SelIde))
            If FicheScript Is Nothing Then
                SelIde = 0
                SelIdeW.Value = "0"
                NoeudW.Value = ""
            End If
        Else
            FicheScript = New Virtualia.Net.Script.ObjetScript(ColScripts, 0)
        End If
        FicheScript.Intitule = IntituleScript.DonText
        FicheScript.Categorie = WebFct.PointeurGlobal.CategorieNum(CategorieRH.DonText)

        Dim CacheScripts As ArrayList
        Dim CacheExe As ArrayList
        If Me.ViewState(WsNomStateScript) IsNot Nothing Then
            CacheScripts = CType(Me.ViewState(WsNomStateScript), ArrayList)
        Else
            SelIde = 0
            SelIdeW.Value = "0"
            NoeudW.Value = ""
            Exit Sub
        End If
        If Me.ViewState(WsNomStateExe) IsNot Nothing Then
            CacheExe = CType(Me.ViewState(WsNomStateExe), ArrayList)
        Else
            SelIde = 0
            SelIdeW.Value = "0"
            NoeudW.Value = ""
            Exit Sub
        End If
        Dim Cretour As Boolean
        Select Case WsOutil
            Case VI.OptionInfo.DicoCmc
                Cretour = FicheScript.MajCMC(CacheExe, CacheScripts)
            Case VI.OptionInfo.DicoExport
                Cretour = FicheScript.MajExtraction(CacheExe, CacheScripts)
        End Select
        If Cretour = True Then
            Call WebFct.PointeurUtilisateur.InitialiserScripts(PvueOutil)
            ColScripts = WebFct.PointeurUtilisateur.PointeurScripts(PvueOutil, V_PointdeVue)
            TreeAlbum.Nodes(0).ChildNodes.Clear()
            Call FaireListeCategories(TreeAlbum.Nodes(0))
        End If
    End Sub

    Protected Sub Cmd12_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Cmd12.Click
        'Supprimer de l'Album
        Dim SelIde As Integer = 0
        If IsNumeric(SelIdeW.Value) Then
            SelIde = CInt(SelIdeW.Value)
        End If
        If SelIde = 0 Then Exit Sub

        Dim FicheScript As Virtualia.Net.Script.ObjetScript
        Dim PvueOutil As Short
        Dim ColScripts As Virtualia.Net.Script.EnsembleScripts

        Select Case WsOutil
            Case VI.OptionInfo.DicoCmc
                PvueOutil = VI.PointdeVue.PVueScriptCmc
            Case VI.OptionInfo.DicoExport
                PvueOutil = VI.PointdeVue.PVueScriptExport
        End Select

        ColScripts = WebFct.PointeurUtilisateur.PointeurScripts(PvueOutil, V_PointdeVue)
        FicheScript = ColScripts.ListeDesScripts.Find(Function(Recherche) Recherche.Identifiant = CInt(SelIde))
        If FicheScript Is Nothing Then
            SelIdeW.Value = "0"
            NoeudW.Value = ""
            IntituleScript.DonText = ""
            CategorieRH.DonText = ""
            Exit Sub
        End If

        Dim Cretour As Boolean = FicheScript.SupprimerDossier
        If Cretour = True Then
            ColScripts.ListeDesScripts.Remove(FicheScript)
            Dim Noeud As TreeNode = TreeAlbum.FindNode(NoeudW.Value)
            If Noeud IsNot Nothing Then
                Dim TableauTmp(0) As String
                Dim N1 As Integer
                TableauTmp = Strings.Split(NoeudW.Value, TreeAlbum.PathSeparator)
                N1 = CInt(TableauTmp(1))
                TreeAlbum.Nodes.Item(0).ChildNodes(N1).ChildNodes.Remove(Noeud)
            End If
        End If

        SelIdeW.Value = "0"
        NoeudW.Value = ""
        IntituleScript.DonText = ""
        CategorieRH.DonText = ""
    End Sub

    Protected Sub Cmd60_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Cmd60.Click
        'Insérer une colonne avant la colonne sélectionnée
        If Me.ViewState(WsNomStateScript) Is Nothing Then
            Exit Sub
        End If

        Dim CacheScripts As ArrayList

        CacheScripts = CType(Me.ViewState(WsNomStateScript), ArrayList)
        Dim I As Integer = 0
        Dim Ligne As ArrayList
        Do
            Ligne = CType(CacheScripts(I), ArrayList)
            If Ligne Is Nothing Then
                Exit Do
            End If
            Select Case TreeScript.Nodes.Item(I).Selected
                Case True
                    Exit Do
            End Select
            I += 1
            If I > CacheScripts.Count - 1 Then
                Ligne = Nothing
                Exit Do
            End If
        Loop
        If Ligne IsNot Nothing And I > 0 Then
            Dim K As Integer
            CacheScripts.Add("")
            For K = CacheScripts.Count - 1 To I Step -1
                CacheScripts(K) = CacheScripts(K - 1)
            Next K
            Ligne(0) = 0
            Ligne(1) = 0
            Ligne(2) = 10000
            Ligne(3) = 0
            Ligne(4) = 0
            Ligne(5) = "?"
            AjouterNoeud(I) = Ligne
        End If

    End Sub

    Protected Sub Cmd61_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Cmd61.Click
        'Ajouter une colonne avec une valeur constante
        Dim CacheScripts As ArrayList
        Dim Ligne As New ArrayList
        Dim NumLigne As Integer = 0
        Ligne.Add(V_PointdeVue)
        Ligne.Add(0)
        If Me.ViewState(WsNomStateScript) IsNot Nothing Then
            CacheScripts = CType(Me.ViewState(WsNomStateScript), ArrayList)
            Ligne.Add(9000 + CacheScripts.Count + 1)
            NumLigne = CacheScripts.Count
        Else
            Ligne.Add(9001)
        End If
        Ligne.Add(0)
        Ligne.Add(0)
        Ligne.Add("Constante")
        Ligne.Add(0)
        Ligne.Add(0)
        Ligne.Add(0)
        Ligne.Add(0)
        AjouterNoeud(NumLigne) = Ligne
    End Sub

    Protected Sub Cmd62_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Cmd62.Click
        'Filtre sur la colonne
        If Me.ViewState(WsNomStateScript) Is Nothing Then
            Exit Sub
        End If

        Dim CacheScripts As ArrayList

        CacheScripts = CType(Me.ViewState(WsNomStateScript), ArrayList)
        Dim I As Integer = 0
        Dim Ligne As ArrayList
        Do
            Ligne = CType(CacheScripts(I), ArrayList)
            If Ligne Is Nothing Then
                Exit Do
            End If
            Select Case TreeScript.Nodes.Item(I).Selected
                Case True
                    Exit Do
            End Select
            I += 1
            If I > CacheScripts.Count - 1 Then
                Ligne = Nothing
                Exit Do
            End If
        Loop
        If Ligne Is Nothing Then
            Exit Sub
        End If
        Dim Evenement As Virtualia.Systeme.Evenements.AppelChoixEventArgs
        Evenement = New Virtualia.Systeme.Evenements.AppelChoixEventArgs(I, Ligne, "Filtre")
        Appel_Choix(Evenement)
    End Sub

    Private Function FormatEnClair(ByVal Nature As Short, ByVal Index As Integer) As String
        Dim LibelNat As String = ""
        Dim Chaine As String
        Select Case Nature
            Case VI.NatureDonnee.DonneeDate
                LibelNat = "Date"
            Case VI.NatureDonnee.DonneeNumerique
                LibelNat = "Numeric"
            Case Else
                LibelNat = "Alpha"
        End Select
        Chaine = WebFct.ViRhFonction.OptionInformation(LibelNat, Index)
        Return Chaine
    End Function

    Private ReadOnly Property LibelleRetour() As String
        Get
            Select Case WsOutil
                Case VI.OptionInfo.DicoCmc
                    Return "CMC"
                Case VI.OptionInfo.DicoExport
                    Return "Edition"
                Case Else
                    Return ""
            End Select
        End Get
    End Property

    Protected Sub TreeAlbum_SelectedNodeChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles TreeAlbum.SelectedNodeChanged
        Select Case CType(sender, System.Web.UI.WebControls.TreeView).SelectedNode.Depth
            Case Is < 1
                Exit Sub
        End Select
        Dim SelIde As String = CType(sender, System.Web.UI.WebControls.TreeView).SelectedNode.Value
        IntituleScript.DonText = CType(sender, System.Web.UI.WebControls.TreeView).SelectedNode.Text
        CategorieRH.DonText = CType(sender, System.Web.UI.WebControls.TreeView).SelectedNode.Parent.Text

        Dim FicheScript As Virtualia.Net.Script.ObjetScript
        Dim PvueOutil As Short
        Dim ColScripts As Virtualia.Net.Script.EnsembleScripts
        Dim I As Integer

        Select Case WsOutil
            Case VI.OptionInfo.DicoCmc
                PvueOutil = VI.PointdeVue.PVueScriptCmc
            Case VI.OptionInfo.DicoExport
                PvueOutil = VI.PointdeVue.PVueScriptExport
        End Select

        ColScripts = WebFct.PointeurUtilisateur.PointeurScripts(PvueOutil, V_PointdeVue)
        FicheScript = ColScripts.ListeDesScripts.Find(Function(Recherche) Recherche.Identifiant = CInt(SelIde))

        TreeScript.Nodes.Clear()
        If FicheScript IsNot Nothing Then

            SelIdeW.Value = SelIde
            NoeudW.Value = CType(sender, System.Web.UI.WebControls.TreeView).SelectedNode.ValuePath

            If Me.ViewState(WsNomStateExe) IsNot Nothing Then
                Me.ViewState.Remove(WsNomStateExe)
            End If
            Me.ViewState.Add(WsNomStateExe, FicheScript.ConditionsLues)
            If Me.ViewState(WsNomStateScript) IsNot Nothing Then
                Me.ViewState.Remove(WsNomStateScript)
            End If
            For I = 0 To FicheScript.ScriptLu.Count - 1
                AjouterNoeud(0) = CType(FicheScript.ScriptLu(I), ArrayList)
            Next I
        End If

        Dim Evenement As Virtualia.Systeme.Evenements.DonneeChangeEventArgs
        Evenement = New Virtualia.Systeme.Evenements.DonneeChangeEventArgs("Nouveau")
        Saisie_Change(Evenement)
    End Sub

    Protected Sub TreeAlbum_TreeNodeExpanded(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.TreeNodeEventArgs) Handles TreeAlbum.TreeNodeExpanded
        e.Node.Expand()
    End Sub

    Protected Sub TreeAlbum_TreeNodePopulate(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.TreeNodeEventArgs) Handles TreeAlbum.TreeNodePopulate
        TreeAlbum.ImageSet = System.Web.UI.WebControls.TreeViewImageSet.XPFileExplorer
        If e.Node.ChildNodes.Count = 0 Then
            Select Case e.Node.Depth
                Case Is = 0
                    Call FaireListeCategories(e.Node)
                Case Is = 1
                    Call FaireListeScripts(e.Node)
            End Select
        End If
    End Sub

    Private Sub FaireListeScripts(ByVal Noeud As TreeNode)
        Dim ColScripts As Virtualia.Net.Script.EnsembleScripts
        Dim NewNoeud As TreeNode
        Dim NoCat As Short = CShort(Noeud.Value)
        Dim PvueOutil As Short
        Select Case WsOutil
            Case VI.OptionInfo.DicoCmc
                PvueOutil = VI.PointdeVue.PVueScriptCmc
            Case VI.OptionInfo.DicoExport
                PvueOutil = VI.PointdeVue.PVueScriptExport
        End Select
        ColScripts = WebFct.PointeurUtilisateur.PointeurScripts(PvueOutil, V_PointdeVue)
        If ColScripts Is Nothing Then
            Exit Sub
        End If

        Dim ListeScripts As New List(Of Virtualia.Net.Script.ObjetScript)
        Dim FicheScript As Virtualia.Net.Script.ObjetScript
        Try
            ListeScripts = (From instance In ColScripts.ListeDesScripts Select instance Where instance.PointdeVue = V_PointdeVue And instance.Categorie = NoCat Order By instance.Intitule Ascending).ToList
            If ListeScripts.Count = 0 Then
                Exit Sub
            End If
        Catch ex As Exception
            Exit Sub
        End Try

        Dim IndiceC As IEnumerator = ListeScripts.GetEnumerator
        While IndiceC.MoveNext
            FicheScript = CType(IndiceC.Current, Virtualia.Net.Script.ObjetScript)
            NewNoeud = New TreeNode(FicheScript.Intitule, FicheScript.Identifiant.ToString)
            NewNoeud.PopulateOnDemand = False
            Select Case WsOutil
                Case VI.OptionInfo.DicoCmc
                    NewNoeud.ImageUrl = "~\Images\Menus\ArmoireJauneStd.bmp"
                Case VI.OptionInfo.DicoExport
                    NewNoeud.ImageUrl = "~\Images\Menus\EditerStd.bmp"
            End Select
            NewNoeud.SelectAction = TreeNodeSelectAction.Select
            Noeud.ChildNodes.Add(NewNoeud)
        End While
        Noeud.Expanded = True
        Noeud.Expand()
    End Sub

    Private Sub FaireListeCategories(ByVal Noeud As TreeNode)
        Dim NoCat As Integer = 0
        Dim NewNoeud As TreeNode

        Do
            NewNoeud = New TreeNode(WebFct.PointeurGlobal.LibelleCategorie(NoCat), NoCat.ToString)
            NewNoeud.PopulateOnDemand = True
            NewNoeud.ImageUrl = "~\Images\General\CarreUniversel.bmp"
            NewNoeud.SelectAction = TreeNodeSelectAction.Expand
            Noeud.ChildNodes.Add(NewNoeud)
            NoCat += 1
            If NoCat > 12 Then
                Exit Do
            End If
        Loop

    End Sub

    Protected Sub IntituleScript_ValeurChange(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.DonneeChangeEventArgs) Handles IntituleScript.ValeurChange
        Dim SelIde As Integer = 0
        If IsNumeric(SelIdeW.Value) Then
            SelIde = CInt(SelIdeW.Value)
        End If
        If SelIde = 0 Then Exit Sub

        Dim FicheScript As Virtualia.Net.Script.ObjetScript
        Dim PvueOutil As Short
        Dim ColScripts As Virtualia.Net.Script.EnsembleScripts

        Select Case WsOutil
            Case VI.OptionInfo.DicoCmc
                PvueOutil = VI.PointdeVue.PVueScriptCmc
            Case VI.OptionInfo.DicoExport
                PvueOutil = VI.PointdeVue.PVueScriptExport
        End Select

        ColScripts = WebFct.PointeurUtilisateur.PointeurScripts(PvueOutil, V_PointdeVue)
        FicheScript = ColScripts.ListeDesScripts.Find(Function(Recherche) Recherche.Identifiant = CInt(SelIde))
        If FicheScript Is Nothing Then
            Exit Sub
        End If
        IntituleScript.DonText = e.Valeur
    End Sub

    Protected Sub CategorieRH_AppelTable(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.AppelTableEventArgs) Handles CategorieRH.AppelTable
        Dim Evenement As Virtualia.Systeme.Evenements.AppelTableEventArgs
        Evenement = New Virtualia.Systeme.Evenements.AppelTableEventArgs(e.ControleAppelant, e.ObjetAppelant, e.PointdeVueInverse, e.NomdelaTable)
        Appel_Table(Evenement)
    End Sub

End Class
