﻿<%@ Control Language="VB" AutoEventWireup="false" Inherits="Virtualia.Net.Controles_Dictionnaire" Codebehind="Dictionnaire.ascx.vb" %>

<asp:Table ID="Dictionnaire" runat="server" Width="340px">
    <asp:TableRow>
        <asp:TableCell Width="340px" HorizontalAlign="Left">
            <asp:Table ID="CadreAllerA" runat="server" Width="350px" CellPadding="0" CellSpacing="0">
                <asp:TableRow>
                    <asp:TableCell Width="90px" HorizontalAlign="Justify"> 
                        <asp:Label ID="EtiRecherche" runat="server" Height="18px" Width="80px" Text="Aller à"
                                BackColor="#225C59" BorderColor="#98D4CA"  BorderStyle="Outset"
                                BorderWidth="2px" ForeColor="#E9FDF9" Font-Italic="true"
                                Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                style="margin-top: 0px; margin-left: 1px; text-indent: 5px; text-align: center" >
                        </asp:Label>
                    </asp:TableCell> 
                    <asp:TableCell Width="155px" HorizontalAlign="Justify"> 
                        <asp:TextBox ID="VRecherche" runat="server" BackColor="Snow" 
                            BorderColor="Snow" BorderStyle="Inset" Height="15px" Width="215px" 
                            Font-Bold="false" ForeColor="#142425"></asp:TextBox>
                    </asp:TableCell>
                    <asp:TableCell Width="10px"></asp:TableCell>
                    <asp:TableCell Width="90px" HorizontalAlign="Justify"> 
                        <asp:ImageButton ID="CmdRecherche" runat="server" BorderStyle="None"
                           ImageAlign="Middle" ImageUrl="~/Images/Menus/Executer_Dico.bmp"
                           ToolTip="Accés direct à une préoccupation"
                           style="margin-bottom: 2px" />
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell Width="340px" HorizontalAlign="Justify">
            <asp:Panel ID="PanelLstDico" runat="server" Width="340px" Height="1150px" BorderStyle="Outset"
                                BorderWidth="2px" BorderColor="#CAEBE4" BackColor="White" ScrollBars="Vertical" 
                                style="display: table-cell;" >
                <asp:TreeView ID="TreeDictionnaire" runat="server" BorderStyle="None" NodeIndent="7"
                     Font-Italic="True" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                     ForeColor="#124545" MaxDataBindDepth="2" Height="1140px" Width="340px" ImageSet="XPFileExplorer"
                     RootNodeStyle-ImageUrl="~/Images/Icones/Dico.bmp" RootNodeStyle-HorizontalPadding="10"
                      ParentNodeStyle-Font-Bold="true"
                     style="text-align:left; vertical-align:top; margin-left:0px; margin-top: 2px; margin-bottom: 10px;" >
                     <SelectedNodeStyle BackColor="#6C9690" BorderColor="Snow" BorderStyle="Solid" 
                         Forecolor="White" Font-Overline="true" />
                     <Nodes>
                         <asp:TreeNode PopulateOnDemand="true" Text="Dictionnaire de données" 
                             SelectAction="Expand" Value="MetaModele"></asp:TreeNode>
                     </Nodes>
               </asp:TreeView>
            </asp:Panel>
        </asp:TableCell>
    </asp:TableRow>
</asp:Table>
   