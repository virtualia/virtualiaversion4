﻿Option Strict On
Option Explicit On
Option Compare Text
Imports VI = Virtualia.Systeme.Constantes
Partial Class Controles_ListeDatabases
    Inherits System.Web.UI.UserControl
    Private WebFct As Virtualia.Net.WebFonctions
    '
    Public Delegate Sub ListeSgbd_ClickEventHandler(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.ListeSgbdEventArgs)
    Public Event ListeSgbd_Click As ListeSgbd_ClickEventHandler
    '
    Private WsSiVisuEtendu As Boolean = False

    Protected Overridable Sub V_OnClick(ByVal e As Virtualia.Systeme.Evenements.ListeSgbdEventArgs)
        RaiseEvent ListeSgbd_Click(Me, e)
    End Sub

    Public Property SiIntituleComplet() As Boolean
        Get
            Return WsSiVisuEtendu
        End Get
        Set(ByVal value As Boolean)
            WsSiVisuEtendu = value
        End Set
    End Property

    Public Property ListeHeight() As System.Web.UI.WebControls.Unit
        Get
            Return TreeDatabases.Height
        End Get
        Set(ByVal value As System.Web.UI.WebControls.Unit)
            TreeDatabases.Height = value
        End Set
    End Property

    Public Property ListeWidth() As System.Web.UI.WebControls.Unit
        Get
            Return TreeDatabases.Width
        End Get
        Set(ByVal value As System.Web.UI.WebControls.Unit)
            TreeDatabases.Width = value
        End Set
    End Property

    Protected Sub TreeDatabases_SelectedNodeChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles TreeDatabases.SelectedNodeChanged
        If CType(sender, System.Web.UI.WebControls.TreeView).SelectedNode.Depth = 0 Then
            Exit Sub
        End If

        Dim IndiceI As Integer
        Dim Cretour As Boolean
        Dim Tableaudata(0) As String
        Dim SelNoBd As Short
        Dim SelNom As String = ""
        Dim SelIntitule As String = ""
        Dim SelFormatDates As Short = VI.FormatDateSgbdr.DateStandard
        Dim SelSchema As String = ""
        Dim SelCarDecimal As String = VI.Virgule
        Dim SelTypeSgbd As Short = VI.TypeSgbdrNumeric.SgbdrSqlServer
        Dim SelTypeSgbdAlpha As String = VI.XmlSgbdrSqlServer
        Dim SelDsn As String = ""
        Dim SelServeur As String = ""
        Dim SelUti As String = ""
        Dim SelPw As String = ""
        Dim SelStatus As String = ""
        Dim Evenement As Virtualia.Systeme.Evenements.ListeSgbdEventArgs
        If WebFct Is Nothing Then
            WebFct = New Virtualia.Net.WebFonctions(Me, 0)
        End If
        Dim PointeurUti As Virtualia.Net.Session.ObjetSession = WebFct.PointeurUtilisateur
        If PointeurUti Is Nothing Then
            Exit Sub
        End If

        Tableaudata = Strings.Split(CType(sender, System.Web.UI.WebControls.TreeView).SelectedNode.Text, VI.Tiret, -1)
        SelNoBd = Convert.ToInt16(Tableaudata(0))
        Cretour = PointeurUti.ChangerBaseCourante(SelNoBd)
        Select Case Cretour
            Case False
                SelStatus = "Impossible de se connecter à la base de données"
                Evenement = New Virtualia.Systeme.Evenements.ListeSgbdEventArgs(SelNoBd, SelNom, SelIntitule, SelFormatDates, SelSchema, SelCarDecimal, SelTypeSgbd, SelTypeSgbdAlpha, SelDsn, SelServeur, SelUti, SelPw, SelStatus)
                V_OnClick(Evenement)
                Exit Sub
        End Select
        SelStatus = "Connexion réussie à la base de données"
        Dim AppObjetglobal As Virtualia.Net.Session.ObjetGlobal = WebFct.PointeurGlobal
        For IndiceI = 0 To AppObjetglobal.VirSgbd.NombredeDatabases - 1
            Select Case AppObjetglobal.VirSgbd.Item(IndiceI).Numero
                Case Is = PointeurUti.V_NumeroSgbdActif
                    SelNom = AppObjetglobal.VirSgbd.Item(IndiceI).Nom
                    SelIntitule = AppObjetglobal.VirSgbd.Item(IndiceI).Intitule
                    SelSchema = AppObjetglobal.VirSgbd.Item(IndiceI).Schema
                    SelFormatDates = AppObjetglobal.VirSgbd.Item(IndiceI).FormatDesDates
                    SelCarDecimal = AppObjetglobal.VirSgbd.Item(IndiceI).SymboleDecimal
                    SelTypeSgbd = AppObjetglobal.VirSgbd.Item(IndiceI).TypeduSgbd
                    SelTypeSgbdAlpha = AppObjetglobal.VirSgbd.Item(IndiceI).TypeduSgbdAlpha
                    SelDsn = AppObjetglobal.VirSgbd.Item(IndiceI).Item(0).DsnODBC
                    SelServeur = AppObjetglobal.VirSgbd.Item(IndiceI).Item(0).Serveur
                    SelUti = AppObjetglobal.VirSgbd.Item(IndiceI).Item(0).Utilisateur
                    SelPw = AppObjetglobal.VirSgbd.Item(IndiceI).Item(0).Password
                    Evenement = New Virtualia.Systeme.Evenements.ListeSgbdEventArgs(SelNoBd, SelNom, SelIntitule, SelFormatDates, SelSchema, SelCarDecimal, SelTypeSgbd, SelTypeSgbdAlpha, SelDsn, SelServeur, SelUti, SelPw, SelStatus)
                    V_OnClick(Evenement)
                    Exit For
            End Select
        Next IndiceI
    End Sub

    Private Sub FaireListe()
        If TreeDatabases.Nodes.Item(0).ChildNodes.Count > 0 Then
            Exit Sub
        End If
        Dim IndiceA As Integer
        Dim ChaineLibel As String
        Dim NoBd As Integer = 0
        Dim AppObjetglobal As Virtualia.Net.Session.ObjetGlobal

        If WebFct Is Nothing Then
            WebFct = New Virtualia.Net.WebFonctions(Me, 0)
        End If
        AppObjetglobal = WebFct.PointeurGlobal

        TreeDatabases.MaxDataBindDepth = 2
        TreeDatabases.ImageSet = System.Web.UI.WebControls.TreeViewImageSet.XPFileExplorer
        TreeDatabases.Nodes.Item(0).ChildNodes.Clear()

        For IndiceA = 0 To AppObjetglobal.VirSgbd.NombredeDatabases - 1
            'Select Case AppObjetglobal.VirSiBdAutorisee(AppObjetglobal.VirSgbd.Item(IndiceA).Numero, WebFct.PointeurUtilisateur.Nom, "")
            'Case True
            NoBd = AppObjetglobal.VirSgbd.Item(IndiceA).Numero
            If WsSiVisuEtendu = True Then
                ChaineLibel = NoBd & " - " & AppObjetglobal.VirSgbd.Item(IndiceA).Intitule
            Else
                ChaineLibel = NoBd & " - " & AppObjetglobal.VirSgbd.Item(IndiceA).Nom
            End If
            Dim NewNoeud As TreeNode = New TreeNode(ChaineLibel, NoBd.ToString)
            NewNoeud.ImageUrl = AppObjetglobal.VirUrlImageSgbd(AppObjetglobal.VirSgbd.Item(IndiceA).TypeduSgbd)
            NewNoeud.PopulateOnDemand = False
            NewNoeud.SelectAction = TreeNodeSelectAction.Select
            NewNoeud.ToolTip = NoBd & " - " & AppObjetglobal.VirSgbd.Item(IndiceA).Nom & " - " & AppObjetglobal.VirSgbd.Item(IndiceA).Intitule
            TreeDatabases.Nodes.Item(0).ChildNodes.Add(NewNoeud)
            'End Select
        Next IndiceA

        TreeDatabases.Nodes.Item(0).ExpandAll()
        For IndiceA = 0 To TreeDatabases.Nodes.Item(0).ChildNodes.Count - 1
            If CShort(TreeDatabases.Nodes.Item(0).ChildNodes.Item(IndiceA).Value) = WebFct.PointeurUtilisateur.InstanceBd.Numero Then
                TreeDatabases.Nodes.Item(0).ChildNodes.Item(IndiceA).Selected = True
                Exit For
            End If
        Next IndiceA
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Call FaireListe()
    End Sub

End Class
