﻿<%@ Control Language="VB" AutoEventWireup="false" Inherits="Virtualia.Net.Controles_ScriptDictionnaire" Codebehind="ScriptDictionnaire.ascx.vb" %>

<%@ Register src="../Saisies/VCoupleEtiDonnee.ascx" tagname="VCoupleEtiDonnee" tagprefix="Virtualia" %>
<%@ Register src="../Saisies/VDuoEtiquetteCommande.ascx" tagname="VDuoEtiquetteCommande" tagprefix="Virtualia" %>

<asp:Table ID="CadreScripts" runat="server" Width="490px" CellPadding="0" CellSpacing="0">
    <asp:TableRow>
        <asp:TableCell HorizontalAlign="Justify" VerticalAlign="Top">
            <asp:Panel ID="PanelCmdScript" runat="server" Height="25px" Width="477px" BackColor="#225C59"
                style="text-align: center;">
                <asp:Label ID="Eti01" runat="server" BackColor="Transparent" ForeColor="White"
                    Text="Script" Width="60px" Font-Italic="true">
                </asp:Label>
                <asp:ImageButton ID="Cmd01" runat="server" BorderStyle="None" 
                    ImageAlign="Middle" ImageUrl="~/Images/Menus/NouveauDocument_Outil.bmp" 
                    ToolTip="Nouveau script" /> 
                <asp:ImageButton ID="Cmd02" runat="server" BorderStyle="None" 
                    ImageAlign="Middle" ImageUrl="~/Images/Menus/Modifier_Outil.bmp" 
                    ToolTip="Modifier le script" /> 
                <asp:ImageButton ID="Cmd03" runat="server" BorderStyle="None" 
                    ImageAlign="Middle" ImageUrl="~/Images/Menus/Gomme_Outil.bmp" 
                    ToolTip="Effacer la valeur sélectionnée" />
                <asp:ImageButton ID="Cmd04" runat="server" BorderStyle="None" 
                    ImageAlign="Middle" ImageUrl="~/Images/Menus/Sql_Outil.bmp" 
                    ToolTip="Ajouter directement une requête SQL" /> 
                <asp:ImageButton ID="Cmd60" runat="server" BorderStyle="None" 
                    ImageAlign="Middle" ImageUrl="~/Images/Menus/Inserer_Outil.bmp" 
                    ToolTip="Insérer une colonne avant la colonne sélectionnée" /> 
                <asp:ImageButton ID="Cmd61" runat="server" BorderStyle="None" 
                    ImageAlign="Middle" ImageUrl="~/Images/Menus/Constante_Outil.bmp" 
                    ToolTip="Ajouter une colonne avec une valeur constante" /> 
                <asp:ImageButton ID="Cmd62" runat="server" BorderStyle="None" 
                    ImageAlign="Middle" ImageUrl="~/Images/Menus/Filtre_Outil.bmp" 
                    ToolTip="Ajouter un filtre de données sur la colonne sélectionnée" /> 
                <asp:Label ID="EtiExec" runat="server" BackColor="Transparent" ForeColor="White"
                                Text="Exécution" Width="70px" Font-Italic="true">
                </asp:Label>
                <asp:ImageButton ID="Cmd05" runat="server" BorderStyle="None" 
                        ImageAlign="Middle" ImageUrl="~/Images/Menus/Executer_Outil.bmp" 
                        ToolTip="Exécuter la recherche multi-critères" />
            </asp:Panel>
            <asp:UpdateProgress ID="UpdateAttente" runat="server">
                    <ProgressTemplate>
                        <img alt="" src="../Images/General/Attente.gif" />
                    </ProgressTemplate>
            </asp:UpdateProgress>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell HorizontalAlign="Justify">
            <asp:Panel ID="PanelDatesScript" runat="server" Height="25px" Width="477px" BackColor="Transparent"
                style="text-align: center;">
                <asp:Label ID="EtiDateDebut" runat="server" BackColor="Transparent" ForeColor="#124545"
                        Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Small" Font-Italic="true"
                        Text="du" Width="20px">
                </asp:Label>
                <asp:TextBox ID="ExeDateDebut" runat="server" Height="18px" Width="85px" 
                    BackColor="White" ForeColor="#142425" BorderColor="Snow" AutoPostBack="true"
                    Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Small" Font-Italic="false"
                    style="text-align:center">
                </asp:TextBox>
                <asp:Label ID="EtiDateFin" runat="server" BackColor="Transparent" ForeColor="#124545"
                        Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Small" Font-Italic="true"
                        Text="au" Width="20px">
                </asp:Label>
                <asp:TextBox ID="ExeDateFin" runat="server" Height="18px" Width="85px" 
                    BackColor="White" ForeColor="#142425" BorderColor="Snow" AutoPostBack="true"
                    Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Small" Font-Italic="false"
                    style="text-align:center">
                </asp:TextBox>
                <asp:RadioButton ID="OptionET" runat="server" GroupName="EtOu" BackColor="Transparent" 
                    ForeColor="#124545" Text="ET" Height="20px" Width="40px" Checked="true"
                    Font-Bold="true" Font-Names="Trebuchet MS" Font-Size="Small" Font-Italic="true" AutoPostBack="True">
                </asp:RadioButton>
                <asp:RadioButton ID="OptionOU" runat="server" GroupName="EtOu" BackColor="Transparent"
                    ForeColor="#124545" Text="OU" Height="20px" Width="40px"
                    Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Small" Font-Italic="true" AutoPostBack="True">
                </asp:RadioButton>
            </asp:Panel>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell HorizontalAlign="Left">
            <asp:CheckBox ID="CocheHisto" runat="server" BackColor="Transparent"
                    ForeColor="#124545" Text="Faire un historique de situations" Height="20px" Width="350px"
                    Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Small" Font-Italic="true" AutoPostBack="True">
            </asp:CheckBox>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell Width="490px" HorizontalAlign="Justify">
            <asp:Panel ID="PanelLstScript" runat="server" Width="490px" Height="470px" BorderStyle="Outset"
                                BorderWidth="2px" BorderColor="#CAEBE4" BackColor="#E2F5F1" ScrollBars="Vertical" 
                                style="display: table-cell;" >
                <asp:TreeView ID="TreeScript" runat="server" BorderStyle="None" NodeIndent="15"
                     Font-Italic="False" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                     ForeColor="#124545" MaxDataBindDepth="2" Height="460px" Width="490px" ImageSet="XPFileExplorer"
                     RootNodeStyle-ImageUrl="~/Images/Menus/FlecheDroite.bmp" 
                     style="text-align:left; vertical-align:top; margin-top: 2px; margin-bottom: 2px;" >
                     <SelectedNodeStyle BackColor="#6C9690" BorderColor="Snow" BorderStyle="Solid" 
                         Forecolor="White" Font-Overline="true" />
               </asp:TreeView>
            </asp:Panel> 
        </asp:TableCell>
    </asp:TableRow>
</asp:Table>
<asp:Table ID="CadreAlbums" runat="server" Width="490px" CellPadding="0" CellSpacing="0">
    <asp:TableRow Height="20px"></asp:TableRow>
    <asp:TableRow>
        <asp:TableCell HorizontalAlign="Justify" >
            <asp:Panel ID="PanelCmdAlbum" runat="server" Height="25px" Width="477px" BackColor="#225C59" 
                        style="text-align: center">
                <asp:Label ID="EtiAlbum" runat="server" BackColor="Transparent" ForeColor="White"
                    Text="Album" Width="60px" Font-Italic="true">
                </asp:Label>   
                <asp:ImageButton ID="Cmd11" runat="server" BorderStyle="None" 
                    ImageAlign="Middle" ImageUrl="~/Images/Menus/Enregistrer_Outil.bmp" 
                    ToolTip="Enregistrer le script dans l'album" />      
                <asp:ImageButton ID="Cmd12" runat="server" BorderStyle="None" 
                    ImageAlign="Middle" ImageUrl="~/Images/Menus/Supprimer_Outil.bmp" 
                    ToolTip="Supprimer le script de l'album" />
            </asp:Panel>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell Width="460px" HorizontalAlign="Left">
            <Virtualia:VCoupleEtiDonnee ID="IntituleScript" runat="server"
               V_SiDonneeDico="false" EtiWidth="120px" DonWidth="335px" EtiBackColor="#82BEA1"
               EtiText="Intitulé du script" DonTabIndex="1" />
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell Width="460px" HorizontalAlign="Left"> 
                <Virtualia:VDuoEtiquetteCommande ID="CategorieRH" runat="server" EtiText="Catégorie RH"
                    V_SiDonneeDico="false" EtiWidth="170px" DonWidth="290px" DonTabIndex="2"
                    EtiBackColor="#E2F5F1" V_PointdeVue="99" V_NomTable="$CatégorieRH" /> 
            </asp:TableCell>
        </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell Width="490px" HorizontalAlign="Justify">
            <asp:Panel ID="PanelLstAlbum" runat="server" Width="490px" Height="540px" BorderStyle="Outset"
                                BorderWidth="2px" BorderColor="#CAEBE4" BackColor="White" ScrollBars="Vertical" 
                                style="display: table-cell;" >
                <asp:TreeView ID="TreeAlbum" runat="server" BorderStyle="None" NodeIndent="7"
                     Font-Italic="False" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                     ForeColor="#124545" MaxDataBindDepth="2" Height="530px" Width="480px" ImageSet="XPFileExplorer"
                     RootNodeStyle-ImageUrl="~\Images\General\CarreUniversel.bmp"
                     style="text-align:left; vertical-align:top; margin-top: 2px; margin-bottom: 2px"
                     ParentNodeStyle-HorizontalPadding="10" ParentNodeStyle-BackColor="#E2F5F1" 
                     ParentNodeStyle-BorderStyle="NotSet" ParentNodeStyle-Font-Bold="true" >
                     <SelectedNodeStyle BackColor="#6C9690" BorderColor="Snow" BorderStyle="Solid" 
                         Forecolor="White" Font-Overline="true" />
                     <Nodes>
                         <asp:TreeNode PopulateOnDemand="true" Text="Albums" 
                             SelectAction="Expand" Value="Album"></asp:TreeNode>
                     </Nodes>
               </asp:TreeView>
            </asp:Panel>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <asp:HiddenField ID="SelIdeW" runat="server" Value="0" />
        </asp:TableCell>
        <asp:TableCell>
            <asp:HiddenField ID="NoeudW" runat="server" Value="" />
        </asp:TableCell>
    </asp:TableRow>
</asp:Table>