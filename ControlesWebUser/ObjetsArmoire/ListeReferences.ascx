﻿<%@ Control Language="VB" AutoEventWireup="false" Inherits="Virtualia.Net.Controles_ListeReferences" Codebehind="ListeReferences.ascx.vb" %>

<asp:UpdatePanel ID="UpdSysRef" runat="server">
  <ContentTemplate>
<asp:Table ID="SystemeReference" runat="server">
    <asp:TableRow>
        <asp:TableCell ID="MenuRef" runat="server" Visible="true"> 
            <div style=" margin-top:10px; width:300px; height:1195px; border: outset 2px #FFEBC8; background-color: White;
                 vertical-align:top; display: table-cell; overflow: scroll" >
                <asp:Table ID="Recherche" runat="server" Width="300px" CellPadding="1" CellSpacing="0">
                    <asp:TableRow VerticalAlign="Top" Height="20px" BackColor="#A68247">
                        <asp:TableCell Width="80px" HorizontalAlign="Left"> 
                            <asp:Label ID="EtiAllerA" runat="server" Height="18px" Width="80px" Text="Aller à"
                                    BackColor="#A68247" Visible="true" BorderColor="#FFEBC8"
                                    BorderStyle="Inset" BorderWidth="2px" ForeColor="#FFF2DB"
                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                    style="margin-top: 0px; margin-left: 1px; text-indent: 5px; text-align: center" >
                            </asp:Label>
                        </asp:TableCell> 
                        <asp:TableCell Width="140px" HorizontalAlign="Left"> 
                            <asp:TextBox ID="VAllerA" runat="server" BackColor="Snow" 
                                BorderColor="#FFEBC8" BorderStyle="Inset" Height="16px" Width="140px" 
                                Font-Bold="false" ForeColor="#142425"></asp:TextBox>
                        </asp:TableCell>
                        <asp:TableCell HorizontalAlign="Left"> 
                            <asp:ImageButton ID="CmdAllerA" runat="server" BorderStyle="None" Height="20px"
                               ImageAlign="Middle" ImageUrl="~/Images/Menus/Executer_SysRef1.bmp"
                               ToolTip="Accés direct à une préoccupation"
                               style="margin-bottom: 2px" />
                        </asp:TableCell>
                   </asp:TableRow>     
               </asp:Table>
                 <asp:TreeView ID="TreeDicoRef" runat="server" BorderStyle="None" NodeIndent="15"
                     Font-Italic="False" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                     ForeColor="#121245" MaxDataBindDepth="2" Height="600px" Width="290px" ImageSet="BulletedList4"
                     RootNodeStyle-ImageUrl="~/Images/Armoire/MarronFermer16.bmp"
                     style=" vertical-align:top; margin-top: 10px; margin-bottom: 10px;"
                      RootNodeStyle-BorderStyle="Ridge" RootNodeStyle-BackColor="#EDC477" LeafNodeStyle-HorizontalPadding="5" ParentNodeStyle-HorizontalPadding="5">
                     <SelectedNodeStyle BackColor="#6C9690" BorderColor="Snow" BorderStyle="Solid" 
                         Forecolor="White" Font-Overline="true" />
                     <Nodes>
                         <asp:TreeNode PopulateOnDemand="true" Text="Règlementaire général" 
                             SelectAction="Expand" Value="General"></asp:TreeNode>
                         <asp:TreeNode PopulateOnDemand="true" Text="Emplois et statuts" 
                             SelectAction="Expand" Value="Statutaire"></asp:TreeNode>
                          <asp:TreeNode PopulateOnDemand="true" Text="Temps de travail" 
                             SelectAction="Expand" Value="Travail"></asp:TreeNode>
                          <asp:TreeNode PopulateOnDemand="true" Text="Organisation" 
                             SelectAction="Expand" Value="Organisation"></asp:TreeNode>
                          <asp:TreeNode PopulateOnDemand="true" Text="Plan de formation" 
                             SelectAction="Expand" Value="Formation"></asp:TreeNode>
                          <asp:TreeNode PopulateOnDemand="true" Text="Rémunération" 
                             SelectAction="Expand" Value="Paie"></asp:TreeNode>
                     </Nodes>
                 </asp:TreeView>
            </div>
        </asp:TableCell>
        <asp:TableCell ID="CelluleDroite" BackColor="#BFA36D" BorderColor="#BFB197" BorderStyle="Ridge" BorderWidth="4px" 
                            Font-Names="Trebuchet MS" Font-Italic="true" Font-Size="Small" HorizontalAlign="Center">
           <div style=" margin-left:5px; width:770px; height:1200px;" >
               <asp:Panel ID="PanelTitre" runat="server" Width="770px">
                  <asp:Label ID="EtiTitre" runat="server" Height="20px" Width="300px"
                        BackColor="#61450B" Visible="true" BorderColor="#FFEBC8"
                        BorderStyle="Inset" BorderWidth="2px" ForeColor="#FFF2DB"
                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Text="Référentiel"
                        style="margin-top: 10px; font-style: oblique; text-align: center">
                  </asp:Label>
                  <asp:Table ID="CadreSituation" runat="server" CellPadding="10" CellSpacing="0" 
                             Width="640px">
                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Center"> 
                                <asp:Label ID="EtiPrincipal" runat="server" Height="20px" Width="300px"
                                    BackColor="#A68247" Visible="true" BorderColor="#FFEBC8"
                                    BorderStyle="Inset" BorderWidth="2px" ForeColor="#FFF2DB"
                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                    Text=""
                                    style="margin-top: 2px; margin-left: 30px; font-style: oblique; text-align: center">
                                </asp:Label>
                            </asp:TableCell> 
                            <asp:TableCell HorizontalAlign="Center"> 
                                <asp:Label ID="EtiSecondaire" runat="server" Height="20px" Width="300px"
                                    BackColor="#A68247" Visible="true" BorderColor="#FFEBC8" 
                                    BorderStyle="Inset" BorderWidth="2px" ForeColor="#FFF2DB"
                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                    Text=""
                                    style="margin-top: 2px; margin-left: 1px; font-style: oblique; text-align: center">
                                </asp:Label>
                            </asp:TableCell> 
                        </asp:TableRow>
                  </asp:Table>
              </asp:Panel>
              <asp:Table ID="CadreRecherche" runat="server" CellPadding="0" CellSpacing="0" 
                         Width="650px" HorizontalAlign="Center">
                    <asp:TableRow>
                        <asp:TableCell HorizontalAlign="Right"> 
                            <asp:Label ID="EtiRecherche" runat="server" Height="20px" Width="80px" Text="Aller à"
                                BackColor="#A68247"  BorderColor="#FFEBC8"  BorderStyle="Outset"
                                BorderWidth="2px" ForeColor="#FFF2DB" Font-Italic="true"
                                Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                style="margin-top: 0px; margin-left: 120px; text-indent: 5px; text-align: center" >
                            </asp:Label>
                        </asp:TableCell>
                        <asp:TableCell Width="250px"> 
                            <asp:TextBox ID="VRecherche" runat="server" BackColor="#BFB8AB" 
                                BorderColor="#FFEBC8" BorderStyle="Inset" BorderWidth="1px" Height="16px" Width="290px" 
                                Font-Bold="false" ForeColor="#142425"></asp:TextBox>
                        </asp:TableCell>
                        <asp:TableCell HorizontalAlign="Left"> 
                            <asp:ImageButton ID="CmdRecherche" runat="server" BorderStyle="None"
                               ImageAlign="Middle" ImageUrl="~/Images/Menus/Executer_SysRef2.bmp"
                               ToolTip="Recherche d'une valeur"
                               style="margin-left:2px; margin-bottom: 2px" />
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell Height="2px"></asp:TableCell>
                    </asp:TableRow>
              </asp:Table>
              <asp:Table ID="CadreLettre" runat="server" Height="22px" Width="624px" 
                           CellPadding="0" CellSpacing="0" HorizontalAlign="Center" >
                    <asp:TableRow>
                        <asp:TableCell>
                            <asp:Button ID="ButtonA" runat="server" BorderStyle="Inset" 
                             Text="A" Height="22px" Width="24px" BackColor="#BFB8AB" ForeColor="Gray"
                             Font-Names="Trebuchet MS" Font-Bold="True" Font-Size="Small"
                             style="text-align: center; vertical-align:middle; padding-left: 2px;"/>
                         </asp:TableCell>
                        <asp:TableCell >
                            <asp:Button ID="ButtonB" runat="server" BorderStyle="Inset"  
                            Text="B" Height="22px" Width="24px" BackColor="#BFB8AB" ForeColor="Gray"
                            Font-Names="Trebuchet MS" Font-Bold="True" Font-Size="Small"
                            style="text-align: center; vertical-align:middle; padding-left: 2px;"/>
                        </asp:TableCell>
                        <asp:TableCell >     
                            <asp:Button ID="ButtonC" runat="server" BorderStyle="Inset"  
                            Text="C" Height="22px" Width="24px" BackColor="#BFB8AB" ForeColor="Gray"
                            Font-Names="Trebuchet MS" Font-Bold="True" Font-Size="Small"
                            style="text-align: center; vertical-align:middle; padding-left: 2px;"/>
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:Button ID="ButtonD" runat="server" BorderStyle="Inset" 
                             Text="D" Height="22px" Width="24px" BackColor="#BFB8AB" ForeColor="Gray"
                             Font-Names="Trebuchet MS" Font-Bold="True" Font-Size="Small"
                             style="text-align: center; vertical-align:middle; padding-left: 2px;"/>
                         </asp:TableCell>
                        <asp:TableCell >
                            <asp:Button ID="ButtonE" runat="server" BorderStyle="Inset"  
                            Text="E" Height="22px" Width="24px" BackColor="#BFB8AB" ForeColor="Gray"
                            Font-Names="Trebuchet MS" Font-Bold="True" Font-Size="Small"
                            style="text-align: center; vertical-align:middle; padding-left: 2px;"/>
                        </asp:TableCell>
                        <asp:TableCell >     
                            <asp:Button ID="ButtonF" runat="server" BorderStyle="Inset"  
                            Text="F" Height="22px" Width="24px" BackColor="#BFB8AB" ForeColor="Gray"
                            Font-Names="Trebuchet MS" Font-Bold="True" Font-Size="Small"
                            style="text-align: center; vertical-align:middle; padding-left: 2px;"/>
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:Button ID="ButtonG" runat="server" BorderStyle="Inset" 
                             Text="G" Height="22px" Width="24px" BackColor="#BFB8AB" ForeColor="Gray"
                             Font-Names="Trebuchet MS" Font-Bold="True" Font-Size="Small"
                             style="text-align: center; vertical-align:middle; padding-left: 2px;"/>
                         </asp:TableCell>
                        <asp:TableCell >
                            <asp:Button ID="ButtonH" runat="server" BorderStyle="Inset"  
                            Text="H" Height="22px" Width="24px" BackColor="#BFB8AB" ForeColor="Gray"
                            Font-Names="Trebuchet MS" Font-Bold="True" Font-Size="Small"
                            style="text-align: center; vertical-align:middle; padding-left: 2px;"/>
                        </asp:TableCell>
                        <asp:TableCell >     
                            <asp:Button ID="ButtonI" runat="server" BorderStyle="Inset"  
                            Text="I" Height="22px" Width="24px" BackColor="#BFB8AB" ForeColor="Gray"
                            Font-Names="Trebuchet MS" Font-Bold="True" Font-Size="Small"
                            style="text-align: center; vertical-align:middle; padding-left: 2px;"/>
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:Button ID="ButtonJ" runat="server" BorderStyle="Inset" 
                             Text="J" Height="22px" Width="24px" BackColor="#BFB8AB" ForeColor="Gray"
                             Font-Names="Trebuchet MS" Font-Bold="True" Font-Size="Small"
                             style="text-align: center; vertical-align:middle; padding-left: 2px;"/>
                         </asp:TableCell>
                        <asp:TableCell >
                            <asp:Button ID="ButtonK" runat="server" BorderStyle="Inset"  
                            Text="K" Height="22px" Width="24px" BackColor="#BFB8AB" ForeColor="Gray"
                            Font-Names="Trebuchet MS" Font-Bold="True" Font-Size="Small"
                            style="text-align: center; vertical-align:middle; padding-left: 2px;"/>
                        </asp:TableCell>
                        <asp:TableCell >     
                            <asp:Button ID="ButtonL" runat="server" BorderStyle="Inset"  
                            Text="L" Height="22px" Width="24px" BackColor="#BFB8AB" ForeColor="Gray"
                            Font-Names="Trebuchet MS" Font-Bold="True" Font-Size="Small"
                            style="text-align: center; vertical-align:middle; padding-left: 2px;"/>
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:Button ID="ButtonM" runat="server" BorderStyle="Inset" 
                             Text="M" Height="22px" Width="24px" BackColor="#BFB8AB" ForeColor="Gray"
                             Font-Names="Trebuchet MS" Font-Bold="True" Font-Size="Small"
                             style="text-align: center; vertical-align:middle; padding-left: 2px;"/>
                         </asp:TableCell>
                        <asp:TableCell >
                            <asp:Button ID="ButtonN" runat="server" BorderStyle="Inset"  
                            Text="N" Height="22px" Width="24px" BackColor="#BFB8AB" ForeColor="Gray"
                            Font-Names="Trebuchet MS" Font-Bold="True" Font-Size="Small"
                            style="text-align: center; vertical-align:middle; padding-left: 2px;"/>
                        </asp:TableCell>
                        <asp:TableCell >     
                            <asp:Button ID="ButtonO" runat="server" BorderStyle="Inset"  
                            Text="O" Height="22px" Width="24px" BackColor="#BFB8AB" ForeColor="Gray"
                            Font-Names="Trebuchet MS" Font-Bold="True" Font-Size="Small"
                            style="text-align: center; vertical-align:middle; padding-left: 2px;"/>
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:Button ID="ButtonP" runat="server" BorderStyle="Inset" 
                             Text="P" Height="22px" Width="24px" BackColor="#BFB8AB" ForeColor="Gray"
                             Font-Names="Trebuchet MS" Font-Bold="True" Font-Size="Small"
                             style="text-align: center; vertical-align:middle; padding-left: 2px;"/>
                         </asp:TableCell>
                        <asp:TableCell >
                            <asp:Button ID="ButtonQ" runat="server" BorderStyle="Inset"  
                            Text="Q" Height="22px" Width="24px" BackColor="#BFB8AB" ForeColor="Gray"
                            Font-Names="Trebuchet MS" Font-Bold="True" Font-Size="Small"
                            style="text-align: center; vertical-align:middle; padding-left: 2px;"/>
                        </asp:TableCell>
                        <asp:TableCell >     
                            <asp:Button ID="ButtonR" runat="server" BorderStyle="Inset"  
                            Text="R" Height="22px" Width="24px" BackColor="#BFB8AB" ForeColor="Gray"
                            Font-Names="Trebuchet MS" Font-Bold="True" Font-Size="Small"
                            style="text-align: center; vertical-align:middle; padding-left: 2px;"/>
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:Button ID="ButtonS" runat="server" BorderStyle="Inset" 
                             Text="S" Height="22px" Width="24px" BackColor="#BFB8AB" ForeColor="Gray"
                             Font-Names="Trebuchet MS" Font-Bold="True" Font-Size="Small"
                             style="text-align: center; vertical-align:middle; padding-left: 2px;"/>
                         </asp:TableCell>
                        <asp:TableCell >
                            <asp:Button ID="ButtonT" runat="server" BorderStyle="Inset"  
                            Text="T" Height="22px" Width="24px" BackColor="#BFB8AB" ForeColor="Gray"
                            Font-Names="Trebuchet MS" Font-Bold="True" Font-Size="Small"
                            style="text-align: center; vertical-align:middle; padding-left: 2px;"/>
                        </asp:TableCell>
                        <asp:TableCell >     
                            <asp:Button ID="ButtonU" runat="server" BorderStyle="Inset"  
                            Text="U" Height="22px" Width="24px" BackColor="#BFB8AB" ForeColor="Gray"
                            Font-Names="Trebuchet MS" Font-Bold="True" Font-Size="Small"
                            style="text-align: center; vertical-align:middle; padding-left: 2px;"/>
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:Button ID="ButtonV" runat="server" BorderStyle="Inset" 
                             Text="V" Height="22px" Width="24px" BackColor="#BFB8AB" ForeColor="Gray"
                             Font-Names="Trebuchet MS" Font-Bold="True" Font-Size="Small"
                             style="text-align: center; vertical-align:middle; padding-left: 2px;"/>
                         </asp:TableCell>
                        <asp:TableCell >
                            <asp:Button ID="ButtonW" runat="server" BorderStyle="Inset"  
                            Text="W" Height="22px" Width="24px" BackColor="#BFB8AB" ForeColor="Gray"
                            Font-Names="Trebuchet MS" Font-Bold="True" Font-Size="Small"
                            style="text-align: center; vertical-align:middle; padding-left: 2px;"/>
                        </asp:TableCell>
                        <asp:TableCell >     
                            <asp:Button ID="ButtonX" runat="server" BorderStyle="Inset"  
                            Text="X" Height="22px" Width="24px" BackColor="#BFB8AB" ForeColor="Gray"
                            Font-Names="Trebuchet MS" Font-Bold="True" Font-Size="Small"
                            style="text-align: center; vertical-align:middle; padding-left: 2px;"/>
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:Button ID="ButtonY" runat="server" BorderStyle="Inset" 
                             Text="Y" Height="22px" Width="24px" BackColor="#BFB8AB" ForeColor="Gray"
                             Font-Names="Trebuchet MS" Font-Bold="True" Font-Size="Small"
                             style="text-align: center; vertical-align:middle; padding-left: 2px;"/>
                         </asp:TableCell>
                        <asp:TableCell >
                            <asp:Button ID="ButtonZ" runat="server" BorderStyle="Inset"  
                            Text="Z" Height="22px" Width="24px" BackColor="#BFB8AB" ForeColor="Gray"
                            Font-Names="Trebuchet MS" Font-Bold="True" Font-Size="Small"
                            style="text-align: center; vertical-align:middle; padding-left: 2px;"/>
                        </asp:TableCell>
                        <asp:TableCell >     
                            <asp:Button ID="ButtonTout" runat="server" BorderStyle="Inset"  
                            Text="@" Height="22px" Width="24px" BackColor="#BFB8AB" ForeColor="Gray"
                            Font-Names="Trebuchet MS" Font-Bold="True" Font-Size="Small"
                            style="text-align: center; vertical-align:middle; padding-left: 2px;"/>
                        </asp:TableCell>
                    </asp:TableRow>
              </asp:Table>
              <asp:Table ID="CadreControle" runat="server" HorizontalAlign="Center"
                         style="margin-top: 5px; width: 660px; height:1000px; 
                         background-attachment: inherit; display: table-cell;">
                    <asp:TableRow>
                        <asp:TableCell HorizontalAlign="Center" Width="480px">
                            <asp:Label ID="EtiStatus1" runat="server" Height="20px" Width="200px"
                                BackColor="#FFEBC8" Visible="true" BorderColor="#FFEBC8"  
                                BorderStyle="Inset" BorderWidth="2px" ForeColor="#124545"
                                Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                style="font-style: oblique; text-align: center">
                            </asp:Label>
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:Button ID="CommandeBlanc" runat="server" Text="Remettre à blanc" Width="120px" Height="24px"
                                 BackColor="#7D9F99" BorderColor="#FFEBC8" ForeColor="#FFF2DB" Visible="false"
                                 Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Font-Italic="true"
                                 BorderStyle="Outset" style=" margin-left: 8px; text-align: center;"
                                 Tooltip="Remettre à blanc la donnée sélectionnée" >
                             </asp:Button>
                         </asp:TableCell>
                         <asp:TableCell>
                             <asp:Button ID="CommandeRetour" runat="server" Text="Retour" Width="70px" Height="24px"
                                 BackColor="#7D9F99" BorderColor="#FFEBC8" ForeColor="#FFF2DB" Visible="false"
                                 Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Font-Italic="true"
                                 BorderStyle="Outset" style=" margin-left: 8px; text-align: center;"
                                 Tooltip="Revenir au menu initial" >
                             </asp:Button>
                        </asp:TableCell>
                   </asp:TableRow>
                   <asp:TableRow>
                        <asp:TableCell ColumnSpan="3">
                            <asp:Table ID="CadreRadio" runat="server" HorizontalAlign ="Center">
                                <asp:TableRow Height="25px">
                                    <asp:TableCell>
                                       <asp:RadioButton ID="RadioV0" runat="server" Text="Liste alphabétique" Visible="true"
                                          AutoPostBack="true" GroupName="OptionV" ForeColor="#FFF2DB" Height="20px" Width="200px"
                                          BackColor="#7D9F99" BorderColor="#FFEBC8" BorderStyle="inset" BorderWidth="2px"
                                          Font-Bold="true" Font-Names="Trebuchet MS" Font-Size="Small" Checked="true"
                                          style="margin-top: 0px; font-style: oblique;
                                          vertical-align: middle; text-indent: 5px; text-align: left" />
                                    </asp:TableCell>
                                    <asp:TableCell>
                                      <asp:RadioButton ID="RadioV1" runat="server" Text="Liste organisée" 
                                          AutoPostBack="true" GroupName="OptionV" ForeColor="#FFF2DB" Height="20px" Width="200px"
                                          BackColor="#7D9F99" BorderColor="#FFEBC8" BorderStyle="inset" BorderWidth="2px"
                                          Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Small"
                                          style="margin-top: 0px; margin-left: 1px; font-style: oblique;
                                          vertical-align: middle; text-indent: 5px; text-align: left" />
                                    </asp:TableCell>
                                    <asp:TableCell>
                                      <asp:RadioButton ID="RadioV2" runat="server" Text="Liste par référence" 
                                          AutoPostBack="true" GroupName="OptionV" ForeColor="#FFF2DB" Height="20px" Width="200px"
                                          BackColor="#7D9F99" BorderColor="#FFEBC8" BorderStyle="inset" BorderWidth="2px"
                                          Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Small"
                                          style="margin-top: 0px; margin-left: 1px; font-style: oblique;
                                          vertical-align: middle; text-indent: 5px; text-align: left" />
                                    </asp:TableCell>
                                </asp:TableRow>
                            </asp:Table>
                        </asp:TableCell>
                   </asp:TableRow>
                   <asp:TableRow>
                     <asp:TableCell ColumnSpan="3" Width="660px">
                        <asp:Table ID="CadreCommandes" runat="server" Height="22px" CellPadding="0" 
                             CellSpacing="0" BackImageUrl="~/Images/Boutons/NewSuppOK_Std.bmp"
                             BorderWidth="2px" BorderStyle="Outset" BorderColor="#FFEBC8" ForeColor="#FFF2DB"
                             Width="160px" HorizontalAlign="Right" style="margin-top: 3px;margin-right: 2px" Visible="true">
                            <asp:TableRow VerticalAlign="Top">
                                <asp:TableCell>
                                     <asp:Button ID="CommandeNew" runat="server" Text="Nouveau" Width="68px" Height="20px"
                                         BackColor="Transparent" BorderColor="#FFEBC8" ForeColor="#FFF2DB"
                                         Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Font-Italic="true"
                                         BorderStyle="None" style=" margin-left: 8px; text-align: center;"
                                         Tooltip="Nouvelle fiche" >
                                     </asp:Button>
                                </asp:TableCell>
                                <asp:TableCell>
                                     <asp:Button ID="CommandeSupp" runat="server" Text="Supprimer" Width="68px" Height="20px"
                                         BackColor="Transparent" BorderColor="#FFEBC8" ForeColor="#FFF2DB"
                                         Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Font-Italic="true"
                                         BorderStyle="None" style=" margin-left: 8px; text-align: center;"
                                         Tooltip="Supprimer la fiche" >
                                     </asp:Button>
                                </asp:TableCell>
                            </asp:TableRow>
                          </asp:Table>
                    </asp:TableCell>
                 </asp:TableRow>
                   <asp:TableRow>
                     <asp:TableCell ColumnSpan="3">
                       <div id="divscroll" style="width: 660px; height:950px; text-align: left; 
                                   overflow: scroll;">     
                        <asp:TreeView ID="TreeListeRef" runat="server" MaxDataBindDepth="2" BorderStyle="Inset" 
                            BorderWidth="2px" BorderColor="#CCFFCC" ForeColor="#124545" Font-Names="Trebuchet MS"
                            Width="660px" Height="930px" Font-Bold="False" NodeIndent="10" BackColor="Snow" ImageSet="XPFileExplorer"
                            Font-Size="Small" Font-Italic="true" LeafNodeStyle-HorizontalPadding="10px"
                            style="margin-bottom: 1px; margin-left: 30px; display:table-cell" >
                            <SelectedNodeStyle BackColor="#6C9690" BorderColor="#BFB8AB" 
                                BorderStyle="Solid" BorderWidth="1px" Forecolor="White" />
                            <Nodes>
                                <asp:TreeNode PopulateOnDemand="False" Text="Système de référence" 
                                    Value="SystemeReference"  ImageUrl="~/Images/Armoire/VertFonceOuvert16.bmp" ></asp:TreeNode>
                            </Nodes>
                        </asp:TreeView>
                      </div>
                    </asp:TableCell>
                 </asp:TableRow>
             </asp:Table>
           </div> 
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell>
          <asp:HiddenField ID="HSelMode" runat="server" Value="1" />
          <asp:HiddenField ID="HSelRadio" runat="server" Value="0" />
          <asp:HiddenField ID="HSelValeur" runat="server" Value="" />
          <asp:HiddenField ID="HSelDateValeur" runat="server" Value="" />
       </asp:TableCell>
    </asp:TableRow> 
 </asp:Table>
 </ContentTemplate>
</asp:UpdatePanel>