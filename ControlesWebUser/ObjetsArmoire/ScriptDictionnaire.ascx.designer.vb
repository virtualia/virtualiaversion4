﻿'------------------------------------------------------------------------------
' <généré automatiquement>
'     Ce code a été généré par un outil.
'
'     Les modifications apportées à ce fichier peuvent provoquer un comportement incorrect et seront perdues si
'     le code est régénéré.
' </généré automatiquement>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class Controles_ScriptDictionnaire

    '''<summary>
    '''Contrôle CadreScripts.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CadreScripts As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle PanelCmdScript.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PanelCmdScript As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''Contrôle Eti01.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Eti01 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle Cmd01.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Cmd01 As Global.System.Web.UI.WebControls.ImageButton

    '''<summary>
    '''Contrôle Cmd02.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Cmd02 As Global.System.Web.UI.WebControls.ImageButton

    '''<summary>
    '''Contrôle Cmd03.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Cmd03 As Global.System.Web.UI.WebControls.ImageButton

    '''<summary>
    '''Contrôle Cmd04.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Cmd04 As Global.System.Web.UI.WebControls.ImageButton

    '''<summary>
    '''Contrôle Cmd60.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Cmd60 As Global.System.Web.UI.WebControls.ImageButton

    '''<summary>
    '''Contrôle Cmd61.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Cmd61 As Global.System.Web.UI.WebControls.ImageButton

    '''<summary>
    '''Contrôle Cmd62.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Cmd62 As Global.System.Web.UI.WebControls.ImageButton

    '''<summary>
    '''Contrôle EtiExec.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiExec As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle Cmd05.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Cmd05 As Global.System.Web.UI.WebControls.ImageButton

    '''<summary>
    '''Contrôle UpdateAttente.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents UpdateAttente As Global.System.Web.UI.UpdateProgress

    '''<summary>
    '''Contrôle PanelDatesScript.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PanelDatesScript As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''Contrôle EtiDateDebut.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiDateDebut As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle ExeDateDebut.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents ExeDateDebut As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''Contrôle EtiDateFin.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiDateFin As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle ExeDateFin.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents ExeDateFin As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''Contrôle OptionET.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents OptionET As Global.System.Web.UI.WebControls.RadioButton

    '''<summary>
    '''Contrôle OptionOU.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents OptionOU As Global.System.Web.UI.WebControls.RadioButton

    '''<summary>
    '''Contrôle CocheHisto.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CocheHisto As Global.System.Web.UI.WebControls.CheckBox

    '''<summary>
    '''Contrôle PanelLstScript.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PanelLstScript As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''Contrôle TreeScript.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents TreeScript As Global.System.Web.UI.WebControls.TreeView

    '''<summary>
    '''Contrôle CadreAlbums.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CadreAlbums As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle PanelCmdAlbum.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PanelCmdAlbum As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''Contrôle EtiAlbum.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiAlbum As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle Cmd11.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Cmd11 As Global.System.Web.UI.WebControls.ImageButton

    '''<summary>
    '''Contrôle Cmd12.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Cmd12 As Global.System.Web.UI.WebControls.ImageButton

    '''<summary>
    '''Contrôle IntituleScript.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents IntituleScript As Global.Virtualia.Net.Controles_VCoupleEtiDonnee

    '''<summary>
    '''Contrôle CategorieRH.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CategorieRH As Global.Virtualia.Net.Controles_VDuoEtiquetteCommande

    '''<summary>
    '''Contrôle PanelLstAlbum.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PanelLstAlbum As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''Contrôle TreeAlbum.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents TreeAlbum As Global.System.Web.UI.WebControls.TreeView

    '''<summary>
    '''Contrôle SelIdeW.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents SelIdeW As Global.System.Web.UI.WebControls.HiddenField

    '''<summary>
    '''Contrôle NoeudW.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents NoeudW As Global.System.Web.UI.WebControls.HiddenField
End Class
