﻿<%@ Control Language="VB" AutoEventWireup="false" Inherits="Virtualia.Net.Controles_ObjetsArmoire_AlbumdesScripts" CodeBehind="AlbumdesScripts.ascx.vb" %>

<%@ Register Src="../Saisies/VCoupleEtiDonnee.ascx" TagName="VCoupleEtiDonnee" TagPrefix="Virtualia" %>

<asp:UpdatePanel ID="UpdAlbum" runat="server">
    <ContentTemplate>
        <asp:Table ID="PanelAlbum" runat="server" Width="650px" HorizontalAlign="Center" BackColor="#19968D" Height="1220px" BorderWidth="2px" BorderStyle="Groove" BorderColor="#B0E0D7">
            <asp:TableRow>
                <asp:TableCell>
                    <asp:Table ID="CadreScripts" runat="server" Width="650px" CellPadding="0" CellSpacing="0" HorizontalAlign="Center">
                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Justify" VerticalAlign="Top">
                                <asp:Panel ID="PanelCmdScript" runat="server" Height="25px" Width="650px" BackColor="#225C59" Style="text-align: center;">
                                    <asp:Label ID="Eti01" runat="server" BackColor="Transparent" ForeColor="White" Text="Choix d'une armoire" Width="300px" Font-Italic="true" />
                                    <asp:Label ID="EtiExec" runat="server" BackColor="Transparent" ForeColor="White" Text="Obtenir" Width="70px" Font-Italic="true" />
                                    <asp:ImageButton ID="Cmd01" runat="server" BorderStyle="None" ImageAlign="Middle" ImageUrl="~/Images/Menus/Executer_Outil.bmp" ToolTip="Charger la requete dans l'armoire courante des dossiers" />
                                </asp:Panel>
                                <asp:UpdateProgress ID="UpdateAttente" runat="server">
                                    <ProgressTemplate>
                                        <img alt="" src="../Images/General/Attente.gif" />
                                    </ProgressTemplate>
                                </asp:UpdateProgress>
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell Height="5px" />
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Justify">
                                <asp:Panel ID="PanelDatesScript" runat="server" Height="25px" Width="550px" BackColor="Transparent" Style="text-align: center;">
                                    <asp:Label ID="EtiDateDebut" runat="server" BackColor="Transparent" ForeColor="#142425" Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Small" Font-Italic="true" Text="du" Width="20px" />
                                    <asp:TextBox ID="ExeDateDebut" runat="server" Height="18px" Width="85px" BackColor="White" ForeColor="#142425" BorderColor="Snow" AutoPostBack="true" Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Small" Font-Italic="false" Style="text-align: center" />
                                    <asp:Label ID="EtiDateFin" runat="server" BackColor="Transparent" ForeColor="#142425" Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Small" Font-Italic="true" Text="au" Width="20px" />
                                    <asp:TextBox ID="ExeDateFin" runat="server" Height="18px" Width="85px" BackColor="White" ForeColor="#142425" BorderColor="Snow" AutoPostBack="true" Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Small" Font-Italic="false" Style="text-align: center" />
                                </asp:Panel>
                            </asp:TableCell>
                        </asp:TableRow>
                    </asp:Table>
                </asp:TableCell>
            </asp:TableRow>
            <asp:TableRow>
                <asp:TableCell>
                    <asp:Table ID="CadreAlbums" runat="server" Width="600px" CellPadding="0" CellSpacing="0" HorizontalAlign="Center">
                        <asp:TableRow Height="20px" />
                        <asp:TableRow>
                            <asp:TableCell Width="600px" HorizontalAlign="Left">
                                <asp:Panel ID="PanelLstAlbum" runat="server" Width="600px" Height="1080px" BorderStyle="Outset" BorderWidth="2px" BorderColor="#CAEBE4" BackColor="White" ScrollBars="Vertical" Style="display: table-cell;">
                                    <asp:TreeView ID="TreeAlbum" runat="server" BorderStyle="None" NodeIndent="7" Font-Italic="False" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" ForeColor="#124545" NodeStyle-HorizontalPadding="10" MaxDataBindDepth="2" Height="1080px" Width="580px" ImageSet="XPFileExplorer" Style="text-align: left; vertical-align: top; margin-top: 2px; margin-bottom: 2px;" RootNodeStyle-HorizontalPadding="10" RootNodeStyle-BackColor="#B0E0D7" RootNodeStyle-BorderStyle="Ridge">
                                        <SelectedNodeStyle BackColor="#6C9690" BorderColor="Snow" BorderStyle="Solid" ForeColor="#D7FAF3" Font-Overline="true" />
                                    </asp:TreeView>
                                </asp:Panel>
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell>
                                <asp:HiddenField ID="SelIntituleW" runat="server" Value="0" />
                            </asp:TableCell>
                        </asp:TableRow>
                    </asp:Table>
                </asp:TableCell>
            </asp:TableRow>
        </asp:Table>
    </ContentTemplate>
</asp:UpdatePanel>
