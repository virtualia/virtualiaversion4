﻿'------------------------------------------------------------------------------
' <généré automatiquement>
'     Ce code a été généré par un outil.
'
'     Les modifications apportées à ce fichier peuvent provoquer un comportement incorrect et seront perdues si
'     le code est régénéré.
' </généré automatiquement>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class Controles_Dictionnaire

    '''<summary>
    '''Contrôle Dictionnaire.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Dictionnaire As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle CadreBouton.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CadreBouton As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle CadreCommandeRetour.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CadreCommandeRetour As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle CommandeRetour.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CommandeRetour As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Contrôle CadreAllerA.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CadreAllerA As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle EtiRecherche.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiRecherche As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle VRecherche.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VRecherche As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''Contrôle CmdRecherche.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CmdRecherche As Global.System.Web.UI.WebControls.ImageButton

    '''<summary>
    '''Contrôle PanelLstDico.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PanelLstDico As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''Contrôle TreeDictionnaire.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents TreeDictionnaire As Global.System.Web.UI.WebControls.TreeView
End Class
