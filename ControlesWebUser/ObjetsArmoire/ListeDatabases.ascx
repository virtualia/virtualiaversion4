﻿<%@ Control Language="VB" AutoEventWireup="false" Inherits="Virtualia.Net.Controles_ListeDatabases" Codebehind="ListeDatabases.ascx.vb" %>

<asp:Table ID="ArmoireDatabase" runat="server">
    <asp:TableRow>
        <asp:TableCell HorizontalAlign="Left"> 
            <asp:TreeView ID="TreeDatabases" runat="server" MaxDataBindDepth="2" BorderStyle="Inset"
                BorderColor="#B0E0D7" BorderWidth="2px" ForeColor="#142425"
                Width="230px" Height="250px" Font-Bold="False" NodeIndent="10" BackColor="Snow"
                style="margin-bottom: 10px; margin-left: 30px; display:table-cell; font-style: oblique" 
                LeafNodeStyle-HorizontalPadding="8px" >
                <SelectedNodeStyle BackColor="#6C9690" BorderColor="#E3924F"
                    BorderStyle="Solid" BorderWidth="1px" ForeColor="White" />
                <Nodes>
                    <asp:TreeNode PopulateOnDemand="False" Text="Bases de données" SelectAction="Select"
                        Value="Sgbdr"></asp:TreeNode>
                </Nodes>
            </asp:TreeView>
        </asp:TableCell>
    </asp:TableRow>
</asp:Table>

