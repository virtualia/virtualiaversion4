﻿Option Strict On
Option Explicit On
Option Compare Text
Imports VI = Virtualia.Systeme.Constantes
Partial Class Controles_ObjetsArmoire_AlbumdesScripts
    Inherits System.Web.UI.UserControl
    Public Delegate Sub Valeur_ChangeEventHandler(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.DonneeChangeEventArgs)
    Public Event ValeurChange As Valeur_ChangeEventHandler
    Public Delegate Sub ScriptResultatEventHandler(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.ScriptResultatEventArgs)
    Public Event ScriptResultat As ScriptResultatEventHandler
    Private WebFct As Virtualia.Net.WebFonctions
    Private WsPointdeVue As Short = 1
    Private WsOutil As Short
    Private WsNomStateScript As String = "AlbumScript"
    Private WsNomStateExe As String = "AlbumExec"
    '
    Protected Overridable Sub Saisie_Change(ByVal e As Virtualia.Systeme.Evenements.DonneeChangeEventArgs)
        RaiseEvent ValeurChange(Me, e)
    End Sub

    Protected Overridable Sub Resultat_Script(ByVal e As Virtualia.Systeme.Evenements.ScriptResultatEventArgs)
        RaiseEvent ScriptResultat(Me, e)
    End Sub

    Public Property V_PointdeVue() As Short
        Get
            Return WsPointdeVue
        End Get
        Set(ByVal value As Short)
            WsPointdeVue = value
        End Set
    End Property

    Public Property V_Outil() As Short
        Get
            Return WsOutil
        End Get
        Set(ByVal value As Short)
            WsOutil = value
            ExeDateFin.Text = WebFct.ViRhDates.DateduJour(False)
            Select Case WsOutil
                Case Is = VI.OptionInfo.DicoCmc
                    Eti01.Text = "Choix d'une armoire"
                    Cmd01.ImageUrl = "~/Images/Menus/Executer_Outil.bmp"
                    Cmd01.ToolTip = "Charger la requête dans l'armoire courante des dossiers"
                    PanelAlbum.BackColor = WebFct.ConvertCouleur("#19968D")
                    PanelAlbum.BackImageUrl = "~/Images/Armoire/Armoire_Standard.bmp"
                    PanelCmdScript.BackColor = WebFct.ConvertCouleur("#225C59")
                    EtiDateDebut.ForeColor = Drawing.Color.White
                    EtiDateFin.ForeColor = Drawing.Color.White
                Case Is = VI.OptionInfo.DicoExport
                    Eti01.Text = "Choix d'une liste à éditer"
                    Eti01.ForeColor = WebFct.ConvertCouleur("#142425")
                    EtiExec.ForeColor = WebFct.ConvertCouleur("#142425")
                    Cmd01.ImageUrl = "~/Images/Menus/Executer_Album.bmp"
                    Cmd01.ToolTip = "Exécuter la requête sur le contenu de l'armoire courante des dossiers"
                    PanelAlbum.BackImageUrl = "~/Images/Fonds/Fond_Header.jpg"
                    PanelAlbum.BackColor = WebFct.ConvertCouleur("#7D9F99")
                    PanelCmdScript.BackColor = WebFct.ConvertCouleur("#98D4CA")
                    EtiDateDebut.ForeColor = Drawing.Color.Black
                    EtiDateFin.ForeColor = Drawing.Color.Black
            End Select
            Dim I As Integer
            For I = 0 To TreeAlbum.Nodes.Count - 1
                TreeAlbum.Nodes(I).ChildNodes.Clear()
            Next I
            TreeAlbum.Nodes.Clear()
            Call FaireListeCategories()
            For I = 0 To TreeAlbum.Nodes.Count - 1
                Call FaireListeScripts(TreeAlbum.Nodes.Item(I))
            Next I
            TreeAlbum.ExpandAll()
        End Set
    End Property

    Public ReadOnly Property VTitre() As String
        Get
            Return SelIntituleW.Value
        End Get
    End Property

    Protected Sub ExeDateDebut_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ExeDateDebut.TextChanged
        Dim CacheExe As ArrayList
        If Me.ViewState(WsNomStateExe) IsNot Nothing Then
            CacheExe = CType(Me.ViewState(WsNomStateExe), ArrayList)
        Else
            Exit Sub
        End If
        Dim Chaine As String
        Chaine = WebFct.ViRhDates.DateSaisieVerifiee(ExeDateDebut.Text)
        CacheExe(1) = Chaine
        ExeDateDebut.Text = Chaine
        Me.ViewState.Remove(WsNomStateExe)
        Me.ViewState.Add(WsNomStateExe, CacheExe)

    End Sub

    Protected Sub ExeDateFin_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ExeDateFin.TextChanged
        Dim CacheExe As ArrayList
        If Me.ViewState(WsNomStateExe) IsNot Nothing Then
            CacheExe = CType(Me.ViewState(WsNomStateExe), ArrayList)
        Else
            Exit Sub
        End If
        Dim Chaine As String
        Chaine = WebFct.ViRhDates.DateSaisieVerifiee(ExeDateFin.Text)
        CacheExe(2) = Chaine
        ExeDateFin.Text = Chaine
        Me.ViewState.Remove(WsNomStateExe)
        Me.ViewState.Add(WsNomStateExe, CacheExe)

    End Sub

    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        WebFct = New Virtualia.Net.WebFonctions(Me, 0)
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            WsPointdeVue = WebFct.PointeurContexte.Fenetre_PointdeVue
            WsOutil = WebFct.PointeurContexte.Fenetre_Outil
        Catch ex As Exception
            Exit Try
        End Try
    End Sub

    Private Sub FaireListeScripts(ByVal Noeud As TreeNode)
        Dim ColScripts As Virtualia.Net.Script.EnsembleScripts
        Dim NewNoeud As TreeNode
        Dim NoCat As Short = CShort(Noeud.Value)
        Dim PvueOutil As Short
        Select Case WsOutil
            Case VI.OptionInfo.DicoCmc
                PvueOutil = VI.PointdeVue.PVueScriptCmc
            Case VI.OptionInfo.DicoExport
                PvueOutil = VI.PointdeVue.PVueScriptExport
        End Select
        ColScripts = WebFct.PointeurUtilisateur.PointeurScripts(PvueOutil, V_PointdeVue)
        If ColScripts Is Nothing Then
            Exit Sub
        End If

        Dim ListeScripts As New List(Of Virtualia.Net.Script.ObjetScript)
        Dim FicheScript As Virtualia.Net.Script.ObjetScript
        Try
            ListeScripts = (From instance In ColScripts.ListeDesScripts Select instance Where instance.PointdeVue = V_PointdeVue And instance.Categorie = NoCat Order By instance.Intitule Ascending).ToList
            If ListeScripts.Count = 0 Then
                Exit Sub
            End If
        Catch ex As Exception
            Exit Sub
        End Try

        Dim IndiceC As IEnumerator = ListeScripts.GetEnumerator
        While IndiceC.MoveNext
            FicheScript = CType(IndiceC.Current, Virtualia.Net.Script.ObjetScript)
            NewNoeud = New TreeNode(FicheScript.Intitule, FicheScript.Identifiant.ToString)
            NewNoeud.PopulateOnDemand = False
            Select Case WsOutil
                Case Is = VI.OptionInfo.DicoCmc
                    NewNoeud.ImageUrl = "~\Images\Menus\ArmoireJauneStd.bmp"
                Case Is = VI.OptionInfo.DicoExport
                    NewNoeud.ImageUrl = "~\Images\Menus\EditerStd.bmp"
            End Select
            NewNoeud.SelectAction = TreeNodeSelectAction.Select
            NewNoeud.ToolTip = FicheScript.ToolTip
            Noeud.ChildNodes.Add(NewNoeud)
        End While
    End Sub

    Private Sub FaireListeCategories()
        Dim NoCat As Integer = 0
        Dim NewNoeud As TreeNode

        Do
            NewNoeud = New TreeNode(WebFct.PointeurGlobal.LibelleCategorie(NoCat), NoCat.ToString)
            NewNoeud.PopulateOnDemand = False
            NewNoeud.ImageUrl = "~\Images\General\CarreUniversel.bmp"
            NewNoeud.SelectAction = TreeNodeSelectAction.Expand
            TreeAlbum.Nodes.Add(NewNoeud)
            NoCat += 1
            If NoCat > 12 Then
                Exit Do
            End If
        Loop
    End Sub

    Protected Sub TreeAlbum_SelectedNodeChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles TreeAlbum.SelectedNodeChanged
        Select Case CType(sender, System.Web.UI.WebControls.TreeView).SelectedNode.Depth
            Case Is < 1
                Exit Sub
        End Select
        If WsOutil = 0 Then
            Exit Sub
        End If

        Dim SelIde As String = CType(sender, System.Web.UI.WebControls.TreeView).SelectedNode.Value
        SelIntituleW.Value = CType(sender, System.Web.UI.WebControls.TreeView).SelectedNode.Text

        Dim FicheScript As Virtualia.Net.Script.ObjetScript
        Dim PvueOutil As Short
        Dim ColScripts As Virtualia.Net.Script.EnsembleScripts

        Select Case WsOutil
            Case VI.OptionInfo.DicoCmc
                PvueOutil = VI.PointdeVue.PVueScriptCmc
            Case VI.OptionInfo.DicoExport
                PvueOutil = VI.PointdeVue.PVueScriptExport
        End Select

        ColScripts = WebFct.PointeurUtilisateur.PointeurScripts(PvueOutil, V_PointdeVue)
        FicheScript = ColScripts.ListeDesScripts.Find(Function(Recherche) Recherche.Identifiant = CInt(SelIde))

        If FicheScript Is Nothing Then
            Exit Sub
        End If
        Dim SelScript As ArrayList = FicheScript.ScriptLu
        Dim SelExe As ArrayList = FicheScript.ConditionsLues

        If FicheScript IsNot Nothing Then
            If Me.ViewState(WsNomStateExe) IsNot Nothing Then
                Me.ViewState.Remove(WsNomStateExe)
            End If
            Me.ViewState.Add(WsNomStateExe, FicheScript.ConditionsLues)
            If Me.ViewState(WsNomStateScript) IsNot Nothing Then
                Me.ViewState.Remove(WsNomStateScript)
            End If
            Me.ViewState.Add(WsNomStateScript, FicheScript.ScriptLu)
        End If
    End Sub

    Protected Sub Cmd01_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Cmd01.Click
        'Obtenir
        If Me.ViewState(WsNomStateScript) Is Nothing Then
            Exit Sub
        End If
        If Me.ViewState(WsNomStateExe) Is Nothing Then
            Exit Sub
        End If
        Dim CacheScripts As ArrayList = CType(Me.ViewState(WsNomStateScript), ArrayList)
        Dim CacheExe As ArrayList = CType(Me.ViewState(WsNomStateExe), ArrayList)
        Dim K As Integer = 0

        Select Case WsOutil
            Case VI.OptionInfo.DicoCmc
                WebFct.PointeurContexte.Armoire_Active = 1000
                Dim ScriptExe As Virtualia.Net.Script.ExecutionCMC

                K = WebFct.PointeurUtilisateur.NombreDossiersArmoire(WebFct.PointeurContexte.Armoire_Active)

                ScriptExe = New Virtualia.Net.Script.ExecutionCMC(WebFct.PointeurUtilisateur, _
                                VI.PointdeVue.PVueApplicatif, True)

                K = ScriptExe.Executer(CacheExe, CacheScripts)

                If K = 0 Then
                    Exit Sub
                End If

                WebFct.PointeurArmoire.EnsembleDossiers = ScriptExe.ListeArmoire

                Dim Evenement As Virtualia.Systeme.Evenements.DonneeChangeEventArgs
                Evenement = New Virtualia.Systeme.Evenements.DonneeChangeEventArgs(SelIntituleW.Value)
                ScriptExe.Dispose()
                Saisie_Change(Evenement)

            Case VI.OptionInfo.DicoExport
                If WebFct.PointeurArmoire.EnsembleDossiers.Count = 0 Then
                    Exit Sub
                End If
                Dim ScriptExe As Virtualia.Net.Script.ExecutionExtraction

                ScriptExe = New Virtualia.Net.Script.ExecutionExtraction(WebFct.PointeurUtilisateur, VI.PointdeVue.PVueApplicatif)
                ScriptExe.TableIdentifiants = WebFct.PointeurArmoire.TabIdentifiants

                K = ScriptExe.Executer(CacheExe, CacheScripts)
                If K > 0 Then
                    Dim Evenement As Virtualia.Systeme.Evenements.ScriptResultatEventArgs
                    Dim LibCols As String = ScriptExe.LibelleColonnes

                    Evenement = New Virtualia.Systeme.Evenements.ScriptResultatEventArgs("", ScriptExe.LibelleColonnes, ScriptExe.Resultat, ScriptExe.CentrageColonne)
                    ScriptExe.Dispose()
                    Resultat_Script(Evenement)
                End If

        End Select
    End Sub

End Class
