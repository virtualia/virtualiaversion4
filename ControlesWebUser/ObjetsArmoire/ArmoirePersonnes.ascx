﻿<%@ Control Language="VB" AutoEventWireup="false" Inherits="Virtualia.Net.Controles_ArmoirePersonnes" Codebehind="ArmoirePersonnes.ascx.vb" %>

<div style=" background-image: url(../Images/Armoire/Armoire_Standard.bmp); width:330px; height:1220px; border: outset 4px #B0E0D7; font-family: Trebuchet MS; font-size: small; 
    font-style: oblique" >
  <asp:Table ID="CadreCmd" runat="server" width="330px" Height="40px" CellSpacing="0" >
    <asp:TableRow>
      <asp:TableCell HorizontalAlign="Center" VerticalAlign="Middle">
           <asp:ImageButton ID="Cmd00" runat="server" BorderStyle="None"
                ImageAlign="Middle" ImageUrl="~/Images/Menus/ArmoireJaune.bmp" 
                ToolTip="Sélectionner ses armoires" /> 
           <asp:ImageButton ID="Cmd01" runat="server" BorderStyle="None" PostBackUrl = "~/Fenetres/FrmNouveauDossierPER.aspx"
                ImageAlign="Middle" ImageUrl="~/Images/Menus/NouveauDocument.bmp" 
                ToolTip="Nouveau dossier" /> 
           <asp:ImageButton ID="Cmd02" runat="server" BorderStyle="None" 
                ImageAlign="Middle" ImageUrl="~/Images/Menus/RechercherPersonne.bmp"
                ToolTip="Rechercher un dossier" /> 
           <asp:ImageButton ID="Cmd03" runat="server" BorderStyle="None" 
                ImageAlign="Middle" ImageUrl="~/Images/Menus/SupprimerDossier.bmp"
                ToolTip="Supprimer un dossier" /> 
           <asp:ImageButton ID="Cmd04" runat="server" BorderStyle="None" 
                ImageAlign="Middle" ImageUrl="~/Images/Menus/Exporter.bmp"
                ToolTip="Transférer un dossier" />
           <asp:ImageButton ID="Cmd05" runat="server" BorderStyle="None" 
                ImageAlign="Middle" ImageUrl="~/Images/Menus/Operationscollectives.bmp"
                ToolTip="Organiser en fonction du critère de couleur" />
           <asp:ImageButton ID="Cmd06" runat="server" BorderStyle="None" 
                ImageAlign="Middle" ImageUrl="~/Images/Menus/Organiser.bmp"
                ToolTip="Organiser en fonction des caractéristiques de l'armoire" />
           <asp:ImageButton ID="Cmd07" runat="server" BorderStyle="None" 
                ImageAlign="Middle" ImageUrl="~/Images/Menus/Parametres.bmp"
                ToolTip="Organiser ses préférences de couleur" />
           <asp:ImageButton ID="Cmd08" runat="server" BorderStyle="None" 
                ImageAlign="Middle" ImageUrl="~/Images/Menus/Editer_Fonce.bmp"
                ToolTip="Editer l'armoire" />   
      </asp:TableCell>
    </asp:TableRow>
 </asp:Table>
 <asp:UpdatePanel ID="UpdatePanelOK" runat="server">
   <ContentTemplate>
       <asp:Table ID="CadreRecherche" runat="server" CellPadding="1" CellSpacing="0">
            <asp:TableRow>
                <asp:TableCell>
                    <asp:DropDownList ID="DropDownArmoire" runat="server" AutoPostBack="True" 
                        Height="22px" Width="205px" BackColor="Snow" ForeColor="#142425"
                        style="margin-top: 0px; margin-left: 7px; border-spacing: 2px; font-family:Trebuchet MS; 
                              font-size: small; font-weight: bold; font-style: normal; text-indent: 5px; text-align: left">
                        <asp:ListItem Value="0">Tous les dossiers</asp:ListItem>
                        <asp:ListItem Value="1">Les dossiers en activité</asp:ListItem>
                        <asp:ListItem Value="1000">Armoire personnalisée</asp:ListItem>
                        <asp:ListItem Value="1001">Le panel issu de la recherche</asp:ListItem>
                    </asp:DropDownList>
                 </asp:TableCell>
                 <asp:TableCell> 
                    <asp:TextBox ID="VRecherche" runat="server" BackColor="Snow" 
                        BorderColor="#000066" BorderStyle="Inset" Height="19px" Width="75px" 
                        Font-Bold="false" ForeColor="#142425"></asp:TextBox>
                </asp:TableCell>
                <asp:TableCell > 
                    <asp:ImageButton ID="CmdRecherche" runat="server" BorderStyle="None"
                       ImageAlign="Middle" ImageUrl="~/Images/Menus/Executer_Fonce.bmp"
                       ToolTip="Rechercher dans l'armoire en fonction du critère saisi (* et blanc autorisés)"
                       style="margin-bottom: 2px" />
                </asp:TableCell>
            </asp:TableRow>
        </asp:Table>
        <asp:Table ID="CadreLettreAM" runat="server" style="height: 18px; width:312px" 
                   CellPadding="0" CellSpacing="0" HorizontalAlign="Center" >
            <asp:TableRow>
                <asp:TableCell>
                    <asp:ImageButton ID="ButtonA" runat="server" BorderStyle="None"  
                     ImageAlign="Middle" ImageUrl="~/Images/Lettres/A.bmp" />
                 </asp:TableCell>
                <asp:TableCell >
                    <asp:ImageButton ID="ButtonB" runat="server" BorderStyle="None"  
                    ImageAlign="Middle" ImageUrl="~/Images/Lettres/B.bmp" />
                </asp:TableCell>
                <asp:TableCell >     
                    <asp:ImageButton ID="ButtonC" runat="server" BorderStyle="None"  
                        ImageAlign="Middle" ImageUrl="~/Images/Lettres/C.bmp" />
                </asp:TableCell>
                <asp:TableCell > 
                    <asp:ImageButton ID="ButtonD" runat="server" BorderStyle="None"  
                        ImageAlign="Middle" ImageUrl="~/Images/Lettres/D.bmp" />
                </asp:TableCell>
                <asp:TableCell > 
                    <asp:ImageButton ID="ButtonE" runat="server" BorderStyle="None"   
                        ImageAlign="Middle" ImageUrl="~/Images/Lettres/E.bmp" />
                </asp:TableCell>
                <asp:TableCell > 
                    <asp:ImageButton ID="ButtonF" runat="server" BorderStyle="None"  
                        ImageAlign="Middle" ImageUrl="~/Images/Lettres/F.bmp" />
                </asp:TableCell>
                <asp:TableCell > 
                    <asp:ImageButton ID="ButtonG" runat="server" BorderStyle="None"  
                        ImageAlign="Middle" ImageUrl="~/Images/Lettres/G.bmp" />
                </asp:TableCell>
                <asp:TableCell > 
                    <asp:ImageButton ID="ButtonH" runat="server" BorderStyle="None"  
                        ImageAlign="Middle" ImageUrl="~/Images/Lettres/H.bmp" />
                </asp:TableCell>
                <asp:TableCell > 
                    <asp:ImageButton ID="ButtonI" runat="server" BorderStyle="None"  
                        ImageAlign="Middle" ImageUrl="~/Images/Lettres/I.bmp" />
                </asp:TableCell>
                <asp:TableCell > 
                    <asp:ImageButton ID="ButtonJ" runat="server" BorderStyle="None"  
                        ImageAlign="Middle" ImageUrl="~/Images/Lettres/J.bmp" />
                </asp:TableCell>
                <asp:TableCell > 
                    <asp:ImageButton ID="ButtonK" runat="server" BorderStyle="None"  
                        ImageAlign="Middle" ImageUrl="~/Images/Lettres/K.bmp" />
                </asp:TableCell>
                <asp:TableCell > 
                    <asp:ImageButton ID="ButtonL" runat="server" BorderStyle="None"  
                        ImageAlign="Middle" ImageUrl="~/Images/Lettres/L.bmp" />
                </asp:TableCell>
                <asp:TableCell > 
                    <asp:ImageButton ID="ButtonM" runat="server" BorderStyle="None"  
                        ImageAlign="Middle" ImageUrl="~/Images/Lettres/M.bmp" />
                </asp:TableCell>
            </asp:TableRow>
        </asp:Table>
        <asp:Table ID="CadreLettreNZ" runat="server" style="height: 18px; width: 312px"
                   CellPadding="0" CellSpacing="0" HorizontalAlign="Center" >
            <asp:TableRow>
                 <asp:TableCell > 
                    <asp:ImageButton ID="ButtonN" runat="server" BorderStyle="None"  
                        ImageAlign="Middle" ImageUrl="~/Images/Lettres/N.bmp" />
                </asp:TableCell>
                <asp:TableCell > 
                    <asp:ImageButton ID="ButtonO" runat="server" BorderStyle="None"  
                        ImageAlign="Middle" ImageUrl="~/Images/Lettres/O.bmp" />
                </asp:TableCell>
                <asp:TableCell > 
                    <asp:ImageButton ID="ButtonP" runat="server" BorderStyle="None"  
                        ImageAlign="Middle" ImageUrl="~/Images/Lettres/P.bmp" />
                 </asp:TableCell>
                <asp:TableCell > 
                    <asp:ImageButton ID="ButtonQ" runat="server" BorderStyle="None"  
                     ImageAlign="Middle" ImageUrl="~/Images/Lettres/Q.bmp" />
                 </asp:TableCell>
                <asp:TableCell > 
                    <asp:ImageButton ID="ButtonR" runat="server" BorderStyle="None"   
                    ImageAlign="Middle" ImageUrl="~/Images/Lettres/R.bmp" />
                </asp:TableCell>
                <asp:TableCell > 
                    <asp:ImageButton ID="ButtonS" runat="server" BorderStyle="None"  
                        ImageAlign="Middle" ImageUrl="~/Images/Lettres/S.bmp" />
                </asp:TableCell>
                <asp:TableCell > 
                    <asp:ImageButton ID="ButtonT" runat="server" BorderStyle="None"   
                        ImageAlign="Middle" ImageUrl="~/Images/Lettres/T.bmp" />
                </asp:TableCell>
                <asp:TableCell > 
                    <asp:ImageButton ID="ButtonU" runat="server" BorderStyle="None"  
                        ImageAlign="Middle" ImageUrl="~/Images/Lettres/U.bmp" />
                </asp:TableCell>
                <asp:TableCell > 
                    <asp:ImageButton ID="ButtonV" runat="server" BorderStyle="None"   
                        ImageAlign="Middle" ImageUrl="~/Images/Lettres/V.bmp" />
                </asp:TableCell>
                <asp:TableCell > 
                    <asp:ImageButton ID="ButtonW" runat="server" BorderStyle="None"  
                        ImageAlign="Middle" ImageUrl="~/Images/Lettres/W.bmp" />
                </asp:TableCell>
                <asp:TableCell > 
                    <asp:ImageButton ID="ButtonX" runat="server" BorderStyle="None"
                        ImageAlign="Middle" ImageUrl="~/Images/Lettres/X.bmp" />
                </asp:TableCell>
                <asp:TableCell > 
                    <asp:ImageButton ID="ButtonY" runat="server" BorderStyle="None" 
                        ImageAlign="Middle" ImageUrl="~/Images/Lettres/Y.bmp" />
                </asp:TableCell>
                <asp:TableCell > 
                    <asp:ImageButton ID="ButtonZ" runat="server" BorderStyle="None" 
                        ImageAlign="Middle" ImageUrl="~/Images/Lettres/Z.bmp" />
                </asp:TableCell>
            </asp:TableRow>
        </asp:Table>
        <asp:Table ID="CadreImage" runat="server" CellPadding="1" CellSpacing="0" HorizontalAlign="Center" 
              style="border-bottom: Solid 3px #D7FAF3; height: 18px; width: 330px; text-align: center;" >
            <asp:TableRow>
                <asp:TableCell>
                    <asp:ImageButton ID="BImage0" runat="server" BorderStyle="Solid" 
                        ImageAlign="Middle" ImageUrl=" ~/Images/Armoire/JauneFermer16.bmp" />
                    <asp:ImageButton ID="BImage1" runat="server" BorderStyle="Solid"  
                        ImageAlign="Middle" ImageUrl="~/Images/Armoire/BleuClairFermer16.bmp" />
                    <asp:ImageButton ID="BImage2" runat="server" BorderStyle="Solid" 
                        ImageAlign="Middle" ImageUrl="~/Images/Armoire/FuschiaFermer16.bmp" />
                    <asp:ImageButton ID="BImage3" runat="server" BorderStyle="Solid" 
                        ImageAlign="Middle" ImageUrl="~/Images/Armoire/BleuFermer16.bmp" />
                    <asp:ImageButton ID="BImage4" runat="server" BorderStyle="Solid" 
                        ImageAlign="Middle" ImageUrl="~/Images/Armoire/GrisClairFermer16.bmp" />
                    <asp:ImageButton ID="BImage5" runat="server" BorderStyle="Solid" 
                        ImageAlign="Middle" ImageUrl="~/Images/Armoire/BleuFonceFermer16.bmp" />
                    <asp:ImageButton ID="BImage6" runat="server" BorderStyle="Solid" 
                        ImageAlign="Middle" ImageUrl="~/Images/Armoire/GrisFonceFermer16.bmp" />
                    <asp:ImageButton ID="BImage7" runat="server" BorderStyle="Solid" 
                        ImageAlign="Middle" ImageUrl="~/Images/Armoire/MarronFermer16.bmp" />
                    <asp:ImageButton ID="BImage8" runat="server" BorderStyle="Solid" 
                        ImageAlign="Middle" ImageUrl="~/Images/Armoire/NoirFermer16.bmp" />
                    <asp:ImageButton ID="BImage9" runat="server" BorderStyle="Solid" 
                        ImageAlign="Middle" ImageUrl="~/Images/Armoire/OrangeFermer16.bmp" />
                    <asp:ImageButton ID="BImage10" runat="server" BorderStyle="Solid" 
                        ImageAlign="Middle" ImageUrl="~/Images/Armoire/TurquoiseFermer16.bmp" />
                    <asp:ImageButton ID="BImage11" runat="server" BorderStyle="Solid" 
                        ImageAlign="Middle" ImageUrl="~/Images/Armoire/OrangeFonceFermer16.bmp" />
                    <asp:ImageButton ID="BImage12" runat="server" BorderStyle="Solid" 
                        ImageAlign="Middle" ImageUrl="~/Images/Armoire/RougeFermer16.bmp" />
                    <asp:ImageButton ID="BImage13" runat="server" BorderStyle="Solid" 
                        ImageAlign="Middle" ImageUrl="~/Images/Armoire/VertFonceFermer16.bmp" />
                    <asp:ImageButton ID="BImage14" runat="server" BorderStyle="Solid" 
                        ImageAlign="Middle" ImageUrl="~/Images/Armoire/SaumonFermer16.bmp" />
                    <asp:ImageButton ID="BImage15" runat="server" BorderStyle="Solid" 
                        ImageAlign="Middle" ImageUrl="~/Images/Armoire/VertClairFermer16.bmp" />
                 </asp:TableCell>
            </asp:TableRow>
        </asp:Table>
        <asp:Table ID="CadreListe" runat="server" style="margin-top: 0px; width: 320px; height:1040px;
             background-attachment: inherit; display: table-cell;" >
           <asp:TableRow>
             <asp:TableCell>
                <div style=" margin-top: 0px; width: 320px; height:1040px; vertical-align:top; overflow: scroll;
                        background-color: White">   
                    <asp:TreeView ID="TreeListeDossier" runat="server" MaxDataBindDepth="2" BorderStyle="None" 
                        BorderWidth="2px" BorderColor="Snow" ForeColor="#142425"
                        Width="320px" Height="1020px" Font-Bold="True" NodeIndent="10" BackColor="Snow"
                        LeafNodeStyle-HorizontalPadding="8px" RootNodeStyle-Font-Bold="True"
                        RootNodeStyle-ImageUrl="~/Images/Icones/DatabaseBleu.bmp" RootNodeStyle-Font-Italic="False">
                        <SelectedNodeStyle BackColor="#6C9690" BorderColor="#E3924F" 
                            BorderStyle="Solid" BorderWidth="1px" ForeColor="White" />
                        <Nodes>
                            <asp:TreeNode PopulateOnDemand="True" Text="Armoire virtuelle" 
                                Value="Armoirevirtuelle"></asp:TreeNode>
                        </Nodes>
                    </asp:TreeView>
                </div>
             </asp:TableCell>
          </asp:TableRow>
          <asp:TableRow>
            <asp:TableCell Width="320px" HorizontalAlign="Center">
                <asp:Label ID="EtiStatus1" runat="server" Height="15px" Width="150px"
                    BackColor="#D7DEFA" Visible="true" BorderColor="#CCFFFF"  
                    BorderStyle="Inset" BorderWidth="2px" ForeColor="#142425"
                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                    style="margin-left: 0px; font-style: oblique; text-align: center">
                </asp:Label>
                <asp:Label ID="EtiStatus2" runat="server" Height="15px" Width="150px"
                    BackColor="#D7DEFA" Visible="true" BorderColor="#CCFFFF"  
                    BorderStyle="Inset" BorderWidth="2px" ForeColor="#142425"
                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                    style="margin-left: 0px; font-style: oblique; text-align: center">
                </asp:Label>
            </asp:TableCell>
           </asp:TableRow>
        </asp:Table>
   </ContentTemplate>
 </asp:UpdatePanel>
 </div>