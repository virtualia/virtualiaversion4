﻿Option Strict On
Option Explicit On
Option Compare Text
Imports VI = Virtualia.Systeme.Constantes
Partial Class Controles_ArmoirePersonnes
    Inherits System.Web.UI.UserControl
    Private WebFct As Virtualia.Net.WebFonctions
    Private WsNomState As String = "CtlArmoire"
    '
    Private Const ListeSimple As Short = 0
    Private Const ListeHie As Short = 1
    '
    Private Const TousLesDossiers As Short = 0
    Private Const EnActivite As Short = 1
    Private Const Personnalisee As Short = 1000
    Private Const LePanel As Short = 1001
    '
    Public Delegate Sub Dossier_ClickEventHandler(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.DossierClickEventArgs)
    Public Event Dossier_Click As Dossier_ClickEventHandler
    Public Delegate Sub CmdAlbum_ClickEventHandler(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.AppelAlbumClickEventArgs)
    Public Event CmdAlbum_Click As CmdAlbum_ClickEventHandler
    Public Delegate Sub Valeur_ChangeEventHandler(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.DonneeChangeEventArgs)
    Public Event ValeurChange As Valeur_ChangeEventHandler
    Public Delegate Sub MsgDialog_MsgEventHandler(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.MessageSaisieEventArgs)
    Public Event MessageDialogue As MsgDialog_MsgEventHandler

    Protected Overridable Sub VMessageDialog(ByVal e As Virtualia.Systeme.Evenements.MessageSaisieEventArgs)
        RaiseEvent MessageDialogue(Me, e)
    End Sub

    Protected Overridable Sub VDossier_Click(ByVal e As Virtualia.Systeme.Evenements.DossierClickEventArgs)
        RaiseEvent Dossier_Click(Me, e)
    End Sub

    Protected Overridable Sub VAlbum_Click(ByVal e As Virtualia.Systeme.Evenements.AppelAlbumClickEventArgs)
        RaiseEvent CmdAlbum_Click(Me, e)
    End Sub

    Protected Overridable Sub Saisie_Change(ByVal e As Virtualia.Systeme.Evenements.DonneeChangeEventArgs)
        RaiseEvent ValeurChange(Me, e)
    End Sub

    Public WriteOnly Property ArmoirePersonnalisee() As String
        Set(ByVal value As String)
            If value = "" Then
                Exit Property
            End If
            TreeListeDossier.Nodes(0).ChildNodes.Clear()
            WebFct.PointeurContexte.Armoire_Lettre = ""
            WebFct.PointeurContexte.Armoire_CouleurEti = ""
            WebFct.PointeurArmoire.NomdelArmoire = value
            VRecherche.Text = ""
            DropDownArmoire.SelectedValue = "1000"
            DropDownArmoire.ForeColor = Drawing.Color.Gray
            Select Case WebFct.PointeurContexte.Armoire_ListeActive
                Case ListeSimple
                    Call FaireListeSimple(TreeListeDossier.Nodes(0))
                    TreeListeDossier.Nodes(0).ExpandAll()
                Case ListeHie
                    Call FaireListeHieN0(TreeListeDossier.Nodes(0))
                    TreeListeDossier.Nodes(0).ExpandAll()
            End Select
            WebFct.PointeurContexte.InitialiserFenetres()
        End Set
    End Property

    Public Sub Actualiser()
        Call InitialiserBoutonsLettre()
        If WebFct.PointeurContexte.Armoire_Lettre = "" Then
            WebFct.PointeurContexte.Armoire_Lettre = "A"
        Else
            WebFct.PointeurContexte.Armoire_Lettre = ""
        End If
        VRecherche.Text = WebFct.PointeurContexte.Armoire_Lettre
        TreeListeDossier.Nodes(0).ChildNodes.Clear()
        Select Case WebFct.PointeurContexte.Armoire_ListeActive
            Case ListeSimple
                Call FaireListeSimple(TreeListeDossier.Nodes(0))
                TreeListeDossier.Nodes(0).ExpandAll()
            Case ListeHie
                Call FaireListeHieN0(TreeListeDossier.Nodes(0))
                TreeListeDossier.Nodes(0).ExpandAll()
        End Select
    End Sub

    Public ReadOnly Property CompteDossiers() As String
        Get
            Dim Nb As Integer
            Dim Libel As String
            If IsNumeric(EtiStatus1.Text) Then
                Nb = CInt(EtiStatus1.Text)
                Select Case Nb
                    Case Is = 0
                        Libel = "(Aucun dossier)"
                    Case Is = 1
                        Libel = "(Un dossier)"
                    Case Else
                        Libel = "(" & Nb.ToString & " dossiers)"
                End Select
            Else
                Libel = "Aucun dossier"
            End If
            Return Libel
        End Get
    End Property

    Protected Sub TreeListeDossier_SelectedNodeChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles TreeListeDossier.SelectedNodeChanged
        If WebFct.PointeurArmoire Is Nothing Then
            Exit Sub
        End If
        Dim SelIde As Integer
        Dim SelNom As String
        Dim SelPrenom As String

        If IsNumeric(CType(sender, System.Web.UI.WebControls.TreeView).SelectedNode.Value) Then
            SelIde = CInt(CType(sender, System.Web.UI.WebControls.TreeView).SelectedNode.Value)
        Else
            Exit Sub
        End If
        If SelIde = 0 Then
            Exit Sub
        End If

        Dim Predicat As New Virtualia.Ressources.Predicats.PredicateDossier(SelIde)
        Dim LstDossier As New List(Of Virtualia.Ressources.Datas.ObjetDossierPER)
        LstDossier = WebFct.PointeurArmoire.EnsembleDossiers.FindAll(AddressOf Predicat.RechercherIdentifiant)
        If LstDossier.Count = 0 Then
            Exit Sub
        End If
        SelNom = LstDossier.Item(0).Nom
        SelPrenom = LstDossier.Item(0).Prenom

        WebFct.PointeurContexte.Armoire_Identifiant = SelIde

        Dim Evenement As Virtualia.Systeme.Evenements.DossierClickEventArgs
        Evenement = New Virtualia.Systeme.Evenements.DossierClickEventArgs(SelIde)
        VDossier_Click(Evenement)
    End Sub

    Protected Sub TreeListeDossier_TreeNodePopulate(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.TreeNodeEventArgs) Handles TreeListeDossier.TreeNodePopulate
        WebFct.PointeurContexte.Armoire_Lettre = VRecherche.Text
        Select Case WebFct.PointeurContexte.Armoire_ListeActive
            Case ListeSimple
                TreeListeDossier.MaxDataBindDepth = 1
                TreeListeDossier.ImageSet = System.Web.UI.WebControls.TreeViewImageSet.XPFileExplorer
                If e.Node.ChildNodes.Count = 0 Then
                    Select Case e.Node.Depth
                        Case Is = 0
                            Call FaireListeSimple(e.Node)
                    End Select
                End If
                e.Node.Expand()
            Case ListeHie
                TreeListeDossier.MaxDataBindDepth = 2
                TreeListeDossier.ImageSet = System.Web.UI.WebControls.TreeViewImageSet.XPFileExplorer
                If e.Node.ChildNodes.Count = 0 Then
                    Select Case e.Node.Depth
                        Case Is = 0
                            Call FaireListeHieN0(e.Node)
                        Case Is = 1
                            Call FaireListeHieN1(e.Node)
                    End Select
                End If
                e.Node.Expand()
        End Select
    End Sub

    Protected Sub ButtonLettre_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ButtonA.Click, _
    ButtonB.Click, ButtonC.Click, ButtonD.Click, ButtonE.Click, ButtonF.Click, ButtonG.Click, ButtonH.Click, _
    ButtonI.Click, ButtonJ.Click, ButtonK.Click, ButtonL.Click, ButtonM.Click, ButtonN.Click, ButtonO.Click, _
    ButtonP.Click, ButtonQ.Click, ButtonR.Click, ButtonS.Click, ButtonT.Click, ButtonU.Click, ButtonV.Click, _
    ButtonW.Click, ButtonX.Click, ButtonY.Click, ButtonZ.Click

        Call InitialiserBoutonsLettre()
        WebFct.PointeurContexte.Armoire_Lettre = Strings.Right(CType(sender, ImageButton).ID, 1)
        CType(sender, ImageButton).ImageUrl = "~/Images/Lettres/" & Strings.Right(CType(sender, ImageButton).ID, 1) & "_Sel.bmp"
        VRecherche.Text = WebFct.PointeurContexte.Armoire_Lettre
        TreeListeDossier.Nodes(0).ChildNodes.Clear()
        Select Case WebFct.PointeurContexte.Armoire_ListeActive
            Case ListeSimple
                Call FaireListeSimple(TreeListeDossier.Nodes(0))
                TreeListeDossier.Nodes(0).ExpandAll()
            Case ListeHie
                Call FaireListeHieN0(TreeListeDossier.Nodes(0))
                TreeListeDossier.Nodes(0).ExpandAll()
        End Select
    End Sub

    Protected Sub BImage_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles BImage0.Click, _
    BImage1.Click, BImage2.Click, BImage3.Click, BImage4.Click, BImage5.Click, BImage6.Click, BImage7.Click, _
    BImage8.Click, BImage9.Click, BImage10.Click, BImage11.Click, BImage12.Click, BImage13.Click, BImage14.Click, _
    BImage15.Click

        Dim Bouton As ImageButton
        Bouton = CType(sender, ImageButton)
        WebFct.PointeurContexte.Armoire_CouleurEti = Bouton.ImageUrl
        WebFct.PointeurContexte.Armoire_Lettre = ""
        VRecherche.Text = ""
        TreeListeDossier.Nodes(0).ChildNodes.Clear()
        Select Case WebFct.PointeurContexte.Armoire_ListeActive
            Case ListeSimple
                Call FaireListeSimple(TreeListeDossier.Nodes(0))
                TreeListeDossier.Nodes(0).ExpandAll()
            Case ListeHie
                Call FaireListeHieN0(TreeListeDossier.Nodes(0))
                TreeListeDossier.Nodes(0).ExpandAll()
        End Select
    End Sub

    Protected Sub DropDownArmoire_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DropDownArmoire.SelectedIndexChanged

        If DropDownArmoire.SelectedItem.Value = "1001" Then
            If WebFct.PointeurContexte.ListePanel Is Nothing Then
                Exit Sub
            End If
            If WebFct.PointeurContexte.ListePanel.Count = 0 Then
                Exit Sub
            End If
            Call ObtenirPanel()
        End If
        TreeListeDossier.Nodes(0).ChildNodes.Clear()
        VRecherche.Text = ""
        WebFct.PointeurContexte.Armoire_Lettre = ""
        WebFct.PointeurContexte.Armoire_CouleurEti = ""

        WebFct.PointeurContexte.Armoire_Active = CShort(DropDownArmoire.SelectedItem.Value)
        If DropDownArmoire.SelectedItem.Value > "999" Then
            DropDownArmoire.ForeColor = Drawing.Color.Gray
        Else
            DropDownArmoire.ForeColor = WebFct.ConvertCouleur("#142425")
        End If

        Select Case WebFct.PointeurContexte.Armoire_ListeActive
            Case ListeSimple
                Call FaireListeSimple(TreeListeDossier.Nodes(0))
                TreeListeDossier.Nodes(0).ExpandAll()
            Case ListeHie
                Call FaireListeHieN0(TreeListeDossier.Nodes(0))
                TreeListeDossier.Nodes(0).ExpandAll()
        End Select
        WebFct.PointeurContexte.InitialiserFenetres()

        If DropDownArmoire.SelectedItem.Value > "999" Then
            Exit Sub
        End If

        Dim Evenement As Virtualia.Systeme.Evenements.DonneeChangeEventArgs
        Evenement = New Virtualia.Systeme.Evenements.DonneeChangeEventArgs(WebFct.PointeurArmoire.NomdelArmoire & Strings.Space(1) & CompteDossiers)
        Saisie_Change(Evenement)
    End Sub

    Private Sub FaireListeSimple(ByVal Noeud As TreeNode)
        Dim IndiceA As Integer
        Dim VImage As String = ""
        Dim SiAfaire As Boolean
        Dim CptSel As Integer = 0
        Dim VirtuelPath As String = ""
        Dim CptTotal As Integer

        EtiStatus1.Text = ""
        EtiStatus2.Text = ""
        CptTotal = WebFct.PointeurUtilisateur.NombreDossiersArmoire(WebFct.PointeurContexte.Armoire_Active)
        If CptTotal = 0 Then
            Exit Sub
        End If
        If WebFct.PointeurArmoire.EnsembleDossiers Is Nothing Then
            Exit Sub
        End If
        If WebFct.PointeurArmoire.EnsembleDossiers.Count = 0 Then
            Exit Sub
        End If

        If WebFct.PointeurContexte.Armoire_Lettre = "" And WebFct.PointeurUtilisateur.NombreDossiersArmoire(WebFct.PointeurContexte.Armoire_Active) > 1000 Then
            WebFct.PointeurContexte.Armoire_Lettre = "A"
            VRecherche.Text = "A"
        End If
        Noeud.Text = WebFct.PointeurArmoire.NomdelArmoire

        Dim FichePer As Virtualia.Ressources.Datas.ObjetDossierPER
        Dim Predicat As New Virtualia.Ressources.Predicats.PredicateDossier(WebFct.PointeurContexte.Armoire_Lettre)
        Dim LstDossier As New List(Of Virtualia.Ressources.Datas.ObjetDossierPER)
        LstDossier = WebFct.PointeurArmoire.EnsembleDossiers.FindAll(AddressOf Predicat.RechercherNom)
        If LstDossier.Count = 0 Then
            Exit Sub
        End If

        Call InitialiserCouleurs()

        For IndiceA = 0 To LstDossier.Count - 1
            SiAfaire = True
            FichePer = LstDossier.Item(IndiceA)
            VImage = IconeArmoire(FichePer.CritereIcone)
            Select Case WebFct.PointeurContexte.Armoire_CouleurEti
                Case Is <> ""
                    Select Case WebFct.PointeurContexte.Armoire_CouleurEti
                        Case Is <> VImage
                            SiAfaire = False
                    End Select
            End Select
            Select Case SiAfaire
                Case True
                    Dim NewNoeud As TreeNode = New TreeNode(FichePer.Nom & Space(1) & FichePer.Prenom, FichePer.V_Identifiant.ToString)
                    NewNoeud.ImageUrl = VImage
                    NewNoeud.PopulateOnDemand = False
                    NewNoeud.SelectAction = TreeNodeSelectAction.Select
                    Noeud.ChildNodes.Add(NewNoeud)
                    If FichePer.V_Identifiant = WebFct.PointeurContexte.Armoire_Identifiant Then
                        VirtuelPath = NewNoeud.ValuePath
                    End If
                    CptSel += 1
            End Select
        Next IndiceA
        If VirtuelPath <> "" Then
            TreeListeDossier.FindNode(VirtuelPath).Selected = True
        End If
        If EtiStatus1.Text = "" Then
            EtiStatus1.Text = CptTotal.ToString
            Dim Evenement As Virtualia.Systeme.Evenements.DonneeChangeEventArgs
            Evenement = New Virtualia.Systeme.Evenements.DonneeChangeEventArgs(WebFct.PointeurArmoire.NomdelArmoire & Strings.Space(1) & CompteDossiers)
            Saisie_Change(Evenement)
        End If
        EtiStatus1.Text = CptTotal.ToString
        EtiStatus2.Text = CptSel.ToString
    End Sub

    Private Sub FaireListeHieN0(ByVal Noeud As TreeNode)
        Dim IndiceA As Integer
        Dim NoImage As Integer
        Dim SiAfaire As Boolean
        Dim CptTotal As Integer
        Dim CptSel As Integer = 0

        EtiStatus1.Text = ""
        EtiStatus2.Text = ""
        CptTotal = WebFct.PointeurUtilisateur.NombreDossiersArmoire(WebFct.PointeurContexte.Armoire_Active)
        If CptTotal = 0 Then
            Exit Sub
        End If
        If WebFct.PointeurArmoire.EnsembleDossiers Is Nothing Then
            Exit Sub
        End If
        If WebFct.PointeurArmoire.EnsembleDossiers.Count = 0 Then
            Exit Sub
        End If
        Dim PointeurUti As Virtualia.Net.Session.ObjetSession = WebFct.PointeurUtilisateur
        If PointeurUti Is Nothing Then
            Exit Sub
        End If

        Noeud.Text = WebFct.PointeurArmoire.NomdelArmoire
        For IndiceA = 0 To 14
            Select Case PointeurUti.CritereValeurArmoire(IndiceA)
                Case Is = ""
                    Exit For
            End Select
            SiAfaire = False
            NoImage = PointeurUti.CritereCouleurArmoire(IndiceA)
            Select Case WebFct.PointeurContexte.Armoire_CouleurEti
                Case Is = ""
                    SiAfaire = True
                Case Else
                    Select Case PointeurUti.VParent.VirUrlImageArmoire(NoImage)
                        Case Is = WebFct.PointeurContexte.Armoire_CouleurEti
                            SiAfaire = True
                    End Select
            End Select
            Select Case SiAfaire
                Case True
                    Dim NewNoeud As TreeNode = New TreeNode(Strings.Replace(PointeurUti.CritereValeurArmoire(IndiceA), ";", ","), PointeurUti.CritereValeurArmoire(IndiceA))
                    NewNoeud.ImageUrl = PointeurUti.VParent.VirUrlImageArmoire(NoImage)
                    NewNoeud.PopulateOnDemand = True
                    NewNoeud.SelectAction = TreeNodeSelectAction.Select
                    Noeud.ChildNodes.Add(NewNoeud)

                    CptSel += NewNoeud.ChildNodes.Count
            End Select
        Next IndiceA
        Select Case WebFct.PointeurContexte.Armoire_CouleurEti
            Case Is = ""
                Dim NewNoeud As TreeNode = New TreeNode("Hors critères", "Hors critères")
                NewNoeud.ImageUrl = PointeurUti.VParent.VirUrlImageArmoire(15)
                NewNoeud.PopulateOnDemand = True
                NewNoeud.SelectAction = TreeNodeSelectAction.Select
                Noeud.ChildNodes.Add(NewNoeud)

                CptSel += NewNoeud.ChildNodes.Count
        End Select

        If EtiStatus1.Text = "" Then
            EtiStatus1.Text = CptTotal.ToString
            Dim Evenement As Virtualia.Systeme.Evenements.DonneeChangeEventArgs
            Evenement = New Virtualia.Systeme.Evenements.DonneeChangeEventArgs(WebFct.PointeurArmoire.NomdelArmoire & Strings.Space(1) & CompteDossiers)
            Saisie_Change(Evenement)
        End If
        EtiStatus1.Text = CptTotal.ToString
        EtiStatus2.Text = CptSel.ToString
    End Sub

    Private Sub FaireListeHieN1(ByVal Noeud As TreeNode)
        Dim IndiceA As Integer
        Dim VImage As String = ""
        Dim SiAfaire As Boolean
        Dim CptSel As Integer = 0
        Dim VirtuelPath As String = ""

        If WebFct.PointeurUtilisateur.NombreDossiersArmoire(WebFct.PointeurContexte.Armoire_Active) = 0 Then
            Exit Sub
        End If

        Dim FichePer As Virtualia.Ressources.Datas.ObjetDossierPER
        Dim Predicat As New Virtualia.Ressources.Predicats.PredicateDossier(WebFct.PointeurContexte.Armoire_Lettre)
        Dim LstDossier As New List(Of Virtualia.Ressources.Datas.ObjetDossierPER)
        LstDossier = WebFct.PointeurArmoire.EnsembleDossiers.FindAll(AddressOf Predicat.RechercherNom)
        If LstDossier.Count = 0 Then
            Exit Sub
        End If

        Call InitialiserCouleurs()

        For IndiceA = 0 To LstDossier.Count - 1
            FichePer = LstDossier.Item(IndiceA)
            SiAfaire = True
            VImage = IconeArmoire(FichePer.CritereIcone)
            Select Case WebFct.PointeurContexte.Armoire_CouleurEti
                Case Is <> ""
                    Select Case WebFct.PointeurContexte.Armoire_CouleurEti
                        Case Is <> VImage
                            SiAfaire = False
                    End Select
            End Select
            Select Case SiAfaire
                Case True
                    Select Case Noeud.ImageUrl
                        Case Is <> VImage
                            SiAfaire = False
                    End Select
            End Select
            Select Case SiAfaire
                Case True
                    Dim NewNoeud As TreeNode = New TreeNode(FichePer.Nom & Space(1) & FichePer.Prenom, FichePer.V_Identifiant.ToString)
                    NewNoeud.ImageUrl = ""
                    NewNoeud.PopulateOnDemand = False
                    NewNoeud.SelectAction = TreeNodeSelectAction.Select
                    Noeud.ChildNodes.Add(NewNoeud)
                    If FichePer.V_Identifiant = WebFct.PointeurContexte.Armoire_Identifiant Then
                        VirtuelPath = NewNoeud.ValuePath
                    End If
                    CptSel += 1
            End Select
        Next IndiceA
        If VirtuelPath <> "" Then
            TreeListeDossier.FindNode(VirtuelPath).Selected = True
        End If
    End Sub

    Private ReadOnly Property IconeArmoire(ByVal Valeur As String) As String
        Get
            Select Case Valeur
                Case Is = ""
                    Return WebFct.PointeurUtilisateur.VParent.VirUrlImageArmoire(15)
                    Exit Property
            End Select
            Dim IndiceI As Integer
            Dim BoutonImage As ImageButton
            For IndiceI = 0 To 15
                BoutonImage = PointeurBouton(IndiceI.ToString)
                If BoutonImage.AlternateText <> "" Then
                    Select Case Strings.InStr(BoutonImage.AlternateText, Valeur)
                        Case Is > 0
                            Return BoutonImage.ImageUrl
                            Exit Property
                    End Select
                End If
            Next IndiceI
            Return WebFct.PointeurUtilisateur.VParent.VirUrlImageArmoire(15)
        End Get
    End Property

    Private Sub InitialiserCouleurs()
        Dim PointeurUti As Virtualia.Net.Session.ObjetSession = WebFct.PointeurUtilisateur
        If PointeurUti Is Nothing Then
            Exit Sub
        End If
        Dim NoRang As Integer
        Dim Bouton As ImageButton
        Dim IndiceImage As Integer
        For IndiceA = 0 To 15
            Bouton = PointeurBouton(IndiceA.ToString)
            NoRang = CInt(Strings.Right(Bouton.ID, Bouton.ID.Length - 6))
            Select Case NoRang
                Case 0 To 15
                    IndiceImage = PointeurUti.CritereCouleurArmoire(NoRang)
                    Select Case PointeurUti.CritereValeurArmoire(NoRang)
                        Case Is <> ""
                            Bouton.ImageUrl = PointeurUti.VParent.VirUrlImageArmoire(IndiceImage)
                            Bouton.AlternateText = PointeurUti.CritereValeurArmoire(NoRang)
                            Bouton.ToolTip = PointeurUti.CritereValeurArmoire(NoRang)
                            Bouton.Visible = True
                        Case Else
                            Bouton.ImageUrl = ""
                            Bouton.AlternateText = ""
                            Bouton.ToolTip = ""
                            Bouton.Visible = False
                    End Select
            End Select
        Next IndiceA
    End Sub

    Private ReadOnly Property PointeurBouton(ByVal Valeur As String) As ImageButton
        Get
            Select Case Valeur
                Case "0"
                    Return BImage0
                Case "1"
                    Return BImage1
                Case "2"
                    Return BImage2
                Case "3"
                    Return BImage3
                Case "4"
                    Return BImage4
                Case "5"
                    Return BImage5
                Case "6"
                    Return BImage6
                Case "7"
                    Return BImage7
                Case "8"
                    Return BImage8
                Case "9"
                    Return BImage9
                Case "10"
                    Return BImage10
                Case "11"
                    Return BImage11
                Case "12"
                    Return BImage12
                Case "13"
                    Return BImage13
                Case "14"
                    Return BImage14
                Case "15"
                    Return BImage15
                Case Else
                    Return BImage0
            End Select
        End Get
    End Property

    Protected Sub CmdRecherche_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles CmdRecherche.Click
        Call InitialiserBoutonsLettre()
        WebFct.PointeurContexte.Armoire_Lettre = VRecherche.Text
        TreeListeDossier.Nodes(0).ChildNodes.Clear()
        Select Case WebFct.PointeurContexte.Armoire_ListeActive
            Case ListeSimple
                Call FaireListeSimple(TreeListeDossier.Nodes(0))
                TreeListeDossier.Nodes(0).ExpandAll()
            Case ListeHie
                Call FaireListeHieN0(TreeListeDossier.Nodes(0))
                TreeListeDossier.Nodes(0).ExpandAll()
        End Select
    End Sub

    Private Sub InitialiserBoutonsLettre()
        Dim IndiceI As Integer = 0
        Dim Ctl As Control
        Do
            Ctl = WebFct.VirWebControle(CadreLettreAM, "Button", IndiceI)
            If Ctl Is Nothing Then
                Exit Do
            End If
            CType(Ctl, ImageButton).ImageUrl = "~/Images/Lettres/" & Strings.Right(CType(Ctl, ImageButton).ID, 1) & ".bmp"
            IndiceI += 1
        Loop
        IndiceI = 0
        Do
            Ctl = WebFct.VirWebControle(CadreLettreNZ, "Button", IndiceI)
            If Ctl Is Nothing Then
                Exit Do
            End If
            CType(Ctl, ImageButton).ImageUrl = "~/Images/Lettres/" & Strings.Right(CType(Ctl, ImageButton).ID, 1) & ".bmp"
            IndiceI += 1
        Loop
    End Sub

    Private Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        WebFct = New Virtualia.Net.WebFonctions(Me, 0)
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        If WebFct.PointeurUtilisateur Is Nothing Then
            WebFct = New Virtualia.Net.WebFonctions(Me, 0)
        End If
        Dim I As Integer
        For I = 0 To DropDownArmoire.Items.Count - 1
            Select Case CShort(DropDownArmoire.Items(I).Value)
                Case Is = WebFct.PointeurContexte.Armoire_Active
                    DropDownArmoire.Items(I).Selected = True
                    If WebFct.PointeurContexte.Armoire_Active = Personnalisee Then
                        DropDownArmoire.ForeColor = Drawing.Color.Gray
                    Else
                        DropDownArmoire.ForeColor = WebFct.ConvertCouleur("#142425")
                    End If
                    Exit For
            End Select
        Next I
        VRecherche.Text = WebFct.PointeurContexte.Armoire_Lettre
    End Sub

    Protected Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
        WebFct.Dispose()
    End Sub

    Private Sub ObtenirPanel()
        Dim PointeurUti As Virtualia.Net.Session.ObjetSession = WebFct.PointeurUtilisateur
        If PointeurUti Is Nothing Then
            Exit Sub
        End If

        If PointeurUti.NombreDossiersArmoire(LePanel) > 0 Then
            Exit Sub
        End If

        Dim ArmComplete As Virtualia.Net.Datas.ObjetArmoire
        Dim PanelW As List(Of Virtualia.Ressources.Datas.ItemSelection)
        Dim PointeurClone As Virtualia.Ressources.Datas.ObjetDossierPER
        Dim PDossier As Virtualia.Ressources.Datas.ObjetDossierPER
        Dim ListeW As New List(Of Virtualia.Ressources.Datas.ObjetDossierPER)
        Dim ListeRes As List(Of Virtualia.Ressources.Datas.ObjetDossierPER)
        Dim K As Integer

        ArmComplete = PointeurUti.ItemArmoire(0)
        PanelW = WebFct.PointeurContexte.ListePanel
        WebFct.PointeurContexte.Armoire_Active = LePanel

        For K = 0 To PanelW.Count - 1
            PDossier = ArmComplete.EnsembleDossiers.Find(Function(Recherche) Recherche.V_Identifiant = PanelW.Item(K).Identifiant)
            If PDossier IsNot Nothing Then
                PointeurClone = New Virtualia.Ressources.Datas.ObjetDossierPER(System.Configuration.ConfigurationManager.AppSettings("WebService.VirtualiaServeur"), PointeurUti.VParent.VirModele)
                PointeurClone = PDossier
                ListeW.Add(PointeurClone)
            End If
        Next K
        ListeRes = New List(Of Virtualia.Ressources.Datas.ObjetDossierPER)
        ListeRes = (From instance In ListeW Select instance Where instance.V_Identifiant > 0 Order By instance.Nom Ascending, instance.Prenom Ascending).ToList
        WebFct.PointeurArmoire.EnsembleDossiers = ListeRes
    End Sub

    Protected Sub Cmd00_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Cmd00.Click
        'Sélectionner une armoire dans l'album
        Dim Evenement As Virtualia.Systeme.Evenements.AppelAlbumClickEventArgs
        Evenement = New Virtualia.Systeme.Evenements.AppelAlbumClickEventArgs(VI.PointdeVue.PVueApplicatif, VI.OptionInfo.DicoCmc)
        VAlbum_Click(Evenement)
    End Sub

    ' Cmd01 Nouveau Dossier - Ouvre la fenêtre FrmNouveauDossierPER

    Protected Sub Cmd03_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Cmd03.Click
        'Supprimer un dossier
        If WebFct.PointeurContexte.Armoire_Identifiant = 0 Then
            Exit Sub
        End If
        Dim Predicat As New Virtualia.Ressources.Predicats.PredicateDossier(WebFct.PointeurContexte.Armoire_Identifiant)
        Dim LstDossier As New List(Of Virtualia.Ressources.Datas.ObjetDossierPER)
        Dim TitreMsg As String = "Supprimer le dossier"
        Dim ContenuMsg As String = "Cette opération va supprimer définitivement le dossier"

        LstDossier = WebFct.PointeurArmoire.EnsembleDossiers.FindAll(AddressOf Predicat.RechercherIdentifiant)
        If LstDossier.Count = 0 Then
            Exit Sub
        End If
        ContenuMsg &= " de " & LstDossier.Item(0).Nom & Strings.Space(1) & LstDossier.Item(0).Prenom
        ContenuMsg &= Strings.Chr(13) & Strings.Chr(10) & "Confirmez-vous la suppression ?"
        Dim Evenement As Virtualia.Systeme.Evenements.MessageSaisieEventArgs
        Evenement = New Virtualia.Systeme.Evenements.MessageSaisieEventArgs("SuppDossier", 1, "", "Oui;Non", TitreMsg, ContenuMsg)
        VMessageDialog(Evenement)
    End Sub

    Protected Sub Cmd05_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Cmd05.Click
        'Organiser en fonction du critère de couleur
        Select Case WebFct.PointeurContexte.Armoire_ListeActive
            Case Is = ListeSimple
                WebFct.PointeurContexte.Armoire_ListeActive = ListeHie
            Case Is = ListeHie
                WebFct.PointeurContexte.Armoire_ListeActive = ListeSimple
        End Select
        TreeListeDossier.Nodes(0).ChildNodes.Clear()
        Select Case WebFct.PointeurContexte.Armoire_ListeActive
            Case ListeSimple
                Call FaireListeSimple(TreeListeDossier.Nodes(0))
                TreeListeDossier.Nodes(0).ExpandAll()
            Case ListeHie
                Call FaireListeHieN0(TreeListeDossier.Nodes(0))
                TreeListeDossier.Nodes(0).ExpandAll()
        End Select
    End Sub

    Protected Sub Cmd08_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Cmd08.Click
        'Sélectionner une liste à éditer dans l'album
        Dim Evenement As Virtualia.Systeme.Evenements.AppelAlbumClickEventArgs
        Evenement = New Virtualia.Systeme.Evenements.AppelAlbumClickEventArgs(VI.PointdeVue.PVueApplicatif, VI.OptionInfo.DicoExport)
        VAlbum_Click(Evenement)
    End Sub

End Class
