﻿Option Strict On
Option Explicit On
Option Compare Text
Imports VI = Virtualia.Systeme.Constantes
Partial Class Controles_ListeReferences
    Inherits System.Web.UI.UserControl
    Public Delegate Sub ValeurSelectionneeEventHandler(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.ValeurSelectionneeEventArgs)
    Public Event ValeurSelectionnee As ValeurSelectionneeEventHandler
    Public Delegate Sub EventHandler(ByVal sender As Object, ByVal e As EventArgs)
    Public Event RetourEventHandler As EventHandler
    Public Delegate Sub EventNew(ByVal sender As Object, ByVal e As EventArgs)
    Private WebFct As Virtualia.Net.WebFonctions
    Private WsNomState As String = "DicoRef"
    '
    Private WsVirtuelPath As String = ""

    Protected Overridable Sub VSelection(ByVal e As Virtualia.Systeme.Evenements.ValeurSelectionneeEventArgs)
        RaiseEvent ValeurSelectionnee(Me, e)
    End Sub

    Public Property SiListeMenuVisible() As Boolean
        Get
            If HSelMode.Value = "0" Then
                Return False
            Else
                Return True
            End If
        End Get
        Set(ByVal value As Boolean)
            MenuRef.Visible = value
            If value = False Then
                Dim I As Integer
                For I = 0 To TreeDicoRef.Nodes.Count - 1
                    TreeDicoRef.Nodes(I).PopulateOnDemand = False
                Next I
                CommandeBlanc.Visible = True
                CommandeRetour.Visible = True
                CadreCommandes.Visible = False
                HSelMode.Value = "0"
            Else
                HSelMode.Value = "1"
            End If
        End Set
    End Property

    Public Property SiPanelTitreVisible() As Boolean
        Get
            Return PanelTitre.Visible
        End Get
        Set(ByVal value As Boolean)
            PanelTitre.Visible = value
            CommandeBlanc.Visible = value
            CommandeRetour.Visible = value
        End Set
    End Property

    Public Property V_PointdeVue() As Short
        Get
            Return WebFct.PointeurContexte.SysRef_PointdeVueInverse
        End Get
        Set(ByVal value As Short)
            If WebFct.PointeurContexte.SysRef_PointdeVueInverse <> value Then
                VRecherche.Text = ""
                WebFct.PointeurContexte.SysRef_Lettre = ""
            End If
            WebFct.PointeurContexte.SysRef_PointdeVueInverse = value
            WebFct.PointeurContexte.SysRef_Duo_PointdeVueInverse(0) = 0
            WebFct.PointeurContexte.SysRef_Duo_PointdeVueInverse(1) = 0
            If value = VI.PointdeVue.PVueGeneral Then
                RadioV1.Visible = False
                RadioV2.Visible = True
            Else
                RadioV1.Visible = True
                RadioV2.Visible = False
            End If
        End Set
    End Property

    Public Property V_NomTable() As String
        Get
            Return WebFct.PointeurContexte.SysRef_NomTable
        End Get
        Set(ByVal value As String)
            If WebFct.PointeurContexte.SysRef_NomTable <> value Then
                VRecherche.Text = ""
                WebFct.PointeurContexte.SysRef_Lettre = ""
            End If
            WebFct.PointeurContexte.SysRef_NomTable = value
            WebFct.PointeurContexte.SysRef_Duo_NomTable(0) = ""
            WebFct.PointeurContexte.SysRef_Duo_NomTable(1) = ""
        End Set
    End Property

    Public WriteOnly Property V_DuoTable(ByVal PtdeVue_1 As Short, ByVal Nomtable_1 As String, ByVal PtdeVue_2 As Short) As String
        Set(ByVal value As String)
            WebFct.PointeurContexte.SysRef_Duo_PointdeVueInverse(0) = PtdeVue_1
            WebFct.PointeurContexte.SysRef_Duo_NomTable(0) = Nomtable_1
            WebFct.PointeurContexte.SysRef_Duo_PointdeVueInverse(1) = PtdeVue_2
            WebFct.PointeurContexte.SysRef_Duo_NomTable(1) = value
            VRecherche.Text = ""
            WebFct.PointeurContexte.SysRef_Lettre = ""
        End Set
    End Property

    Public WriteOnly Property V_Appelant(ByVal NoObjet As Short) As String
        Set(ByVal value As String)
            Call InitialiserEtiquettes()
            WebFct.PointeurContexte.SysRef_IDAppelant = value
            WebFct.PointeurContexte.SysRef_ObjetAppelant = NoObjet
            HSelDateValeur.Value = ""

        End Set
    End Property

    Public WriteOnly Property V_Appelant(ByVal NoObjet As Short, ByVal DateValeur As String) As String
        Set(ByVal value As String)
            Call InitialiserEtiquettes()
            WebFct.PointeurContexte.SysRef_IDAppelant = value
            WebFct.PointeurContexte.SysRef_ObjetAppelant = NoObjet
            HSelDateValeur.Value = DateValeur

        End Set
    End Property

    Public Property V_SiTriOrganise() As Boolean
        Get
            If HSelRadio.Value = "1" Then
                Return True
            Else
                Return False
            End If
        End Get
        Set(ByVal value As Boolean)
            If value = True Then
                HSelRadio.Value = "1"
            Else
                HSelRadio.Value = "0"
            End If
        End Set
    End Property

    Protected Sub TreeDicoRef_TreeNodePopulate(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.TreeNodeEventArgs) Handles TreeDicoRef.TreeNodePopulate
        If HSelMode.Value = "0" Then
            Exit Sub
        End If
        TreeDicoRef.MaxDataBindDepth = 2
        TreeDicoRef.ImageSet = System.Web.UI.WebControls.TreeViewImageSet.BulletedList4
        If e.Node.ChildNodes.Count = 0 Then
            Select Case e.Node.Depth
                Case Is = 0
                    Select Case e.Node.Value
                        Case Is = "General"
                            Call ChargerMenuGeneral(e.Node)
                        Case Is = "Statutaire"
                            Call ChargerMenuStatut(e.Node)
                        Case Is = "Travail"
                            Call ChargerMenuTempsTravail(e.Node)
                        Case Is = "Organisation"
                            Call ChargerMenuOrganisation(e.Node)
                        Case Is = "Formation"
                            Call ChargerMenuFormation(e.Node)
                        Case Is = "Paie"
                            Call ChargerMenuPaie(e.Node)
                    End Select
                    e.Node.Expand()
                Case Is = 1
                    Select Case e.Node.Text
                        Case Is = "Activités - Fonctions"
                            Call ChargerSousMenus(e.Node, VI.PointdeVue.PVueFonctionSupport)
                        Case Is = "Activités - Mesures"
                            Call ChargerSousMenus(e.Node, VI.PointdeVue.PVueActivite)
                        Case Is = "Cycles de base"
                            Call ChargerSousMenus(e.Node, VI.PointdeVue.PVueBaseHebdo)
                        Case Is = "Cycles de travail"
                            Call ChargerSousMenus(e.Node, VI.PointdeVue.PVueCycle)
                        Case Is = "Emplois budgétaires"
                            Call ChargerSousMenus(e.Node, VI.PointdeVue.PVuePosteBud)
                        Case Is = "Grades"
                            Call ChargerSousMenus(e.Node, VI.PointdeVue.PVueGrades)
                        Case Is = "Missions"
                            Call ChargerSousMenus(e.Node, VI.PointdeVue.PVueMission)
                        Case Is = "Postes fonctionnels"
                            Call ChargerSousMenus(e.Node, VI.PointdeVue.PVuePosteFct)
                        Case Is = "Stages et sessions"
                            Call ChargerSousMenus(e.Node, VI.PointdeVue.PVueFormation)
                        Case Is = "Tables générales"
                            Call ChargerListeTablesGenerales(e.Node)
                    End Select
                    e.Node.Collapse()
            End Select
        End If
    End Sub

    Protected Sub TreeDicoRef_SelectedNodeChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles TreeDicoRef.SelectedNodeChanged
        Call SelectSurDicoRef(CType(sender, System.Web.UI.WebControls.TreeView).SelectedNode)
    End Sub

    Protected Sub TreeListeRef_SelectedNodeChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles TreeListeRef.SelectedNodeChanged
        Dim Tableaudata(0) As String
        Dim SelPVue As Short
        Dim SelIde As Integer
        Dim SelCode As String
        Dim SelText As String
        Dim Tampon As ArrayList
        Dim TableauSel(0) As String
        Dim I As Integer

        Tableaudata = Strings.Split(CType(sender, System.Web.UI.WebControls.TreeView).SelectedNode.Value, VI.Tild, -1)
        If Not (IsNumeric(Tableaudata(0))) Then
            Exit Sub
        End If
        If CType(sender, System.Web.UI.WebControls.TreeView).SelectedNode.SelectAction = _
               TreeNodeSelectAction.SelectExpand Then
            Exit Sub
        End If

        SelIde = CInt(Tableaudata(0))
        SelText = CType(sender, System.Web.UI.WebControls.TreeView).SelectedNode.Text
        SelCode = Tableaudata(2)
        SelPVue = CShort(Tableaudata(3))

        Select Case SelPVue
            Case VI.PointdeVue.PVueGrades
                Tampon = New ArrayList
                TableauSel = Strings.Split(SelCode, VI.PointVirgule, -1)
                For I = 0 To TableauSel.Count - 1
                    Tampon.Add(TableauSel(I))
                Next I
                WebFct.PointeurContexte.TsTampon = Tampon

            Case VI.PointdeVue.PVueGrilles
                Tampon = New ArrayList
                TableauSel = Strings.Split(SelCode, VI.PointVirgule, -1)
                For I = 0 To TableauSel.Count - 1
                    Tampon.Add(TableauSel(I))
                Next I
                WebFct.PointeurContexte.TsTampon = Tampon

                SelText = CType(sender, System.Web.UI.WebControls.TreeView).SelectedNode.Parent.Parent.Text

            Case VI.PointdeVue.PVueFormation
                SelText = CType(sender, System.Web.UI.WebControls.TreeView).SelectedNode.Parent.Text

        End Select
        HSelValeur.Value = SelText

        Dim Evenement As Virtualia.Systeme.Evenements.ValeurSelectionneeEventArgs
        If HSelMode.Value = "1" Then
            Evenement = New Virtualia.Systeme.Evenements.ValeurSelectionneeEventArgs("Sysref", _
                    WebFct.PointeurContexte.SysRef_ObjetAppelant, SelPVue, V_NomTable, SelIde, SelText, "Maj")
        Else
            Evenement = New Virtualia.Systeme.Evenements.ValeurSelectionneeEventArgs(WebFct.PointeurContexte.SysRef_IDAppelant, _
                WebFct.PointeurContexte.SysRef_ObjetAppelant, SelPVue, V_NomTable, SelIde, SelText, SelCode)
        End If

        VSelection(Evenement)

    End Sub

    Private Sub SelectSurDicoRef(ByVal Noeud As TreeNode)
        Dim Tableaudata(0) As String
        Dim SelPvue As Short
        Dim SelTable As String = ""
        Dim SelText As String
        Tableaudata = Strings.Split(Noeud.Value, VI.Tild, -1)
        If Not (IsNumeric(Tableaudata(0))) Then
            Exit Sub
        End If
        VRecherche.Text = ""
        WebFct.PointeurContexte.SysRef_Lettre = ""
        SelPvue = CShort(Tableaudata(0))
        If Tableaudata.Count > 1 Then
            SelTable = Tableaudata(1)
        End If
        Noeud.PopulateOnDemand = False
        SelText = Noeud.Text
        V_NomTable = SelTable
        V_PointdeVue = SelPvue
        V_Appelant(0) = ""

    End Sub

    Protected Sub VRecherche_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles VRecherche.TextChanged
        Call InitialiserBoutons()

        If WebFct.PointeurContexte.SysRef_Lettre = "" Then
            ButtonTout.BackColor = WebFct.ConvertCouleur("#3D3421")
            ButtonTout.ForeColor = WebFct.ConvertCouleur("#BFB8AB")
        End If
        WebFct.PointeurContexte.SysRef_Lettre = VRecherche.Text

        HSelRadio.Value = "0"
        RadioV0.Checked = True

    End Sub

    Protected Sub BoutonLettre_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButtonA.Click, ButtonB.Click, ButtonC.Click, ButtonD.Click, ButtonE.Click, ButtonF.Click, ButtonG.Click, ButtonH.Click, ButtonI.Click, ButtonJ.Click, ButtonK.Click, ButtonL.Click, ButtonM.Click, ButtonN.Click, ButtonO.Click, ButtonP.Click, ButtonQ.Click, ButtonR.Click, ButtonS.Click, ButtonT.Click, ButtonU.Click, ButtonV.Click, ButtonW.Click, ButtonX.Click, ButtonY.Click, ButtonZ.Click, ButtonTout.Click
        Call InitialiserBoutons()

        If CType(sender, Button).ID = "ButtonTout" Then
            WebFct.PointeurContexte.SysRef_Lettre = ""
        Else
            WebFct.PointeurContexte.SysRef_Lettre = Strings.Right(CType(sender, Button).ID, 1)
        End If
        CType(sender, Button).BackColor = WebFct.ConvertCouleur("#3D3421")
        CType(sender, Button).ForeColor = WebFct.ConvertCouleur("#BFB8AB")
        VRecherche.Text = WebFct.PointeurContexte.SysRef_Lettre

        HSelRadio.Value = "0"
        RadioV0.Checked = True

    End Sub

    Private Sub FaireListeSimple(ByVal Noeud As TreeNode)
        If V_PointdeVue = VI.PointdeVue.PVueGeneral And V_NomTable = "" Then
            Exit Sub
        Else
            If V_NomTable = "" Then
                Try
                    Noeud.Text = WebFct.PointeurUtilisateur.VParent.VirModele.Item(V_PointdeVue - 1).Intitule
                Catch ex As Exception
                    Exit Sub
                End Try
            Else
                Noeud.Text = V_NomTable
            End If
        End If

        '**** Listes adaptées à un point de vue
        Select Case V_PointdeVue
            Case Is = VI.PointdeVue.PVueGrades
                Call FaireListeGrades(Noeud)
                Exit Sub
            Case Is = VI.PointdeVue.PVuePosteBud
                Call FaireListePosteBud(Noeud)
                Exit Sub
            Case Is = VI.PointdeVue.PVueFormation
                Call FaireListeStages(Noeud)
                Exit Sub
            Case Is = VI.PointdeVue.PVueCommission
                Call FaireListeCommissions(Noeud)
                Exit Sub
            Case Is = VI.PointdeVue.PVueMission
                Call FaireListeMissions(Noeud)
                Exit Sub
            Case Is = VI.PointdeVue.PVuePosteFct
                Call FaireListePostesFonctionnels(Noeud)
                Exit Sub
            Case Is = VI.PointdeVue.PVueLolf
                Call FaireListeLolf(Noeud)
                Exit Sub
            Case Is = VI.PointdeVue.PVueBaseHebdo, VI.PointdeVue.PVueCycle, VI.PointdeVue.PVueFonctionSupport, VI.PointdeVue.PVueActivite
                Call FaireListeParEtablissement(Noeud)
                Exit Sub
            Case Is = VI.PointdeVue.PVueEtablissement, VI.PointdeVue.PVueItineraire, VI.PointdeVue.PVuePays, VI.PointdeVue.PVueInterface
                Call FaireListeClassique(Noeud)
                Exit Sub
        End Select

        Dim IndiceA As Integer
        Dim TableauObjet(0) As String
        Dim Tableaudata(0) As String
        Dim VImage As String = ""
        Dim CptSel As Integer = 0
        Dim FicheRef As Virtualia.Ressources.Datas.ObjetDossierREF
        Dim SiAfaire As Boolean = False
        Dim Predicat As Virtualia.Ressources.Predicats.PredicateSysRef
        Dim LstFiches As List(Of Virtualia.Ressources.Datas.ObjetDossierREF)

        If WebFct.PointeurContexte.SysRef_Listes.Count = 0 Then
            SiAfaire = True
        Else
            Predicat = New Virtualia.Ressources.Predicats.PredicateSysRef(Noeud.Text, "")
            LstFiches = New List(Of Virtualia.Ressources.Datas.ObjetDossierREF)
            LstFiches = WebFct.PointeurContexte.SysRef_Listes.FindAll(AddressOf Predicat.RechercherTable)
            If LstFiches.Count = 0 Then
                SiAfaire = True
            End If
            Predicat = Nothing
            LstFiches = Nothing
        End If

        If SiAfaire = True Then
            Select Case V_PointdeVue
                Case 99 'Point de vue Système
                    Select Case Noeud.Text
                        Case Is = "$CatégorieRH"
                            TableauObjet = WebFct.PointeurGlobal.TabledesCategories
                    End Select
                Case VI.PointdeVue.PVueGeneral
                    TableauObjet = Strings.Split(WebFct.PointeurUtilisateur.LireTableGeneraleSimple(Noeud.Text), VI.SigneBarre, -1)
                Case Else
                    TableauObjet = Strings.Split(WebFct.PointeurUtilisateur.LireTableSysReference(V_PointdeVue), VI.SigneBarre, -1)
            End Select
            For IndiceA = 0 To TableauObjet.Count - 1
                If TableauObjet(IndiceA) = "" Then
                    Exit For
                End If
                FicheRef = New Virtualia.Ressources.Datas.ObjetDossierREF(System.Configuration.ConfigurationManager.AppSettings("WebService.VirtualiaServeur"), _
                               WebFct.PointeurGlobal.VirModele, V_PointdeVue, Noeud.Text, TableauObjet(IndiceA))
                WebFct.PointeurContexte.SysRef_Listes.Add(FicheRef)
            Next IndiceA
        End If

        Predicat = New Virtualia.Ressources.Predicats.PredicateSysRef(Noeud.Text, WebFct.PointeurContexte.SysRef_Lettre)
        LstFiches = New List(Of Virtualia.Ressources.Datas.ObjetDossierREF)
        LstFiches = WebFct.PointeurContexte.SysRef_Listes.FindAll(AddressOf Predicat.RechercherLettre)
        If LstFiches.Count = 0 Then
            EtiStatus1.Text = "Aucune référence"
            Exit Sub
        End If

        Dim NewNoeud As TreeNode

        For IndiceA = 0 To LstFiches.Count - 1
            FicheRef = LstFiches.Item(IndiceA)
            VImage = "~/Images/Armoire/FicheJaune.bmp"
            Select Case V_PointdeVue
                Case VI.PointdeVue.PVueGeneral
                    If HSelRadio.Value = "0" Then
                        NewNoeud = New TreeNode(FicheRef.Valeur, _
                                        FicheRef.V_Identifiant & VI.Tild & FicheRef.Valeur & VI.Tild & _
                                        FicheRef.Code & VI.Tild & FicheRef.V_PointdeVue)
                    Else
                        NewNoeud = New TreeNode(FicheRef.Code & Strings.Space(5) & FicheRef.Valeur, _
                                       FicheRef.V_Identifiant & VI.Tild & FicheRef.Valeur & VI.Tild & _
                                       FicheRef.Code & VI.Tild & FicheRef.V_PointdeVue)
                    End If
                Case Else
                    NewNoeud = New TreeNode(FicheRef.Valeur, _
                                       FicheRef.V_Identifiant & VI.Tild & FicheRef.Valeur & VI.Tild & _
                                       FicheRef.Code & VI.Tild & FicheRef.V_PointdeVue)
            End Select
            NewNoeud.ImageUrl = VImage
            NewNoeud.PopulateOnDemand = False
            NewNoeud.SelectAction = TreeNodeSelectAction.Select
            Noeud.ChildNodes.Add(NewNoeud)
            If HSelValeur.Value <> "" Then
                If NewNoeud.Text = HSelValeur.Value Then
                    WsVirtuelPath = NewNoeud.ValuePath
                End If
            End If
            CptSel += 1
        Next IndiceA

        Select Case CptSel
            Case Is = 1
                EtiStatus1.Text = "Une référence"
            Case Else
                EtiStatus1.Text = CptSel.ToString & " références"
        End Select

        If WsVirtuelPath <> "" Then
            Select Case HSelMode.Value
                Case Is = "0"
                    TreeListeRef.FindNode(WsVirtuelPath).Selected = False
                Case Else
                    TreeListeRef.FindNode(WsVirtuelPath).Selected = True
            End Select
            TreeListeRef.FindNode(WsVirtuelPath).Parent.Expanded = True
        End If

        Dim NoeudRecherche As TreeNode
        Select Case V_PointdeVue
            Case VI.PointdeVue.PVueGeneral
                NoeudRecherche = AlleraInfoDico(0, False, V_NomTable)
            Case Else
                NoeudRecherche = AlleraInfoDico(0, True, "")
        End Select
        If NoeudRecherche IsNot Nothing Then
            EtiSecondaire.Text = NoeudRecherche.Text
            EtiPrincipal.Text = NoeudRecherche.Parent.Text
        End If
    End Sub

    Private Sub FaireListeMultiple()
        Dim I As Integer
        Dim NewNoeud As TreeNode
        TreeListeRef.Nodes.Clear()

        If WebFct.PointeurContexte.SysRef_Duo_PointdeVueInverse(0) = 0 Then
            NewNoeud = New TreeNode(V_NomTable, V_NomTable)
            NewNoeud.PopulateOnDemand = False
            NewNoeud.ImageUrl = "~/Images/Armoire/VertFonceOuvert16.bmp"
            NewNoeud.SelectAction = TreeNodeSelectAction.Expand
            TreeListeRef.Nodes.Add(NewNoeud)
            Call FaireListeSimple(TreeListeRef.Nodes(0))
            TreeListeRef.Nodes(0).Expand()
        Else
            For I = 0 To 1
                WebFct.PointeurContexte.SysRef_PointdeVueInverse = WebFct.PointeurContexte.SysRef_Duo_PointdeVueInverse(I)
                WebFct.PointeurContexte.SysRef_NomTable = WebFct.PointeurContexte.SysRef_Duo_NomTable(I)
                NewNoeud = New TreeNode(V_NomTable, V_NomTable)
                NewNoeud.PopulateOnDemand = False
                NewNoeud.ImageUrl = "~/Images/Armoire/VertFonceOuvert16.bmp"
                NewNoeud.SelectAction = TreeNodeSelectAction.Expand
                TreeListeRef.Nodes.Add(NewNoeud)
                Call FaireListeSimple(TreeListeRef.Nodes(I))
                TreeListeRef.Nodes(I).Expand()
            Next I
        End If
    End Sub

    Protected Sub CmdAllerA_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles CmdAllerA.Click
        If VAllerA.Text = "" Or VAllerA.Text = "*" Then
            Exit Sub
        End If
        Dim Noeud As TreeNode
        Dim CacheMaj As ArrayList
        Dim ValCourante As String = ""
        Dim NoCourant As Integer = -1

        If Me.ViewState(WsNomState) IsNot Nothing Then
            CacheMaj = CType(Me.ViewState(WsNomState), ArrayList)
            If CacheMaj(0) IsNot Nothing Then
                ValCourante = CacheMaj(0).ToString
            End If
            If CacheMaj(1) IsNot Nothing Then
                NoCourant = CInt(CacheMaj(1))
            End If
            If ValCourante <> VAllerA.Text Then
                NoCourant = -1
            End If
            Me.ViewState.Remove(WsNomState)
            CacheMaj.Clear()
        End If
        NoCourant += 1
        CacheMaj = New ArrayList(1)
        CacheMaj.Add(VAllerA.Text)
        CacheMaj.Add(NoCourant)
        Me.ViewState.Add(WsNomState, CacheMaj)

        Noeud = AlleraInfoDico(NoCourant, False, VAllerA.Text)
        If Noeud IsNot Nothing Then
            Noeud.Select()
            Call SelectSurDicoRef(TreeDicoRef.FindNode(Noeud.ValuePath))
        End If
    End Sub

    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        WebFct = New Virtualia.Net.WebFonctions(Me, 0)
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        CelluleDroite.BackColor = WebFct.CouleurCharte(2, "SysRef_Base")
        CelluleDroite.BorderColor = WebFct.CouleurCharte(2, "SysRef_Bordure")

        EtiTitre.BackColor = WebFct.CouleurCharte(2, "SysRef_Titre")
        EtiTitre.ForeColor = WebFct.CouleurCharte(2, "SysRef_ForeColor")
        EtiTitre.BorderColor = WebFct.CouleurCharte(2, "SysRef_Bordure")

        EtiPrincipal.BackColor = WebFct.CouleurCharte(2, "SysRef_Sous-Titre")
        EtiPrincipal.ForeColor = WebFct.CouleurCharte(2, "Etiquette_ForeColor")
        EtiPrincipal.BorderColor = WebFct.CouleurCharte(2, "SysRef_Bordure")
        EtiSecondaire.BackColor = WebFct.CouleurCharte(2, "SysRef_Sous-Titre")
        EtiSecondaire.ForeColor = WebFct.CouleurCharte(2, "Etiquette_ForeColor")
        EtiSecondaire.BorderColor = WebFct.CouleurCharte(2, "SysRef_Bordure")
        EtiRecherche.BackColor = WebFct.CouleurCharte(2, "SysRef_Sous-Titre")
        EtiRecherche.ForeColor = WebFct.CouleurCharte(2, "Etiquette_ForeColor")
        EtiRecherche.BorderColor = WebFct.CouleurCharte(2, "SysRef_Bordure")

        VRecherche.BackColor = WebFct.CouleurCharte(2, "SysRef_Text")
        VRecherche.ForeColor = WebFct.CouleurCharte(2, "Etiquette_ForeColor")
        VRecherche.BorderColor = WebFct.CouleurCharte(2, "SysRef_Bordure")
        EtiStatus1.BackColor = WebFct.CouleurCharte(2, "SysRef_Text")
        EtiStatus1.ForeColor = WebFct.CouleurCharte(2, "Etiquette_ForeColor")
        EtiStatus1.BorderColor = WebFct.CouleurCharte(2, "SysRef_Bordure")

        Call FaireListeMultiple()
    End Sub

    Protected Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
        WebFct.Dispose()
    End Sub

    Private Function AlleraInfoDico(ByVal Index As Integer, ByVal SiPtdevue As Boolean, ByVal Valeur As String) As TreeNode
        Dim Tableaudata(0) As String
        Dim Noeud0 As TreeNode
        Dim NoeudPvue As TreeNode
        Dim NoeudTable As TreeNode
        Dim IndiceA As Integer
        Dim IndiceB As Integer
        Dim IndiceC As Integer
        Dim Lg As Integer = Valeur.Length
        Dim Rec As Integer = 0
        Dim IndexRec As Integer = 0

        For IndiceA = 0 To TreeDicoRef.Nodes.Count - 1
            Noeud0 = TreeDicoRef.Nodes.Item(IndiceA)
            Select Case SiPtdevue
                Case False
                    Select Case Strings.Left(Valeur, 1)
                        Case Is = "*"
                            If Strings.InStr(Noeud0.Text, Strings.Right(Valeur, Lg - 1)) > 0 Then
                                If IndexRec = Index Then
                                    Return Noeud0
                                    Exit Function
                                End If
                                IndexRec += 1
                            End If
                        Case Else
                            Rec = Noeud0.Text.Length
                            If Rec >= Lg Then
                                If Strings.Left(Noeud0.Text, Lg) = Valeur Then
                                    If IndexRec = Index Then
                                        Return Noeud0
                                        Exit Function
                                    End If
                                    IndexRec += 1
                                End If
                            End If
                    End Select
            End Select
            Noeud0.Expand()
            For IndiceB = 0 To Noeud0.ChildNodes.Count - 1
                NoeudPvue = Noeud0.ChildNodes.Item(IndiceB)
                Select Case SiPtdevue
                    Case True
                        Tableaudata = Strings.Split(NoeudPvue.Value, VI.Tild, -1)
                        If IsNumeric(Tableaudata(0)) Then
                            If CShort(Tableaudata(0)) = V_PointdeVue Then
                                Return NoeudPvue
                                Exit Function
                            End If
                        End If
                    Case False
                        Select Case Strings.Left(Valeur, 1)
                            Case Is = "*"
                                If Strings.InStr(NoeudPvue.Text, Strings.Right(Valeur, Lg - 1)) > 0 Then
                                    If IndexRec = Index Then
                                        Return NoeudPvue
                                        Exit Function
                                    End If
                                    IndexRec += 1
                                End If
                            Case Else
                                Rec = NoeudPvue.Text.Length
                                If Rec >= Lg Then
                                    If Strings.Left(NoeudPvue.Text, Lg) = Valeur Then
                                        If IndexRec = Index Then
                                            Return NoeudPvue
                                            Exit Function
                                        End If
                                        IndexRec += 1
                                    End If
                                End If
                        End Select
                End Select
                NoeudPvue.Expand()
                For IndiceC = 0 To NoeudPvue.ChildNodes.Count - 1
                    NoeudTable = NoeudPvue.ChildNodes.Item(IndiceC)
                    Select Case SiPtdevue
                        Case False
                            Select Case Strings.Left(Valeur, 1)
                                Case Is = "*"
                                    If Strings.InStr(NoeudTable.Text, Strings.Right(Valeur, Lg - 1)) > 0 Then
                                        If IndexRec = Index Then
                                            Return NoeudTable
                                            Exit Function
                                        End If
                                        IndexRec += 1
                                    End If
                                Case Else
                                    Rec = NoeudTable.Text.Length
                                    If Rec >= Lg Then
                                        If Strings.Left(NoeudTable.Text, Lg) = Valeur Then
                                            If IndexRec = Index Then
                                                Return NoeudTable
                                                Exit Function
                                            End If
                                            IndexRec += 1
                                        End If
                                    End If
                            End Select
                    End Select
                Next IndiceC
                NoeudPvue.Collapse()
            Next IndiceB
        Next IndiceA
        Return Nothing
    End Function

    Private Sub InitialiserBoutons()
        Dim IndiceI As Integer = 0
        Dim Ctl As Control
        Do
            Ctl = WebFct.VirWebControle(CadreLettre, "Button", IndiceI)
            If Ctl Is Nothing Then
                Exit Do
            End If
            CType(Ctl, Button).BackColor = WebFct.ConvertCouleur("#BFB8AB")
            CType(Ctl, Button).ForeColor = Drawing.Color.Gray
            IndiceI += 1
        Loop
    End Sub

    Private Sub InitialiserEtiquettes()
        Select Case V_PointdeVue
            Case Is = VI.PointdeVue.PVueGeneral
                EtiPrincipal.Text = "Règlementaire général"
                EtiSecondaire.Text = "Tables générales"
        End Select
    End Sub

    Private Sub ChargerMenuStatut(ByVal Noeud As TreeNode)
        Dim NewNoeud As TreeNode

        NewNoeud = New TreeNode("Statuts", VI.PointdeVue.PVueGeneral & VI.Tild & "Statut")
        NewNoeud.PopulateOnDemand = False
        NewNoeud.SelectAction = TreeNodeSelectAction.Select
        Noeud.ChildNodes.Add(NewNoeud)

        NewNoeud = New TreeNode("Positions", VI.PointdeVue.PVuePosition & VI.Tild)
        NewNoeud.PopulateOnDemand = False
        NewNoeud.SelectAction = TreeNodeSelectAction.Select
        Noeud.ChildNodes.Add(NewNoeud)

        NewNoeud = New TreeNode("Grades", VI.PointdeVue.PVueGrades & VI.Tild)
        NewNoeud.PopulateOnDemand = True
        NewNoeud.SelectAction = TreeNodeSelectAction.Expand
        Noeud.ChildNodes.Add(NewNoeud)

        NewNoeud = New TreeNode("Emplois", VI.PointdeVue.PVueMetier & VI.Tild)
        NewNoeud.PopulateOnDemand = False
        NewNoeud.SelectAction = TreeNodeSelectAction.Select
        Noeud.ChildNodes.Add(NewNoeud)

        NewNoeud = New TreeNode("Emplois budgétaires", VI.PointdeVue.PVuePosteBud & VI.Tild)
        NewNoeud.PopulateOnDemand = True
        NewNoeud.SelectAction = TreeNodeSelectAction.Expand
        Noeud.ChildNodes.Add(NewNoeud)

        NewNoeud = New TreeNode("LOLF", VI.PointdeVue.PVueLolf & VI.Tild)
        NewNoeud.PopulateOnDemand = False
        NewNoeud.SelectAction = TreeNodeSelectAction.Select
        Noeud.ChildNodes.Add(NewNoeud)
    End Sub

    Private Sub ChargerMenuTempsTravail(ByVal Noeud As TreeNode)
        Dim NewNoeud As TreeNode

        NewNoeud = New TreeNode("Présences", VI.PointdeVue.PVuePresences & VI.Tild)
        NewNoeud.PopulateOnDemand = False
        NewNoeud.SelectAction = TreeNodeSelectAction.Select
        Noeud.ChildNodes.Add(NewNoeud)

        NewNoeud = New TreeNode("Absences", VI.PointdeVue.PVueAbsences & VI.Tild)
        NewNoeud.PopulateOnDemand = False
        NewNoeud.SelectAction = TreeNodeSelectAction.Select
        Noeud.ChildNodes.Add(NewNoeud)

        NewNoeud = New TreeNode("Cycles de base", VI.PointdeVue.PVueBaseHebdo & VI.Tild)
        NewNoeud.PopulateOnDemand = True
        NewNoeud.SelectAction = TreeNodeSelectAction.Expand
        Noeud.ChildNodes.Add(NewNoeud)

        NewNoeud = New TreeNode("Cycles de travail", VI.PointdeVue.PVueCycle & VI.Tild)
        NewNoeud.PopulateOnDemand = True
        NewNoeud.SelectAction = TreeNodeSelectAction.Expand
        Noeud.ChildNodes.Add(NewNoeud)

        NewNoeud = New TreeNode("Règles de valorisation", VI.PointdeVue.PVueValTemps & VI.Tild)
        NewNoeud.PopulateOnDemand = False
        NewNoeud.SelectAction = TreeNodeSelectAction.Select
        Noeud.ChildNodes.Add(NewNoeud)
    End Sub

    Private Sub ChargerMenuOrganisation(ByVal Noeud As TreeNode)
        Dim NewNoeud As TreeNode
        Dim LibelNiveau As String

        NewNoeud = New TreeNode("Etablissements", VI.PointdeVue.PVueEtablissement & VI.Tild)
        NewNoeud.PopulateOnDemand = False
        NewNoeud.SelectAction = TreeNodeSelectAction.Select
        Noeud.ChildNodes.Add(NewNoeud)

        LibelNiveau = WebFct.PointeurUtilisateur.PointeurParametres.LibelleNiveau_Organigramme(0)
        If LibelNiveau = "" Then
            LibelNiveau = "Niveau 1"
        End If
        NewNoeud = New TreeNode(LibelNiveau, VI.PointdeVue.PVueDirection & VI.Tild)
        NewNoeud.PopulateOnDemand = False
        NewNoeud.SelectAction = TreeNodeSelectAction.Select
        Noeud.ChildNodes.Add(NewNoeud)

        LibelNiveau = WebFct.PointeurUtilisateur.PointeurParametres.LibelleNiveau_Organigramme(1)
        If LibelNiveau = "" Then
            LibelNiveau = "Niveau 2"
        End If
        NewNoeud = New TreeNode(LibelNiveau, VI.PointdeVue.PVueGeneral & VI.Tild & "Service")
        NewNoeud.PopulateOnDemand = False
        NewNoeud.SelectAction = TreeNodeSelectAction.Select
        Noeud.ChildNodes.Add(NewNoeud)

        LibelNiveau = WebFct.PointeurUtilisateur.PointeurParametres.LibelleNiveau_Organigramme(2)
        If LibelNiveau = "" Then
            LibelNiveau = "Niveau 3"
        End If
        NewNoeud = New TreeNode(LibelNiveau, VI.PointdeVue.PVueGeneral & VI.Tild & "Org. Niveau 3")
        NewNoeud.PopulateOnDemand = False
        NewNoeud.SelectAction = TreeNodeSelectAction.Select
        Noeud.ChildNodes.Add(NewNoeud)

        LibelNiveau = WebFct.PointeurUtilisateur.PointeurParametres.LibelleNiveau_Organigramme(3)
        If LibelNiveau = "" Then
            LibelNiveau = "Niveau 4"
        End If
        NewNoeud = New TreeNode(LibelNiveau, VI.PointdeVue.PVueGeneral & VI.Tild & "Org. Niveau 4")
        NewNoeud.PopulateOnDemand = False
        NewNoeud.SelectAction = TreeNodeSelectAction.Select
        Noeud.ChildNodes.Add(NewNoeud)

        LibelNiveau = WebFct.PointeurUtilisateur.PointeurParametres.LibelleNiveau_Organigramme(4)
        If LibelNiveau = "" Then
            LibelNiveau = "Niveau 5"
        End If
        NewNoeud = New TreeNode(LibelNiveau, VI.PointdeVue.PVueGeneral & VI.Tild & "Org. Niveau 5")
        NewNoeud.PopulateOnDemand = False
        NewNoeud.SelectAction = TreeNodeSelectAction.Select
        Noeud.ChildNodes.Add(NewNoeud)

        LibelNiveau = WebFct.PointeurUtilisateur.PointeurParametres.LibelleNiveau_Organigramme(5)
        If LibelNiveau = "" Then
            LibelNiveau = "Niveau 6"
        End If
        NewNoeud = New TreeNode(LibelNiveau, VI.PointdeVue.PVueGeneral & VI.Tild & "Org. Niveau 6")
        NewNoeud.PopulateOnDemand = False
        NewNoeud.SelectAction = TreeNodeSelectAction.Select
        Noeud.ChildNodes.Add(NewNoeud)

        NewNoeud = New TreeNode("Postes fonctionnels", VI.PointdeVue.PVuePosteFct & VI.Tild)
        NewNoeud.PopulateOnDemand = True
        NewNoeud.SelectAction = TreeNodeSelectAction.Expand
        Noeud.ChildNodes.Add(NewNoeud)

        NewNoeud = New TreeNode("Activités - Fonctions", 42 & VI.Tild)
        NewNoeud.PopulateOnDemand = True
        NewNoeud.SelectAction = TreeNodeSelectAction.Expand
        Noeud.ChildNodes.Add(NewNoeud)

        NewNoeud = New TreeNode("Activités - Mesures", VI.PointdeVue.PVueActivite & VI.Tild)
        NewNoeud.PopulateOnDemand = True
        NewNoeud.SelectAction = TreeNodeSelectAction.Expand
        Noeud.ChildNodes.Add(NewNoeud)
    End Sub

    Private Sub ChargerMenuFormation(ByVal Noeud As TreeNode)
        Dim NewNoeud As TreeNode

        NewNoeud = New TreeNode("Stages et sessions", VI.PointdeVue.PVueFormation & VI.Tild)
        NewNoeud.PopulateOnDemand = True
        NewNoeud.SelectAction = TreeNodeSelectAction.Expand
        Noeud.ChildNodes.Add(NewNoeud)

        NewNoeud = New TreeNode("Entreprises et Organismes", VI.PointdeVue.PVueEntreprise & VI.Tild)
        NewNoeud.PopulateOnDemand = False
        NewNoeud.SelectAction = TreeNodeSelectAction.Select
        Noeud.ChildNodes.Add(NewNoeud)
    End Sub

    Private Sub ChargerMenuGeneral(ByVal Noeud As TreeNode)
        Dim NewNoeud As TreeNode

        NewNoeud = New TreeNode("Tables générales", VI.PointdeVue.PVueGeneral & VI.Tild)
        NewNoeud.PopulateOnDemand = True
        NewNoeud.SelectAction = TreeNodeSelectAction.Expand
        Noeud.ChildNodes.Add(NewNoeud)

        NewNoeud = New TreeNode("Commissions internes", VI.PointdeVue.PVueCommission & VI.Tild & "Interne")
        NewNoeud.PopulateOnDemand = False
        NewNoeud.SelectAction = TreeNodeSelectAction.Select
        Noeud.ChildNodes.Add(NewNoeud)

        NewNoeud = New TreeNode("Commissions externes", VI.PointdeVue.PVueCommission & VI.Tild & "Externe")
        NewNoeud.PopulateOnDemand = False
        NewNoeud.SelectAction = TreeNodeSelectAction.Select
        Noeud.ChildNodes.Add(NewNoeud)

        NewNoeud = New TreeNode("Distancier", VI.PointdeVue.PVueItineraire & VI.Tild)
        NewNoeud.PopulateOnDemand = False
        NewNoeud.SelectAction = TreeNodeSelectAction.Select
        Noeud.ChildNodes.Add(NewNoeud)

        NewNoeud = New TreeNode("Pays", VI.PointdeVue.PVuePays & VI.Tild)
        NewNoeud.PopulateOnDemand = False
        NewNoeud.SelectAction = TreeNodeSelectAction.Select
        Noeud.ChildNodes.Add(NewNoeud)

        NewNoeud = New TreeNode("Sites géographiques", VI.PointdeVue.PVueSiteGeo & VI.Tild)
        NewNoeud.PopulateOnDemand = False
        NewNoeud.SelectAction = TreeNodeSelectAction.Select
        Noeud.ChildNodes.Add(NewNoeud)

        NewNoeud = New TreeNode("Interfaces", VI.PointdeVue.PVueInterface & VI.Tild)
        NewNoeud.PopulateOnDemand = False
        NewNoeud.SelectAction = TreeNodeSelectAction.Select
        Noeud.ChildNodes.Add(NewNoeud)

        NewNoeud = New TreeNode("Tables de correspondance", VI.PointdeVue.PVueTranscodification & VI.Tild)
        NewNoeud.PopulateOnDemand = False
        NewNoeud.SelectAction = TreeNodeSelectAction.Select
        Noeud.ChildNodes.Add(NewNoeud)

    End Sub

    Private Sub ChargerMenuPaie(ByVal Noeud As TreeNode)
        Dim NewNoeud As TreeNode

        NewNoeud = New TreeNode("Règles de rémunération", VI.PointdeVue.PVuePaie & VI.Tild)
        NewNoeud.PopulateOnDemand = False
        NewNoeud.SelectAction = TreeNodeSelectAction.Select
        Noeud.ChildNodes.Add(NewNoeud)

        NewNoeud = New TreeNode("Primes", VI.PointdeVue.PVuePrimes & VI.Tild)
        NewNoeud.PopulateOnDemand = False
        NewNoeud.SelectAction = TreeNodeSelectAction.Select
        Noeud.ChildNodes.Add(NewNoeud)

        NewNoeud = New TreeNode("Missions", VI.PointdeVue.PVueMission & VI.Tild)
        NewNoeud.PopulateOnDemand = True
        NewNoeud.SelectAction = TreeNodeSelectAction.Expand
        Noeud.ChildNodes.Add(NewNoeud)
    End Sub

    Private Sub ChargerListeTablesGenerales(ByVal Noeud As TreeNode)
        Dim IndiceA As Integer
        Dim TableauObjet(0) As String
        Dim Tableaudata(0) As String
        Dim VImage As String = ""
        Dim CptSel As Integer = 0
        Dim FicheRef As Virtualia.Ressources.Datas.ObjetDossierREF
        Dim SiAfaire As Boolean = False
        Dim Predicat As Virtualia.Ressources.Predicats.PredicateSysRef
        Dim LstFiches As List(Of Virtualia.Ressources.Datas.ObjetDossierREF)

        If WebFct.PointeurContexte.SysRef_Listes.Count = 0 Then
            SiAfaire = True
        Else
            Predicat = New Virtualia.Ressources.Predicats.PredicateSysRef(V_NomTable, "")
            LstFiches = New List(Of Virtualia.Ressources.Datas.ObjetDossierREF)
            LstFiches = WebFct.PointeurContexte.SysRef_Listes.FindAll(AddressOf Predicat.RechercherTable)
            If LstFiches.Count = 0 Then
                SiAfaire = True
            End If
            Predicat = Nothing
            LstFiches = Nothing
        End If

        If SiAfaire = True Then
            TableauObjet = Strings.Split(WebFct.PointeurUtilisateur.LireTableSysReference(VI.PointdeVue.PVueGeneral), VI.SigneBarre, -1)
            For IndiceA = 0 To TableauObjet.Count - 1
                If TableauObjet(IndiceA) = "" Then
                    Exit For
                End If
                FicheRef = New Virtualia.Ressources.Datas.ObjetDossierREF(System.Configuration.ConfigurationManager.AppSettings("WebService.VirtualiaServeur"), _
                              WebFct.PointeurGlobal.VirModele, VI.PointdeVue.PVueGeneral, "TablesGenerales", TableauObjet(IndiceA))
                WebFct.PointeurContexte.SysRef_Listes.Add(FicheRef)
            Next IndiceA
        End If

        Predicat = New Virtualia.Ressources.Predicats.PredicateSysRef("TablesGenerales", "")
        LstFiches = New List(Of Virtualia.Ressources.Datas.ObjetDossierREF)
        LstFiches = WebFct.PointeurContexte.SysRef_Listes.FindAll(AddressOf Predicat.RechercherTable)
        If LstFiches.Count = 0 Then
            Exit Sub
        End If

        For IndiceA = 0 To LstFiches.Count - 1
            FicheRef = LstFiches.Item(IndiceA)
            Dim NewNoeud As TreeNode = New TreeNode(FicheRef.Valeur, VI.PointdeVue.PVueGeneral & VI.Tild & FicheRef.Valeur)
            NewNoeud.PopulateOnDemand = False
            NewNoeud.SelectAction = TreeNodeSelectAction.Select
            Noeud.ChildNodes.Add(NewNoeud)
            CptSel += 1
        Next IndiceA
    End Sub

    Private Sub ChargerSousMenus(ByVal Noeud As TreeNode, ByVal PtdeVue As Short)
        Dim VImage As String = ""
        Dim CptSel As Integer = 0
        Dim SousMenu As String = ""
        Dim IndiceA As Integer = 0

        Do
            Select Case PtdeVue
                Case VI.PointdeVue.PVueGrades
                    SousMenu = WebFct.PointeurUtilisateur.PointeurParametres.PointeurDistingo.ItemGestionGrade(IndiceA)
                Case VI.PointdeVue.PVueFormation
                    SousMenu = WebFct.PointeurUtilisateur.PointeurParametres.PointeurDistingo.ItemPlanFormation(IndiceA)
                Case VI.PointdeVue.PVuePosteBud
                    SousMenu = WebFct.PointeurUtilisateur.PointeurParametres.PointeurDistingo.ItemFilierePosteBud(IndiceA)
                Case VI.PointdeVue.PVueBaseHebdo
                    SousMenu = WebFct.PointeurUtilisateur.PointeurParametres.PointeurDistingo.ItemEtablissementUnite(IndiceA)
                Case VI.PointdeVue.PVueCycle
                    SousMenu = WebFct.PointeurUtilisateur.PointeurParametres.PointeurDistingo.ItemEtablissementCycle(IndiceA)
                Case VI.PointdeVue.PVueMission
                    SousMenu = WebFct.PointeurUtilisateur.PointeurParametres.PointeurFraisMission.ItemSelectionAnnee(IndiceA)
                Case VI.PointdeVue.PVuePosteFct
                    SousMenu = WebFct.PointeurUtilisateur.PointeurParametres.PointeurDistingo.ItemNiveau1PosteFct(IndiceA)
                Case VI.PointdeVue.PVueFonctionSupport, VI.PointdeVue.PVueActivite
                    SousMenu = WebFct.PointeurUtilisateur.PointeurParametres.PointeurDistingo.ItemEtablissementActivites(IndiceA)
            End Select
            If SousMenu = "" Then
                Exit Do
            End If
            Dim NewNoeud As TreeNode = New TreeNode(SousMenu, PtdeVue & VI.Tild & SousMenu)
            NewNoeud.PopulateOnDemand = False
            NewNoeud.SelectAction = TreeNodeSelectAction.Select
            Noeud.ChildNodes.Add(NewNoeud)
            IndiceA += 1
        Loop
    End Sub

    Protected Sub CommandeBlanc_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles CommandeBlanc.Click
        Dim Evenement As Virtualia.Systeme.Evenements.ValeurSelectionneeEventArgs
        Evenement = New Virtualia.Systeme.Evenements.ValeurSelectionneeEventArgs(WebFct.PointeurContexte.SysRef_IDAppelant, _
                WebFct.PointeurContexte.SysRef_ObjetAppelant, 0, "", 0, "", "")
        VSelection(Evenement)
    End Sub

    Protected Sub CommandeRetour_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles CommandeRetour.Click
        RaiseEvent RetourEventHandler(Me, e)
    End Sub

    Protected Sub CommandeNew_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles CommandeNew.Click
        HSelValeur.Value = ""
        Dim Evenement As Virtualia.Systeme.Evenements.ValeurSelectionneeEventArgs
        Evenement = New Virtualia.Systeme.Evenements.ValeurSelectionneeEventArgs("SysRef", _
                1, V_PointdeVue, V_NomTable, 0, "", "New")
        VSelection(Evenement)
    End Sub

    Protected Sub CommandeSupp_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles CommandeSupp.Click
        If HSelValeur.Value = "" Then
            Exit Sub
        End If
        Dim Evenement As Virtualia.Systeme.Evenements.ValeurSelectionneeEventArgs
        Evenement = New Virtualia.Systeme.Evenements.ValeurSelectionneeEventArgs("SysRef", _
                1, V_PointdeVue, V_NomTable, 0, "", "Supp")
        VSelection(Evenement)
    End Sub

    Private Sub FaireListeGrades(ByVal Noeud As TreeNode)
        Dim Collgrades As Virtualia.Ressources.Datas.ObjetGradeGrille = _
                                WebFct.PointeurUtilisateur.PointeurParametres.PointeurGradeGrilles
        Dim LstFiches As List(Of Virtualia.TablesObjet.ShemaREF.GRD_GRADE)
        Dim FicheRef As Virtualia.TablesObjet.ShemaREF.GRD_GRADE
        Dim FicheGrille As Virtualia.TablesObjet.ShemaREF.GRD_GRILLE
        Dim FicheEch As Virtualia.TablesObjet.ShemaREF.GRD_ECHELON
        Dim ClefListe As String
        Dim IndiceA As Integer
        Dim IndiceK As Integer
        Dim NoeudN1 As TreeNode = Nothing
        Dim NoeudN2 As TreeNode = Nothing
        Dim NoeudN3 As TreeNode = Nothing
        Dim LuN1 As String
        Dim LuN2 As String
        Dim LuN3 As String
        Dim CptSel As Integer = 0
        Dim ClefSel As String

        If Collgrades Is Nothing Then
            EtiStatus1.Text = "Aucune référence"
            Exit Sub
        End If

        Select Case HSelRadio.Value
            Case Is = "0" 'Liste Alpha
                If HSelMode.Value = "0" And HSelDateValeur.Value <> "" Then
                    TreeListeRef.MaxDataBindDepth = 2
                Else
                    TreeListeRef.MaxDataBindDepth = 0
                End If
                LstFiches = Collgrades.ListeDesGrades(WebFct.PointeurContexte.SysRef_Lettre, V_NomTable)
                If LstFiches.Count = 0 Then
                    EtiStatus1.Text = "Aucune référence"
                    Exit Sub
                End If
                For IndiceA = 0 To LstFiches.Count - 1
                    FicheRef = LstFiches.Item(IndiceA)
                    LuN1 = FicheRef.Intitule
                    ClefSel = LuN1 & VI.PointVirgule & FicheRef.Categorie & VI.PointVirgule & _
                                FicheRef.Filiere & VI.PointVirgule & FicheRef.Corps

                    ClefListe = FicheRef.Ide_Dossier & VI.Tild & LuN1 & VI.Tild & _
                                    ClefSel & VI.Tild & VI.PointdeVue.PVueGrades

                    NoeudN1 = AjouterNoeudValeur(Noeud, LuN1, ClefListe)
                    CptSel += 1

                    If HSelMode.Value = "0" And HSelDateValeur.Value <> "" Then 'Liste des échelons à la date du
                        FicheGrille = FicheRef.Fiche_Grille_Valable(HSelDateValeur.Value)
                        If FicheGrille IsNot Nothing Then
                            LuN2 = HSelDateValeur.Value
                            ClefListe = FicheRef.Ide_Dossier & VI.Tild & LuN2 & VI.Tild & _
                                                                LuN1 & LuN2 & VI.Tild & VI.PointdeVue.PVueGrilles
                            NoeudN2 = AjouterNoeudValeur(NoeudN1, "Grille indiciaire valable au : " & LuN2, ClefListe)
                            For IndiceK = 0 To FicheGrille.ListedesEchelons("").Count - 1
                                FicheEch = FicheGrille.ListedesEchelons("").Item(IndiceK)
                                If FicheEch.AliasEchelon = "" Then
                                    Exit For
                                End If
                                LuN3 = ""
                                If FicheEch.Chevron <> "" Then
                                    LuN3 &= "Chevron = " & FicheEch.Chevron & Strings.Space(2)
                                End If
                                LuN3 &= FicheEch.AliasEchelon & Strings.Space(2)
                                LuN3 &= "Majoré = " & FicheEch.IndiceMajore & Strings.Space(2)
                                LuN3 &= "Brut = " & FicheEch.IndiceBrut & Strings.Space(2)

                                ClefSel = LuN1 & VI.PointVirgule & FicheRef.Categorie & VI.PointVirgule & _
                                            FicheRef.Filiere & VI.PointVirgule & FicheRef.Corps

                                ClefSel &= VI.PointVirgule & FicheGrille.Intitule & VI.PointVirgule & _
                                        FicheEch.AliasEchelon & VI.PointVirgule & FicheEch.IndiceMajore & _
                                        VI.PointVirgule & FicheEch.IndiceBrut

                                ClefListe = FicheRef.Ide_Dossier & VI.Tild & LuN3.Trim & VI.Tild & _
                                                ClefSel & VI.Tild & VI.PointdeVue.PVueGrilles

                                NoeudN3 = AjouterNoeudValeur(NoeudN2, LuN3.Trim, ClefListe)
                            Next IndiceK
                        End If
                    End If
                Next IndiceA

            Case Is = "1" 'Liste Organisée
                Dim LuN4 As String
                Dim LuValeur As String
                Dim RuptN1 As String = "(Vide)"
                Dim RuptN2 As String = "(Vide)"
                Dim RuptN3 As String = "(Vide)"
                Dim RuptN4 As String = "(Vide)"
                Dim NoeudN4 As TreeNode = Nothing
                Dim VNoeud As TreeNode = Nothing

                If HSelMode.Value = "1" Then
                    LstFiches = Collgrades.ListeDesGradesTries("Gestion", V_NomTable)
                    TreeListeRef.MaxDataBindDepth = 3
                Else
                    LstFiches = Collgrades.ListeDesGradesTries("", "")
                    TreeListeRef.MaxDataBindDepth = 4
                End If
                If LstFiches.Count = 0 Then
                    EtiStatus1.Text = "Aucune référence"
                    Exit Sub
                End If

                For IndiceA = 0 To LstFiches.Count - 1
                    FicheRef = LstFiches.Item(IndiceA)
                    LuN1 = FicheRef.Gestion
                    LuN2 = FicheRef.Categorie
                    LuN3 = FicheRef.Filiere
                    LuN4 = FicheRef.Corps
                    LuValeur = FicheRef.Intitule
                    ClefSel = LuValeur & VI.PointVirgule & LuN2 & VI.PointVirgule & _
                                LuN3 & VI.PointVirgule & LuN4

                    If HSelMode.Value = "1" Then
                        NoeudN1 = Noeud
                    Else
                        Select Case LuN1
                            Case Is <> RuptN1
                                RuptN1 = LuN1
                                RuptN2 = "(Vide)"
                                RuptN3 = "(Vide)"
                                RuptN4 = "(Vide)"
                                ClefListe = FicheRef.Ide_Dossier & VI.Tild & LuN1 & VI.Tild & _
                                        LuN1 & VI.Tild & VI.PointdeVue.PVueGrades
                                NoeudN1 = AjouterNoeudValeur(Noeud, LuN1, ClefListe)
                        End Select
                    End If
                    Select Case LuN2
                        Case Is <> RuptN2
                            RuptN2 = LuN2
                            RuptN3 = "(Vide)"
                            RuptN4 = "(Vide)"
                            ClefListe = FicheRef.Ide_Dossier & VI.Tild & LuN2 & VI.Tild & _
                                    LuN1 & LuN2 & VI.Tild & VI.PointdeVue.PVueGrades
                            NoeudN2 = AjouterNoeudValeur(NoeudN1, LuN2, ClefListe)
                    End Select
                    Select Case LuN3
                        Case Is <> RuptN3
                            RuptN3 = LuN3
                            RuptN4 = "(Vide)"
                            ClefListe = FicheRef.Ide_Dossier & VI.Tild & LuN3 & VI.Tild & _
                                    LuN1 & LuN2 & LuN3 & VI.Tild & VI.PointdeVue.PVueGrades
                            NoeudN3 = AjouterNoeudValeur(NoeudN2, LuN3, ClefListe)
                    End Select
                    Select Case LuN4
                        Case Is <> RuptN4
                            RuptN4 = LuN4
                            ClefListe = FicheRef.Ide_Dossier & VI.Tild & LuN4 & VI.Tild & _
                                    LuN1 & LuN2 & LuN3 & LuN4 & VI.Tild & VI.PointdeVue.PVueGrades
                            NoeudN4 = AjouterNoeudValeur(NoeudN3, LuN4, ClefListe)
                    End Select
                    ClefListe = FicheRef.Ide_Dossier & VI.Tild & LuValeur & VI.Tild & _
                                    ClefSel & VI.Tild & VI.PointdeVue.PVueGrades
                    VNoeud = AjouterNoeudValeur(NoeudN4, LuValeur, ClefListe)
                    CptSel += 1
                Next IndiceA
        End Select

        Try
            If WsVirtuelPath <> "" Then
                Select Case HSelMode.Value
                    Case Is = "0"
                        TreeListeRef.FindNode(WsVirtuelPath).Selected = False
                    Case Else
                        TreeListeRef.FindNode(WsVirtuelPath).Selected = True
                End Select
                TreeListeRef.FindNode(WsVirtuelPath).Parent.Expanded = True
            End If
        Catch ex As Exception
            Exit Try
        End Try

        Select Case CptSel
            Case Is = 1
                EtiStatus1.Text = "Une référence"
            Case Else
                EtiStatus1.Text = CptSel.ToString & " références"
        End Select

        Dim NoeudRecherche As TreeNode
        NoeudRecherche = AlleraInfoDico(0, True, "")
        If NoeudRecherche IsNot Nothing Then
            EtiSecondaire.Text = NoeudRecherche.Text
            EtiPrincipal.Text = NoeudRecherche.Parent.Text
        End If

        If HSelMode.Value = "1" Then
            Noeud.ExpandAll()
        Else
            If HSelDateValeur.Value = "" Then
                Noeud.ExpandAll()
            End If
        End If
    End Sub

    Private Sub FaireListePosteBud(ByVal Noeud As TreeNode)
        Dim CollPostes As Virtualia.Ressources.Datas.ObjetPosteBudgetaire = _
                                WebFct.PointeurUtilisateur.PointeurParametres.PointeurPostesBudgetaires
        Dim LstFiches As List(Of Virtualia.TablesObjet.ShemaREF.ORG_BUDGETAIRE)
        Dim FicheRef As Virtualia.TablesObjet.ShemaREF.ORG_BUDGETAIRE
        Dim ClefListe As String
        Dim IndiceA As Integer
        Dim NoeudN1 As TreeNode = Nothing
        Dim CptSel As Integer = 0
        Dim DateJour As String = WebFct.ViRhDates.DateduJour(False)

        If CollPostes Is Nothing Then
            EtiStatus1.Text = "Aucune référence"
            Exit Sub
        End If

        Select Case HSelRadio.Value
            Case Is = "0" 'Liste Alpha
                TreeListeRef.MaxDataBindDepth = 0
                LstFiches = CollPostes.ListeDesPostes_Alpha(WebFct.PointeurContexte.SysRef_Lettre, V_NomTable)
                If LstFiches.Count = 0 Then
                    EtiStatus1.Text = "Aucune référence"
                    Exit Sub
                End If
                For IndiceA = 0 To LstFiches.Count - 1
                    FicheRef = LstFiches.Item(IndiceA)
                    ClefListe = FicheRef.Ide_Dossier & VI.Tild & FicheRef.Numero_EB & VI.Tild & _
                                    FicheRef.Filiere_Definition(DateJour) & VI.Tild & VI.PointdeVue.PVuePosteBud
                    NoeudN1 = AjouterNoeudValeur(Noeud, FicheRef.Numero_EB & " - " & _
                                                 FicheRef.Intitule__Definition & " - " & _
                                                 FicheRef.Budget__Definition(DateJour), ClefListe)
                    CptSel += 1
                Next IndiceA

            Case Is = "1" 'Liste Organisée
                Dim LuN1 As String
                Dim LuN2 As String
                Dim LuN3 As String
                Dim LuValeur As String
                Dim RuptN1 As String = "(Vide)"
                Dim RuptN2 As String = "(Vide)"
                Dim RuptN3 As String = "(Vide)"
                Dim NoeudN2 As TreeNode = Nothing
                Dim NoeudN3 As TreeNode = Nothing
                Dim VNoeud As TreeNode = Nothing

                If HSelMode.Value = "1" Then
                    LstFiches = CollPostes.ListeDesPostes(V_NomTable, DateJour)
                    TreeListeRef.MaxDataBindDepth = 2
                Else
                    LstFiches = CollPostes.ListeDesPostes("", DateJour)
                    TreeListeRef.MaxDataBindDepth = 3
                End If
                If LstFiches.Count = 0 Then
                    EtiStatus1.Text = "Aucune référence"
                    Exit Sub
                End If

                For IndiceA = 0 To LstFiches.Count - 1
                    FicheRef = LstFiches.Item(IndiceA)
                    LuN1 = FicheRef.Filiere_Definition(DateJour)
                    LuN2 = FicheRef.Secteur_Definition
                    LuN3 = FicheRef.Intitule__Definition
                    LuValeur = FicheRef.Numero_EB & " - " & FicheRef.Budget__Definition

                    If HSelMode.Value = "1" Then
                        NoeudN1 = Noeud
                    Else
                        Select Case LuN1
                            Case Is <> RuptN1
                                RuptN1 = LuN1
                                RuptN2 = "(Vide)"
                                RuptN3 = "(Vide)"
                                ClefListe = FicheRef.Ide_Dossier & VI.Tild & LuN1 & VI.Tild & _
                                        LuN1 & VI.Tild & VI.PointdeVue.PVuePosteBud
                                NoeudN1 = AjouterNoeudValeur(Noeud, LuN1, ClefListe)
                        End Select
                    End If

                    Select Case LuN2
                        Case Is <> RuptN2
                            RuptN2 = LuN2
                            RuptN3 = "(Vide)"
                            ClefListe = FicheRef.Ide_Dossier & VI.Tild & LuN2 & VI.Tild & _
                                    LuN1 & LuN2 & VI.Tild & VI.PointdeVue.PVuePosteBud
                            NoeudN2 = AjouterNoeudValeur(NoeudN1, LuN2, ClefListe)
                    End Select
                    Select Case LuN3
                        Case Is <> RuptN3
                            RuptN3 = LuN3
                            ClefListe = FicheRef.Ide_Dossier & VI.Tild & LuN3 & VI.Tild & _
                                    LuN1 & LuN2 & LuN3 & VI.Tild & VI.PointdeVue.PVuePosteBud
                            NoeudN3 = AjouterNoeudValeur(NoeudN2, LuN3, ClefListe)
                    End Select
                    ClefListe = FicheRef.Ide_Dossier & VI.Tild & LuValeur & VI.Tild & _
                                    LuN1 & LuN2 & LuN3 & LuValeur & VI.Tild & VI.PointdeVue.PVuePosteBud
                    VNoeud = AjouterNoeudValeur(NoeudN3, LuValeur, ClefListe)
                    CptSel += 1
                Next IndiceA
        End Select

        Try
            If WsVirtuelPath <> "" Then
                Select Case HSelMode.Value
                    Case Is = "0"
                        TreeListeRef.FindNode(WsVirtuelPath).Selected = False
                    Case Else
                        TreeListeRef.FindNode(WsVirtuelPath).Selected = True
                End Select
                TreeListeRef.FindNode(WsVirtuelPath).Parent.Expanded = True
            End If
        Catch ex As Exception
            Exit Try
        End Try

        Select Case CptSel
            Case Is = 1
                EtiStatus1.Text = "Une référence"
            Case Else
                EtiStatus1.Text = CptSel.ToString & " références"
        End Select

        Dim NoeudRecherche As TreeNode
        NoeudRecherche = AlleraInfoDico(0, True, "")
        If NoeudRecherche IsNot Nothing Then
            EtiSecondaire.Text = NoeudRecherche.Text
            EtiPrincipal.Text = NoeudRecherche.Parent.Text
        End If

        If HSelMode.Value = "1" Then
            Noeud.ExpandAll()
        End If
    End Sub

    Private Sub FaireListeParEtablissement(ByVal Noeud As TreeNode)
        Dim ClefListe As String
        Dim IndiceA As Integer
        Dim NoeudN1 As TreeNode = Nothing
        Dim CptSel As Integer = 0

        Select Case V_PointdeVue
            Case VI.PointdeVue.PVueBaseHebdo
                Dim CollWork As Virtualia.Ressources.Datas.ObjetUniteCycle = _
                                WebFct.PointeurUtilisateur.PointeurParametres.PointeurUniteCycles
                Dim LstFiches As List(Of Virtualia.TablesObjet.ShemaREF.CYC_IDENTIFICATION)
                Dim FicheRef As Virtualia.TablesObjet.ShemaREF.CYC_IDENTIFICATION
                If CollWork Is Nothing Then
                    EtiStatus1.Text = "Aucune référence"
                    Exit Sub
                End If
                Select Case HSelRadio.Value
                    Case Is = "0" 'Liste Alpha
                        TreeListeRef.MaxDataBindDepth = 0
                        LstFiches = CollWork.ListeDesUnites(WebFct.PointeurContexte.SysRef_Lettre, V_NomTable)
                        If LstFiches.Count = 0 Then
                            EtiStatus1.Text = "Aucune référence"
                            Exit Sub
                        End If
                        For IndiceA = 0 To LstFiches.Count - 1
                            FicheRef = LstFiches.Item(IndiceA)
                            ClefListe = FicheRef.Ide_Dossier & VI.Tild & FicheRef.Intitule & VI.Tild & _
                                            FicheRef.Etablissement & VI.Tild & VI.PointdeVue.PVueBaseHebdo
                            NoeudN1 = AjouterNoeudValeur(Noeud, FicheRef.Intitule, ClefListe)
                            CptSel += 1
                        Next IndiceA

                    Case Is = "1" 'Liste Organisée
                        Dim LuN1 As String
                        Dim LuValeur As String
                        Dim RuptN1 As String = "(Vide)"
                        Dim VNoeud As TreeNode = Nothing

                        If HSelMode.Value = "1" Then
                            TreeListeRef.MaxDataBindDepth = 0
                            LstFiches = CollWork.ListeDesUnites(V_NomTable)
                        Else
                            TreeListeRef.MaxDataBindDepth = 1
                            LstFiches = CollWork.ListeDesUnites("")
                        End If
                        If LstFiches.Count = 0 Then
                            EtiStatus1.Text = "Aucune référence"
                            Exit Sub
                        End If

                        For IndiceA = 0 To LstFiches.Count - 1
                            FicheRef = LstFiches.Item(IndiceA)
                            LuN1 = FicheRef.Etablissement
                            LuValeur = FicheRef.Intitule
                            If HSelMode.Value = "1" Then
                                NoeudN1 = Noeud
                            Else
                                Select Case LuN1
                                    Case Is <> RuptN1
                                        RuptN1 = LuN1
                                        ClefListe = FicheRef.Ide_Dossier & VI.Tild & LuN1 & VI.Tild & _
                                                LuN1 & VI.Tild & VI.PointdeVue.PVueBaseHebdo
                                        NoeudN1 = AjouterNoeudValeur(Noeud, LuN1, ClefListe)
                                End Select
                            End If
                            ClefListe = FicheRef.Ide_Dossier & VI.Tild & LuValeur & VI.Tild & _
                                            LuN1 & LuValeur & VI.Tild & VI.PointdeVue.PVueBaseHebdo
                            VNoeud = AjouterNoeudValeur(NoeudN1, LuValeur, ClefListe)
                            CptSel += 1
                        Next IndiceA
                End Select

            Case VI.PointdeVue.PVueCycle
                Dim CollWork As Virtualia.Ressources.Datas.ObjetCycleTravail = _
                                WebFct.PointeurUtilisateur.PointeurParametres.PointeurCyclesTravail
                Dim LstFiches As List(Of Virtualia.TablesObjet.ShemaREF.TRA_IDENTIFICATION)
                Dim FicheRef As Virtualia.TablesObjet.ShemaREF.TRA_IDENTIFICATION
                If CollWork Is Nothing Then
                    EtiStatus1.Text = "Aucune référence"
                    Exit Sub
                End If
                Select Case HSelRadio.Value
                    Case Is = "0" 'Liste Alpha
                        TreeListeRef.MaxDataBindDepth = 0
                        LstFiches = CollWork.ListeDesCycles(WebFct.PointeurContexte.SysRef_Lettre, V_NomTable)
                        If LstFiches.Count = 0 Then
                            EtiStatus1.Text = "Aucune référence"
                            Exit Sub
                        End If
                        For IndiceA = 0 To LstFiches.Count - 1
                            FicheRef = LstFiches.Item(IndiceA)
                            ClefListe = FicheRef.Ide_Dossier & VI.Tild & FicheRef.Intitule & VI.Tild & _
                                            FicheRef.Etablissement & VI.Tild & VI.PointdeVue.PVueCycle
                            NoeudN1 = AjouterNoeudValeur(Noeud, FicheRef.Intitule, ClefListe)
                            CptSel += 1
                        Next IndiceA

                    Case Is = "1" 'Liste Organisée
                        Dim LuN1 As String
                        Dim LuValeur As String
                        Dim RuptN1 As String = "(Vide)"
                        Dim VNoeud As TreeNode = Nothing

                        If HSelMode.Value = "1" Then
                            TreeListeRef.MaxDataBindDepth = 0
                            LstFiches = CollWork.ListeDesCycles(V_NomTable)
                        Else
                            TreeListeRef.MaxDataBindDepth = 1
                            LstFiches = CollWork.ListeDesCycles("")
                        End If
                        If LstFiches.Count = 0 Then
                            EtiStatus1.Text = "Aucune référence"
                            Exit Sub
                        End If

                        For IndiceA = 0 To LstFiches.Count - 1
                            FicheRef = LstFiches.Item(IndiceA)
                            LuN1 = FicheRef.Etablissement
                            LuValeur = FicheRef.Intitule
                            If HSelMode.Value = "1" Then
                                NoeudN1 = Noeud
                            Else
                                Select Case LuN1
                                    Case Is <> RuptN1
                                        RuptN1 = LuN1
                                        ClefListe = FicheRef.Ide_Dossier & VI.Tild & LuN1 & VI.Tild & _
                                                LuN1 & VI.Tild & VI.PointdeVue.PVueCycle
                                        NoeudN1 = AjouterNoeudValeur(Noeud, LuN1, ClefListe)
                                End Select
                            End If
                            ClefListe = FicheRef.Ide_Dossier & VI.Tild & LuValeur & VI.Tild & _
                                            LuN1 & LuValeur & VI.Tild & VI.PointdeVue.PVueCycle
                            VNoeud = AjouterNoeudValeur(NoeudN1, LuValeur, ClefListe)
                            CptSel += 1
                        Next IndiceA
                End Select

            Case VI.PointdeVue.PVueFonctionSupport
                Dim CollWork As Virtualia.Ressources.Datas.ObjetMesureActivite = _
                                WebFct.PointeurUtilisateur.PointeurParametres.PointeurActiviteMesures
                Dim LstFiches As List(Of Virtualia.TablesObjet.ShemaREF.FCT_SUPPORT)
                Dim FicheRef As Virtualia.TablesObjet.ShemaREF.FCT_SUPPORT
                If CollWork Is Nothing Then
                    EtiStatus1.Text = "Aucune référence"
                    Exit Sub
                End If
                Select Case HSelRadio.Value
                    Case Is = "0" 'Liste Alpha
                        TreeListeRef.MaxDataBindDepth = 0
                        LstFiches = CollWork.ListeFonctionsSupport(WebFct.PointeurContexte.SysRef_Lettre, V_NomTable)
                        If LstFiches.Count = 0 Then
                            EtiStatus1.Text = "Aucune référence"
                            Exit Sub
                        End If
                        For IndiceA = 0 To LstFiches.Count - 1
                            FicheRef = LstFiches.Item(IndiceA)
                            ClefListe = FicheRef.Ide_Dossier & VI.Tild & FicheRef.Intitule & VI.Tild & _
                                            FicheRef.Etablissement & VI.Tild & VI.PointdeVue.PVueFonctionSupport
                            NoeudN1 = AjouterNoeudValeur(Noeud, FicheRef.Intitule, ClefListe)
                            CptSel += 1
                        Next IndiceA

                    Case Is = "1" 'Liste Organisée
                        Dim LuN1 As String
                        Dim LuValeur As String
                        Dim RuptN1 As String = "(Vide)"
                        Dim VNoeud As TreeNode = Nothing

                        If HSelMode.Value = "1" Then
                            TreeListeRef.MaxDataBindDepth = 0
                            LstFiches = CollWork.ListeFonctionsSupport(V_NomTable)
                        Else
                            TreeListeRef.MaxDataBindDepth = 1
                            LstFiches = CollWork.ListeFonctionsSupport("")
                        End If
                        If LstFiches.Count = 0 Then
                            EtiStatus1.Text = "Aucune référence"
                            Exit Sub
                        End If

                        For IndiceA = 0 To LstFiches.Count - 1
                            FicheRef = LstFiches.Item(IndiceA)
                            LuN1 = FicheRef.Etablissement
                            LuValeur = FicheRef.Intitule
                            If HSelMode.Value = "1" Then
                                NoeudN1 = Noeud
                            Else
                                Select Case LuN1
                                    Case Is <> RuptN1
                                        RuptN1 = LuN1
                                        ClefListe = FicheRef.Ide_Dossier & VI.Tild & LuN1 & VI.Tild & _
                                                LuN1 & VI.Tild & VI.PointdeVue.PVueFonctionSupport
                                        NoeudN1 = AjouterNoeudValeur(Noeud, LuN1, ClefListe)
                                End Select
                            End If
                            ClefListe = FicheRef.Ide_Dossier & VI.Tild & LuValeur & VI.Tild & _
                                            LuN1 & LuValeur & VI.Tild & VI.PointdeVue.PVueFonctionSupport
                            VNoeud = AjouterNoeudValeur(NoeudN1, LuValeur, ClefListe)
                            CptSel += 1
                        Next IndiceA
                End Select

            Case VI.PointdeVue.PVueActivite
                Dim CollWork As Virtualia.Ressources.Datas.ObjetMesureActivite = _
                                WebFct.PointeurUtilisateur.PointeurParametres.PointeurActiviteMesures
                Dim LstFiches As List(Of Virtualia.TablesObjet.ShemaREF.ONIC_MISSION)
                Dim FicheRef As Virtualia.TablesObjet.ShemaREF.ONIC_MISSION
                If CollWork Is Nothing Then
                    EtiStatus1.Text = "Aucune référence"
                    Exit Sub
                End If
                Select Case HSelRadio.Value
                    Case Is = "0" 'Liste Alpha
                        TreeListeRef.MaxDataBindDepth = 0
                        LstFiches = CollWork.ListeDesMisions(V_NomTable)
                        If LstFiches.Count = 0 Then
                            EtiStatus1.Text = "Aucune référence"
                            Exit Sub
                        End If
                        For IndiceA = 0 To LstFiches.Count - 1
                            FicheRef = LstFiches.Item(IndiceA)
                            ClefListe = FicheRef.Ide_Dossier & VI.Tild & FicheRef.Intitule & VI.Tild & _
                                            FicheRef.Etablissement & VI.Tild & VI.PointdeVue.PVueActivite
                            NoeudN1 = AjouterNoeudValeur(Noeud, FicheRef.Intitule, ClefListe)
                            CptSel += 1
                        Next IndiceA

                    Case Is = "1" 'Liste Organisée
                        Dim LuN1 As String
                        Dim LuValeur As String
                        Dim RuptN1 As String = "(Vide)"
                        Dim VNoeud As TreeNode = Nothing

                        If HSelMode.Value = "1" Then
                            TreeListeRef.MaxDataBindDepth = 0
                            LstFiches = CollWork.ListeDesMisions(V_NomTable)
                        Else
                            TreeListeRef.MaxDataBindDepth = 1
                            LstFiches = CollWork.ListeDesMisions("")
                        End If
                        If LstFiches.Count = 0 Then
                            EtiStatus1.Text = "Aucune référence"
                            Exit Sub
                        End If

                        For IndiceA = 0 To LstFiches.Count - 1
                            FicheRef = LstFiches.Item(IndiceA)
                            LuN1 = FicheRef.Etablissement
                            LuValeur = FicheRef.Intitule
                            If HSelMode.Value = "1" Then
                                NoeudN1 = Noeud
                            Else
                                Select Case LuN1
                                    Case Is <> RuptN1
                                        RuptN1 = LuN1
                                        ClefListe = FicheRef.Ide_Dossier & VI.Tild & LuN1 & VI.Tild & _
                                                LuN1 & VI.Tild & VI.PointdeVue.PVueActivite
                                        NoeudN1 = AjouterNoeudValeur(Noeud, LuN1, ClefListe)
                                End Select
                            End If
                            ClefListe = FicheRef.Ide_Dossier & VI.Tild & LuValeur & VI.Tild & _
                                            LuN1 & LuValeur & VI.Tild & VI.PointdeVue.PVueActivite
                            VNoeud = AjouterNoeudValeur(NoeudN1, LuValeur, ClefListe)
                            CptSel += 1
                        Next IndiceA
                End Select
        End Select

        If WsVirtuelPath <> "" Then
            Select Case HSelMode.Value
                Case Is = "0"
                    TreeListeRef.FindNode(WsVirtuelPath).Selected = False
                Case Else
                    TreeListeRef.FindNode(WsVirtuelPath).Selected = True
            End Select
            TreeListeRef.FindNode(WsVirtuelPath).Parent.Expanded = True
        End If

        Select Case CptSel
            Case Is = 1
                EtiStatus1.Text = "Une référence"
            Case Else
                EtiStatus1.Text = CptSel.ToString & " références"
        End Select

        Dim NoeudRecherche As TreeNode
        NoeudRecherche = AlleraInfoDico(0, True, "")
        If NoeudRecherche IsNot Nothing Then
            EtiSecondaire.Text = NoeudRecherche.Text
            EtiPrincipal.Text = NoeudRecherche.Parent.Text
        End If

        If HSelMode.Value = "1" Then
            Noeud.ExpandAll()
        End If
    End Sub

    Private Sub FaireListeStages(ByVal Noeud As TreeNode)
        Dim CollStages As Virtualia.Ressources.Datas.ObjetStageFormation _
                        = WebFct.PointeurUtilisateur.PointeurParametres.PointeurStagesFormation
        Dim LstFiches As List(Of Virtualia.TablesObjet.ShemaREF.FOR_IDENTIFICATION)
        Dim FicheRef As Virtualia.TablesObjet.ShemaREF.FOR_IDENTIFICATION
        Dim ClefListe As String
        Dim IndiceA As Integer
        Dim NoeudN1 As TreeNode = Nothing
        Dim CptSel As Integer = 0

        If CollStages Is Nothing Then
            EtiStatus1.Text = "Aucune référence"
            Exit Sub
        End If

        Select Case HSelRadio.Value
            Case Is = "0" 'Liste Alpha
                TreeListeRef.MaxDataBindDepth = 0
                If HSelMode.Value = "1" Then
                    LstFiches = CollStages.ListeDesStages_Alpha_Plan(WebFct.PointeurContexte.SysRef_Lettre, V_NomTable)
                Else
                    If CollStages.NombredeStages > 500 And WebFct.PointeurContexte.SysRef_Lettre = "" Then
                        WebFct.PointeurContexte.SysRef_Lettre = "A"
                        VRecherche.Text = "A"
                    End If
                    LstFiches = CollStages.ListeDesStages_Alpha_Etablissement(WebFct.PointeurContexte.SysRef_Lettre, "")
                End If
                If LstFiches.Count = 0 Then
                    EtiStatus1.Text = "Aucune référence"
                    Exit Sub
                End If
                For IndiceA = 0 To LstFiches.Count - 1
                    FicheRef = LstFiches.Item(IndiceA)
                    ClefListe = FicheRef.Ide_Dossier & VI.Tild & FicheRef.Intitule & VI.Tild & _
                                    FicheRef.Domaine & VI.Tild & VI.PointdeVue.PVueFormation
                    NoeudN1 = AjouterNoeudValeur(Noeud, FicheRef.Intitule, ClefListe)
                    CptSel += 1
                Next IndiceA

            Case Is = "1" 'Liste Organisée
                Dim LuN1 As String
                Dim LuN2 As String
                Dim LuN3 As String
                Dim LuN4 As String
                Dim LuValeur As String
                Dim RuptN1 As String = "(Vide)"
                Dim RuptN2 As String = "(Vide)"
                Dim RuptN3 As String = "(Vide)"
                Dim RuptN4 As String = "(Vide)"
                Dim NoeudN2 As TreeNode = Nothing
                Dim NoeudN3 As TreeNode = Nothing
                Dim NoeudN4 As TreeNode = Nothing
                Dim VNoeud As TreeNode = Nothing
                Dim IndiceK As Integer

                WebFct.PointeurContexte.SysRef_Lettre = ""
                VRecherche.Text = ""

                If HSelMode.Value = "1" Then
                    TreeListeRef.MaxDataBindDepth = 3
                    LstFiches = CollStages.ListeDesStages("Plan", V_NomTable)
                Else
                    TreeListeRef.MaxDataBindDepth = 4
                    LstFiches = CollStages.ListeDesStages("", "")
                End If
                If LstFiches.Count = 0 Then
                    EtiStatus1.Text = "Aucune référence"
                    Exit Sub
                End If

                For IndiceA = 0 To LstFiches.Count - 1
                    FicheRef = LstFiches.Item(IndiceA)
                    LuN1 = FicheRef.PlandeFormation
                    LuN2 = FicheRef.Domaine
                    LuN3 = FicheRef.Theme
                    LuN4 = FicheRef.Intitule
                    If HSelMode.Value = "1" Then
                        NoeudN1 = Noeud
                    Else
                        Select Case LuN1
                            Case Is <> RuptN1
                                RuptN1 = LuN1
                                RuptN2 = "(Vide)"
                                RuptN3 = "(Vide)"
                                RuptN4 = "(Vide)"
                                ClefListe = FicheRef.Ide_Dossier & VI.Tild & LuN1 & VI.Tild & _
                                        LuN1 & VI.Tild & VI.PointdeVue.PVueFormation
                                NoeudN1 = AjouterNoeudValeur(Noeud, LuN1, ClefListe)
                        End Select
                    End If
                    Select Case LuN2
                        Case Is <> RuptN2
                            RuptN2 = LuN2
                            RuptN3 = "(Vide)"
                            RuptN4 = "(Vide)"
                            ClefListe = FicheRef.Ide_Dossier & VI.Tild & LuN2 & VI.Tild & _
                                    LuN1 & LuN2 & VI.Tild & VI.PointdeVue.PVueFormation
                            NoeudN2 = AjouterNoeudValeur(NoeudN1, LuN2, ClefListe)
                    End Select
                    Select Case LuN3
                        Case Is <> RuptN3
                            RuptN3 = LuN3
                            RuptN4 = "(Vide)"
                            ClefListe = FicheRef.Ide_Dossier & VI.Tild & LuN3 & VI.Tild & _
                                    LuN1 & LuN2 & LuN3 & VI.Tild & VI.PointdeVue.PVueFormation
                            NoeudN3 = AjouterNoeudValeur(NoeudN2, LuN3, ClefListe)
                    End Select
                    Select Case LuN4
                        Case Is <> RuptN4
                            RuptN4 = LuN4
                            ClefListe = FicheRef.Ide_Dossier & VI.Tild & LuN4 & VI.Tild & _
                                    LuN1 & LuN2 & LuN3 & LuN4 & VI.Tild & VI.PointdeVue.PVueFormation
                            NoeudN4 = AjouterNoeudValeur(NoeudN3, LuN4, ClefListe)
                    End Select
                    If FicheRef.ListedesSessions IsNot Nothing Then
                        For IndiceK = 0 To FicheRef.ListedesSessions.Count - 1
                            LuValeur = WebFct.ViRhDates.ClairDate(FicheRef.ListedesSessions.Item(IndiceK).Date_de_Valeur, True)
                            ClefListe = FicheRef.Ide_Dossier & VI.Tild & LuValeur & VI.Tild & _
                                            LuN1 & LuN2 & LuN3 & LuN4 & LuValeur & VI.Tild & VI.PointdeVue.PVueFormation
                            VNoeud = AjouterNoeudValeur(NoeudN4, LuValeur, ClefListe)
                        Next IndiceK
                    End If
                    CptSel += 1
                Next IndiceA
        End Select

        If WsVirtuelPath <> "" Then
            Try
                Select Case HSelMode.Value
                    Case Is = "0"
                        TreeListeRef.FindNode(WsVirtuelPath).Selected = False
                    Case Else
                        TreeListeRef.FindNode(WsVirtuelPath).Selected = True
                End Select
                TreeListeRef.FindNode(WsVirtuelPath).Parent.Expanded = True
            Catch ex As Exception
                Exit Try
            End Try
        End If

        Select Case CptSel
            Case Is = 1
                EtiStatus1.Text = "Une référence"
            Case Else
                EtiStatus1.Text = CptSel.ToString & " références"
        End Select

        Dim NoeudRecherche As TreeNode
        NoeudRecherche = AlleraInfoDico(0, True, "")
        If NoeudRecherche IsNot Nothing Then
            EtiSecondaire.Text = NoeudRecherche.Text
            EtiPrincipal.Text = NoeudRecherche.Parent.Text
        End If

        If HSelMode.Value = "1" Then
            Noeud.ExpandAll()
        End If
    End Sub

    Private Sub FaireListeCommissions(ByVal Noeud As TreeNode)
        Dim CollCommis As Virtualia.Ressources.Datas.ObjetCommission = _
                                WebFct.PointeurUtilisateur.PointeurParametres.PointeurCommissions
        Dim LstFiches As List(Of Virtualia.TablesObjet.ShemaREF.ACT_COMMISSION)
        Dim FicheRef As Virtualia.TablesObjet.ShemaREF.ACT_COMMISSION
        Dim ClefListe As String
        Dim IndiceA As Integer
        Dim NoeudN1 As TreeNode = Nothing
        Dim CptSel As Integer = 0
        Dim DateJour As String = WebFct.ViRhDates.DateduJour(False)
        Dim SiInterne As Boolean = True

        If CollCommis Is Nothing Then
            EtiStatus1.Text = "Aucune référence"
            Exit Sub
        End If
        If V_NomTable <> "Interne" Then
            SiInterne = False
        End If

        Select Case HSelRadio.Value
            Case Is = "0" 'Liste Alpha
                TreeListeRef.MaxDataBindDepth = 0
                LstFiches = CollCommis.ListeDesCommissions(SiInterne, WebFct.PointeurContexte.SysRef_Lettre)
                If LstFiches.Count = 0 Then
                    EtiStatus1.Text = "Aucune référence"
                    Exit Sub
                End If
                For IndiceA = 0 To LstFiches.Count - 1
                    FicheRef = LstFiches.Item(IndiceA)
                    ClefListe = FicheRef.Ide_Dossier & VI.Tild & FicheRef.Intitule & VI.Tild & _
                                    FicheRef.Numero & VI.Tild & VI.PointdeVue.PVueCommission
                    NoeudN1 = AjouterNoeudValeur(Noeud, FicheRef.Intitule & " - " & _
                                                 FicheRef.Secteur_Activite & " - " & _
                                                 FicheRef.Organisme_Competent, ClefListe)
                    CptSel += 1
                Next IndiceA

            Case Is = "1" 'Liste Organisée
                Dim LuN1 As String
                Dim LuN2 As String
                Dim LuValeur As String
                Dim RuptN1 As String = "(Vide)"
                Dim RuptN2 As String = "(Vide)"
                Dim NoeudN2 As TreeNode = Nothing
                Dim VNoeud As TreeNode = Nothing

                LstFiches = CollCommis.ListeDesCommissions(SiInterne, "", "Activité")
                TreeListeRef.MaxDataBindDepth = 2
                If LstFiches.Count = 0 Then
                    EtiStatus1.Text = "Aucune référence"
                    Exit Sub
                End If

                For IndiceA = 0 To LstFiches.Count - 1
                    FicheRef = LstFiches.Item(IndiceA)
                    LuN1 = FicheRef.Secteur_Activite
                    LuN2 = FicheRef.Organisme_Competent
                    LuValeur = FicheRef.Intitule

                    Select Case LuN1
                        Case Is <> RuptN1
                            RuptN1 = LuN1
                            RuptN2 = "(Vide)"
                            ClefListe = FicheRef.Ide_Dossier & VI.Tild & LuN1 & VI.Tild & _
                                    LuN1 & VI.Tild & VI.PointdeVue.PVueCommission
                            NoeudN1 = AjouterNoeudValeur(Noeud, LuN1, ClefListe)
                    End Select
                    Select Case LuN2
                        Case Is <> RuptN2
                            RuptN2 = LuN2
                            ClefListe = FicheRef.Ide_Dossier & VI.Tild & LuN2 & VI.Tild & _
                                    LuN1 & LuN2 & VI.Tild & VI.PointdeVue.PVueCommission
                            NoeudN2 = AjouterNoeudValeur(NoeudN1, LuN2, ClefListe)
                    End Select
                    ClefListe = FicheRef.Ide_Dossier & VI.Tild & LuValeur & VI.Tild & _
                                    LuN1 & LuN2 & LuValeur & VI.Tild & VI.PointdeVue.PVueCommission
                    VNoeud = AjouterNoeudValeur(NoeudN2, LuValeur, ClefListe)
                    CptSel += 1
                Next IndiceA
        End Select

        Try
            If WsVirtuelPath <> "" Then
                Select Case HSelMode.Value
                    Case Is = "0"
                        TreeListeRef.FindNode(WsVirtuelPath).Selected = False
                    Case Else
                        TreeListeRef.FindNode(WsVirtuelPath).Selected = True
                End Select
                TreeListeRef.FindNode(WsVirtuelPath).Parent.Expanded = True
            End If
        Catch ex As Exception
            Exit Try
        End Try

        Select Case CptSel
            Case Is = 1
                EtiStatus1.Text = "Une référence"
            Case Else
                EtiStatus1.Text = CptSel.ToString & " références"
        End Select

        Dim NoeudRecherche As TreeNode
        NoeudRecherche = AlleraInfoDico(0, True, "")
        If NoeudRecherche IsNot Nothing Then
            EtiSecondaire.Text = NoeudRecherche.Text
            EtiPrincipal.Text = NoeudRecherche.Parent.Text
        End If

        If HSelMode.Value = "1" Then
            Noeud.ExpandAll()
        End If
    End Sub

    Private Sub FaireListePostesFonctionnels(ByVal Noeud As TreeNode)
        Dim CollPostes As Virtualia.Ressources.Datas.ObjetPosteFonctionnel = _
                                WebFct.PointeurUtilisateur.PointeurParametres.PointeurPostesFonctionnels
        Dim LstFiches As List(Of Virtualia.TablesObjet.ShemaREF.PST_IDENTIFICATION)
        Dim FicheRef As Virtualia.TablesObjet.ShemaREF.PST_IDENTIFICATION
        Dim ClefListe As String
        Dim IndiceA As Integer
        Dim NoeudN1 As TreeNode = Nothing
        Dim CptSel As Integer = 0
        Dim DateJour As String = WebFct.ViRhDates.DateduJour(False)
        Dim SiInterne As Boolean = True

        If CollPostes Is Nothing Then
            EtiStatus1.Text = "Aucune référence"
            Exit Sub
        End If

        Select Case HSelRadio.Value
            Case Is = "0" 'Liste Alpha
                TreeListeRef.MaxDataBindDepth = 0
                LstFiches = CollPostes.ListeDesPostes_Alpha(WebFct.PointeurContexte.SysRef_Lettre, V_NomTable)
                If LstFiches.Count = 0 Then
                    EtiStatus1.Text = "Aucune référence"
                    Exit Sub
                End If
                For IndiceA = 0 To LstFiches.Count - 1
                    FicheRef = LstFiches.Item(IndiceA)
                    ClefListe = FicheRef.Ide_Dossier & VI.Tild & FicheRef.Intitule & VI.Tild & _
                                    FicheRef.Numero_du_poste & VI.Tild & VI.PointdeVue.PVuePosteFct
                    NoeudN1 = AjouterNoeudValeur(Noeud, FicheRef.Intitule & " - " & _
                                                 FicheRef.Numero_du_poste & " - " & _
                                                 FicheRef.Affectation_Organigramme("", 1), ClefListe)
                    CptSel += 1
                Next IndiceA

            Case Is = "1" 'Liste Organisée
                Dim LuN1 As String
                Dim LuN2 As String
                Dim LuValeur As String
                Dim RuptN1 As String = "(Vide)"
                Dim RuptN2 As String = "(Vide)"
                Dim NoeudN2 As TreeNode = Nothing
                Dim VNoeud As TreeNode = Nothing

                If HSelMode.Value = "1" Then
                    LstFiches = CollPostes.ListeDesPostes(DateJour, V_NomTable)
                    TreeListeRef.MaxDataBindDepth = 1
                Else
                    LstFiches = CollPostes.ListeDesPostes(DateJour, "")
                    TreeListeRef.MaxDataBindDepth = 2
                End If
                If LstFiches.Count = 0 Then
                    EtiStatus1.Text = "Aucune référence"
                    Exit Sub
                End If

                For IndiceA = 0 To LstFiches.Count - 1
                    FicheRef = LstFiches.Item(IndiceA)
                    LuN1 = FicheRef.Affectation_Organigramme("", 1)
                    LuN2 = FicheRef.Affectation_Organigramme("", 2)
                    LuValeur = FicheRef.Intitule

                    If HSelMode.Value = "1" Then
                        NoeudN1 = Noeud
                    Else
                        Select Case LuN1
                            Case Is <> RuptN1
                                RuptN1 = LuN1
                                RuptN2 = "(Vide)"
                                ClefListe = FicheRef.Ide_Dossier & VI.Tild & LuN1 & VI.Tild & _
                                        LuN1 & VI.Tild & VI.PointdeVue.PVuePosteFct
                                NoeudN1 = AjouterNoeudValeur(Noeud, LuN1, ClefListe)
                        End Select
                    End If
                    Select Case LuN2
                        Case Is <> RuptN2
                            RuptN2 = LuN2
                            ClefListe = FicheRef.Ide_Dossier & VI.Tild & LuN2 & VI.Tild & _
                                    LuN1 & LuN2 & VI.Tild & VI.PointdeVue.PVuePosteFct
                            NoeudN2 = AjouterNoeudValeur(NoeudN1, LuN2, ClefListe)
                    End Select
                    ClefListe = FicheRef.Ide_Dossier & VI.Tild & LuValeur & VI.Tild & _
                                    LuN1 & LuN2 & LuValeur & VI.Tild & VI.PointdeVue.PVuePosteFct
                    VNoeud = AjouterNoeudValeur(NoeudN2, LuValeur, ClefListe)
                    CptSel += 1
                Next IndiceA
        End Select

        Try
            If WsVirtuelPath <> "" Then
                Select Case HSelMode.Value
                    Case Is = "0"
                        TreeListeRef.FindNode(WsVirtuelPath).Selected = False
                    Case Else
                        TreeListeRef.FindNode(WsVirtuelPath).Selected = True
                End Select
                TreeListeRef.FindNode(WsVirtuelPath).Parent.Expanded = True
            End If
        Catch ex As Exception
            Exit Try
        End Try

        Select Case CptSel
            Case Is = 1
                EtiStatus1.Text = "Une référence"
            Case Else
                EtiStatus1.Text = CptSel.ToString & " références"
        End Select

        Dim NoeudRecherche As TreeNode
        NoeudRecherche = AlleraInfoDico(0, True, "")
        If NoeudRecherche IsNot Nothing Then
            EtiSecondaire.Text = NoeudRecherche.Text
            EtiPrincipal.Text = NoeudRecherche.Parent.Text
        End If

        If HSelMode.Value = "1" Then
            Noeud.ExpandAll()
        End If
    End Sub

    Private Sub FaireListeMissions(ByVal Noeud As TreeNode)
        Dim CollMission As Virtualia.Ressources.Datas.ObjetMission = _
                                WebFct.PointeurUtilisateur.PointeurParametres.PointeurFraisMission
        Dim LstFiches As List(Of Virtualia.TablesObjet.ShemaREF.MIS_CARACTERISTIC)
        Dim FicheRef As Virtualia.TablesObjet.ShemaREF.MIS_CARACTERISTIC
        Dim ClefListe As String
        Dim IndiceA As Integer
        Dim NoeudN1 As TreeNode = Nothing
        Dim CptSel As Integer = 0
        Dim DateJour As String = WebFct.ViRhDates.DateduJour(False)

        If CollMission Is Nothing Then
            EtiStatus1.Text = "Aucune référence"
            Exit Sub
        End If
        If HSelMode.Value = "0" Then
            V_NomTable = Strings.Right(WebFct.ViRhDates.DateduJour(False), 4)
        End If

        Select Case HSelRadio.Value
            Case Is = "0" 'Liste Alpha
                TreeListeRef.MaxDataBindDepth = 0
                LstFiches = CollMission.ListeDesMissions_Alpha(WebFct.PointeurContexte.SysRef_Lettre, V_NomTable)
                If LstFiches.Count = 0 Then
                    EtiStatus1.Text = "Aucune référence"
                    Exit Sub
                End If
                For IndiceA = 0 To LstFiches.Count - 1
                    FicheRef = LstFiches.Item(IndiceA)
                    ClefListe = FicheRef.Ide_Dossier & VI.Tild & FicheRef.Intitule & VI.Tild & _
                                    FicheRef.Numero & VI.Tild & VI.PointdeVue.PVueMission
                    NoeudN1 = AjouterNoeudValeur(Noeud, FicheRef.Intitule & " - " & _
                                                 FicheRef.ObjetdelaMission & " - ", ClefListe)
                    CptSel += 1
                Next IndiceA

            Case Is = "1" 'Liste Organisée
                Dim LuN1 As String
                Dim LuN2 As String
                Dim LuValeur As String
                Dim RuptN1 As String = "(Vide)"
                Dim RuptN2 As String = "(Vide)"
                Dim NoeudN2 As TreeNode = Nothing
                Dim VNoeud As TreeNode = Nothing

                LstFiches = CollMission.ListeDesMissions(V_NomTable, 0)
                TreeListeRef.MaxDataBindDepth = 2
                If LstFiches.Count = 0 Then
                    EtiStatus1.Text = "Aucune référence"
                    Exit Sub
                End If

                For IndiceA = 0 To LstFiches.Count - 1
                    FicheRef = LstFiches.Item(IndiceA)
                    LuN1 = WebFct.ViRhDates.MoisEnClair(FicheRef.MoisMission)
                    LuN2 = FicheRef.ObjetdelaMission
                    LuValeur = FicheRef.Intitule

                    Select Case LuN1
                        Case Is <> RuptN1
                            RuptN1 = LuN1
                            RuptN2 = "(Vide)"
                            ClefListe = FicheRef.Ide_Dossier & VI.Tild & LuN1 & VI.Tild & _
                                    LuN1 & VI.Tild & VI.PointdeVue.PVueMission
                            NoeudN1 = AjouterNoeudValeur(Noeud, LuN1, ClefListe)
                    End Select
                    Select Case LuN2
                        Case Is <> RuptN2
                            RuptN2 = LuN2
                            ClefListe = FicheRef.Ide_Dossier & VI.Tild & LuN2 & VI.Tild & _
                                    LuN1 & LuN2 & VI.Tild & VI.PointdeVue.PVueMission
                            NoeudN2 = AjouterNoeudValeur(NoeudN1, LuN2, ClefListe)
                    End Select
                    ClefListe = FicheRef.Ide_Dossier & VI.Tild & LuValeur & VI.Tild & _
                                    LuN1 & LuN2 & LuValeur & VI.Tild & VI.PointdeVue.PVueMission
                    VNoeud = AjouterNoeudValeur(NoeudN2, LuValeur, ClefListe)
                    CptSel += 1
                Next IndiceA
        End Select

        Try
            If WsVirtuelPath <> "" Then
                Select Case HSelMode.Value
                    Case Is = "0"
                        TreeListeRef.FindNode(WsVirtuelPath).Selected = False
                    Case Else
                        TreeListeRef.FindNode(WsVirtuelPath).Selected = True
                End Select
                TreeListeRef.FindNode(WsVirtuelPath).Parent.Expanded = True
            End If
        Catch ex As Exception
            Exit Try
        End Try

        Select Case CptSel
            Case Is = 1
                EtiStatus1.Text = "Une référence"
            Case Else
                EtiStatus1.Text = CptSel.ToString & " références"
        End Select

        Dim NoeudRecherche As TreeNode
        NoeudRecherche = AlleraInfoDico(0, True, "")
        If NoeudRecherche IsNot Nothing Then
            EtiSecondaire.Text = NoeudRecherche.Text
            EtiPrincipal.Text = NoeudRecherche.Parent.Text
        End If

        If HSelMode.Value = "1" Then
            Noeud.ExpandAll()
        End If
    End Sub

    Private Sub FaireListeClassique(ByVal Noeud As TreeNode)
        Dim ClefListe As String
        Dim IndiceA As Integer
        Dim NoeudN1 As TreeNode = Nothing
        Dim CptSel As Integer = 0

        Select Case V_PointdeVue
            Case VI.PointdeVue.PVueEtablissement
                Dim CollWork As Virtualia.Ressources.Datas.ObjetEtablissement = _
                                WebFct.PointeurUtilisateur.PointeurParametres.PointeurEtablissements
                Dim LstFiches As List(Of Virtualia.TablesObjet.ShemaREF.ETA_IDENTITE)
                Dim FicheRef As Virtualia.TablesObjet.ShemaREF.ETA_IDENTITE
                If CollWork Is Nothing Then
                    EtiStatus1.Text = "Aucune référence"
                    Exit Sub
                End If
                TreeListeRef.MaxDataBindDepth = 0
                LstFiches = CollWork.ListeDesEtablissements(WebFct.PointeurContexte.SysRef_Lettre)
                If LstFiches.Count = 0 Then
                    EtiStatus1.Text = "Aucune référence"
                    Exit Sub
                End If
                For IndiceA = 0 To LstFiches.Count - 1
                    FicheRef = LstFiches.Item(IndiceA)
                    ClefListe = FicheRef.Ide_Dossier & VI.Tild & FicheRef.Denomination & VI.Tild & _
                                    FicheRef.Denomination & VI.Tild & VI.PointdeVue.PVueEtablissement
                    NoeudN1 = AjouterNoeudValeur(Noeud, FicheRef.Denomination, ClefListe)
                    CptSel += 1
                Next IndiceA

            Case VI.PointdeVue.PVuePays
                Dim CollWork As Virtualia.Ressources.Datas.ObjetPays = _
                                WebFct.PointeurUtilisateur.PointeurParametres.PointeurPays
                Dim LstFiches As List(Of Virtualia.TablesObjet.ShemaREF.PAYS_DESCRIPTION)
                Dim FicheRef As Virtualia.TablesObjet.ShemaREF.PAYS_DESCRIPTION
                If CollWork Is Nothing Then
                    EtiStatus1.Text = "Aucune référence"
                    Exit Sub
                End If
                TreeListeRef.MaxDataBindDepth = 0
                LstFiches = CollWork.ListeDesPays(WebFct.PointeurContexte.SysRef_Lettre)
                If LstFiches.Count = 0 Then
                    EtiStatus1.Text = "Aucune référence"
                    Exit Sub
                End If
                For IndiceA = 0 To LstFiches.Count - 1
                    FicheRef = LstFiches.Item(IndiceA)
                    ClefListe = FicheRef.Ide_Dossier & VI.Tild & FicheRef.Intitule & VI.Tild & _
                                    FicheRef.Intitule & VI.Tild & VI.PointdeVue.PVuePays
                    NoeudN1 = AjouterNoeudValeur(Noeud, FicheRef.Intitule, ClefListe)
                    CptSel += 1
                Next IndiceA

            Case VI.PointdeVue.PVueInterface
                Dim CollWork As Virtualia.Ressources.Datas.ObjetLogicielExterne = _
                                WebFct.PointeurUtilisateur.PointeurParametres.PointeurLogicielExterne
                Dim LstFiches As List(Of Virtualia.TablesObjet.ShemaREF.OUT_LOGICIEL)
                Dim FicheRef As Virtualia.TablesObjet.ShemaREF.OUT_LOGICIEL
                If CollWork Is Nothing Then
                    EtiStatus1.Text = "Aucune référence"
                    Exit Sub
                End If
                TreeListeRef.MaxDataBindDepth = 0
                LstFiches = CollWork.ListeDesLogiciels
                If LstFiches.Count = 0 Then
                    EtiStatus1.Text = "Aucune référence"
                    Exit Sub
                End If
                For IndiceA = 0 To LstFiches.Count - 1
                    FicheRef = LstFiches.Item(IndiceA)
                    ClefListe = FicheRef.Ide_Dossier & VI.Tild & FicheRef.Nom & VI.Tild & _
                                    FicheRef.Nom & VI.Tild & VI.PointdeVue.PVueInterface
                    NoeudN1 = AjouterNoeudValeur(Noeud, FicheRef.Nom, ClefListe)
                    CptSel += 1
                Next IndiceA

            Case VI.PointdeVue.PVueItineraire
                Dim CollWork As Virtualia.Ressources.Datas.ObjetItineraire = _
                                WebFct.PointeurUtilisateur.PointeurParametres.PointeurItineraire
                Dim LstFiches As List(Of Virtualia.TablesObjet.ShemaREF.ITI_DISTANCE)
                Dim FicheRef As Virtualia.TablesObjet.ShemaREF.ITI_DISTANCE
                If CollWork Is Nothing Then
                    EtiStatus1.Text = "Aucune référence"
                    Exit Sub
                End If
                Dim LuN1 As String
                Dim LuValeur As String
                Dim RuptN1 As String = "(Vide)"
                Dim VNoeud As TreeNode = Nothing

                TreeListeRef.MaxDataBindDepth = 1
                LstFiches = CollWork.ListeDesVilles
                If LstFiches.Count = 0 Then
                    EtiStatus1.Text = "Aucune référence"
                    Exit Sub
                End If
                For IndiceA = 0 To LstFiches.Count - 1
                    FicheRef = LstFiches.Item(IndiceA)
                    LuN1 = FicheRef.Villedepart
                    LuValeur = FicheRef.Villearrivee
                    Select Case LuN1
                        Case Is <> RuptN1
                            RuptN1 = LuN1
                            ClefListe = FicheRef.Ide_Dossier & VI.Tild & LuN1 & VI.Tild & _
                                    LuN1 & VI.Tild & VI.PointdeVue.PVueItineraire
                            NoeudN1 = AjouterNoeudValeur(Noeud, LuN1, ClefListe)
                    End Select
                    ClefListe = FicheRef.Ide_Dossier & VI.Tild & LuValeur & VI.Tild & _
                                    LuN1 & LuValeur & VI.Tild & VI.PointdeVue.PVueItineraire
                    VNoeud = AjouterNoeudValeur(NoeudN1, LuValeur, ClefListe)
                    CptSel += 1
                Next IndiceA

        End Select

        If WsVirtuelPath <> "" Then
            Select Case HSelMode.Value
                Case Is = "0"
                    TreeListeRef.FindNode(WsVirtuelPath).Selected = False
                Case Else
                    TreeListeRef.FindNode(WsVirtuelPath).Selected = True
            End Select
            TreeListeRef.FindNode(WsVirtuelPath).Parent.Expanded = True
        End If

        Select Case CptSel
            Case Is = 1
                EtiStatus1.Text = "Une référence"
            Case Else
                EtiStatus1.Text = CptSel.ToString & " références"
        End Select

        Dim NoeudRecherche As TreeNode
        NoeudRecherche = AlleraInfoDico(0, True, "")
        If NoeudRecherche IsNot Nothing Then
            EtiSecondaire.Text = NoeudRecherche.Text
            EtiPrincipal.Text = NoeudRecherche.Parent.Text
        End If

        If HSelMode.Value = "1" Then
            Noeud.ExpandAll()
        End If
    End Sub

    Private Sub FaireListeLolf(ByVal Noeud As TreeNode)
        Dim CollLolf As Virtualia.Ressources.Datas.ObjetLOLF = _
                                WebFct.PointeurUtilisateur.PointeurParametres.PointeurLOLF
        Dim LstFiches As List(Of Virtualia.TablesObjet.ShemaREF.LOLF_MISSION)
        Dim FicheRef As Virtualia.TablesObjet.ShemaREF.LOLF_MISSION
        Dim LstPgm As List(Of Virtualia.TablesObjet.ShemaREF.LOLF_PROGRAMME)
        Dim FichePgm As Virtualia.TablesObjet.ShemaREF.LOLF_PROGRAMME
        Dim LstAction As List(Of Virtualia.TablesObjet.ShemaREF.LOLF_ACTION)
        Dim FicheAction As Virtualia.TablesObjet.ShemaREF.LOLF_ACTION
        Dim LstBop As List(Of Virtualia.TablesObjet.ShemaREF.LOLF_BOP)
        Dim FicheBop As Virtualia.TablesObjet.ShemaREF.LOLF_BOP
        Dim LstAE As List(Of Virtualia.TablesObjet.ShemaREF.LOLF_AUTORISATION_EMPLOI)
        Dim FicheAE As Virtualia.TablesObjet.ShemaREF.LOLF_AUTORISATION_EMPLOI
        Dim LstUO As List(Of Virtualia.TablesObjet.ShemaREF.LOLF_UO)
        Dim FicheUO As Virtualia.TablesObjet.ShemaREF.LOLF_UO
        Dim ClefListe As String
        Dim IndiceA As Integer
        Dim IndiceB As Integer
        Dim IndiceC As Integer
        Dim IndiceD As Integer
        Dim LuN1 As String
        Dim LuN2 As String
        Dim LuN3 As String
        Dim LuN4 As String
        Dim LuValeur As String
        Dim NoeudN1 As TreeNode = Nothing
        Dim NoeudN2 As TreeNode = Nothing
        Dim NoeudN3 As TreeNode = Nothing
        Dim NoeudN4 As TreeNode = Nothing
        Dim VNoeud As TreeNode = Nothing
        Dim CptSel As Integer = 0

        If CollLolf Is Nothing Then
            EtiStatus1.Text = "Aucune référence"
            Exit Sub
        End If
        LstFiches = CollLolf.ListeDesMissions()
        TreeListeRef.MaxDataBindDepth = 4
        If LstFiches.Count = 0 Then
            EtiStatus1.Text = "Aucune référence"
            Exit Sub
        End If

        For IndiceA = 0 To LstFiches.Count - 1
            FicheRef = LstFiches.Item(IndiceA)
            LuN1 = FicheRef.Intitule
            ClefListe = FicheRef.Ide_Dossier & VI.Tild & LuN1 & VI.Tild & _
                    LuN1 & VI.Tild & VI.PointdeVue.PVueLolf
            NoeudN1 = AjouterNoeudValeur(Noeud, LuN1, ClefListe)

            LstPgm = FicheRef.ListedesProgrammes("")
            If LstPgm IsNot Nothing Then
                For IndiceB = 0 To LstPgm.Count - 1
                    FichePgm = LstPgm.Item(IndiceB)
                    LuN2 = FichePgm.Numero & " - " & FichePgm.Intitule
                    ClefListe = FicheRef.Ide_Dossier & VI.Tild & LuN2 & VI.Tild & _
                            LuN1 & LuN2 & VI.Tild & VI.PointdeVue.PVueLolf
                    NoeudN2 = AjouterNoeudValeur(NoeudN1, LuN2, ClefListe)

                    LstAction = FichePgm.ListedesActions("Numero")
                    If LstAction IsNot Nothing Then
                        LuN3 = "Actions et Sous-actions"
                        ClefListe = FicheRef.Ide_Dossier & VI.Tild & LuN2 & VI.Tild & _
                                LuN1 & LuN2 & LuN3 & VI.Tild & VI.PointdeVue.PVueLolf
                        NoeudN3 = AjouterNoeudValeur(NoeudN2, LuN3, ClefListe)

                        For IndiceC = 0 To LstAction.Count - 1
                            FicheAction = LstAction.Item(IndiceC)
                            LuN4 = FicheAction.NumerodelAction & " - " & FicheAction.Intitule
                            ClefListe = FicheRef.Ide_Dossier & VI.Tild & LuN4 & VI.Tild & _
                                LuN1 & LuN2 & LuN3 & LuN4 & VI.Tild & VI.PointdeVue.PVueLolf
                            NoeudN4 = AjouterNoeudValeur(NoeudN3, LuN4, ClefListe)

                            LstAE = FicheAction.ListedesAutorisations("Numero")
                            For IndiceD = 0 To LstAE.Count - 1
                                FicheAE = LstAE.Item(IndiceD)
                                LuValeur = FicheAE.NumerodelAutorisation & " - " & FicheAE.CategorieFonctionnelle & " ( Nombre = " & FicheAE.Nombre.ToString & ")"
                                ClefListe = FicheRef.Ide_Dossier & VI.Tild & LuValeur & VI.Tild & _
                                    LuN1 & LuN2 & LuN3 & LuN4 & LuValeur & VI.Tild & VI.PointdeVue.PVueLolf
                                VNoeud = AjouterNoeudValeur(NoeudN4, LuValeur, ClefListe)
                                CptSel += 1
                            Next IndiceD
                        Next IndiceC
                    End If

                    LstBop = FichePgm.ListedesBOPs("Numero")
                    If LstBop IsNot Nothing Then
                        LuN3 = "Budget Opérationnel de Programme"
                        ClefListe = FicheRef.Ide_Dossier & VI.Tild & LuN2 & VI.Tild & _
                                LuN1 & LuN2 & LuN3 & VI.Tild & VI.PointdeVue.PVueLolf
                        NoeudN3 = AjouterNoeudValeur(NoeudN2, LuN3, ClefListe)

                        For IndiceC = 0 To LstBop.Count - 1
                            FicheBop = LstBop.Item(IndiceC)
                            LuN4 = FicheBop.NumeroduBOP & " - " & FicheBop.Intitule
                            ClefListe = FicheRef.Ide_Dossier & VI.Tild & LuN4 & VI.Tild & _
                                LuN1 & LuN2 & LuN3 & LuN4 & VI.Tild & VI.PointdeVue.PVueLolf
                            NoeudN4 = AjouterNoeudValeur(NoeudN3, LuN4, ClefListe)

                            LstUO = FicheBop.ListedesUOs("Numero")
                            For IndiceD = 0 To LstUO.Count - 1
                                FicheUO = LstUO.Item(IndiceD)
                                LuValeur = FicheUO.NumerodelUO & " - " & FicheUO.Intitule
                                ClefListe = FicheRef.Ide_Dossier & VI.Tild & LuValeur & VI.Tild & _
                                    LuN1 & LuN2 & LuN3 & LuN4 & LuValeur & VI.Tild & VI.PointdeVue.PVueLolf
                                VNoeud = AjouterNoeudValeur(NoeudN4, LuValeur, ClefListe)
                                CptSel += 1
                            Next IndiceD
                        Next IndiceC
                    End If
                Next IndiceB
            End If
        Next IndiceA

        Try
            If WsVirtuelPath <> "" Then
                Select Case HSelMode.Value
                    Case Is = "0"
                        TreeListeRef.FindNode(WsVirtuelPath).Selected = False
                    Case Else
                        TreeListeRef.FindNode(WsVirtuelPath).Selected = True
                End Select
                TreeListeRef.FindNode(WsVirtuelPath).Parent.Expanded = True
            End If
        Catch ex As Exception
            Exit Try
        End Try

        Select Case CptSel
            Case Is = 1
                EtiStatus1.Text = "Une référence"
            Case Else
                EtiStatus1.Text = CptSel.ToString & " références"
        End Select

        Dim NoeudRecherche As TreeNode
        NoeudRecherche = AlleraInfoDico(0, True, "")
        If NoeudRecherche IsNot Nothing Then
            EtiSecondaire.Text = NoeudRecherche.Text
            EtiPrincipal.Text = NoeudRecherche.Parent.Text
        End If

        Noeud.ExpandAll()
    End Sub

    Protected Sub RadioCheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles RadioV0.CheckedChanged, _
        RadioV1.CheckedChanged, RadioV2.CheckedChanged
        HSelRadio.Value = Strings.Right(CType(sender, System.Web.UI.WebControls.RadioButton).ID, 1)
        RadioV0.Font.Bold = False
        RadioV1.Font.Bold = False
        RadioV2.Font.Bold = False
        CType(sender, System.Web.UI.WebControls.RadioButton).Font.Bold = True

        WebFct.PointeurContexte.SysRef_Lettre = ""
        VRecherche.Text = ""

    End Sub

    Private Function AjouterNoeudValeur(ByVal Noeud As TreeNode, ByVal Valeur As String, ByVal Clef As String, Optional ByVal SiSelSurParent As Boolean = False) As TreeNode
        Dim VImage As String = ""
        Dim VClef As String = Strings.Replace(Clef, "/", "")
        Select Case Noeud.Depth
            Case Is = TreeListeRef.MaxDataBindDepth
                VImage = "~/Images/Armoire/FicheJaune.bmp"
            Case Else
                Select Case Noeud.Depth
                    Case Is = 0
                        VImage = "~/Images/Armoire/BleuFonceFermer16.bmp"
                    Case Is = 1
                        VImage = "~/Images/Armoire/TurquoiseFermer16.bmp"
                    Case Is = 2
                        VImage = "~/Images/Armoire/RougeCarminFermer16.bmp"
                    Case Is = 3
                        VImage = "~/Images/Armoire/BleuClairFermer16.bmp"
                    Case Is = 4
                        VImage = "~/Images/Armoire/GrisFonceFermer16.bmp"
                End Select
        End Select

        Dim NewNoeud As TreeNode = New TreeNode(Valeur, VClef)
        NewNoeud.ImageUrl = VImage
        NewNoeud.PopulateOnDemand = False
        Select Case Noeud.Depth
            Case Is = TreeListeRef.MaxDataBindDepth
                NewNoeud.SelectAction = TreeNodeSelectAction.Select
            Case Else
                Select Case SiSelSurParent
                    Case True
                        Select Case Noeud.Depth
                            Case Is = TreeListeRef.MaxDataBindDepth - 1
                                NewNoeud.SelectAction = TreeNodeSelectAction.Expand
                            Case Else
                                NewNoeud.SelectAction = TreeNodeSelectAction.Expand
                        End Select
                    Case False
                        NewNoeud.SelectAction = TreeNodeSelectAction.Expand
                End Select
        End Select
        Noeud.ChildNodes.Add(NewNoeud)

        If HSelValeur.Value <> "" Then
            If NewNoeud.Text = HSelValeur.Value Then
                WsVirtuelPath = NewNoeud.ValuePath
            End If
        End If

        Return NewNoeud
    End Function

End Class
