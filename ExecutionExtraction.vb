﻿Option Strict Off
Option Explicit On
Option Compare Text
Imports Microsoft.VisualBasic
Imports VI = Virtualia.Systeme.Constantes
Namespace Script
    Public Class ExecutionExtraction
        Implements IDisposable
        Private disposedValue As Boolean = False        ' Pour détecter les appels redondants
        Private WsPointdeVue As Short
        Private AppObjetUser As Virtualia.Net.Session.ObjetSession
        '
        Private WsListeRequetes As ArrayList 'Liste des requetes effectues = ObjetSelection
        Private WsTabIde() As Integer 'Préselection d'identifiants
        Private WsListeRes As List(Of Virtualia.Ressources.Datas.ItemSelection)
        Private WsTabObjet As ArrayList
        Private WsTabColonnes As ArrayList
        Private WsTabFiltre As ArrayList
        Private WsObjetHisto As Short
        '
        Private WsLibelleColonne As System.Text.StringBuilder
        Private WsChaineDatas As System.Text.StringBuilder

        Public ReadOnly Property Resultat() As String
            Get
                Return WsChaineDatas.ToString
            End Get
        End Property

        Public ReadOnly Property LibelleColonnes() As String
            Get
                Return WsLibelleColonne.ToString
            End Get
        End Property

        Public WriteOnly Property TableIdentifiants() As Integer()
            Set(ByVal value As Integer())
                WsTabIde = value
            End Set
        End Property

        Public Function Executer(ByVal VConditions As ArrayList, ByVal VScript As ArrayList) As Integer
            '** Recherche de la table des identifiants 1. Panel 2. CMC 3. Armoire courante
            If WsTabIde Is Nothing Then
                WsTabIde = AppObjetUser.PointeurContexte.ListeIdentifiantsPanel
                If WsTabIde Is Nothing Then
                    If AppObjetUser.PointeurResultatCMC(WsPointdeVue) IsNot Nothing Then
                        WsTabIde = AppObjetUser.PointeurResultatCMC(WsPointdeVue).ListeIdentifiants
                    End If
                End If
            End If
            If WsTabIde Is Nothing Then
                WsTabIde = AppObjetUser.ItemArmoire(AppObjetUser.ListeArmoires.Count - 1).TabIdentifiants()
                If WsTabIde Is Nothing Then
                    WsTabIde = AppObjetUser.ItemArmoire(1).TabIdentifiants
                End If
            End If

            Call AnalyserScript(VScript, VConditions)
            Call FaireRequetes(VConditions)
            If WsListeRequetes.Count = 0 Then
                Return 0
                Exit Function
            End If
            Call FusionnerLesRequetes()
            If WsListeRes.Count = 0 Then
                Return 0
                Exit Function
            End If
            Call TraiterResultat()
            Return WsListeRes.Count
        End Function

        Private Sub TraiterResultat()
            Dim I As Integer
            Dim H As Integer
            Dim K As Integer
            Dim ListeIdeAbsentsArmoire As New ArrayList

            WsLibelleColonne = New System.Text.StringBuilder
            '** Libellé des colonnes **********
            For I = 0 To WsTabColonnes.Count - 1
                K = RepetitionColonne(I)
                If K = 0 Then
                    WsLibelleColonne.Append(IntituleColonne(I) & VI.Tild)
                Else
                    For H = 0 To K - 1
                        WsLibelleColonne.Append(IntituleColonne(I) & "_" & (H + 1) & VI.Tild)
                    Next H
                End If
            Next I
            WsLibelleColonne.Append("Clef")

            'Tri et extraction
            Dim ResTrie = New List(Of Virtualia.Ressources.Datas.ItemSelection)
            Dim FicheRes As Virtualia.Ressources.Datas.ItemSelection
            WsChaineDatas = New System.Text.StringBuilder

            ResTrie = (From instance In WsListeRes Select instance Where instance.Identifiant > 0 Order By instance.CritereTri Ascending).ToList
            If ResTrie.Count = 0 Then
                Exit Sub
            End If
            Dim IndiceC As IEnumerator = ResTrie.GetEnumerator
            While IndiceC.MoveNext
                FicheRes = CType(IndiceC.Current, Virtualia.Ressources.Datas.ItemSelection)
                WsChaineDatas.Append(FicheRes.LigneExtraite)
            End While
        End Sub

        Private Sub AnalyserScript(ByVal CacheScript As ArrayList, ByVal Param As ArrayList)
            Dim I As Integer = 0
            Dim K As Integer
            Dim R As Integer
            Dim LigneScript As ArrayList
            Dim Pvue As Short
            Dim NumObjet As Short
            Dim NumInfo As Short
            Dim AFaire As Boolean
            Dim TableauTmp(0) As String
            Dim SiHisto As Boolean = False
            Dim PositionRelativeColonne As Integer = 0

            '*********** Un seul Accés par objet  et par dossier *************
            If WsTabObjet IsNot Nothing Then
                WsTabObjet.Clear()
                WsTabObjet = Nothing
            End If
            If WsTabFiltre IsNot Nothing Then
                WsTabFiltre.Clear()
                WsTabFiltre = Nothing
            End If
            If WsTabColonnes IsNot Nothing Then
                WsTabColonnes.Clear()
                WsTabColonnes = Nothing
            End If
            WsTabObjet = New ArrayList
            WsTabFiltre = New ArrayList
            WsTabColonnes = New ArrayList

            If Param(3).ToString <> "0" Then
                SiHisto = True
            End If

            For I = 0 To CacheScript.Count - 1
                LigneScript = CType(CacheScript(I), ArrayList)
                If LigneScript Is Nothing Then
                    Exit For
                End If
                Try
                    Pvue = CShort(LigneScript(0))
                    NumObjet = CShort(LigneScript(1))
                    NumInfo = CShort(LigneScript(2))
                    If WsTabObjet.Count = 0 Then
                        WsTabObjet.Add(NumObjet.ToString & VI.Tild & NumInfo.ToString)
                        WsTabFiltre.Add("")
                        If LigneScript.Count >= 9 Then
                            For R = 9 To LigneScript.Count - 1
                                WsTabFiltre(0) &= LigneScript(R).ToString & VI.PointVirgule
                            Next R
                        End If
                        WsTabFiltre(0) &= VI.Tild
                    Else
                        AFaire = True
                        For K = 0 To WsTabObjet.Count - 1
                            TableauTmp = Strings.Split(WsTabObjet(K).ToString, VI.Tild, -1)
                            If CShort(TableauTmp(0)) = NumObjet Then
                                If NumInfo < 500 Then
                                    WsTabObjet(K) = WsTabObjet(K).ToString & VI.PointVirgule & NumInfo.ToString
                                    If LigneScript.Count >= 9 Then
                                        For R = 9 To LigneScript.Count - 1
                                            WsTabFiltre(K) &= LigneScript(R).ToString & VI.PointVirgule
                                        Next R
                                    End If
                                    WsTabFiltre(K) &= VI.Tild
                                    AFaire = False
                                    Exit For
                                End If
                            End If
                        Next K
                        If AFaire = True Then
                            WsTabObjet.Add(NumObjet.ToString & VI.Tild & NumInfo.ToString)
                            WsTabFiltre.Add("")
                            If LigneScript.Count >= 9 Then
                                For R = 9 To LigneScript.Count - 1
                                    WsTabFiltre(WsTabFiltre.Count - 1) &= LigneScript(R).ToString & VI.PointVirgule
                                Next R
                            End If
                            WsTabFiltre(WsTabFiltre.Count - 1) &= VI.Tild
                        End If
                    End If
                    If SiHisto = True Then
                        Select Case PointeurDicoObjet(Pvue, NumObjet).VNature
                            Case VI.TypeObjet.ObjetDate, VI.TypeObjet.ObjetDateRang, VI.TypeObjet.ObjetHeure, VI.TypeObjet.ObjetTableau
                                WsObjetHisto = NumObjet
                        End Select
                    End If
                    WsTabColonnes.Add(NumObjet.ToString & VI.PointVirgule & NumInfo.ToString _
                                      & VI.PointVirgule & LigneScript(7).ToString & VI.PointVirgule _
                                      & LigneScript(6).ToString & VI.PointVirgule & LigneScript(8).ToString & VI.PointVirgule _
                                      & LigneScript(5).ToString)

                Catch ex As Exception
                    Exit For
                End Try
            Next I
            '** Resolution contradiction eventuelle Histo et Répétition colonnes en lignes
            For I = 0 To WsTabColonnes.Count - 1
                If RepetitionColonne(I) > 0 Then
                    WsObjetHisto = 0
                    Exit For
                End If
            Next I

        End Sub

        Private Sub FaireRequetes(ByVal Param As ArrayList)
            Dim ObjetRequete As Virtualia.Net.Datas.ObjetSelection
            Dim I As Integer
            Dim K As Integer
            Dim R As Integer
            Dim TableauScript(0) As String
            Dim NumObjet As Short
            Dim ChaineSql As String
            Dim DateDebut As String = Param(1).ToString
            Dim DateFin As String = Param(2).ToString
            Dim SiHisto As Short = CShort(Param(3))
            Dim OpeComparaison As Short = VI.Operateurs.ET
            Dim SiPresenceObjet_1 As Boolean = False
            Dim NumExperte As Short
            Dim TableauFiltre(0) As String

            If WsListeRes IsNot Nothing Then
                WsListeRes.Clear()
                WsListeRes = Nothing
            End If
            WsListeRes = New List(Of Virtualia.Ressources.Datas.ItemSelection)

            If DateFin = "" Then
                DateFin = AppObjetUser.V_RhDates.DateduJour(False)
            End If
            If SiHisto <> 0 And WsObjetHisto > 0 Then
                If DateDebut = "" Then
                    DateDebut = "01/01/1980"
                End If
            Else
                WsObjetHisto = 0
            End If

            WsListeRequetes = New ArrayList
            For I = 0 To WsTabObjet.Count - 1
                TableauScript = Strings.Split(WsTabObjet(I).ToString, VI.Tild, -1)
                NumObjet = CShort(TableauScript(0))
                NumExperte = NumeroExperte(TableauScript(1))

                ObjetRequete = New Virtualia.Net.Datas.ObjetSelection(AppObjetUser, WsPointdeVue)

                If NumExperte = 0 And NumObjet > 0 Then
                    If NumObjet = 1 Then
                        SiPresenceObjet_1 = True
                    End If

                    If WsObjetHisto > 0 And WsObjetHisto = NumObjet Then
                        ChaineSql = ObjetRequete.RequeteObjetSql(NumObjet, TableauScript(1), _
                                    OpeComparaison, WsTabFiltre(I).ToString, DateDebut, DateFin, WsTabIde)
                    Else
                        If NombreColonnesRepetees(NumObjet, 99) > 0 Then
                            If DateDebut = "" Then
                                DateDebut = "01/01/1980"
                            End If
                            ChaineSql = ObjetRequete.RequeteObjetSql(NumObjet, TableauScript(1), _
                                    OpeComparaison, WsTabFiltre(I).ToString, DateDebut, DateFin, WsTabIde)
                        Else
                            ChaineSql = ObjetRequete.RequeteObjetSql(NumObjet, TableauScript(1), _
                                    OpeComparaison, WsTabFiltre(I).ToString, "", DateFin, WsTabIde)
                        End If
                    End If

                    K = ObjetRequete.SelectionDynamique(NumObjet, ChaineSql)

                Else

                    If NumExperte > 9000 Then
                        K = ObjetRequete.RequeteExperte(NumObjet, NumExperte, WsTabIde, AliasConstante(NumExperte), "")
                    Else
                        K = ObjetRequete.RequeteExperte(NumObjet, NumExperte, WsTabIde, DateDebut, DateFin)
                    End If

                End If

                If K > 0 Then
                    WsListeRequetes.Add(ObjetRequete)
                    TableauFiltre = Strings.Split(WsTabFiltre(I).ToString, VI.Tild, -1)
                    If TableauFiltre.Count > 0 Then
                        For R = 0 To TableauFiltre.Count - 1
                            If TableauFiltre(R) <> "" Then
                                WsTabIde = ObjetRequete.ListeIdentifiants
                                Exit For
                            End If
                        Next R
                    End If
                End If

            Next I

            If SiPresenceObjet_1 = False Then
                ObjetRequete = New Virtualia.Net.Datas.ObjetSelection(AppObjetUser, WsPointdeVue)
                ChaineSql = ObjetRequete.RequeteObjetSql(1, "2;3", _
                                OpeComparaison, "", "", DateFin, WsTabIde)
                K = ObjetRequete.SelectionDynamique(1, ChaineSql)
                WsListeRequetes.Add(ObjetRequete)
            End If

        End Sub

        Private Sub FusionnerLesRequetes()
            Dim I As Integer = 0
            Dim K As Integer = 0
            Dim ICol As Integer
            Dim IHisto As Integer
            Dim TableauData(0) As String
            Dim RequeteObjet As Virtualia.Net.Datas.ObjetSelection
            Dim LstResultat As List(Of Virtualia.Ressources.Datas.ItemSelection)
            Dim LigneResultat As Virtualia.Ressources.Datas.ItemSelection
            Dim LigneClone As Virtualia.Ressources.Datas.ItemSelection = Nothing
            Dim NombreColonnes As Integer = 0
            Dim NumObjet As Short
            Dim NumInfo As Short
            Dim NumColExtrait As Integer
            Dim PositionColonne As Integer = 0
            Dim NbColRepetees As Integer = 0

            If WsTabIde.Count = 0 Then
                Exit Sub
            End If

            '** Si Colonnes répétées Calcul du Nombre total de colonnes **********
            For I = 0 To WsTabColonnes.Count - 1
                K = RepetitionColonne(I)
                If K = 0 Then
                    NombreColonnes += 1
                Else
                    NombreColonnes += RepetitionColonne(I)
                End If
            Next I

            WsListeRes = New List(Of Virtualia.Ressources.Datas.ItemSelection)
            For K = 0 To WsTabIde.Count - 1
                LigneResultat = New Virtualia.Ressources.Datas.ItemSelection(WsTabIde(K), NombreColonnes)
                'Tri par défaut
                LigneResultat.TriParDefaut = True
                WsListeRes.Add(LigneResultat)
            Next K

            '** 
            For I = 0 To WsListeRequetes.Count - 1
                RequeteObjet = CType(WsListeRequetes(I), Virtualia.Net.Datas.ObjetSelection)
                NumObjet = RequeteObjet.NumeroObjet
                For K = 0 To WsTabIde.Count - 1
                    LstResultat = RequeteObjet.ListeResultat.FindAll(Function(Recherche) Recherche.Identifiant = WsTabIde(K))
                    If NumObjet = WsObjetHisto Then
                        LigneClone = Nothing
                    End If
                    If LstResultat.Count > 0 Then
                        LigneResultat = Nothing
                        For IHisto = 0 To LstResultat.Count - 1
                            If NumObjet <> WsObjetHisto And IHisto > NombreColonnesRepetees(NumObjet, 99) Then
                                Exit For
                            End If
                            Select Case IHisto
                                Case Is = 0
                                    LigneResultat = WsListeRes.Find(Function(Recherche) Recherche.Identifiant = WsTabIde(K))
                                    If NumObjet = WsObjetHisto Then
                                        LigneClone = New Virtualia.Ressources.Datas.ItemSelection(LigneResultat.Identifiant, NombreColonnes)
                                        For ICol = 0 To NombreColonnes - 1
                                            LigneClone.ChampExtrait(ICol) = LigneResultat.ChampExtrait(ICol)
                                        Next ICol
                                        LigneClone.TriParDefaut = LigneResultat.TriParDefaut
                                    End If
                                Case Else
                                    If NumObjet = WsObjetHisto Then
                                        LigneResultat = New Virtualia.Ressources.Datas.ItemSelection(WsTabIde(K), NombreColonnes)
                                        For ICol = 0 To NombreColonnes - 1
                                            LigneResultat.ChampExtrait(ICol) = LigneClone.ChampExtrait(ICol)
                                        Next ICol
                                        LigneResultat.TriParDefaut = LigneClone.TriParDefaut
                                        WsListeRes.Add(LigneResultat)
                                    End If
                            End Select
                            PositionColonne = 0
                            For ICol = 0 To WsTabColonnes.Count - 1
                                TableauData = Strings.Split(WsTabColonnes(ICol).ToString, VI.PointVirgule, -1)
                                NumObjet = CShort(TableauData(0))
                                NbColRepetees = CShort(TableauData(2))
                                If NumObjet = RequeteObjet.NumeroObjet Then
                                    NumInfo = CShort(TableauData(1))
                                    If IndexTabObjet(NumObjet, NumInfo) = I Then
                                        NumColExtrait = NumColCourante(NumObjet, NumInfo)
                                        If NumColExtrait < 0 Then
                                            Exit For
                                        End If
                                        Select Case NbColRepetees
                                            Case 0
                                                LigneResultat.ChampExtrait(PositionColonne) = DonneeMiseEnForme(ICol, LstResultat.Item(IHisto).ChampExtrait(NumColExtrait))
                                            Case Else
                                                LigneResultat.ChampExtrait(PositionColonne + IHisto) = DonneeMiseEnForme(ICol, LstResultat.Item(IHisto).ChampExtrait(NumColExtrait))
                                        End Select
                                    End If
                                End If
                                Select Case NbColRepetees
                                    Case Is = 0
                                        PositionColonne += 1
                                    Case Else
                                        PositionColonne += NbColRepetees
                                End Select
                            Next ICol
                        Next IHisto
                    End If
                Next K
            Next I
            '**
            For I = 0 To WsListeRequetes.Count - 1
                CType(WsListeRequetes(I), Virtualia.Net.Datas.ObjetSelection).Dispose()
            Next I
            WsListeRequetes.Clear()
        End Sub

        Private ReadOnly Property ListeCourante(ByVal NoObjet As Short) As Virtualia.Net.Datas.ObjetSelection
            Get
                Dim IndiceA As Integer
                For IndiceA = 0 To WsListeRequetes.Count - 1
                    If CType(WsListeRequetes.Item(IndiceA), Virtualia.Net.Datas.ObjetSelection).NumeroObjet = NoObjet Then
                        Return CType(WsListeRequetes.Item(IndiceA), Virtualia.Net.Datas.ObjetSelection)
                        Exit Property
                    End If
                Next IndiceA
                Return Nothing
            End Get
        End Property

        Private ReadOnly Property NumColCourante(ByVal NoObjet As Short, ByVal NoInfo As Short) As Integer
            Get
                Dim IndiceA As Integer
                Dim IndiceI As Integer
                Dim TableauObjet(0) As String
                Dim TableauInfo(0) As String
                Dim NumObjet As Short
                Dim NumInfo As Short
                For IndiceA = 0 To WsTabObjet.Count - 1
                    TableauObjet = Strings.Split(WsTabObjet(IndiceA).ToString, VI.Tild, -1)
                    NumObjet = CShort(TableauObjet(0))
                    If NumObjet = NoObjet Then
                        TableauInfo = Strings.Split(TableauObjet(1), VI.PointVirgule, -1)
                        For IndiceI = 0 To TableauInfo.Count - 1
                            NumInfo = CShort(TableauInfo(IndiceI))
                            If NumInfo = NoInfo Then
                                Return IndiceI
                                Exit Property
                            End If
                        Next IndiceI
                    End If
                Next IndiceA
                Return -1
            End Get
        End Property

        Private ReadOnly Property IntituleColonne(ByVal Index As Integer) As String
            Get
                Dim TableauObjet(0) As String
                Select Case Index
                    Case 0 To WsTabColonnes.Count - 1
                        TableauObjet = Strings.Split(WsTabColonnes(Index).ToString, VI.PointVirgule, -1)
                        Return TableauObjet(TableauObjet.Count - 1)
                        Exit Property
                End Select
                Return ""
            End Get
        End Property

        Private ReadOnly Property AliasConstante(ByVal NoInfo As Short) As String
            Get
                Dim TableauObjet(0) As String
                Dim NumInfo As Short
                Dim ICol As Integer

                For ICol = 0 To WsTabColonnes.Count - 1
                    TableauObjet = Strings.Split(WsTabColonnes(ICol).ToString, VI.PointVirgule, -1)
                    NumInfo = CShort(TableauObjet(1))
                    If NumInfo = NoInfo Then
                        Return TableauObjet(TableauObjet.Count - 1)
                    End If
                Next ICol
                Return ""
            End Get
        End Property

        Private ReadOnly Property MeilleureCleSecondaire(ByVal NoObjet As Short, ByVal NoInfo As Short) As Boolean
            Get
                Dim IndiceA As Integer
                Dim IndiceI As Integer
                Dim TableauObjet(0) As String
                Dim TableauInfo(0) As String
                Dim NumObjet As Short
                Dim NumInfo As Short
                For IndiceA = 0 To WsTabObjet.Count - 1
                    TableauObjet = Strings.Split(WsTabObjet(IndiceA).ToString, VI.Tild, -1)
                    NumObjet = CShort(TableauObjet(0))
                    If NumObjet = NoObjet Then
                        TableauInfo = Strings.Split(TableauObjet(1), VI.PointVirgule, -1)
                        For IndiceI = 0 To TableauInfo.Count - 1
                            NumInfo = CShort(TableauInfo(IndiceI))
                            If NumInfo = 0 Then
                                Return True
                            End If
                        Next IndiceI
                        For IndiceI = 0 To TableauInfo.Count - 1
                            NumInfo = CShort(TableauInfo(IndiceI))
                            If NumInfo = 1 Then
                                Return True
                            End If
                        Next IndiceI
                        Return True
                    End If
                Next IndiceA
                Return False
            End Get
        End Property

        Private ReadOnly Property NombreColonnesRepetees(ByVal NoObjet As Short, ByVal NoInfo As Short) As Short
            Get
                Dim TableauObjet(0) As String
                Dim I As Integer
                Dim NumObjet As Short
                Dim NumInfo As Short
                For I = 0 To WsTabColonnes.Count - 1
                    TableauObjet = Strings.Split(WsTabColonnes(I).ToString, VI.PointVirgule, -1)
                    NumObjet = CShort(TableauObjet(0))
                    NumInfo = CShort(TableauObjet(1))
                    If NumObjet = NoObjet Then
                        If NoInfo = 99 Then
                            Return CShort(TableauObjet(2))
                        End If
                        If NumInfo = NoInfo Then
                            Return CShort(TableauObjet(2))
                        End If
                    End If
                Next I
                Return 0
            End Get
        End Property

        Private ReadOnly Property RepetitionColonne(ByVal Index As Integer) As Short
            Get
                Dim TableauObjet(0) As String
                Select Case Index
                    Case 0 To WsTabColonnes.Count - 1
                        TableauObjet = Strings.Split(WsTabColonnes(Index).ToString, VI.PointVirgule, -1)
                        Return CShort(TableauObjet(2))
                        Exit Property
                End Select
                Return 0
            End Get
        End Property

        Private ReadOnly Property NumeroExperte(ByVal Chaine As String) As Short
            Get
                Dim TableauInfo(0) As String
                Dim NumInfo As Short

                TableauInfo = Strings.Split(Chaine, VI.PointVirgule, -1)
                NumInfo = CShort(TableauInfo(0))
                If NumInfo > 500 Then
                    Return NumInfo
                Else
                    Return 0
                End If
            End Get
        End Property

        Private ReadOnly Property IndexTabObjet(ByVal NoObjet As Short, ByVal NoInfo As Short) As Integer
            Get
                Dim IndiceA As Integer
                Dim IndiceI As Integer
                Dim TableauObjet(0) As String
                Dim TableauInfo(0) As String
                Dim NumObjet As Short
                Dim NumInfo As Short
                For IndiceA = 0 To WsTabObjet.Count - 1
                    TableauObjet = Strings.Split(WsTabObjet(IndiceA).ToString, VI.Tild, -1)
                    NumObjet = CShort(TableauObjet(0))
                    If NumObjet = NoObjet Then
                        TableauInfo = Strings.Split(TableauObjet(1), VI.PointVirgule, -1)
                        For IndiceI = 0 To TableauInfo.Count - 1
                            NumInfo = CShort(TableauInfo(IndiceI))
                            If NumInfo = NoInfo Then
                                Return IndiceA
                                Exit Property
                            End If
                        Next IndiceI
                    End If
                Next IndiceA
                Return -1
            End Get
        End Property

        Private ReadOnly Property DonneeMiseEnForme(ByVal Index As Integer, ByVal Valeur As String) As String
            Get
                Dim Chaine As String = Valeur.Trim
                Dim TableauObjet(0) As String
                Dim NumObjet As Short
                Dim NumInfo As Short
                Dim NatureInfo As Short
                Dim FormatInfo As Short
                Dim LongueurScript As Short
                Dim FormatScript As Short
                Dim FormatAlpha As String

                If Index > WsTabColonnes.Count - 1 Then
                    Return Chaine
                End If
                TableauObjet = Strings.Split(WsTabColonnes(Index).ToString, VI.PointVirgule, -1)
                NumObjet = CShort(TableauObjet(0))
                NumInfo = CShort(TableauObjet(1))
                FormatScript = CShort(TableauObjet(3))
                LongueurScript = CShort(TableauObjet(4))
                Select Case NumInfo
                    Case Is < 500
                        NatureInfo = PointeurDicoInfo(WsPointdeVue, NumObjet, NumInfo).VNature
                        FormatInfo = PointeurDicoInfo(WsPointdeVue, NumObjet, NumInfo).Format
                    Case Is > 9000
                        NatureInfo = 0
                        FormatInfo = 0
                    Case Else
                        NatureInfo = PointeurDicoExperte(WsPointdeVue, NumObjet, NumInfo).VNature
                        FormatInfo = PointeurDicoExperte(WsPointdeVue, NumObjet, NumInfo).Format
                End Select

                If Chaine = "" Then
                    Select Case NatureInfo
                        Case VI.NatureDonnee.DonneeNumerique, VI.NatureDonnee.DonneeCalculee
                            If LongueurScript = 0 Then
                                Return ""
                            Else
                                Return Strings.StrDup(LongueurScript, "0")
                            End If
                    End Select
                End If
                Select Case NatureInfo
                    Case VI.NatureDonnee.DonneeDate
                        Select Case FormatScript
                            Case VI.FormatDonnee.Standard
                                Return Chaine
                            Case Else
                                FormatAlpha = FormatEnClair(NatureInfo, FormatScript)
                                Select Case FormatAlpha
                                    Case Is = "Nombre julien"
                                        Return DateSerial(CInt(Strings.Right(Chaine, 4)), CInt(Strings.Mid(Chaine, 4, 2)), CInt(Strings.Left(Chaine, 2))).ToString
                                    Case Is = "Année"
                                        Return Strings.Right(Chaine, 4)
                                    Case Is = "Jour en clair"
                                        Return AppObjetUser.V_RhDates.ClairDate(Chaine, True)
                                    Case Is = "Date en clair"
                                        Return AppObjetUser.V_RhDates.ClairDate(Chaine, False)
                                    Case Else
                                        Return AppObjetUser.V_RhDates.MiseEnFormeDate(Chaine, FormatAlpha, False)
                                End Select
                        End Select
                    Case VI.NatureDonnee.DonneeNumerique, VI.NatureDonnee.DonneeCalculee
                        Chaine = AppObjetUser.V_RhFonction.VirgulePoint(Valeur.Trim)
                        If Chaine = "" Then
                            If LongueurScript = 0 Then
                                Return ""
                            Else
                                Return Strings.StrDup(LongueurScript, "0")
                            End If
                        End If
                        Select Case FormatScript
                            Case 0 'Standard
                                Select Case FormatInfo
                                    Case VI.FormatDonnee.Monetaire
                                        Chaine = AppObjetUser.V_RhFonction.MontantEdite(Chaine)
                                    Case VI.FormatDonnee.Fixe
                                        If WsPointdeVue = 1 And NumObjet = VI.ObjetPer.ObaBanque Then
                                            Chaine = Strings.StrDup(PointeurDicoInfo(WsPointdeVue, NumObjet, NumInfo).LongueurMaxi - Chaine.Length, "0") & Chaine
                                        End If
                                End Select
                            Case 2 'Monétaire
                                Chaine = AppObjetUser.V_RhFonction.MontantEdite(Chaine)
                            Case 3 'Entier
                                Chaine = Strings.Format(Val(Chaine), "#0")
                            Case 4 'Décimale sans la virgule  
                                If Chaine.Length > 2 Then
                                    Chaine = Strings.Left(Chaine, Chaine.Length - 2)
                                End If
                            Case 6 '€uro
                                Chaine = Strings.Format(Val(Chaine), "##.00")
                            Case 7 'Ancienneté sous forme - n ans n mois n jours
                                Chaine = AppObjetUser.V_RhFonction.MiseEnFormeAnciennete(Chaine)
                        End Select
                        If LongueurScript = 0 Then
                            Return Chaine
                        Else
                            If Chaine.Length < LongueurScript Then
                                Return Strings.StrDup(LongueurScript - Chaine.Length, "0") & Chaine
                            Else
                                Return Strings.Left(Chaine, LongueurScript)
                            End If
                        End If
                    Case VI.NatureDonnee.DonneeFichier
                        If FormatInfo = 1 Then
                            Return VI.DoubleQuote & Chaine.Replace("\", "\\") & VI.DoubleQuote
                        End If
                    Case Else
                        Select Case FormatScript
                            Case VI.FormatDonnee.Minuscule
                                Chaine = Chaine.ToLower
                            Case VI.FormatDonnee.Majuscule
                                Chaine = Chaine.ToUpper
                        End Select
                        If LongueurScript = 0 Then
                            Return Chaine
                        Else
                            If Chaine.Length < LongueurScript Then
                                Return Chaine & Strings.StrDup(LongueurScript - Chaine.Length, Strings.Space(1))
                            Else
                                Return Strings.Left(Chaine, LongueurScript)
                            End If
                        End If
                End Select

                Return Chaine
            End Get
        End Property

        Private Function FormatEnClair(ByVal Nature As Short, ByVal Index As Integer) As String
            Dim LibelNat As String = ""
            Dim Chaine As String
            Select Case Nature
                Case VI.NatureDonnee.DonneeDate
                    LibelNat = "Date"
                Case VI.NatureDonnee.DonneeNumerique
                    LibelNat = "Numeric"
                Case Else
                    LibelNat = "Alpha"
            End Select
            Chaine = AppObjetUser.V_RhFonction.OptionInformation(LibelNat, Index)
            Return Chaine
        End Function

        Public ReadOnly Property CentrageColonne() As Short()
            Get
                Dim TableauObjet(0) As String
                Dim I As Integer
                Dim K As Integer
                Dim NumObjet As Short
                Dim NumInfo As Short
                Dim NbColRepetees As Short
                Dim NatureInfo As Short
                Dim TabTmp As New ArrayList
                Dim TabCentrage() As Short

                For I = 0 To WsTabColonnes.Count - 1
                    TableauObjet = Strings.Split(WsTabColonnes(I).ToString, VI.PointVirgule, -1)
                    NumObjet = CShort(TableauObjet(0))
                    NumInfo = CShort(TableauObjet(1))
                    NbColRepetees = CShort(TableauObjet(2))
                    Select Case NumInfo
                        Case Is < 500
                            NatureInfo = PointeurDicoInfo(WsPointdeVue, NumObjet, NumInfo).VNature
                        Case Is > 9000
                            NatureInfo = 0
                        Case Else
                            NatureInfo = PointeurDicoExperte(WsPointdeVue, NumObjet, NumInfo).VNature
                    End Select
                    Select Case NbColRepetees
                        Case Is = 0
                            Select Case NatureInfo
                                Case Is = VI.NatureDonnee.DonneeDate, VI.NatureDonnee.DonneeHeure
                                    TabTmp.Add(1)
                                Case Is = VI.NatureDonnee.DonneeNumerique, VI.NatureDonnee.DonneeCalculee
                                    TabTmp.Add(1)
                                Case Else
                                    TabTmp.Add(0)
                            End Select
                        Case Else
                            For K = 0 To NbColRepetees - 1
                                Select Case NatureInfo
                                    Case Is = VI.NatureDonnee.DonneeDate, VI.NatureDonnee.DonneeHeure
                                        TabTmp.Add(1)
                                    Case Is = VI.NatureDonnee.DonneeNumerique, VI.NatureDonnee.DonneeCalculee
                                        TabTmp.Add(1)
                                    Case Else
                                        TabTmp.Add(0)
                                End Select
                            Next K
                    End Select
                Next I
                ReDim TabCentrage(TabTmp.Count)
                For I = 0 To TabTmp.Count - 1
                    TabCentrage(I) = CShort(TabTmp(I))
                Next I
                Return TabCentrage
            End Get
        End Property

        Private ReadOnly Property PointeurDicoObjet(ByVal PointdeVue As Short, ByVal Numobjet As Short) As Virtualia.Systeme.MetaModele.Donnees.Objet
            Get
                If PointdeVue = 0 Or Numobjet = 0 Then
                    Return Nothing
                    Exit Property
                End If
                Dim Predicat As New Virtualia.Systeme.MetaModele.Predicats.PredicateDictionnaire(PointdeVue, Numobjet)
                Dim Dico As List(Of Virtualia.Systeme.MetaModele.Outils.FicheObjetDictionnaire)
                Dico = AppObjetUser.VParent.VirListeObjetsDico.FindAll(AddressOf Predicat.InformationParIdeObjet)
                If Dico.Count = 0 Then
                    Return Nothing
                    Exit Property
                End If
                Return Dico.Item(0).PointeurModele
            End Get
        End Property

        Private ReadOnly Property PointeurDicoInfo(ByVal PointdeVue As Short, ByVal Numobjet As Short, ByVal NumInfo As Short) As Virtualia.Systeme.MetaModele.Donnees.Information
            Get
                If PointdeVue = 0 Or Numobjet = 0 Then
                    Return Nothing
                    Exit Property
                End If
                Dim Predicat As New Virtualia.Systeme.MetaModele.Predicats.PredicateDictionnaire(PointdeVue, Numobjet, NumInfo)
                Dim Dico As List(Of Virtualia.Systeme.MetaModele.Outils.FicheInfoDictionnaire)
                Dico = AppObjetUser.VParent.VirListeInfosDico.FindAll(AddressOf Predicat.InformationParIdeInfo)
                If Dico.Count = 0 Then
                    Return Nothing
                    Exit Property
                End If
                Return Dico.Item(0).PointeurModele
            End Get
        End Property

        Private ReadOnly Property PointeurDicoExperte(ByVal PointdeVue As Short, ByVal Numobjet As Short, ByVal NumInfo As Short) As Virtualia.Systeme.MetaModele.Expertes.InformationExperte
            Get
                If PointdeVue = 0 Or Numobjet = 0 Then
                    Return Nothing
                    Exit Property
                End If
                Dim Predicat As New Virtualia.Systeme.MetaModele.Predicats.PredicateDictionnaire(PointdeVue, Numobjet, NumInfo)
                Dim Dico As List(Of Virtualia.Systeme.MetaModele.Outils.FicheInfoExperte)
                Dico = AppObjetUser.VParent.VirListeExpertesDico.FindAll(AddressOf Predicat.InformationParIdeExperte)
                If Dico.Count = 0 Then
                    Return Nothing
                    Exit Property
                End If
                Return Dico.Item(0).PointeurModele
            End Get
        End Property

        Public Sub New(ByVal InstanceUtilisateur As Virtualia.Net.Session.ObjetSession, ByVal PointdeVue As Short)
            MyBase.New()
            AppObjetUser = InstanceUtilisateur
            WsPointdeVue = PointdeVue
        End Sub

        Public Overloads Sub Dispose() Implements IDisposable.Dispose
            Dispose(True)
            GC.SuppressFinalize(Me)
        End Sub

        Protected Overridable Overloads Sub Dispose(ByVal disposing As Boolean)
            If Not Me.disposedValue Then
                If disposing Then
                    If WsListeRes IsNot Nothing Then
                        WsListeRes.Clear()
                        WsListeRes = Nothing
                    End If
                    If WsListeRequetes IsNot Nothing Then
                        WsListeRequetes.Clear()
                        WsListeRequetes = Nothing
                    End If
                    If WsTabIde IsNot Nothing Then
                        WsTabIde = Nothing
                    End If
                    If WsChaineDatas IsNot Nothing Then
                        WsChaineDatas = Nothing
                    End If
                End If
            End If
            Me.disposedValue = True
        End Sub
    End Class
End Namespace
