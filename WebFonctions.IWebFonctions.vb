﻿Option Strict On
Option Explicit On
Option Compare Text

Imports VI = Virtualia.Systeme.Constantes
Imports Virtualia.OutilsVisu
Imports Virtualia.Ressources.Datas

Partial Public Class WebFonctions
    Implements IWebFonctionsExperte

#Region "Variables"
    Private _application As HttpApplicationState
    Private _session As HttpSessionState
#End Region

#Region "IWebFonctions"
    Public ReadOnly Property CouleurMaj() As System.Drawing.Color Implements IWebFonctionsExperte.CouleurMaj
        Get
            Return ConvertCouleur("DEFAD7")
        End Get
    End Property

    Public Function MajFiche(ByVal CacheIde As CacheDossier, ByVal CacheMaj As CacheItem, ByVal Chainelue As String) As String Implements IWebFonctionsExperte.MajFiche
        If WsAppUtiSession Is Nothing Then
            Return ""
        End If

        Dim IndiceI As Integer
        Dim SiOK As Boolean
        Dim ChaineMaj As String

        Dim Tableaudata(PointeurDicoObjet(CacheIde.PointDeVue, CacheIde.NumObjet).LimiteMaximum) As String
        For IndiceI = 0 To Tableaudata.Count - 1
            If CacheMaj(IndiceI).valeur Is Nothing Then
                Tableaudata(IndiceI) = ""
            Else
                Tableaudata(IndiceI) = CacheMaj(IndiceI).valeur.ToString
            End If
        Next IndiceI


        Select Case CacheIde.PointDeVue
            Case VI.PointdeVue.PVueApplicatif
                SiOK = PointeurDossier(CacheIde.Ide_Dossier).V_MettreAJourFiche(PointeurUtilisateur.V_NomdUtilisateurSgbd, _
                                                                                PointeurUtilisateur.V_NomdeConnexion, _
                                                                                CacheIde.NumObjet, _
                                                                                Chainelue, _
                                                                                Strings.Join(Tableaudata, VI.Tild))
            Case Else
                SiOK = PointeurIdeSysRef(CacheIde.PointDeVue, CacheIde.Ide_Dossier).V_MettreAJourFiche(PointeurUtilisateur.V_NomdUtilisateurSgbd, _
                                                                                                        PointeurUtilisateur.V_NomdeConnexion, _
                                                                                                        CacheIde.NumObjet, _
                                                                                                        Chainelue, _
                                                                                                        Strings.Join(Tableaudata, VI.Tild))
        End Select

        ChaineMaj = ""
        If SiOK Then
            Select Case PointeurDicoObjet(CacheIde.PointDeVue, CacheIde.NumObjet).VNature
                Case VI.TypeObjet.ObjetSimple, VI.TypeObjet.ObjetTableau, VI.TypeObjet.ObjetMemo
                    For IndiceI = 1 To Tableaudata.Count - 1
                        ChaineMaj &= Tableaudata(IndiceI) & VI.Tild
                    Next IndiceI
                Case Else
                    For IndiceI = 0 To Tableaudata.Count - 1
                        ChaineMaj &= Tableaudata(IndiceI) & VI.Tild
                    Next IndiceI
            End Select
            Return CacheIde.Ide_Dossier.ToString & VI.Tild & ChaineMaj
        Else
            Return ""
        End If

    End Function

    'Public Sub SetContexteFenetreVueActive(CategorieVueActive As Short, activevue As Integer) Implements IWebFonctionsExperte.SetContexteFenetreVueActive
    '    If (activevue >= 0) Then
    '        PointeurContexte.Fenetre_VueActive(CategorieVueActive) = activevue
    '    End If
    'End Sub

    Public Function GetDicoInfo_Format(PointDeVue As Short, NumObjet As Short, NumInfo As Short) As Short Implements IWebFonctionsExperte.GetDicoInfo_Format
        Return Me.PointeurDicoInfo(PointDeVue, NumObjet, NumInfo).Format
    End Function

    Public Function GetDicoInfo_Nature(PointDeVue As Short, NumObjet As Short, NumInfo As Short) As Short Implements IWebFonctionsExperte.GetDicoInfo_Nature
        Return Me.PointeurDicoInfo(PointDeVue, NumObjet, NumInfo).VNature
    End Function

    Public Function GetDicoInfo_Etiquette(PointdeVue As Short, Objet As Short, Information As Short) As String Implements IWebFonctionsExperte.GetDicoInfo_Etiquette
        Return PointeurDicoInfo(PointdeVue, Objet, Information).Etiquette
    End Function

    Public Function GetDicoInfo_LongueurMaxi(PointdeVue As Short, Objet As Short, Information As Short) As Integer Implements IWebFonctionsExperte.GetDicoInfo_LongueurMaxi
        Return PointeurDicoInfo(PointdeVue, Objet, Information).LongueurMaxi
    End Function

    Public Function GetDicoInfo_TabledeReference(PointdeVue As Short, Objet As Short, Information As Short) As String Implements IWebFonctionsExperte.GetDicoInfo_TabledeReference
        Return PointeurDicoInfo(PointdeVue, Objet, Information).TabledeReference
    End Function

    Public Function GetDicoInfo_PointdeVueInverse(PointdeVue As Short, Objet As Short, Information As Short) As Short Implements IWebFonctionsExperte.GetDicoInfo_PointDeVueInverse
        Return PointeurDicoInfo(PointdeVue, Objet, Information).PointdeVueInverse
    End Function

    Public Function GetGenericDossier(PointDeVue As Short, Ide_Dossier As Integer) As Ressources.Datas.GenericDossier Implements IWebFonctionsExperte.GetGenericDossier
        Dim ret As GenericDossier = Nothing

        Select Case PointDeVue
            Case VI.PointdeVue.PVueApplicatif
                ret = PointeurDossier(Ide_Dossier)
            Case Else
                ret = PointeurIdeSysRef(PointDeVue, Ide_Dossier)
        End Select

        Return ret
    End Function

    Public Function GetLimiteMaximum(PointDeVue As Short, NumObjet As Short) As Integer Implements IWebFonctionsExperte.GetLimiteMaximum

        Dim truc As Object = PointeurDicoObjet(PointDeVue, NumObjet)

        Return PointeurDicoObjet(PointDeVue, NumObjet).LimiteMaximum
    End Function

    Public Function GetNomDeConnection() As String Implements IWebFonctionsExperte.GetNomDeConnection
        Return PointeurUtilisateur.V_NomdeConnexion
    End Function

    Public Function GetNomUtilisateurSGBD() As String Implements IWebFonctionsExperte.GetNomUtilisateurSGBD
        Return PointeurUtilisateur.V_NomdUtilisateurSgbd
    End Function

    Public Function GetNouvelIdentifiant(nomtable As String) As Integer Implements IWebFonctionsExperte.GetNouvelIdentifiant
        Return PointeurGlobal.NouvelIdentifiant(nomtable)
    End Function

    Public Function GetValeurRegistre(Intitule As String) As String Implements IWebFonctionsExperte.GetValeurRegistre
        Return PointeurRegistre.Valeur(Intitule)
    End Function

    Public Function GetValeurRegistre(NomSousSection As String, Intitule As String) As String Implements IWebFonctionsExperte.GetValeurRegistre
        Return PointeurRegistre.Valeur(NomSousSection, Intitule)
    End Function

    Public Function GetValeurRegistre(NomSection As String, NomSousSection As String, Intitule As String) As String Implements IWebFonctionsExperte.GetValeurRegistre
        Return PointeurRegistre.Valeur(NomSection, NomSousSection, Intitule)
    End Function

    Public Function GetValeurRegistre(NomCategorie As String, NomSection As String, NomSousSection As String, Intitule As String) As String Implements IWebFonctionsExperte.GetValeurRegistre
        Return PointeurRegistre.Valeur(NomCategorie, NomSection, NomSousSection, Intitule)
    End Function

    Public Function PointeurRegistreOK() As Boolean Implements IWebFonctionsExperte.PointeurRegistreOK
        Return Not (PointeurRegistre Is Nothing)
    End Function

    Public Function PointeurUtilisateurOK() As Boolean Implements IWebFonctionsExperte.PointeurUtilisateurOK
        Return Not (PointeurUtilisateur Is Nothing)
    End Function

    Public Function GetFocusChange() As Integer Implements IWebFonctionsExperte.GetFocusChange
        Return Me.PointeurContexte.Focus_Change
    End Function

    Public Sub SetFocusChange(NumTabul As Integer) Implements IWebFonctionsExperte.SetFocusChange
        PointeurContexte.Focus_Change = NumTabul
    End Sub

    Public Function ControleSaisie(Nature As Short, Format As Short, Txt As String) As String Implements IWebFonctionsExperte.ControleSaisie
        Return Controle_LostFocusStd(Nature, Format, Txt)
    End Function

    Public Function DoubleToChaine(text As String, format As Short) As String Implements IWebFonctionsExperte.DoubleToChaine

        Dim ret As String = ""

        Dim Montant As Double = ViRhFonction.ConversionDouble(ret)
        Select Case format
            Case VI.FormatDonnee.Decimal1
                ret = Strings.Format(Montant, "0.0")
            Case VI.FormatDonnee.Decimal1, VI.FormatDonnee.Monetaire
                ret = Strings.Format(Montant, "0.00")
            Case VI.FormatDonnee.Decimal3
                ret = Strings.Format(Montant, "0.000")
            Case VI.FormatDonnee.Decimal4
                ret = Strings.Format(Montant, "0.0000")
            Case VI.FormatDonnee.Decimal5
                ret = Strings.Format(Montant, "0.00000")
            Case VI.FormatDonnee.Decimal6
                ret = Strings.Format(Montant, "0.000000")
        End Select

        Return ret
    End Function

    Public Function GetDateDuJour(SiEdite As Boolean) As String Implements IWebFonctionsExperte.GetDateDuJour
        Return ViRhDates.DateduJour(SiEdite)
    End Function

    Public Function CalculerDateAvancement(DateEchelon As String, DureeEchelon As String, DureeInter As String, DureeReliquat As String) As String Implements IWebFonctionsExperte.CalculerDateAvancement
        Return ViRhDates.CalculerDateAvancement(DateEchelon, DureeEchelon, DureeInter, DureeReliquat)
    End Function

    Public Function ComparerDates(V1 As String, V2 As String) As Short Implements IWebFonctionsExperte.ComparerDates
        Return ViRhDates.ComparerDates(V1, V2)
    End Function

    Public Function CalcDateMoinsJour(LaDate As String, PlusJour As String, MoinsJour As String) As String Implements IWebFonctionsExperte.CalcDateMoinsJour
        Return ViRhDates.CalcDateMoinsJour(LaDate, PlusJour, MoinsJour)
    End Function

    Public Function DateStandardVirtualia(ArgumentDate As String) As String Implements IWebFonctionsExperte.DateStandardVirtualia
        Return ViRhDates.DateStandardVirtualia(ArgumentDate)
    End Function

    Public Function DureeDates360(DateDebut As String, DateFin As String) As String Implements IWebFonctionsExperte.DureeDates360
        Return ViRhDates.DureeDates360(DateDebut, DateFin)
    End Function

    Public Function GetDicoExperte_Description(PointdeVue As Short, Objet As Short, InfoExperte As Short) As String Implements IWebFonctionsExperte.GetDicoExperte_Description
        Return PointeurDicoExperte(PointdeVue, Objet, InfoExperte).Description
    End Function

    Public Function GetDicoExperte_Intitule(PointdeVue As Short, Objet As Short, InfoExperte As Short) As String Implements IWebFonctionsExperte.GetDicoExperte_Intitule
        Return PointeurDicoExperte(PointdeVue, Objet, InfoExperte).Intitule
    End Function

    Public Function GetInfoExperte(PointdeVue As Short, Objet As Short, NumExperte As Short, ide As Integer) As String Implements IWebFonctionsExperte.GetInfoExperte
        Return InfoExperte(PointdeVue, Objet, NumExperte, ide)
    End Function
#End Region

#Region "Constructeur"
    Public Sub New(ByVal Host As IControlModule)

        _application = Host.GetApplication()
        _session = Host.GetSession()

        Call InitialiserSession()

        WsRhFonction = New Virtualia.Systeme.Fonctions.Generales
        WsRhDates = New Virtualia.Systeme.Fonctions.CalculDates

    End Sub
#End Region

#Region "Methodes statics"
    Public Shared Function ConvertiCouleur(ByVal Valeur As String) As System.Drawing.Color

        Dim R As Integer
        Dim G As Integer
        Dim B As Integer

        Select Case Valeur.Length
            Case Is = 6
                R = CInt(Val("&H" & Strings.Left(Valeur, 2) & "&"))
                G = CInt(Val("&H" & Strings.Mid(Valeur, 3, 2) & "&"))
                B = CInt(Val("&H" & Strings.Right(Valeur, 2) & "&"))
                Return System.Drawing.Color.FromArgb(R, G, B)
            Case Is = 7
                R = CInt(Val("&H" & Strings.Mid(Valeur, 2, 2) & "&"))
                G = CInt(Val("&H" & Strings.Mid(Valeur, 4, 2) & "&"))
                B = CInt(Val("&H" & Strings.Right(Valeur, 2) & "&"))
                Return System.Drawing.Color.FromArgb(R, G, B)
            Case Else
                Return Drawing.Color.White
        End Select
    End Function
#End Region

End Class
