﻿Option Explicit On
Option Strict On
Option Compare Text
Imports Microsoft.VisualBasic
Imports VI = Virtualia.Systeme.Constantes
Namespace Session
    Public Class ObjetNavigation
        Implements IDisposable
        Private disposedValue As Boolean = False        ' Pour détecter les appels redondants
        Private WsParent As Virtualia.Net.Session.ObjetSession

        'Focus
        Private WsFocus As Integer = 0
        'Armoire
        Private WsArmoireIdentifiant As Integer = 0
        Private WsArmoireListeActive As Short
        Private WsArmoireActive As Short = 1
        Private WsArmoireLettre As String = ""
        Private WsArmoireCouleurEti As String = ""
        Private WsListePanel As List(Of Virtualia.Ressources.Datas.ItemSelection)
        '
        'Système de référence
        Private WsSysRefListe As List(Of Virtualia.Ressources.Datas.ObjetDossierREF)
        Private WsSysRefPvueInverse As Short
        Private WsSysRefNomTable As String
        Private WsSysRefLettre As String = ""
        Private WsSysRefIDAppelant As String 'ID du contrôle appelant le SysRef
        Private WsSysRefObjetAppelant As Short 'Objet origine appelant le SysRef
        Private WsSysRefIDVueRetour As String
        Private WsSysRefDossier As Virtualia.Ressources.Datas.ObjetDossierREF
        Private WsSysRefDuoPvueInverse(1) As Short
        Private WsSysRefDuoNomTable(1) As String
        'Fenetres
        Private WsFenActiveVue(12) As Integer
        Private WsFenVuePrecedente(12) As Integer
        Private WsPointdeVueActif As Short
        Private WsOutilActif As Short
        Private WsTsTampon As ArrayList

        'Administrer les bases de données
        Private WsAdmEnCours As Boolean
        Private WsAdmFichier As String
        Private WsAdmNomTable As String = ""
        Private WsAdmNoPage As Integer = 0
        Private WsAdmCptTotalTable As Integer = 0
        Private WsAdmCptTotalGen As Integer = 0
        Private WsAdmCptTotalErr As Integer = 0
        'Administrer les utilisateurs
        Private WsLstTriee As List(Of TablesObjet.ShemaREF.UTI_IDENTIFICATION)
        'Regle
        Private WsRegleIdentifiant As Integer
        Private WsRegleFenActiveVue As Integer
        Private WsObjetFormule As Virtualia.TablesObjet.ShemaREF.REG_FORMULE
        Private WsObjetCritere As Virtualia.TablesObjet.ShemaREF.REG_CRITERE

        Public ReadOnly Property VParent() As Virtualia.Net.Session.ObjetSession
            Get
                Return WsParent
            End Get
        End Property

        Public Property Focus_Change() As Integer
            Get
                Return WsFocus
            End Get
            Set(ByVal value As Integer)
                WsFocus = value
            End Set
        End Property

        Public Property Armoire_Identifiant() As Integer
            Get
                Return WsArmoireIdentifiant
            End Get
            Set(ByVal value As Integer)
                WsArmoireIdentifiant = value
            End Set
        End Property

        Public Property Armoire_ListeActive() As Short
            Get
                Return WsArmoireListeActive
            End Get
            Set(ByVal value As Short)
                WsArmoireListeActive = value
            End Set
        End Property

        Public Property Armoire_Active() As Short
            Get
                Return WsArmoireActive
            End Get
            Set(ByVal value As Short)
                WsArmoireActive = value
            End Set
        End Property

        Public Property Armoire_Lettre() As String
            Get
                Return WsArmoireLettre
            End Get
            Set(ByVal value As String)
                WsArmoireLettre = value
            End Set
        End Property

        Public Property Armoire_CouleurEti() As String
            Get
                Return WsArmoireCouleurEti
            End Get
            Set(ByVal value As String)
                WsArmoireCouleurEti = value
            End Set
        End Property

        Public ReadOnly Property SysRef_Listes() As List(Of Virtualia.Ressources.Datas.ObjetDossierREF)
            Get
                If WsSysRefListe Is Nothing Then
                    WsSysRefListe = New List(Of Virtualia.Ressources.Datas.ObjetDossierREF)
                End If
                Return WsSysRefListe
            End Get
        End Property

        Public Property SysRef_Lettre() As String
            Get
                Return WsSysRefLettre
            End Get
            Set(ByVal value As String)
                WsSysRefLettre = value
            End Set
        End Property

        Public Property SysRef_IDAppelant() As String
            Get
                Return WsSysRefIDAppelant
            End Get
            Set(ByVal value As String)
                WsSysRefIDAppelant = value
            End Set
        End Property

        Public Property SysRef_ObjetAppelant() As Short
            Get
                Return WsSysRefObjetAppelant
            End Get
            Set(ByVal value As Short)
                WsSysRefObjetAppelant = value
            End Set
        End Property

        Public Property SysRef_PointdeVueInverse() As Short
            Get
                Return WsSysRefPvueInverse
            End Get
            Set(ByVal value As Short)
                WsSysRefPvueInverse = value
            End Set
        End Property

        Public Property SysRef_NomTable() As String
            Get
                Return WsSysRefNomTable
            End Get
            Set(ByVal value As String)
                WsSysRefNomTable = value
            End Set
        End Property

        Public Property SysRef_IDVueRetour() As String
            Get
                Return WsSysRefIDVueRetour
            End Get
            Set(ByVal value As String)
                WsSysRefIDVueRetour = value
            End Set
        End Property

        Public ReadOnly Property SysRef_PointeurDossier(ByVal Pvue As Short, ByVal Ide As Integer) As Virtualia.Ressources.Datas.ObjetDossierREF
            Get
                If WsSysRefDossier Is Nothing Then
                    WsSysRefDossier = New Virtualia.Ressources.Datas.ObjetDossierREF(WsParent.VParent.VirUrlWebServeur, _
                                         WsParent.VParent.VirModele, Pvue, Ide)
                Else
                    If WsSysRefDossier.V_PointdeVue <> Pvue Or WsSysRefDossier.V_Identifiant <> Ide Then
                        WsSysRefDossier = New Virtualia.Ressources.Datas.ObjetDossierREF(WsParent.VParent.VirUrlWebServeur, _
                                             WsParent.VParent.VirModele, Pvue, Ide)
                    End If
                End If
                Return WsSysRefDossier
            End Get
        End Property

        Public Property SysRef_Duo_PointdeVueInverse(ByVal Index As Integer) As Short
            Get
                Select Case Index
                    Case 0, 1
                        Return WsSysRefDuoPvueInverse(Index)
                End Select
                Return 0
            End Get
            Set(ByVal value As Short)
                Select Case Index
                    Case 0, 1
                        WsSysRefDuoPvueInverse(Index) = value
                End Select
            End Set
        End Property

        Public Property SysRef_Duo_NomTable(ByVal Index As Integer) As String
            Get
                Select Case Index
                    Case 0, 1
                        Return WsSysRefDuoNomTable(Index)
                End Select
                Return ""
            End Get
            Set(ByVal value As String)
                Select Case Index
                    Case 0, 1
                        WsSysRefDuoNomTable(Index) = value
                End Select
            End Set
        End Property

        Public Property Administrer_EnCours() As Boolean
            Get
                Return WsAdmEnCours
            End Get
            Set(ByVal value As Boolean)
                WsAdmEnCours = value
            End Set
        End Property

        Public Property Administrer_NomTable() As String
            Get
                Return WsAdmNomTable
            End Get
            Set(ByVal value As String)
                WsAdmNomTable = value
            End Set
        End Property

        Public Property Administrer_NumPage() As Integer
            Get
                Return WsAdmNoPage
            End Get
            Set(ByVal value As Integer)
                WsAdmNoPage = value
            End Set
        End Property

        Public Property Administrer_TotalTable() As Integer
            Get
                Return WsAdmCptTotalTable
            End Get
            Set(ByVal value As Integer)
                WsAdmCptTotalTable = value
            End Set
        End Property

        Public Property Administrer_TotalGen() As Integer
            Get
                Return WsAdmCptTotalGen
            End Get
            Set(ByVal value As Integer)
                WsAdmCptTotalGen = value
            End Set
        End Property

        Public Property Administrer_TotalErr() As Integer
            Get
                Return WsAdmCptTotalErr
            End Get
            Set(ByVal value As Integer)
                WsAdmCptTotalErr = value
            End Set
        End Property

        Public Property Administrer_Fichier() As String
            Get
                Return WsAdmFichier
            End Get
            Set(ByVal value As String)
                WsAdmFichier = value
            End Set
        End Property

        Public Property Administrer_LstUtilisateurs() As List(Of TablesObjet.ShemaREF.UTI_IDENTIFICATION)
            Get
                Return WsLstTriee
            End Get
            Set(ByVal value As List(Of TablesObjet.ShemaREF.UTI_IDENTIFICATION))
                WsLstTriee = value
            End Set
        End Property

        Public Sub InitialiserFenetres()
            Dim IndiceI As Integer
            For IndiceI = 0 To WsFenActiveVue.Count - 1
                WsFenActiveVue(IndiceI) = 0
            Next IndiceI
            For IndiceI = 0 To WsFenVuePrecedente.Count - 1
                WsFenVuePrecedente(IndiceI) = 0
            Next IndiceI
        End Sub

        Public Property Fenetre_VueActive(ByVal Index As Integer) As Integer
            Get
                If Index > WsFenActiveVue.Count - 1 Then
                    Return -1
                    Exit Property
                End If
                Return WsFenActiveVue(Index)
            End Get
            Set(ByVal value As Integer)
                If Index > WsFenActiveVue.Count - 1 Then
                    Exit Property
                End If
                WsFenVuePrecedente(Index) = WsFenActiveVue(Index)
                WsFenActiveVue(Index) = value
            End Set
        End Property

        Public ReadOnly Property Fenetre_VuePrecedente(ByVal Index As Integer) As Integer
            Get
                If Index > WsFenVuePrecedente.Count - 1 Then
                    Return -1
                    Exit Property
                End If
                Return WsFenVuePrecedente(Index)
            End Get
        End Property

        Public Property Fenetre_PointdeVue() As Short
            Get
                Return WsPointdeVueActif
            End Get
            Set(ByVal value As Short)
                WsPointdeVueActif = value
            End Set
        End Property

        Public Property Fenetre_Outil() As Short
            Get
                Return WsOutilActif
            End Get
            Set(ByVal value As Short)
                WsOutilActif = value
            End Set
        End Property

        Public Sub InitialiserPanel()
            If WsListePanel IsNot Nothing Then
                WsListePanel.Clear()
                WsListePanel = Nothing
            End If
            WsListePanel = New List(Of Virtualia.Ressources.Datas.ItemSelection)
        End Sub

        Public Property TsTampon() As ArrayList
            Get
                Return WsTsTampon
            End Get
            Set(ByVal value As ArrayList)
                WsTsTampon = value
            End Set
        End Property

        Public ReadOnly Property ListePanel() As List(Of Virtualia.Ressources.Datas.ItemSelection)
            Get
                Return WsListePanel
            End Get
        End Property

        Public ReadOnly Property ListeIdentifiantsPanel() As Integer()
            Get
                If WsListePanel Is Nothing Then
                    Return Nothing
                End If
                If WsListePanel.Count = 0 Then
                    Return Nothing
                End If
                Dim TabIde As Integer()
                Dim I As Integer
                ReDim TabIde(WsListePanel.Count)
                For I = 0 To WsListePanel.Count - 1
                    TabIde(I) = WsListePanel.Item(I).Identifiant
                Next I
                Return TabIde
            End Get
        End Property

        '(REGLE)
        Public Property RegleIdentifiant() As Integer
            Get
                Return WsRegleIdentifiant
            End Get
            Set(ByVal value As Integer)
                WsRegleIdentifiant = value
            End Set
        End Property

        '(REGLE)
        Public Property FormuleCrt() As Virtualia.TablesObjet.ShemaREF.REG_FORMULE
            Get
                Return WsObjetFormule
            End Get
            Set(ByVal value As Virtualia.TablesObjet.ShemaREF.REG_FORMULE)
                WsObjetFormule = value
            End Set
        End Property

        '(REGLE)
        Public Property CritereCrt() As Virtualia.TablesObjet.ShemaREF.REG_CRITERE
            Get
                Return WsObjetCritere
            End Get
            Set(ByVal value As Virtualia.TablesObjet.ShemaREF.REG_CRITERE)
                WsObjetCritere = value
            End Set
        End Property

        Public Property RegleFenActiveVue() As Integer
            Get
                Return WsRegleFenActiveVue
            End Get
            Set(ByVal value As Integer)
                WsRegleFenActiveVue = value
            End Set
        End Property

        Public Sub New(ByVal Host As Virtualia.Net.Session.ObjetSession)
            WsParent = Host
        End Sub

        Public Overloads Sub Dispose() Implements IDisposable.Dispose
            Dispose(True)
            GC.SuppressFinalize(Me)
        End Sub

        Protected Overridable Overloads Sub Dispose(ByVal disposing As Boolean)
            If Not Me.disposedValue Then
                If disposing Then
                    If WsSysRefListe IsNot Nothing Then
                        WsSysRefListe.Clear()
                        WsSysRefListe = Nothing
                    End If
                End If
            End If
            Me.disposedValue = True
        End Sub

    End Class
End Namespace
