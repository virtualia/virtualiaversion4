﻿'------------------------------------------------------------------------------
' <généré automatiquement>
'     Ce code a été généré par un outil.
'
'     Les modifications apportées à ce fichier peuvent provoquer un comportement incorrect et seront perdues si
'     le code est régénéré.
' </généré automatiquement>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class MasterPage

    '''<summary>
    '''Contrôle HeadMaitre.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents HeadMaitre As Global.System.Web.UI.HtmlControls.HtmlHead

    '''<summary>
    '''Contrôle HeadMaster.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents HeadMaster As Global.System.Web.UI.WebControls.ContentPlaceHolder

    '''<summary>
    '''Contrôle VirtualiaMainMaster.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VirtualiaMainMaster As Global.System.Web.UI.HtmlControls.HtmlForm

    '''<summary>
    '''Contrôle ScriptManagerMaster.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents ScriptManagerMaster As Global.System.Web.UI.ScriptManager

    '''<summary>
    '''Contrôle HautdePage.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents HautdePage As Global.System.Web.UI.WebControls.ContentPlaceHolder

    '''<summary>
    '''Contrôle UpdatePanelHaut.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents UpdatePanelHaut As Global.System.Web.UI.UpdatePanel

    '''<summary>
    '''Contrôle HorlogeVirtuelle.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents HorlogeVirtuelle As Global.System.Web.UI.Timer

    '''<summary>
    '''Contrôle CadreMenu.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CadreMenu As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle ImageLogo.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents ImageLogo As Global.System.Web.UI.WebControls.Image

    '''<summary>
    '''Contrôle CadreEntete.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CadreEntete As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle EtiTitre.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiTitre As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle LienEdossier.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LienEdossier As Global.System.Web.UI.WebControls.HyperLink

    '''<summary>
    '''Contrôle LienAnnuaire.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LienAnnuaire As Global.System.Web.UI.WebControls.HyperLink

    '''<summary>
    '''Contrôle LienDeconnexion.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LienDeconnexion As Global.System.Web.UI.WebControls.HyperLink

    '''<summary>
    '''Contrôle EtiUtilisateur.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiUtilisateur As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle LienDatabase.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LienDatabase As Global.System.Web.UI.WebControls.HyperLink

    '''<summary>
    '''Contrôle EtiDateJour.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiDateJour As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle EtiHeure.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiHeure As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle MenuPrincipal.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents MenuPrincipal As Global.System.Web.UI.WebControls.Menu

    '''<summary>
    '''Contrôle InfoGenMaster.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoGenMaster As Global.System.Web.UI.WebControls.ContentPlaceHolder

    '''<summary>
    '''Contrôle UpdatePanelInfoGen.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents UpdatePanelInfoGen As Global.System.Web.UI.UpdatePanel

    '''<summary>
    '''Contrôle CadreInfos.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CadreInfos As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle MsgCourant.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents MsgCourant As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle MsgSaisie.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents MsgSaisie As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle MsgArmoire.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents MsgArmoire As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle CorpsMaster.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CorpsMaster As Global.System.Web.UI.WebControls.ContentPlaceHolder

    '''<summary>
    '''Contrôle InfoGenPiedMaster.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoGenPiedMaster As Global.System.Web.UI.WebControls.ContentPlaceHolder

    '''<summary>
    '''Contrôle UpdatePanelPied.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents UpdatePanelPied As Global.System.Web.UI.UpdatePanel

    '''<summary>
    '''Contrôle CadreInfosPied.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CadreInfosPied As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle MsgCourantPied.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents MsgCourantPied As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle MsgSaisiePied.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents MsgSaisiePied As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle MsgArmoirePied.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents MsgArmoirePied As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle PieddePageMaster.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PieddePageMaster As Global.System.Web.UI.WebControls.ContentPlaceHolder

    '''<summary>
    '''Contrôle TableDroits.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents TableDroits As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle Droits.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Droits As Global.System.Web.UI.WebControls.Label
End Class
