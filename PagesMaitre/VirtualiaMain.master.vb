﻿Option Strict On
Option Explicit On
Option Compare Text
Imports VI = Virtualia.Systeme.Constantes
Partial Class MasterPage
    Inherits System.Web.UI.MasterPage
    Private WebFct As Virtualia.Net.WebFonctions
    Private WsIdentifiantCourant As Integer = 0
    Private WsStatus_1 As String

    Public Property V_NoSession As String
        Get
            If Session.Item("IDVirtualia") IsNot Nothing Then
                Return Session.Item("IDVirtualia").ToString
            Else
                Return ""
            End If
        End Get
        Set(ByVal value As String)
            Session.Clear()
            Session.Add("IDVirtualia", value)
        End Set
    End Property

    Public Property IdentifiantCourant() As Integer
        Get
            Return WsIdentifiantCourant
        End Get
        Set(ByVal value As Integer)
            If value = 0 Then
                MsgCourant.Text = ""
                MsgSaisie.Text = ""
                MsgCourantPied.Text = ""
                MsgSaisiePied.Text = ""
                Exit Property
            End If
            If WsIdentifiantCourant = value Then
                Exit Property
            End If
            If WebFct Is Nothing Then
                WebFct = New Virtualia.Net.WebFonctions(Me, 2)
            End If
            If WebFct.PointeurUtilisateur Is Nothing Then
                Exit Property
            End If
            Try
                WsIdentifiantCourant = value
                Dim Predicat As New Virtualia.Ressources.Predicats.PredicateDossier(WsIdentifiantCourant)
                Dim LstDossier As New List(Of Virtualia.Ressources.Datas.ObjetDossierPER)
                LstDossier = WebFct.PointeurArmoire.EnsembleDossiers.FindAll(AddressOf Predicat.RechercherIdentifiant)
                If LstDossier.Count = 0 Then
                    Exit Property
                End If

                Dim IndiceI As Integer
                IndiceI = LstDossier.Item(0).V_UnObjetLu(WebFct.PointeurUtilisateur.V_NomdUtilisateurSgbd, "PER_ETATCIVIL", 1)
                Dim Fiche As Virtualia.TablesObjet.ShemaPER.PER_ETATCIVIL
                Fiche = CType(LstDossier.Item(0).Item(IndiceI), Virtualia.TablesObjet.ShemaPER.PER_ETATCIVIL)
                MsgCourant.Text = "(" & value.ToString & ") "
                MsgCourant.Text &= Fiche.Qualite & Strings.Space(1) & Fiche.Nom & Strings.Space(1) & Fiche.Prenom
                MsgCourantPied.Text = MsgCourant.Text
            Catch ex As Exception
                Exit Property
            End Try

        End Set
    End Property

    Protected Sub EtiDateJour_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles EtiDateJour.Init
        If WebFct Is Nothing Then
            WebFct = New Virtualia.Net.WebFonctions(Me, 2)
        End If
        EtiDateJour.Text = WebFct.ViRhDates.ClairDate(WebFct.ViRhDates.DateduJour, True)
        EtiHeure.Text = Format(TimeOfDay, "t")
    End Sub

    Protected Sub EtiUtilisateur_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles EtiUtilisateur.PreRender
        If WebFct Is Nothing Then
            WebFct = New Virtualia.Net.WebFonctions(Me, 2)
        End If
        If WebFct.PointeurUtilisateur Is Nothing Then
            Exit Sub
        End If
        EtiUtilisateur.Text = WebFct.PointeurUtilisateur.V_NomdUtilisateurSgbd
    End Sub

    Protected Sub LienDatabase_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles LienDatabase.PreRender
        If WebFct Is Nothing Then
            WebFct = New Virtualia.Net.WebFonctions(Me, 2)
        End If
        If WebFct.PointeurUtilisateur Is Nothing Then
            Exit Sub
        End If
        LienDatabase.Text = WebFct.PointeurUtilisateur.InstanceBd.Intitule
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Call ChargerMenus()
        If WebFct.PointeurUtilisateur Is Nothing Then
            Exit Sub
        End If
        If WebFct.PointeurContexte.Armoire_Identifiant <> 0 Then
            IdentifiantCourant = WebFct.PointeurContexte.Armoire_Identifiant
        End If
        Me.Page.MaintainScrollPositionOnPostBack = True
    End Sub

    Private Sub ChargerMenus()
        If WebFct Is Nothing Then
            WebFct = New Virtualia.Net.WebFonctions(Me, 2)
        End If
        Dim Identification As String = "?IDVirtualia=" & V_NoSession
        'Menu Fenêtres
        MenuPrincipal.Items(0).ChildItems.Item(0).NavigateUrl = "~/Fenetres/FrmInfosPersonnelles.aspx" & Identification
        MenuPrincipal.Items(0).ChildItems.Item(1).NavigateUrl = "~/Fenetres/FrmDiplomesCV.aspx" & Identification
        Select Case WebFct.PointeurGlobal.VirModele.InstanceProduit.ClefModele
            Case VI.ModeleRh.FPCollectivite, VI.ModeleRh.FPPompier, VI.ModeleRh.FPEtat, VI.ModeleRh.AfriquePublic
                MenuPrincipal.Items(0).ChildItems.Item(3).NavigateUrl = "~/Fenetres/FrmSituationAdministrative.aspx" & Identification
                MenuPrincipal.Items(0).ChildItems.Item(4).NavigateUrl = "~/Fenetres/FrmPublicFonctionnelles.aspx" & Identification
                MenuPrincipal.Items(0).ChildItems.Item(5).NavigateUrl = "~/Fenetres/FrmPublicBudgetaire.aspx" & Identification
            Case Else
                MenuPrincipal.Items(0).ChildItems.Item(3).NavigateUrl = "~/Fenetres/FrmSituationContractuelle.aspx" & Identification
                MenuPrincipal.Items(0).ChildItems.Item(4).NavigateUrl = "~/Fenetres/FrmPriveFonctionnelles.aspx" & Identification
                MenuPrincipal.Items(0).ChildItems.Item(5).NavigateUrl = "~/Fenetres/FrmPriveBudgetaire.aspx" & Identification
        End Select
        MenuPrincipal.Items(0).ChildItems.Item(7).NavigateUrl = "~/Fenetres/FrmTempsTravail.aspx" & Identification
        MenuPrincipal.Items(0).ChildItems.Item(8).NavigateUrl = "~/Fenetres/FrmFormationContinue.aspx" & Identification

        Select Case WebFct.PointeurGlobal.VirModele.InstanceProduit.NumeroLicence
            Case Is = "2203110" 'Conseil d'Etat Magistrats
                MenuPrincipal.Items(0).ChildItems.Item(9).NavigateUrl = "~/Fenetres/FrmEntretienMagistrat.aspx" & Identification
            Case Else
                MenuPrincipal.Items(0).ChildItems.Item(9).NavigateUrl = "~/Fenetres/FrmPublicEvaluation.aspx" & Identification
        End Select

        MenuPrincipal.Items(0).ChildItems.Item(10).NavigateUrl = "~/Fenetres/FrmRemuneration.aspx" & Identification
        MenuPrincipal.Items(0).ChildItems.Item(12).NavigateUrl = "~/Fenetres/FrmPublicFraisMission.aspx" & Identification
        MenuPrincipal.Items(0).ChildItems.Item(13).NavigateUrl = "~/Fenetres/FrmInfosSociales.aspx" & Identification
        MenuPrincipal.Items(0).ChildItems.Item(15).NavigateUrl = "~/Fenetres/FrmPublicRetraite.aspx" & Identification

        'Menu Armoires
        MenuPrincipal.Items(1).ChildItems.Item(0).NavigateUrl = "~/Fenetres/FrmCmcExtraction.aspx" & Identification

        'Menu Points de vue
        MenuPrincipal.Items(2).ChildItems.Item(0).NavigateUrl = "~/Administration/FrmSystemeReferences.aspx" & Identification

        'Menu Paramètres
        MenuPrincipal.Items(3).ChildItems.Item(0).NavigateUrl = "~/Administration/FrmRegleDeGestion.aspx" & Identification

        'Liens
        LienDatabase.NavigateUrl = "~/Administration/FrmChangerBaseCourante.aspx" & Identification
        LienDeconnexion.NavigateUrl = "~/Fenetres/Connexion/FrmDeconnexion.aspx" & Identification
    End Sub

    Protected Sub HorlogeVirtuelle_Tick(ByVal sender As Object, ByVal e As System.EventArgs) Handles HorlogeVirtuelle.Tick
        EtiHeure.Text = Format(TimeOfDay, "t")
    End Sub
End Class

