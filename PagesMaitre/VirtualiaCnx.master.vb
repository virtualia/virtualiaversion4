﻿Public Class VirtualiaCnx
    Inherits System.Web.UI.MasterPage

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Dim WebFct As Virtualia.Net.WebFonctions = New Virtualia.Net.WebFonctions(Me, 2)
        EtiDateJour.Text = WebFct.ViRhDates.ClairDate(WebFct.ViRhDates.DateduJour, True)
        EtiHeure.Text = Format(TimeOfDay, "t")
    End Sub

    Protected Sub HorlogeVirtuelle_Tick(ByVal sender As Object, ByVal e As System.EventArgs) Handles HorlogeVirtuelle.Tick
        EtiHeure.Text = Format(TimeOfDay, "t")
    End Sub

End Class