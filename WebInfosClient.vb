﻿Option Strict On
Option Explicit On
Option Compare Text
Imports Microsoft.VisualBasic
Public Class WebInfosClient
    Private WsNomConnexion As String
    Private WsSiAnonyme As Boolean
    Private WsSiAuthentifie As Boolean
    Private WsNomNavigateur As String
    Private WsVersionNavigateur As String
    Private WsAdresseIPClient As String
    Private WsNomMachineClient As String
    Private WsCheminPhysiqueAppli As String
    Private WsCheminPhysiqueFichiers As String
    Private WsUrlAppli As String
    Private WsUrlAbsolue As String
    Private WsUrlRelative As String
    Private WsUrlCourante As String
    Private WsUrlRepertoireVirtuel As String
    Private WsMethodeHTTP As String
    Private WsSiHttps As Boolean

    Public Property Client_AdresseDNS() As String
        Get
            Return WsNomMachineClient
        End Get
        Set(ByVal value As String)
            WsNomMachineClient = value
        End Set
    End Property

    Public Property Client_AdresseIP() As String
        Get
            Return WsAdresseIPClient
        End Get
        Set(ByVal value As String)
            WsAdresseIPClient = value
        End Set
    End Property

    Public Property Utilisateur_Nom() As String
        Get
            Return WsNomConnexion
        End Get
        Set(ByVal value As String)
            WsNomConnexion = value
        End Set
    End Property

    Public Property CheminPhysique_Application() As String
        Get
            Return WsCheminPhysiqueAppli
        End Get
        Set(ByVal value As String)
            WsCheminPhysiqueAppli = value
        End Set
    End Property

    Public Property CheminPhysique_Fichiers() As String
        Get
            Return WsCheminPhysiqueFichiers
        End Get
        Set(ByVal value As String)
            WsCheminPhysiqueFichiers = value
        End Set
    End Property

    Public Property SiHttps() As Boolean
        Get
            Return WsSiHttps
        End Get
        Set(ByVal value As Boolean)
            WsSiHttps = value
        End Set
    End Property

    Public Property SiClientAuthentifie() As Boolean
        Get
            Return WsSiAuthentifie
        End Get
        Set(ByVal value As Boolean)
            WsSiAuthentifie = value
        End Set
    End Property

    Public Property SiClientAnonyme() As Boolean
        Get
            Return WsSiAnonyme
        End Get
        Set(ByVal value As Boolean)
            WsSiAnonyme = value
        End Set
    End Property

    Public Property MethodeHTTP() As String
        Get
            Return WsMethodeHTTP
        End Get
        Set(ByVal value As String)
            WsMethodeHTTP = value
        End Set
    End Property

    Public Property Url_RepertoireVirtuel() As String
        Get
            Return WsUrlRepertoireVirtuel
        End Get
        Set(ByVal value As String)
            WsUrlRepertoireVirtuel = value
        End Set
    End Property

    Public Property Url_Absolue() As String
        Get
            Return WsUrlAbsolue
        End Get
        Set(ByVal value As String)
            WsUrlAbsolue = value
        End Set
    End Property

    Public Property Url_Relative() As String
        Get
            Return WsUrlRelative
        End Get
        Set(ByVal value As String)
            WsUrlRelative = value
        End Set
    End Property

    Public Property Url_Racine() As String
        Get
            Return WsUrlAppli
        End Get
        Set(ByVal value As String)
            WsUrlAppli = value
        End Set
    End Property

    Public Property Url_Courante() As String
        Get
            Return WsUrlCourante
        End Get
        Set(ByVal value As String)
            WsUrlCourante = value
        End Set
    End Property

End Class
