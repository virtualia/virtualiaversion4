﻿Option Explicit On
Option Strict On
Option Compare Text
Imports VI = Virtualia.Systeme.Constantes
Namespace Session
    Public Class ObjetSession
        Inherits Virtualia.Ressources.Session.Utilisateur
        Implements IDisposable
        Private disposedValue As Boolean = False        ' Pour détecter les appels redondants
        Private WsRegistreVirtualia As Virtualia.Ressources.Registre.ObjetConfiguration
        Private WsParametres As Virtualia.Net.Datas.ParametresGlobaux
        Private WsCollSysRef As List(Of Virtualia.Ressources.Datas.ObjetDossierREF) = Nothing
        Private WsDossierSysRef As Virtualia.Ressources.Datas.ObjetDossierREF = Nothing

        Private WsRhPartition As Virtualia.Metier.Expertes.Partition = Nothing
        Private WsParent As Virtualia.Net.Session.ObjetGlobal
        Private WsChaineActivite As System.Text.StringBuilder
        Private WsListeArmoires As List(Of Virtualia.Net.Datas.ObjetArmoire)
        Private WsFiltreEtablissement As String = ""
        '
        Private WsSiCouleurDefaut As Boolean = True
        '
        Private WsCritereObjetArmoire As Short = 0
        Private WsCritereInfoArmoire As Short = 0
        Private WsCritereSiObjetDate As Boolean = False
        Private WsCritereCouleur(14) As Integer
        Private WsCritereValeur(14) As String
        '
        Private Const BaseComplete As Short = 0
        Private Const EnActivite As Short = 1
        Private Const Personnalisee As Short = 1000
        Private Const LePanel As Short = 1001
        '
        'TS - Objet Navigation pour le contexte
        Private WsContexte As Virtualia.Net.Session.ObjetNavigation
        Private WsProxySauvegarde As Virtualia.Ressources.WebService.Serveur
        'Objets Temps de travail
        Private WsCollDossierRTT As Virtualia.Metier.TempsTravail.EnsembleDossiers
        'Objets Script Pour la gestion des scripts CMC, Edition etc...
        Private WsScriptsCMC As Virtualia.Net.Script.EnsembleScripts
        Private WsScriptsExtraction As Virtualia.Net.Script.EnsembleScripts
        Private WsResultatCMC As Virtualia.Net.Datas.ObjetSelection
        'Objet Vues Restitution
        Private WsCollRestitution As Virtualia.Net.Script.EnsembleRestitution

        Public ReadOnly Property VParent As Virtualia.Net.Session.ObjetGlobal
            Get
                Return WsParent
            End Get
        End Property

        Public ReadOnly Property PointeurDllExpert As Virtualia.Metier.Expertes.Partition
            Get
                If WsRhPartition Is Nothing Then
                    WsRhPartition = New Virtualia.Metier.Expertes.Partition(WsParent.VirModele, _
                              InstanceBd, WsParent.VirUrlWebServeur)
                    WsRhPartition.NomUtilisateur = V_NomdUtilisateurSgbd
                    WsRhPartition.PreselectiondeDossiers = ""
                End If
                Return WsRhPartition
            End Get
        End Property

        Public ReadOnly Property PointeurDllRTT As Virtualia.Metier.TempsTravail.EnsembleDossiers
            Get
                If WsCollDossierRTT Is Nothing Then
                    WsCollDossierRTT = New Virtualia.Metier.TempsTravail.EnsembleDossiers(VParent.VirModele, _
                                       InstanceBd, WsParent.VirUrlWebServeur, V_NomdUtilisateurSgbd)

                    WsCollDossierRTT.RegistreVirtualia = PointeurRegistre
                    WsCollDossierRTT.PointeurUniteCycles = PointeurParametres.PointeurUniteCycles
                    WsCollDossierRTT.PointeurCyclesTravail = PointeurParametres.PointeurCyclesTravail
                    PointeurParametres.PointeurCyclesTravail.PointeurUniteCycles = PointeurParametres.PointeurUniteCycles
                    WsCollDossierRTT.PointeurStagesFormation = PointeurParametres.PointeurStagesFormation
                End If
                Return WsCollDossierRTT
            End Get
        End Property

        Public ReadOnly Property PointeurScripts(ByVal PtdeVueOutil As Short, ByVal PtdeVueAppli As Short) As Virtualia.Net.Script.EnsembleScripts
            Get
                Select Case PtdeVueOutil
                    Case VI.PointdeVue.PVueScriptCmc
                        If WsScriptsCMC Is Nothing Then
                            WsScriptsCMC = New Virtualia.Net.Script.EnsembleScripts(Me, PtdeVueOutil, PtdeVueAppli)
                        End If
                        Return WsScriptsCMC
                    Case VI.PointdeVue.PVueScriptExport
                        If WsScriptsExtraction Is Nothing Then
                            WsScriptsExtraction = New Virtualia.Net.Script.EnsembleScripts(Me, PtdeVueOutil, PtdeVueAppli)
                        End If
                        Return WsScriptsExtraction
                    Case Else
                        Return Nothing
                End Select
            End Get
        End Property

        Public ReadOnly Property PointeurNewCMCExperte(ByVal InfoExperte As Virtualia.Systeme.MetaModele.Expertes.InformationExperte, ByVal OpeComparaison As Short, ByVal ChaineComparaison As String) As Virtualia.Net.Datas.ObjetSelection
            Get
                If WsResultatCMC IsNot Nothing Then
                    WsResultatCMC.Dispose()
                    WsResultatCMC = Nothing
                End If
                WsResultatCMC = New Virtualia.Net.Datas.ObjetSelection(Me, InfoExperte, OpeComparaison, ChaineComparaison)
                Return WsResultatCMC
            End Get
        End Property

        Public ReadOnly Property PointeurResultatCMC(ByVal PtdeVue As Short) As Virtualia.Net.Datas.ObjetSelection
            Get
                If PtdeVue = 0 Then
                    If WsResultatCMC IsNot Nothing Then
                        WsResultatCMC.Dispose()
                        WsResultatCMC = Nothing
                    End If
                Else
                    If WsResultatCMC Is Nothing Then
                        WsResultatCMC = New Virtualia.Net.Datas.ObjetSelection(Me, PtdeVue)
                    End If
                End If
                Return WsResultatCMC
            End Get
        End Property

        Public ReadOnly Property PointeurSysRef As List(Of Virtualia.Ressources.Datas.ObjetDossierREF)
            Get
                Return WsCollSysRef
            End Get
        End Property

        Public ReadOnly Property PointeurDossier_SysRef(ByVal PointdeVue As Short, ByVal NumObjet As Short, ByVal Nomtable As String, ByVal Ide As Integer) As Virtualia.Ressources.Datas.ObjetDossierREF
            Get
                Dim Predicat As Virtualia.Ressources.Predicats.PredicateSysRef

                WsCollSysRef = New List(Of Virtualia.Ressources.Datas.ObjetDossierREF)

                If Nomtable <> "" And Ide > 0 Then
                    Predicat = New Virtualia.Ressources.Predicats.PredicateSysRef(Nomtable, Ide)
                    WsDossierSysRef = WsCollSysRef.Find(AddressOf Predicat.RechercherDossier)
                ElseIf Nomtable <> "" Then
                    Predicat = New Virtualia.Ressources.Predicats.PredicateSysRef(Nomtable, "")
                    WsDossierSysRef = WsCollSysRef.Find(AddressOf Predicat.RechercherTable)
                ElseIf Ide > 0 Then
                    Predicat = New Virtualia.Ressources.Predicats.PredicateSysRef(Ide)
                    WsDossierSysRef = WsCollSysRef.Find(AddressOf Predicat.RechercherIdentifiant)
                End If

                If WsDossierSysRef IsNot Nothing Then
                    Return WsDossierSysRef
                    Exit Property
                End If

                Select Case PointdeVue
                    Case VI.PointdeVue.PVuePaie
                        Ide = IdentifiantInverse(PointdeVue, NumObjet.ToString)
                End Select
                Dim IndiceF As Integer
                WsDossierSysRef = New Virtualia.Ressources.Datas.ObjetDossierREF(WsParent.VirUrlWebServeur, WsParent.VirModele, PointdeVue, Ide)
                IndiceF = WsDossierSysRef.V_UnObjetLu(V_NomdUtilisateurSgbd, Nomtable, NumObjet)
                If IndiceF < 0 Then
                    WsDossierSysRef = Nothing
                    Return Nothing
                Else
                    Return WsDossierSysRef
                End If
            End Get
        End Property

        Public ReadOnly Property PointeurParametres As Virtualia.Net.Datas.ParametresGlobaux
            Get
                Return WsParametres
            End Get
        End Property

        Public ReadOnly Property PointeurRegistre As Virtualia.Ressources.Registre.ObjetConfiguration
            Get
                Return WsRegistreVirtualia
            End Get
        End Property

        Public ReadOnly Property IdentifiantInverse(ByVal PointdeVue As Short, ByVal Valeur As String) As Integer
            Get
                Dim Proxy As Virtualia.Ressources.WebService.Serveur
                Dim Constructeur As Virtualia.Systeme.Sgbd.Sql.SqlInterne
                Dim ChaineLue As String
                Dim OrdreSql As String
                Dim TableauObjet(0) As String
                Dim TableauData(0) As String
                Dim Ide As Integer = 0

                Constructeur = New Virtualia.Systeme.Sgbd.Sql.SqlInterne(WsParent.VirModele, InstanceBd)
                Constructeur.NombredeRequetes(PointdeVue, "", "", VI.Operateurs.ET) = 1
                Constructeur.SiPasdeTriSurIdeDossier = True
                Select Case PointdeVue
                    Case VI.PointdeVue.PVuePaie
                        Constructeur.NoInfoSelection(0, 1) = 2
                    Case Else
                        Constructeur.NoInfoSelection(0, 1) = 1
                End Select
                Constructeur.ValeuraComparer(0, VI.Operateurs.OU, VI.Operateurs.Egalite, False) = Valeur
                Constructeur.InfoExtraite(0, 1, 0) = 1

                OrdreSql = Constructeur.OrdreSqlDynamique

                Proxy = New Virtualia.Ressources.WebService.Serveur(WsParent.VirUrlWebServeur)
                ChaineLue = Proxy.SelectionSql(V_NomdUtilisateurSgbd, PointdeVue, 1, OrdreSql, 1)
                Proxy.Dispose()

                If ChaineLue = "" Then
                    Return 0
                    Exit Property
                End If
                TableauData = Strings.Split(ChaineLue, VI.Tild, -1)
                If IsNumeric(TableauData(0)) Then
                    Ide = CInt(TableauData(0))
                End If
                Return Ide
            End Get
        End Property

        Public Sub InitialiserScripts(ByVal PtdeVue As Short)
            Select Case PtdeVue
                Case VI.PointdeVue.PVueScriptCmc
                    If WsScriptsCMC IsNot Nothing Then
                        WsScriptsCMC.Dispose()
                        WsScriptsCMC = Nothing
                    End If
                Case VI.PointdeVue.PVueScriptExport
                    If WsScriptsExtraction IsNot Nothing Then
                        WsScriptsExtraction.Dispose()
                        WsScriptsExtraction = Nothing
                    End If
            End Select
        End Sub

        Public ReadOnly Property PointeurContexte As Virtualia.Net.Session.ObjetNavigation
            Get
                If WsContexte Is Nothing Then
                    WsContexte = New Virtualia.Net.Session.ObjetNavigation(Me)
                End If
                Return WsContexte
            End Get
        End Property

        '** Sauvegardes et restitutions
        Public Property PointeurRestitution As Virtualia.Net.Script.EnsembleRestitution
            Get
                Return WsCollRestitution
            End Get
            Set(ByVal value As Virtualia.Net.Script.EnsembleRestitution)
                WsCollRestitution = value
            End Set
        End Property

        Public ReadOnly Property RequeteSauvegarde(ByVal Chaine As String, ByVal NoPage As Integer) As String
            Get
                Dim Tableaudata(0) As String
                Dim Pvue As Short
                Dim NoObjet As Short
                Dim SqlClause As String

                If Chaine = "" Then
                    If WsProxySauvegarde IsNot Nothing Then
                        WsProxySauvegarde.Dispose()
                    End If
                    Return ""
                    Exit Property
                End If
                Tableaudata = Strings.Split(Chaine, VI.Tild, -1)
                SqlClause = Tableaudata(1)
                Pvue = CShort(Tableaudata(2))
                NoObjet = CShort(Tableaudata(3))
                If WsProxySauvegarde Is Nothing Then
                    WsProxySauvegarde = New Virtualia.Ressources.WebService.Serveur(WsParent.VirUrlWebServeur)
                Else
                    If NoPage = 1 Then
                        WsProxySauvegarde.Dispose()
                        WsProxySauvegarde = New Virtualia.Ressources.WebService.Serveur(WsParent.VirUrlWebServeur)
                    End If
                End If
                Try
                    Return WsProxySauvegarde.SelectionSql(V_NomdUtilisateurSgbd, Pvue, NoObjet, SqlClause, NoPage)
                Catch ex As Exception
                    Call WsParent.EcrireLogTraitement("Sauvegardes", True, "Table = " & Tableaudata(0) & Space(2) & "Erreur = " & ex.Message)
                    Return ""
                End Try
            End Get
        End Property

        Public ReadOnly Property MiseAJourRestitution(ByVal Chaine As String) As Integer
            Get
                Dim Cpt As Integer
                If Chaine = "" Then
                    If WsProxySauvegarde IsNot Nothing Then
                        WsProxySauvegarde.Dispose()
                    End If
                    Return 0
                    Exit Property
                End If
                If WsProxySauvegarde Is Nothing Then
                    WsProxySauvegarde = New Virtualia.Ressources.WebService.Serveur(WsParent.VirUrlWebServeur)
                End If
                Try
                    Cpt = WsProxySauvegarde.MiseAJourFluxSql(V_NomdUtilisateurSgbd, Chaine)
                Catch ex As Exception
                    Cpt = 0
                End Try
                Return Cpt
            End Get
        End Property

        Public Function NombreDossiersArmoire(ByVal TypeArmoireActive As Short) As Integer
            If ItemArmoire(TypeArmoireActive) IsNot Nothing Then
                If ItemArmoire(TypeArmoireActive).NombredeDossiers > 0 Then
                    Return ItemArmoire(TypeArmoireActive).NombredeDossiers
                    Exit Function
                Else
                    Select Case TypeArmoireActive
                        Case Is > 999
                            Return 0
                            Exit Function
                    End Select
                End If
            End If
            If WsListeArmoires Is Nothing Then
                WsListeArmoires = New List(Of Virtualia.Net.Datas.ObjetArmoire)
            End If
            Dim NouvelleArmoire As Virtualia.Net.Datas.ObjetArmoire
            Dim NbDossiers As Integer = 0
            NouvelleArmoire = New Virtualia.Net.Datas.ObjetArmoire(Me, VI.PointdeVue.PVueApplicatif)
            NouvelleArmoire.InfoIconeArmoire(CritereObjetArmoire, SiCritereObjetDate) = CritereInfoArmoire
            NouvelleArmoire.SiArmoireStandard = True
            Select Case TypeArmoireActive
                Case BaseComplete
                    NouvelleArmoire.NomdelArmoire = "Tous les dossiers"
                    NbDossiers = NouvelleArmoire.ArmoireComplete(V_RhDates.DateduJour(False))
                Case EnActivite
                    NouvelleArmoire.NomdelArmoire = "Les dossiers en activité"
                    NbDossiers = NouvelleArmoire.SelectionDynamique(EnActivite, VI.ObjetPer.ObaActivite, 1, VI.Operateurs.OU, VI.Operateurs.Inclu, ChaineActivite, "", V_RhDates.DateduJour(False))
                Case Personnalisee
                    NouvelleArmoire.NomdelArmoire = "Armoire personnalisée"
                    NouvelleArmoire.SiArmoireStandard = False
                Case LePanel
                    NouvelleArmoire.NomdelArmoire = "Le panel issu de la recherche"
                    NouvelleArmoire.SiArmoireStandard = False
            End Select
            NouvelleArmoire.TypeArmoire = TypeArmoireActive
            WsListeArmoires.Add(NouvelleArmoire)
            Select Case TypeArmoireActive
                Case Is > 999
                    Return NouvelleArmoire.NouvelEnsemble.Count
                Case Else
                    Return NbDossiers
            End Select
        End Function

        Public ReadOnly Property ListeArmoires As List(Of Virtualia.Net.Datas.ObjetArmoire)
            Get
                Return WsListeArmoires
            End Get
        End Property

        Default Public ReadOnly Property Item(ByVal Index As Integer) As Virtualia.Net.Datas.ObjetArmoire
            Get
                If WsListeArmoires Is Nothing Then
                    Return Nothing
                End If
                Return WsListeArmoires.Item(Index)
            End Get
        End Property

        Public ReadOnly Property ItemArmoire(ByVal TypeArmoireActive As Short) As Virtualia.Net.Datas.ObjetArmoire
            Get
                If WsListeArmoires Is Nothing Then
                    Return Nothing
                End If
                Dim IndiceA As Integer
                For IndiceA = 0 To WsListeArmoires.Count - 1
                    Select Case Item(IndiceA).TypeArmoire
                        Case Is = TypeArmoireActive
                            Return Item(IndiceA)
                            Exit Property
                    End Select
                Next IndiceA
                Return Nothing
            End Get
        End Property

        Public Sub ActualiserLesArmoires()
            Dim Nb As Integer
            If WsListeArmoires IsNot Nothing Then
                Do Until WsListeArmoires.Count = 0
                    Item(0).Dispose()
                    WsListeArmoires.RemoveAt(0)
                Loop
                WsListeArmoires.Clear()
            End If
            Nb = NombreDossiersArmoire(BaseComplete)
        End Sub

        Public Property SiCouleurParDefaut As Boolean
            Get
                Return WsSiCouleurDefaut
            End Get
            Set(ByVal value As Boolean)
                WsSiCouleurDefaut = value
            End Set
        End Property

        Public ReadOnly Property CritereObjetArmoire As Short
            Get
                Return WsCritereObjetArmoire
            End Get
        End Property

        Public ReadOnly Property CritereInfoArmoire As Short
            Get
                Return WsCritereInfoArmoire
            End Get
        End Property

        Public ReadOnly Property SiCritereObjetDate As Boolean
            Get
                Return WsCritereSiObjetDate
            End Get
        End Property

        Public Property CritereCouleurArmoire(ByVal Index As Integer) As Integer
            Get
                Select Case Index
                    Case 0 To 14
                        Return WsCritereCouleur(Index)
                    Case Else
                        Return 15
                End Select
            End Get
            Set(ByVal value As Integer)
                Dim IconeSave As Integer = 0
                Dim IndiceI As Integer
                Select Case Index
                    Case 0 To 14
                        IconeSave = WsCritereCouleur(Index)
                        WsCritereCouleur(Index) = value
                        For IndiceI = 0 To 14
                            If WsCritereCouleur(IndiceI) = value And IndiceI <> Index Then
                                WsCritereCouleur(IndiceI) = IconeSave
                                Exit For
                            End If
                        Next IndiceI
                End Select
            End Set
        End Property

        Public Property CritereValeurArmoire(ByVal Index As Integer) As String
            Get
                Select Case Index
                    Case 0 To 14
                        Return WsCritereValeur(Index)
                    Case Else
                        Return ""
                End Select
            End Get
            Set(ByVal value As String)
                Select Case Index
                    Case 0 To 14
                        WsCritereValeur(Index) = value
                End Select
            End Set
        End Property

        Public ReadOnly Property ChaineActivite As String
            Get
                Return WsChaineActivite.ToString
            End Get
        End Property

        Public ReadOnly Property TableActivite As ArrayList
            Get
                Dim I As Integer
                Dim TabActivite As New ArrayList
                Dim TableauData(0) As String
                TableauData = Strings.Split(ChaineActivite, VI.PointVirgule, -1)
                For I = 0 To TableauData.Count - 1
                    TabActivite.Add(TableauData(I))
                Next I
                Return TabActivite
            End Get
        End Property

        Public Function ChangerBaseCourante(ByVal NoBd As Short) As Boolean
            Dim CodeRetour As Boolean
            Dim Nb As Integer

            CodeRetour = V_ChangerBaseCourante(NoBd)
            Select Case CodeRetour
                Case True
                    Call Initialiser()
                    Call LireCouleursDossier()
                    Nb = NombreDossiersArmoire(BaseComplete)
                    Call ChercherPositionsdActivite()
            End Select
            Return CodeRetour
        End Function

        Private Sub Initialiser()
            If WsListeArmoires Is Nothing Then
                Exit Sub
            End If
            Dim IndiceA As Integer
            For IndiceA = 0 To WsListeArmoires.Count - 1
                Item(IndiceA).Dispose()
            Next IndiceA
            WsListeArmoires.Clear()
            If WsContexte IsNot Nothing Then
                WsContexte.Dispose()
                WsContexte = Nothing
            End If
            If WsProxySauvegarde IsNot Nothing Then
                WsProxySauvegarde.Dispose()
                WsProxySauvegarde = Nothing
            End If
            If WsCollDossierRTT IsNot Nothing Then
                WsCollDossierRTT = Nothing
            End If
            If WsRhPartition IsNot Nothing Then
                WsRhPartition = Nothing
            End If
            If WsResultatCMC IsNot Nothing Then
                WsResultatCMC.Dispose()
                WsResultatCMC = Nothing
            End If
            If WsScriptsCMC IsNot Nothing Then
                WsScriptsCMC.Dispose()
                WsScriptsCMC = Nothing
            End If
            If WsScriptsExtraction IsNot Nothing Then
                WsScriptsExtraction.Dispose()
                WsScriptsExtraction = Nothing
            End If
        End Sub

        Public Function FiltreEtablissement() As String
            Dim TableauData(0) As String
            Dim Proxy As New Virtualia.Ressources.WebService.Serveur(WsParent.VirUrlWebServeur)
            TableauData = Strings.Split(Proxy.LireProfilUtilisateur(V_NomdUtilisateurSgbd, _
                                                                        V_NomdUtilisateurSgbd, V_NumeroSgbdActif, 2), VI.Tild, -1)
            Proxy.Dispose()
            Return TableauData(1)
        End Function

        Private Sub LireCouleursDossier()
            Dim TableauData(0) As String
            Dim TableauObjet(0) As String
            Dim Proxy As New Virtualia.Ressources.WebService.Serveur(WsParent.VirUrlWebServeur)
            Dim IndiceP As Integer
            Dim IndiceO As Integer
            Dim IndiceI As Integer
            Dim NoRang As Integer
            Dim SavCouleur As Integer

            For IndiceI = 0 To WsCritereCouleur.Count - 1
                WsCritereCouleur(IndiceI) = IndiceI
            Next IndiceI

            TableauData = Strings.Split(Proxy.LireUnObjet(V_NomdUtilisateurSgbd, VI.PointdeVue.PVueUtilisateur, 4, V_Identifiant, False, 1), VI.Tild, -1)
            Select Case TableauData(0)
                Case Is = ""
                    WsCritereObjetArmoire = 1
                    WsCritereInfoArmoire = 8
                    WsCritereValeur(0) = "Masculin"
                    WsCritereValeur(1) = "Féminin"
                    Proxy.Dispose()
                    Exit Sub
            End Select
            WsCritereObjetArmoire = CShort(TableauData(1))
            WsCritereInfoArmoire = CShort(TableauData(2))
            WsCritereSiObjetDate = False

            For IndiceP = 0 To WsParent.VirModele.NombredePointdeVue - 1
                Select Case WsParent.VirModele.Item(IndiceP).Numero
                    Case Is = 1
                        For IndiceO = 0 To WsParent.VirModele.Item(IndiceP).NombredObjets - 1
                            Select Case WsParent.VirModele.Item(IndiceP).Item(IndiceO).Numero
                                Case Is = WsCritereObjetArmoire
                                    Select Case WsParent.VirModele.Item(IndiceP).Item(IndiceO).VNature
                                        Case Is = VI.TypeObjet.ObjetDate, VI.TypeObjet.ObjetDateRang
                                            WsCritereSiObjetDate = True
                                    End Select
                                    Exit For
                            End Select
                        Next IndiceO
                        Exit For
                End Select
            Next IndiceP

            TableauObjet = Strings.Split(Proxy.LireUnObjet(V_NomdUtilisateurSgbd, VI.PointdeVue.PVueUtilisateur, 5, V_Identifiant, False, 1), VI.SigneBarre, -1)

            For IndiceO = 0 To TableauObjet.Count - 1
                If TableauObjet(IndiceO) = "" Then
                    Exit For
                End If
                TableauData = Strings.Split(TableauObjet(IndiceO), VI.Tild, -1)
                If IsNumeric(TableauData(1)) Then
                    NoRang = CInt(TableauData(1)) - 1 'NumIcone
                    IndiceI = 0
                    Select Case TableauData(2)
                        Case Is <> ""
                            IndiceI = CInt(TableauData(2)) - 1 'Couleur
                    End Select
                    SavCouleur = WsCritereCouleur(NoRang) ' Sauvegarde

                    WsCritereCouleur(NoRang) = IndiceI
                    WsCritereValeur(NoRang) = TableauData(3)

                    For IndiceP = 0 To WsCritereCouleur.Count - 1
                        Select Case IndiceP
                            Case Is <> NoRang
                                Select Case WsCritereCouleur(IndiceP)
                                    Case Is = WsCritereCouleur(NoRang)
                                        WsCritereCouleur(IndiceP) = SavCouleur
                                        Exit For
                                End Select
                        End Select
                    Next IndiceP
                End If
            Next IndiceO

            Proxy.Dispose()

        End Sub

        Private Sub ChercherPositionsdActivite()
            Dim Constructeur As Virtualia.Systeme.Sgbd.Sql.SqlInterne
            Dim Proxy As Virtualia.Ressources.WebService.Serveur
            Dim ChaineSql As String
            Dim ResultatPage As String
            Dim TableauObjet(0) As String
            Dim TableauData(0) As String
            Dim IndiceI As Integer

            Constructeur = New Virtualia.Systeme.Sgbd.Sql.SqlInterne(WsParent.VirModele, InstanceBd)
            Constructeur.NombredeRequetes(VI.PointdeVue.PVuePosition, "", "", VI.Operateurs.ET) = 1
            Constructeur.SiPasdeTriSurIdeDossier = False
            Constructeur.NoInfoSelection(0, 1) = 8
            Constructeur.ValeuraComparer(0, VI.Operateurs.OU, VI.Operateurs.Egalite, False) = "Oui;"
            Constructeur.InfoExtraite(0, 1, 0) = 1
            ChaineSql = Constructeur.OrdreSqlDynamique

            Proxy = New Virtualia.Ressources.WebService.Serveur(WsParent.VirUrlWebServeur)
            ResultatPage = Proxy.SelectionSql(V_NomdUtilisateurSgbd, VI.PointdeVue.PVuePosition, 1, ChaineSql, 1)
            Proxy.Dispose()

            WsChaineActivite = New System.Text.StringBuilder
            TableauObjet = Strings.Split(ResultatPage, VI.SigneBarre, -1)
            For IndiceI = 0 To TableauObjet.Count - 1
                If TableauObjet(IndiceI) = "" Then
                    Exit For
                End If
                TableauData = Split(TableauObjet(IndiceI), VI.Tild, -1)
                WsChaineActivite.Append(TableauData(1) & VI.PointVirgule)
            Next IndiceI
        End Sub

        Public Function LireTableGeneraleSimple(ByVal Intitule As String) As String
            Dim Constructeur As Virtualia.Systeme.Sgbd.Sql.SqlInterne
            Dim Proxy As Virtualia.Ressources.WebService.Serveur
            Dim ChaineSql As String
            Dim ResultatPage As String
            Dim ChaineRes As System.Text.StringBuilder
            Dim IndiceI As Integer

            Constructeur = New Virtualia.Systeme.Sgbd.Sql.SqlInterne(WsParent.VirModele, InstanceBd)
            Constructeur.NombredeRequetes(VI.PointdeVue.PVueGeneral, "", "", VI.Operateurs.ET) = 1
            Constructeur.SiPasdeTriSurIdeDossier = True
            Constructeur.NoInfoSelection(0, 1) = 1
            Constructeur.ValeuraComparer(0, VI.Operateurs.OU, VI.Operateurs.Egalite, False) = Intitule
            Constructeur.InfoExtraite(0, 2, 1) = 1
            Constructeur.InfoExtraite(1, 2, 0) = 2
            ChaineSql = Constructeur.OrdreSqlDynamique

            Proxy = New Virtualia.Ressources.WebService.Serveur(WsParent.VirUrlWebServeur)

            IndiceI = 1
            ChaineRes = New System.Text.StringBuilder
            Do
                ResultatPage = Proxy.SelectionSql(V_NomdUtilisateurSgbd, VI.PointdeVue.PVueGeneral, 2, ChaineSql, IndiceI)
                If ResultatPage Is Nothing Then
                    Exit Do
                End If
                If ResultatPage = "" Then
                    Exit Do
                End If
                ChaineRes.Append(ResultatPage)
                IndiceI += 1
            Loop

            If ChaineRes.ToString <> "" Then
                Proxy.Dispose()
                Return ChaineRes.ToString
                Exit Function
            End If

            '*** Si Aucune valeur et la table générale n'existe pas alors Création de celle-ci. ******
            '*** Si Aucune valuer et que la table générale existe alors initialisation avec (Aucun) ****
            Constructeur = New Virtualia.Systeme.Sgbd.Sql.SqlInterne(WsParent.VirModele, InstanceBd)
            Constructeur.NombredeRequetes(VI.PointdeVue.PVueGeneral, "", "", VI.Operateurs.ET) = 1
            Constructeur.SiPasdeTriSurIdeDossier = True
            Constructeur.NoInfoSelection(0, 1) = 1
            Constructeur.ValeuraComparer(0, VI.Operateurs.OU, VI.Operateurs.Egalite, False) = Intitule
            Constructeur.InfoExtraite(0, 1, 0) = 1
            ChaineSql = Constructeur.OrdreSqlDynamique
            ResultatPage = Proxy.SelectionSql(V_NomdUtilisateurSgbd, VI.PointdeVue.PVueGeneral, 1, ChaineSql, 1)

            Dim FicheRef As New TablesObjet.ShemaREF.TAB_DESCRIPTION
            Dim Cretour As Boolean
            If ResultatPage = "" Then
                IndiceI = Proxy.ObtenirUnCompteur(V_NomdUtilisateurSgbd, "TAB_DESCRIPTION", "Max") + 1
                FicheRef.Ide_Dossier = IndiceI
                FicheRef.Intitule = Intitule
                FicheRef.SiModifiable = "Oui"
                Cretour = Proxy.InsererUneFiche(V_NomdUtilisateurSgbd, VI.PointdeVue.PVueGeneral, 1, IndiceI, FicheRef.ContenuTable)
            Else
                FicheRef.ContenuTable = ResultatPage & VI.Tild & VI.Tild
            End If
            Dim FicheVal As New TablesObjet.ShemaREF.TAB_LISTE
            FicheVal.Ide_Dossier = FicheRef.Ide_Dossier
            FicheVal.Valeur = "(Aucun)"
            FicheVal.Reference = ""
            Cretour = Proxy.InsererUneFiche(V_NomdUtilisateurSgbd, VI.PointdeVue.PVueGeneral, 2, FicheVal.Ide_Dossier, FicheVal.ContenuTable)

            Proxy.Dispose()

            Return FicheVal.Ide_Dossier & VI.Tild & FicheVal.Valeur & VI.Tild
        End Function

        Public Function LireTableSysReference(ByVal NoPvue As Short) As String
            Dim Constructeur As Virtualia.Systeme.Sgbd.Sql.SqlInterne
            Dim Proxy As Virtualia.Ressources.WebService.Serveur
            Dim ChaineSql As String
            Dim ResultatPage As String
            Dim ChaineRes As System.Text.StringBuilder
            Dim IndiceI As Integer

            Constructeur = New Virtualia.Systeme.Sgbd.Sql.SqlInterne(WsParent.VirModele, InstanceBd)
            Constructeur.NombredeRequetes(NoPvue, "", "", VI.Operateurs.ET) = 1
            Constructeur.SiPasdeTriSurIdeDossier = True
            Constructeur.NoInfoSelection(0, 1) = 1
            Constructeur.InfoExtraite(0, 1, 1) = 1
            ChaineSql = Constructeur.OrdreSqlDynamique

            Proxy = New Virtualia.Ressources.WebService.Serveur(WsParent.VirUrlWebServeur)

            IndiceI = 1
            ChaineRes = New System.Text.StringBuilder
            Do
                ResultatPage = Proxy.SelectionSql(V_NomdUtilisateurSgbd, NoPvue, 1, ChaineSql, IndiceI)
                If ResultatPage Is Nothing Then
                    Exit Do
                End If
                If ResultatPage = "" Then
                    Exit Do
                End If
                ChaineRes.Append(ResultatPage)
                IndiceI += 1
            Loop
            Proxy.Dispose()

            Return ChaineRes.ToString
        End Function

        Public Function LireUnObjet(ByVal NoPvue As Short, ByVal NoObjet As Short, ByVal NoPage As Integer) As String
            Dim Chaine As String
            Dim Proxy As New Virtualia.Ressources.WebService.Serveur(WsParent.VirUrlWebServeur)

            Chaine = Proxy.LireUnObjet(V_NomdUtilisateurSgbd, NoPvue, NoObjet, 0, True, NoPage)
            Proxy.Dispose()
            Return Chaine
        End Function

        Public ReadOnly Property InstanceBd As Virtualia.Systeme.Parametrage.BasesdeDonnees.ParamSgbdDatabase
            Get
                Dim IndiceI As Integer

                For IndiceI = 0 To WsParent.VirSgbd.NombredeDatabases - 1
                    Select Case WsParent.VirSgbd.Item(IndiceI).Numero
                        Case Is = V_NumeroSgbdActif
                            Return WsParent.VirSgbd.Item(IndiceI)
                            Exit Property
                    End Select
                Next IndiceI
                Return Nothing
            End Get
        End Property

        Public Function SiBdAutorisee(ByVal NoBd As Short) As Boolean
            Dim TableauObjet(0) As String
            Dim TableauData(0) As String
            Dim IndiceU As Integer
            Dim Proxy As New Virtualia.Ressources.WebService.Serveur(WsParent.VirUrlWebServeur)
            TableauObjet = Strings.Split(Proxy.ListedeTouslesUtilisateurs(NoBd), VI.SigneBarre, -1)
            Proxy.Dispose()
            For IndiceU = 0 To TableauObjet.Count - 1
                If TableauObjet(IndiceU) = "" Then
                    Exit For
                End If
                TableauData = Strings.Split(TableauObjet(IndiceU), VI.Tild, -1)
                Select Case TableauData(0)
                    Case Is = V_NomdUtilisateurSgbd
                        Return True
                        Exit Function
                End Select
            Next IndiceU
            Return False
        End Function

        Public Sub TracerConnexion()
            Dim Chaine As String = "Accés : "
            Chaine &= V_NomdeConnexion & " - " & ID_AdresseIP & " - " & ID_Machine
            Call EcrireLog(Chaine)
        End Sub

        Private Sub EcrireLog(ByVal Msg As String)
            Const SysFicLog As String = "Gestionnaires.log"
            Dim SysCodeIso As System.Text.Encoding = System.Text.Encoding.GetEncoding(1252)
            Dim FicStream As System.IO.FileStream
            Dim FicWriter As System.IO.StreamWriter
            Dim NomLog As String
            Dim NomRep As String

            NomRep = System.Configuration.ConfigurationManager.AppSettings("RepertoireVirtualia")
            NomLog = Systeme.Constantes.DossierVirtualiaService(NomRep, "Logs") & SysFicLog
            FicStream = New System.IO.FileStream(NomLog, IO.FileMode.Append, IO.FileAccess.Write)
            FicWriter = New System.IO.StreamWriter(FicStream, SysCodeIso)
            FicWriter.WriteLine(Format(System.DateTime.Now, "g") & Space(1) & Msg)
            FicWriter.Flush()
            FicWriter.Close()
            FicStream.Close()
        End Sub

        Public Sub New(ByVal Host As Virtualia.Net.Session.ObjetGlobal, ByVal NomUtilisateur As String, ByVal NoBd As Short)
            MyBase.New(NomUtilisateur, NoBd, Host.VirUrlWebServeur, NomUtilisateur)
            WsParent = Host
            Dim SiOk As Boolean = ChangerBaseCourante(NoBd)
            Try
                WsRegistreVirtualia = New Virtualia.Ressources.Registre.ObjetConfiguration( _
                                                NomUtilisateur, Host.VirUrlWebServeur)
            Catch Ex As Exception
                WsRegistreVirtualia = Nothing
            End Try
            WsParametres = New Virtualia.Net.Datas.ParametresGlobaux(Me)
        End Sub

        Public Overloads Sub Dispose() Implements IDisposable.Dispose
            Dispose(True)
            GC.SuppressFinalize(Me)
        End Sub

        Protected Overridable Overloads Sub Dispose(ByVal disposing As Boolean)
            If Not Me.disposedValue Then
                If disposing Then
                    Dim Proxy As Virtualia.Ressources.WebService.Serveur
                    Dim SiOk As Boolean
                    Proxy = New Virtualia.Ressources.WebService.Serveur(WsParent.VirUrlWebServeur)
                    SiOk = Proxy.FermetureSession(V_NomdeConnexion)
                    Proxy.Dispose()
                    Call Initialiser()
                End If
            End If
            Me.disposedValue = True
        End Sub

    End Class
End Namespace

