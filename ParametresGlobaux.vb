﻿Option Strict Off
Option Explicit On
Option Compare Text
Imports VI = Virtualia.Systeme.Constantes
Imports Microsoft.VisualBasic
Namespace Datas
    Public Class ParametresGlobaux
        Implements IDisposable
        Private disposedValue As Boolean = False        ' Pour détecter les appels redondants
        Private WsParent As Virtualia.Net.Session.ObjetSession
        '** Niveaux affectations fonctionnelles
        Private WsNbNiveaux As Integer = 0
        Private WsTabLibelNiveaux(5) As String
        '** Compteurs Congés
        Private WsTabLibelCompteurs As List(Of String)
        '** Couleurs du Planning
        Private WsTabLibelCouleurs(14, 7) As String
        Private WsTabCouleurs(14) As System.Drawing.Color
        '** Discriminants des menus du sytème de référence
        Private WsObjetDistingo As Virtualia.Ressources.Datas.ObjetDiscriminants
        '** OBJETS DATAS - METIER organisés *****************************************
        'Temps de travail
        Private WsCollAbsences As Virtualia.Ressources.Datas.ObjetAbsence
        Private WsCollPresences As Virtualia.Ressources.Datas.ObjetPresence
        Private WsCollUniteCycles As Virtualia.Ressources.Datas.ObjetUniteCycle
        Private WsCollCycleTravail As Virtualia.Ressources.Datas.ObjetCycleTravail
        Private WsCollActiviteMesures As Virtualia.Ressources.Datas.ObjetMesureActivite
        '** SECTEUR PUBLIC : Grades et grilles indiciaires, Postes Budgétaires, LOLF, Frais de Mission ******
        Private WsCollGradeGrilles As Virtualia.Ressources.Datas.ObjetGradeGrille
        Private WsCollPostesBud As Virtualia.Ressources.Datas.ObjetPosteBudgetaire
        Private WsCollLolf As Virtualia.Ressources.Datas.ObjetLOLF
        Private WsCollFraisMission As Virtualia.Ressources.Datas.ObjetMission
        '** Etablissements, Postes Fonctionnels, Commissions
        Private WsCollEtablissments As Virtualia.Ressources.Datas.ObjetEtablissement
        Private WsCollPostesFct As Virtualia.Ressources.Datas.ObjetPosteFonctionnel
        Private WsCollCommissions As Virtualia.Ressources.Datas.ObjetCommission
        '** Formations
        Private WsCollStages As Virtualia.Ressources.Datas.ObjetStageFormation
        '** Divers
        Private WsCollPays As Virtualia.Ressources.Datas.ObjetPays
        Private WsCollItineraires As Virtualia.Ressources.Datas.ObjetItineraire
        Private WsCollInterface As Virtualia.Ressources.Datas.ObjetLogicielExterne

        Public ReadOnly Property PointeurGlobal As Virtualia.Net.Session.ObjetGlobal
            Get
                Return WsParent.VParent
            End Get
        End Property

        '** Objet Discriminant
        Public ReadOnly Property PointeurDistingo() As Virtualia.Ressources.Datas.ObjetDiscriminants
            Get
                If WsObjetDistingo Is Nothing Then
                    Try
                        WsObjetDistingo = New Virtualia.Ressources.Datas.ObjetDiscriminants( _
                               WsParent.V_NomdUtilisateurSgbd, PointeurGlobal.VirUrlWebServeur)
                    Catch Ex As Exception
                        WsObjetDistingo = Nothing
                    End Try
                End If
                Return WsObjetDistingo
            End Get
        End Property

        '** Pointeurs Références organisées
        Public ReadOnly Property PointeurAbsences() As Virtualia.Ressources.Datas.ObjetAbsence
            Get
                If WsCollAbsences Is Nothing Then
                    Try
                        WsCollAbsences = New Virtualia.Ressources.Datas.ObjetAbsence( _
                               WsParent.V_NomdUtilisateurSgbd, PointeurGlobal.VirUrlWebServeur)
                    Catch Ex As Exception
                        WsCollAbsences = Nothing
                    End Try
                End If
                Return WsCollAbsences
            End Get
        End Property

        Public ReadOnly Property PointeurPresences() As Virtualia.Ressources.Datas.ObjetPresence
            Get
                If WsCollPresences Is Nothing Then
                    Try
                        WsCollPresences = New Virtualia.Ressources.Datas.ObjetPresence( _
                               WsParent.V_NomdUtilisateurSgbd, PointeurGlobal.VirUrlWebServeur)
                    Catch Ex As Exception
                        WsCollPresences = Nothing
                    End Try
                End If
                Return WsCollPresences
            End Get
        End Property

        Public ReadOnly Property PointeurGradeGrilles() As Virtualia.Ressources.Datas.ObjetGradeGrille
            Get
                If WsCollGradeGrilles Is Nothing Then
                    Try
                        WsCollGradeGrilles = New Virtualia.Ressources.Datas.ObjetGradeGrille( _
                               WsParent.V_NomdUtilisateurSgbd, PointeurGlobal.VirUrlWebServeur, 0)
                    Catch Ex As Exception
                        WsCollGradeGrilles = Nothing
                    End Try
                End If
                Return WsCollGradeGrilles
            End Get
        End Property

        Public ReadOnly Property PointeurUniteCycles() As Virtualia.Ressources.Datas.ObjetUniteCycle
            Get
                If WsCollUniteCycles Is Nothing Then
                    Try
                        WsCollUniteCycles = New Virtualia.Ressources.Datas.ObjetUniteCycle( _
                               WsParent.V_NomdUtilisateurSgbd, PointeurGlobal.VirUrlWebServeur, 0)
                    Catch Ex As Exception
                        WsCollUniteCycles = Nothing
                    End Try
                End If
                Return WsCollUniteCycles
            End Get
        End Property

        Public ReadOnly Property PointeurCyclesTravail() As Virtualia.Ressources.Datas.ObjetCycleTravail
            Get
                If WsCollCycleTravail Is Nothing Then
                    Try
                        WsCollCycleTravail = New Virtualia.Ressources.Datas.ObjetCycleTravail( _
                               WsParent.V_NomdUtilisateurSgbd, PointeurGlobal.VirUrlWebServeur, 0)
                    Catch Ex As Exception
                        WsCollCycleTravail = Nothing
                    End Try
                End If
                Return WsCollCycleTravail
            End Get
        End Property

        Public ReadOnly Property PointeurActiviteMesures() As Virtualia.Ressources.Datas.ObjetMesureActivite
            Get
                If WsCollActiviteMesures Is Nothing Then
                    Try
                        WsCollActiviteMesures = New Virtualia.Ressources.Datas.ObjetMesureActivite( _
                               WsParent.V_NomdUtilisateurSgbd, PointeurGlobal.VirUrlWebServeur, 0)
                    Catch Ex As Exception
                        WsCollActiviteMesures = Nothing
                    End Try
                End If
                Return WsCollActiviteMesures
            End Get
        End Property

        Public ReadOnly Property PointeurStagesFormation() As Virtualia.Ressources.Datas.ObjetStageFormation
            Get
                If WsCollStages Is Nothing Then
                    Try
                        WsCollStages = New Virtualia.Ressources.Datas.ObjetStageFormation( _
                               WsParent.V_NomdUtilisateurSgbd, PointeurGlobal.VirUrlWebServeur, 0)
                    Catch Ex As Exception
                        WsCollStages = Nothing
                    End Try
                End If
                Return WsCollStages
            End Get
        End Property

        Public ReadOnly Property PointeurPostesBudgetaires() As Virtualia.Ressources.Datas.ObjetPosteBudgetaire
            Get
                If WsCollPostesBud Is Nothing Then
                    Try
                        WsCollPostesBud = New Virtualia.Ressources.Datas.ObjetPosteBudgetaire( _
                               WsParent.V_NomdUtilisateurSgbd, PointeurGlobal.VirUrlWebServeur)
                    Catch Ex As Exception
                        WsCollPostesBud = Nothing
                    End Try
                End If
                Return WsCollPostesBud
            End Get
        End Property

        Public ReadOnly Property PointeurLOLF() As Virtualia.Ressources.Datas.ObjetLOLF
            Get
                If WsCollLolf Is Nothing Then
                    Try
                        WsCollLolf = New Virtualia.Ressources.Datas.ObjetLOLF( _
                               WsParent.V_NomdUtilisateurSgbd, PointeurGlobal.VirUrlWebServeur)
                    Catch Ex As Exception
                        WsCollLolf = Nothing
                    End Try
                End If
                Return WsCollLolf
            End Get
        End Property

        Public ReadOnly Property PointeurEtablissements() As Virtualia.Ressources.Datas.ObjetEtablissement
            Get
                If WsCollEtablissments Is Nothing Then
                    Try
                        WsCollEtablissments = New Virtualia.Ressources.Datas.ObjetEtablissement( _
                               WsParent.V_NomdUtilisateurSgbd, PointeurGlobal.VirUrlWebServeur)
                    Catch Ex As Exception
                        WsCollEtablissments = Nothing
                    End Try
                End If
                Return WsCollEtablissments
            End Get
        End Property

        Public ReadOnly Property PointeurPostesFonctionnels() As Virtualia.Ressources.Datas.ObjetPosteFonctionnel
            Get
                If WsCollPostesFct Is Nothing Then
                    Try
                        WsCollPostesFct = New Virtualia.Ressources.Datas.ObjetPosteFonctionnel( _
                               WsParent.V_NomdUtilisateurSgbd, PointeurGlobal.VirUrlWebServeur, 0)
                    Catch Ex As Exception
                        WsCollPostesFct = Nothing
                    End Try
                End If
                Return WsCollPostesFct
            End Get
        End Property

        Public ReadOnly Property PointeurCommissions() As Virtualia.Ressources.Datas.ObjetCommission
            Get
                If WsCollCommissions Is Nothing Then
                    Try
                        WsCollCommissions = New Virtualia.Ressources.Datas.ObjetCommission( _
                               WsParent.V_NomdUtilisateurSgbd, PointeurGlobal.VirUrlWebServeur, 0)
                    Catch Ex As Exception
                        WsCollCommissions = Nothing
                    End Try
                End If
                Return WsCollCommissions
            End Get
        End Property

        Public ReadOnly Property PointeurFraisMission() As Virtualia.Ressources.Datas.ObjetMission
            Get
                If WsCollFraisMission Is Nothing Then
                    Try
                        WsCollFraisMission = New Virtualia.Ressources.Datas.ObjetMission( _
                               WsParent.V_NomdUtilisateurSgbd, PointeurGlobal.VirUrlWebServeur, 0)
                    Catch Ex As Exception
                        WsCollFraisMission = Nothing
                    End Try
                End If
                Return WsCollFraisMission
            End Get
        End Property

        Public ReadOnly Property PointeurPays() As Virtualia.Ressources.Datas.ObjetPays
            Get
                If WsCollPays Is Nothing Then
                    Try
                        WsCollPays = New Virtualia.Ressources.Datas.ObjetPays( _
                               WsParent.V_NomdUtilisateurSgbd, PointeurGlobal.VirUrlWebServeur)
                    Catch Ex As Exception
                        WsCollPays = Nothing
                    End Try
                End If
                Return WsCollPays
            End Get
        End Property

        Public ReadOnly Property PointeurItineraire() As Virtualia.Ressources.Datas.ObjetItineraire
            Get
                If WsCollItineraires Is Nothing Then
                    Try
                        WsCollItineraires = New Virtualia.Ressources.Datas.ObjetItineraire( _
                               WsParent.V_NomdUtilisateurSgbd, PointeurGlobal.VirUrlWebServeur)
                    Catch Ex As Exception
                        WsCollItineraires = Nothing
                    End Try
                End If
                Return WsCollItineraires
            End Get
        End Property

        Public ReadOnly Property PointeurLogicielExterne() As Virtualia.Ressources.Datas.ObjetLogicielExterne
            Get
                If WsCollInterface Is Nothing Then
                    Try
                        WsCollInterface = New Virtualia.Ressources.Datas.ObjetLogicielExterne( _
                               WsParent.V_NomdUtilisateurSgbd, PointeurGlobal.VirUrlWebServeur, 0)
                    Catch Ex As Exception
                        WsCollInterface = Nothing
                    End Try
                End If
                Return WsCollInterface
            End Get
        End Property

        Public Property LibellePlanning(ByVal Index As Integer, ByVal Rang As Integer) As String
            Get
                Select Case Index
                    Case 0 To 14
                        Select Case Rang
                            Case 0 To 7
                                Return WsTabLibelCouleurs(Index, Rang)
                            Case Else
                                Return ""
                        End Select
                    Case Else
                        Return ""
                End Select
            End Get
            Set(ByVal value As String)
                Select Case Index
                    Case 0 To 14
                        Select Case Rang
                            Case 0 To 7
                                WsTabLibelCouleurs(Index, Rang) = value
                        End Select
                End Select
            End Set
        End Property

        Public Property CouleurPlanning(ByVal Index As Integer) As System.Drawing.Color
            Get
                Select Case Index
                    Case 0 To 14
                        Return WsTabCouleurs(Index)
                    Case Else
                        Return Drawing.Color.LightGray
                End Select
            End Get
            Set(ByVal value As System.Drawing.Color)
                Select Case Index
                    Case 0 To 14
                        WsTabCouleurs(Index) = value
                End Select
            End Set
        End Property

        Public ReadOnly Property IndexCouleurPlanning(ByVal Intitule As String) As Integer
            Get
                Dim I As Integer
                Dim R As Integer
                For I = 0 To 14
                    For R = 1 To 7
                        If WsTabLibelCouleurs(I, R) = Intitule Then
                            Return I
                        End If
                    Next R
                Next I
                Return 1
            End Get
        End Property

        Public ReadOnly Property NombreNiveaux_Organigramme() As Integer
            Get
                If WsNbNiveaux > 0 Then
                    Return WsNbNiveaux
                End If
                Dim I As Integer
                WsTabLibelNiveaux(0) = WsParent.PointeurRegistre.Valeur("Fonctionnel", "Environnement", "Organigramme", "Niveau_1")
                WsTabLibelNiveaux(1) = WsParent.PointeurRegistre.Valeur("Fonctionnel", "Environnement", "Organigramme", "Niveau_2")
                WsTabLibelNiveaux(2) = WsParent.PointeurRegistre.Valeur("Fonctionnel", "Environnement", "Organigramme", "Niveau_3")
                WsTabLibelNiveaux(3) = WsParent.PointeurRegistre.Valeur("Fonctionnel", "Environnement", "Organigramme", "Niveau_4")
                WsTabLibelNiveaux(4) = WsParent.PointeurRegistre.Valeur("Fonctionnel", "Environnement", "Organigramme", "Niveau_5")
                WsTabLibelNiveaux(5) = WsParent.PointeurRegistre.Valeur("Fonctionnel", "Environnement", "Organigramme", "Niveau_6")

                For I = 0 To 5
                    If WsTabLibelNiveaux(I) = "" Then
                        Exit For
                    End If
                    WsNbNiveaux += 1
                Next I
                If WsNbNiveaux = 0 Then
                    WsTabLibelNiveaux(0) = "Direction"
                    WsTabLibelNiveaux(1) = "Service"
                    WsNbNiveaux = 2
                End If
                Return WsNbNiveaux
            End Get
        End Property

        Public ReadOnly Property LibelleNiveau_Organigramme(ByVal Index As Integer) As String
            Get
                Select Case Index
                    Case 0 To NombreNiveaux_Organigramme - 1
                        Return WsTabLibelNiveaux(Index)
                End Select
                Return ""
            End Get
        End Property

        Public ReadOnly Property LibelleCompteur_Conge(ByVal Index As Integer) As String
            Get
                If WsTabLibelCompteurs Is Nothing Then
                    WsTabLibelCompteurs = New List(Of String)
                    WsTabLibelCompteurs.Add(WsParent.PointeurRegistre.Valeur("Fonctionnel", "Environnement", "Compteurs_Conges", "Conges_Annuels"))
                    WsTabLibelCompteurs.Add(WsParent.PointeurRegistre.Valeur("Fonctionnel", "Environnement", "Compteurs_Conges", "Journees_ARTT"))
                    WsTabLibelCompteurs.Add(WsParent.PointeurRegistre.Valeur("Fonctionnel", "Environnement", "Compteurs_Conges", "Compteur_1"))
                    WsTabLibelCompteurs.Add(WsParent.PointeurRegistre.Valeur("Fonctionnel", "Environnement", "Compteurs_Conges", "Compteur_2"))
                    WsTabLibelCompteurs.Add(WsParent.PointeurRegistre.Valeur("Fonctionnel", "Environnement", "Compteurs_Conges", "Compteur_3"))
                    WsTabLibelCompteurs.Add(WsParent.PointeurRegistre.Valeur("Fonctionnel", "Environnement", "Compteurs_Conges", "Repos_Forfaitaire"))
                    WsTabLibelCompteurs.Add(WsParent.PointeurRegistre.Valeur("Fonctionnel", "Environnement", "Compteurs_Conges", "Repos_Compensateur"))
                End If
                Select Case Index
                    Case 0 To WsTabLibelCompteurs.Count - 1
                        Return WsTabLibelCompteurs(Index)
                    Case Else
                        Return ""
                End Select
            End Get
        End Property

        Public ReadOnly Property Parametre_Absence(ByVal Valeur As String) As String
            Get
                Return WsParent.PointeurRegistre.Valeur("Fonctionnel", "Environnement", "Parametres_Absence", Valeur)
            End Get
        End Property

        Public Sub ChargerCouleursPlanning()
            Dim IndiceA As Integer
            Dim DossierCouleur As Virtualia.Ressources.Datas.ObjetDossierREF

            WsTabLibelCouleurs.Initialize()
            WsTabCouleurs.Initialize()

            'Initialisation Couleurs par défaut
            For IndiceA = 0 To 7
                Select Case IndiceA
                    Case 0
                        CouleurPlanning(IndiceA) = Drawing.Color.LightGray
                        LibellePlanning(IndiceA, 0) = "Jours travaillés"
                    Case 1
                        CouleurPlanning(IndiceA) = Drawing.Color.Gray
                        LibellePlanning(IndiceA, 0) = "Jours non travaillés"
                    Case 2
                        CouleurPlanning(IndiceA) = Drawing.Color.Snow
                        LibellePlanning(IndiceA, 0) = "Missions"
                    Case 3
                        CouleurPlanning(IndiceA) = Drawing.Color.Green
                        LibellePlanning(IndiceA, 0) = "Formations"
                    Case 4
                        CouleurPlanning(IndiceA) = Drawing.Color.Orange
                        LibellePlanning(IndiceA, 0) = "Prévisionnel"
                    Case 5
                        CouleurPlanning(IndiceA) = Drawing.Color.LightBlue
                        LibellePlanning(IndiceA, 0) = "Congés annuels"
                    Case 6
                        CouleurPlanning(IndiceA) = Drawing.Color.Salmon
                        LibellePlanning(IndiceA, 0) = "Journées ARTT"
                    Case 7
                        CouleurPlanning(IndiceA) = Drawing.Color.Yellow
                        LibellePlanning(IndiceA, 0) = "Maladies et maternités"
                End Select
            Next IndiceA
            DossierCouleur = WsParent.PointeurDossier_SysRef(VI.PointdeVue.PVuePaie, 24, "PAI_COULEURS", 0)
            If DossierCouleur Is Nothing Then
                Exit Sub
            End If
            Dim IndexF As Integer
            Dim FicheRef As TablesObjet.ShemaREF.PAI_COULEURS
            Dim IndiceI As Integer = 0
            Dim CouleurHexa As String
            Dim ColStocke As String

            Do
                IndexF = DossierCouleur.V_IndexFiche(IndiceI, "PAI_COULEURS")
                If IndexF = -1 Then
                    Exit Do
                End If
                FicheRef = CType(DossierCouleur.Item(IndexF), TablesObjet.ShemaREF.PAI_COULEURS)
                IndiceA = FicheRef.Rang - 1
                CouleurHexa = Hex(FicheRef.Couleur)
                Select Case IndiceA
                    Case 0 To 14
                        Select Case CouleurHexa.Length
                            Case Is = 6
                                ColStocke = "#" & Strings.Right(CouleurHexa, 2) & Strings.Mid(CouleurHexa, 3, 2) & Strings.Left(CouleurHexa, 2)
                            Case Is = 4
                                ColStocke = "#" & Strings.Right(CouleurHexa, 2) & Strings.Left(CouleurHexa, 2) & "00"
                            Case Else
                                ColStocke = "#" & CouleurHexa & "0000"
                        End Select
                        CouleurPlanning(IndiceA) = ConvertCouleur(ColStocke)
                        LibellePlanning(IndiceA, 0) = FicheRef.Etiquette
                        LibellePlanning(IndiceA, 1) = FicheRef.Valeur_N1
                        LibellePlanning(IndiceA, 2) = FicheRef.Valeur_N2
                        LibellePlanning(IndiceA, 3) = FicheRef.Valeur_N3
                        LibellePlanning(IndiceA, 4) = FicheRef.Valeur_N4
                        LibellePlanning(IndiceA, 5) = FicheRef.Valeur_N5
                        LibellePlanning(IndiceA, 6) = FicheRef.Valeur_N6
                        LibellePlanning(IndiceA, 7) = FicheRef.Valeur_N7
                End Select
                IndiceI += 1
            Loop
        End Sub

        Private Function ConvertCouleur(ByVal Valeur As String) As System.Drawing.Color
            Dim R As Integer
            Dim G As Integer
            Dim B As Integer
            Select Case Valeur.Length
                Case Is = 6
                    R = CInt(Val("&H" & Strings.Left(Valeur, 2) & "&"))
                    G = CInt(Val("&H" & Strings.Mid(Valeur, 3, 2) & "&"))
                    B = CInt(Val("&H" & Strings.Right(Valeur, 2) & "&"))
                    Return System.Drawing.Color.FromArgb(R, G, B)
                Case Is = 7
                    R = CInt(Val("&H" & Strings.Mid(Valeur, 2, 2) & "&"))
                    G = CInt(Val("&H" & Strings.Mid(Valeur, 4, 2) & "&"))
                    B = CInt(Val("&H" & Strings.Right(Valeur, 2) & "&"))
                    Return System.Drawing.Color.FromArgb(R, G, B)
                Case Else
                    Return Drawing.Color.White
            End Select
        End Function

        Public Sub Actualiser()
            Call ChargerCouleursPlanning()
            If WsObjetDistingo IsNot Nothing Then
                WsObjetDistingo = Nothing
            End If
            Dim CollVirtualia As Object
            CollVirtualia = PointeurDistingo
            CollVirtualia = PointeurUniteCycles
            CollVirtualia = PointeurCyclesTravail
            CollVirtualia = PointeurStagesFormation
        End Sub

        Public Sub New(ByVal host As Virtualia.Net.Session.ObjetSession)
            WsParent = host
            Call Actualiser()
        End Sub

        Public Overloads Sub Dispose() Implements IDisposable.Dispose
            Dispose(True)
            GC.SuppressFinalize(Me)
        End Sub

        Protected Overridable Overloads Sub Dispose(ByVal disposing As Boolean)
            If Not Me.disposedValue Then
                If disposing Then
                    Erase WsTabLibelCouleurs
                    Erase WsTabCouleurs
                End If
            End If
            Me.disposedValue = True
        End Sub

    End Class
End Namespace
