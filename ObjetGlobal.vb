﻿Option Explicit On
Option Strict On
Option Compare Text
Imports Microsoft.VisualBasic
Imports VI = Virtualia.Systeme.Constantes
Namespace Session
    Public Class ObjetGlobal
        Inherits System.Collections.CollectionBase
        Implements IDisposable
        Private disposedValue As Boolean = False        ' Pour détecter les appels redondants
        Private WsPointeurUtiGlobal As Virtualia.Net.Session.ObjetSession = Nothing
        Private WsRhModele As Virtualia.Systeme.MetaModele.Donnees.ModeleRH
        Private WsRhExpertes As Virtualia.Systeme.MetaModele.Expertes.ExpertesRH
        Private WsRhSgbd As Virtualia.Systeme.Parametrage.BasesdeDonnees.ParamSgbd
        '
        Private WsListeObjetsDico As List(Of Virtualia.Systeme.MetaModele.Outils.FicheObjetDictionnaire) = Nothing
        Private WsListeInfosDico As List(Of Virtualia.Systeme.MetaModele.Outils.FicheInfoDictionnaire) = Nothing
        Private WsListeExpertesDico As List(Of Virtualia.Systeme.MetaModele.Outils.FicheInfoExperte) = Nothing
        Private WsUrlImageArmoire(31) As String
        '
        Private WsUrlWebService As String = System.Configuration.ConfigurationManager.AppSettings("WebService.VirtualiaServeur")
        '
        Private WsModeConnexion As String = "V4" 'V4=Via New Uti gestion, VI=Intranet Standard
        Private WsListeConnexions As List(Of Virtualia.Net.Session.LoginConnexion)
        Private WsListeCnxTmp As List(Of Virtualia.Net.Session.LoginConnexion)
        Private WsListeSessions As List(Of Virtualia.Net.Session.ObjetSession)

        Public ReadOnly Property AjouterUneSession(ByVal NoSession As String, ByVal NomUtilisateur As String, _
                                 ByVal Ide As Integer, ByVal FiltreEtablissement As String, ByVal SiHisto As Integer) As Virtualia.Net.Session.ObjetSession
            Get
                Dim NouveauUser As Virtualia.Net.Session.ObjetSession

                If WsListeSessions Is Nothing Then
                    WsListeSessions = New List(Of Virtualia.Net.Session.ObjetSession)
                End If
                If ItemSession(NomUtilisateur) IsNot Nothing Then
                    Return ItemUtilisateur(NomUtilisateur)
                End If
                NouveauUser = New Virtualia.Net.Session.ObjetSession(Me, NomUtilisateur, 1)
                NouveauUser.V_IDSession = NoSession
                WsListeSessions.Add(NouveauUser)
                Return NouveauUser
            End Get
        End Property

        Public ReadOnly Property ItemSession(ByVal NoSession As String) As Virtualia.Net.Session.ObjetSession
            Get
                If WsListeSessions Is Nothing Then
                    Return Nothing
                End If
                Return WsListeSessions.Find(Function(Recherche) Recherche.V_IDSession = NoSession)
            End Get
        End Property

        Public ReadOnly Property ItemUtilisateur(ByVal NomUtilisateur As String) As Virtualia.Net.Session.ObjetSession
            Get
                If WsListeSessions Is Nothing Then
                    Return Nothing
                End If
                Return WsListeSessions.Find(Function(Recherche) Recherche.V_NomdeConnexion = NomUtilisateur)
            End Get
        End Property

        Public Property Connexion(ByVal NoSession As String) As Virtualia.Net.Session.LoginConnexion
            Get
                If WsListeConnexions Is Nothing Then
                    Return Nothing
                End If
                Return WsListeConnexions.Find(Function(Recherche) Recherche.IDSessionIIS = NoSession)
            End Get
            Set(ByVal value As Virtualia.Net.Session.LoginConnexion)
                If WsListeConnexions Is Nothing Then
                    WsListeConnexions = New List(Of Virtualia.Net.Session.LoginConnexion)
                End If
                WsListeConnexions.Add(value)
            End Set
        End Property

        Public WriteOnly Property Deconnexion As String
            Set(ByVal value As String)
                Dim SessionVirtualia As Virtualia.Net.Session.ObjetSession = ItemSession(value)
                Dim SessionLogin As Virtualia.Net.Session.LoginConnexion = Connexion(value)
                If SessionVirtualia IsNot Nothing Then
                    WsListeSessions.Remove(SessionVirtualia)
                End If
                If SessionLogin IsNot Nothing Then
                    WsListeConnexions.Remove(SessionLogin)
                End If
            End Set
        End Property

        Public ReadOnly Property VirModeConnexxion As String
            Get
                Return WsModeConnexion
            End Get
        End Property

        '** Gestion des mots de passe Changer ou Oublier
        Public Property ConnexionTemporaire(ByVal NoSession As String) As Virtualia.Net.Session.LoginConnexion
            Get
                If WsListeCnxTmp Is Nothing Then
                    Return Nothing
                End If
                Return WsListeCnxTmp.Find(Function(Recherche) Recherche.IDSessionIIS = NoSession)
            End Get
            Set(ByVal value As Virtualia.Net.Session.LoginConnexion)
                If WsListeCnxTmp Is Nothing Then
                    WsListeCnxTmp = New List(Of Virtualia.Net.Session.LoginConnexion)
                End If
                WsListeCnxTmp.Add(value)
            End Set
        End Property

        Public Sub SupprimerCnxTemporaire(ByVal NoSession As String)
            Dim SessionTmp As Virtualia.Net.Session.LoginConnexion = ConnexionTemporaire(NoSession)
            If SessionTmp IsNot Nothing Then
                WsListeCnxTmp.Remove(SessionTmp)
            End If
        End Sub

        Public ReadOnly Property AjouterUnUtilisateur(ByVal NomUtilisateur As String, ByVal NoBd As Short) As Virtualia.Net.Session.ObjetSession
            Get
                Dim NouveauUser As Virtualia.Net.Session.ObjetSession
                Dim I As Integer
                For I = 0 To Me.List.Count - 1
                    If Item(I).V_NomdeConnexion = NomUtilisateur And Item(I).V_NumeroSgbdActif = NoBd Then
                        Return Item(I)
                    End If
                Next I
                NouveauUser = New Virtualia.Net.Session.ObjetSession(Me, NomUtilisateur, NoBd)
                Me.List.Add(NouveauUser)
                Return NouveauUser
            End Get
        End Property

        Default Public ReadOnly Property Item(ByVal Index As Integer) As Virtualia.Net.Session.ObjetSession
            Get
                Return CType(Me.List.Item(Index), Virtualia.Net.Session.ObjetSession)
            End Get
        End Property

        Public ReadOnly Property ItemUti(ByVal Nom As String, ByVal NoBd As Short) As Virtualia.Net.Session.ObjetSession
            Get
                Dim IndiceA As Integer
                For IndiceA = 0 To Me.List.Count - 1
                    Select Case Item(IndiceA).V_NomdeConnexion
                        Case Is = Nom
                            Select Case Item(IndiceA).V_NumeroSgbdActif
                                Case Is = NoBd
                                    Return Item(IndiceA)
                            End Select
                    End Select
                Next IndiceA
                Return Nothing
            End Get
        End Property

        Public ReadOnly Property ItemUti(ByVal IdSession As String) As Virtualia.Net.Session.ObjetSession
            Get
                Dim IndiceA As Integer
                For IndiceA = 0 To Me.List.Count - 1
                    Select Case Item(IndiceA).V_IDSession
                        Case Is = IdSession
                            Return Item(IndiceA)
                    End Select
                Next IndiceA
                Return Nothing
            End Get
        End Property

        Public ReadOnly Property VirUrlWebServeur() As String
            Get
                Return WsUrlWebService
            End Get
        End Property

        Public ReadOnly Property VirModele() As Virtualia.Systeme.MetaModele.Donnees.ModeleRH
            Get
                Return WsRhModele
            End Get
        End Property

        Public ReadOnly Property VirObjet(ByVal PointdeVue As Short, ByVal NumObjet As Short) As Virtualia.Systeme.MetaModele.Donnees.Objet
            Get
                Dim IndiceP As Integer
                Dim IndiceO As Integer
                For IndiceP = 0 To WsRhModele.NombredePointdeVue - 1
                    Select Case WsRhModele.Item(IndiceP).Numero
                        Case Is = PointdeVue
                            For IndiceO = 0 To WsRhModele.Item(IndiceP).NombredObjets - 1
                                Select Case WsRhModele.Item(IndiceP).Item(IndiceO).Numero
                                    Case Is = NumObjet
                                        Return WsRhModele.Item(IndiceP).Item(IndiceO)
                                        Exit Property
                                End Select
                            Next IndiceO
                            Exit For
                    End Select
                Next IndiceP
                Return Nothing
            End Get
        End Property

        Public ReadOnly Property VirExpertes() As Virtualia.Systeme.MetaModele.Expertes.ExpertesRH
            Get
                Return WsRhExpertes
            End Get
        End Property

        Public ReadOnly Property VirSgbd() As Virtualia.Systeme.Parametrage.BasesdeDonnees.ParamSgbd
            Get
                Return WsRhSgbd
            End Get
        End Property

        Public ReadOnly Property VirInstanceBd(ByVal Nobd As Short) As Virtualia.Systeme.Parametrage.BasesdeDonnees.ParamSgbdDatabase
            Get
                Dim IndiceI As Integer

                For IndiceI = 0 To WsRhSgbd.NombredeDatabases - 1
                    Select Case WsRhSgbd.Item(IndiceI).Numero
                        Case Is = Nobd
                            Return WsRhSgbd.Item(IndiceI)
                    End Select
                Next IndiceI
                Return Nothing
            End Get
        End Property

        Public Function VirSiBdAutorisee(ByVal NoBd As Short, ByVal NomUtilisateur As String, ByVal FiltreEtablissement As String) As Boolean
            Dim TableauObjet(0) As String
            Dim TableauData(0) As String
            Dim IndiceU As Integer
            Dim Proxy As New Virtualia.Ressources.WebService.Serveur(VirUrlWebServeur)

            TableauObjet = Strings.Split(Proxy.ListedeTouslesUtilisateurs(NoBd), VI.SigneBarre, -1)
            Proxy.Dispose()
            For IndiceU = 0 To TableauObjet.Count - 1
                If TableauObjet(IndiceU) = "" Then
                    Exit For
                End If
                TableauData = Strings.Split(TableauObjet(IndiceU), VI.Tild, -1)
                Select Case TableauData(0)
                    Case Is = NomUtilisateur
                        Select Case TableauData(3)
                            Case Is = "Administrateur"
                                If FiltreEtablissement = "" Then
                                    FiltreEtablissement = TableauData(4)
                                End If
                                Return True
                            Case Else
                                Return False
                        End Select
                End Select
            Next IndiceU
            Return False
        End Function

        Public ReadOnly Property VirUrlImageSgbd(ByVal TypeduSgbd As Short) As String
            Get
                Select Case TypeduSgbd
                    Case VI.TypeSgbdrNumeric.SgbdrAccess
                        Return "~\Images\Icones\DatabaseJaune.bmp"
                    Case VI.TypeSgbdrNumeric.SgbdrSqlServer, VI.TypeSgbdrNumeric.SgbdrDb2, VI.TypeSgbdrNumeric.SgbdrMySql
                        Return "~\Images\Icones\DatabaseBleu.bmp"
                    Case VI.TypeSgbdrNumeric.SgbdrOracle, VI.TypeSgbdrNumeric.SgbdrProgress
                        Return "~\Images\Icones\DatabaseRouge.bmp"
                    Case Else
                        Return "~\Images\Icones\DatabaseJaune.bmp"
                End Select
            End Get
        End Property

        Public ReadOnly Property VirUrlImageArmoire(ByVal Index As Integer) As String
            Get
                Select Case Index
                    Case 0 To WsUrlImageArmoire.Count - 1
                        Return WsUrlImageArmoire(Index)
                        Exit Property
                End Select
                Return WsUrlImageArmoire(0)
            End Get
        End Property

        Public ReadOnly Property VirUrlNavigateImageArmoire(ByVal Valeur As String) As String
            Get
                Dim IndiceI As Integer
                For IndiceI = 0 To 15
                    Select Case WsUrlImageArmoire(IndiceI)
                        Case Is = Valeur
                            Return WsUrlImageArmoire(IndiceI + 16)
                            Exit Property
                    End Select
                Next IndiceI
                Return ""
            End Get
        End Property

        Public ReadOnly Property VirListeObjetsDico() As List(Of Virtualia.Systeme.MetaModele.Outils.FicheObjetDictionnaire)
            Get
                If WsListeObjetsDico Is Nothing Then
                    WsListeObjetsDico = New List(Of Virtualia.Systeme.MetaModele.Outils.FicheObjetDictionnaire)
                    Dim FicheDico As Virtualia.Systeme.MetaModele.Outils.FicheObjetDictionnaire
                    Dim IndiceP As Integer
                    Dim IndiceO As Integer
                    For IndiceP = 0 To VirModele.NombredePointdeVue - 1
                        For IndiceO = 0 To VirModele.Item(IndiceP).NombredObjets - 1
                            FicheDico = New Virtualia.Systeme.MetaModele.Outils.FicheObjetDictionnaire(VirModele.Item(IndiceP).Item(IndiceO))
                            WsListeObjetsDico.Add(FicheDico)
                            FicheDico.VIndex = WsListeObjetsDico.Count
                        Next IndiceO
                    Next IndiceP
                End If
                Return WsListeObjetsDico
            End Get
        End Property

        Public ReadOnly Property VirListeInfosDico() As List(Of Virtualia.Systeme.MetaModele.Outils.FicheInfoDictionnaire)
            Get
                If WsListeInfosDico Is Nothing Then
                    WsListeInfosDico = New List(Of Virtualia.Systeme.MetaModele.Outils.FicheInfoDictionnaire)
                    Dim FicheDico As Virtualia.Systeme.MetaModele.Outils.FicheInfoDictionnaire
                    Dim IndiceP As Integer
                    Dim IndiceO As Integer
                    Dim IndiceI As Integer
                    For IndiceP = 0 To VirModele.NombredePointdeVue - 1
                        For IndiceO = 0 To VirModele.Item(IndiceP).NombredObjets - 1
                            For IndiceI = 0 To VirModele.Item(IndiceP).Item(IndiceO).NombredInformations - 1
                                FicheDico = New Virtualia.Systeme.MetaModele.Outils.FicheInfoDictionnaire(VirModele.Item(IndiceP).Item(IndiceO).Item(IndiceI))
                                WsListeInfosDico.Add(FicheDico)
                                FicheDico.VIndex = WsListeInfosDico.Count
                            Next IndiceI
                        Next IndiceO
                    Next IndiceP
                End If
                Return WsListeInfosDico
            End Get
        End Property

        Public ReadOnly Property VirListeExpertesDico() As List(Of Virtualia.Systeme.MetaModele.Outils.FicheInfoExperte)
            Get
                If WsListeExpertesDico Is Nothing Then
                    WsListeExpertesDico = New List(Of Virtualia.Systeme.MetaModele.Outils.FicheInfoExperte)
                    Dim FicheDico As Virtualia.Systeme.MetaModele.Outils.FicheInfoExperte
                    Dim IndiceP As Integer
                    Dim IndiceO As Integer
                    Dim IndiceI As Integer
                    For IndiceP = 0 To VirExpertes.NombredePointdeVue - 1
                        For IndiceO = 0 To VirExpertes.Item(IndiceP).NombredObjets - 1
                            For IndiceI = 0 To VirExpertes.Item(IndiceP).Item(IndiceO).NombredInformations - 1
                                FicheDico = New Virtualia.Systeme.MetaModele.Outils.FicheInfoExperte(VirExpertes.Item(IndiceP).Item(IndiceO).Item(IndiceI))
                                WsListeExpertesDico.Add(FicheDico)
                                FicheDico.VIndex = WsListeExpertesDico.Count
                            Next IndiceI
                        Next IndiceO
                    Next IndiceP
                End If
                Return WsListeExpertesDico
            End Get
        End Property

        Public ReadOnly Property RequeteSql(ByVal NomUtiSgbd As String, ByVal Pvue As Short, ByVal NoObjet As Short, ByVal OrdreSql As String) As ArrayList
            Get
                Dim Proxy As Virtualia.Ressources.WebService.Serveur
                Dim ChaineLue As System.Text.StringBuilder
                Dim ChainePage As String
                Dim NoPage As Integer = 1
                Dim TableauObjet(0) As String
                Dim Resultat As ArrayList

                Proxy = New Virtualia.Ressources.WebService.Serveur(VirUrlWebServeur)

                ChaineLue = New System.Text.StringBuilder
                Do
                    ChainePage = Proxy.SelectionSql(NomUtiSgbd, Pvue, NoObjet, OrdreSql, NoPage)
                    If ChainePage = "" Then
                        Exit Do
                    End If
                    ChaineLue.Append(ChainePage)
                    NoPage += 1
                Loop

                Proxy.Dispose()

                TableauObjet = Strings.Split(ChaineLue.ToString, VI.SigneBarre, -1)
                Resultat = New ArrayList
                For NoPage = 0 To TableauObjet.Count - 1
                    If TableauObjet(NoPage) <> "" Then
                        Resultat.Add(TableauObjet(NoPage))
                    End If
                Next NoPage
                Return Resultat
            End Get
        End Property

        Private Sub ObtenirSgbd(ByVal NomUtilisateur As String)
            Dim NomFicXml As String = VI.DossierVirtualiaService(System.Configuration.ConfigurationManager.AppSettings("RepertoireVirtualia"), "Parametrage") & "VirtualiaSgbd.Xml"
            Dim NomFicXsd As String = VI.DossierVirtualiaService(System.Configuration.ConfigurationManager.AppSettings("RepertoireVirtualia"), "Parametrage") & "VirtualiaSgbd.Xsd"
            Dim Proxy As Virtualia.Ressources.WebService.Serveur
            Dim RhOrdi As Virtualia.Systeme.Outils.Ordinateur
            Dim TabChaine As Byte()

            RhOrdi = New Virtualia.Systeme.Outils.Ordinateur
            Proxy = New Virtualia.Ressources.WebService.Serveur(VirUrlWebServeur)

            TabChaine = Proxy.ObtenirFichierXml(NomUtilisateur, "SgbdXml")
            If TabChaine Is Nothing Then
                Exit Sub
            End If
            My.Computer.FileSystem.WriteAllBytes(NomFicXml, TabChaine, False)
            TabChaine = Proxy.ObtenirFichierXml(NomUtilisateur, "SgbdXsd")
            If TabChaine Is Nothing Then
                Exit Sub
            End If
            My.Computer.FileSystem.WriteAllBytes(NomFicXsd, TabChaine, False)

            Proxy.Dispose()

            WsRhSgbd = New Virtualia.Systeme.Parametrage.BasesdeDonnees.ParamSgbd

            'My.Computer.FileSystem.DeleteFile(NomFicXml)
            'My.Computer.FileSystem.DeleteFile(NomFicXsd)

        End Sub

        Private Sub ObtenirModele(ByVal NomUtilisateur As String)
            Dim NomFicXml As String = VI.DossierVirtualiaService(System.Configuration.ConfigurationManager.AppSettings("RepertoireVirtualia"), "Modele") & "VirtualiaModele.Xml"
            Dim NomFicXsd As String = VI.DossierVirtualiaService(System.Configuration.ConfigurationManager.AppSettings("RepertoireVirtualia"), "Modele") & "VirtualiaModele.Xsd"
            Dim Proxy As Virtualia.Ressources.WebService.Serveur
            Dim TabChaine As Byte()
            Dim Chaine As String
            Dim AFaire As Boolean = False

            Proxy = New Virtualia.Ressources.WebService.Serveur(VirUrlWebServeur)
            Select Case My.Computer.FileSystem.FileExists(NomFicXml)
                Case True
                    Chaine = Proxy.InformationsProduit("Version")
                    WsRhModele = New Virtualia.Systeme.MetaModele.Donnees.ModeleRH
                    Select Case WsRhModele.InstanceProduit.NumVersion
                        Case Is <> Chaine
                            AFaire = True
                    End Select
                Case False
                    AFaire = True
            End Select

            Select Case AFaire
                Case False
                    WsRhExpertes = New Virtualia.Systeme.MetaModele.Expertes.ExpertesRH
                Case True
                    TabChaine = Proxy.ObtenirFichierXml(NomUtilisateur, "ModeleXml")
                    If TabChaine Is Nothing Then
                        Exit Sub
                    End If
                    My.Computer.FileSystem.WriteAllBytes(NomFicXml, TabChaine, False)

                    TabChaine = Proxy.ObtenirFichierXml(NomUtilisateur, "ModeleXsd")
                    If TabChaine Is Nothing Then
                        Exit Sub
                    End If
                    My.Computer.FileSystem.WriteAllBytes(NomFicXsd, TabChaine, False)
                    WsRhModele = New Virtualia.Systeme.MetaModele.Donnees.ModeleRH

                    NomFicXml = VI.DossierVirtualiaService(System.Configuration.ConfigurationManager.AppSettings("RepertoireVirtualia"), "Modele") & "VirtualiaExpertes.Xml"
                    NomFicXsd = VI.DossierVirtualiaService(System.Configuration.ConfigurationManager.AppSettings("RepertoireVirtualia"), "Modele") & "VirtualiaExpertes.Xsd"

                    TabChaine = Proxy.ObtenirFichierXml(NomUtilisateur, "ExpertesXml")
                    If TabChaine Is Nothing Then
                        Exit Sub
                    End If
                    My.Computer.FileSystem.WriteAllBytes(NomFicXml, TabChaine, False)

                    TabChaine = Proxy.ObtenirFichierXml(NomUtilisateur, "ExpertesXsd")
                    If TabChaine Is Nothing Then
                        Exit Sub
                    End If
                    My.Computer.FileSystem.WriteAllBytes(NomFicXsd, TabChaine, False)

                    WsRhExpertes = New Virtualia.Systeme.MetaModele.Expertes.ExpertesRH

            End Select

            Proxy.Dispose()

        End Sub

        Public ReadOnly Property ControleDoublon(ByVal PointdeVue As Short, ByVal Ide As Integer, ByVal NumInfo As Short, ByVal Valeur_N1 As String, Optional ByVal Valeur_N2 As String = "", Optional ByVal Valeur_N3 As String = "") As ArrayList
            Get
                Dim Proxy As Virtualia.Ressources.WebService.Serveur
                Dim Constructeur As Virtualia.Systeme.Sgbd.Sql.SqlInterne
                Dim ChaineLue As String
                Dim OrdreSql As String
                Dim TableauObjet(0) As String
                Dim TableauData(0) As String

                Constructeur = New Virtualia.Systeme.Sgbd.Sql.SqlInterne(WsRhModele, WsPointeurUtiGlobal.InstanceBd)
                Constructeur.NombredeRequetes(PointdeVue, "", "", VI.Operateurs.ET) = 1
                Constructeur.SiPasdeTriSurIdeDossier = True

                Constructeur.NoInfoSelection(0, 1) = NumInfo
                Constructeur.ValeuraComparer(0, VI.Operateurs.OU, VI.Operateurs.Egalite, False) = Valeur_N1
                Constructeur.InfoExtraite(0, 1, 0) = NumInfo
                If Valeur_N2 <> "" Then
                    Constructeur.NoInfoSelection(1, 1) = CShort(NumInfo + 1)
                    Constructeur.ValeuraComparer(1, VI.Operateurs.OU, VI.Operateurs.Egalite, False) = Valeur_N2
                    Constructeur.InfoExtraite(1, 1, 0) = CShort(NumInfo + 1)
                End If
                If Valeur_N3 <> "" Then
                    Constructeur.NoInfoSelection(2, 1) = CShort(NumInfo + 2)
                    Constructeur.ValeuraComparer(2, VI.Operateurs.OU, VI.Operateurs.Egalite, False) = Valeur_N3
                    Constructeur.InfoExtraite(2, 1, 0) = CShort(NumInfo + 2)
                End If

                OrdreSql = Constructeur.OrdreSqlDynamique

                Proxy = New Virtualia.Ressources.WebService.Serveur(VirUrlWebServeur)
                ChaineLue = Proxy.SelectionSql(WsPointeurUtiGlobal.V_NomdUtilisateurSgbd, PointdeVue, 1, OrdreSql, 1)
                Proxy.Dispose()

                If ChaineLue = "" Then
                    Return Nothing
                    Exit Property
                End If
                Dim TabRes As New ArrayList
                TableauData = Strings.Split(ChaineLue, VI.Tild, -1)
                If IsNumeric(TableauData(0)) Then
                    If TableauData(0) <> Ide.ToString Then
                        TabRes.Add(TableauData(0))
                    End If
                End If
                If TabRes.Count > 0 Then
                    Return TabRes
                Else
                    Return Nothing
                End If
            End Get
        End Property

        Public ReadOnly Property NouvelIdentifiant(ByVal NomTableSgbd As String) As Integer
            Get
                Dim Proxy As New Virtualia.Ressources.WebService.Serveur(VirUrlWebServeur)
                Dim Ide As Integer
                Ide = Proxy.ObtenirUnCompteur(WsPointeurUtiGlobal.V_NomdUtilisateurSgbd, NomTableSgbd, "Max") + 1
                Proxy.Dispose()
                Return Ide
            End Get
        End Property

        Public ReadOnly Property TabledesCategories() As String()
            Get
                Dim Tabliste(12) As String
                Dim I As Integer
                For I = 0 To Tabliste.Count - 1
                    Tabliste(I) = "9999" & VI.Tild & LibelleCategorie(I) & VI.Tild & I.ToString
                Next I
                Return Tabliste
            End Get
        End Property

        Public ReadOnly Property CategorieNum(ByVal Valeur As String) As Short
            Get
                Dim I As Integer
                For I = 0 To 12
                    If LibelleCategorie(I) = Valeur Then
                        Return CShort(I)
                    End If
                Next I
                Return 0
            End Get
        End Property

        Public ReadOnly Property LibelleCategorie(ByVal Index As Integer) As String
            Get
                Select Case Index
                    Case VI.CategorieRH.InfosPersonnelles
                        Return "Informations personnelles"
                    Case VI.CategorieRH.Diplomes_Qualification
                        Return "Diplômes et qualificatons"
                    Case VI.CategorieRH.SituationAdministrative
                        Return "Situation administrative"
                    Case VI.CategorieRH.AffectationsFonctionnelles
                        Return "Affectations fonctionnelles"
                    Case VI.CategorieRH.AffectationsBudgetaires
                        Return "Affectations budgétaires"
                    Case VI.CategorieRH.TempsdeTravail
                        Return "Temps de travail, congés et absences"
                    Case VI.CategorieRH.Formation
                        Return "Formation continue"
                    Case VI.CategorieRH.Evaluation_Gpec
                        Return "Entretiens et évaluations "
                    Case VI.CategorieRH.Remuneration
                        Return "Rémunération"
                    Case VI.CategorieRH.Frais_Deplacement
                        Return "Frais de déplacement"
                    Case VI.CategorieRH.Social
                        Return "Informations sociales"
                    Case VI.CategorieRH.Retraite
                        Return "Retraite"
                    Case Else
                        Return "(Non défini)"
                End Select
            End Get
        End Property

        Private Sub InitialiserListeImages()
            WsUrlImageArmoire(0) = "~/Images/Armoire/JauneFermer16.bmp"
            WsUrlImageArmoire(1) = "~/Images/Armoire/OrangeFonceFermer16.bmp"
            WsUrlImageArmoire(2) = "~/Images/Armoire/TurquoiseFermer16.bmp"
            WsUrlImageArmoire(3) = "~/Images/Armoire/BleuFermer16.bmp"
            WsUrlImageArmoire(4) = "~/Images/Armoire/BleuFonceFermer16.bmp"
            WsUrlImageArmoire(5) = "~/Images/Armoire/SaumonFermer16.bmp"
            WsUrlImageArmoire(6) = "~/Images/Armoire/RougeCarminFermer16.bmp"
            WsUrlImageArmoire(7) = "~/Images/Armoire/MarronFermer16.bmp"
            WsUrlImageArmoire(8) = "~/Images/Armoire/GrisClairFermer16.bmp"
            WsUrlImageArmoire(9) = "~/Images/Armoire/GrisFonceFermer16.bmp"
            WsUrlImageArmoire(10) = "~/Images/Armoire/NoirFermer16.bmp"
            WsUrlImageArmoire(11) = "~/Images/Armoire/VerdatreFermer16.bmp"
            WsUrlImageArmoire(12) = "~/Images/Armoire/VertClairFermer16.bmp"
            WsUrlImageArmoire(13) = "~/Images/Armoire/VertFermer16.bmp"
            WsUrlImageArmoire(14) = "~/Images/Armoire/VertFonceFermer16.bmp"
            WsUrlImageArmoire(15) = "~/Images/Armoire/RougeFermer16.bmp"
            WsUrlImageArmoire(16) = "~/Images/Armoire/JauneOuvert16.bmp"
            WsUrlImageArmoire(17) = "~/Images/Armoire/OrangeFonceOuvert16.bmp"
            WsUrlImageArmoire(18) = "~/Images/Armoire/TurquoiseOuvert16.bmp"
            WsUrlImageArmoire(19) = "~/Images/Armoire/BleuOuvert16.bmp"
            WsUrlImageArmoire(20) = "~/Images/Armoire/BleuFonceOuvert16.bmp"
            WsUrlImageArmoire(21) = "~/Images/Armoire/SaumonOuvert16.bmp"
            WsUrlImageArmoire(22) = "~/Images/Armoire/RougeCarminOuvert16.bmp"
            WsUrlImageArmoire(23) = "~/Images/Armoire/OrangeOuvert16.bmp"
            WsUrlImageArmoire(24) = "~/Images/Armoire/GrisClairOuvert16.bmp"
            WsUrlImageArmoire(25) = "~/Images/Armoire/GrisFonceOuvert16.bmp"
            WsUrlImageArmoire(26) = "~/Images/Armoire/NoirOuvert16.bmp"
            WsUrlImageArmoire(27) = "~/Images/Armoire/VerdatreOuvert16.bmp"
            WsUrlImageArmoire(28) = "~/Images/Armoire/VertClairOuvert16.bmp"
            WsUrlImageArmoire(29) = "~/Images/Armoire/VertOuvert16.bmp"
            WsUrlImageArmoire(30) = "~/Images/Armoire/VertFonceOuvert16.bmp"
            WsUrlImageArmoire(31) = "~/Images/Armoire/RougeOuvert16.bmp"
        End Sub

        Public Sub EcrireLogTraitement(ByVal Nature As String, ByVal SiDatee As Boolean, ByVal Msg As String)
            Dim FicStream As System.IO.FileStream
            Dim FicWriter As System.IO.StreamWriter
            Dim NomLog As String
            Dim NomRep As String
            Dim SysCodeIso As System.Text.Encoding = System.Text.Encoding.GetEncoding(1252)
            Dim dateValue As Date

            NomRep = System.Configuration.ConfigurationManager.AppSettings("RepertoireVirtualia")
            NomLog = Systeme.Constantes.DossierVirtualiaService(NomRep, "Logs") & Nature & ".log"
            FicStream = New System.IO.FileStream(NomLog, IO.FileMode.Append, IO.FileAccess.Write)
            FicWriter = New System.IO.StreamWriter(FicStream, SysCodeIso)
            dateValue = System.DateTime.Now
            Select Case Msg
                Case Is = ""
                    FicWriter.WriteLine(Strings.StrDup(20, "-") & Strings.Space(1) & dateValue.ToString("dd/MM/yyyy hh:mm:ss.fff tt") & Space(1) & Strings.StrDup(20, "-"))
                Case Else
                    Select Case SiDatee
                        Case True
                            FicWriter.WriteLine(dateValue.ToString("dd/MM/yyyy hh:mm:ss.fff tt") & Space(1) & Msg)
                        Case False
                            FicWriter.WriteLine(Space(5) & Msg)
                    End Select
            End Select
            FicWriter.Flush()
            FicWriter.Close()
            FicStream.Close()
        End Sub

        Public Sub EcrireLogErreur(ByVal Origine As String, ByVal OrdreSql As String)
            Dim FicStream As System.IO.FileStream
            Dim FicWriter As System.IO.StreamWriter
            Dim NomLog As String
            Dim NomRep As String
            Dim SysCodeIso As System.Text.Encoding = System.Text.Encoding.GetEncoding(1252)

            NomRep = System.Configuration.ConfigurationManager.AppSettings("RepertoireVirtualia")
            NomLog = Systeme.Constantes.DossierVirtualiaService(NomRep, "Logs") & Origine & ".log"
            FicStream = New System.IO.FileStream(NomLog, IO.FileMode.Append, IO.FileAccess.Write)
            FicWriter = New System.IO.StreamWriter(FicStream, SysCodeIso)
            FicWriter.WriteLine("-------------------------------------")
            FicWriter.WriteLine("Le " & Format(System.DateTime.Now, "G"))
            FicWriter.WriteLine(OrdreSql)
            FicWriter.WriteLine("-------------------------------------")
            FicWriter.Flush()
            FicWriter.Close()
            FicStream.Close()
        End Sub

        Public Sub New(ByVal NomUtilisateur As String)
            Call ObtenirSgbd(NomUtilisateur)
            Call ObtenirModele(NomUtilisateur)
            Call InitialiserListeImages()
        End Sub

        Public Overloads Sub Dispose() Implements IDisposable.Dispose
            Dispose(True)
            GC.SuppressFinalize(Me)
        End Sub

        Protected Overridable Overloads Sub Dispose(ByVal disposing As Boolean)
            If Not Me.disposedValue Then
                If disposing Then
                    Me.List.Clear()
                End If
            End If
            Me.disposedValue = True
        End Sub

    End Class
End Namespace
