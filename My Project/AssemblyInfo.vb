﻿Imports System
Imports System.Reflection
Imports System.Runtime.InteropServices

' Les informations générales relatives à un assembly dépendent de 
' l'ensemble d'attributs suivant. Changez les valeurs de ces attributs pour modifier les informations
' associées à un assembly.

' Passez en revue les valeurs des attributs de l'assembly
<Assembly: AssemblyTitle("Virtualia.Version4")> 
<Assembly: AssemblyDescription("Virtualia - Version 4.0")> 
<Assembly: AssemblyCompany("Virtualia")> 
<Assembly: AssemblyProduct("Virtualia - Version 4.0")> 
<Assembly: AssemblyCopyright("Copyright ©  1992-2010")> 
<Assembly: AssemblyTrademark("Virtualia.Net")> 

<Assembly: ComVisible(False)>

'Le GUID suivant est pour l'ID de la typelib si ce projet est exposé à COM
<Assembly: Guid("c63676b0-b3d0-4c06-9b9f-e0c83bd07a1f")> 

' Les informations de version pour un assembly se composent des quatre valeurs suivantes :
'
'      Version principale
'      Version secondaire 
'      Numéro de build
'      Révision
'
' Vous pouvez spécifier toutes les valeurs ou indiquer les numéros de build et de révision par défaut 
' en utilisant '*', comme indiqué ci-dessous :
' <Assembly: AssemblyVersion("1.0.*")> 

<Assembly: AssemblyVersion("4.0.2010.7")> 
<Assembly: AssemblyFileVersion("4.0.2010.7")> 
