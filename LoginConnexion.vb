﻿Option Explicit On
Option Strict On
Option Compare Text
Imports Microsoft.VisualBasic
Imports VI = Virtualia.Systeme.Constantes
Namespace Session
    Public Class LoginConnexion
        Private WsAppGlobal As Virtualia.Net.Session.ObjetGlobal
        Private WsRhDate As Virtualia.Systeme.Fonctions.CalculDates
        Private WsRhFonction As Virtualia.Systeme.Fonctions.Generales
        '
        Private WsIDSessionIIS As String
        Private WsNoBd As Short
        '
        Private WsNom As String
        Private WsPrenom As String
        Private WsIdentifiant As Integer
        Private WsEmailPro As String = ""
        Private WsEmailPerso As String = ""
        Private WsFiltreEtablissement As String = ""
        Private WsParametreNumerique As Integer
        '
        Private WsMessage As String = ""

        Public Property IDSessionIIS As String
            Get
                Return WsIDSessionIIS
            End Get
            Set(ByVal value As String)
                WsIDSessionIIS = value
            End Set
        End Property

        Public Property IDSessionIIS(ByVal NomUti As String, ByVal PrenomUti As String) As String 'Pour Connexion temporaire
            Get
                Return WsIDSessionIIS
            End Get
            Set(ByVal value As String)
                WsIDSessionIIS = value
                WsNom = NomUti
                WsPrenom = PrenomUti
            End Set
        End Property

        Public ReadOnly Property Si_V4_ConnexionAutorisee(ByVal NomUti As String, ByVal Pw As String, ByVal NomMachine As String, _
                                    ByVal AdresseIp As String, ByVal NoBd As Short) As Boolean
            Get
                Dim ProxyServeur As Virtualia.Ressources.WebService.Serveur
                Dim SiOK As Boolean = False

                ProxyServeur = New Virtualia.Ressources.WebService.Serveur(WsAppGlobal.VirUrlWebServeur)
                Select Case ProxyServeur.OuvertureSession(NomUti, Pw, NomMachine, AdresseIp, NoBd)
                    Case VI.CnxUtilisateur.CnxOK, VI.CnxUtilisateur.CnxReprise, VI.CnxUtilisateur.CnxVirtualia
                        WsMessage = "Identification OK"
                        SiOK = True
                    Case VI.CnxUtilisateur.CnxRefus
                        WsMessage = "Identification inconnue"
                    Case VI.CnxUtilisateur.CnxDejaConnecte
                        WsMessage = "Utilisateur déjà connecté sur un autre poste de travail"
                        SiOK = True
                    Case VI.CnxUtilisateur.CnxErreurSysteme
                        WsMessage = "Erreur système - Base de données inaccessible "
                    Case VI.CnxUtilisateur.CnxNbLicences
                        WsMessage = "Nombre de licences d'accés simultanés dépassé."
                End Select

                WsIdentifiant = IdentifiantVirtuel(NomUti)

                If SiOK = True Then
                    Dim TableauData(0) As String
                    Dim Chaine As String
                    Chaine = ProxyServeur.InformationSession(NomUti)
                    If Chaine <> "" Then
                        TableauData = Strings.Split(Chaine, VI.Tild, -1)
                        WsNoBd = CShort(TableauData(3))
                        WsNom = NomUti
                        WsPrenom = ""
                        WsEmailPro = ""
                        WsEmailPerso = ""
                    End If
                End If

                ProxyServeur.Dispose()
                Return SiOK

            End Get
        End Property

        Public ReadOnly Property Si_V4_ModificationPasswordOK(ByVal NomUtiCnx As String, ByVal NewPW As String, Optional ByVal NomUtiSgbd As String = "") As Boolean
            Get
                Dim ProxyServeur As Virtualia.Ressources.WebService.Serveur
                Dim TableauData(0) As String
                Dim Chaine As String
                Dim Cretour As Boolean
                Dim IndiceI As Integer

                If NomUtiSgbd = "" Then
                    NomUtiSgbd = NomUtiCnx
                End If

                ProxyServeur = New Virtualia.Ressources.WebService.Serveur(WsAppGlobal.VirUrlWebServeur)
                Chaine = ProxyServeur.InformationSession(NomUtiCnx)
                If Chaine = "" Then
                    ProxyServeur.Dispose()
                    Return False
                End If
                TableauData = Strings.Split(Chaine, VI.Tild, -1)
                WsNoBd = CShort(TableauData(3))

                Chaine = ProxyServeur.LireProfilUtilisateur(NomUtiSgbd, NomUtiCnx, WsNoBd, 1)
                If Chaine = "" Then
                    ProxyServeur.Dispose()
                    Return False
                End If
                TableauData = Strings.Split(Chaine, VI.Tild, -1)
                TableauData(7) = NewPW
                TableauData(8) = WsRhDate.DateduJour
                TableauData(9) = WsNoBd.ToString
                TableauData(10) = WsRhDate.DateduJour
                Chaine = ""
                For IndiceI = 0 To TableauData.Count - 1
                    Chaine &= TableauData(IndiceI) & VI.Tild
                Next IndiceI
                Cretour = ProxyServeur.MajProfilUtilisateur(NomUtiSgbd, NomUtiCnx, WsNoBd, 1, Chaine)
                ProxyServeur.Dispose()
                Return Cretour
            End Get
        End Property

        Public ReadOnly Property V4_NouveauPassword(ByVal NomUtiCnx As String, ByVal EMail As String, Optional ByVal NomUtiSgbd As String = "") As String
            Get
                Dim ProxyServeur As Virtualia.Ressources.WebService.Serveur
                Dim TableauData(0) As String
                Dim Chaine As String
                Dim NouveauPW As String
                Dim SiOK As Boolean

                If NomUtiSgbd = "" Then
                    NomUtiSgbd = NomUtiCnx
                End If

                ProxyServeur = New Virtualia.Ressources.WebService.Serveur(WsAppGlobal.VirUrlWebServeur)
                Chaine = ProxyServeur.LireProfilUtilisateur(NomUtiSgbd, NomUtiCnx, WsNoBd, 1)
                If Chaine = "" Then
                    ProxyServeur.Dispose()
                    Return ""
                End If
                TableauData = Strings.Split(Chaine, VI.Tild, -1)
                If TableauData.Count < 11 Then
                    Return ""
                End If
                If TableauData(11).ToUpper <> EMail.ToUpper Then
                    Return ""
                End If
                NouveauPW = NouveauMotdePasseAleatoire(CInt(TableauData(0)))
                If NouveauPW = "" Then
                    Return ""
                End If
                SiOK = Si_V4_ModificationPasswordOK(NomUtiCnx, NouveauPW)
                If SiOK = True Then
                    Return NouveauPW
                Else
                    Return ""
                End If
            End Get
        End Property

        Public ReadOnly Property Si_VI_ConnexionAutorisee(ByVal NomUti As String, ByVal PrenomUti As String, ByVal PW As String, ByVal NomUtiSgbd As String) As Boolean
            Get
                If NomUtiSgbd = "" Then
                    Return False
                End If

                Dim TabRes As ArrayList
                Dim IndiceI As Integer
                Dim TableauData(0) As String
                Dim SiOk As Boolean = False

                TabRes = VI_RequeteNomPrenom(NomUti, PrenomUti, NomUtiSgbd)

                If TabRes Is Nothing Then
                    Return False
                End If

                For IndiceI = 0 To TabRes.Count - 1
                    TableauData = Strings.Split(TabRes(IndiceI).ToString, VI.Tild, -1)
                    If TableauData(6) = "Oui" Then
                        If TableauData(7) = WsRhFonction.HashSHA(PW) Then
                            '**OK
                            SiOk = True
                            WsIdentifiant = CInt(TableauData(0))
                            WsNom = TableauData(1)
                            WsPrenom = TableauData(2)
                            WsEmailPro = TableauData(3)
                            WsEmailPerso = TableauData(4)
                            Exit For
                        End If
                    End If
                Next IndiceI

                If SiOk = True Then
                    Call VI_ActualiserFicheLogin(WsIdentifiant, NomUtiSgbd)
                End If

                Return SiOk
            End Get
        End Property

        Public ReadOnly Property Si_VI_ModificationPasswordOK(ByVal Ide As Integer, ByVal NewPW As String, ByVal NomUtiSgbd As String) As Boolean
            Get
                If NomUtiSgbd = "" Then
                    Return False
                End If

                Dim FichePER As Virtualia.TablesObjet.ShemaPER.PER_LOGIN
                Dim Proxy As Virtualia.Ressources.WebService.Serveur
                Dim Chaine As String
                Dim Cretour As Boolean

                Proxy = New Virtualia.Ressources.WebService.Serveur(WsAppGlobal.VirUrlWebServeur)
                Chaine = Proxy.LireUnObjet(NomUtiSgbd, VI.PointdeVue.PVueApplicatif, VI.ObjetPer.ObaLoginIntranet, _
                                         Ide, False, 1)
                If Chaine = "" Then
                    Return False
                End If
                FichePER = New Virtualia.TablesObjet.ShemaPER.PER_LOGIN
                FichePER.ContenuTable = Chaine
                FichePER.DateMaj_PW = WsRhDate.DateduJour
                FichePER.Cle_LDAP = WsRhFonction.HashSHA(NewPW)
                FichePER.MotdePasse = WsRhFonction.CryptageVirtualia(NewPW)

                Cretour = Proxy.MettreAJourUneFiche(NomUtiSgbd, VI.PointdeVue.PVueApplicatif, VI.ObjetPer.ObaLoginIntranet, _
                                   FichePER.Ide_Dossier, FichePER.FicheLue, FichePER.ContenuTable)
                Proxy.Dispose()
                Return Cretour
            End Get
        End Property

        Public ReadOnly Property VI_NouveauPassword(ByVal NomUti As String, ByVal PrenomUti As String, ByVal EMail As String, ByVal NomUtiSgbd As String) As String
            Get
                If NomUtiSgbd = "" Then
                    Return ""
                End If

                Dim TabRes As ArrayList
                Dim IndiceI As Integer
                Dim TableauData(0) As String
                Dim SiOk As Boolean = False
                Dim NouveauPW As String

                TabRes = VI_RequeteNomPrenom(NomUti, PrenomUti, NomUtiSgbd)

                If TabRes Is Nothing Then
                    Return ""
                End If

                For IndiceI = 0 To TabRes.Count - 1
                    TableauData = Strings.Split(TabRes(IndiceI).ToString, VI.Tild, -1)
                    If TableauData(3).ToUpper = EMail.ToUpper Then
                        SiOk = True
                        Exit For
                    End If
                Next IndiceI
                If SiOk = False Then
                    Return ""
                End If
                NouveauPW = NouveauMotdePasseAleatoire(CInt(TableauData(0)))
                If NouveauPW = "" Then
                    Return ""
                End If
                SiOk = Si_VI_ModificationPasswordOK(CInt(TableauData(0)), NouveauPW, NomUtiSgbd)
                If SiOk = True Then
                    Return NouveauPW
                Else
                    Return ""
                End If
            End Get
        End Property

        Public ReadOnly Property Message As String
            Get
                Return WsMessage
            End Get
        End Property

        Public Sub VI_ActualiserFicheLogin(ByVal Ide As Integer, ByVal NomUtiSgbd As String)
            If NomUtiSgbd = "" Then
                Exit Sub
            End If

            Dim FichePER As Virtualia.TablesObjet.ShemaPER.PER_LOGIN
            Dim Proxy As Virtualia.Ressources.WebService.Serveur
            Dim Chaine As String
            Dim Cretour As Boolean

            Proxy = New Virtualia.Ressources.WebService.Serveur(WsAppGlobal.VirUrlWebServeur)
            Chaine = Proxy.LireUnObjet(NomUtiSgbd, VI.PointdeVue.PVueApplicatif, VI.ObjetPer.ObaLoginIntranet, _
                                     Ide, False, 1)
            If Chaine = "" Then
                Exit Sub
            End If
            FichePER = New Virtualia.TablesObjet.ShemaPER.PER_LOGIN
            FichePER.ContenuTable = Chaine
            FichePER.DateAcces = WsRhDate.DateduJour
            FichePER.Heure_Acces = Strings.Format(TimeOfDay, "hh:mm")
            FichePER.Nombre_Acces += 1
            Cretour = Proxy.MettreAJourUneFiche(NomUtiSgbd, VI.PointdeVue.PVueApplicatif, VI.ObjetPer.ObaLoginIntranet, _
                                   FichePER.Ide_Dossier, FichePER.FicheLue, FichePER.ContenuTable)
            Proxy.Dispose()
        End Sub

        Private ReadOnly Property NouveauMotdePasseAleatoire(ByVal Ide As Integer) As String
            Get
                Dim IndiceI As Integer
                Dim IndiceK As Integer
                Dim Chaine As String
                Dim Pw As String
                Dim Heure As String
                Const Algo As String = "bLeuaZUr"

                If Ide <= 0 Then
                    Return ""
                End If
                Chaine = Ide.ToString
                Heure = Strings.Format(Now, "mmss")
                Select Case Chaine.Length
                    Case Is < 4
                        Chaine &= Strings.StrDup(4 - Chaine.Length, "7") & Heure
                    Case Is = 4
                        Chaine &= Heure
                    Case Else
                        Chaine &= Strings.Right(Heure, 8 - Chaine.Length)
                End Select
                Pw = ""
                For IndiceI = 1 To Chaine.Length
                    Pw &= Strings.Format(Asc(Mid(Algo, IndiceI, 1)) + Val(Mid(Chaine, IndiceI, 1)), "000")
                Next IndiceI
                Chaine = ""
                IndiceK = 1
                For IndiceI = 1 To 8
                    Select Case Val(Strings.Mid(Pw, IndiceK, 3))
                        Case 65 To 90
                            Chaine &= Strings.Chr(CInt(Strings.Mid(Pw, IndiceK, 3)))
                        Case 97 To 122
                            Chaine &= Strings.Chr(CInt(Mid(Pw, IndiceK, 3)))
                        Case Else
                            Chaine &= Strings.Right(Heure, 1)
                    End Select
                    IndiceK = IndiceK + 3
                Next IndiceI
                Return Chaine.ToLower
            End Get
        End Property

        Private ReadOnly Property VI_RequeteNomPrenom(ByVal NomUti As String, ByVal PrenomUti As String, ByVal NomUtiSgbd As String) As ArrayList
            Get
                If NomUtiSgbd = "" Then
                    Return Nothing
                End If

                Dim Constructeur As Virtualia.Systeme.Sgbd.Sql.SqlInterne
                Dim ChaineSql As String
                Dim NomVirtualia As String = WsRhFonction.Lettre1Capi(NomUti, 2)
                Dim PrenomVirtualia As String = WsRhFonction.Lettre1Capi(PrenomUti, 1)

                Constructeur = New Virtualia.Systeme.Sgbd.Sql.SqlInterne(WsAppGlobal.VirModele, WsAppGlobal.VirInstanceBd(WsNoBd))
                Constructeur.NombredeRequetes(VI.PointdeVue.PVueApplicatif, "", WsRhDate.DateduJour, VI.Operateurs.ET) = 2
                Constructeur.SiPasdeTriSurIdeDossier = False
                Constructeur.SiHistoriquedeSituation = False
                Constructeur.NoInfoSelection(0, VI.ObjetPer.ObaCivil) = 2
                Constructeur.ValeuraComparer(0, VI.Operateurs.OU, VI.Operateurs.Egalite, False) = NomVirtualia
                Constructeur.NoInfoSelection(1, VI.ObjetPer.ObaCivil) = 3
                Constructeur.ValeuraComparer(1, VI.Operateurs.OU, VI.Operateurs.Egalite, False) = PrenomVirtualia
                Constructeur.InfoExtraite(0, VI.ObjetPer.ObaCivil, 0) = 2 'le nom
                Constructeur.InfoExtraite(1, VI.ObjetPer.ObaCivil, 0) = 3 'le prénom
                Constructeur.InfoExtraite(2, VI.ObjetPer.ObaAdrPro, 0) = 7 'le mail professionnel
                Constructeur.InfoExtraite(3, VI.ObjetPer.ObaAdresse, 0) = 10 'le Mail personnel
                Constructeur.InfoExtraite(4, VI.ObjetPer.ObaLoginIntranet, 0) = 2 'le mot de passe crypté Virtualia
                Constructeur.InfoExtraite(5, VI.ObjetPer.ObaLoginIntranet, 0) = 7 'le statut (Actif ou non)
                Constructeur.InfoExtraite(6, VI.ObjetPer.ObaLoginIntranet, 0) = 13 'le mot de passe crypté SHA

                ChaineSql = Constructeur.OrdreSqlDynamique

                Return WsAppGlobal.RequeteSql(NomUtiSgbd, VI.PointdeVue.PVueApplicatif, 1, ChaineSql)
            End Get
        End Property

        Private ReadOnly Property VI_RequeteNomPrenom(ByVal ide As Integer, ByVal NomUtiSgbd As String) As ArrayList
            Get
                If NomUtiSgbd = "" Then
                    Return Nothing
                End If

                Dim Constructeur As Virtualia.Systeme.Sgbd.Sql.SqlInterne
                Dim ChaineSql As String

                Constructeur = New Virtualia.Systeme.Sgbd.Sql.SqlInterne(WsAppGlobal.VirModele, WsAppGlobal.VirInstanceBd(WsNoBd))
                Constructeur.NombredeRequetes(VI.PointdeVue.PVueApplicatif, "", WsRhDate.DateduJour, VI.Operateurs.ET) = 1
                Constructeur.SiPasdeTriSurIdeDossier = False
                Constructeur.SiHistoriquedeSituation = False
                Constructeur.NoInfoSelection(0, VI.ObjetPer.ObaCivil) = 99
                Constructeur.ValeuraComparer(0, VI.Operateurs.OU, VI.Operateurs.Egalite, False) = ide.ToString
                Constructeur.InfoExtraite(0, VI.ObjetPer.ObaCivil, 0) = 2 'le nom
                Constructeur.InfoExtraite(1, VI.ObjetPer.ObaCivil, 0) = 3 'le prénom
                Constructeur.InfoExtraite(2, VI.ObjetPer.ObaAdrPro, 0) = 7 'le mail professionnel
                Constructeur.InfoExtraite(3, VI.ObjetPer.ObaAdresse, 0) = 10 'le Mail personnel
                Constructeur.InfoExtraite(4, VI.ObjetPer.ObaLoginIntranet, 0) = 2 'le mot de passe crypté Virtualia
                Constructeur.InfoExtraite(5, VI.ObjetPer.ObaLoginIntranet, 0) = 7 'le statut (Actif ou non)
                Constructeur.InfoExtraite(6, VI.ObjetPer.ObaLoginIntranet, 0) = 13 'le mot de passe crypté SHA

                ChaineSql = Constructeur.OrdreSqlDynamique

                Return WsAppGlobal.RequeteSql(NomUtiSgbd, VI.PointdeVue.PVueApplicatif, 1, ChaineSql)
            End Get
        End Property

        Public ReadOnly Property Identifiant As Integer
            Get
                Return WsIdentifiant
            End Get
        End Property

        Public ReadOnly Property Nom As String
            Get
                Return WsNom
            End Get
        End Property

        Public ReadOnly Property Prenom As String
            Get
                Return WsPrenom
            End Get
        End Property

        Public ReadOnly Property Email_Professionnel As String
            Get
                Return WsEmailPro
            End Get
        End Property


        Public ReadOnly Property Filtre_Etablissement As String
            Get
                Return WsFiltreEtablissement
            End Get
        End Property

        Public Property ParametreNumerique As Integer
            Get
                Return WsParametreNumerique
            End Get
            Set(ByVal value As Integer)
                WsParametreNumerique = value
            End Set
        End Property

        Private ReadOnly Property IdentifiantVirtuel(ByVal Nom As String) As Integer
            Get
                Dim IndiceI As Integer
                Dim IndiceK As Integer = 0
                Dim ChaineNum As String = ""

                For IndiceI = Nom.Length To 1 Step -1
                    Select Case Strings.Mid(Nom, IndiceI, 1)
                        Case "A", "X", "Y", "Z"
                            ChaineNum &= "0"
                            IndiceK += 1
                        Case "B", "V", "W"
                            ChaineNum &= "9"
                            IndiceK += 1
                        Case "C", "T", "U"
                            ChaineNum &= "8"
                            IndiceK += 1
                        Case "D", "S"
                            ChaineNum &= "7"
                            IndiceK += 1
                        Case "E", "Q", "R"
                            ChaineNum &= "6"
                            IndiceK += 1
                        Case "F", "O", "P"
                            ChaineNum &= "5"
                            IndiceK += 1
                        Case "G", "N"
                            ChaineNum &= "4"
                            IndiceK += 1
                        Case "H", "M"
                            ChaineNum &= "3"
                            IndiceK += 1
                        Case "I", "L"
                            ChaineNum &= "2"
                            IndiceK += 1
                        Case "J", "K"
                            ChaineNum &= "1"
                            IndiceK += 1
                    End Select
                    If IndiceK > 5 Then
                        Exit For
                    End If
                Next IndiceI
                Return CInt(ChaineNum)
            End Get
        End Property

        Public Sub New(ByVal Host As Virtualia.Net.Session.ObjetGlobal, ByVal NomCnx As String, ByVal Ide As Integer, ByVal Filtre As String)
            WsAppGlobal = Host
            WsNom = NomCnx
            WsIdentifiant = Ide
            WsFiltreEtablissement = Filtre
            WsRhDate = New Virtualia.Systeme.Fonctions.CalculDates
            WsRhFonction = New Virtualia.Systeme.Fonctions.Generales
        End Sub

        Public Sub New(ByVal Host As Virtualia.Net.Session.ObjetGlobal)
            WsAppGlobal = Host
            WsRhDate = New Virtualia.Systeme.Fonctions.CalculDates
            WsRhFonction = New Virtualia.Systeme.Fonctions.Generales
        End Sub
    End Class
End Namespace