﻿
Partial Class _Default
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Application.Item("CheminRelatif") Is Nothing Then
            If Request.ApplicationPath <> "/" Then
                Application.Add("CheminRelatif", Request.ApplicationPath & "/")
            Else
                Application.Add("CheminRelatif", "/")
            End If
        End If
        Dim Chaine As New System.Text.StringBuilder
        Chaine.Append("Nom Cnx Anonyme = " & Request.AnonymousID & Strings.Chr(13) & Strings.Chr(10))
        Chaine.Append("Nom Cnx Windows = " & Request.LogonUserIdentity.Name & Strings.Chr(13) & Strings.Chr(10))
        Chaine.Append("Si Anonyme = " & Request.LogonUserIdentity.IsAnonymous.ToString & Strings.Chr(13) & Strings.Chr(10))
        Chaine.Append("Si Authentifié = " & Request.LogonUserIdentity.IsAuthenticated.ToString & Strings.Chr(13) & Strings.Chr(10))
        Chaine.Append("Browser = " & Request.Browser.Id & Strings.Chr(13) & Strings.Chr(10))
        Chaine.Append("Browser N° Version= " & Request.Browser.Version & Strings.Chr(13) & Strings.Chr(10))
        Chaine.Append("Adresse IP = " & Request.UserHostAddress & Strings.Chr(13) & Strings.Chr(10))
        Chaine.Append("Machine cliente = " & Request.UserHostName & Strings.Chr(13) & Strings.Chr(10))
        Chaine.Append("Chemin Application = " & Request.PhysicalApplicationPath & Strings.Chr(13) & Strings.Chr(10))
        Chaine.Append("Chemin Fichiers = " & Request.PhysicalPath & Strings.Chr(13) & Strings.Chr(10))
        Chaine.Append("URL Racine = " & Request.ApplicationPath & Strings.Chr(13) & Strings.Chr(10))
        Chaine.Append("URL Absolue = " & Request.Url.ToString & Strings.Chr(13) & Strings.Chr(10))
        Chaine.Append("URL Relative = " & Request.RawUrl.ToString & Strings.Chr(13) & Strings.Chr(10))
        Chaine.Append("URL Repertoire virtuel = " & Request.FilePath & Strings.Chr(13) & Strings.Chr(10))
        Chaine.Append("URL Courante = " & Request.CurrentExecutionFilePath & Strings.Chr(13) & Strings.Chr(10))
        Chaine.Append("Méthode HTTP = " & Request.HttpMethod & Strings.Chr(13) & Strings.Chr(10))
        Chaine.Append("Si Https = " & Request.IsSecureConnection.ToString & Strings.Chr(13) & Strings.Chr(10))
        Chaine.Append("")
    End Sub

    Protected Sub Page_LoadComplete(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.LoadComplete
        Dim AppObjetGlobal As Virtualia.Net.Session.ObjetGlobal

        Session.Add("Machine", Request.UserHostName)
        Session.Add("IP", Request.UserHostAddress)
        Session.Add("Identite", Request.LogonUserIdentity.Name)

        AppObjetGlobal = CType(Me.Application.Item("VirGlobales"), Virtualia.Net.Session.ObjetGlobal)
        If AppObjetGlobal Is Nothing Then
            Response.Redirect("~/Fenetres/Connexion/FrmConnexion.aspx")
            Exit Sub
        End If

        Dim NoBd As Short = CShort(System.Configuration.ConfigurationManager.AppSettings("NumeroDatabase"))
        Dim Uti As Virtualia.Net.Session.ObjetSession
        Dim TabUti(0) As String
        Dim NomUti As String
        TabUti = Strings.Split(Request.LogonUserIdentity.Name, "\", -1)
        If TabUti Is Nothing Then
            Response.Redirect("~/Fenetres/Connexion/FrmConnexion.aspx")
            Exit Sub
        End If

        If TabUti.Count = 2 Then
            NomUti = TabUti(1).ToUpper
        Else
            NomUti = TabUti(0).ToUpper
        End If
        Uti = AppObjetGlobal.ItemUti(NomUti, NoBd)
        If Uti Is Nothing Then
            Response.Redirect("~/Fenetres/Connexion/FrmConnexion.aspx")
        Else
            Uti.V_IDSession = Session.SessionID
            Uti.ID_AdresseIP = Request.UserHostAddress
            Uti.ID_Machine = Request.UserHostName
            Response.Redirect("~/Fenetres/FrmInfosPersonnelles.aspx")
        End If
    End Sub
End Class
